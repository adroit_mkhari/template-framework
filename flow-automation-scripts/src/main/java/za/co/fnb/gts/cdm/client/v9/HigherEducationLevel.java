
package za.co.fnb.gts.cdm.client.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for HigherEducationLevel.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="HigherEducationLevel"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="BACHELORS"/&gt;
 *     &lt;enumeration value="DIPLOMA"/&gt;
 *     &lt;enumeration value="DOCTORATE"/&gt;
 *     &lt;enumeration value="GRADUATE"/&gt;
 *     &lt;enumeration value="GRADE_10"/&gt;
 *     &lt;enumeration value="GRADE_12"/&gt;
 *     &lt;enumeration value="HONOURS"/&gt;
 *     &lt;enumeration value="INCOMPLETE"/&gt;
 *     &lt;enumeration value="LOWER_THAN_GRADE_10"/&gt;
 *     &lt;enumeration value="MASTERS"/&gt;
 *     &lt;enumeration value="DEGREE"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "HigherEducationLevel")
@XmlEnum
public enum HigherEducationLevel {

    BACHELORS,
    DIPLOMA,
    DOCTORATE,
    GRADUATE,
    GRADE_10,
    GRADE_12,
    HONOURS,
    INCOMPLETE,
    LOWER_THAN_GRADE_10,
    MASTERS,
    DEGREE;

    public String value() {
        return name();
    }

    public static HigherEducationLevel fromValue(String v) {
        return valueOf(v);
    }

}
