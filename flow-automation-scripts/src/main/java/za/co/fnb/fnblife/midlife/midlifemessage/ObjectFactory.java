
package za.co.fnb.fnblife.midlife.midlifemessage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import za.co.fnb.fnblife.midlife.LookupMandateDetailsRequest;
import za.co.fnb.fnblife.midlife.LookupMandateDetailsRequestType;
import za.co.fnb.fnblife.midlife.LookupMandateDetailsResponse;
import za.co.fnb.fnblife.midlife.LookupMandateDetailsResponseType;
import za.co.fnb.fnblife.midlife.MandateDetailsRequest;
import za.co.fnb.fnblife.midlife.MandateDetailsResponse;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.midlife.midlifemessage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FraudCheckRequest_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "FraudCheckRequest");
    private final static QName _FraudCheckResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "FraudCheckResponse");
    private final static QName _LookupMandateDetailsRequest_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "LookupMandateDetailsRequest");
    private final static QName _LookupMandateDetailsResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "LookupMandateDetailsResponse");
    private final static QName _CollectionAmount_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "collectionAmount");
    private final static QName _EffectiveDate_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "effectiveDate");
    private final static QName _ExtrnalPolicyID_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "extrnalPolicyID");
    private final static QName _FraudCheckRequest1_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "fraudCheckRequest1");
    private final static QName _FraudCheckResponse1_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "fraudCheckResponse1");
    private final static QName _Header_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "header");
    private final static QName _LookupMandateDetailsRequest1_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "lookupMandateDetailsRequest1");
    private final static QName _LookupMandateDetailsRequestType_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "lookupMandateDetailsRequestType");
    private final static QName _LookupMandateDetailsResponse1_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "lookupMandateDetailsResponse1");
    private final static QName _LookupMandateDetailsResponseType_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "lookupMandateDetailsResponseType");
    private final static QName _MandateDetailsRequest_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "mandateDetailsRequest");
    private final static QName _MandateDetailsResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "mandateDetailsResponse");
    private final static QName _MandateReference_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "mandateReference");
    private final static QName _MandateRegistered_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "mandateRegistered");
    private final static QName _PingResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "pingResponse");
    private final static QName _SendToBankDate_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "sendToBankDate");
    private final static QName _Ucn_QNAME = new QName("http://midlife.fnblife.fnb.co.za/MidLifeMessage", "ucn");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.midlife.midlifemessage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FraudCheckRequest }
     * 
     */
    public FraudCheckRequest createFraudCheckRequest() {
        return new FraudCheckRequest();
    }

    /**
     * Create an instance of {@link FraudCheckResponse }
     * 
     */
    public FraudCheckResponse createFraudCheckResponse() {
        return new FraudCheckResponse();
    }

    /**
     * Create an instance of {@link MidLifeMessage }
     * 
     */
    public MidLifeMessage createMidLifeMessage() {
        return new MidLifeMessage();
    }

    /**
     * Create an instance of {@link PingResponse }
     * 
     */
    public PingResponse createPingResponse() {
        return new PingResponse();
    }

    /**
     * Create an instance of {@link FraudDetails }
     * 
     */
    public FraudDetails createFraudDetails() {
        return new FraudDetails();
    }

    /**
     * Create an instance of {@link Channel }
     * 
     */
    public Channel createChannel() {
        return new Channel();
    }

    /**
     * Create an instance of {@link Individual }
     * 
     */
    public Individual createIndividual() {
        return new Individual();
    }

    /**
     * Create an instance of {@link Company }
     * 
     */
    public Company createCompany() {
        return new Company();
    }

    /**
     * Create an instance of {@link OtherCriteria }
     * 
     */
    public OtherCriteria createOtherCriteria() {
        return new OtherCriteria();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudCheckRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FraudCheckRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "FraudCheckRequest")
    public JAXBElement<FraudCheckRequest> createFraudCheckRequest(FraudCheckRequest value) {
        return new JAXBElement<FraudCheckRequest>(_FraudCheckRequest_QNAME, FraudCheckRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudCheckResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FraudCheckResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "FraudCheckResponse")
    public JAXBElement<FraudCheckResponse> createFraudCheckResponse(FraudCheckResponse value) {
        return new JAXBElement<FraudCheckResponse>(_FraudCheckResponse_QNAME, FraudCheckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "LookupMandateDetailsRequest")
    public JAXBElement<LookupMandateDetailsRequest> createLookupMandateDetailsRequest(LookupMandateDetailsRequest value) {
        return new JAXBElement<LookupMandateDetailsRequest>(_LookupMandateDetailsRequest_QNAME, LookupMandateDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "LookupMandateDetailsResponse")
    public JAXBElement<LookupMandateDetailsResponse> createLookupMandateDetailsResponse(LookupMandateDetailsResponse value) {
        return new JAXBElement<LookupMandateDetailsResponse>(_LookupMandateDetailsResponse_QNAME, LookupMandateDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "collectionAmount")
    public JAXBElement<String> createCollectionAmount(String value) {
        return new JAXBElement<String>(_CollectionAmount_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "effectiveDate")
    public JAXBElement<String> createEffectiveDate(String value) {
        return new JAXBElement<String>(_EffectiveDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "extrnalPolicyID")
    public JAXBElement<String> createExtrnalPolicyID(String value) {
        return new JAXBElement<String>(_ExtrnalPolicyID_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudCheckRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FraudCheckRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "fraudCheckRequest1")
    public JAXBElement<FraudCheckRequest> createFraudCheckRequest1(FraudCheckRequest value) {
        return new JAXBElement<FraudCheckRequest>(_FraudCheckRequest1_QNAME, FraudCheckRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FraudCheckResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FraudCheckResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "fraudCheckResponse1")
    public JAXBElement<FraudCheckResponse> createFraudCheckResponse1(FraudCheckResponse value) {
        return new JAXBElement<FraudCheckResponse>(_FraudCheckResponse1_QNAME, FraudCheckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MidLifeHeaderType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MidLifeHeaderType }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "header")
    public JAXBElement<MidLifeHeaderType> createHeader(MidLifeHeaderType value) {
        return new JAXBElement<MidLifeHeaderType>(_Header_QNAME, MidLifeHeaderType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "lookupMandateDetailsRequest1")
    public JAXBElement<LookupMandateDetailsRequest> createLookupMandateDetailsRequest1(LookupMandateDetailsRequest value) {
        return new JAXBElement<LookupMandateDetailsRequest>(_LookupMandateDetailsRequest1_QNAME, LookupMandateDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "lookupMandateDetailsRequestType")
    public JAXBElement<LookupMandateDetailsRequestType> createLookupMandateDetailsRequestType(LookupMandateDetailsRequestType value) {
        return new JAXBElement<LookupMandateDetailsRequestType>(_LookupMandateDetailsRequestType_QNAME, LookupMandateDetailsRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "lookupMandateDetailsResponse1")
    public JAXBElement<LookupMandateDetailsResponse> createLookupMandateDetailsResponse1(LookupMandateDetailsResponse value) {
        return new JAXBElement<LookupMandateDetailsResponse>(_LookupMandateDetailsResponse1_QNAME, LookupMandateDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LookupMandateDetailsResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "lookupMandateDetailsResponseType")
    public JAXBElement<LookupMandateDetailsResponseType> createLookupMandateDetailsResponseType(LookupMandateDetailsResponseType value) {
        return new JAXBElement<LookupMandateDetailsResponseType>(_LookupMandateDetailsResponseType_QNAME, LookupMandateDetailsResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MandateDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MandateDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "mandateDetailsRequest")
    public JAXBElement<MandateDetailsRequest> createMandateDetailsRequest(MandateDetailsRequest value) {
        return new JAXBElement<MandateDetailsRequest>(_MandateDetailsRequest_QNAME, MandateDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MandateDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MandateDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "mandateDetailsResponse")
    public JAXBElement<MandateDetailsResponse> createMandateDetailsResponse(MandateDetailsResponse value) {
        return new JAXBElement<MandateDetailsResponse>(_MandateDetailsResponse_QNAME, MandateDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "mandateReference")
    public JAXBElement<String> createMandateReference(String value) {
        return new JAXBElement<String>(_MandateReference_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Boolean }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "mandateRegistered")
    public JAXBElement<Boolean> createMandateRegistered(Boolean value) {
        return new JAXBElement<Boolean>(_MandateRegistered_QNAME, Boolean.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link PingResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link PingResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "pingResponse")
    public JAXBElement<PingResponse> createPingResponse(PingResponse value) {
        return new JAXBElement<PingResponse>(_PingResponse_QNAME, PingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "sendToBankDate")
    public JAXBElement<String> createSendToBankDate(String value) {
        return new JAXBElement<String>(_SendToBankDate_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", name = "ucn")
    public JAXBElement<String> createUcn(String value) {
        return new JAXBElement<String>(_Ucn_QNAME, String.class, null, value);
    }

}
