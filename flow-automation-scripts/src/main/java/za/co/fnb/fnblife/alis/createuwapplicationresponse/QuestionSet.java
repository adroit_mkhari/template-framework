
package za.co.fnb.fnblife.alis.createuwapplicationresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for QuestionSet complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="QuestionSet"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="QuestionID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="Hierarchy" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="QuestionText" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="AnswerType" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="AnswerOptions" type="{http://alis.fnblife.fnb.co.za/createUWApplicationResponse}AnswerOptions" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "QuestionSet", propOrder = {
    "questionID",
    "hierarchy",
    "questionText",
    "answerType",
    "answerOptions"
})
public class QuestionSet {

    @XmlElement(name = "QuestionID", required = true)
    protected BigInteger questionID;
    @XmlElement(name = "Hierarchy", required = true)
    protected BigInteger hierarchy;
    @XmlElement(name = "QuestionText", required = true)
    protected String questionText;
    @XmlElement(name = "AnswerType", required = true)
    protected BigInteger answerType;
    @XmlElement(name = "AnswerOptions")
    protected AnswerOptions answerOptions;

    /**
     * Gets the value of the questionID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuestionID() {
        return questionID;
    }

    /**
     * Sets the value of the questionID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuestionID(BigInteger value) {
        this.questionID = value;
    }

    /**
     * Gets the value of the hierarchy property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getHierarchy() {
        return hierarchy;
    }

    /**
     * Sets the value of the hierarchy property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setHierarchy(BigInteger value) {
        this.hierarchy = value;
    }

    /**
     * Gets the value of the questionText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuestionText() {
        return questionText;
    }

    /**
     * Sets the value of the questionText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuestionText(String value) {
        this.questionText = value;
    }

    /**
     * Gets the value of the answerType property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAnswerType() {
        return answerType;
    }

    /**
     * Sets the value of the answerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAnswerType(BigInteger value) {
        this.answerType = value;
    }

    /**
     * Gets the value of the answerOptions property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerOptions }
     *     
     */
    public AnswerOptions getAnswerOptions() {
        return answerOptions;
    }

    /**
     * Sets the value of the answerOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerOptions }
     *     
     */
    public void setAnswerOptions(AnswerOptions value) {
        this.answerOptions = value;
    }

}
