
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Covers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Covers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Cover" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}Cover" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Covers", propOrder = {
    "cover"
})
public class Covers {

    @XmlElement(name = "Cover", required = true)
    protected List<Cover> cover;

    /**
     * Gets the value of the cover property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cover property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCover().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Cover }
     * 
     * 
     */
    public List<Cover> getCover() {
        if (cover == null) {
            cover = new ArrayList<Cover>();
        }
        return this.cover;
    }

}
