
package za.co.fnb.fnblife.alis.createuwapplicationresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverLayers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverLayers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoverLayerNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="ConsequenceID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="LoadingDetails" type="{http://alis.fnblife.fnb.co.za/createUWApplicationResponse}LoadingDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverLayers", propOrder = {
    "coverLayerNo",
    "consequenceID",
    "loadingDetails"
})
public class CoverLayers {

    @XmlElement(name = "CoverLayerNo", required = true)
    protected BigInteger coverLayerNo;
    @XmlElement(name = "ConsequenceID", required = true)
    protected BigInteger consequenceID;
    @XmlElement(name = "LoadingDetails")
    protected LoadingDetails loadingDetails;

    /**
     * Gets the value of the coverLayerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCoverLayerNo() {
        return coverLayerNo;
    }

    /**
     * Sets the value of the coverLayerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCoverLayerNo(BigInteger value) {
        this.coverLayerNo = value;
    }

    /**
     * Gets the value of the consequenceID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getConsequenceID() {
        return consequenceID;
    }

    /**
     * Sets the value of the consequenceID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setConsequenceID(BigInteger value) {
        this.consequenceID = value;
    }

    /**
     * Gets the value of the loadingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link LoadingDetails }
     *     
     */
    public LoadingDetails getLoadingDetails() {
        return loadingDetails;
    }

    /**
     * Sets the value of the loadingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadingDetails }
     *     
     */
    public void setLoadingDetails(LoadingDetails value) {
        this.loadingDetails = value;
    }

}
