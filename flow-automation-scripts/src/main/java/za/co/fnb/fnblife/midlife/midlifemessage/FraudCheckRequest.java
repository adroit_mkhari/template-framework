
package za.co.fnb.fnblife.midlife.midlifemessage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fraudCheckRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fraudCheckRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fraudDetails" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}fraudDetails"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fraudCheckRequest", propOrder = {
    "fraudDetails"
})
public class FraudCheckRequest {

    @XmlElement(required = true)
    protected FraudDetails fraudDetails;

    /**
     * Gets the value of the fraudDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FraudDetails }
     *     
     */
    public FraudDetails getFraudDetails() {
        return fraudDetails;
    }

    /**
     * Sets the value of the fraudDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FraudDetails }
     *     
     */
    public void setFraudDetails(FraudDetails value) {
        this.fraudDetails = value;
    }

}
