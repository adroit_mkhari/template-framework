
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverOutcomeDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverOutcomeDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="InternalCovers" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryResponse}InternalCovers" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverOutcomeDetail", propOrder = {
    "internalCovers"
})
public class CoverOutcomeDetail {

    @XmlElement(name = "InternalCovers", required = true)
    protected List<InternalCovers> internalCovers;

    /**
     * Gets the value of the internalCovers property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the internalCovers property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInternalCovers().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InternalCovers }
     * 
     * 
     */
    public List<InternalCovers> getInternalCovers() {
        if (internalCovers == null) {
            internalCovers = new ArrayList<InternalCovers>();
        }
        return this.internalCovers;
    }

}
