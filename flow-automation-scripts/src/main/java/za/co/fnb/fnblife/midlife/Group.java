
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subChannel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="leadsno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="coveramount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="inceptdate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="paydate" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="bankname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="branchCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="accountType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="staffCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="staffIntendCount" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="policyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="salePerson" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="refNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subChannel",
    "leadsno",
    "companyname",
    "coveramount",
    "inceptdate",
    "paydate",
    "bankname",
    "branchCode",
    "accountNumber",
    "accountType",
    "staffCount",
    "staffIntendCount",
    "policyNo",
    "salePerson",
    "refNumber"
})
@XmlRootElement(name = "group")
public class Group {

    @XmlElement(required = true)
    protected String subChannel;
    @XmlElement(required = true)
    protected String leadsno;
    @XmlElement(required = true)
    protected String companyname;
    @XmlElement(required = true)
    protected String coveramount;
    @XmlElement(required = true)
    protected String inceptdate;
    @XmlElement(required = true)
    protected String paydate;
    @XmlElement(required = true)
    protected String bankname;
    @XmlElement(required = true)
    protected String branchCode;
    @XmlElement(required = true)
    protected String accountNumber;
    @XmlElement(required = true)
    protected String accountType;
    @XmlElement(required = true)
    protected String staffCount;
    @XmlElement(required = true)
    protected String staffIntendCount;
    protected String policyNo;
    protected String salePerson;
    protected String refNumber;

    /**
     * Gets the value of the subChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubChannel() {
        return subChannel;
    }

    /**
     * Sets the value of the subChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubChannel(String value) {
        this.subChannel = value;
    }

    /**
     * Gets the value of the leadsno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLeadsno() {
        return leadsno;
    }

    /**
     * Sets the value of the leadsno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLeadsno(String value) {
        this.leadsno = value;
    }

    /**
     * Gets the value of the companyname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * Sets the value of the companyname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyname(String value) {
        this.companyname = value;
    }

    /**
     * Gets the value of the coveramount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCoveramount() {
        return coveramount;
    }

    /**
     * Sets the value of the coveramount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCoveramount(String value) {
        this.coveramount = value;
    }

    /**
     * Gets the value of the inceptdate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInceptdate() {
        return inceptdate;
    }

    /**
     * Sets the value of the inceptdate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInceptdate(String value) {
        this.inceptdate = value;
    }

    /**
     * Gets the value of the paydate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaydate() {
        return paydate;
    }

    /**
     * Sets the value of the paydate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaydate(String value) {
        this.paydate = value;
    }

    /**
     * Gets the value of the bankname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankname() {
        return bankname;
    }

    /**
     * Sets the value of the bankname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankname(String value) {
        this.bankname = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the staffCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStaffCount() {
        return staffCount;
    }

    /**
     * Sets the value of the staffCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStaffCount(String value) {
        this.staffCount = value;
    }

    /**
     * Gets the value of the staffIntendCount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStaffIntendCount() {
        return staffIntendCount;
    }

    /**
     * Sets the value of the staffIntendCount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStaffIntendCount(String value) {
        this.staffIntendCount = value;
    }

    /**
     * Gets the value of the policyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNo() {
        return policyNo;
    }

    /**
     * Sets the value of the policyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNo(String value) {
        this.policyNo = value;
    }

    /**
     * Gets the value of the salePerson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSalePerson() {
        return salePerson;
    }

    /**
     * Sets the value of the salePerson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSalePerson(String value) {
        this.salePerson = value;
    }

    /**
     * Gets the value of the refNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRefNumber() {
        return refNumber;
    }

    /**
     * Sets the value of the refNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRefNumber(String value) {
        this.refNumber = value;
    }

}
