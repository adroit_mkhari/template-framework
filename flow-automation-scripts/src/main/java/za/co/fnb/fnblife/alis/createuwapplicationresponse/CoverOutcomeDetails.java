
package za.co.fnb.fnblife.alis.createuwapplicationresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CoverOutcomeDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverOutcomeDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoverNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="CoverOutcomeDetail" type="{http://alis.fnblife.fnb.co.za/createUWApplicationResponse}CoverOutcomeDetail"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverOutcomeDetails", propOrder = {
    "coverNo",
    "coverOutcomeDetail"
})
public class CoverOutcomeDetails {

    @XmlElement(name = "CoverNo", required = true)
    protected BigInteger coverNo;
    @XmlElement(name = "CoverOutcomeDetail", required = true)
    protected CoverOutcomeDetail coverOutcomeDetail;

    /**
     * Gets the value of the coverNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCoverNo() {
        return coverNo;
    }

    /**
     * Sets the value of the coverNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCoverNo(BigInteger value) {
        this.coverNo = value;
    }

    /**
     * Gets the value of the coverOutcomeDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CoverOutcomeDetail }
     *     
     */
    public CoverOutcomeDetail getCoverOutcomeDetail() {
        return coverOutcomeDetail;
    }

    /**
     * Sets the value of the coverOutcomeDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverOutcomeDetail }
     *     
     */
    public void setCoverOutcomeDetail(CoverOutcomeDetail value) {
        this.coverOutcomeDetail = value;
    }

}
