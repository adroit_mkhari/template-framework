
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.midlife package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AcMandateManagementDTO_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "acMandateManagementDTO");
    private final static QName _AcMandateRegisterDTO_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "acMandateRegisterDTO");
    private final static QName _AcMandateResponseDTO_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "acMandateResponseDTO");
    private final static QName _AccountDetails_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "accountDetails");
    private final static QName _Customer_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "customer");
    private final static QName _DoPing_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "doPing");
    private final static QName _DoPingResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "doPingResponse");
    private final static QName _ProductData_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "productData");
    private final static QName _Response_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "response");
    private final static QName _RetrieveRealTimeAccountBalanceResponseType_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "retrieveRealTimeAccountBalanceResponseType");
    private final static QName _SendAsyncRequest_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendAsyncRequest");
    private final static QName _SendAsyncRequestResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendAsyncRequestResponse");
    private final static QName _SendAsyncRequestWithAck_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendAsyncRequestWithAck");
    private final static QName _SendAsyncRequestWithAckResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendAsyncRequestWithAckResponse");
    private final static QName _SendSyncRequest_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendSyncRequest");
    private final static QName _SendSyncRequestResponse_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "sendSyncRequestResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.midlife
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Scheme }
     * 
     */
    public Scheme createScheme() {
        return new Scheme();
    }

    /**
     * Create an instance of {@link LookupMandateDetailsRequest }
     * 
     */
    public LookupMandateDetailsRequest createLookupMandateDetailsRequest() {
        return new LookupMandateDetailsRequest();
    }

    /**
     * Create an instance of {@link LookupMandateDetailsResponse }
     * 
     */
    public LookupMandateDetailsResponse createLookupMandateDetailsResponse() {
        return new LookupMandateDetailsResponse();
    }

    /**
     * Create an instance of {@link LookupMandateDetailsRequestType }
     * 
     */
    public LookupMandateDetailsRequestType createLookupMandateDetailsRequestType() {
        return new LookupMandateDetailsRequestType();
    }

    /**
     * Create an instance of {@link LookupMandateDetailsResponseType }
     * 
     */
    public LookupMandateDetailsResponseType createLookupMandateDetailsResponseType() {
        return new LookupMandateDetailsResponseType();
    }

    /**
     * Create an instance of {@link MandateDetailsRequest }
     * 
     */
    public MandateDetailsRequest createMandateDetailsRequest() {
        return new MandateDetailsRequest();
    }

    /**
     * Create an instance of {@link MandateDetailsResponse }
     * 
     */
    public MandateDetailsResponse createMandateDetailsResponse() {
        return new MandateDetailsResponse();
    }

    /**
     * Create an instance of {@link AcMandateManagementDTO }
     * 
     */
    public AcMandateManagementDTO createAcMandateManagementDTO() {
        return new AcMandateManagementDTO();
    }

    /**
     * Create an instance of {@link AcMandateRegisterDTO }
     * 
     */
    public AcMandateRegisterDTO createAcMandateRegisterDTO() {
        return new AcMandateRegisterDTO();
    }

    /**
     * Create an instance of {@link AcMandateResponseDTO }
     * 
     */
    public AcMandateResponseDTO createAcMandateResponseDTO() {
        return new AcMandateResponseDTO();
    }

    /**
     * Create an instance of {@link AccountDetails }
     * 
     */
    public AccountDetails createAccountDetails() {
        return new AccountDetails();
    }

    /**
     * Create an instance of {@link DoPing }
     * 
     */
    public DoPing createDoPing() {
        return new DoPing();
    }

    /**
     * Create an instance of {@link DoPingResponse }
     * 
     */
    public DoPingResponse createDoPingResponse() {
        return new DoPingResponse();
    }

    /**
     * Create an instance of {@link Group }
     * 
     */
    public Group createGroup() {
        return new Group();
    }

    /**
     * Create an instance of {@link ProductTypes }
     * 
     */
    public ProductTypes createProductTypes() {
        return new ProductTypes();
    }

    /**
     * Create an instance of {@link RetrieveRealTimeAccountBalanceResponseType }
     * 
     */
    public RetrieveRealTimeAccountBalanceResponseType createRetrieveRealTimeAccountBalanceResponseType() {
        return new RetrieveRealTimeAccountBalanceResponseType();
    }

    /**
     * Create an instance of {@link Scheme.Address }
     * 
     */
    public Scheme.Address createSchemeAddress() {
        return new Scheme.Address();
    }

    /**
     * Create an instance of {@link SendAsyncRequest }
     * 
     */
    public SendAsyncRequest createSendAsyncRequest() {
        return new SendAsyncRequest();
    }

    /**
     * Create an instance of {@link SendAsyncRequestResponse }
     * 
     */
    public SendAsyncRequestResponse createSendAsyncRequestResponse() {
        return new SendAsyncRequestResponse();
    }

    /**
     * Create an instance of {@link SendAsyncRequestWithAck }
     * 
     */
    public SendAsyncRequestWithAck createSendAsyncRequestWithAck() {
        return new SendAsyncRequestWithAck();
    }

    /**
     * Create an instance of {@link SendAsyncRequestWithAckResponse }
     * 
     */
    public SendAsyncRequestWithAckResponse createSendAsyncRequestWithAckResponse() {
        return new SendAsyncRequestWithAckResponse();
    }

    /**
     * Create an instance of {@link SendSyncRequest }
     * 
     */
    public SendSyncRequest createSendSyncRequest() {
        return new SendSyncRequest();
    }

    /**
     * Create an instance of {@link SendSyncRequestResponse }
     * 
     */
    public SendSyncRequestResponse createSendSyncRequestResponse() {
        return new SendSyncRequestResponse();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link Error }
     * 
     */
    public Error createError() {
        return new Error();
    }

    /**
     * Create an instance of {@link Throwable }
     * 
     */
    public Throwable createThrowable() {
        return new Throwable();
    }

    /**
     * Create an instance of {@link StackTraceElement }
     * 
     */
    public StackTraceElement createStackTraceElement() {
        return new StackTraceElement();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcMandateManagementDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AcMandateManagementDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "acMandateManagementDTO")
    public JAXBElement<AcMandateManagementDTO> createAcMandateManagementDTO(AcMandateManagementDTO value) {
        return new JAXBElement<AcMandateManagementDTO>(_AcMandateManagementDTO_QNAME, AcMandateManagementDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcMandateRegisterDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AcMandateRegisterDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "acMandateRegisterDTO")
    public JAXBElement<AcMandateRegisterDTO> createAcMandateRegisterDTO(AcMandateRegisterDTO value) {
        return new JAXBElement<AcMandateRegisterDTO>(_AcMandateRegisterDTO_QNAME, AcMandateRegisterDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AcMandateResponseDTO }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AcMandateResponseDTO }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "acMandateResponseDTO")
    public JAXBElement<AcMandateResponseDTO> createAcMandateResponseDTO(AcMandateResponseDTO value) {
        return new JAXBElement<AcMandateResponseDTO>(_AcMandateResponseDTO_QNAME, AcMandateResponseDTO.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountDetails }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AccountDetails }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "accountDetails")
    public JAXBElement<AccountDetails> createAccountDetails(AccountDetails value) {
        return new JAXBElement<AccountDetails>(_AccountDetails_QNAME, AccountDetails.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "customer")
    public JAXBElement<Object> createCustomer(Object value) {
        return new JAXBElement<Object>(_Customer_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPing }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DoPing }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "doPing")
    public JAXBElement<DoPing> createDoPing(DoPing value) {
        return new JAXBElement<DoPing>(_DoPing_QNAME, DoPing.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoPingResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DoPingResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "doPingResponse")
    public JAXBElement<DoPingResponse> createDoPingResponse(DoPingResponse value) {
        return new JAXBElement<DoPingResponse>(_DoPingResponse_QNAME, DoPingResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "productData")
    public JAXBElement<Object> createProductData(Object value) {
        return new JAXBElement<Object>(_ProductData_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "response")
    public JAXBElement<Object> createResponse(Object value) {
        return new JAXBElement<Object>(_Response_QNAME, Object.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrieveRealTimeAccountBalanceResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrieveRealTimeAccountBalanceResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "retrieveRealTimeAccountBalanceResponseType")
    public JAXBElement<RetrieveRealTimeAccountBalanceResponseType> createRetrieveRealTimeAccountBalanceResponseType(RetrieveRealTimeAccountBalanceResponseType value) {
        return new JAXBElement<RetrieveRealTimeAccountBalanceResponseType>(_RetrieveRealTimeAccountBalanceResponseType_QNAME, RetrieveRealTimeAccountBalanceResponseType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAsyncRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendAsyncRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendAsyncRequest")
    public JAXBElement<SendAsyncRequest> createSendAsyncRequest(SendAsyncRequest value) {
        return new JAXBElement<SendAsyncRequest>(_SendAsyncRequest_QNAME, SendAsyncRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendAsyncRequestResponse")
    public JAXBElement<SendAsyncRequestResponse> createSendAsyncRequestResponse(SendAsyncRequestResponse value) {
        return new JAXBElement<SendAsyncRequestResponse>(_SendAsyncRequestResponse_QNAME, SendAsyncRequestResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestWithAck }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestWithAck }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendAsyncRequestWithAck")
    public JAXBElement<SendAsyncRequestWithAck> createSendAsyncRequestWithAck(SendAsyncRequestWithAck value) {
        return new JAXBElement<SendAsyncRequestWithAck>(_SendAsyncRequestWithAck_QNAME, SendAsyncRequestWithAck.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestWithAckResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendAsyncRequestWithAckResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendAsyncRequestWithAckResponse")
    public JAXBElement<SendAsyncRequestWithAckResponse> createSendAsyncRequestWithAckResponse(SendAsyncRequestWithAckResponse value) {
        return new JAXBElement<SendAsyncRequestWithAckResponse>(_SendAsyncRequestWithAckResponse_QNAME, SendAsyncRequestWithAckResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendSyncRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendSyncRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendSyncRequest")
    public JAXBElement<SendSyncRequest> createSendSyncRequest(SendSyncRequest value) {
        return new JAXBElement<SendSyncRequest>(_SendSyncRequest_QNAME, SendSyncRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendSyncRequestResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendSyncRequestResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.fnb.co.za", name = "sendSyncRequestResponse")
    public JAXBElement<SendSyncRequestResponse> createSendSyncRequestResponse(SendSyncRequestResponse value) {
        return new JAXBElement<SendSyncRequestResponse>(_SendSyncRequestResponse_QNAME, SendSyncRequestResponse.class, null, value);
    }

}
