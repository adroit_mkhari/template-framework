
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PolicyDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PolicyDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Source" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="RequestChannel" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="QuoteEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="ExternalPolicyNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PolicyDetails", propOrder = {
    "source",
    "requestChannel",
    "quoteEffectiveDate",
    "externalPolicyNo"
})
public class PolicyDetails {

    @XmlElement(name = "Source", required = true)
    protected BigInteger source;
    @XmlElement(name = "RequestChannel", required = true)
    protected BigInteger requestChannel;
    @XmlElement(name = "QuoteEffectiveDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar quoteEffectiveDate;
    @XmlElement(name = "ExternalPolicyNo")
    protected String externalPolicyNo;

    /**
     * Gets the value of the source property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSource() {
        return source;
    }

    /**
     * Sets the value of the source property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSource(BigInteger value) {
        this.source = value;
    }

    /**
     * Gets the value of the requestChannel property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getRequestChannel() {
        return requestChannel;
    }

    /**
     * Sets the value of the requestChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setRequestChannel(BigInteger value) {
        this.requestChannel = value;
    }

    /**
     * Gets the value of the quoteEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getQuoteEffectiveDate() {
        return quoteEffectiveDate;
    }

    /**
     * Sets the value of the quoteEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setQuoteEffectiveDate(XMLGregorianCalendar value) {
        this.quoteEffectiveDate = value;
    }

    /**
     * Gets the value of the externalPolicyNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalPolicyNo() {
        return externalPolicyNo;
    }

    /**
     * Sets the value of the externalPolicyNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalPolicyNo(String value) {
        this.externalPolicyNo = value;
    }

}
