
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnb.fnblife.midlife.midlifemessage.MidLifeMessage;


/**
 * <p>Java class for doPingResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doPingResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}midLifeMessage" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doPingResponse", propOrder = {
    "midLifeMessage"
})
public class DoPingResponse {

    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected MidLifeMessage midLifeMessage;

    /**
     * Gets the value of the midLifeMessage property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeMessage }
     *     
     */
    public MidLifeMessage getMidLifeMessage() {
        return midLifeMessage;
    }

    /**
     * Sets the value of the midLifeMessage property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeMessage }
     *     
     */
    public void setMidLifeMessage(MidLifeMessage value) {
        this.midLifeMessage = value;
    }

}
