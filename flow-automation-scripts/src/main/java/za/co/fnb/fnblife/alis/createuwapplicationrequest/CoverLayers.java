
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CoverLayers complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CoverLayers"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoverLayerNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="New" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="RequestedSumAssured" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="EffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="OccupationalClass" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="WaitingPeriod" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="SEC" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CoverLayers", propOrder = {
    "coverLayerNo",
    "_new",
    "requestedSumAssured",
    "effectiveDate",
    "occupationalClass",
    "waitingPeriod",
    "sec"
})
public class CoverLayers {

    @XmlElement(name = "CoverLayerNo", required = true)
    protected BigInteger coverLayerNo;
    @XmlElement(name = "New", required = true)
    protected BigInteger _new;
    @XmlElement(name = "RequestedSumAssured")
    protected BigDecimal requestedSumAssured;
    @XmlElement(name = "EffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar effectiveDate;
    @XmlElement(name = "OccupationalClass")
    protected BigInteger occupationalClass;
    @XmlElement(name = "WaitingPeriod")
    protected BigInteger waitingPeriod;
    @XmlElement(name = "SEC")
    protected BigInteger sec;

    /**
     * Gets the value of the coverLayerNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCoverLayerNo() {
        return coverLayerNo;
    }

    /**
     * Sets the value of the coverLayerNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCoverLayerNo(BigInteger value) {
        this.coverLayerNo = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNew(BigInteger value) {
        this._new = value;
    }

    /**
     * Gets the value of the requestedSumAssured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getRequestedSumAssured() {
        return requestedSumAssured;
    }

    /**
     * Sets the value of the requestedSumAssured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setRequestedSumAssured(BigDecimal value) {
        this.requestedSumAssured = value;
    }

    /**
     * Gets the value of the effectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getEffectiveDate() {
        return effectiveDate;
    }

    /**
     * Sets the value of the effectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setEffectiveDate(XMLGregorianCalendar value) {
        this.effectiveDate = value;
    }

    /**
     * Gets the value of the occupationalClass property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getOccupationalClass() {
        return occupationalClass;
    }

    /**
     * Sets the value of the occupationalClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setOccupationalClass(BigInteger value) {
        this.occupationalClass = value;
    }

    /**
     * Gets the value of the waitingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getWaitingPeriod() {
        return waitingPeriod;
    }

    /**
     * Sets the value of the waitingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setWaitingPeriod(BigInteger value) {
        this.waitingPeriod = value;
    }

    /**
     * Gets the value of the sec property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSEC() {
        return sec;
    }

    /**
     * Sets the value of the sec property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSEC(BigInteger value) {
        this.sec = value;
    }

}
