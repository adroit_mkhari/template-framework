
package za.co.fnb.fnblife.midlife.midlifemessage;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeRequestType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeResponseType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://midlife.fnblife.fnb.co.za/MidLife_Types}MidLifeHeaderType"/&gt;
 *         &lt;element name="request" type="{http://midlife.fnblife.fnb.co.za/MidLife_Types}MidLifeRequestType" minOccurs="0"/&gt;
 *         &lt;element name="response" type="{http://midlife.fnblife.fnb.co.za/MidLife_Types}MidLifeResponseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "request",
    "response"
})
@XmlRootElement(name = "midLifeMessage")
public class MidLifeMessage {

    @XmlElement(required = true)
    protected MidLifeHeaderType header;
    protected MidLifeRequestType request;
    protected MidLifeResponseType response;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public MidLifeHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public void setHeader(MidLifeHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the request property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeRequestType }
     *     
     */
    public MidLifeRequestType getRequest() {
        return request;
    }

    /**
     * Sets the value of the request property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeRequestType }
     *     
     */
    public void setRequest(MidLifeRequestType value) {
        this.request = value;
    }

    /**
     * Gets the value of the response property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeResponseType }
     *     
     */
    public MidLifeResponseType getResponse() {
        return response;
    }

    /**
     * Sets the value of the response property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeResponseType }
     *     
     */
    public void setResponse(MidLifeResponseType value) {
        this.response = value;
    }

}
