
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CreateUWApplicationRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateUWApplicationRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ExternalClientReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="NationalInsuranceRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="PolicyDetails" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}PolicyDetails"/&gt;
 *         &lt;element name="ProductDetails" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}ProductDetails"/&gt;
 *         &lt;element name="PersonalDetails" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}PersonalDetails"/&gt;
 *         &lt;element name="Covers" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}Covers"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateUWApplicationRequestType", propOrder = {
    "externalClientReference",
    "nationalInsuranceRef",
    "policyDetails",
    "productDetails",
    "personalDetails",
    "covers"
})
public class CreateUWApplicationRequestType {

    @XmlElement(name = "ExternalClientReference")
    protected String externalClientReference;
    @XmlElement(name = "NationalInsuranceRef")
    protected String nationalInsuranceRef;
    @XmlElement(name = "PolicyDetails", required = true)
    protected PolicyDetails policyDetails;
    @XmlElement(name = "ProductDetails", required = true)
    protected ProductDetails productDetails;
    @XmlElement(name = "PersonalDetails", required = true)
    protected PersonalDetails personalDetails;
    @XmlElement(name = "Covers", required = true)
    protected Covers covers;

    /**
     * Gets the value of the externalClientReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getExternalClientReference() {
        return externalClientReference;
    }

    /**
     * Sets the value of the externalClientReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setExternalClientReference(String value) {
        this.externalClientReference = value;
    }

    /**
     * Gets the value of the nationalInsuranceRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationalInsuranceRef() {
        return nationalInsuranceRef;
    }

    /**
     * Sets the value of the nationalInsuranceRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationalInsuranceRef(String value) {
        this.nationalInsuranceRef = value;
    }

    /**
     * Gets the value of the policyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetails }
     *     
     */
    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    /**
     * Sets the value of the policyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetails }
     *     
     */
    public void setPolicyDetails(PolicyDetails value) {
        this.policyDetails = value;
    }

    /**
     * Gets the value of the productDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ProductDetails }
     *     
     */
    public ProductDetails getProductDetails() {
        return productDetails;
    }

    /**
     * Sets the value of the productDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProductDetails }
     *     
     */
    public void setProductDetails(ProductDetails value) {
        this.productDetails = value;
    }

    /**
     * Gets the value of the personalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PersonalDetails }
     *     
     */
    public PersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    /**
     * Sets the value of the personalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PersonalDetails }
     *     
     */
    public void setPersonalDetails(PersonalDetails value) {
        this.personalDetails = value;
    }

    /**
     * Gets the value of the covers property.
     * 
     * @return
     *     possible object is
     *     {@link Covers }
     *     
     */
    public Covers getCovers() {
        return covers;
    }

    /**
     * Sets the value of the covers property.
     * 
     * @param value
     *     allowed object is
     *     {@link Covers }
     *     
     */
    public void setCovers(Covers value) {
        this.covers = value;
    }

}
