
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for LoadingDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="LoadingDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="LoadingID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="LoadingMethod" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="LoadingValue" type="{http://www.w3.org/2001/XMLSchema}decimal"/&gt;
 *         &lt;element name="LoadingPeriod" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "LoadingDetail", propOrder = {
    "loadingID",
    "loadingMethod",
    "loadingValue",
    "loadingPeriod"
})
public class LoadingDetail {

    @XmlElement(name = "LoadingID", required = true)
    protected BigInteger loadingID;
    @XmlElement(name = "LoadingMethod", required = true)
    protected BigInteger loadingMethod;
    @XmlElement(name = "LoadingValue", required = true)
    protected BigDecimal loadingValue;
    @XmlElement(name = "LoadingPeriod")
    protected BigInteger loadingPeriod;

    /**
     * Gets the value of the loadingID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLoadingID() {
        return loadingID;
    }

    /**
     * Sets the value of the loadingID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLoadingID(BigInteger value) {
        this.loadingID = value;
    }

    /**
     * Gets the value of the loadingMethod property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLoadingMethod() {
        return loadingMethod;
    }

    /**
     * Sets the value of the loadingMethod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLoadingMethod(BigInteger value) {
        this.loadingMethod = value;
    }

    /**
     * Gets the value of the loadingValue property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getLoadingValue() {
        return loadingValue;
    }

    /**
     * Sets the value of the loadingValue property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setLoadingValue(BigDecimal value) {
        this.loadingValue = value;
    }

    /**
     * Gets the value of the loadingPeriod property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getLoadingPeriod() {
        return loadingPeriod;
    }

    /**
     * Sets the value of the loadingPeriod property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setLoadingPeriod(BigInteger value) {
        this.loadingPeriod = value;
    }

}
