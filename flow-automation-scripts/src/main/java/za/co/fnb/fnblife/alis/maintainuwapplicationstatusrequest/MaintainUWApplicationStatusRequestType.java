
package za.co.fnb.fnblife.alis.maintainuwapplicationstatusrequest;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaintainUWApplicationStatusRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MaintainUWApplicationStatusRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="NewApplicationStatus" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainUWApplicationStatusRequestType", propOrder = {
    "uwApplicationID",
    "newApplicationStatus"
})
public class MaintainUWApplicationStatusRequestType {

    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "NewApplicationStatus", required = true)
    protected BigInteger newApplicationStatus;

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the newApplicationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNewApplicationStatus() {
        return newApplicationStatus;
    }

    /**
     * Sets the value of the newApplicationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNewApplicationStatus(BigInteger value) {
        this.newApplicationStatus = value;
    }

}
