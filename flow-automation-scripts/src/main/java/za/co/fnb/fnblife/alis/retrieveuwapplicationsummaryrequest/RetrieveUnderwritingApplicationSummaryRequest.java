
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="Header" type="{http://midlife.fnblife.fnb.co.za/MidLife_Types}MidLifeHeaderType" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="DataArea" type="{http://alis.fnblife.fnb.co.za/retrieveUWApplicationSummaryRequest}RetrieveUnderwritingApplicationSummaryRequestType" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "header",
    "dataArea"
})
@XmlRootElement(name = "RetrieveUnderwritingApplicationSummaryRequest")
public class RetrieveUnderwritingApplicationSummaryRequest {

    @XmlElement(name = "Header")
    protected MidLifeHeaderType header;
    @XmlElement(name = "DataArea")
    protected RetrieveUnderwritingApplicationSummaryRequestType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public MidLifeHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public void setHeader(MidLifeHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveUnderwritingApplicationSummaryRequestType }
     *     
     */
    public RetrieveUnderwritingApplicationSummaryRequestType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveUnderwritingApplicationSummaryRequestType }
     *     
     */
    public void setDataArea(RetrieveUnderwritingApplicationSummaryRequestType value) {
        this.dataArea = value;
    }

}
