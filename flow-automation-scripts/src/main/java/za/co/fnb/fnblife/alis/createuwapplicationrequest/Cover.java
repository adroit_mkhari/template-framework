
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Cover complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Cover"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="CoverNo" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="UnderWritingGroup" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="CoverCommencementDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="TotalExistingSumAssured" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="TotalRequestedSumAssured" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="New" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="CoverDetail" type="{http://alis.fnblife.fnb.co.za/createUWApplicationRequest}CoverDetail" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Cover", propOrder = {
    "coverNo",
    "underWritingGroup",
    "coverCommencementDate",
    "totalExistingSumAssured",
    "totalRequestedSumAssured",
    "_new",
    "coverDetail"
})
public class Cover {

    @XmlElement(name = "CoverNo", required = true)
    protected BigInteger coverNo;
    @XmlElement(name = "UnderWritingGroup")
    protected BigInteger underWritingGroup;
    @XmlElement(name = "CoverCommencementDate", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar coverCommencementDate;
    @XmlElement(name = "TotalExistingSumAssured")
    protected BigDecimal totalExistingSumAssured;
    @XmlElement(name = "TotalRequestedSumAssured")
    protected BigDecimal totalRequestedSumAssured;
    @XmlElement(name = "New")
    protected BigInteger _new;
    @XmlElement(name = "CoverDetail")
    protected CoverDetail coverDetail;

    /**
     * Gets the value of the coverNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCoverNo() {
        return coverNo;
    }

    /**
     * Sets the value of the coverNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCoverNo(BigInteger value) {
        this.coverNo = value;
    }

    /**
     * Gets the value of the underWritingGroup property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUnderWritingGroup() {
        return underWritingGroup;
    }

    /**
     * Sets the value of the underWritingGroup property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUnderWritingGroup(BigInteger value) {
        this.underWritingGroup = value;
    }

    /**
     * Gets the value of the coverCommencementDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCoverCommencementDate() {
        return coverCommencementDate;
    }

    /**
     * Sets the value of the coverCommencementDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCoverCommencementDate(XMLGregorianCalendar value) {
        this.coverCommencementDate = value;
    }

    /**
     * Gets the value of the totalExistingSumAssured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalExistingSumAssured() {
        return totalExistingSumAssured;
    }

    /**
     * Sets the value of the totalExistingSumAssured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalExistingSumAssured(BigDecimal value) {
        this.totalExistingSumAssured = value;
    }

    /**
     * Gets the value of the totalRequestedSumAssured property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getTotalRequestedSumAssured() {
        return totalRequestedSumAssured;
    }

    /**
     * Sets the value of the totalRequestedSumAssured property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setTotalRequestedSumAssured(BigDecimal value) {
        this.totalRequestedSumAssured = value;
    }

    /**
     * Gets the value of the new property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getNew() {
        return _new;
    }

    /**
     * Sets the value of the new property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setNew(BigInteger value) {
        this._new = value;
    }

    /**
     * Gets the value of the coverDetail property.
     * 
     * @return
     *     possible object is
     *     {@link CoverDetail }
     *     
     */
    public CoverDetail getCoverDetail() {
        return coverDetail;
    }

    /**
     * Sets the value of the coverDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link CoverDetail }
     *     
     */
    public void setCoverDetail(CoverDetail value) {
        this.coverDetail = value;
    }

}
