
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryresponse
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrieveUnderwritingApplicationSummaryResponse }
     * 
     */
    public RetrieveUnderwritingApplicationSummaryResponse createRetrieveUnderwritingApplicationSummaryResponse() {
        return new RetrieveUnderwritingApplicationSummaryResponse();
    }

    /**
     * Create an instance of {@link RetrieveUnderwritingApplicationSummaryResponseType }
     * 
     */
    public RetrieveUnderwritingApplicationSummaryResponseType createRetrieveUnderwritingApplicationSummaryResponseType() {
        return new RetrieveUnderwritingApplicationSummaryResponseType();
    }

    /**
     * Create an instance of {@link Answers }
     * 
     */
    public Answers createAnswers() {
        return new Answers();
    }

    /**
     * Create an instance of {@link Answer }
     * 
     */
    public Answer createAnswer() {
        return new Answer();
    }

    /**
     * Create an instance of {@link MedicalEvidenceDetail }
     * 
     */
    public MedicalEvidenceDetail createMedicalEvidenceDetail() {
        return new MedicalEvidenceDetail();
    }

    /**
     * Create an instance of {@link ExclusionDetail }
     * 
     */
    public ExclusionDetail createExclusionDetail() {
        return new ExclusionDetail();
    }

    /**
     * Create an instance of {@link PolicyDetails }
     * 
     */
    public PolicyDetails createPolicyDetails() {
        return new PolicyDetails();
    }

    /**
     * Create an instance of {@link ProductDetails }
     * 
     */
    public ProductDetails createProductDetails() {
        return new ProductDetails();
    }

    /**
     * Create an instance of {@link PersonalDetails }
     * 
     */
    public PersonalDetails createPersonalDetails() {
        return new PersonalDetails();
    }

    /**
     * Create an instance of {@link AnswerSets }
     * 
     */
    public AnswerSets createAnswerSets() {
        return new AnswerSets();
    }

    /**
     * Create an instance of {@link AnswerSet }
     * 
     */
    public AnswerSet createAnswerSet() {
        return new AnswerSet();
    }

    /**
     * Create an instance of {@link OutcomeDetails }
     * 
     */
    public OutcomeDetails createOutcomeDetails() {
        return new OutcomeDetails();
    }

    /**
     * Create an instance of {@link PolicyOutcome }
     * 
     */
    public PolicyOutcome createPolicyOutcome() {
        return new PolicyOutcome();
    }

    /**
     * Create an instance of {@link CoverOutcomeDetails }
     * 
     */
    public CoverOutcomeDetails createCoverOutcomeDetails() {
        return new CoverOutcomeDetails();
    }

    /**
     * Create an instance of {@link CoverOutcomeDetail }
     * 
     */
    public CoverOutcomeDetail createCoverOutcomeDetail() {
        return new CoverOutcomeDetail();
    }

    /**
     * Create an instance of {@link InternalCovers }
     * 
     */
    public InternalCovers createInternalCovers() {
        return new InternalCovers();
    }

    /**
     * Create an instance of {@link CoverLayers }
     * 
     */
    public CoverLayers createCoverLayers() {
        return new CoverLayers();
    }

    /**
     * Create an instance of {@link LoadingDetails }
     * 
     */
    public LoadingDetails createLoadingDetails() {
        return new LoadingDetails();
    }

    /**
     * Create an instance of {@link LoadingDetail }
     * 
     */
    public LoadingDetail createLoadingDetail() {
        return new LoadingDetail();
    }

}
