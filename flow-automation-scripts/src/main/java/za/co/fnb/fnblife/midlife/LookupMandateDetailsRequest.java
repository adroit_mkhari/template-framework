
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;


/**
 * <p>Java class for lookupMandateDetailsRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="lookupMandateDetailsRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}header" minOccurs="0"/&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}lookupMandateDetailsRequestType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lookupMandateDetailsRequest", propOrder = {
    "header",
    "lookupMandateDetailsRequestType"
})
public class LookupMandateDetailsRequest {

    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected MidLifeHeaderType header;
    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage")
    protected LookupMandateDetailsRequestType lookupMandateDetailsRequestType;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public MidLifeHeaderType getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link MidLifeHeaderType }
     *     
     */
    public void setHeader(MidLifeHeaderType value) {
        this.header = value;
    }

    /**
     * Gets the value of the lookupMandateDetailsRequestType property.
     * 
     * @return
     *     possible object is
     *     {@link LookupMandateDetailsRequestType }
     *     
     */
    public LookupMandateDetailsRequestType getLookupMandateDetailsRequestType() {
        return lookupMandateDetailsRequestType;
    }

    /**
     * Sets the value of the lookupMandateDetailsRequestType property.
     * 
     * @param value
     *     allowed object is
     *     {@link LookupMandateDetailsRequestType }
     *     
     */
    public void setLookupMandateDetailsRequestType(LookupMandateDetailsRequestType value) {
        this.lookupMandateDetailsRequestType = value;
    }

}
