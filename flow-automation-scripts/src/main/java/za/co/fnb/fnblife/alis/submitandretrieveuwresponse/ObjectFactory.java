
package za.co.fnb.fnblife.alis.submitandretrieveuwresponse;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.alis.submitandretrieveuwresponse package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.alis.submitandretrieveuwresponse
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitAndRetrieveUWResponse }
     * 
     */
    public SubmitAndRetrieveUWResponse createSubmitAndRetrieveUWResponse() {
        return new SubmitAndRetrieveUWResponse();
    }

    /**
     * Create an instance of {@link SubmitAndRetrieveUWResponseType }
     * 
     */
    public SubmitAndRetrieveUWResponseType createSubmitAndRetrieveUWResponseType() {
        return new SubmitAndRetrieveUWResponseType();
    }

    /**
     * Create an instance of {@link Answers }
     * 
     */
    public Answers createAnswers() {
        return new Answers();
    }

    /**
     * Create an instance of {@link AnswerOptions }
     * 
     */
    public AnswerOptions createAnswerOptions() {
        return new AnswerOptions();
    }

    /**
     * Create an instance of {@link QuestionSet }
     * 
     */
    public QuestionSet createQuestionSet() {
        return new QuestionSet();
    }

    /**
     * Create an instance of {@link OutcomeDetails }
     * 
     */
    public OutcomeDetails createOutcomeDetails() {
        return new OutcomeDetails();
    }

    /**
     * Create an instance of {@link PolicyOutcome }
     * 
     */
    public PolicyOutcome createPolicyOutcome() {
        return new PolicyOutcome();
    }

    /**
     * Create an instance of {@link CoverOutcomeDetails }
     * 
     */
    public CoverOutcomeDetails createCoverOutcomeDetails() {
        return new CoverOutcomeDetails();
    }

    /**
     * Create an instance of {@link CoverOutcomeDetail }
     * 
     */
    public CoverOutcomeDetail createCoverOutcomeDetail() {
        return new CoverOutcomeDetail();
    }

    /**
     * Create an instance of {@link InternalCovers }
     * 
     */
    public InternalCovers createInternalCovers() {
        return new InternalCovers();
    }

    /**
     * Create an instance of {@link CoverLayers }
     * 
     */
    public CoverLayers createCoverLayers() {
        return new CoverLayers();
    }

    /**
     * Create an instance of {@link LoadingDetails }
     * 
     */
    public LoadingDetails createLoadingDetails() {
        return new LoadingDetails();
    }

    /**
     * Create an instance of {@link LoadingDetail }
     * 
     */
    public LoadingDetail createLoadingDetail() {
        return new LoadingDetail();
    }

}
