
package za.co.fnb.fnblife.alis.createuwapplicationresponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for CreateUWApplicationResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CreateUWApplicationResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ApplicationStartTime" type="{http://www.w3.org/2001/XMLSchema}dateTime"/&gt;
 *         &lt;element name="EndOfTreeIndicator" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="QuestionSet" type="{http://alis.fnblife.fnb.co.za/createUWApplicationResponse}QuestionSet" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OutcomeDetails" type="{http://alis.fnblife.fnb.co.za/createUWApplicationResponse}OutcomeDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CreateUWApplicationResponseType", propOrder = {
    "uwApplicationID",
    "applicationStartTime",
    "endOfTreeIndicator",
    "questionSet",
    "outcomeDetails"
})
public class CreateUWApplicationResponseType {

    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "ApplicationStartTime", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar applicationStartTime;
    @XmlElement(name = "EndOfTreeIndicator", required = true)
    protected BigInteger endOfTreeIndicator;
    @XmlElement(name = "QuestionSet")
    protected List<QuestionSet> questionSet;
    @XmlElement(name = "OutcomeDetails")
    protected OutcomeDetails outcomeDetails;

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the applicationStartTime property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getApplicationStartTime() {
        return applicationStartTime;
    }

    /**
     * Sets the value of the applicationStartTime property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setApplicationStartTime(XMLGregorianCalendar value) {
        this.applicationStartTime = value;
    }

    /**
     * Gets the value of the endOfTreeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEndOfTreeIndicator() {
        return endOfTreeIndicator;
    }

    /**
     * Sets the value of the endOfTreeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEndOfTreeIndicator(BigInteger value) {
        this.endOfTreeIndicator = value;
    }

    /**
     * Gets the value of the questionSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the questionSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestionSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuestionSet }
     * 
     * 
     */
    public List<QuestionSet> getQuestionSet() {
        if (questionSet == null) {
            questionSet = new ArrayList<QuestionSet>();
        }
        return this.questionSet;
    }

    /**
     * Gets the value of the outcomeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OutcomeDetails }
     *     
     */
    public OutcomeDetails getOutcomeDetails() {
        return outcomeDetails;
    }

    /**
     * Sets the value of the outcomeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutcomeDetails }
     *     
     */
    public void setOutcomeDetails(OutcomeDetails value) {
        this.outcomeDetails = value;
    }

}
