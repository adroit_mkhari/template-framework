
package za.co.fnb.fnblife.midlife;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for lookupMandateDetailsRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="lookupMandateDetailsRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element ref="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}mandateDetailsRequest" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "lookupMandateDetailsRequestType", propOrder = {
    "mandateDetailsRequest"
})
public class LookupMandateDetailsRequestType {

    @XmlElement(namespace = "http://midlife.fnblife.fnb.co.za/MidLifeMessage", required = true)
    protected List<MandateDetailsRequest> mandateDetailsRequest;

    /**
     * Gets the value of the mandateDetailsRequest property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the mandateDetailsRequest property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getMandateDetailsRequest().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MandateDetailsRequest }
     * 
     * 
     */
    public List<MandateDetailsRequest> getMandateDetailsRequest() {
        if (mandateDetailsRequest == null) {
            mandateDetailsRequest = new ArrayList<MandateDetailsRequest>();
        }
        return this.mandateDetailsRequest;
    }

}
