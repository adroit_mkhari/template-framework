
package za.co.fnb.fnblife.alis.retrieveuwapplicationsummaryrequest;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for RetrieveUnderwritingApplicationSummaryRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveUnderwritingApplicationSummaryRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="UWResponsesEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveUnderwritingApplicationSummaryRequestType", propOrder = {
    "uwApplicationID",
    "uwResponsesEffectiveDate"
})
public class RetrieveUnderwritingApplicationSummaryRequestType {

    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "UWResponsesEffectiveDate")
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar uwResponsesEffectiveDate;

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the uwResponsesEffectiveDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getUWResponsesEffectiveDate() {
        return uwResponsesEffectiveDate;
    }

    /**
     * Sets the value of the uwResponsesEffectiveDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setUWResponsesEffectiveDate(XMLGregorianCalendar value) {
        this.uwResponsesEffectiveDate = value;
    }

}
