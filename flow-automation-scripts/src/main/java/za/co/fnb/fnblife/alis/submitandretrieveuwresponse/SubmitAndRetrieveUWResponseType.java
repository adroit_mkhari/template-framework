
package za.co.fnb.fnblife.alis.submitandretrieveuwresponse;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SubmitAndRetrieveUWResponseType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitAndRetrieveUWResponseType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="UWApplicationID" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="EndOfTreeIndicator" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="QuestionSet" type="{http://alis.fnblife.fnb.co.za/submitAndRetrieveUWResponse}QuestionSet" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="OutcomeDetails" type="{http://alis.fnblife.fnb.co.za/submitAndRetrieveUWResponse}OutcomeDetails" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitAndRetrieveUWResponseType", propOrder = {
    "uwApplicationID",
    "endOfTreeIndicator",
    "questionSet",
    "outcomeDetails"
})
public class SubmitAndRetrieveUWResponseType {

    @XmlElement(name = "UWApplicationID", required = true)
    protected String uwApplicationID;
    @XmlElement(name = "EndOfTreeIndicator", required = true)
    protected BigInteger endOfTreeIndicator;
    @XmlElement(name = "QuestionSet")
    protected List<QuestionSet> questionSet;
    @XmlElement(name = "OutcomeDetails")
    protected OutcomeDetails outcomeDetails;

    /**
     * Gets the value of the uwApplicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUWApplicationID() {
        return uwApplicationID;
    }

    /**
     * Sets the value of the uwApplicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUWApplicationID(String value) {
        this.uwApplicationID = value;
    }

    /**
     * Gets the value of the endOfTreeIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getEndOfTreeIndicator() {
        return endOfTreeIndicator;
    }

    /**
     * Sets the value of the endOfTreeIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setEndOfTreeIndicator(BigInteger value) {
        this.endOfTreeIndicator = value;
    }

    /**
     * Gets the value of the questionSet property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the questionSet property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuestionSet().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuestionSet }
     * 
     * 
     */
    public List<QuestionSet> getQuestionSet() {
        if (questionSet == null) {
            questionSet = new ArrayList<QuestionSet>();
        }
        return this.questionSet;
    }

    /**
     * Gets the value of the outcomeDetails property.
     * 
     * @return
     *     possible object is
     *     {@link OutcomeDetails }
     *     
     */
    public OutcomeDetails getOutcomeDetails() {
        return outcomeDetails;
    }

    /**
     * Sets the value of the outcomeDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutcomeDetails }
     *     
     */
    public void setOutcomeDetails(OutcomeDetails value) {
        this.outcomeDetails = value;
    }

}
