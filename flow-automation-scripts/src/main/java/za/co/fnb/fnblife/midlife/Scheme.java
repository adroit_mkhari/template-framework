
package za.co.fnb.fnblife.midlife;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subChannel" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="schemename" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="companyname" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="compregno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="vatregno" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="turnover" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="contactperson" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="emailaddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="mobilenumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="worknumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="faxnumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="address" maxOccurs="unbounded"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="line1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="line2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                   &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "subChannel",
    "schemename",
    "companyname",
    "compregno",
    "vatregno",
    "turnover",
    "ucn",
    "contactperson",
    "emailaddress",
    "mobilenumber",
    "worknumber",
    "faxnumber",
    "address"
})
@XmlRootElement(name = "scheme")
public class Scheme {

    @XmlElement(required = true)
    protected String subChannel;
    @XmlElement(required = true)
    protected String schemename;
    @XmlElement(required = true)
    protected String companyname;
    @XmlElement(required = true)
    protected String compregno;
    @XmlElement(required = true)
    protected String vatregno;
    @XmlElement(required = true)
    protected String turnover;
    @XmlElement(required = true)
    protected String ucn;
    @XmlElement(required = true)
    protected String contactperson;
    @XmlElement(required = true)
    protected String emailaddress;
    @XmlElement(required = true)
    protected String mobilenumber;
    @XmlElement(required = true)
    protected String worknumber;
    @XmlElement(required = true)
    protected String faxnumber;
    @XmlElement(required = true)
    protected List<Scheme.Address> address;

    /**
     * Gets the value of the subChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubChannel() {
        return subChannel;
    }

    /**
     * Sets the value of the subChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubChannel(String value) {
        this.subChannel = value;
    }

    /**
     * Gets the value of the schemename property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSchemename() {
        return schemename;
    }

    /**
     * Sets the value of the schemename property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSchemename(String value) {
        this.schemename = value;
    }

    /**
     * Gets the value of the companyname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyname() {
        return companyname;
    }

    /**
     * Sets the value of the companyname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyname(String value) {
        this.companyname = value;
    }

    /**
     * Gets the value of the compregno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompregno() {
        return compregno;
    }

    /**
     * Sets the value of the compregno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompregno(String value) {
        this.compregno = value;
    }

    /**
     * Gets the value of the vatregno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVatregno() {
        return vatregno;
    }

    /**
     * Sets the value of the vatregno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVatregno(String value) {
        this.vatregno = value;
    }

    /**
     * Gets the value of the turnover property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTurnover() {
        return turnover;
    }

    /**
     * Sets the value of the turnover property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTurnover(String value) {
        this.turnover = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcn(String value) {
        this.ucn = value;
    }

    /**
     * Gets the value of the contactperson property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactperson() {
        return contactperson;
    }

    /**
     * Sets the value of the contactperson property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactperson(String value) {
        this.contactperson = value;
    }

    /**
     * Gets the value of the emailaddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailaddress() {
        return emailaddress;
    }

    /**
     * Sets the value of the emailaddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailaddress(String value) {
        this.emailaddress = value;
    }

    /**
     * Gets the value of the mobilenumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMobilenumber() {
        return mobilenumber;
    }

    /**
     * Sets the value of the mobilenumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMobilenumber(String value) {
        this.mobilenumber = value;
    }

    /**
     * Gets the value of the worknumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWorknumber() {
        return worknumber;
    }

    /**
     * Sets the value of the worknumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWorknumber(String value) {
        this.worknumber = value;
    }

    /**
     * Gets the value of the faxnumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFaxnumber() {
        return faxnumber;
    }

    /**
     * Sets the value of the faxnumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFaxnumber(String value) {
        this.faxnumber = value;
    }

    /**
     * Gets the value of the address property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the address property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAddress().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Scheme.Address }
     * 
     * 
     */
    public List<Scheme.Address> getAddress() {
        if (address == null) {
            address = new ArrayList<Scheme.Address>();
        }
        return this.address;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="type" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="line1" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="line2" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="suburb" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="city" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *         &lt;element name="code" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "type",
        "line1",
        "line2",
        "suburb",
        "city",
        "code"
    })
    public static class Address {

        @XmlElement(required = true)
        protected String type;
        @XmlElement(required = true)
        protected String line1;
        @XmlElement(required = true)
        protected String line2;
        @XmlElement(required = true)
        protected String suburb;
        @XmlElement(required = true)
        protected String city;
        @XmlElement(required = true)
        protected String code;

        /**
         * Gets the value of the type property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getType() {
            return type;
        }

        /**
         * Sets the value of the type property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setType(String value) {
            this.type = value;
        }

        /**
         * Gets the value of the line1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLine1() {
            return line1;
        }

        /**
         * Sets the value of the line1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLine1(String value) {
            this.line1 = value;
        }

        /**
         * Gets the value of the line2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLine2() {
            return line2;
        }

        /**
         * Sets the value of the line2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLine2(String value) {
            this.line2 = value;
        }

        /**
         * Gets the value of the suburb property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getSuburb() {
            return suburb;
        }

        /**
         * Sets the value of the suburb property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setSuburb(String value) {
            this.suburb = value;
        }

        /**
         * Gets the value of the city property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCity() {
            return city;
        }

        /**
         * Sets the value of the city property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCity(String value) {
            this.city = value;
        }

        /**
         * Gets the value of the code property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCode() {
            return code;
        }

        /**
         * Sets the value of the code property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCode(String value) {
            this.code = value;
        }

    }

}
