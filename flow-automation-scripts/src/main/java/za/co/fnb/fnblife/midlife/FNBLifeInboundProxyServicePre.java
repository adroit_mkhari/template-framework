
package za.co.fnb.fnblife.midlife;

import java.net.MalformedURLException;
import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;


/**
 * This class was generated by the JAX-WS RI.
 * JAX-WS RI 2.3.2
 * Generated source version: 2.2
 * 
 */
@WebServiceClient(name = "FNBLifeInboundProxyServicePre", targetNamespace = "http://midlife.fnblife.fnb.co.za", wsdlLocation = "file:./src/main/resources/wsdl/create/fnbpre-FNBLifeInboundProxy.wsdl")
public class FNBLifeInboundProxyServicePre
    extends Service
{

    private final static URL FNBLIFEINBOUNDPROXYSERVICEPRE_WSDL_LOCATION;
    private final static WebServiceException FNBLIFEINBOUNDPROXYSERVICEPRE_EXCEPTION;
    private final static QName FNBLIFEINBOUNDPROXYSERVICEPRE_QNAME = new QName("http://midlife.fnblife.fnb.co.za", "FNBLifeInboundProxyServicePre");

    static {
        URL url = null;
        WebServiceException e = null;
        try {
            url = new URL("file:./src/main/resources/wsdl/create/fnbpre-FNBLifeInboundProxy.wsdl");
        } catch (MalformedURLException ex) {
            e = new WebServiceException(ex);
        }
        FNBLIFEINBOUNDPROXYSERVICEPRE_WSDL_LOCATION = url;
        FNBLIFEINBOUNDPROXYSERVICEPRE_EXCEPTION = e;
    }

    public FNBLifeInboundProxyServicePre() {
        super(__getWsdlLocation(), FNBLIFEINBOUNDPROXYSERVICEPRE_QNAME);
    }

    public FNBLifeInboundProxyServicePre(WebServiceFeature... features) {
        super(__getWsdlLocation(), FNBLIFEINBOUNDPROXYSERVICEPRE_QNAME, features);
    }

    public FNBLifeInboundProxyServicePre(URL wsdlLocation) {
        super(wsdlLocation, FNBLIFEINBOUNDPROXYSERVICEPRE_QNAME);
    }

    public FNBLifeInboundProxyServicePre(URL wsdlLocation, WebServiceFeature... features) {
        super(wsdlLocation, FNBLIFEINBOUNDPROXYSERVICEPRE_QNAME, features);
    }

    public FNBLifeInboundProxyServicePre(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    public FNBLifeInboundProxyServicePre(URL wsdlLocation, QName serviceName, WebServiceFeature... features) {
        super(wsdlLocation, serviceName, features);
    }

    /**
     * 
     * @return
     *     returns FNBLifeInboundProxy
     */
    @WebEndpoint(name = "FNBLifeInboundProxyPort")
    public FNBLifeInboundProxy getFNBLifeInboundProxyPort() {
        return super.getPort(new QName("http://midlife.fnblife.fnb.co.za", "FNBLifeInboundProxyPort"), FNBLifeInboundProxy.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns FNBLifeInboundProxy
     */
    @WebEndpoint(name = "FNBLifeInboundProxyPort")
    public FNBLifeInboundProxy getFNBLifeInboundProxyPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://midlife.fnblife.fnb.co.za", "FNBLifeInboundProxyPort"), FNBLifeInboundProxy.class, features);
    }

    private static URL __getWsdlLocation() {
        if (FNBLIFEINBOUNDPROXYSERVICEPRE_EXCEPTION!= null) {
            throw FNBLIFEINBOUNDPROXYSERVICEPRE_EXCEPTION;
        }
        return FNBLIFEINBOUNDPROXYSERVICEPRE_WSDL_LOCATION;
    }

}
