
package za.co.fnb.fnblife.alis.createuwapplicationrequest;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnb.fnblife.alis.createuwapplicationrequest package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnb.fnblife.alis.createuwapplicationrequest
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUWApplicationRequest }
     * 
     */
    public CreateUWApplicationRequest createCreateUWApplicationRequest() {
        return new CreateUWApplicationRequest();
    }

    /**
     * Create an instance of {@link CreateUWApplicationRequestType }
     * 
     */
    public CreateUWApplicationRequestType createCreateUWApplicationRequestType() {
        return new CreateUWApplicationRequestType();
    }

    /**
     * Create an instance of {@link PolicyDetails }
     * 
     */
    public PolicyDetails createPolicyDetails() {
        return new PolicyDetails();
    }

    /**
     * Create an instance of {@link ProductDetails }
     * 
     */
    public ProductDetails createProductDetails() {
        return new ProductDetails();
    }

    /**
     * Create an instance of {@link PersonalDetails }
     * 
     */
    public PersonalDetails createPersonalDetails() {
        return new PersonalDetails();
    }

    /**
     * Create an instance of {@link Entity }
     * 
     */
    public Entity createEntity() {
        return new Entity();
    }

    /**
     * Create an instance of {@link Covers }
     * 
     */
    public Covers createCovers() {
        return new Covers();
    }

    /**
     * Create an instance of {@link Cover }
     * 
     */
    public Cover createCover() {
        return new Cover();
    }

    /**
     * Create an instance of {@link CoverDetail }
     * 
     */
    public CoverDetail createCoverDetail() {
        return new CoverDetail();
    }

    /**
     * Create an instance of {@link InternalCovers }
     * 
     */
    public InternalCovers createInternalCovers() {
        return new InternalCovers();
    }

    /**
     * Create an instance of {@link CoverLayers }
     * 
     */
    public CoverLayers createCoverLayers() {
        return new CoverLayers();
    }

}
