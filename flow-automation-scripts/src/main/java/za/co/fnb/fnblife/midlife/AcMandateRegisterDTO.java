
package za.co.fnb.fnblife.midlife;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for acMandateRegisterDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="acMandateRegisterDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="policyNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="UCN" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="product" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subProduct" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="channelName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="sourceName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="inceptionDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="fullName" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IDType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="IDNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="homeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cellNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="emailAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="branchCode" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="accountType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="nonFnbAccount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="payDate" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="collectionDay" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="firstDebitOrderDate" type="{http://www.w3.org/2001/XMLSchema}date"/&gt;
 *         &lt;element name="premium" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="preAuthIndicator" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="customerAgreed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="MAC" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maxCollectionAmount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "acMandateRegisterDTO", propOrder = {
    "policyNumber",
    "ucn",
    "product",
    "subProduct",
    "channelName",
    "sourceName",
    "inceptionDate",
    "fullName",
    "idType",
    "idNumber",
    "homeNumber",
    "cellNumber",
    "emailAddress",
    "accountNumber",
    "branchCode",
    "accountType",
    "nonFnbAccount",
    "payDate",
    "collectionDay",
    "firstDebitOrderDate",
    "premium",
    "preAuthIndicator",
    "customerAgreed",
    "mac",
    "maxCollectionAmount"
})
public class AcMandateRegisterDTO {

    @XmlElement(required = true)
    protected String policyNumber;
    @XmlElement(name = "UCN")
    protected String ucn;
    @XmlElement(required = true)
    protected String product;
    @XmlElement(required = true)
    protected String subProduct;
    @XmlElement(required = true)
    protected String channelName;
    @XmlElement(required = true)
    protected String sourceName;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar inceptionDate;
    @XmlElement(required = true)
    protected String fullName;
    @XmlElement(name = "IDType", required = true)
    protected String idType;
    @XmlElement(name = "IDNumber", required = true)
    protected String idNumber;
    protected String homeNumber;
    protected String cellNumber;
    protected String emailAddress;
    @XmlElement(required = true)
    protected String accountNumber;
    @XmlElement(required = true)
    protected String branchCode;
    @XmlElement(required = true)
    protected String accountType;
    protected String nonFnbAccount;
    protected int payDate;
    protected int collectionDay;
    @XmlElement(required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar firstDebitOrderDate;
    @XmlElement(required = true)
    protected String premium;
    @XmlElement(required = true)
    protected String preAuthIndicator;
    protected String customerAgreed;
    @XmlElement(name = "MAC")
    protected String mac;
    protected String maxCollectionAmount;

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUCN() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUCN(String value) {
        this.ucn = value;
    }

    /**
     * Gets the value of the product property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProduct() {
        return product;
    }

    /**
     * Sets the value of the product property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProduct(String value) {
        this.product = value;
    }

    /**
     * Gets the value of the subProduct property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubProduct() {
        return subProduct;
    }

    /**
     * Sets the value of the subProduct property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubProduct(String value) {
        this.subProduct = value;
    }

    /**
     * Gets the value of the channelName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * Sets the value of the channelName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelName(String value) {
        this.channelName = value;
    }

    /**
     * Gets the value of the sourceName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceName() {
        return sourceName;
    }

    /**
     * Sets the value of the sourceName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceName(String value) {
        this.sourceName = value;
    }

    /**
     * Gets the value of the inceptionDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getInceptionDate() {
        return inceptionDate;
    }

    /**
     * Sets the value of the inceptionDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setInceptionDate(XMLGregorianCalendar value) {
        this.inceptionDate = value;
    }

    /**
     * Gets the value of the fullName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFullName() {
        return fullName;
    }

    /**
     * Sets the value of the fullName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFullName(String value) {
        this.fullName = value;
    }

    /**
     * Gets the value of the idType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDType() {
        return idType;
    }

    /**
     * Sets the value of the idType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDType(String value) {
        this.idType = value;
    }

    /**
     * Gets the value of the idNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIDNumber() {
        return idNumber;
    }

    /**
     * Sets the value of the idNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIDNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Gets the value of the homeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHomeNumber() {
        return homeNumber;
    }

    /**
     * Sets the value of the homeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHomeNumber(String value) {
        this.homeNumber = value;
    }

    /**
     * Gets the value of the cellNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellNumber() {
        return cellNumber;
    }

    /**
     * Sets the value of the cellNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellNumber(String value) {
        this.cellNumber = value;
    }

    /**
     * Gets the value of the emailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailAddress() {
        return emailAddress;
    }

    /**
     * Sets the value of the emailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailAddress(String value) {
        this.emailAddress = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the branchCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBranchCode() {
        return branchCode;
    }

    /**
     * Sets the value of the branchCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBranchCode(String value) {
        this.branchCode = value;
    }

    /**
     * Gets the value of the accountType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountType() {
        return accountType;
    }

    /**
     * Sets the value of the accountType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountType(String value) {
        this.accountType = value;
    }

    /**
     * Gets the value of the nonFnbAccount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNonFnbAccount() {
        return nonFnbAccount;
    }

    /**
     * Sets the value of the nonFnbAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNonFnbAccount(String value) {
        this.nonFnbAccount = value;
    }

    /**
     * Gets the value of the payDate property.
     * 
     */
    public int getPayDate() {
        return payDate;
    }

    /**
     * Sets the value of the payDate property.
     * 
     */
    public void setPayDate(int value) {
        this.payDate = value;
    }

    /**
     * Gets the value of the collectionDay property.
     * 
     */
    public int getCollectionDay() {
        return collectionDay;
    }

    /**
     * Sets the value of the collectionDay property.
     * 
     */
    public void setCollectionDay(int value) {
        this.collectionDay = value;
    }

    /**
     * Gets the value of the firstDebitOrderDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFirstDebitOrderDate() {
        return firstDebitOrderDate;
    }

    /**
     * Sets the value of the firstDebitOrderDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFirstDebitOrderDate(XMLGregorianCalendar value) {
        this.firstDebitOrderDate = value;
    }

    /**
     * Gets the value of the premium property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPremium() {
        return premium;
    }

    /**
     * Sets the value of the premium property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPremium(String value) {
        this.premium = value;
    }

    /**
     * Gets the value of the preAuthIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreAuthIndicator() {
        return preAuthIndicator;
    }

    /**
     * Sets the value of the preAuthIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreAuthIndicator(String value) {
        this.preAuthIndicator = value;
    }

    /**
     * Gets the value of the customerAgreed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerAgreed() {
        return customerAgreed;
    }

    /**
     * Sets the value of the customerAgreed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerAgreed(String value) {
        this.customerAgreed = value;
    }

    /**
     * Gets the value of the mac property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAC() {
        return mac;
    }

    /**
     * Sets the value of the mac property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAC(String value) {
        this.mac = value;
    }

    /**
     * Gets the value of the maxCollectionAmount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxCollectionAmount() {
        return maxCollectionAmount;
    }

    /**
     * Sets the value of the maxCollectionAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxCollectionAmount(String value) {
        this.maxCollectionAmount = value;
    }

}
