
package za.co.fnb.fnblife.midlife.midlifemessage;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for fraudDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="fraudDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="channel" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}channel" minOccurs="0"/&gt;
 *         &lt;element name="individual" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}individual" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="company" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}company" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="otherCriteria" type="{http://midlife.fnblife.fnb.co.za/MidLifeMessage}otherCriteria" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "fraudDetails", propOrder = {
    "channel",
    "individual",
    "company",
    "otherCriteria"
})
public class FraudDetails {

    protected Channel channel;
    protected List<Individual> individual;
    protected List<Company> company;
    protected List<OtherCriteria> otherCriteria;

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link Channel }
     *     
     */
    public Channel getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link Channel }
     *     
     */
    public void setChannel(Channel value) {
        this.channel = value;
    }

    /**
     * Gets the value of the individual property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the individual property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIndividual().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Individual }
     * 
     * 
     */
    public List<Individual> getIndividual() {
        if (individual == null) {
            individual = new ArrayList<Individual>();
        }
        return this.individual;
    }

    /**
     * Gets the value of the company property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the company property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCompany().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Company }
     * 
     * 
     */
    public List<Company> getCompany() {
        if (company == null) {
            company = new ArrayList<Company>();
        }
        return this.company;
    }

    /**
     * Gets the value of the otherCriteria property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the otherCriteria property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOtherCriteria().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OtherCriteria }
     * 
     * 
     */
    public List<OtherCriteria> getOtherCriteria() {
        if (otherCriteria == null) {
            otherCriteria = new ArrayList<OtherCriteria>();
        }
        return this.otherCriteria;
    }

}
