
package za.co.firstrand.gts.cdm.elementschema.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tableHeaderV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tableHeaderV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="selectItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}selectV1.2" minOccurs="0"/&gt;
 *         &lt;element name="textItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}textV1.2" minOccurs="0"/&gt;
 *         &lt;element name="linkItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}linkV1.2" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tableHeaderV1.2", propOrder = {
    "selectItem",
    "textItem",
    "linkItem"
})
public class TableHeaderV12 {

    protected SelectV12 selectItem;
    protected TextV12 textItem;
    protected LinkV12 linkItem;

    /**
     * Gets the value of the selectItem property.
     * 
     * @return
     *     possible object is
     *     {@link SelectV12 }
     *     
     */
    public SelectV12 getSelectItem() {
        return selectItem;
    }

    /**
     * Sets the value of the selectItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectV12 }
     *     
     */
    public void setSelectItem(SelectV12 value) {
        this.selectItem = value;
    }

    /**
     * Gets the value of the textItem property.
     * 
     * @return
     *     possible object is
     *     {@link TextV12 }
     *     
     */
    public TextV12 getTextItem() {
        return textItem;
    }

    /**
     * Sets the value of the textItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextV12 }
     *     
     */
    public void setTextItem(TextV12 value) {
        this.textItem = value;
    }

    /**
     * Gets the value of the linkItem property.
     * 
     * @return
     *     possible object is
     *     {@link LinkV12 }
     *     
     */
    public LinkV12 getLinkItem() {
        return linkItem;
    }

    /**
     * Sets the value of the linkItem property.
     * 
     * @param value
     *     allowed object is
     *     {@link LinkV12 }
     *     
     */
    public void setLinkItem(LinkV12 value) {
        this.linkItem = value;
    }

}
