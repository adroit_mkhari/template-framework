
package za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for GenerateMacCodeRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateMacCodeRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="debtorAccountDetails" type="{http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1}DebtorAccountDetails" form="qualified"/&gt;
 *         &lt;element name="debtorReference" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="debtorIdDetails" type="{http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1}DebtorIdDetails" form="qualified"/&gt;
 *         &lt;element name="currency" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="maxCollectionAmount" type="{http://www.w3.org/2001/XMLSchema}double" form="qualified"/&gt;
 *         &lt;element name="dateApproved" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" form="qualified"/&gt;
 *         &lt;element name="timeApproved" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" form="qualified"/&gt;
 *         &lt;element name="channelStubName" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="channelTraceInfo" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateMacCodeRequest", propOrder = {
    "country",
    "debtorAccountDetails",
    "debtorReference",
    "debtorIdDetails",
    "currency",
    "maxCollectionAmount",
    "dateApproved",
    "timeApproved",
    "channelStubName",
    "channelTraceInfo"
})
public class GenerateMacCodeRequest
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String country;
    @XmlElement(required = true)
    protected DebtorAccountDetails debtorAccountDetails;
    @XmlElement(required = true)
    protected String debtorReference;
    @XmlElement(required = true)
    protected DebtorIdDetails debtorIdDetails;
    @XmlElement(required = true)
    protected String currency;
    protected double maxCollectionAmount;
    @XmlElement(required = true)
    protected Object dateApproved;
    @XmlElement(required = true)
    protected Object timeApproved;
    @XmlElement(required = true)
    protected String channelStubName;
    @XmlElement(required = true)
    protected String channelTraceInfo;

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the debtorAccountDetails property.
     * 
     * @return
     *     possible object is
     *     {@link DebtorAccountDetails }
     *     
     */
    public DebtorAccountDetails getDebtorAccountDetails() {
        return debtorAccountDetails;
    }

    /**
     * Sets the value of the debtorAccountDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebtorAccountDetails }
     *     
     */
    public void setDebtorAccountDetails(DebtorAccountDetails value) {
        this.debtorAccountDetails = value;
    }

    /**
     * Gets the value of the debtorReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebtorReference() {
        return debtorReference;
    }

    /**
     * Sets the value of the debtorReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebtorReference(String value) {
        this.debtorReference = value;
    }

    /**
     * Gets the value of the debtorIdDetails property.
     * 
     * @return
     *     possible object is
     *     {@link DebtorIdDetails }
     *     
     */
    public DebtorIdDetails getDebtorIdDetails() {
        return debtorIdDetails;
    }

    /**
     * Sets the value of the debtorIdDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link DebtorIdDetails }
     *     
     */
    public void setDebtorIdDetails(DebtorIdDetails value) {
        this.debtorIdDetails = value;
    }

    /**
     * Gets the value of the currency property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * Sets the value of the currency property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrency(String value) {
        this.currency = value;
    }

    /**
     * Gets the value of the maxCollectionAmount property.
     * 
     */
    public double getMaxCollectionAmount() {
        return maxCollectionAmount;
    }

    /**
     * Sets the value of the maxCollectionAmount property.
     * 
     */
    public void setMaxCollectionAmount(double value) {
        this.maxCollectionAmount = value;
    }

    /**
     * Gets the value of the dateApproved property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getDateApproved() {
        return dateApproved;
    }

    /**
     * Sets the value of the dateApproved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setDateApproved(Object value) {
        this.dateApproved = value;
    }

    /**
     * Gets the value of the timeApproved property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getTimeApproved() {
        return timeApproved;
    }

    /**
     * Sets the value of the timeApproved property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setTimeApproved(Object value) {
        this.timeApproved = value;
    }

    /**
     * Gets the value of the channelStubName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelStubName() {
        return channelStubName;
    }

    /**
     * Sets the value of the channelStubName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelStubName(String value) {
        this.channelStubName = value;
    }

    /**
     * Gets the value of the channelTraceInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelTraceInfo() {
        return channelTraceInfo;
    }

    /**
     * Sets the value of the channelTraceInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelTraceInfo(String value) {
        this.channelTraceInfo = value;
    }

}
