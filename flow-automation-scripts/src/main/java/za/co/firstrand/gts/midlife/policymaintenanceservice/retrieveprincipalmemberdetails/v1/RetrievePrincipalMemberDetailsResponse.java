
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.policyservicing.v1.Address;
import za.co.firstrand.gts.cdm.policyservicing.v1.ContactDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.ExtendedDetails;
import za.co.firstrand.gts.cdm.policyservicing.v1.PolicyDetails;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for RetrievePrincipalMemberDetailsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrievePrincipalMemberDetailsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="policyDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}PolicyDetails" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="contactDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}ContactDetails" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="residentialAddress" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}Address" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="postalAddress" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}Address" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="extendedDetails" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}ExtendedDetails" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrievePrincipalMemberDetailsResponse", propOrder = {
    "message",
    "policyDetails",
    "contactDetails",
    "residentialAddress",
    "postalAddress",
    "extendedDetails"
})
public class RetrievePrincipalMemberDetailsResponse
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String message;
    protected PolicyDetails policyDetails;
    protected ContactDetails contactDetails;
    protected Address residentialAddress;
    protected Address postalAddress;
    protected ExtendedDetails extendedDetails;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the policyDetails property.
     * 
     * @return
     *     possible object is
     *     {@link PolicyDetails }
     *     
     */
    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    /**
     * Sets the value of the policyDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link PolicyDetails }
     *     
     */
    public void setPolicyDetails(PolicyDetails value) {
        this.policyDetails = value;
    }

    /**
     * Gets the value of the contactDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ContactDetails }
     *     
     */
    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    /**
     * Sets the value of the contactDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDetails }
     *     
     */
    public void setContactDetails(ContactDetails value) {
        this.contactDetails = value;
    }

    /**
     * Gets the value of the residentialAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getResidentialAddress() {
        return residentialAddress;
    }

    /**
     * Sets the value of the residentialAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setResidentialAddress(Address value) {
        this.residentialAddress = value;
    }

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Address }
     *     
     */
    public Address getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Address }
     *     
     */
    public void setPostalAddress(Address value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the extendedDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ExtendedDetails }
     *     
     */
    public ExtendedDetails getExtendedDetails() {
        return extendedDetails;
    }

    /**
     * Sets the value of the extendedDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ExtendedDetails }
     *     
     */
    public void setExtendedDetails(ExtendedDetails value) {
        this.extendedDetails = value;
    }

}
