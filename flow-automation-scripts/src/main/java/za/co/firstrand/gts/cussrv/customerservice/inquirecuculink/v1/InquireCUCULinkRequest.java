
package za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessageCUCU;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageCUCU"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="action" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="customerKey" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="companyID" minOccurs="0" form="qualified"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="uniqueCustomerNumber" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="filtering" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="ruleSet" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="allOrMissing" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="channel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="nextCUCUInfo" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="nextCIFKey" minOccurs="0" form="qualified"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="nextCIFCompanyID" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                             &lt;element name="nextCIFtie" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCIFtieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="nextCUCUKey" minOccurs="0" form="qualified"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCIFCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                             &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "action",
    "customerKey",
    "filtering",
    "ruleSet",
    "allOrMissing",
    "channel",
    "nextCUCUInfo"
})
@XmlRootElement(name = "InquireCUCULinkRequest")
public class InquireCUCULinkRequest
    extends EnterpriseMessageCUCU
{

    protected String action;
    protected InquireCUCULinkRequest.CustomerKey customerKey;
    protected String filtering;
    protected String ruleSet;
    protected String allOrMissing;
    protected String channel;
    protected InquireCUCULinkRequest.NextCUCUInfo nextCUCUInfo;

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the customerKey property.
     * 
     * @return
     *     possible object is
     *     {@link InquireCUCULinkRequest.CustomerKey }
     *     
     */
    public InquireCUCULinkRequest.CustomerKey getCustomerKey() {
        return customerKey;
    }

    /**
     * Sets the value of the customerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquireCUCULinkRequest.CustomerKey }
     *     
     */
    public void setCustomerKey(InquireCUCULinkRequest.CustomerKey value) {
        this.customerKey = value;
    }

    /**
     * Gets the value of the filtering property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFiltering() {
        return filtering;
    }

    /**
     * Sets the value of the filtering property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFiltering(String value) {
        this.filtering = value;
    }

    /**
     * Gets the value of the ruleSet property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRuleSet() {
        return ruleSet;
    }

    /**
     * Sets the value of the ruleSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRuleSet(String value) {
        this.ruleSet = value;
    }

    /**
     * Gets the value of the allOrMissing property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllOrMissing() {
        return allOrMissing;
    }

    /**
     * Sets the value of the allOrMissing property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllOrMissing(String value) {
        this.allOrMissing = value;
    }

    /**
     * Gets the value of the channel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannel() {
        return channel;
    }

    /**
     * Sets the value of the channel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannel(String value) {
        this.channel = value;
    }

    /**
     * Gets the value of the nextCUCUInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InquireCUCULinkRequest.NextCUCUInfo }
     *     
     */
    public InquireCUCULinkRequest.NextCUCUInfo getNextCUCUInfo() {
        return nextCUCUInfo;
    }

    /**
     * Sets the value of the nextCUCUInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquireCUCULinkRequest.NextCUCUInfo }
     *     
     */
    public void setNextCUCUInfo(InquireCUCULinkRequest.NextCUCUInfo value) {
        this.nextCUCUInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="companyID" minOccurs="0" form="qualified"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="uniqueCustomerNumber" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "companyID",
        "uniqueCustomerNumber"
    })
    public static class CustomerKey {

        protected InquireCUCULinkRequest.CustomerKey.CompanyID companyID;
        protected int uniqueCustomerNumber;

        /**
         * Gets the value of the companyID property.
         * 
         * @return
         *     possible object is
         *     {@link InquireCUCULinkRequest.CustomerKey.CompanyID }
         *     
         */
        public InquireCUCULinkRequest.CustomerKey.CompanyID getCompanyID() {
            return companyID;
        }

        /**
         * Sets the value of the companyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link InquireCUCULinkRequest.CustomerKey.CompanyID }
         *     
         */
        public void setCompanyID(InquireCUCULinkRequest.CustomerKey.CompanyID value) {
            this.companyID = value;
        }

        /**
         * Gets the value of the uniqueCustomerNumber property.
         * 
         */
        public int getUniqueCustomerNumber() {
            return uniqueCustomerNumber;
        }

        /**
         * Sets the value of the uniqueCustomerNumber property.
         * 
         */
        public void setUniqueCustomerNumber(int value) {
            this.uniqueCustomerNumber = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "country"
        })
        public static class CompanyID {

            @XmlElement(required = true)
            protected String country;

            /**
             * Gets the value of the country property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getCountry() {
                return country;
            }

            /**
             * Sets the value of the country property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCountry(String value) {
                this.country = value;
            }

        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="nextCIFKey" minOccurs="0" form="qualified"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="nextCIFCompanyID" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                   &lt;element name="nextCIFtie" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCIFtieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="nextCUCUKey" minOccurs="0" form="qualified"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCIFCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                   &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nextCIFKey",
        "nextCUCUKey"
    })
    public static class NextCUCUInfo {

        protected InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey nextCIFKey;
        protected InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey nextCUCUKey;

        /**
         * Gets the value of the nextCIFKey property.
         * 
         * @return
         *     possible object is
         *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey }
         *     
         */
        public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey getNextCIFKey() {
            return nextCIFKey;
        }

        /**
         * Sets the value of the nextCIFKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey }
         *     
         */
        public void setNextCIFKey(InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey value) {
            this.nextCIFKey = value;
        }

        /**
         * Gets the value of the nextCUCUKey property.
         * 
         * @return
         *     possible object is
         *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey }
         *     
         */
        public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey getNextCUCUKey() {
            return nextCUCUKey;
        }

        /**
         * Sets the value of the nextCUCUKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey }
         *     
         */
        public void setNextCUCUKey(InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey value) {
            this.nextCUCUKey = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="nextCIFCompanyID" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *         &lt;element name="nextCIFtie" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCIFtieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nextCIFCompanyID",
            "nextCIFID",
            "nextCIFtie"
        })
        public static class NextCIFKey {

            protected InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID nextCIFCompanyID;
            protected String nextCIFID;
            protected InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie nextCIFtie;

            /**
             * Gets the value of the nextCIFCompanyID property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID }
             *     
             */
            public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID getNextCIFCompanyID() {
                return nextCIFCompanyID;
            }

            /**
             * Sets the value of the nextCIFCompanyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID }
             *     
             */
            public void setNextCIFCompanyID(InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFCompanyID value) {
                this.nextCIFCompanyID = value;
            }

            /**
             * Gets the value of the nextCIFID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNextCIFID() {
                return nextCIFID;
            }

            /**
             * Sets the value of the nextCIFID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNextCIFID(String value) {
                this.nextCIFID = value;
            }

            /**
             * Gets the value of the nextCIFtie property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie }
             *     
             */
            public InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie getNextCIFtie() {
                return nextCIFtie;
            }

            /**
             * Sets the value of the nextCIFtie property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie }
             *     
             */
            public void setNextCIFtie(InquireCUCULinkRequest.NextCUCUInfo.NextCIFKey.NextCIFtie value) {
                this.nextCIFtie = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCIFCompanyNumber"
            })
            public static class NextCIFCompanyID {

                protected String nextCIFCompanyNumber;

                /**
                 * Gets the value of the nextCIFCompanyNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNextCIFCompanyNumber() {
                    return nextCIFCompanyNumber;
                }

                /**
                 * Sets the value of the nextCIFCompanyNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNextCIFCompanyNumber(String value) {
                    this.nextCIFCompanyNumber = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCIFtieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCIFtieNumber"
            })
            public static class NextCIFtie {

                protected Integer nextCIFtieNumber;

                /**
                 * Gets the value of the nextCIFtieNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCIFtieNumber() {
                    return nextCIFtieNumber;
                }

                /**
                 * Sets the value of the nextCIFtieNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCIFtieNumber(Integer value) {
                    this.nextCIFtieNumber = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCIFCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *         &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nextCUCUCompanyID",
            "nextCUCUCustomerID",
            "nextCUCUTie",
            "nextCUCURelationshipCode"
        })
        public static class NextCUCUKey {

            protected InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID nextCUCUCompanyID;
            protected String nextCUCUCustomerID;
            protected InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie nextCUCUTie;
            protected String nextCUCURelationshipCode;

            /**
             * Gets the value of the nextCUCUCompanyID property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
             *     
             */
            public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID getNextCUCUCompanyID() {
                return nextCUCUCompanyID;
            }

            /**
             * Sets the value of the nextCUCUCompanyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
             *     
             */
            public void setNextCUCUCompanyID(InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID value) {
                this.nextCUCUCompanyID = value;
            }

            /**
             * Gets the value of the nextCUCUCustomerID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNextCUCUCustomerID() {
                return nextCUCUCustomerID;
            }

            /**
             * Sets the value of the nextCUCUCustomerID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNextCUCUCustomerID(String value) {
                this.nextCUCUCustomerID = value;
            }

            /**
             * Gets the value of the nextCUCUTie property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
             *     
             */
            public InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie getNextCUCUTie() {
                return nextCUCUTie;
            }

            /**
             * Sets the value of the nextCUCUTie property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
             *     
             */
            public void setNextCUCUTie(InquireCUCULinkRequest.NextCUCUInfo.NextCUCUKey.NextCUCUTie value) {
                this.nextCUCUTie = value;
            }

            /**
             * Gets the value of the nextCUCURelationshipCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNextCUCURelationshipCode() {
                return nextCUCURelationshipCode;
            }

            /**
             * Sets the value of the nextCUCURelationshipCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNextCUCURelationshipCode(String value) {
                this.nextCUCURelationshipCode = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCIFCompany" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCIFCompany"
            })
            public static class NextCUCUCompanyID {

                protected String nextCIFCompany;

                /**
                 * Gets the value of the nextCIFCompany property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getNextCIFCompany() {
                    return nextCIFCompany;
                }

                /**
                 * Sets the value of the nextCIFCompany property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setNextCIFCompany(String value) {
                    this.nextCIFCompany = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCUCUTieNumber"
            })
            public static class NextCUCUTie {

                protected Integer nextCUCUTieNumber;

                /**
                 * Gets the value of the nextCUCUTieNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCUCUTieNumber() {
                    return nextCUCUTieNumber;
                }

                /**
                 * Sets the value of the nextCUCUTieNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCUCUTieNumber(Integer value) {
                    this.nextCUCUTieNumber = value;
                }

            }

        }

    }

}
