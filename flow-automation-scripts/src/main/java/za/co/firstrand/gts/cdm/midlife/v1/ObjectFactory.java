
package za.co.firstrand.gts.cdm.midlife.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1.SubmitPreLoginClaimRequest;
import za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1.SubmitPreLoginClaimResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.midlife.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Literal_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "literal");
    private final static QName _Literals_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "literals");
    private final static QName _Parameter_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "parameter");
    private final static QName _Parameters_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "parameters");
    private final static QName _SubmitPreLoginClaimRequest_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "submitPreLoginClaimRequest");
    private final static QName _SubmitPreLoginClaimResponse_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "submitPreLoginClaimResponse");
    private final static QName _Value_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/midlife/v1", "value");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.midlife.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ParametersV12 }
     * 
     */
    public ParametersV12 createParametersV12() {
        return new ParametersV12();
    }

    /**
     * Create an instance of {@link LiteralsV12 }
     * 
     */
    public LiteralsV12 createLiteralsV12() {
        return new LiteralsV12();
    }

    /**
     * Create an instance of {@link KeyValue }
     * 
     */
    public KeyValue createKeyValue() {
        return new KeyValue();
    }

    /**
     * Create an instance of {@link DeceasedInformation }
     * 
     */
    public DeceasedInformation createDeceasedInformation() {
        return new DeceasedInformation();
    }

    /**
     * Create an instance of {@link ClaimantInformation }
     * 
     */
    public ClaimantInformation createClaimantInformation() {
        return new ClaimantInformation();
    }

    /**
     * Create an instance of {@link BankingDetails }
     * 
     */
    public BankingDetails createBankingDetails() {
        return new BankingDetails();
    }

    /**
     * Create an instance of {@link MedicalPractitionerDetails }
     * 
     */
    public MedicalPractitionerDetails createMedicalPractitionerDetails() {
        return new MedicalPractitionerDetails();
    }

    /**
     * Create an instance of {@link InformantDetails }
     * 
     */
    public InformantDetails createInformantDetails() {
        return new InformantDetails();
    }

    /**
     * Create an instance of {@link FuneralParlourDetails }
     * 
     */
    public FuneralParlourDetails createFuneralParlourDetails() {
        return new FuneralParlourDetails();
    }

    /**
     * Create an instance of {@link ParameterV12 }
     * 
     */
    public ParameterV12 createParameterV12() {
        return new ParameterV12();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link String }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "literal")
    public JAXBElement<String> createLiteral(String value) {
        return new JAXBElement<String>(_Literal_QNAME, String.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LiteralsV12 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link LiteralsV12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "literals")
    public JAXBElement<LiteralsV12> createLiterals(LiteralsV12 value) {
        return new JAXBElement<LiteralsV12>(_Literals_QNAME, LiteralsV12 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link KeyValue }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link KeyValue }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "parameter")
    public JAXBElement<KeyValue> createParameter(KeyValue value) {
        return new JAXBElement<KeyValue>(_Parameter_QNAME, KeyValue.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ParametersV12 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ParametersV12 }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "parameters")
    public JAXBElement<ParametersV12> createParameters(ParametersV12 value) {
        return new JAXBElement<ParametersV12>(_Parameters_QNAME, ParametersV12 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "submitPreLoginClaimRequest")
    public JAXBElement<SubmitPreLoginClaimRequest> createSubmitPreLoginClaimRequest(SubmitPreLoginClaimRequest value) {
        return new JAXBElement<SubmitPreLoginClaimRequest>(_SubmitPreLoginClaimRequest_QNAME, SubmitPreLoginClaimRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "submitPreLoginClaimResponse")
    public JAXBElement<SubmitPreLoginClaimResponse> createSubmitPreLoginClaimResponse(SubmitPreLoginClaimResponse value) {
        return new JAXBElement<SubmitPreLoginClaimResponse>(_SubmitPreLoginClaimResponse_QNAME, SubmitPreLoginClaimResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Object }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/midlife/v1", name = "value")
    public JAXBElement<Object> createValue(Object value) {
        return new JAXBElement<Object>(_Value_QNAME, Object.class, null, value);
    }

}
