
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.midlife.v1.ParametersV12;


/**
 * <p>Java class for selectOptionsV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="selectOptionsV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="optionItem" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}ParametersV1.2" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "selectOptionsV1.2", propOrder = {
    "optionItem"
})
public class SelectOptionsV12 {

    @XmlElement(required = true)
    protected List<ParametersV12> optionItem;

    /**
     * Gets the value of the optionItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the optionItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOptionItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametersV12 }
     * 
     * 
     */
    public List<ParametersV12> getOptionItem() {
        if (optionItem == null) {
            optionItem = new ArrayList<ParametersV12>();
        }
        return this.optionItem;
    }

}
