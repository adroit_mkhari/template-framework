
package za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v7;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v7 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v7
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LookupCustomerDetailsRequest }
     * 
     */
    public LookupCustomerDetailsRequest createLookupCustomerDetailsRequest() {
        return new LookupCustomerDetailsRequest();
    }

    /**
     * Create an instance of {@link SearchType }
     * 
     */
    public SearchType createSearchType() {
        return new SearchType();
    }

    /**
     * Create an instance of {@link LookupCustomerDetailsResponse }
     * 
     */
    public LookupCustomerDetailsResponse createLookupCustomerDetailsResponse() {
        return new LookupCustomerDetailsResponse();
    }

    /**
     * Create an instance of {@link ByAccount }
     * 
     */
    public ByAccount createByAccount() {
        return new ByAccount();
    }

}
