
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerTypev12.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerTypev12"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="OFFICE_ACCOUNTS"/&gt;
 *     &lt;enumeration value="PRIVATE_INDIVIDUAL"/&gt;
 *     &lt;enumeration value="SOLE_PROPRIETORSHIP"/&gt;
 *     &lt;enumeration value="PARTNERSHIP"/&gt;
 *     &lt;enumeration value="CLOSE_CORPORATION-CC"/&gt;
 *     &lt;enumeration value="PRIVATE_COMPANY-LTD"/&gt;
 *     &lt;enumeration value="PUBLIC_COMPANY-LTD"/&gt;
 *     &lt;enumeration value="NON_PROFIT_COMPANY-NPC"/&gt;
 *     &lt;enumeration value="PERSONAL_LIABILITY_COMPANY-INC"/&gt;
 *     &lt;enumeration value="REGISTERED_TRUST"/&gt;
 *     &lt;enumeration value="FOUNDATION"/&gt;
 *     &lt;enumeration value="CO-OPERATIVE"/&gt;
 *     &lt;enumeration value="GOVERNMENT/QUASI-GOVERNMENT"/&gt;
 *     &lt;enumeration value="STOKVEL"/&gt;
 *     &lt;enumeration value="WESBANK_INDIVIDUAL"/&gt;
 *     &lt;enumeration value="RIFF_RAFF-FRAUD_AND_FORGERIES"/&gt;
 *     &lt;enumeration value="RIFF_RAFF-ADMINISTRATION_ERRORS"/&gt;
 *     &lt;enumeration value="RIFF_RAFF-ROBBERIES"/&gt;
 *     &lt;enumeration value="REGISTERED_FRIENDLY_SOCIETY"/&gt;
 *     &lt;enumeration value="OTHER_ASSOCIATION/CLUB/ETC"/&gt;
 *     &lt;enumeration value="JOINT_HOME_LOANS"/&gt;
 *     &lt;enumeration value="FOREIGN_COMPANY_REGISTERED_IN_SOUTH_AFRICA"/&gt;
 *     &lt;enumeration value="FOREIGN_COMPANY_NOT_REGISTERED_IN_SOUTH_AFRICA"/&gt;
 *     &lt;enumeration value="FNB_ASSOCIATED"/&gt;
 *     &lt;enumeration value="FOREIGN_BANKS"/&gt;
 *     &lt;enumeration value="RESERVED_ACCOUNTS_PROFILE"/&gt;
 *     &lt;enumeration value="PROSPECT_CUSTOMER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomerTypev12")
@XmlEnum
public enum CustomerTypev12 {

    OFFICE_ACCOUNTS("OFFICE_ACCOUNTS"),
    PRIVATE_INDIVIDUAL("PRIVATE_INDIVIDUAL"),
    SOLE_PROPRIETORSHIP("SOLE_PROPRIETORSHIP"),
    PARTNERSHIP("PARTNERSHIP"),
    @XmlEnumValue("CLOSE_CORPORATION-CC")
    CLOSE_CORPORATION_CC("CLOSE_CORPORATION-CC"),
    @XmlEnumValue("PRIVATE_COMPANY-LTD")
    PRIVATE_COMPANY_LTD("PRIVATE_COMPANY-LTD"),
    @XmlEnumValue("PUBLIC_COMPANY-LTD")
    PUBLIC_COMPANY_LTD("PUBLIC_COMPANY-LTD"),
    @XmlEnumValue("NON_PROFIT_COMPANY-NPC")
    NON_PROFIT_COMPANY_NPC("NON_PROFIT_COMPANY-NPC"),
    @XmlEnumValue("PERSONAL_LIABILITY_COMPANY-INC")
    PERSONAL_LIABILITY_COMPANY_INC("PERSONAL_LIABILITY_COMPANY-INC"),
    REGISTERED_TRUST("REGISTERED_TRUST"),
    FOUNDATION("FOUNDATION"),
    @XmlEnumValue("CO-OPERATIVE")
    CO_OPERATIVE("CO-OPERATIVE"),
    @XmlEnumValue("GOVERNMENT/QUASI-GOVERNMENT")
    GOVERNMENT_QUASI_GOVERNMENT("GOVERNMENT/QUASI-GOVERNMENT"),
    STOKVEL("STOKVEL"),
    WESBANK_INDIVIDUAL("WESBANK_INDIVIDUAL"),
    @XmlEnumValue("RIFF_RAFF-FRAUD_AND_FORGERIES")
    RIFF_RAFF_FRAUD_AND_FORGERIES("RIFF_RAFF-FRAUD_AND_FORGERIES"),
    @XmlEnumValue("RIFF_RAFF-ADMINISTRATION_ERRORS")
    RIFF_RAFF_ADMINISTRATION_ERRORS("RIFF_RAFF-ADMINISTRATION_ERRORS"),
    @XmlEnumValue("RIFF_RAFF-ROBBERIES")
    RIFF_RAFF_ROBBERIES("RIFF_RAFF-ROBBERIES"),
    REGISTERED_FRIENDLY_SOCIETY("REGISTERED_FRIENDLY_SOCIETY"),
    @XmlEnumValue("OTHER_ASSOCIATION/CLUB/ETC")
    OTHER_ASSOCIATION_CLUB_ETC("OTHER_ASSOCIATION/CLUB/ETC"),
    JOINT_HOME_LOANS("JOINT_HOME_LOANS"),
    FOREIGN_COMPANY_REGISTERED_IN_SOUTH_AFRICA("FOREIGN_COMPANY_REGISTERED_IN_SOUTH_AFRICA"),
    FOREIGN_COMPANY_NOT_REGISTERED_IN_SOUTH_AFRICA("FOREIGN_COMPANY_NOT_REGISTERED_IN_SOUTH_AFRICA"),
    FNB_ASSOCIATED("FNB_ASSOCIATED"),
    FOREIGN_BANKS("FOREIGN_BANKS"),
    RESERVED_ACCOUNTS_PROFILE("RESERVED_ACCOUNTS_PROFILE"),
    PROSPECT_CUSTOMER("PROSPECT_CUSTOMER");
    private final String value;

    CustomerTypev12(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static CustomerTypev12 fromValue(String v) {
        for (CustomerTypev12 c: CustomerTypev12 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
