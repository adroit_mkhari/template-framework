
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1.MaintainPolicyDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1.MaintainPolicyDetailsResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1.RetrievePolicyOverviewRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1.RetrievePolicyOverviewResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1.RetrievePrincipalMemberDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1.RetrievePrincipalMemberDetailsResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1.SendGenericLetterRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1.SendGenericLetterResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.shared.enterprisemessage.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MaintainPolicyDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "maintainPolicyDetailsRequest");
    private final static QName _MaintainPolicyDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "maintainPolicyDetailsResponse");
    private final static QName _RetrievePolicyOverviewRequest_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "retrievePolicyOverviewRequest");
    private final static QName _RetrievePolicyOverviewResponse_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "retrievePolicyOverviewResponse");
    private final static QName _RetrievePrincipalMemberDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "retrievePrincipalMemberDetailsRequest");
    private final static QName _RetrievePrincipalMemberDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "retrievePrincipalMemberDetailsResponse");
    private final static QName _SendGenericLetterRequest_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "sendGenericLetterRequest");
    private final static QName _SendGenericLetterResponse_QNAME = new QName("http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", "sendGenericLetterResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.shared.enterprisemessage.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EnterpriseMessage }
     * 
     */
    public EnterpriseMessage createEnterpriseMessage() {
        return new EnterpriseMessage();
    }

    /**
     * Create an instance of {@link EnterpriseMessageHeader }
     * 
     */
    public EnterpriseMessageHeader createEnterpriseMessageHeader() {
        return new EnterpriseMessageHeader();
    }

    /**
     * Create an instance of {@link Sequence }
     * 
     */
    public Sequence createSequence() {
        return new Sequence();
    }

    /**
     * Create an instance of {@link UserCredentials }
     * 
     */
    public UserCredentials createUserCredentials() {
        return new UserCredentials();
    }

    /**
     * Create an instance of {@link ResultType }
     * 
     */
    public ResultType createResultType() {
        return new ResultType();
    }

    /**
     * Create an instance of {@link SequenceV12 }
     * 
     */
    public SequenceV12 createSequenceV12() {
        return new SequenceV12();
    }

    /**
     * Create an instance of {@link EnterpriseMessageV12 }
     * 
     */
    public EnterpriseMessageV12 createEnterpriseMessageV12() {
        return new EnterpriseMessageV12();
    }

    /**
     * Create an instance of {@link EnterpriseMessageHeaderV12 }
     * 
     */
    public EnterpriseMessageHeaderV12 createEnterpriseMessageHeaderV12() {
        return new EnterpriseMessageHeaderV12();
    }

    /**
     * Create an instance of {@link UserCredentialsV12 }
     * 
     */
    public UserCredentialsV12 createUserCredentialsV12() {
        return new UserCredentialsV12();
    }

    /**
     * Create an instance of {@link ResultTypeV12 }
     * 
     */
    public ResultTypeV12 createResultTypeV12() {
        return new ResultTypeV12();
    }

    /**
     * Create an instance of {@link SequenceCUCU }
     * 
     */
    public SequenceCUCU createSequenceCUCU() {
        return new SequenceCUCU();
    }

    /**
     * Create an instance of {@link EnterpriseMessageCUCU }
     * 
     */
    public EnterpriseMessageCUCU createEnterpriseMessageCUCU() {
        return new EnterpriseMessageCUCU();
    }

    /**
     * Create an instance of {@link EnterpriseMessageHeaderCUCU }
     * 
     */
    public EnterpriseMessageHeaderCUCU createEnterpriseMessageHeaderCUCU() {
        return new EnterpriseMessageHeaderCUCU();
    }

    /**
     * Create an instance of {@link UserCredentialsCUCU }
     * 
     */
    public UserCredentialsCUCU createUserCredentialsCUCU() {
        return new UserCredentialsCUCU();
    }

    /**
     * Create an instance of {@link ResultTypeCUCU }
     * 
     */
    public ResultTypeCUCU createResultTypeCUCU() {
        return new ResultTypeCUCU();
    }

    /**
     * Create an instance of {@link SequenceLA }
     * 
     */
    public SequenceLA createSequenceLA() {
        return new SequenceLA();
    }

    /**
     * Create an instance of {@link EnterpriseMessageLA }
     * 
     */
    public EnterpriseMessageLA createEnterpriseMessageLA() {
        return new EnterpriseMessageLA();
    }

    /**
     * Create an instance of {@link EnterpriseMessageHeaderLA }
     * 
     */
    public EnterpriseMessageHeaderLA createEnterpriseMessageHeaderLA() {
        return new EnterpriseMessageHeaderLA();
    }

    /**
     * Create an instance of {@link UserCredentialsLA }
     * 
     */
    public UserCredentialsLA createUserCredentialsLA() {
        return new UserCredentialsLA();
    }

    /**
     * Create an instance of {@link ResultTypeLA }
     * 
     */
    public ResultTypeLA createResultTypeLA() {
        return new ResultTypeLA();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "maintainPolicyDetailsRequest")
    public JAXBElement<MaintainPolicyDetailsRequest> createMaintainPolicyDetailsRequest(MaintainPolicyDetailsRequest value) {
        return new JAXBElement<MaintainPolicyDetailsRequest>(_MaintainPolicyDetailsRequest_QNAME, MaintainPolicyDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "maintainPolicyDetailsResponse")
    public JAXBElement<MaintainPolicyDetailsResponse> createMaintainPolicyDetailsResponse(MaintainPolicyDetailsResponse value) {
        return new JAXBElement<MaintainPolicyDetailsResponse>(_MaintainPolicyDetailsResponse_QNAME, MaintainPolicyDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "retrievePolicyOverviewRequest")
    public JAXBElement<RetrievePolicyOverviewRequest> createRetrievePolicyOverviewRequest(RetrievePolicyOverviewRequest value) {
        return new JAXBElement<RetrievePolicyOverviewRequest>(_RetrievePolicyOverviewRequest_QNAME, RetrievePolicyOverviewRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "retrievePolicyOverviewResponse")
    public JAXBElement<RetrievePolicyOverviewResponse> createRetrievePolicyOverviewResponse(RetrievePolicyOverviewResponse value) {
        return new JAXBElement<RetrievePolicyOverviewResponse>(_RetrievePolicyOverviewResponse_QNAME, RetrievePolicyOverviewResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "retrievePrincipalMemberDetailsRequest")
    public JAXBElement<RetrievePrincipalMemberDetailsRequest> createRetrievePrincipalMemberDetailsRequest(RetrievePrincipalMemberDetailsRequest value) {
        return new JAXBElement<RetrievePrincipalMemberDetailsRequest>(_RetrievePrincipalMemberDetailsRequest_QNAME, RetrievePrincipalMemberDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "retrievePrincipalMemberDetailsResponse")
    public JAXBElement<RetrievePrincipalMemberDetailsResponse> createRetrievePrincipalMemberDetailsResponse(RetrievePrincipalMemberDetailsResponse value) {
        return new JAXBElement<RetrievePrincipalMemberDetailsResponse>(_RetrievePrincipalMemberDetailsResponse_QNAME, RetrievePrincipalMemberDetailsResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendGenericLetterRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendGenericLetterRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "sendGenericLetterRequest")
    public JAXBElement<SendGenericLetterRequest> createSendGenericLetterRequest(SendGenericLetterRequest value) {
        return new JAXBElement<SendGenericLetterRequest>(_SendGenericLetterRequest_QNAME, SendGenericLetterRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendGenericLetterResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendGenericLetterResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2", name = "sendGenericLetterResponse")
    public JAXBElement<SendGenericLetterResponse> createSendGenericLetterResponse(SendGenericLetterResponse value) {
        return new JAXBElement<SendGenericLetterResponse>(_SendGenericLetterResponse_QNAME, SendGenericLetterResponse.class, null, value);
    }

}
