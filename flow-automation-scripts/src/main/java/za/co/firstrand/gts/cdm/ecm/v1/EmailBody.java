
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for emailBody complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="emailBody"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="distributionReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="html" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="mht" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "emailBody", propOrder = {
    "distributionReference",
    "html",
    "mht"
})
public class EmailBody {

    protected String distributionReference;
    protected byte[] html;
    protected byte[] mht;

    /**
     * Gets the value of the distributionReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionReference() {
        return distributionReference;
    }

    /**
     * Sets the value of the distributionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionReference(String value) {
        this.distributionReference = value;
    }

    /**
     * Gets the value of the html property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getHtml() {
        return html;
    }

    /**
     * Sets the value of the html property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setHtml(byte[] value) {
        this.html = value;
    }

    /**
     * Gets the value of the mht property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getMht() {
        return mht;
    }

    /**
     * Sets the value of the mht property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setMht(byte[] value) {
        this.mht = value;
    }

}
