
package za.co.firstrand.gts.cdm.midlife.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for KeyValue complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="KeyValue"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;all&gt;
 *         &lt;element name="key" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;choice&gt;
 *           &lt;element ref="{http://www.firstrand.co.za/gts/cdm/midlife/v1}value"/&gt;
 *           &lt;element ref="{http://www.firstrand.co.za/gts/cdm/midlife/v1}literal"/&gt;
 *           &lt;element ref="{http://www.firstrand.co.za/gts/cdm/midlife/v1}parameters"/&gt;
 *           &lt;element ref="{http://www.firstrand.co.za/gts/cdm/midlife/v1}literals"/&gt;
 *           &lt;element ref="{http://www.firstrand.co.za/gts/cdm/midlife/v1}parameter"/&gt;
 *         &lt;/choice&gt;
 *       &lt;/all&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "KeyValue", propOrder = {

})
@XmlSeeAlso({
    ParameterV12 .class
})
public class KeyValue {

    @XmlElement(required = true)
    protected String key;
    protected Object value;
    protected String literal;
    protected ParametersV12 parameters;
    protected LiteralsV12 literals;
    protected KeyValue parameter;

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the value property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getValue() {
        return value;
    }

    /**
     * Sets the value of the value property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setValue(Object value) {
        this.value = value;
    }

    /**
     * Gets the value of the literal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getLiteral() {
        return literal;
    }

    /**
     * Sets the value of the literal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setLiteral(String value) {
        this.literal = value;
    }

    /**
     * Gets the value of the parameters property.
     * 
     * @return
     *     possible object is
     *     {@link ParametersV12 }
     *     
     */
    public ParametersV12 getParameters() {
        return parameters;
    }

    /**
     * Sets the value of the parameters property.
     * 
     * @param value
     *     allowed object is
     *     {@link ParametersV12 }
     *     
     */
    public void setParameters(ParametersV12 value) {
        this.parameters = value;
    }

    /**
     * Gets the value of the literals property.
     * 
     * @return
     *     possible object is
     *     {@link LiteralsV12 }
     *     
     */
    public LiteralsV12 getLiterals() {
        return literals;
    }

    /**
     * Sets the value of the literals property.
     * 
     * @param value
     *     allowed object is
     *     {@link LiteralsV12 }
     *     
     */
    public void setLiterals(LiteralsV12 value) {
        this.literals = value;
    }

    /**
     * Gets the value of the parameter property.
     * 
     * @return
     *     possible object is
     *     {@link KeyValue }
     *     
     */
    public KeyValue getParameter() {
        return parameter;
    }

    /**
     * Sets the value of the parameter property.
     * 
     * @param value
     *     allowed object is
     *     {@link KeyValue }
     *     
     */
    public void setParameter(KeyValue value) {
        this.parameter = value;
    }

}
