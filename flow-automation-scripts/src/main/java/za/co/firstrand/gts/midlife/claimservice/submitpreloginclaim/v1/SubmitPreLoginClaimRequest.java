
package za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.midlife.v1.BankingDetails;
import za.co.firstrand.gts.cdm.midlife.v1.ClaimantInformation;
import za.co.firstrand.gts.cdm.midlife.v1.DeceasedInformation;
import za.co.firstrand.gts.cdm.midlife.v1.FuneralParlourDetails;
import za.co.firstrand.gts.cdm.midlife.v1.InformantDetails;
import za.co.firstrand.gts.cdm.midlife.v1.MedicalPractitionerDetails;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for SubmitPreLoginClaimRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SubmitPreLoginClaimRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestChannel" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="policyNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="dateOfClaimSubmission" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="BI1663submitted" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="deceasedInformation" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}DeceasedInformation" form="qualified"/&gt;
 *         &lt;element name="claimantInformation" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}ClaimantInformation" form="qualified"/&gt;
 *         &lt;element name="bankingDetails" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}BankingDetails" form="qualified"/&gt;
 *         &lt;element name="medicalPractitionerDetails" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}MedicalPractitionerDetails" form="qualified"/&gt;
 *         &lt;element name="informantDetails" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}InformantDetails" form="qualified"/&gt;
 *         &lt;element name="funeralParlourDetails" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}FuneralParlourDetails" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubmitPreLoginClaimRequest", propOrder = {
    "requestChannel",
    "policyNumber",
    "dateOfClaimSubmission",
    "bi1663Submitted",
    "deceasedInformation",
    "claimantInformation",
    "bankingDetails",
    "medicalPractitionerDetails",
    "informantDetails",
    "funeralParlourDetails"
})
public class SubmitPreLoginClaimRequest
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String requestChannel;
    protected String policyNumber;
    @XmlElement(required = true)
    protected String dateOfClaimSubmission;
    @XmlElement(name = "BI1663submitted")
    protected String bi1663Submitted;
    @XmlElement(required = true)
    protected DeceasedInformation deceasedInformation;
    @XmlElement(required = true)
    protected ClaimantInformation claimantInformation;
    @XmlElement(required = true)
    protected BankingDetails bankingDetails;
    @XmlElement(required = true)
    protected MedicalPractitionerDetails medicalPractitionerDetails;
    @XmlElement(required = true)
    protected InformantDetails informantDetails;
    @XmlElement(required = true)
    protected FuneralParlourDetails funeralParlourDetails;

    /**
     * Gets the value of the requestChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestChannel() {
        return requestChannel;
    }

    /**
     * Sets the value of the requestChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestChannel(String value) {
        this.requestChannel = value;
    }

    /**
     * Gets the value of the policyNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPolicyNumber() {
        return policyNumber;
    }

    /**
     * Sets the value of the policyNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPolicyNumber(String value) {
        this.policyNumber = value;
    }

    /**
     * Gets the value of the dateOfClaimSubmission property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateOfClaimSubmission() {
        return dateOfClaimSubmission;
    }

    /**
     * Sets the value of the dateOfClaimSubmission property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateOfClaimSubmission(String value) {
        this.dateOfClaimSubmission = value;
    }

    /**
     * Gets the value of the bi1663Submitted property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBI1663Submitted() {
        return bi1663Submitted;
    }

    /**
     * Sets the value of the bi1663Submitted property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBI1663Submitted(String value) {
        this.bi1663Submitted = value;
    }

    /**
     * Gets the value of the deceasedInformation property.
     * 
     * @return
     *     possible object is
     *     {@link DeceasedInformation }
     *     
     */
    public DeceasedInformation getDeceasedInformation() {
        return deceasedInformation;
    }

    /**
     * Sets the value of the deceasedInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeceasedInformation }
     *     
     */
    public void setDeceasedInformation(DeceasedInformation value) {
        this.deceasedInformation = value;
    }

    /**
     * Gets the value of the claimantInformation property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimantInformation }
     *     
     */
    public ClaimantInformation getClaimantInformation() {
        return claimantInformation;
    }

    /**
     * Sets the value of the claimantInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimantInformation }
     *     
     */
    public void setClaimantInformation(ClaimantInformation value) {
        this.claimantInformation = value;
    }

    /**
     * Gets the value of the bankingDetails property.
     * 
     * @return
     *     possible object is
     *     {@link BankingDetails }
     *     
     */
    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    /**
     * Sets the value of the bankingDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link BankingDetails }
     *     
     */
    public void setBankingDetails(BankingDetails value) {
        this.bankingDetails = value;
    }

    /**
     * Gets the value of the medicalPractitionerDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MedicalPractitionerDetails }
     *     
     */
    public MedicalPractitionerDetails getMedicalPractitionerDetails() {
        return medicalPractitionerDetails;
    }

    /**
     * Sets the value of the medicalPractitionerDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MedicalPractitionerDetails }
     *     
     */
    public void setMedicalPractitionerDetails(MedicalPractitionerDetails value) {
        this.medicalPractitionerDetails = value;
    }

    /**
     * Gets the value of the informantDetails property.
     * 
     * @return
     *     possible object is
     *     {@link InformantDetails }
     *     
     */
    public InformantDetails getInformantDetails() {
        return informantDetails;
    }

    /**
     * Sets the value of the informantDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link InformantDetails }
     *     
     */
    public void setInformantDetails(InformantDetails value) {
        this.informantDetails = value;
    }

    /**
     * Gets the value of the funeralParlourDetails property.
     * 
     * @return
     *     possible object is
     *     {@link FuneralParlourDetails }
     *     
     */
    public FuneralParlourDetails getFuneralParlourDetails() {
        return funeralParlourDetails;
    }

    /**
     * Sets the value of the funeralParlourDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link FuneralParlourDetails }
     *     
     */
    public void setFuneralParlourDetails(FuneralParlourDetails value) {
        this.funeralParlourDetails = value;
    }

}
