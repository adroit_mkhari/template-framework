
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for selectV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="selectV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="selectOptions" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}selectOptionsV1.2"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="minOption" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="maxOption" type="{http://www.w3.org/2001/XMLSchema}integer" /&gt;
 *       &lt;attribute name="title" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="default" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="placeholder" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="inlineError" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="target" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="action" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *       &lt;attribute name="url" type="{http://www.w3.org/2001/XMLSchema}anyURI" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "selectV1.2", propOrder = {
    "selectOptions"
})
public class SelectV12 {

    @XmlElement(required = true)
    protected SelectOptionsV12 selectOptions;
    @XmlAttribute(name = "key")
    protected String key;
    @XmlAttribute(name = "minOption")
    protected BigInteger minOption;
    @XmlAttribute(name = "maxOption")
    protected BigInteger maxOption;
    @XmlAttribute(name = "title")
    protected String title;
    @XmlAttribute(name = "default")
    protected String _default;
    @XmlAttribute(name = "placeholder")
    protected String placeholder;
    @XmlAttribute(name = "inlineError")
    protected String inlineError;
    @XmlAttribute(name = "target")
    protected String target;
    @XmlAttribute(name = "action")
    protected String action;
    @XmlAttribute(name = "url")
    @XmlSchemaType(name = "anyURI")
    protected String url;

    /**
     * Gets the value of the selectOptions property.
     * 
     * @return
     *     possible object is
     *     {@link SelectOptionsV12 }
     *     
     */
    public SelectOptionsV12 getSelectOptions() {
        return selectOptions;
    }

    /**
     * Sets the value of the selectOptions property.
     * 
     * @param value
     *     allowed object is
     *     {@link SelectOptionsV12 }
     *     
     */
    public void setSelectOptions(SelectOptionsV12 value) {
        this.selectOptions = value;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

    /**
     * Gets the value of the minOption property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMinOption() {
        return minOption;
    }

    /**
     * Sets the value of the minOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMinOption(BigInteger value) {
        this.minOption = value;
    }

    /**
     * Gets the value of the maxOption property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMaxOption() {
        return maxOption;
    }

    /**
     * Sets the value of the maxOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMaxOption(BigInteger value) {
        this.maxOption = value;
    }

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the default property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefault() {
        return _default;
    }

    /**
     * Sets the value of the default property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefault(String value) {
        this._default = value;
    }

    /**
     * Gets the value of the placeholder property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceholder() {
        return placeholder;
    }

    /**
     * Sets the value of the placeholder property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceholder(String value) {
        this.placeholder = value;
    }

    /**
     * Gets the value of the inlineError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInlineError() {
        return inlineError;
    }

    /**
     * Sets the value of the inlineError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInlineError(String value) {
        this.inlineError = value;
    }

    /**
     * Gets the value of the target property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTarget() {
        return target;
    }

    /**
     * Sets the value of the target property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTarget(String value) {
        this.target = value;
    }

    /**
     * Gets the value of the action property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAction() {
        return action;
    }

    /**
     * Sets the value of the action property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAction(String value) {
        this.action = value;
    }

    /**
     * Gets the value of the url property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the value of the url property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUrl(String value) {
        this.url = value;
    }

}
