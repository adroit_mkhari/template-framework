
package za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SubmitPreLoginClaimRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/ClaimService/SubmitPreLoginClaim/v1", "SubmitPreLoginClaimRequest");
    private final static QName _SubmitPreLoginClaimResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/ClaimService/SubmitPreLoginClaim/v1", "SubmitPreLoginClaimResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SubmitPreLoginClaimRequest }
     * 
     */
    public SubmitPreLoginClaimRequest createSubmitPreLoginClaimRequest() {
        return new SubmitPreLoginClaimRequest();
    }

    /**
     * Create an instance of {@link SubmitPreLoginClaimResponse }
     * 
     */
    public SubmitPreLoginClaimResponse createSubmitPreLoginClaimResponse() {
        return new SubmitPreLoginClaimResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/ClaimService/SubmitPreLoginClaim/v1", name = "SubmitPreLoginClaimRequest")
    public JAXBElement<SubmitPreLoginClaimRequest> createSubmitPreLoginClaimRequest(SubmitPreLoginClaimRequest value) {
        return new JAXBElement<SubmitPreLoginClaimRequest>(_SubmitPreLoginClaimRequest_QNAME, SubmitPreLoginClaimRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SubmitPreLoginClaimResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/ClaimService/SubmitPreLoginClaim/v1", name = "SubmitPreLoginClaimResponse")
    public JAXBElement<SubmitPreLoginClaimResponse> createSubmitPreLoginClaimResponse(SubmitPreLoginClaimResponse value) {
        return new JAXBElement<SubmitPreLoginClaimResponse>(_SubmitPreLoginClaimResponse_QNAME, SubmitPreLoginClaimResponse.class, null, value);
    }

}
