
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Partyv12 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Partyv12"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="person" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Personv12" minOccurs="0"/&gt;
 *         &lt;element name="organisation" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Organisationv12" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Partyv12", propOrder = {
    "person",
    "organisation"
})
@XmlSeeAlso({
    za.co.firstrand.gts.cdm.customer.v12.Customer2 .Party.class
})
public class Partyv12 {

    protected Personv12 person;
    protected Organisationv12 organisation;

    /**
     * Gets the value of the person property.
     * 
     * @return
     *     possible object is
     *     {@link Personv12 }
     *     
     */
    public Personv12 getPerson() {
        return person;
    }

    /**
     * Sets the value of the person property.
     * 
     * @param value
     *     allowed object is
     *     {@link Personv12 }
     *     
     */
    public void setPerson(Personv12 value) {
        this.person = value;
    }

    /**
     * Gets the value of the organisation property.
     * 
     * @return
     *     possible object is
     *     {@link Organisationv12 }
     *     
     */
    public Organisationv12 getOrganisation() {
        return organisation;
    }

    /**
     * Sets the value of the organisation property.
     * 
     * @param value
     *     allowed object is
     *     {@link Organisationv12 }
     *     
     */
    public void setOrganisation(Organisationv12 value) {
        this.organisation = value;
    }

}
