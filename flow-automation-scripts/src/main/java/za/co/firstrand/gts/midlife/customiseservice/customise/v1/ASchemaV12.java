
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for aSchemaV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="aSchemaV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="formList" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}listOfformsV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "aSchemaV1.2", propOrder = {
    "formList"
})
public class ASchemaV12 {

    @XmlElement(required = true)
    protected List<ListOfformsV12> formList;

    /**
     * Gets the value of the formList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfformsV12 }
     * 
     * 
     */
    public List<ListOfformsV12> getFormList() {
        if (formList == null) {
            formList = new ArrayList<ListOfformsV12>();
        }
        return this.formList;
    }

}
