
package za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for SearchType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="byCustomerKey" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/ListAccounts/v3}ByCustomerKey" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byAccount" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/ListAccounts/v3}ByAccount" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byCustomerIdentification" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/ListAccounts/v3}ByCustomerIdentification" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchType", propOrder = {
    "byCustomerKey",
    "byAccount",
    "byCustomerIdentification"
})
public class SearchType {

    protected ByCustomerKey byCustomerKey;
    protected ByAccount byAccount;
    protected ByCustomerIdentification byCustomerIdentification;

    /**
     * Gets the value of the byCustomerKey property.
     * 
     * @return
     *     possible object is
     *     {@link ByCustomerKey }
     *     
     */
    public ByCustomerKey getByCustomerKey() {
        return byCustomerKey;
    }

    /**
     * Sets the value of the byCustomerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByCustomerKey }
     *     
     */
    public void setByCustomerKey(ByCustomerKey value) {
        this.byCustomerKey = value;
    }

    /**
     * Gets the value of the byAccount property.
     * 
     * @return
     *     possible object is
     *     {@link ByAccount }
     *     
     */
    public ByAccount getByAccount() {
        return byAccount;
    }

    /**
     * Sets the value of the byAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByAccount }
     *     
     */
    public void setByAccount(ByAccount value) {
        this.byAccount = value;
    }

    /**
     * Gets the value of the byCustomerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link ByCustomerIdentification }
     *     
     */
    public ByCustomerIdentification getByCustomerIdentification() {
        return byCustomerIdentification;
    }

    /**
     * Sets the value of the byCustomerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByCustomerIdentification }
     *     
     */
    public void setByCustomerIdentification(ByCustomerIdentification value) {
        this.byCustomerIdentification = value;
    }

}
