
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for tableV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="tableV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="rowItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}tableRowV1.2" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "tableV1.2", propOrder = {
    "rowItem"
})
public class TableV12 {

    @XmlElement(required = true)
    protected List<TableRowV12> rowItem;

    /**
     * Gets the value of the rowItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rowItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRowItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TableRowV12 }
     * 
     * 
     */
    public List<TableRowV12> getRowItem() {
        if (rowItem == null) {
            rowItem = new ArrayList<TableRowV12>();
        }
        return this.rowItem;
    }

}
