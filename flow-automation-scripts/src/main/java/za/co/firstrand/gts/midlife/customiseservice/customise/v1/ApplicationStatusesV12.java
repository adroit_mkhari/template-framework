
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for applicationStatusesV1.2.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="applicationStatusesV1.2"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="inProgress"/&gt;
 *     &lt;enumeration value="referred"/&gt;
 *     &lt;enumeration value="cancelled"/&gt;
 *     &lt;enumeration value="callMeBack"/&gt;
 *     &lt;enumeration value="complete"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "applicationStatusesV1.2")
@XmlEnum
public enum ApplicationStatusesV12 {

    @XmlEnumValue("inProgress")
    IN_PROGRESS("inProgress"),
    @XmlEnumValue("referred")
    REFERRED("referred"),
    @XmlEnumValue("cancelled")
    CANCELLED("cancelled"),
    @XmlEnumValue("callMeBack")
    CALL_ME_BACK("callMeBack"),
    @XmlEnumValue("complete")
    COMPLETE("complete");
    private final String value;

    ApplicationStatusesV12(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ApplicationStatusesV12 fromValue(String v) {
        for (ApplicationStatusesV12 c: ApplicationStatusesV12 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
