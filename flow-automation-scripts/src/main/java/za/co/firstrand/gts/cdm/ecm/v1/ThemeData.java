
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for themeData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="themeData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="userDefinedRef" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="userDefinedRefType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="recordName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="recordStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="recordStatusReason" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="recordExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="businessProcess" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="vaultGenerateData" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}vaultGenerateData" minOccurs="0"/&gt;
 *         &lt;element name="internalGenerateData" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}internalGenerateData" minOccurs="0"/&gt;
 *         &lt;element name="custGenerateData" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}custGenerateData" minOccurs="0"/&gt;
 *         &lt;element name="procGenerateData" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}procGenerateData" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "themeData", propOrder = {
    "userDefinedRef",
    "userDefinedRefType",
    "recordName",
    "recordStatus",
    "recordStatusReason",
    "recordExpiryDate",
    "businessProcess",
    "vaultGenerateData",
    "internalGenerateData",
    "custGenerateData",
    "procGenerateData"
})
public class ThemeData {

    protected String userDefinedRef;
    protected String userDefinedRefType;
    protected String recordName;
    protected String recordStatus;
    protected String recordStatusReason;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar recordExpiryDate;
    protected String businessProcess;
    protected VaultGenerateData vaultGenerateData;
    protected InternalGenerateData internalGenerateData;
    protected CustGenerateData custGenerateData;
    protected ProcGenerateData procGenerateData;

    /**
     * Gets the value of the userDefinedRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefinedRef() {
        return userDefinedRef;
    }

    /**
     * Sets the value of the userDefinedRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefinedRef(String value) {
        this.userDefinedRef = value;
    }

    /**
     * Gets the value of the userDefinedRefType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserDefinedRefType() {
        return userDefinedRefType;
    }

    /**
     * Sets the value of the userDefinedRefType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserDefinedRefType(String value) {
        this.userDefinedRefType = value;
    }

    /**
     * Gets the value of the recordName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordName() {
        return recordName;
    }

    /**
     * Sets the value of the recordName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordName(String value) {
        this.recordName = value;
    }

    /**
     * Gets the value of the recordStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordStatus() {
        return recordStatus;
    }

    /**
     * Sets the value of the recordStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordStatus(String value) {
        this.recordStatus = value;
    }

    /**
     * Gets the value of the recordStatusReason property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRecordStatusReason() {
        return recordStatusReason;
    }

    /**
     * Sets the value of the recordStatusReason property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRecordStatusReason(String value) {
        this.recordStatusReason = value;
    }

    /**
     * Gets the value of the recordExpiryDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getRecordExpiryDate() {
        return recordExpiryDate;
    }

    /**
     * Sets the value of the recordExpiryDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setRecordExpiryDate(XMLGregorianCalendar value) {
        this.recordExpiryDate = value;
    }

    /**
     * Gets the value of the businessProcess property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBusinessProcess() {
        return businessProcess;
    }

    /**
     * Sets the value of the businessProcess property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBusinessProcess(String value) {
        this.businessProcess = value;
    }

    /**
     * Gets the value of the vaultGenerateData property.
     * 
     * @return
     *     possible object is
     *     {@link VaultGenerateData }
     *     
     */
    public VaultGenerateData getVaultGenerateData() {
        return vaultGenerateData;
    }

    /**
     * Sets the value of the vaultGenerateData property.
     * 
     * @param value
     *     allowed object is
     *     {@link VaultGenerateData }
     *     
     */
    public void setVaultGenerateData(VaultGenerateData value) {
        this.vaultGenerateData = value;
    }

    /**
     * Gets the value of the internalGenerateData property.
     * 
     * @return
     *     possible object is
     *     {@link InternalGenerateData }
     *     
     */
    public InternalGenerateData getInternalGenerateData() {
        return internalGenerateData;
    }

    /**
     * Sets the value of the internalGenerateData property.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalGenerateData }
     *     
     */
    public void setInternalGenerateData(InternalGenerateData value) {
        this.internalGenerateData = value;
    }

    /**
     * Gets the value of the custGenerateData property.
     * 
     * @return
     *     possible object is
     *     {@link CustGenerateData }
     *     
     */
    public CustGenerateData getCustGenerateData() {
        return custGenerateData;
    }

    /**
     * Sets the value of the custGenerateData property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustGenerateData }
     *     
     */
    public void setCustGenerateData(CustGenerateData value) {
        this.custGenerateData = value;
    }

    /**
     * Gets the value of the procGenerateData property.
     * 
     * @return
     *     possible object is
     *     {@link ProcGenerateData }
     *     
     */
    public ProcGenerateData getProcGenerateData() {
        return procGenerateData;
    }

    /**
     * Sets the value of the procGenerateData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ProcGenerateData }
     *     
     */
    public void setProcGenerateData(ProcGenerateData value) {
        this.procGenerateData = value;
    }

}
