
package za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _MaintainPolicyDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/MaintainPolicyDetails/v1", "MaintainPolicyDetailsRequest");
    private final static QName _MaintainPolicyDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/MaintainPolicyDetails/v1", "MaintainPolicyDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link MaintainPolicyDetailsRequest }
     * 
     */
    public MaintainPolicyDetailsRequest createMaintainPolicyDetailsRequest() {
        return new MaintainPolicyDetailsRequest();
    }

    /**
     * Create an instance of {@link MaintainPolicyDetailsResponse }
     * 
     */
    public MaintainPolicyDetailsResponse createMaintainPolicyDetailsResponse() {
        return new MaintainPolicyDetailsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/MaintainPolicyDetails/v1", name = "MaintainPolicyDetailsRequest")
    public JAXBElement<MaintainPolicyDetailsRequest> createMaintainPolicyDetailsRequest(MaintainPolicyDetailsRequest value) {
        return new JAXBElement<MaintainPolicyDetailsRequest>(_MaintainPolicyDetailsRequest_QNAME, MaintainPolicyDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link MaintainPolicyDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/MaintainPolicyDetails/v1", name = "MaintainPolicyDetailsResponse")
    public JAXBElement<MaintainPolicyDetailsResponse> createMaintainPolicyDetailsResponse(MaintainPolicyDetailsResponse value) {
        return new JAXBElement<MaintainPolicyDetailsResponse>(_MaintainPolicyDetailsResponse_QNAME, MaintainPolicyDetailsResponse.class, null, value);
    }

}
