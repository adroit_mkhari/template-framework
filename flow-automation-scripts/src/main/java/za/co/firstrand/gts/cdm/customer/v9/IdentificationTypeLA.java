
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationTypeLA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IdentificationTypeLA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="CUSTOMER_NUMBER"/&gt;
 *     &lt;enumeration value="COMPANY_REGISTRATION_NUMBER"/&gt;
 *     &lt;enumeration value="IDENTIFICATION_DOCUMENT"/&gt;
 *     &lt;enumeration value="TAX_REFERENCE_NUMBER"/&gt;
 *     &lt;enumeration value="VAT_REGISTRATION_NUMBER"/&gt;
 *     &lt;enumeration value="PASSPORT"/&gt;
 *     &lt;enumeration value="TEMPORARY_RESIDENCE_PERMIT_NUMBER"/&gt;
 *     &lt;enumeration value="CLIENT_CUSTOMER_NUMBER"/&gt;
 *     &lt;enumeration value="REGISTRATION_CARD"/&gt;
 *     &lt;enumeration value="REFERENCE_BOOK"/&gt;
 *     &lt;enumeration value="CC_REGISTRATION_NUMBER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IdentificationTypeLA")
@XmlEnum
public enum IdentificationTypeLA {

    CUSTOMER_NUMBER,
    COMPANY_REGISTRATION_NUMBER,
    IDENTIFICATION_DOCUMENT,
    TAX_REFERENCE_NUMBER,
    VAT_REGISTRATION_NUMBER,
    PASSPORT,
    TEMPORARY_RESIDENCE_PERMIT_NUMBER,
    CLIENT_CUSTOMER_NUMBER,
    REGISTRATION_CARD,
    REFERENCE_BOOK,
    CC_REGISTRATION_NUMBER;

    public String value() {
        return name();
    }

    public static IdentificationTypeLA fromValue(String v) {
        return valueOf(v);
    }

}
