
package za.co.firstrand.gts.cdm.elementschema.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for requiredElementV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="requiredElementV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="elementKey" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="inlineError" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "requiredElementV1.2", propOrder = {
    "elementKey",
    "inlineError"
})
public class RequiredElementV12 {

    @XmlElement(required = true)
    protected String elementKey;
    @XmlElement(required = true)
    protected String inlineError;

    /**
     * Gets the value of the elementKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getElementKey() {
        return elementKey;
    }

    /**
     * Sets the value of the elementKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setElementKey(String value) {
        this.elementKey = value;
    }

    /**
     * Gets the value of the inlineError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInlineError() {
        return inlineError;
    }

    /**
     * Sets the value of the inlineError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInlineError(String value) {
        this.inlineError = value;
    }

}
