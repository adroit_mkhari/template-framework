
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.midlife.customiseservice.customise.v1.CustomiseRequest;
import za.co.firstrand.gts.midlife.customiseservice.customise.v1.CustomiseResponse;


/**
 * <p>Java class for EnterpriseMessageV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnterpriseMessageV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageHeaderV1.2"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseMessageV1.2", propOrder = {
    "header"
})
@XmlSeeAlso({
    CustomiseResponse.class,
    CustomiseRequest.class
})
public class EnterpriseMessageV12 {

    @XmlElement(required = true)
    protected EnterpriseMessageHeaderV12 header;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseMessageHeaderV12 }
     *     
     */
    public EnterpriseMessageHeaderV12 getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseMessageHeaderV12 }
     *     
     */
    public void setHeader(EnterpriseMessageHeaderV12 value) {
        this.header = value;
    }

}
