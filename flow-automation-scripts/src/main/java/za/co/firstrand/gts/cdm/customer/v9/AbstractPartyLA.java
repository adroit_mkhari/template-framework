
package za.co.firstrand.gts.cdm.customer.v9;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import za.co.firstrand.gts.cdm.datatypes.v4.AddressLA;


/**
 * <p>Java class for AbstractPartyLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AbstractPartyLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="postalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v4}AddressLA" minOccurs="0"/&gt;
 *         &lt;element name="physicalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v4}AddressLA" minOccurs="0"/&gt;
 *         &lt;element name="addressVerified" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="kycVerified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="kycDateVerified" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="contactDetail" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}ContactDetailsLA" minOccurs="0"/&gt;
 *         &lt;element name="marketing" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}MarketingLA" minOccurs="0"/&gt;
 *         &lt;element name="identification" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}IdentificationLA" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="VIPIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxIndicator" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="currentVSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="targetVSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="domicileBranch" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="officerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="operatorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sourceOfFunds" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}SourceOfFundsLA" maxOccurs="unbounded"/&gt;
 *         &lt;element name="idVerified" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AbstractPartyLA", propOrder = {
    "postalAddress",
    "physicalAddress",
    "addressVerified",
    "kycVerified",
    "kycDateVerified",
    "contactDetail",
    "marketing",
    "identification",
    "vipIndicator",
    "foreignTaxIndicator",
    "currentVSI",
    "targetVSI",
    "domicileBranch",
    "officerCode",
    "operatorID",
    "sourceOfFunds",
    "idVerified"
})
@XmlSeeAlso({
    PersonLA.class,
    OrganisationLA.class
})
public abstract class AbstractPartyLA {

    protected AddressLA postalAddress;
    protected AddressLA physicalAddress;
    protected boolean addressVerified;
    protected String kycVerified;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar kycDateVerified;
    protected ContactDetailsLA contactDetail;
    protected MarketingLA marketing;
    @XmlElement(nillable = true)
    protected List<IdentificationLA> identification;
    @XmlElement(name = "VIPIndicator")
    protected String vipIndicator;
    protected BigInteger foreignTaxIndicator;
    protected String currentVSI;
    protected String targetVSI;
    protected BigInteger domicileBranch;
    protected String officerCode;
    protected String operatorID;
    @XmlElement(required = true)
    protected List<SourceOfFundsLA> sourceOfFunds;
    protected Boolean idVerified;

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLA }
     *     
     */
    public AddressLA getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLA }
     *     
     */
    public void setPostalAddress(AddressLA value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the physicalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link AddressLA }
     *     
     */
    public AddressLA getPhysicalAddress() {
        return physicalAddress;
    }

    /**
     * Sets the value of the physicalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddressLA }
     *     
     */
    public void setPhysicalAddress(AddressLA value) {
        this.physicalAddress = value;
    }

    /**
     * Gets the value of the addressVerified property.
     * 
     */
    public boolean isAddressVerified() {
        return addressVerified;
    }

    /**
     * Sets the value of the addressVerified property.
     * 
     */
    public void setAddressVerified(boolean value) {
        this.addressVerified = value;
    }

    /**
     * Gets the value of the kycVerified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKycVerified() {
        return kycVerified;
    }

    /**
     * Sets the value of the kycVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKycVerified(String value) {
        this.kycVerified = value;
    }

    /**
     * Gets the value of the kycDateVerified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getKycDateVerified() {
        return kycDateVerified;
    }

    /**
     * Sets the value of the kycDateVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setKycDateVerified(XMLGregorianCalendar value) {
        this.kycDateVerified = value;
    }

    /**
     * Gets the value of the contactDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ContactDetailsLA }
     *     
     */
    public ContactDetailsLA getContactDetail() {
        return contactDetail;
    }

    /**
     * Sets the value of the contactDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDetailsLA }
     *     
     */
    public void setContactDetail(ContactDetailsLA value) {
        this.contactDetail = value;
    }

    /**
     * Gets the value of the marketing property.
     * 
     * @return
     *     possible object is
     *     {@link MarketingLA }
     *     
     */
    public MarketingLA getMarketing() {
        return marketing;
    }

    /**
     * Sets the value of the marketing property.
     * 
     * @param value
     *     allowed object is
     *     {@link MarketingLA }
     *     
     */
    public void setMarketing(MarketingLA value) {
        this.marketing = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link IdentificationLA }
     * 
     * 
     */
    public List<IdentificationLA> getIdentification() {
        if (identification == null) {
            identification = new ArrayList<IdentificationLA>();
        }
        return this.identification;
    }

    /**
     * Gets the value of the vipIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVIPIndicator() {
        return vipIndicator;
    }

    /**
     * Sets the value of the vipIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVIPIndicator(String value) {
        this.vipIndicator = value;
    }

    /**
     * Gets the value of the foreignTaxIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getForeignTaxIndicator() {
        return foreignTaxIndicator;
    }

    /**
     * Sets the value of the foreignTaxIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setForeignTaxIndicator(BigInteger value) {
        this.foreignTaxIndicator = value;
    }

    /**
     * Gets the value of the currentVSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentVSI() {
        return currentVSI;
    }

    /**
     * Sets the value of the currentVSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentVSI(String value) {
        this.currentVSI = value;
    }

    /**
     * Gets the value of the targetVSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetVSI() {
        return targetVSI;
    }

    /**
     * Sets the value of the targetVSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetVSI(String value) {
        this.targetVSI = value;
    }

    /**
     * Gets the value of the domicileBranch property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDomicileBranch() {
        return domicileBranch;
    }

    /**
     * Sets the value of the domicileBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDomicileBranch(BigInteger value) {
        this.domicileBranch = value;
    }

    /**
     * Gets the value of the officerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerCode() {
        return officerCode;
    }

    /**
     * Sets the value of the officerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerCode(String value) {
        this.officerCode = value;
    }

    /**
     * Gets the value of the operatorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorID() {
        return operatorID;
    }

    /**
     * Sets the value of the operatorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorID(String value) {
        this.operatorID = value;
    }

    /**
     * Gets the value of the sourceOfFunds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sourceOfFunds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSourceOfFunds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SourceOfFundsLA }
     * 
     * 
     */
    public List<SourceOfFundsLA> getSourceOfFunds() {
        if (sourceOfFunds == null) {
            sourceOfFunds = new ArrayList<SourceOfFundsLA>();
        }
        return this.sourceOfFunds;
    }

    /**
     * Gets the value of the idVerified property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isIdVerified() {
        return idVerified;
    }

    /**
     * Sets the value of the idVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setIdVerified(Boolean value) {
        this.idVerified = value;
    }

}
