
package za.co.firstrand.gts.cdm.elementschema.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.elementschema.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.elementschema.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link LinkV12 }
     * 
     */
    public LinkV12 createLinkV12() {
        return new LinkV12();
    }

    /**
     * Create an instance of {@link RequiredElementV12 }
     * 
     */
    public RequiredElementV12 createRequiredElementV12() {
        return new RequiredElementV12();
    }

    /**
     * Create an instance of {@link DateV12 }
     * 
     */
    public DateV12 createDateV12() {
        return new DateV12();
    }

    /**
     * Create an instance of {@link BlobV12 }
     * 
     */
    public BlobV12 createBlobV12() {
        return new BlobV12();
    }

    /**
     * Create an instance of {@link TableV12 }
     * 
     */
    public TableV12 createTableV12() {
        return new TableV12();
    }

    /**
     * Create an instance of {@link TableRowV12 }
     * 
     */
    public TableRowV12 createTableRowV12() {
        return new TableRowV12();
    }

    /**
     * Create an instance of {@link TableHeaderV12 }
     * 
     */
    public TableHeaderV12 createTableHeaderV12() {
        return new TableHeaderV12();
    }

    /**
     * Create an instance of {@link SelectV12 }
     * 
     */
    public SelectV12 createSelectV12() {
        return new SelectV12();
    }

    /**
     * Create an instance of {@link SelectOptionsV12 }
     * 
     */
    public SelectOptionsV12 createSelectOptionsV12() {
        return new SelectOptionsV12();
    }

    /**
     * Create an instance of {@link TextV12 }
     * 
     */
    public TextV12 createTextV12() {
        return new TextV12();
    }

    /**
     * Create an instance of {@link AllElementsV12 }
     * 
     */
    public AllElementsV12 createAllElementsV12() {
        return new AllElementsV12();
    }

    /**
     * Create an instance of {@link InputV12 }
     * 
     */
    public InputV12 createInputV12() {
        return new InputV12();
    }

    /**
     * Create an instance of {@link ListOfElementsV12 }
     * 
     */
    public ListOfElementsV12 createListOfElementsV12() {
        return new ListOfElementsV12();
    }

    /**
     * Create an instance of {@link AbstractElementV12 }
     * 
     */
    public AbstractElementV12 createAbstractElementV12() {
        return new AbstractElementV12();
    }

}
