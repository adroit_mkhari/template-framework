
package za.co.firstrand.gts.cdm.customer.v12;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for ForeignTaxv12 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ForeignTaxv12"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="foreignTaxLiabilityIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxCityOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxIDCounter" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="foreignTaxIDSegment" maxOccurs="unbounded" minOccurs="0"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="foreignTaxIDKey" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="foreignTaxIDData" minOccurs="0"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                             &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ForeignTaxv12", propOrder = {
    "foreignTaxLiabilityIndicator",
    "foreignTaxCityOfBirth",
    "foreignTaxIDCounter",
    "foreignTaxIDSegment"
})
public class ForeignTaxv12 {

    protected String foreignTaxLiabilityIndicator;
    protected String foreignTaxCityOfBirth;
    protected BigInteger foreignTaxIDCounter;
    @XmlElement(nillable = true)
    protected List<ForeignTaxv12 .ForeignTaxIDSegment> foreignTaxIDSegment;

    /**
     * Gets the value of the foreignTaxLiabilityIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTaxLiabilityIndicator() {
        return foreignTaxLiabilityIndicator;
    }

    /**
     * Sets the value of the foreignTaxLiabilityIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTaxLiabilityIndicator(String value) {
        this.foreignTaxLiabilityIndicator = value;
    }

    /**
     * Gets the value of the foreignTaxCityOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getForeignTaxCityOfBirth() {
        return foreignTaxCityOfBirth;
    }

    /**
     * Sets the value of the foreignTaxCityOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setForeignTaxCityOfBirth(String value) {
        this.foreignTaxCityOfBirth = value;
    }

    /**
     * Gets the value of the foreignTaxIDCounter property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getForeignTaxIDCounter() {
        return foreignTaxIDCounter;
    }

    /**
     * Sets the value of the foreignTaxIDCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setForeignTaxIDCounter(BigInteger value) {
        this.foreignTaxIDCounter = value;
    }

    /**
     * Gets the value of the foreignTaxIDSegment property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the foreignTaxIDSegment property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getForeignTaxIDSegment().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ForeignTaxv12 .ForeignTaxIDSegment }
     * 
     * 
     */
    public List<ForeignTaxv12 .ForeignTaxIDSegment> getForeignTaxIDSegment() {
        if (foreignTaxIDSegment == null) {
            foreignTaxIDSegment = new ArrayList<ForeignTaxv12 .ForeignTaxIDSegment>();
        }
        return this.foreignTaxIDSegment;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="foreignTaxIDKey" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="foreignTaxIDData" minOccurs="0"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                   &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "foreignTaxIDKey",
        "foreignTaxIDData"
    })
    public static class ForeignTaxIDSegment {

        protected ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey foreignTaxIDKey;
        protected ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData foreignTaxIDData;

        /**
         * Gets the value of the foreignTaxIDKey property.
         * 
         * @return
         *     possible object is
         *     {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey }
         *     
         */
        public ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey getForeignTaxIDKey() {
            return foreignTaxIDKey;
        }

        /**
         * Sets the value of the foreignTaxIDKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey }
         *     
         */
        public void setForeignTaxIDKey(ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDKey value) {
            this.foreignTaxIDKey = value;
        }

        /**
         * Gets the value of the foreignTaxIDData property.
         * 
         * @return
         *     possible object is
         *     {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData }
         *     
         */
        public ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData getForeignTaxIDData() {
            return foreignTaxIDData;
        }

        /**
         * Sets the value of the foreignTaxIDData property.
         * 
         * @param value
         *     allowed object is
         *     {@link ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData }
         *     
         */
        public void setForeignTaxIDData(ForeignTaxv12 .ForeignTaxIDSegment.ForeignTaxIDData value) {
            this.foreignTaxIDData = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="foreignTaxIDIssuer" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDExpiryDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDClassification" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "foreignTaxIDIssuer",
            "foreignTaxIDExpiryDate",
            "foreignTaxIDDescription",
            "foreignTaxIDClassification",
            "foreignTaxIDStatus"
        })
        public static class ForeignTaxIDData {

            protected String foreignTaxIDIssuer;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar foreignTaxIDExpiryDate;
            protected String foreignTaxIDDescription;
            protected String foreignTaxIDClassification;
            protected String foreignTaxIDStatus;

            /**
             * Gets the value of the foreignTaxIDIssuer property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDIssuer() {
                return foreignTaxIDIssuer;
            }

            /**
             * Sets the value of the foreignTaxIDIssuer property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDIssuer(String value) {
                this.foreignTaxIDIssuer = value;
            }

            /**
             * Gets the value of the foreignTaxIDExpiryDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getForeignTaxIDExpiryDate() {
                return foreignTaxIDExpiryDate;
            }

            /**
             * Sets the value of the foreignTaxIDExpiryDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setForeignTaxIDExpiryDate(XMLGregorianCalendar value) {
                this.foreignTaxIDExpiryDate = value;
            }

            /**
             * Gets the value of the foreignTaxIDDescription property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDDescription() {
                return foreignTaxIDDescription;
            }

            /**
             * Sets the value of the foreignTaxIDDescription property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDDescription(String value) {
                this.foreignTaxIDDescription = value;
            }

            /**
             * Gets the value of the foreignTaxIDClassification property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDClassification() {
                return foreignTaxIDClassification;
            }

            /**
             * Sets the value of the foreignTaxIDClassification property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDClassification(String value) {
                this.foreignTaxIDClassification = value;
            }

            /**
             * Gets the value of the foreignTaxIDStatus property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDStatus() {
                return foreignTaxIDStatus;
            }

            /**
             * Sets the value of the foreignTaxIDStatus property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDStatus(String value) {
                this.foreignTaxIDStatus = value;
            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="foreignTaxIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDOccurrence" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
         *         &lt;element name="foreignTaxIDEffectiveDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "foreignTaxIDType",
            "foreignTaxIDOccurrence",
            "foreignTaxIDEffectiveDate"
        })
        public static class ForeignTaxIDKey {

            protected String foreignTaxIDType;
            protected BigInteger foreignTaxIDOccurrence;
            @XmlSchemaType(name = "date")
            protected XMLGregorianCalendar foreignTaxIDEffectiveDate;

            /**
             * Gets the value of the foreignTaxIDType property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getForeignTaxIDType() {
                return foreignTaxIDType;
            }

            /**
             * Sets the value of the foreignTaxIDType property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setForeignTaxIDType(String value) {
                this.foreignTaxIDType = value;
            }

            /**
             * Gets the value of the foreignTaxIDOccurrence property.
             * 
             * @return
             *     possible object is
             *     {@link BigInteger }
             *     
             */
            public BigInteger getForeignTaxIDOccurrence() {
                return foreignTaxIDOccurrence;
            }

            /**
             * Sets the value of the foreignTaxIDOccurrence property.
             * 
             * @param value
             *     allowed object is
             *     {@link BigInteger }
             *     
             */
            public void setForeignTaxIDOccurrence(BigInteger value) {
                this.foreignTaxIDOccurrence = value;
            }

            /**
             * Gets the value of the foreignTaxIDEffectiveDate property.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public XMLGregorianCalendar getForeignTaxIDEffectiveDate() {
                return foreignTaxIDEffectiveDate;
            }

            /**
             * Sets the value of the foreignTaxIDEffectiveDate property.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setForeignTaxIDEffectiveDate(XMLGregorianCalendar value) {
                this.foreignTaxIDEffectiveDate = value;
            }

        }

    }

}
