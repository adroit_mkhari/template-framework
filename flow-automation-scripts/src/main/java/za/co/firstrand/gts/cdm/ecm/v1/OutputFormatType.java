
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for outputFormatType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="outputFormatType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="outputFormat" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="distributionReference" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="responseType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="generationReference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "outputFormatType", propOrder = {
    "outputFormat",
    "distributionReference",
    "responseType",
    "generationReference"
})
public class OutputFormatType {

    @XmlElement(required = true)
    protected String outputFormat;
    protected String distributionReference;
    @XmlElement(required = true)
    protected String responseType;
    @XmlElement(required = true)
    protected String generationReference;

    /**
     * Gets the value of the outputFormat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOutputFormat() {
        return outputFormat;
    }

    /**
     * Sets the value of the outputFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOutputFormat(String value) {
        this.outputFormat = value;
    }

    /**
     * Gets the value of the distributionReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDistributionReference() {
        return distributionReference;
    }

    /**
     * Sets the value of the distributionReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDistributionReference(String value) {
        this.distributionReference = value;
    }

    /**
     * Gets the value of the responseType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getResponseType() {
        return responseType;
    }

    /**
     * Sets the value of the responseType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setResponseType(String value) {
        this.responseType = value;
    }

    /**
     * Gets the value of the generationReference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerationReference() {
        return generationReference;
    }

    /**
     * Sets the value of the generationReference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerationReference(String value) {
        this.generationReference = value;
    }

}
