
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventTypeV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventTypeV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eventOption" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventTypeV1.2", propOrder = {
    "eventOption"
})
public class EventTypeV12 {

    @XmlElement(required = true)
    protected String eventOption;

    /**
     * Gets the value of the eventOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventOption() {
        return eventOption;
    }

    /**
     * Sets the value of the eventOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventOption(String value) {
        this.eventOption = value;
    }

}
