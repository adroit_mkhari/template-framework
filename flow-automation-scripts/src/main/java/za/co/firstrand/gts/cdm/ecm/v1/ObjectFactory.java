
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.ecm.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.ecm.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestDocument }
     * 
     */
    public RequestDocument createRequestDocument() {
        return new RequestDocument();
    }

    /**
     * Create an instance of {@link GenerateAndStore }
     * 
     */
    public GenerateAndStore createGenerateAndStore() {
        return new GenerateAndStore();
    }

    /**
     * Create an instance of {@link Document }
     * 
     */
    public Document createDocument() {
        return new Document();
    }

    /**
     * Create an instance of {@link ThemeData }
     * 
     */
    public ThemeData createThemeData() {
        return new ThemeData();
    }

    /**
     * Create an instance of {@link VaultGenerateData }
     * 
     */
    public VaultGenerateData createVaultGenerateData() {
        return new VaultGenerateData();
    }

    /**
     * Create an instance of {@link InternalGenerateData }
     * 
     */
    public InternalGenerateData createInternalGenerateData() {
        return new InternalGenerateData();
    }

    /**
     * Create an instance of {@link CustGenerateData }
     * 
     */
    public CustGenerateData createCustGenerateData() {
        return new CustGenerateData();
    }

    /**
     * Create an instance of {@link ProcGenerateData }
     * 
     */
    public ProcGenerateData createProcGenerateData() {
        return new ProcGenerateData();
    }

    /**
     * Create an instance of {@link OutputFormatTypes }
     * 
     */
    public OutputFormatTypes createOutputFormatTypes() {
        return new OutputFormatTypes();
    }

    /**
     * Create an instance of {@link OutputFormatType }
     * 
     */
    public OutputFormatType createOutputFormatType() {
        return new OutputFormatType();
    }

    /**
     * Create an instance of {@link EmbeddedContents }
     * 
     */
    public EmbeddedContents createEmbeddedContents() {
        return new EmbeddedContents();
    }

    /**
     * Create an instance of {@link EmbeddedContent }
     * 
     */
    public EmbeddedContent createEmbeddedContent() {
        return new EmbeddedContent();
    }

    /**
     * Create an instance of {@link PrintAndPost }
     * 
     */
    public PrintAndPost createPrintAndPost() {
        return new PrintAndPost();
    }

    /**
     * Create an instance of {@link Sms }
     * 
     */
    public Sms createSms() {
        return new Sms();
    }

    /**
     * Create an instance of {@link Email }
     * 
     */
    public Email createEmail() {
        return new Email();
    }

    /**
     * Create an instance of {@link MailAddressList }
     * 
     */
    public MailAddressList createMailAddressList() {
        return new MailAddressList();
    }

    /**
     * Create an instance of {@link EmailBody }
     * 
     */
    public EmailBody createEmailBody() {
        return new EmailBody();
    }

    /**
     * Create an instance of {@link EmailAttachments }
     * 
     */
    public EmailAttachments createEmailAttachments() {
        return new EmailAttachments();
    }

    /**
     * Create an instance of {@link EmailAttachment }
     * 
     */
    public EmailAttachment createEmailAttachment() {
        return new EmailAttachment();
    }

    /**
     * Create an instance of {@link GeneratedResult }
     * 
     */
    public GeneratedResult createGeneratedResult() {
        return new GeneratedResult();
    }

    /**
     * Create an instance of {@link DocumentResult }
     * 
     */
    public DocumentResult createDocumentResult() {
        return new DocumentResult();
    }

    /**
     * Create an instance of {@link ResponseType }
     * 
     */
    public ResponseType createResponseType() {
        return new ResponseType();
    }

}
