
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for vaultGenerateData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="vaultGenerateData"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="buCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ucnType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="poiNumberType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="poiNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="specialInstruction" type="{http://www.w3.org/2001/XMLSchema}anyType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "vaultGenerateData", propOrder = {
    "buCategory",
    "ucn",
    "ucnType",
    "poiNumberType",
    "poiNumber",
    "accountNumber",
    "specialInstruction"
})
public class VaultGenerateData {

    protected String buCategory;
    @XmlElement(required = true)
    protected String ucn;
    protected String ucnType;
    protected String poiNumberType;
    protected String poiNumber;
    @XmlElement(required = true)
    protected String accountNumber;
    protected Object specialInstruction;

    /**
     * Gets the value of the buCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBuCategory() {
        return buCategory;
    }

    /**
     * Sets the value of the buCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBuCategory(String value) {
        this.buCategory = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcn(String value) {
        this.ucn = value;
    }

    /**
     * Gets the value of the ucnType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcnType() {
        return ucnType;
    }

    /**
     * Sets the value of the ucnType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcnType(String value) {
        this.ucnType = value;
    }

    /**
     * Gets the value of the poiNumberType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoiNumberType() {
        return poiNumberType;
    }

    /**
     * Sets the value of the poiNumberType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoiNumberType(String value) {
        this.poiNumberType = value;
    }

    /**
     * Gets the value of the poiNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoiNumber() {
        return poiNumber;
    }

    /**
     * Sets the value of the poiNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoiNumber(String value) {
        this.poiNumber = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAccountNumber(String value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the specialInstruction property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getSpecialInstruction() {
        return specialInstruction;
    }

    /**
     * Sets the value of the specialInstruction property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setSpecialInstruction(Object value) {
        this.specialInstruction = value;
    }

}
