
package za.co.firstrand.gts.cdm.ecm.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for generatedResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="generatedResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="generatedDocumentreference" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="document" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}documentResult" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "generatedResult", propOrder = {
    "generatedDocumentreference",
    "document"
})
public class GeneratedResult {

    @XmlElement(required = true)
    protected String generatedDocumentreference;
    @XmlElement(required = true)
    protected List<DocumentResult> document;

    /**
     * Gets the value of the generatedDocumentreference property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGeneratedDocumentreference() {
        return generatedDocumentreference;
    }

    /**
     * Sets the value of the generatedDocumentreference property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGeneratedDocumentreference(String value) {
        this.generatedDocumentreference = value;
    }

    /**
     * Gets the value of the document property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the document property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDocument().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DocumentResult }
     * 
     * 
     */
    public List<DocumentResult> getDocument() {
        if (document == null) {
            document = new ArrayList<DocumentResult>();
        }
        return this.document;
    }

}
