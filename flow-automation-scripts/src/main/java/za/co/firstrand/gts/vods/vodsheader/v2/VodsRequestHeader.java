
package za.co.firstrand.gts.vods.vodsheader.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VodsRequestHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VodsRequestHeader"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="applicationID" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="costCentre" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *         &lt;element name="companyCode" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *         &lt;element name="portalTransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="sourceTerminal" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="stimulusIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="retryIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="batchID" type="{http://www.firstrand.co.za/gts/vods/vodsHeader/v2}BatchId" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="requestEchoData" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="asyncMessageFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="checkDuplicateFlag" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VodsRequestHeader", propOrder = {
    "applicationID",
    "costCentre",
    "companyCode",
    "portalTransactionCode",
    "sourceTerminal",
    "stimulusIdentifier",
    "retryIndicator",
    "batchID",
    "requestEchoData",
    "asyncMessageFlag",
    "checkDuplicateFlag"
})
public class VodsRequestHeader {

    @XmlElement(required = true)
    protected String applicationID;
    protected int costCentre;
    protected int companyCode;
    @XmlElement(required = true)
    protected String portalTransactionCode;
    protected Integer sourceTerminal;
    protected String stimulusIdentifier;
    protected String retryIndicator;
    protected BatchId batchID;
    protected String requestEchoData;
    protected String asyncMessageFlag;
    protected String checkDuplicateFlag;

    /**
     * Gets the value of the applicationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getApplicationID() {
        return applicationID;
    }

    /**
     * Sets the value of the applicationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setApplicationID(String value) {
        this.applicationID = value;
    }

    /**
     * Gets the value of the costCentre property.
     * 
     */
    public int getCostCentre() {
        return costCentre;
    }

    /**
     * Sets the value of the costCentre property.
     * 
     */
    public void setCostCentre(int value) {
        this.costCentre = value;
    }

    /**
     * Gets the value of the companyCode property.
     * 
     */
    public int getCompanyCode() {
        return companyCode;
    }

    /**
     * Sets the value of the companyCode property.
     * 
     */
    public void setCompanyCode(int value) {
        this.companyCode = value;
    }

    /**
     * Gets the value of the portalTransactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortalTransactionCode() {
        return portalTransactionCode;
    }

    /**
     * Sets the value of the portalTransactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortalTransactionCode(String value) {
        this.portalTransactionCode = value;
    }

    /**
     * Gets the value of the sourceTerminal property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSourceTerminal() {
        return sourceTerminal;
    }

    /**
     * Sets the value of the sourceTerminal property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSourceTerminal(Integer value) {
        this.sourceTerminal = value;
    }

    /**
     * Gets the value of the stimulusIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStimulusIdentifier() {
        return stimulusIdentifier;
    }

    /**
     * Sets the value of the stimulusIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStimulusIdentifier(String value) {
        this.stimulusIdentifier = value;
    }

    /**
     * Gets the value of the retryIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRetryIndicator() {
        return retryIndicator;
    }

    /**
     * Sets the value of the retryIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRetryIndicator(String value) {
        this.retryIndicator = value;
    }

    /**
     * Gets the value of the batchID property.
     * 
     * @return
     *     possible object is
     *     {@link BatchId }
     *     
     */
    public BatchId getBatchID() {
        return batchID;
    }

    /**
     * Sets the value of the batchID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BatchId }
     *     
     */
    public void setBatchID(BatchId value) {
        this.batchID = value;
    }

    /**
     * Gets the value of the requestEchoData property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRequestEchoData() {
        return requestEchoData;
    }

    /**
     * Sets the value of the requestEchoData property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRequestEchoData(String value) {
        this.requestEchoData = value;
    }

    /**
     * Gets the value of the asyncMessageFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAsyncMessageFlag() {
        return asyncMessageFlag;
    }

    /**
     * Sets the value of the asyncMessageFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAsyncMessageFlag(String value) {
        this.asyncMessageFlag = value;
    }

    /**
     * Gets the value of the checkDuplicateFlag property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCheckDuplicateFlag() {
        return checkDuplicateFlag;
    }

    /**
     * Sets the value of the checkDuplicateFlag property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCheckDuplicateFlag(String value) {
        this.checkDuplicateFlag = value;
    }

}
