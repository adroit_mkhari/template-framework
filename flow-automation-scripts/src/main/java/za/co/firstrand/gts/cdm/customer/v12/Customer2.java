
package za.co.firstrand.gts.cdm.customer.v12;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;
import za.co.firstrand.gts.cdm.datatypes.v6.Addressv7;


/**
 * <p>Java class for Customer2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Customer2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerKey" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}CustomerKey" minOccurs="0"/&gt;
 *         &lt;element name="customerType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="customerStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customerLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="custName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="custName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="riskIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="CUACToken" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="branchNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="dateLastUpdated" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="openDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="closedDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="kycVerified" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="kycDateVerified" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="VIPIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="currentVSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="targetVSI" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="domicileBranch" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="officerCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="officerCode2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="taxRefNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="operatorID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sourceOfFunds" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}SourceOfFundsv12" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="postalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v6}Addressv7" minOccurs="0"/&gt;
 *         &lt;element name="physicalAddress" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v6}Addressv7" minOccurs="0"/&gt;
 *         &lt;element name="contactDetail" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}ContactDetailsv12" minOccurs="0"/&gt;
 *         &lt;element name="marketing" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Marketingv12" minOccurs="0"/&gt;
 *         &lt;element name="identification" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Identificationv12" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="foreignTax" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}ForeignTaxv12" minOccurs="0"/&gt;
 *         &lt;element name="ficaStatus" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}FicaStatus" minOccurs="0"/&gt;
 *         &lt;element name="party"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;extension base="{http://www.firstrand.co.za/gts/cdm/customer/v12}Partyv12"&gt;
 *                 &lt;sequence&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/extension&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="segment" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Segmentv12" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Customer2", propOrder = {
    "customerKey",
    "customerType",
    "customerStatus",
    "customerLevel",
    "custName1",
    "custName2",
    "riskIndicator",
    "cuacToken",
    "branchNumber",
    "dateLastUpdated",
    "openDate",
    "closedDate",
    "kycVerified",
    "kycDateVerified",
    "vipIndicator",
    "currentVSI",
    "targetVSI",
    "domicileBranch",
    "officerCode",
    "officerCode2",
    "taxRefNo",
    "operatorID",
    "sourceOfFunds",
    "postalAddress",
    "physicalAddress",
    "contactDetail",
    "marketing",
    "identification",
    "foreignTax",
    "ficaStatus",
    "party",
    "segment"
})
public class Customer2 {

    protected CustomerKey customerKey;
    @XmlElement(required = true)
    protected String customerType;
    protected String customerStatus;
    protected String customerLevel;
    protected String custName1;
    protected String custName2;
    protected String riskIndicator;
    @XmlElement(name = "CUACToken")
    protected String cuacToken;
    protected Integer branchNumber;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateLastUpdated;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar openDate;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar closedDate;
    protected String kycVerified;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar kycDateVerified;
    @XmlElement(name = "VIPIndicator")
    protected Boolean vipIndicator;
    protected String currentVSI;
    protected String targetVSI;
    protected BigInteger domicileBranch;
    protected String officerCode;
    protected String officerCode2;
    protected String taxRefNo;
    protected String operatorID;
    @XmlElement(nillable = true)
    protected List<SourceOfFundsv12> sourceOfFunds;
    protected Addressv7 postalAddress;
    protected Addressv7 physicalAddress;
    protected ContactDetailsv12 contactDetail;
    protected Marketingv12 marketing;
    @XmlElement(nillable = true)
    protected List<Identificationv12> identification;
    protected ForeignTaxv12 foreignTax;
    protected FicaStatus ficaStatus;
    @XmlElement(required = true)
    protected Customer2 .Party party;
    protected Segmentv12 segment;

    /**
     * Gets the value of the customerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKey }
     *     
     */
    public CustomerKey getCustomerKey() {
        return customerKey;
    }

    /**
     * Sets the value of the customerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKey }
     *     
     */
    public void setCustomerKey(CustomerKey value) {
        this.customerKey = value;
    }

    /**
     * Gets the value of the customerType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerType() {
        return customerType;
    }

    /**
     * Sets the value of the customerType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerType(String value) {
        this.customerType = value;
    }

    /**
     * Gets the value of the customerStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerStatus() {
        return customerStatus;
    }

    /**
     * Sets the value of the customerStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerStatus(String value) {
        this.customerStatus = value;
    }

    /**
     * Gets the value of the customerLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerLevel() {
        return customerLevel;
    }

    /**
     * Sets the value of the customerLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerLevel(String value) {
        this.customerLevel = value;
    }

    /**
     * Gets the value of the custName1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustName1() {
        return custName1;
    }

    /**
     * Sets the value of the custName1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustName1(String value) {
        this.custName1 = value;
    }

    /**
     * Gets the value of the custName2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustName2() {
        return custName2;
    }

    /**
     * Sets the value of the custName2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustName2(String value) {
        this.custName2 = value;
    }

    /**
     * Gets the value of the riskIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskIndicator() {
        return riskIndicator;
    }

    /**
     * Sets the value of the riskIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskIndicator(String value) {
        this.riskIndicator = value;
    }

    /**
     * Gets the value of the cuacToken property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUACToken() {
        return cuacToken;
    }

    /**
     * Sets the value of the cuacToken property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUACToken(String value) {
        this.cuacToken = value;
    }

    /**
     * Gets the value of the branchNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getBranchNumber() {
        return branchNumber;
    }

    /**
     * Sets the value of the branchNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setBranchNumber(Integer value) {
        this.branchNumber = value;
    }

    /**
     * Gets the value of the dateLastUpdated property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateLastUpdated() {
        return dateLastUpdated;
    }

    /**
     * Sets the value of the dateLastUpdated property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateLastUpdated(XMLGregorianCalendar value) {
        this.dateLastUpdated = value;
    }

    /**
     * Gets the value of the openDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getOpenDate() {
        return openDate;
    }

    /**
     * Sets the value of the openDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setOpenDate(XMLGregorianCalendar value) {
        this.openDate = value;
    }

    /**
     * Gets the value of the closedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getClosedDate() {
        return closedDate;
    }

    /**
     * Sets the value of the closedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setClosedDate(XMLGregorianCalendar value) {
        this.closedDate = value;
    }

    /**
     * Gets the value of the kycVerified property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKycVerified() {
        return kycVerified;
    }

    /**
     * Sets the value of the kycVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKycVerified(String value) {
        this.kycVerified = value;
    }

    /**
     * Gets the value of the kycDateVerified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getKycDateVerified() {
        return kycDateVerified;
    }

    /**
     * Sets the value of the kycDateVerified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setKycDateVerified(XMLGregorianCalendar value) {
        this.kycDateVerified = value;
    }

    /**
     * Gets the value of the vipIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isVIPIndicator() {
        return vipIndicator;
    }

    /**
     * Sets the value of the vipIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setVIPIndicator(Boolean value) {
        this.vipIndicator = value;
    }

    /**
     * Gets the value of the currentVSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCurrentVSI() {
        return currentVSI;
    }

    /**
     * Sets the value of the currentVSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCurrentVSI(String value) {
        this.currentVSI = value;
    }

    /**
     * Gets the value of the targetVSI property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTargetVSI() {
        return targetVSI;
    }

    /**
     * Sets the value of the targetVSI property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTargetVSI(String value) {
        this.targetVSI = value;
    }

    /**
     * Gets the value of the domicileBranch property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getDomicileBranch() {
        return domicileBranch;
    }

    /**
     * Sets the value of the domicileBranch property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setDomicileBranch(BigInteger value) {
        this.domicileBranch = value;
    }

    /**
     * Gets the value of the officerCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerCode() {
        return officerCode;
    }

    /**
     * Sets the value of the officerCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerCode(String value) {
        this.officerCode = value;
    }

    /**
     * Gets the value of the officerCode2 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOfficerCode2() {
        return officerCode2;
    }

    /**
     * Sets the value of the officerCode2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOfficerCode2(String value) {
        this.officerCode2 = value;
    }

    /**
     * Gets the value of the taxRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTaxRefNo() {
        return taxRefNo;
    }

    /**
     * Sets the value of the taxRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTaxRefNo(String value) {
        this.taxRefNo = value;
    }

    /**
     * Gets the value of the operatorID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOperatorID() {
        return operatorID;
    }

    /**
     * Sets the value of the operatorID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOperatorID(String value) {
        this.operatorID = value;
    }

    /**
     * Gets the value of the sourceOfFunds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sourceOfFunds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSourceOfFunds().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SourceOfFundsv12 }
     * 
     * 
     */
    public List<SourceOfFundsv12> getSourceOfFunds() {
        if (sourceOfFunds == null) {
            sourceOfFunds = new ArrayList<SourceOfFundsv12>();
        }
        return this.sourceOfFunds;
    }

    /**
     * Gets the value of the postalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Addressv7 }
     *     
     */
    public Addressv7 getPostalAddress() {
        return postalAddress;
    }

    /**
     * Sets the value of the postalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Addressv7 }
     *     
     */
    public void setPostalAddress(Addressv7 value) {
        this.postalAddress = value;
    }

    /**
     * Gets the value of the physicalAddress property.
     * 
     * @return
     *     possible object is
     *     {@link Addressv7 }
     *     
     */
    public Addressv7 getPhysicalAddress() {
        return physicalAddress;
    }

    /**
     * Sets the value of the physicalAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link Addressv7 }
     *     
     */
    public void setPhysicalAddress(Addressv7 value) {
        this.physicalAddress = value;
    }

    /**
     * Gets the value of the contactDetail property.
     * 
     * @return
     *     possible object is
     *     {@link ContactDetailsv12 }
     *     
     */
    public ContactDetailsv12 getContactDetail() {
        return contactDetail;
    }

    /**
     * Sets the value of the contactDetail property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDetailsv12 }
     *     
     */
    public void setContactDetail(ContactDetailsv12 value) {
        this.contactDetail = value;
    }

    /**
     * Gets the value of the marketing property.
     * 
     * @return
     *     possible object is
     *     {@link Marketingv12 }
     *     
     */
    public Marketingv12 getMarketing() {
        return marketing;
    }

    /**
     * Sets the value of the marketing property.
     * 
     * @param value
     *     allowed object is
     *     {@link Marketingv12 }
     *     
     */
    public void setMarketing(Marketingv12 value) {
        this.marketing = value;
    }

    /**
     * Gets the value of the identification property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the identification property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getIdentification().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Identificationv12 }
     * 
     * 
     */
    public List<Identificationv12> getIdentification() {
        if (identification == null) {
            identification = new ArrayList<Identificationv12>();
        }
        return this.identification;
    }

    /**
     * Gets the value of the foreignTax property.
     * 
     * @return
     *     possible object is
     *     {@link ForeignTaxv12 }
     *     
     */
    public ForeignTaxv12 getForeignTax() {
        return foreignTax;
    }

    /**
     * Sets the value of the foreignTax property.
     * 
     * @param value
     *     allowed object is
     *     {@link ForeignTaxv12 }
     *     
     */
    public void setForeignTax(ForeignTaxv12 value) {
        this.foreignTax = value;
    }

    /**
     * Gets the value of the ficaStatus property.
     * 
     * @return
     *     possible object is
     *     {@link FicaStatus }
     *     
     */
    public FicaStatus getFicaStatus() {
        return ficaStatus;
    }

    /**
     * Sets the value of the ficaStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link FicaStatus }
     *     
     */
    public void setFicaStatus(FicaStatus value) {
        this.ficaStatus = value;
    }

    /**
     * Gets the value of the party property.
     * 
     * @return
     *     possible object is
     *     {@link Customer2 .Party }
     *     
     */
    public Customer2 .Party getParty() {
        return party;
    }

    /**
     * Sets the value of the party property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer2 .Party }
     *     
     */
    public void setParty(Customer2 .Party value) {
        this.party = value;
    }

    /**
     * Gets the value of the segment property.
     * 
     * @return
     *     possible object is
     *     {@link Segmentv12 }
     *     
     */
    public Segmentv12 getSegment() {
        return segment;
    }

    /**
     * Sets the value of the segment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Segmentv12 }
     *     
     */
    public void setSegment(Segmentv12 value) {
        this.segment = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;extension base="{http://www.firstrand.co.za/gts/cdm/customer/v12}Partyv12"&gt;
     *       &lt;sequence&gt;
     *       &lt;/sequence&gt;
     *     &lt;/extension&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "")
    public static class Party
        extends Partyv12
    {


    }

}
