
package za.co.firstrand.gts.accsrv.accountservice.maintainoffhostaccount.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.account.v8.CisAccount;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cisAccount" type="{http://www.firstrand.co.za/gts/cdm/account/v8}CisAccount" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "cisAccount"
})
@XmlRootElement(name = "MaintainOffHostAccountRequest")
public class MaintainOffHostAccountRequest
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected CisAccount cisAccount;

    /**
     * Gets the value of the cisAccount property.
     * 
     * @return
     *     possible object is
     *     {@link CisAccount }
     *     
     */
    public CisAccount getCisAccount() {
        return cisAccount;
    }

    /**
     * Sets the value of the cisAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link CisAccount }
     *     
     */
    public void setCisAccount(CisAccount value) {
        this.cisAccount = value;
    }

}
