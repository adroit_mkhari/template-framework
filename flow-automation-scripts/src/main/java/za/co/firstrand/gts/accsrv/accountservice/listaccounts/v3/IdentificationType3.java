
package za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationType3.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IdentificationType3"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="RSA_IDENTIFICATION_DOCUMENT"/&gt;
 *     &lt;enumeration value="PASSPORT"/&gt;
 *     &lt;enumeration value="NAMIBIAN_IDENTIFICATION_DOCUMENT"/&gt;
 *     &lt;enumeration value="NAMIBIAN_REFERENCE_BOOK"/&gt;
 *     &lt;enumeration value="ZAMBIAN_NATIONAL_REGISTRATION_CARD"/&gt;
 *     &lt;enumeration value="BOTSWANA_IDENTIFICATION_DOCUMENT"/&gt;
 *     &lt;enumeration value="SWAZILAND_REFERENCE_BOOK"/&gt;
 *     &lt;enumeration value="SWAZILAND_IDENTIFICATION_DOCUMENT"/&gt;
 *     &lt;enumeration value="TEMPORARY_RESIDENCE_PERMIT_NO"/&gt;
 *     &lt;enumeration value="CC_REGISTRATION_NUMBER"/&gt;
 *     &lt;enumeration value="COMPANY_REGISTRATION_NUMBER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IdentificationType3")
@XmlEnum
public enum IdentificationType3 {

    RSA_IDENTIFICATION_DOCUMENT,
    PASSPORT,
    NAMIBIAN_IDENTIFICATION_DOCUMENT,
    NAMIBIAN_REFERENCE_BOOK,
    ZAMBIAN_NATIONAL_REGISTRATION_CARD,
    BOTSWANA_IDENTIFICATION_DOCUMENT,
    SWAZILAND_REFERENCE_BOOK,
    SWAZILAND_IDENTIFICATION_DOCUMENT,
    TEMPORARY_RESIDENCE_PERMIT_NO,
    CC_REGISTRATION_NUMBER,
    COMPANY_REGISTRATION_NUMBER;

    public String value() {
        return name();
    }

    public static IdentificationType3 fromValue(String v) {
        return valueOf(v);
    }

}
