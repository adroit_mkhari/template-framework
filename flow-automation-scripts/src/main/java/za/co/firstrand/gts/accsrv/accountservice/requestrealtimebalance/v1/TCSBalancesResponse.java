
package za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.account.v8.AccountBalance;


/**
 * <p>Java class for TCSBalancesResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TCSBalancesResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="productCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="accountNumber" type="{http://www.w3.org/2001/XMLSchema}integer" form="qualified"/&gt;
 *         &lt;element name="creditLimit" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="accountCurrentBalance" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="settlementAmount" type="{http://www.firstrand.co.za/gts/cdm/account/v8}AccountBalance" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TCSBalancesResponse", propOrder = {
    "productCode",
    "companyId",
    "accountNumber",
    "creditLimit",
    "accountCurrentBalance",
    "settlementAmount"
})
public class TCSBalancesResponse {

    @XmlElement(required = true)
    protected String productCode;
    @XmlElement(required = true)
    protected String companyId;
    @XmlElement(required = true)
    protected BigInteger accountNumber;
    protected AccountBalance creditLimit;
    protected AccountBalance accountCurrentBalance;
    protected AccountBalance settlementAmount;

    /**
     * Gets the value of the productCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProductCode() {
        return productCode;
    }

    /**
     * Sets the value of the productCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProductCode(String value) {
        this.productCode = value;
    }

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyId(String value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the accountNumber property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getAccountNumber() {
        return accountNumber;
    }

    /**
     * Sets the value of the accountNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setAccountNumber(BigInteger value) {
        this.accountNumber = value;
    }

    /**
     * Gets the value of the creditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getCreditLimit() {
        return creditLimit;
    }

    /**
     * Sets the value of the creditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setCreditLimit(AccountBalance value) {
        this.creditLimit = value;
    }

    /**
     * Gets the value of the accountCurrentBalance property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getAccountCurrentBalance() {
        return accountCurrentBalance;
    }

    /**
     * Sets the value of the accountCurrentBalance property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setAccountCurrentBalance(AccountBalance value) {
        this.accountCurrentBalance = value;
    }

    /**
     * Gets the value of the settlementAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AccountBalance }
     *     
     */
    public AccountBalance getSettlementAmount() {
        return settlementAmount;
    }

    /**
     * Sets the value of the settlementAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AccountBalance }
     *     
     */
    public void setSettlementAmount(AccountBalance value) {
        this.settlementAmount = value;
    }

}
