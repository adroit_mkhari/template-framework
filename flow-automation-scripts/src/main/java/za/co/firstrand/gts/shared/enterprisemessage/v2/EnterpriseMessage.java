
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.accsrv.accountservice.linkoffhostaccounttocustomer.v2.LinkOffHostAccountToCustomerRequest;
import za.co.firstrand.gts.accsrv.accountservice.linkoffhostaccounttocustomer.v2.LinkOffHostAccountToCustomerResponse;
import za.co.firstrand.gts.accsrv.accountservice.maintainoffhostaccount.v2.MaintainOffHostAccountRequest;
import za.co.firstrand.gts.accsrv.accountservice.maintainoffhostaccount.v2.MaintainOffHostAccountResponse;
import za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1.RequestRealTimeBalanceRequest;
import za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1.RequestRealTimeBalanceResponse;
import za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1.GenerateMacCodeRequest;
import za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1.GenerateMacCodeResponse;
import za.co.firstrand.gts.cussrv.customerservice.maintaincustomerprofile.v4.MaintainCustomerProfileRequest;
import za.co.firstrand.gts.cussrv.customerservice.maintaincustomerprofile.v4.MaintainCustomerProfileResponse;
import za.co.firstrand.gts.cussrv.customerservice.maintainjuristiccustomerprofile.v2.MaintainJuristicCustomerProfileRequest;
import za.co.firstrand.gts.cussrv.customerservice.maintainjuristiccustomerprofile.v2.MaintainJuristicCustomerProfileResponse;
import za.co.firstrand.gts.cussrv.customerservice.retrievecustomerlisting.v1.RetrieveCustomerListingRequest;
import za.co.firstrand.gts.cussrv.customerservice.retrievecustomerlisting.v1.RetrieveCustomerListingResponse;
import za.co.firstrand.gts.ecm.generationservice.generatecontent.v1.GenerateContentRequest;
import za.co.firstrand.gts.ecm.generationservice.generatecontent.v1.GenerateContentResponse;
import za.co.firstrand.gts.midlife.claimservice.processclaim.v1.ProcessClaimRequest;
import za.co.firstrand.gts.midlife.claimservice.processclaim.v1.ProcessClaimResponse;
import za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1.SubmitPreLoginClaimRequest;
import za.co.firstrand.gts.midlife.claimservice.submitpreloginclaim.v1.SubmitPreLoginClaimResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1.MaintainPolicyDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.maintainpolicydetails.v1.MaintainPolicyDetailsResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1.RetrievePolicyDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1.RetrievePolicyDetailsResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1.RetrievePolicyOverviewRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1.RetrievePolicyOverviewResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1.RetrievePrincipalMemberDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1.RetrievePrincipalMemberDetailsResponse;
import za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1.SendGenericLetterRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1.SendGenericLetterResponse;


/**
 * <p>Java class for EnterpriseMessage complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnterpriseMessage"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageHeader"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseMessage", propOrder = {
    "header"
})
@XmlSeeAlso({
    MaintainPolicyDetailsRequest.class,
    MaintainPolicyDetailsResponse.class,
    RetrievePolicyOverviewRequest.class,
    RetrievePolicyOverviewResponse.class,
    RetrievePrincipalMemberDetailsRequest.class,
    RetrievePrincipalMemberDetailsResponse.class,
    SendGenericLetterRequest.class,
    SendGenericLetterResponse.class,
    RetrievePolicyDetailsRequest.class,
    RetrievePolicyDetailsResponse.class,
    SubmitPreLoginClaimRequest.class,
    SubmitPreLoginClaimResponse.class,
    GenerateContentRequest.class,
    GenerateContentResponse.class,
    GenerateMacCodeRequest.class,
    GenerateMacCodeResponse.class,
    RequestRealTimeBalanceRequest.class,
    RequestRealTimeBalanceResponse.class,
    LinkOffHostAccountToCustomerResponse.class,
    LinkOffHostAccountToCustomerRequest.class,
    MaintainOffHostAccountResponse.class,
    MaintainOffHostAccountRequest.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v5.LookupCustomerDetailsResponse.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v5.LookupCustomerDetailsRequest.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v6.LookupCustomerDetailsResponse.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v6.LookupCustomerDetailsRequest.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v7.LookupCustomerDetailsResponse.class,
    za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v7.LookupCustomerDetailsRequest.class,
    MaintainCustomerProfileResponse.class,
    MaintainCustomerProfileRequest.class,
    MaintainJuristicCustomerProfileResponse.class,
    MaintainJuristicCustomerProfileRequest.class,
    RetrieveCustomerListingResponse.class,
    RetrieveCustomerListingRequest.class,
    ProcessClaimResponse.class,
    ProcessClaimRequest.class
})
public class EnterpriseMessage {

    @XmlElement(required = true)
    protected EnterpriseMessageHeader header;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseMessageHeader }
     *     
     */
    public EnterpriseMessageHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseMessageHeader }
     *     
     */
    public void setHeader(EnterpriseMessageHeader value) {
        this.header = value;
    }

}
