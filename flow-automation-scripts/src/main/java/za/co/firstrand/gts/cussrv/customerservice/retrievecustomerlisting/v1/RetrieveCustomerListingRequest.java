
package za.co.firstrand.gts.cussrv.customerservice.retrievecustomerlisting.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v12.Identification;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="searchOption" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="nextCustomerKey" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/RetrieveCustomerListing/v1}CustomerKey" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byCustomerNames" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/RetrieveCustomerListing/v1}ByCustomerNames" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byAccount" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/RetrieveCustomerListing/v1}ByAccount" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byCustomerIdentification" type="{http://www.firstrand.co.za/gts/cdm/customer/v12}Identification" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "searchOption",
    "nextCustomerKey",
    "byCustomerNames",
    "byAccount",
    "byCustomerIdentification"
})
@XmlRootElement(name = "RetrieveCustomerListingRequest")
public class RetrieveCustomerListingRequest
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String searchOption;
    protected CustomerKey nextCustomerKey;
    protected ByCustomerNames byCustomerNames;
    protected ByAccount byAccount;
    protected Identification byCustomerIdentification;

    /**
     * Gets the value of the searchOption property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSearchOption() {
        return searchOption;
    }

    /**
     * Sets the value of the searchOption property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSearchOption(String value) {
        this.searchOption = value;
    }

    /**
     * Gets the value of the nextCustomerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKey }
     *     
     */
    public CustomerKey getNextCustomerKey() {
        return nextCustomerKey;
    }

    /**
     * Sets the value of the nextCustomerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKey }
     *     
     */
    public void setNextCustomerKey(CustomerKey value) {
        this.nextCustomerKey = value;
    }

    /**
     * Gets the value of the byCustomerNames property.
     * 
     * @return
     *     possible object is
     *     {@link ByCustomerNames }
     *     
     */
    public ByCustomerNames getByCustomerNames() {
        return byCustomerNames;
    }

    /**
     * Sets the value of the byCustomerNames property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByCustomerNames }
     *     
     */
    public void setByCustomerNames(ByCustomerNames value) {
        this.byCustomerNames = value;
    }

    /**
     * Gets the value of the byAccount property.
     * 
     * @return
     *     possible object is
     *     {@link ByAccount }
     *     
     */
    public ByAccount getByAccount() {
        return byAccount;
    }

    /**
     * Sets the value of the byAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByAccount }
     *     
     */
    public void setByAccount(ByAccount value) {
        this.byAccount = value;
    }

    /**
     * Gets the value of the byCustomerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link Identification }
     *     
     */
    public Identification getByCustomerIdentification() {
        return byCustomerIdentification;
    }

    /**
     * Sets the value of the byCustomerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Identification }
     *     
     */
    public void setByCustomerIdentification(Identification value) {
        this.byCustomerIdentification = value;
    }

}
