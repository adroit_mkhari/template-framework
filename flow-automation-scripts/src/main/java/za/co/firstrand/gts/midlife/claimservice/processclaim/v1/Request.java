
package za.co.firstrand.gts.midlife.claimservice.processclaim.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Request complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Request"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="claimData" type="{http://www.firstrand.co.za/gts/midlife/ClaimService/ProcessClaim/v1}ClaimData" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Request", propOrder = {
    "claimData"
})
public class Request {

    @XmlElement(required = true)
    protected ClaimData claimData;

    /**
     * Gets the value of the claimData property.
     * 
     * @return
     *     possible object is
     *     {@link ClaimData }
     *     
     */
    public ClaimData getClaimData() {
        return claimData;
    }

    /**
     * Sets the value of the claimData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ClaimData }
     *     
     */
    public void setClaimData(ClaimData value) {
        this.claimData = value;
    }

}
