
package za.co.firstrand.gts.ecm.generationservice.generatecontent.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.ecm.v1.GeneratedResult;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for GenerateContentResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateContentResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="generatedResult" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}generatedResult" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateContentResponse", propOrder = {
    "generatedResult"
})
public class GenerateContentResponse
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected GeneratedResult generatedResult;

    /**
     * Gets the value of the generatedResult property.
     * 
     * @return
     *     possible object is
     *     {@link GeneratedResult }
     *     
     */
    public GeneratedResult getGeneratedResult() {
        return generatedResult;
    }

    /**
     * Sets the value of the generatedResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link GeneratedResult }
     *     
     */
    public void setGeneratedResult(GeneratedResult value) {
        this.generatedResult = value;
    }

}
