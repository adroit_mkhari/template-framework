
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Segment complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Segment"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="primarySegment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="segmentType" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="subSegment" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Segment", propOrder = {
    "primarySegment",
    "segmentType",
    "subSegment"
})
public class Segment {

    @XmlElement(required = true)
    protected String primarySegment;
    @XmlElement(required = true)
    protected String segmentType;
    @XmlElement(required = true)
    protected String subSegment;

    /**
     * Gets the value of the primarySegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrimarySegment() {
        return primarySegment;
    }

    /**
     * Sets the value of the primarySegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrimarySegment(String value) {
        this.primarySegment = value;
    }

    /**
     * Gets the value of the segmentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSegmentType() {
        return segmentType;
    }

    /**
     * Sets the value of the segmentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSegmentType(String value) {
        this.segmentType = value;
    }

    /**
     * Gets the value of the subSegment property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSegment() {
        return subSegment;
    }

    /**
     * Sets the value of the subSegment property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSegment(String value) {
        this.subSegment = value;
    }

}
