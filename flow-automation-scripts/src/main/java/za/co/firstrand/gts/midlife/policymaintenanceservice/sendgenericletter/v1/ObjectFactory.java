
package za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SendGenericLetterRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/SendGenericLetter/v1", "SendGenericLetterRequest");
    private final static QName _SendGenericLetterResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/SendGenericLetter/v1", "SendGenericLetterResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.policymaintenanceservice.sendgenericletter.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SendGenericLetterRequest }
     * 
     */
    public SendGenericLetterRequest createSendGenericLetterRequest() {
        return new SendGenericLetterRequest();
    }

    /**
     * Create an instance of {@link SendGenericLetterResponse }
     * 
     */
    public SendGenericLetterResponse createSendGenericLetterResponse() {
        return new SendGenericLetterResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendGenericLetterRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendGenericLetterRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/SendGenericLetter/v1", name = "SendGenericLetterRequest")
    public JAXBElement<SendGenericLetterRequest> createSendGenericLetterRequest(SendGenericLetterRequest value) {
        return new JAXBElement<SendGenericLetterRequest>(_SendGenericLetterRequest_QNAME, SendGenericLetterRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SendGenericLetterResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SendGenericLetterResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/SendGenericLetter/v1", name = "SendGenericLetterResponse")
    public JAXBElement<SendGenericLetterResponse> createSendGenericLetterResponse(SendGenericLetterResponse value) {
        return new JAXBElement<SendGenericLetterResponse>(_SendGenericLetterResponse_QNAME, SendGenericLetterResponse.class, null, value);
    }

}
