
package za.co.firstrand.gts.cdm.policyservicing.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for IdentificationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="IdentificationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UCN"/&gt;
 *     &lt;enumeration value="ID_NUMBER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "IdentificationType")
@XmlEnum
public enum IdentificationType {

    UCN,
    ID_NUMBER;

    public String value() {
        return name();
    }

    public static IdentificationType fromValue(String v) {
        return valueOf(v);
    }

}
