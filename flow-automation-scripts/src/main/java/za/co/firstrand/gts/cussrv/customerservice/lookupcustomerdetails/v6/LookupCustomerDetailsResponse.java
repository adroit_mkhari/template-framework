
package za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v6;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v11.Customer2;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customer" type="{http://www.firstrand.co.za/gts/cdm/customer/v11}Customer2" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customer"
})
@XmlRootElement(name = "LookupCustomerDetailsResponse")
public class LookupCustomerDetailsResponse
    extends EnterpriseMessage
{

    protected List<Customer2> customer;

    /**
     * Gets the value of the customer property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customer property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomer().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Customer2 }
     * 
     * 
     */
    public List<Customer2> getCustomer() {
        if (customer == null) {
            customer = new ArrayList<Customer2>();
        }
        return this.customer;
    }

}
