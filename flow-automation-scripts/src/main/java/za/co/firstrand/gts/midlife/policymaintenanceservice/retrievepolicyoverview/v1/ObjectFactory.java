
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrievePolicyOverviewRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyOverview/v1", "RetrievePolicyOverviewRequest");
    private final static QName _RetrievePolicyOverviewResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyOverview/v1", "RetrievePolicyOverviewResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrievePolicyOverviewRequest }
     * 
     */
    public RetrievePolicyOverviewRequest createRetrievePolicyOverviewRequest() {
        return new RetrievePolicyOverviewRequest();
    }

    /**
     * Create an instance of {@link RetrievePolicyOverviewResponse }
     * 
     */
    public RetrievePolicyOverviewResponse createRetrievePolicyOverviewResponse() {
        return new RetrievePolicyOverviewResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyOverview/v1", name = "RetrievePolicyOverviewRequest")
    public JAXBElement<RetrievePolicyOverviewRequest> createRetrievePolicyOverviewRequest(RetrievePolicyOverviewRequest value) {
        return new JAXBElement<RetrievePolicyOverviewRequest>(_RetrievePolicyOverviewRequest_QNAME, RetrievePolicyOverviewRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyOverviewResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePolicyOverview/v1", name = "RetrievePolicyOverviewResponse")
    public JAXBElement<RetrievePolicyOverviewResponse> createRetrievePolicyOverviewResponse(RetrievePolicyOverviewResponse value) {
        return new JAXBElement<RetrievePolicyOverviewResponse>(_RetrievePolicyOverviewResponse_QNAME, RetrievePolicyOverviewResponse.class, null, value);
    }

}
