
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicyoverview.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.policyservicing.v1.PolicyDetails;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for RetrievePolicyOverviewResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrievePolicyOverviewResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="policyDetailsList" type="{http://www.firstrand.co.za/gts/cdm/policyservicing/v1}PolicyDetails" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrievePolicyOverviewResponse", propOrder = {
    "message",
    "policyDetailsList"
})
public class RetrievePolicyOverviewResponse
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String message;
    @XmlElement(nillable = true)
    protected List<PolicyDetails> policyDetailsList;

    /**
     * Gets the value of the message property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Gets the value of the policyDetailsList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the policyDetailsList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPolicyDetailsList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PolicyDetails }
     * 
     * 
     */
    public List<PolicyDetails> getPolicyDetailsList() {
        if (policyDetailsList == null) {
            policyDetailsList = new ArrayList<PolicyDetails>();
        }
        return this.policyDetailsList;
    }

}
