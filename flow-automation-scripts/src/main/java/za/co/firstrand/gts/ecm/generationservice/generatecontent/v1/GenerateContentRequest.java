
package za.co.firstrand.gts.ecm.generationservice.generatecontent.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.ecm.v1.RequestDocument;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for GenerateContentRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateContentRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="companyId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="theme" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="userName" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="sourceChannel" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="integrationChannel" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="createStation" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="templateXmlList" type="{http://www.firstrand.co.za/gts/ecm/GenerationService/GenerateContent/v1}templateXmlList" form="qualified"/&gt;
 *         &lt;element name="requestDocument" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}requestDocument" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateContentRequest", propOrder = {
    "companyId",
    "theme",
    "userName",
    "password",
    "sourceChannel",
    "integrationChannel",
    "createStation",
    "templateXmlList",
    "requestDocument"
})
public class GenerateContentRequest
    extends EnterpriseMessage
{

    @XmlElement(required = true)
    protected String companyId;
    @XmlElement(required = true)
    protected String theme;
    @XmlElement(required = true)
    protected String userName;
    @XmlElement(required = true)
    protected String password;
    @XmlElement(required = true)
    protected String sourceChannel;
    @XmlElement(required = true)
    protected String integrationChannel;
    @XmlElement(required = true)
    protected String createStation;
    @XmlElement(required = true)
    protected TemplateXmlList templateXmlList;
    @XmlElement(required = true)
    protected RequestDocument requestDocument;

    /**
     * Gets the value of the companyId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCompanyId() {
        return companyId;
    }

    /**
     * Sets the value of the companyId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCompanyId(String value) {
        this.companyId = value;
    }

    /**
     * Gets the value of the theme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTheme() {
        return theme;
    }

    /**
     * Sets the value of the theme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTheme(String value) {
        this.theme = value;
    }

    /**
     * Gets the value of the userName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the value of the userName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserName(String value) {
        this.userName = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the sourceChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSourceChannel() {
        return sourceChannel;
    }

    /**
     * Sets the value of the sourceChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSourceChannel(String value) {
        this.sourceChannel = value;
    }

    /**
     * Gets the value of the integrationChannel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIntegrationChannel() {
        return integrationChannel;
    }

    /**
     * Sets the value of the integrationChannel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIntegrationChannel(String value) {
        this.integrationChannel = value;
    }

    /**
     * Gets the value of the createStation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCreateStation() {
        return createStation;
    }

    /**
     * Sets the value of the createStation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCreateStation(String value) {
        this.createStation = value;
    }

    /**
     * Gets the value of the templateXmlList property.
     * 
     * @return
     *     possible object is
     *     {@link TemplateXmlList }
     *     
     */
    public TemplateXmlList getTemplateXmlList() {
        return templateXmlList;
    }

    /**
     * Sets the value of the templateXmlList property.
     * 
     * @param value
     *     allowed object is
     *     {@link TemplateXmlList }
     *     
     */
    public void setTemplateXmlList(TemplateXmlList value) {
        this.templateXmlList = value;
    }

    /**
     * Gets the value of the requestDocument property.
     * 
     * @return
     *     possible object is
     *     {@link RequestDocument }
     *     
     */
    public RequestDocument getRequestDocument() {
        return requestDocument;
    }

    /**
     * Sets the value of the requestDocument property.
     * 
     * @param value
     *     allowed object is
     *     {@link RequestDocument }
     *     
     */
    public void setRequestDocument(RequestDocument value) {
        this.requestDocument = value;
    }

}
