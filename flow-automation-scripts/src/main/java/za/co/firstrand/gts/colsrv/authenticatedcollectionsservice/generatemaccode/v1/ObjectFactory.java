
package za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GenerateMacCodeRequest_QNAME = new QName("http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1", "GenerateMacCodeRequest");
    private final static QName _GenerateMacCodeResponse_QNAME = new QName("http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1", "GenerateMacCodeResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GenerateMacCodeRequest }
     * 
     */
    public GenerateMacCodeRequest createGenerateMacCodeRequest() {
        return new GenerateMacCodeRequest();
    }

    /**
     * Create an instance of {@link GenerateMacCodeResponse }
     * 
     */
    public GenerateMacCodeResponse createGenerateMacCodeResponse() {
        return new GenerateMacCodeResponse();
    }

    /**
     * Create an instance of {@link DebtorAccountDetails }
     * 
     */
    public DebtorAccountDetails createDebtorAccountDetails() {
        return new DebtorAccountDetails();
    }

    /**
     * Create an instance of {@link DebtorIdDetails }
     * 
     */
    public DebtorIdDetails createDebtorIdDetails() {
        return new DebtorIdDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateMacCodeRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerateMacCodeRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1", name = "GenerateMacCodeRequest")
    public JAXBElement<GenerateMacCodeRequest> createGenerateMacCodeRequest(GenerateMacCodeRequest value) {
        return new JAXBElement<GenerateMacCodeRequest>(_GenerateMacCodeRequest_QNAME, GenerateMacCodeRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GenerateMacCodeResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GenerateMacCodeResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/colsrv/AuthenticatedCollectionsService/GenerateMacCode/v1", name = "GenerateMacCodeResponse")
    public JAXBElement<GenerateMacCodeResponse> createGenerateMacCodeResponse(GenerateMacCodeResponse value) {
        return new JAXBElement<GenerateMacCodeResponse>(_GenerateMacCodeResponse_QNAME, GenerateMacCodeResponse.class, null, value);
    }

}
