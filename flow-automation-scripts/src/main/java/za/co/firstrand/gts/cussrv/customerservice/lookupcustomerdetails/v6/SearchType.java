
package za.co.firstrand.gts.cussrv.customerservice.lookupcustomerdetails.v6;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v11.CustomerKey;
import za.co.firstrand.gts.cdm.customer.v11.Identification;


/**
 * <p>Java class for SearchType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SearchType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="byAccount" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/LookupCustomerDetails/v6}ByAccount" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byCustomerKey" type="{http://www.firstrand.co.za/gts/cdm/customer/v11}CustomerKey" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="byCustomerIdentification" type="{http://www.firstrand.co.za/gts/cdm/customer/v11}Identification" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SearchType", propOrder = {
    "byAccount",
    "byCustomerKey",
    "byCustomerIdentification"
})
public class SearchType {

    protected ByAccount byAccount;
    protected CustomerKey byCustomerKey;
    protected Identification byCustomerIdentification;

    /**
     * Gets the value of the byAccount property.
     * 
     * @return
     *     possible object is
     *     {@link ByAccount }
     *     
     */
    public ByAccount getByAccount() {
        return byAccount;
    }

    /**
     * Sets the value of the byAccount property.
     * 
     * @param value
     *     allowed object is
     *     {@link ByAccount }
     *     
     */
    public void setByAccount(ByAccount value) {
        this.byAccount = value;
    }

    /**
     * Gets the value of the byCustomerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKey }
     *     
     */
    public CustomerKey getByCustomerKey() {
        return byCustomerKey;
    }

    /**
     * Sets the value of the byCustomerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKey }
     *     
     */
    public void setByCustomerKey(CustomerKey value) {
        this.byCustomerKey = value;
    }

    /**
     * Gets the value of the byCustomerIdentification property.
     * 
     * @return
     *     possible object is
     *     {@link Identification }
     *     
     */
    public Identification getByCustomerIdentification() {
        return byCustomerIdentification;
    }

    /**
     * Sets the value of the byCustomerIdentification property.
     * 
     * @param value
     *     allowed object is
     *     {@link Identification }
     *     
     */
    public void setByCustomerIdentification(Identification value) {
        this.byCustomerIdentification = value;
    }

}
