
package za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessageCUCU;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageCUCU"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerInfo" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customerName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="customerName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="customerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="customerCompanyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="customerUCN" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="rulesCounter" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CUCURulesInfo" maxOccurs="unbounded" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="customerRelationshipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="minRelationshipType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="maxRelationshipType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CUCUNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="CUCUInfo" maxOccurs="unbounded" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="CUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUCustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUCompanyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUUCNNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUIDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUCustomerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUKYCIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                   &lt;element name="CUCUPercentageOwned" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0" form="qualified"/&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *         &lt;element name="CUCUMoreIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="nextCUCUInfo" minOccurs="0" form="qualified"&gt;
 *           &lt;complexType&gt;
 *             &lt;complexContent&gt;
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                 &lt;sequence&gt;
 *                   &lt;element name="nextCUCUCIFKey" minOccurs="0" form="qualified"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="nextCUCUCIFCompanyID" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCUCUCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCUCUCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                             &lt;element name="nextCUCUCIFTie" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCUCUCIFTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                   &lt;element name="nextCUCUKey" minOccurs="0" form="qualified"&gt;
 *                     &lt;complexType&gt;
 *                       &lt;complexContent&gt;
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                           &lt;sequence&gt;
 *                             &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCUCUCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                             &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
 *                               &lt;complexType&gt;
 *                                 &lt;complexContent&gt;
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *                                     &lt;sequence&gt;
 *                                       &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
 *                                     &lt;/sequence&gt;
 *                                   &lt;/restriction&gt;
 *                                 &lt;/complexContent&gt;
 *                               &lt;/complexType&gt;
 *                             &lt;/element&gt;
 *                             &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *                           &lt;/sequence&gt;
 *                         &lt;/restriction&gt;
 *                       &lt;/complexContent&gt;
 *                     &lt;/complexType&gt;
 *                   &lt;/element&gt;
 *                 &lt;/sequence&gt;
 *               &lt;/restriction&gt;
 *             &lt;/complexContent&gt;
 *           &lt;/complexType&gt;
 *         &lt;/element&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customerInfo",
    "rulesCounter",
    "cucuRulesInfo",
    "cucuNumber",
    "cucuInfo",
    "cucuMoreIndicator",
    "nextCUCUInfo"
})
@XmlRootElement(name = "InquireCUCULinkResponse")
public class InquireCUCULinkResponse
    extends EnterpriseMessageCUCU
{

    protected InquireCUCULinkResponse.CustomerInfo customerInfo;
    protected Integer rulesCounter;
    @XmlElement(name = "CUCURulesInfo")
    protected List<InquireCUCULinkResponse.CUCURulesInfo> cucuRulesInfo;
    @XmlElement(name = "CUCUNumber")
    protected Integer cucuNumber;
    @XmlElement(name = "CUCUInfo")
    protected List<InquireCUCULinkResponse.CUCUInfo> cucuInfo;
    @XmlElement(name = "CUCUMoreIndicator")
    protected String cucuMoreIndicator;
    protected InquireCUCULinkResponse.NextCUCUInfo nextCUCUInfo;

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InquireCUCULinkResponse.CustomerInfo }
     *     
     */
    public InquireCUCULinkResponse.CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquireCUCULinkResponse.CustomerInfo }
     *     
     */
    public void setCustomerInfo(InquireCUCULinkResponse.CustomerInfo value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the rulesCounter property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getRulesCounter() {
        return rulesCounter;
    }

    /**
     * Sets the value of the rulesCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setRulesCounter(Integer value) {
        this.rulesCounter = value;
    }

    /**
     * Gets the value of the cucuRulesInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cucuRulesInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCUCURulesInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InquireCUCULinkResponse.CUCURulesInfo }
     * 
     * 
     */
    public List<InquireCUCULinkResponse.CUCURulesInfo> getCUCURulesInfo() {
        if (cucuRulesInfo == null) {
            cucuRulesInfo = new ArrayList<InquireCUCULinkResponse.CUCURulesInfo>();
        }
        return this.cucuRulesInfo;
    }

    /**
     * Gets the value of the cucuNumber property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCUCUNumber() {
        return cucuNumber;
    }

    /**
     * Sets the value of the cucuNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCUCUNumber(Integer value) {
        this.cucuNumber = value;
    }

    /**
     * Gets the value of the cucuInfo property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cucuInfo property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCUCUInfo().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InquireCUCULinkResponse.CUCUInfo }
     * 
     * 
     */
    public List<InquireCUCULinkResponse.CUCUInfo> getCUCUInfo() {
        if (cucuInfo == null) {
            cucuInfo = new ArrayList<InquireCUCULinkResponse.CUCUInfo>();
        }
        return this.cucuInfo;
    }

    /**
     * Gets the value of the cucuMoreIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCUCUMoreIndicator() {
        return cucuMoreIndicator;
    }

    /**
     * Sets the value of the cucuMoreIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCUCUMoreIndicator(String value) {
        this.cucuMoreIndicator = value;
    }

    /**
     * Gets the value of the nextCUCUInfo property.
     * 
     * @return
     *     possible object is
     *     {@link InquireCUCULinkResponse.NextCUCUInfo }
     *     
     */
    public InquireCUCULinkResponse.NextCUCUInfo getNextCUCUInfo() {
        return nextCUCUInfo;
    }

    /**
     * Sets the value of the nextCUCUInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link InquireCUCULinkResponse.NextCUCUInfo }
     *     
     */
    public void setNextCUCUInfo(InquireCUCULinkResponse.NextCUCUInfo value) {
        this.nextCUCUInfo = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="CUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUCustomerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUCompanyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUUCNNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUIDType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUIDNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUCustomerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUKYCIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="CUCUPercentageOwned" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cucuRelationshipCode",
        "cucuCustomerName",
        "cucuCompanyID",
        "cucuucnNumber",
        "cucuidType",
        "cucuidNumber",
        "cucuCustomerType",
        "cucukycIndicator",
        "cucuPercentageOwned"
    })
    public static class CUCUInfo {

        @XmlElement(name = "CUCURelationshipCode")
        protected String cucuRelationshipCode;
        @XmlElement(name = "CUCUCustomerName")
        protected String cucuCustomerName;
        @XmlElement(name = "CUCUCompanyID")
        protected Integer cucuCompanyID;
        @XmlElement(name = "CUCUUCNNumber")
        protected Integer cucuucnNumber;
        @XmlElement(name = "CUCUIDType")
        protected String cucuidType;
        @XmlElement(name = "CUCUIDNumber")
        protected String cucuidNumber;
        @XmlElement(name = "CUCUCustomerType")
        protected String cucuCustomerType;
        @XmlElement(name = "CUCUKYCIndicator")
        protected String cucukycIndicator;
        @XmlElement(name = "CUCUPercentageOwned")
        protected BigDecimal cucuPercentageOwned;

        /**
         * Gets the value of the cucuRelationshipCode property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCURelationshipCode() {
            return cucuRelationshipCode;
        }

        /**
         * Sets the value of the cucuRelationshipCode property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCURelationshipCode(String value) {
            this.cucuRelationshipCode = value;
        }

        /**
         * Gets the value of the cucuCustomerName property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCUCustomerName() {
            return cucuCustomerName;
        }

        /**
         * Sets the value of the cucuCustomerName property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCUCustomerName(String value) {
            this.cucuCustomerName = value;
        }

        /**
         * Gets the value of the cucuCompanyID property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCUCUCompanyID() {
            return cucuCompanyID;
        }

        /**
         * Sets the value of the cucuCompanyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCUCUCompanyID(Integer value) {
            this.cucuCompanyID = value;
        }

        /**
         * Gets the value of the cucuucnNumber property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCUCUUCNNumber() {
            return cucuucnNumber;
        }

        /**
         * Sets the value of the cucuucnNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCUCUUCNNumber(Integer value) {
            this.cucuucnNumber = value;
        }

        /**
         * Gets the value of the cucuidType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCUIDType() {
            return cucuidType;
        }

        /**
         * Sets the value of the cucuidType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCUIDType(String value) {
            this.cucuidType = value;
        }

        /**
         * Gets the value of the cucuidNumber property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCUIDNumber() {
            return cucuidNumber;
        }

        /**
         * Sets the value of the cucuidNumber property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCUIDNumber(String value) {
            this.cucuidNumber = value;
        }

        /**
         * Gets the value of the cucuCustomerType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCUCustomerType() {
            return cucuCustomerType;
        }

        /**
         * Sets the value of the cucuCustomerType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCUCustomerType(String value) {
            this.cucuCustomerType = value;
        }

        /**
         * Gets the value of the cucukycIndicator property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCUCUKYCIndicator() {
            return cucukycIndicator;
        }

        /**
         * Sets the value of the cucukycIndicator property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCUCUKYCIndicator(String value) {
            this.cucukycIndicator = value;
        }

        /**
         * Gets the value of the cucuPercentageOwned property.
         * 
         * @return
         *     possible object is
         *     {@link BigDecimal }
         *     
         */
        public BigDecimal getCUCUPercentageOwned() {
            return cucuPercentageOwned;
        }

        /**
         * Sets the value of the cucuPercentageOwned property.
         * 
         * @param value
         *     allowed object is
         *     {@link BigDecimal }
         *     
         */
        public void setCUCUPercentageOwned(BigDecimal value) {
            this.cucuPercentageOwned = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customerRelationshipType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="minRelationshipType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="maxRelationshipType" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerRelationshipType",
        "minRelationshipType",
        "maxRelationshipType"
    })
    public static class CUCURulesInfo {

        protected String customerRelationshipType;
        protected Integer minRelationshipType;
        protected Integer maxRelationshipType;

        /**
         * Gets the value of the customerRelationshipType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerRelationshipType() {
            return customerRelationshipType;
        }

        /**
         * Sets the value of the customerRelationshipType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerRelationshipType(String value) {
            this.customerRelationshipType = value;
        }

        /**
         * Gets the value of the minRelationshipType property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMinRelationshipType() {
            return minRelationshipType;
        }

        /**
         * Sets the value of the minRelationshipType property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMinRelationshipType(Integer value) {
            this.minRelationshipType = value;
        }

        /**
         * Gets the value of the maxRelationshipType property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getMaxRelationshipType() {
            return maxRelationshipType;
        }

        /**
         * Sets the value of the maxRelationshipType property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setMaxRelationshipType(Integer value) {
            this.maxRelationshipType = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="customerName1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="customerName2" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="customerType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="customerCompanyID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *         &lt;element name="customerUCN" type="{http://www.w3.org/2001/XMLSchema}int" form="qualified"/&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "customerName1",
        "customerName2",
        "customerType",
        "customerCompanyID",
        "customerUCN"
    })
    public static class CustomerInfo {

        protected String customerName1;
        protected String customerName2;
        protected String customerType;
        protected Integer customerCompanyID;
        protected int customerUCN;

        /**
         * Gets the value of the customerName1 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerName1() {
            return customerName1;
        }

        /**
         * Sets the value of the customerName1 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerName1(String value) {
            this.customerName1 = value;
        }

        /**
         * Gets the value of the customerName2 property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerName2() {
            return customerName2;
        }

        /**
         * Sets the value of the customerName2 property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerName2(String value) {
            this.customerName2 = value;
        }

        /**
         * Gets the value of the customerType property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCustomerType() {
            return customerType;
        }

        /**
         * Sets the value of the customerType property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCustomerType(String value) {
            this.customerType = value;
        }

        /**
         * Gets the value of the customerCompanyID property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getCustomerCompanyID() {
            return customerCompanyID;
        }

        /**
         * Sets the value of the customerCompanyID property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setCustomerCompanyID(Integer value) {
            this.customerCompanyID = value;
        }

        /**
         * Gets the value of the customerUCN property.
         * 
         */
        public int getCustomerUCN() {
            return customerUCN;
        }

        /**
         * Sets the value of the customerUCN property.
         * 
         */
        public void setCustomerUCN(int value) {
            this.customerUCN = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType&gt;
     *   &lt;complexContent&gt;
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *       &lt;sequence&gt;
     *         &lt;element name="nextCUCUCIFKey" minOccurs="0" form="qualified"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="nextCUCUCIFCompanyID" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCUCUCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCUCUCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                   &lt;element name="nextCUCUCIFTie" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCUCUCIFTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *         &lt;element name="nextCUCUKey" minOccurs="0" form="qualified"&gt;
     *           &lt;complexType&gt;
     *             &lt;complexContent&gt;
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                 &lt;sequence&gt;
     *                   &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCUCUCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                   &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
     *                     &lt;complexType&gt;
     *                       &lt;complexContent&gt;
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
     *                           &lt;sequence&gt;
     *                             &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
     *                           &lt;/sequence&gt;
     *                         &lt;/restriction&gt;
     *                       &lt;/complexContent&gt;
     *                     &lt;/complexType&gt;
     *                   &lt;/element&gt;
     *                   &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
     *                 &lt;/sequence&gt;
     *               &lt;/restriction&gt;
     *             &lt;/complexContent&gt;
     *           &lt;/complexType&gt;
     *         &lt;/element&gt;
     *       &lt;/sequence&gt;
     *     &lt;/restriction&gt;
     *   &lt;/complexContent&gt;
     * &lt;/complexType&gt;
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nextCUCUCIFKey",
        "nextCUCUKey"
    })
    public static class NextCUCUInfo {

        protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey nextCUCUCIFKey;
        protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey nextCUCUKey;

        /**
         * Gets the value of the nextCUCUCIFKey property.
         * 
         * @return
         *     possible object is
         *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey }
         *     
         */
        public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey getNextCUCUCIFKey() {
            return nextCUCUCIFKey;
        }

        /**
         * Sets the value of the nextCUCUCIFKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey }
         *     
         */
        public void setNextCUCUCIFKey(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey value) {
            this.nextCUCUCIFKey = value;
        }

        /**
         * Gets the value of the nextCUCUKey property.
         * 
         * @return
         *     possible object is
         *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey }
         *     
         */
        public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey getNextCUCUKey() {
            return nextCUCUKey;
        }

        /**
         * Sets the value of the nextCUCUKey property.
         * 
         * @param value
         *     allowed object is
         *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey }
         *     
         */
        public void setNextCUCUKey(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey value) {
            this.nextCUCUKey = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="nextCUCUCIFCompanyID" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCUCUCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCUCUCIFID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *         &lt;element name="nextCUCUCIFTie" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCUCUCIFTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nextCUCUCIFCompanyID",
            "nextCUCUCIFID",
            "nextCUCUCIFTie"
        })
        public static class NextCUCUCIFKey {

            protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID nextCUCUCIFCompanyID;
            protected String nextCUCUCIFID;
            protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie nextCUCUCIFTie;

            /**
             * Gets the value of the nextCUCUCIFCompanyID property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID }
             *     
             */
            public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID getNextCUCUCIFCompanyID() {
                return nextCUCUCIFCompanyID;
            }

            /**
             * Sets the value of the nextCUCUCIFCompanyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID }
             *     
             */
            public void setNextCUCUCIFCompanyID(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFCompanyID value) {
                this.nextCUCUCIFCompanyID = value;
            }

            /**
             * Gets the value of the nextCUCUCIFID property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNextCUCUCIFID() {
                return nextCUCUCIFID;
            }

            /**
             * Sets the value of the nextCUCUCIFID property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNextCUCUCIFID(String value) {
                this.nextCUCUCIFID = value;
            }

            /**
             * Gets the value of the nextCUCUCIFTie property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie }
             *     
             */
            public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie getNextCUCUCIFTie() {
                return nextCUCUCIFTie;
            }

            /**
             * Sets the value of the nextCUCUCIFTie property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie }
             *     
             */
            public void setNextCUCUCIFTie(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUCIFKey.NextCUCUCIFTie value) {
                this.nextCUCUCIFTie = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCUCUCIFCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCUCUCIFCompanyNumber"
            })
            public static class NextCUCUCIFCompanyID {

                protected Integer nextCUCUCIFCompanyNumber;

                /**
                 * Gets the value of the nextCUCUCIFCompanyNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCUCUCIFCompanyNumber() {
                    return nextCUCUCIFCompanyNumber;
                }

                /**
                 * Sets the value of the nextCUCUCIFCompanyNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCUCUCIFCompanyNumber(Integer value) {
                    this.nextCUCUCIFCompanyNumber = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCUCUCIFTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCUCUCIFTieNumber"
            })
            public static class NextCUCUCIFTie {

                protected Integer nextCUCUCIFTieNumber;

                /**
                 * Gets the value of the nextCUCUCIFTieNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCUCUCIFTieNumber() {
                    return nextCUCUCIFTieNumber;
                }

                /**
                 * Sets the value of the nextCUCUCIFTieNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCUCUCIFTieNumber(Integer value) {
                    this.nextCUCUCIFTieNumber = value;
                }

            }

        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType&gt;
         *   &lt;complexContent&gt;
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *       &lt;sequence&gt;
         *         &lt;element name="nextCUCUCompanyID" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCUCUCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCUCUCustomerID" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *         &lt;element name="nextCUCUTie" minOccurs="0" form="qualified"&gt;
         *           &lt;complexType&gt;
         *             &lt;complexContent&gt;
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
         *                 &lt;sequence&gt;
         *                   &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
         *                 &lt;/sequence&gt;
         *               &lt;/restriction&gt;
         *             &lt;/complexContent&gt;
         *           &lt;/complexType&gt;
         *         &lt;/element&gt;
         *         &lt;element name="nextCUCURelationshipCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
         *       &lt;/sequence&gt;
         *     &lt;/restriction&gt;
         *   &lt;/complexContent&gt;
         * &lt;/complexType&gt;
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "nextCUCUCompanyID",
            "nextCUCUCustomerID",
            "nextCUCUTie",
            "nextCUCURelationshipCode"
        })
        public static class NextCUCUKey {

            protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID nextCUCUCompanyID;
            protected Integer nextCUCUCustomerID;
            protected InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie nextCUCUTie;
            protected String nextCUCURelationshipCode;

            /**
             * Gets the value of the nextCUCUCompanyID property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
             *     
             */
            public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID getNextCUCUCompanyID() {
                return nextCUCUCompanyID;
            }

            /**
             * Sets the value of the nextCUCUCompanyID property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID }
             *     
             */
            public void setNextCUCUCompanyID(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUCompanyID value) {
                this.nextCUCUCompanyID = value;
            }

            /**
             * Gets the value of the nextCUCUCustomerID property.
             * 
             * @return
             *     possible object is
             *     {@link Integer }
             *     
             */
            public Integer getNextCUCUCustomerID() {
                return nextCUCUCustomerID;
            }

            /**
             * Sets the value of the nextCUCUCustomerID property.
             * 
             * @param value
             *     allowed object is
             *     {@link Integer }
             *     
             */
            public void setNextCUCUCustomerID(Integer value) {
                this.nextCUCUCustomerID = value;
            }

            /**
             * Gets the value of the nextCUCUTie property.
             * 
             * @return
             *     possible object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
             *     
             */
            public InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie getNextCUCUTie() {
                return nextCUCUTie;
            }

            /**
             * Sets the value of the nextCUCUTie property.
             * 
             * @param value
             *     allowed object is
             *     {@link InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie }
             *     
             */
            public void setNextCUCUTie(InquireCUCULinkResponse.NextCUCUInfo.NextCUCUKey.NextCUCUTie value) {
                this.nextCUCUTie = value;
            }

            /**
             * Gets the value of the nextCUCURelationshipCode property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNextCUCURelationshipCode() {
                return nextCUCURelationshipCode;
            }

            /**
             * Sets the value of the nextCUCURelationshipCode property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNextCUCURelationshipCode(String value) {
                this.nextCUCURelationshipCode = value;
            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCUCUCompanyNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCUCUCompanyNumber"
            })
            public static class NextCUCUCompanyID {

                protected Integer nextCUCUCompanyNumber;

                /**
                 * Gets the value of the nextCUCUCompanyNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCUCUCompanyNumber() {
                    return nextCUCUCompanyNumber;
                }

                /**
                 * Sets the value of the nextCUCUCompanyNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCUCUCompanyNumber(Integer value) {
                    this.nextCUCUCompanyNumber = value;
                }

            }


            /**
             * <p>Java class for anonymous complex type.
             * 
             * <p>The following schema fragment specifies the expected content contained within this class.
             * 
             * <pre>
             * &lt;complexType&gt;
             *   &lt;complexContent&gt;
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
             *       &lt;sequence&gt;
             *         &lt;element name="nextCUCUTieNumber" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0" form="qualified"/&gt;
             *       &lt;/sequence&gt;
             *     &lt;/restriction&gt;
             *   &lt;/complexContent&gt;
             * &lt;/complexType&gt;
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "nextCUCUTieNumber"
            })
            public static class NextCUCUTie {

                protected Integer nextCUCUTieNumber;

                /**
                 * Gets the value of the nextCUCUTieNumber property.
                 * 
                 * @return
                 *     possible object is
                 *     {@link Integer }
                 *     
                 */
                public Integer getNextCUCUTieNumber() {
                    return nextCUCUTieNumber;
                }

                /**
                 * Sets the value of the nextCUCUTieNumber property.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link Integer }
                 *     
                 */
                public void setNextCUCUTieNumber(Integer value) {
                    this.nextCUCUTieNumber = value;
                }

            }

        }

    }

}
