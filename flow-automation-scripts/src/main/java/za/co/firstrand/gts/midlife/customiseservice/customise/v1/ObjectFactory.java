
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.customiseservice.customise.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.customiseservice.customise.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CustomiseRequest }
     * 
     */
    public CustomiseRequest createCustomiseRequest() {
        return new CustomiseRequest();
    }

    /**
     * Create an instance of {@link HeaderV12 }
     * 
     */
    public HeaderV12 createHeaderV12() {
        return new HeaderV12();
    }

    /**
     * Create an instance of {@link CustomiseResponse }
     * 
     */
    public CustomiseResponse createCustomiseResponse() {
        return new CustomiseResponse();
    }

    /**
     * Create an instance of {@link ASchemaV12 }
     * 
     */
    public ASchemaV12 createASchemaV12() {
        return new ASchemaV12();
    }

    /**
     * Create an instance of {@link EventV12 }
     * 
     */
    public EventV12 createEventV12() {
        return new EventV12();
    }

    /**
     * Create an instance of {@link EventTypeV12 }
     * 
     */
    public EventTypeV12 createEventTypeV12() {
        return new EventTypeV12();
    }

    /**
     * Create an instance of {@link EventparamsV12 }
     * 
     */
    public EventparamsV12 createEventparamsV12() {
        return new EventparamsV12();
    }

    /**
     * Create an instance of {@link ConditionV12 }
     * 
     */
    public ConditionV12 createConditionV12() {
        return new ConditionV12();
    }

    /**
     * Create an instance of {@link ListOfformsV12 }
     * 
     */
    public ListOfformsV12 createListOfformsV12() {
        return new ListOfformsV12();
    }

    /**
     * Create an instance of {@link FormV12 }
     * 
     */
    public FormV12 createFormV12() {
        return new FormV12();
    }

    /**
     * Create an instance of {@link ListOfSectionsV12 }
     * 
     */
    public ListOfSectionsV12 createListOfSectionsV12() {
        return new ListOfSectionsV12();
    }

    /**
     * Create an instance of {@link SectionV12 }
     * 
     */
    public SectionV12 createSectionV12() {
        return new SectionV12();
    }

    /**
     * Create an instance of {@link RulesV12 }
     * 
     */
    public RulesV12 createRulesV12() {
        return new RulesV12();
    }

}
