
package za.co.firstrand.gts.cdm.ecm.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for document complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="document"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="themeData" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}themeData" minOccurs="0"/&gt;
 *         &lt;element name="event" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="outputFormatTypes" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}outputFormatTypes"/&gt;
 *         &lt;element name="templateXmlRef" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="embeddedContents" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}embeddedContents" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "document", propOrder = {
    "themeData",
    "event",
    "outputFormatTypes",
    "templateXmlRef",
    "embeddedContents"
})
public class Document {

    protected ThemeData themeData;
    @XmlElement(required = true)
    protected String event;
    @XmlElement(required = true)
    protected OutputFormatTypes outputFormatTypes;
    @XmlElement(required = true)
    protected String templateXmlRef;
    @XmlElement(nillable = true)
    protected List<EmbeddedContents> embeddedContents;

    /**
     * Gets the value of the themeData property.
     * 
     * @return
     *     possible object is
     *     {@link ThemeData }
     *     
     */
    public ThemeData getThemeData() {
        return themeData;
    }

    /**
     * Sets the value of the themeData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ThemeData }
     *     
     */
    public void setThemeData(ThemeData value) {
        this.themeData = value;
    }

    /**
     * Gets the value of the event property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEvent() {
        return event;
    }

    /**
     * Sets the value of the event property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEvent(String value) {
        this.event = value;
    }

    /**
     * Gets the value of the outputFormatTypes property.
     * 
     * @return
     *     possible object is
     *     {@link OutputFormatTypes }
     *     
     */
    public OutputFormatTypes getOutputFormatTypes() {
        return outputFormatTypes;
    }

    /**
     * Sets the value of the outputFormatTypes property.
     * 
     * @param value
     *     allowed object is
     *     {@link OutputFormatTypes }
     *     
     */
    public void setOutputFormatTypes(OutputFormatTypes value) {
        this.outputFormatTypes = value;
    }

    /**
     * Gets the value of the templateXmlRef property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTemplateXmlRef() {
        return templateXmlRef;
    }

    /**
     * Sets the value of the templateXmlRef property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTemplateXmlRef(String value) {
        this.templateXmlRef = value;
    }

    /**
     * Gets the value of the embeddedContents property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the embeddedContents property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmbeddedContents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EmbeddedContents }
     * 
     * 
     */
    public List<EmbeddedContents> getEmbeddedContents() {
        if (embeddedContents == null) {
            embeddedContents = new ArrayList<EmbeddedContents>();
        }
        return this.embeddedContents;
    }

}
