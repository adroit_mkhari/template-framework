
package za.co.firstrand.gts.cdm.account.v9;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.account.v9 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AccountKey_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/account/v9", "AccountKey");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.account.v9
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AccountKey }
     * 
     */
    public AccountKey createAccountKey() {
        return new AccountKey();
    }

    /**
     * Create an instance of {@link Account }
     * 
     */
    public Account createAccount() {
        return new Account();
    }

    /**
     * Create an instance of {@link Customer }
     * 
     */
    public Customer createCustomer() {
        return new Customer();
    }

    /**
     * Create an instance of {@link CustomerAccountRelationship }
     * 
     */
    public CustomerAccountRelationship createCustomerAccountRelationship() {
        return new CustomerAccountRelationship();
    }

    /**
     * Create an instance of {@link AccountBalance }
     * 
     */
    public AccountBalance createAccountBalance() {
        return new AccountBalance();
    }

    /**
     * Create an instance of {@link CisAccount }
     * 
     */
    public CisAccount createCisAccount() {
        return new CisAccount();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AccountKey }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link AccountKey }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/account/v9", name = "AccountKey")
    public JAXBElement<AccountKey> createAccountKey(AccountKey value) {
        return new JAXBElement<AccountKey>(_AccountKey_QNAME, AccountKey.class, null, value);
    }

}
