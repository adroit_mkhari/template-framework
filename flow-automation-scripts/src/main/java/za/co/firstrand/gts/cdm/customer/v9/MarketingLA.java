
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MarketingLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MarketingLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contactConsent" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="contactByPartnerConsent" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="contactViaCellphone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="contactViaTelephone" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="contactViaSMS" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="contactViaEmail" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="contactViaPost" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="preferredMethodOfCommunication" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MarketingLA", propOrder = {
    "contactConsent",
    "contactByPartnerConsent",
    "contactViaCellphone",
    "contactViaTelephone",
    "contactViaSMS",
    "contactViaEmail",
    "contactViaPost",
    "preferredMethodOfCommunication"
})
public class MarketingLA {

    protected String contactConsent;
    protected Boolean contactByPartnerConsent;
    protected Boolean contactViaCellphone;
    protected Boolean contactViaTelephone;
    protected Boolean contactViaSMS;
    protected Boolean contactViaEmail;
    protected Boolean contactViaPost;
    protected String preferredMethodOfCommunication;

    /**
     * Gets the value of the contactConsent property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getContactConsent() {
        return contactConsent;
    }

    /**
     * Sets the value of the contactConsent property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setContactConsent(String value) {
        this.contactConsent = value;
    }

    /**
     * Gets the value of the contactByPartnerConsent property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactByPartnerConsent() {
        return contactByPartnerConsent;
    }

    /**
     * Sets the value of the contactByPartnerConsent property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactByPartnerConsent(Boolean value) {
        this.contactByPartnerConsent = value;
    }

    /**
     * Gets the value of the contactViaCellphone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactViaCellphone() {
        return contactViaCellphone;
    }

    /**
     * Sets the value of the contactViaCellphone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactViaCellphone(Boolean value) {
        this.contactViaCellphone = value;
    }

    /**
     * Gets the value of the contactViaTelephone property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactViaTelephone() {
        return contactViaTelephone;
    }

    /**
     * Sets the value of the contactViaTelephone property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactViaTelephone(Boolean value) {
        this.contactViaTelephone = value;
    }

    /**
     * Gets the value of the contactViaSMS property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactViaSMS() {
        return contactViaSMS;
    }

    /**
     * Sets the value of the contactViaSMS property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactViaSMS(Boolean value) {
        this.contactViaSMS = value;
    }

    /**
     * Gets the value of the contactViaEmail property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactViaEmail() {
        return contactViaEmail;
    }

    /**
     * Sets the value of the contactViaEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactViaEmail(Boolean value) {
        this.contactViaEmail = value;
    }

    /**
     * Gets the value of the contactViaPost property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isContactViaPost() {
        return contactViaPost;
    }

    /**
     * Sets the value of the contactViaPost property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setContactViaPost(Boolean value) {
        this.contactViaPost = value;
    }

    /**
     * Gets the value of the preferredMethodOfCommunication property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPreferredMethodOfCommunication() {
        return preferredMethodOfCommunication;
    }

    /**
     * Sets the value of the preferredMethodOfCommunication property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPreferredMethodOfCommunication(String value) {
        this.preferredMethodOfCommunication = value;
    }

}
