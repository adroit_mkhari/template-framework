
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerKeyLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerKeyLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="country" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="customerKeyType" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}CustomerKeyTypeLA" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerKeyLA", propOrder = {
    "customerId",
    "country",
    "customerKeyType"
})
public class CustomerKeyLA {

    protected String customerId;
    @XmlElement(required = true)
    protected String country;
    @XmlSchemaType(name = "string")
    protected CustomerKeyTypeLA customerKeyType;

    /**
     * Gets the value of the customerId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerId() {
        return customerId;
    }

    /**
     * Sets the value of the customerId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerId(String value) {
        this.customerId = value;
    }

    /**
     * Gets the value of the country property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountry() {
        return country;
    }

    /**
     * Sets the value of the country property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountry(String value) {
        this.country = value;
    }

    /**
     * Gets the value of the customerKeyType property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKeyTypeLA }
     *     
     */
    public CustomerKeyTypeLA getCustomerKeyType() {
        return customerKeyType;
    }

    /**
     * Sets the value of the customerKeyType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKeyTypeLA }
     *     
     */
    public void setCustomerKeyType(CustomerKeyTypeLA value) {
        this.customerKeyType = value;
    }

}
