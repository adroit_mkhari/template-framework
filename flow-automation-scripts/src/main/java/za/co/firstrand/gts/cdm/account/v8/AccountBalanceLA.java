
package za.co.firstrand.gts.cdm.account.v8;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.datatypes.v4.AmountLA;


/**
 * <p>Java class for AccountBalanceLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AccountBalanceLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="balanceAmount" type="{http://www.firstrand.co.za/gts/cdm/dataTypes/v4}AmountLA" form="qualified"/&gt;
 *         &lt;element name="balanceSign" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="balanceType" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AccountBalanceLA", propOrder = {
    "balanceAmount",
    "balanceSign",
    "balanceType"
})
public class AccountBalanceLA {

    @XmlElement(required = true)
    protected AmountLA balanceAmount;
    @XmlElement(required = true)
    protected String balanceSign;
    @XmlElement(required = true)
    protected String balanceType;

    /**
     * Gets the value of the balanceAmount property.
     * 
     * @return
     *     possible object is
     *     {@link AmountLA }
     *     
     */
    public AmountLA getBalanceAmount() {
        return balanceAmount;
    }

    /**
     * Sets the value of the balanceAmount property.
     * 
     * @param value
     *     allowed object is
     *     {@link AmountLA }
     *     
     */
    public void setBalanceAmount(AmountLA value) {
        this.balanceAmount = value;
    }

    /**
     * Gets the value of the balanceSign property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceSign() {
        return balanceSign;
    }

    /**
     * Sets the value of the balanceSign property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceSign(String value) {
        this.balanceSign = value;
    }

    /**
     * Gets the value of the balanceType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBalanceType() {
        return balanceType;
    }

    /**
     * Sets the value of the balanceType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBalanceType(String value) {
        this.balanceType = value;
    }

}
