
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessageV12;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageV1.2"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="platformHeader" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}headerV1.2" form="qualified"/&gt;
 *         &lt;element name="applicationStatus" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}applicationStatusesV1.2" form="qualified"/&gt;
 *         &lt;element name="schema" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}aSchemaV1.2" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "platformHeader",
    "applicationStatus",
    "schema"
})
@XmlRootElement(name = "CustomiseResponse")
public class CustomiseResponse
    extends EnterpriseMessageV12
{

    @XmlElement(required = true)
    protected HeaderV12 platformHeader;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ApplicationStatusesV12 applicationStatus;
    @XmlElement(required = true)
    protected ASchemaV12 schema;

    /**
     * Gets the value of the platformHeader property.
     * 
     * @return
     *     possible object is
     *     {@link HeaderV12 }
     *     
     */
    public HeaderV12 getPlatformHeader() {
        return platformHeader;
    }

    /**
     * Sets the value of the platformHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link HeaderV12 }
     *     
     */
    public void setPlatformHeader(HeaderV12 value) {
        this.platformHeader = value;
    }

    /**
     * Gets the value of the applicationStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ApplicationStatusesV12 }
     *     
     */
    public ApplicationStatusesV12 getApplicationStatus() {
        return applicationStatus;
    }

    /**
     * Sets the value of the applicationStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ApplicationStatusesV12 }
     *     
     */
    public void setApplicationStatus(ApplicationStatusesV12 value) {
        this.applicationStatus = value;
    }

    /**
     * Gets the value of the schema property.
     * 
     * @return
     *     possible object is
     *     {@link ASchemaV12 }
     *     
     */
    public ASchemaV12 getSchema() {
        return schema;
    }

    /**
     * Sets the value of the schema property.
     * 
     * @param value
     *     allowed object is
     *     {@link ASchemaV12 }
     *     
     */
    public void setSchema(ASchemaV12 value) {
        this.schema = value;
    }

}
