
package za.co.firstrand.gts.cdm.datatypes.v6;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.datatypes.v6 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.datatypes.v6
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link Addressv7 }
     * 
     */
    public Addressv7 createAddressv7() {
        return new Addressv7();
    }

    /**
     * Create an instance of {@link Errorv7 }
     * 
     */
    public Errorv7 createErrorv7() {
        return new Errorv7();
    }

    /**
     * Create an instance of {@link EmailAddressv7 }
     * 
     */
    public EmailAddressv7 createEmailAddressv7() {
        return new EmailAddressv7();
    }

    /**
     * Create an instance of {@link Amountv7 }
     * 
     */
    public Amountv7 createAmountv7() {
        return new Amountv7();
    }

    /**
     * Create an instance of {@link TelephoneNumberv7 }
     * 
     */
    public TelephoneNumberv7 createTelephoneNumberv7() {
        return new TelephoneNumberv7();
    }

}
