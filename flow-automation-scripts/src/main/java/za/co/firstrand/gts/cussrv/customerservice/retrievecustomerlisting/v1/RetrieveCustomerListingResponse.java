
package za.co.firstrand.gts.cussrv.customerservice.retrievecustomerlisting.v1;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="moreCustomersInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="nextCustomerKey" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/RetrieveCustomerListing/v1}CustomerKey" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="customersCounter" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="customes" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/RetrieveCustomerListing/v1}Customers" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "moreCustomersInd",
    "nextCustomerKey",
    "customersCounter",
    "customes"
})
@XmlRootElement(name = "RetrieveCustomerListingResponse")
public class RetrieveCustomerListingResponse
    extends EnterpriseMessage
{

    protected String moreCustomersInd;
    protected CustomerKey nextCustomerKey;
    protected BigInteger customersCounter;
    @XmlElement(required = true)
    protected List<Customers> customes;

    /**
     * Gets the value of the moreCustomersInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMoreCustomersInd() {
        return moreCustomersInd;
    }

    /**
     * Sets the value of the moreCustomersInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMoreCustomersInd(String value) {
        this.moreCustomersInd = value;
    }

    /**
     * Gets the value of the nextCustomerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKey }
     *     
     */
    public CustomerKey getNextCustomerKey() {
        return nextCustomerKey;
    }

    /**
     * Sets the value of the nextCustomerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKey }
     *     
     */
    public void setNextCustomerKey(CustomerKey value) {
        this.nextCustomerKey = value;
    }

    /**
     * Gets the value of the customersCounter property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getCustomersCounter() {
        return customersCounter;
    }

    /**
     * Sets the value of the customersCounter property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setCustomersCounter(BigInteger value) {
        this.customersCounter = value;
    }

    /**
     * Gets the value of the customes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the customes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCustomes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Customers }
     * 
     * 
     */
    public List<Customers> getCustomes() {
        if (customes == null) {
            customes = new ArrayList<Customers>();
        }
        return this.customes;
    }

}
