
package za.co.firstrand.gts.accsrv.accountservice.listaccounts.v3;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v9.CustomerKeyLA;


/**
 * <p>Java class for ByCustomerKey complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ByCustomerKey"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerKey" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}CustomerKeyLA" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ByCustomerKey", propOrder = {
    "customerKey"
})
public class ByCustomerKey {

    @XmlElement(required = true)
    protected CustomerKeyLA customerKey;

    /**
     * Gets the value of the customerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKeyLA }
     *     
     */
    public CustomerKeyLA getCustomerKey() {
        return customerKey;
    }

    /**
     * Sets the value of the customerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKeyLA }
     *     
     */
    public void setCustomerKey(CustomerKeyLA value) {
        this.customerKey = value;
    }

}
