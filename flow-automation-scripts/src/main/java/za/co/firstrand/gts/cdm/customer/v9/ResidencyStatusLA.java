
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResidencyStatusLA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResidencyStatusLA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="ASYLUM_SEEKER"/&gt;
 *     &lt;enumeration value="EMIGRANT"/&gt;
 *     &lt;enumeration value="FOREIGN_NATIONAL"/&gt;
 *     &lt;enumeration value="NON_RESIDENT"/&gt;
 *     &lt;enumeration value="REFUGEE"/&gt;
 *     &lt;enumeration value="RESIDENT"/&gt;
 *     &lt;enumeration value="SA_TEMPORARY_ABROAD"/&gt;
 *     &lt;enumeration value="COMMON_MONETARY_AREA"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ResidencyStatusLA")
@XmlEnum
public enum ResidencyStatusLA {

    ASYLUM_SEEKER,
    EMIGRANT,
    FOREIGN_NATIONAL,
    NON_RESIDENT,
    REFUGEE,
    RESIDENT,
    SA_TEMPORARY_ABROAD,
    COMMON_MONETARY_AREA;

    public String value() {
        return name();
    }

    public static ResidencyStatusLA fromValue(String v) {
        return valueOf(v);
    }

}
