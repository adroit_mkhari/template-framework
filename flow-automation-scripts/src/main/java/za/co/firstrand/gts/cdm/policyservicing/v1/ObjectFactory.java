
package za.co.firstrand.gts.cdm.policyservicing.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1.RetrievePolicyDetailsRequest;
import za.co.firstrand.gts.midlife.policymaintenanceservice.retrievepolicydetails.v1.RetrievePolicyDetailsResponse;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.policyservicing.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrievePolicyDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/policyservicing/v1", "retrievePolicyDetailsRequest");
    private final static QName _RetrievePolicyDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/cdm/policyservicing/v1", "retrievePolicyDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.policyservicing.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link PolicyDetails }
     * 
     */
    public PolicyDetails createPolicyDetails() {
        return new PolicyDetails();
    }

    /**
     * Create an instance of {@link BankDetails }
     * 
     */
    public BankDetails createBankDetails() {
        return new BankDetails();
    }

    /**
     * Create an instance of {@link PayorDetails }
     * 
     */
    public PayorDetails createPayorDetails() {
        return new PayorDetails();
    }

    /**
     * Create an instance of {@link Beneficiary }
     * 
     */
    public Beneficiary createBeneficiary() {
        return new Beneficiary();
    }

    /**
     * Create an instance of {@link Role }
     * 
     */
    public Role createRole() {
        return new Role();
    }

    /**
     * Create an instance of {@link CoverDetails }
     * 
     */
    public CoverDetails createCoverDetails() {
        return new CoverDetails();
    }

    /**
     * Create an instance of {@link ContactDetails }
     * 
     */
    public ContactDetails createContactDetails() {
        return new ContactDetails();
    }

    /**
     * Create an instance of {@link Address }
     * 
     */
    public Address createAddress() {
        return new Address();
    }

    /**
     * Create an instance of {@link ExtendedDetails }
     * 
     */
    public ExtendedDetails createExtendedDetails() {
        return new ExtendedDetails();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/policyservicing/v1", name = "retrievePolicyDetailsRequest")
    public JAXBElement<RetrievePolicyDetailsRequest> createRetrievePolicyDetailsRequest(RetrievePolicyDetailsRequest value) {
        return new JAXBElement<RetrievePolicyDetailsRequest>(_RetrievePolicyDetailsRequest_QNAME, RetrievePolicyDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePolicyDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/cdm/policyservicing/v1", name = "retrievePolicyDetailsResponse")
    public JAXBElement<RetrievePolicyDetailsResponse> createRetrievePolicyDetailsResponse(RetrievePolicyDetailsResponse value) {
        return new JAXBElement<RetrievePolicyDetailsResponse>(_RetrievePolicyDetailsResponse_QNAME, RetrievePolicyDetailsResponse.class, null, value);
    }

}
