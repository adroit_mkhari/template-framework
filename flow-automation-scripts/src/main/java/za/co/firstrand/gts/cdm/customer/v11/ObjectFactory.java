
package za.co.firstrand.gts.cdm.customer.v11;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.customer.v11 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.customer.v11
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment createAbstractPartyForeignTaxIDSegment() {
        return new za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment();
    }

    /**
     * Create an instance of {@link ForeignTax }
     * 
     */
    public ForeignTax createForeignTax() {
        return new ForeignTax();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment createForeignTaxForeignTaxIDSegment() {
        return new ForeignTax.ForeignTaxIDSegment();
    }

    /**
     * Create an instance of {@link Customer2 }
     * 
     */
    public Customer2 createCustomer2() {
        return new Customer2();
    }

    /**
     * Create an instance of {@link CustomerKey }
     * 
     */
    public CustomerKey createCustomerKey() {
        return new CustomerKey();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

    /**
     * Create an instance of {@link SourceOfFunds }
     * 
     */
    public SourceOfFunds createSourceOfFunds() {
        return new SourceOfFunds();
    }

    /**
     * Create an instance of {@link ContactDetails }
     * 
     */
    public ContactDetails createContactDetails() {
        return new ContactDetails();
    }

    /**
     * Create an instance of {@link Marketing }
     * 
     */
    public Marketing createMarketing() {
        return new Marketing();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v11.Party }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v11.Party createParty() {
        return new za.co.firstrand.gts.cdm.customer.v11.Party();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link Organisation }
     * 
     */
    public Organisation createOrganisation() {
        return new Organisation();
    }

    /**
     * Create an instance of {@link Segment }
     * 
     */
    public Segment createSegment() {
        return new Segment();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey createAbstractPartyForeignTaxIDSegmentForeignTaxIDKey() {
        return new za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDKey();
    }

    /**
     * Create an instance of {@link za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData }
     * 
     */
    public za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData createAbstractPartyForeignTaxIDSegmentForeignTaxIDData() {
        return new za.co.firstrand.gts.cdm.customer.v11.AbstractParty.ForeignTaxIDSegment.ForeignTaxIDData();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey createForeignTaxForeignTaxIDSegmentForeignTaxIDKey() {
        return new ForeignTax.ForeignTaxIDSegment.ForeignTaxIDKey();
    }

    /**
     * Create an instance of {@link ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData }
     * 
     */
    public ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData createForeignTaxForeignTaxIDSegmentForeignTaxIDData() {
        return new ForeignTax.ForeignTaxIDSegment.ForeignTaxIDData();
    }

    /**
     * Create an instance of {@link Customer2 .Party }
     * 
     */
    public Customer2 .Party createCustomer2Party() {
        return new Customer2 .Party();
    }

}
