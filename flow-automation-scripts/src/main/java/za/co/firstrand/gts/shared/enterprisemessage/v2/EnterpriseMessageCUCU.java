
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1.InquireCUCULinkRequest;
import za.co.firstrand.gts.cussrv.customerservice.inquirecuculink.v1.InquireCUCULinkResponse;


/**
 * <p>Java class for EnterpriseMessageCUCU complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="EnterpriseMessageCUCU"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessageHeaderCUCU"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "EnterpriseMessageCUCU", propOrder = {
    "header"
})
@XmlSeeAlso({
    InquireCUCULinkResponse.class,
    InquireCUCULinkRequest.class
})
public class EnterpriseMessageCUCU {

    @XmlElement(required = true)
    protected EnterpriseMessageHeaderCUCU header;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link EnterpriseMessageHeaderCUCU }
     *     
     */
    public EnterpriseMessageHeaderCUCU getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link EnterpriseMessageHeaderCUCU }
     *     
     */
    public void setHeader(EnterpriseMessageHeaderCUCU value) {
        this.header = value;
    }

}
