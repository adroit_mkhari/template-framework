
package za.co.firstrand.gts.shared.enterprisemessage.v2;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ResultStatusLA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="ResultStatusLA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="SUCCESS"/&gt;
 *     &lt;enumeration value="FAILURE"/&gt;
 *     &lt;enumeration value="REJECTED"/&gt;
 *     &lt;enumeration value="UNKNOWN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "ResultStatusLA")
@XmlEnum
public enum ResultStatusLA {

    SUCCESS,
    FAILURE,
    REJECTED,
    UNKNOWN;

    public String value() {
        return name();
    }

    public static ResultStatusLA fromValue(String v) {
        return valueOf(v);
    }

}
