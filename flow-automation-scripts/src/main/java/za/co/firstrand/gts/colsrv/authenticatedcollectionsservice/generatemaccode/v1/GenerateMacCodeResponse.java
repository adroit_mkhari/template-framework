
package za.co.firstrand.gts.colsrv.authenticatedcollectionsservice.generatemaccode.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for GenerateMacCodeResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GenerateMacCodeResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="macCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GenerateMacCodeResponse", propOrder = {
    "macCode"
})
public class GenerateMacCodeResponse
    extends EnterpriseMessage
{

    protected String macCode;

    /**
     * Gets the value of the macCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMacCode() {
        return macCode;
    }

    /**
     * Sets the value of the macCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMacCode(String value) {
        this.macCode = value;
    }

}
