
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for CustomerKeyTypeLA.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="CustomerKeyTypeLA"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="UCN"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "CustomerKeyTypeLA")
@XmlEnum
public enum CustomerKeyTypeLA {

    UCN;

    public String value() {
        return name();
    }

    public static CustomerKeyTypeLA fromValue(String v) {
        return valueOf(v);
    }

}
