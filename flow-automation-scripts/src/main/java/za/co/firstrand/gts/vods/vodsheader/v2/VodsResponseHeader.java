
package za.co.firstrand.gts.vods.vodsheader.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for VodsResponseHeader complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="VodsResponseHeader"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="vodsTraceId" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="outOfSequenceIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="standInResponse" type="{http://www.w3.org/2001/XMLSchema}boolean" form="qualified"/&gt;
 *         &lt;element name="portalTransactionCode" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *         &lt;element name="stimulusIdentifier" type="{http://www.w3.org/2001/XMLSchema}string" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "VodsResponseHeader", propOrder = {
    "vodsTraceId",
    "outOfSequenceIndicator",
    "standInResponse",
    "portalTransactionCode",
    "stimulusIdentifier"
})
public class VodsResponseHeader {

    @XmlElement(required = true)
    protected String vodsTraceId;
    protected boolean outOfSequenceIndicator;
    protected boolean standInResponse;
    @XmlElement(required = true)
    protected String portalTransactionCode;
    @XmlElement(required = true)
    protected String stimulusIdentifier;

    /**
     * Gets the value of the vodsTraceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVodsTraceId() {
        return vodsTraceId;
    }

    /**
     * Sets the value of the vodsTraceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVodsTraceId(String value) {
        this.vodsTraceId = value;
    }

    /**
     * Gets the value of the outOfSequenceIndicator property.
     * 
     */
    public boolean isOutOfSequenceIndicator() {
        return outOfSequenceIndicator;
    }

    /**
     * Sets the value of the outOfSequenceIndicator property.
     * 
     */
    public void setOutOfSequenceIndicator(boolean value) {
        this.outOfSequenceIndicator = value;
    }

    /**
     * Gets the value of the standInResponse property.
     * 
     */
    public boolean isStandInResponse() {
        return standInResponse;
    }

    /**
     * Sets the value of the standInResponse property.
     * 
     */
    public void setStandInResponse(boolean value) {
        this.standInResponse = value;
    }

    /**
     * Gets the value of the portalTransactionCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPortalTransactionCode() {
        return portalTransactionCode;
    }

    /**
     * Sets the value of the portalTransactionCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPortalTransactionCode(String value) {
        this.portalTransactionCode = value;
    }

    /**
     * Gets the value of the stimulusIdentifier property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStimulusIdentifier() {
        return stimulusIdentifier;
    }

    /**
     * Sets the value of the stimulusIdentifier property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStimulusIdentifier(String value) {
        this.stimulusIdentifier = value;
    }

}
