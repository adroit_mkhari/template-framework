
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Racev12.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="Racev12"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="WHITE"/&gt;
 *     &lt;enumeration value="ASIAN_INDIAN"/&gt;
 *     &lt;enumeration value="COLOURED"/&gt;
 *     &lt;enumeration value="BLACK"/&gt;
 *     &lt;enumeration value="OTHER"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "Racev12")
@XmlEnum
public enum Racev12 {

    WHITE,
    ASIAN_INDIAN,
    COLOURED,
    BLACK,
    OTHER;

    public String value() {
        return name();
    }

    public static Racev12 fromValue(String v) {
        return valueOf(v);
    }

}
