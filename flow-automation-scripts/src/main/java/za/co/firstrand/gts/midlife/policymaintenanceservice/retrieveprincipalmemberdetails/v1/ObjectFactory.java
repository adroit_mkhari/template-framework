
package za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RetrievePrincipalMemberDetailsRequest_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePrincipalMemberDetails/v1", "RetrievePrincipalMemberDetailsRequest");
    private final static QName _RetrievePrincipalMemberDetailsResponse_QNAME = new QName("http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePrincipalMemberDetails/v1", "RetrievePrincipalMemberDetailsResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.midlife.policymaintenanceservice.retrieveprincipalmemberdetails.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RetrievePrincipalMemberDetailsRequest }
     * 
     */
    public RetrievePrincipalMemberDetailsRequest createRetrievePrincipalMemberDetailsRequest() {
        return new RetrievePrincipalMemberDetailsRequest();
    }

    /**
     * Create an instance of {@link RetrievePrincipalMemberDetailsResponse }
     * 
     */
    public RetrievePrincipalMemberDetailsResponse createRetrievePrincipalMemberDetailsResponse() {
        return new RetrievePrincipalMemberDetailsResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePrincipalMemberDetails/v1", name = "RetrievePrincipalMemberDetailsRequest")
    public JAXBElement<RetrievePrincipalMemberDetailsRequest> createRetrievePrincipalMemberDetailsRequest(RetrievePrincipalMemberDetailsRequest value) {
        return new JAXBElement<RetrievePrincipalMemberDetailsRequest>(_RetrievePrincipalMemberDetailsRequest_QNAME, RetrievePrincipalMemberDetailsRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RetrievePrincipalMemberDetailsResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/midlife/PolicyMaintenanceService/RetrievePrincipalMemberDetails/v1", name = "RetrievePrincipalMemberDetailsResponse")
    public JAXBElement<RetrievePrincipalMemberDetailsResponse> createRetrievePrincipalMemberDetailsResponse(RetrievePrincipalMemberDetailsResponse value) {
        return new JAXBElement<RetrievePrincipalMemberDetailsResponse>(_RetrievePrincipalMemberDetailsResponse_QNAME, RetrievePrincipalMemberDetailsResponse.class, null, value);
    }

}
