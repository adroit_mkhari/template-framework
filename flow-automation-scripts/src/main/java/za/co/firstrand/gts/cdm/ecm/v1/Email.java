
package za.co.firstrand.gts.cdm.ecm.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Email complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Email"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="fromMailAddress" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="toMailAddressList" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}mailAddressList"/&gt;
 *         &lt;element name="ccMailAddressList" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}mailAddressList" minOccurs="0"/&gt;
 *         &lt;element name="bccMailAddressList" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}mailAddressList" minOccurs="0"/&gt;
 *         &lt;element name="importance" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}emailImportance" minOccurs="0"/&gt;
 *         &lt;element name="sensitivity" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}emailSensitivity" minOccurs="0"/&gt;
 *         &lt;element name="emailSubject" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="emailBody" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}emailBody"/&gt;
 *         &lt;element name="emailAttachments" type="{http://www.firstrand.co.za/gts/cdm/ecm/v1}emailAttachments" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Email", propOrder = {
    "fromMailAddress",
    "toMailAddressList",
    "ccMailAddressList",
    "bccMailAddressList",
    "importance",
    "sensitivity",
    "emailSubject",
    "emailBody",
    "emailAttachments"
})
public class Email {

    @XmlElement(required = true)
    protected String fromMailAddress;
    @XmlElement(required = true)
    protected MailAddressList toMailAddressList;
    protected MailAddressList ccMailAddressList;
    protected MailAddressList bccMailAddressList;
    @XmlSchemaType(name = "string")
    protected EmailImportance importance;
    @XmlSchemaType(name = "string")
    protected EmailSensitivity sensitivity;
    @XmlElement(required = true)
    protected String emailSubject;
    @XmlElement(required = true)
    protected EmailBody emailBody;
    protected EmailAttachments emailAttachments;

    /**
     * Gets the value of the fromMailAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFromMailAddress() {
        return fromMailAddress;
    }

    /**
     * Sets the value of the fromMailAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFromMailAddress(String value) {
        this.fromMailAddress = value;
    }

    /**
     * Gets the value of the toMailAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link MailAddressList }
     *     
     */
    public MailAddressList getToMailAddressList() {
        return toMailAddressList;
    }

    /**
     * Sets the value of the toMailAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailAddressList }
     *     
     */
    public void setToMailAddressList(MailAddressList value) {
        this.toMailAddressList = value;
    }

    /**
     * Gets the value of the ccMailAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link MailAddressList }
     *     
     */
    public MailAddressList getCcMailAddressList() {
        return ccMailAddressList;
    }

    /**
     * Sets the value of the ccMailAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailAddressList }
     *     
     */
    public void setCcMailAddressList(MailAddressList value) {
        this.ccMailAddressList = value;
    }

    /**
     * Gets the value of the bccMailAddressList property.
     * 
     * @return
     *     possible object is
     *     {@link MailAddressList }
     *     
     */
    public MailAddressList getBccMailAddressList() {
        return bccMailAddressList;
    }

    /**
     * Sets the value of the bccMailAddressList property.
     * 
     * @param value
     *     allowed object is
     *     {@link MailAddressList }
     *     
     */
    public void setBccMailAddressList(MailAddressList value) {
        this.bccMailAddressList = value;
    }

    /**
     * Gets the value of the importance property.
     * 
     * @return
     *     possible object is
     *     {@link EmailImportance }
     *     
     */
    public EmailImportance getImportance() {
        return importance;
    }

    /**
     * Sets the value of the importance property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailImportance }
     *     
     */
    public void setImportance(EmailImportance value) {
        this.importance = value;
    }

    /**
     * Gets the value of the sensitivity property.
     * 
     * @return
     *     possible object is
     *     {@link EmailSensitivity }
     *     
     */
    public EmailSensitivity getSensitivity() {
        return sensitivity;
    }

    /**
     * Sets the value of the sensitivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailSensitivity }
     *     
     */
    public void setSensitivity(EmailSensitivity value) {
        this.sensitivity = value;
    }

    /**
     * Gets the value of the emailSubject property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailSubject() {
        return emailSubject;
    }

    /**
     * Sets the value of the emailSubject property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailSubject(String value) {
        this.emailSubject = value;
    }

    /**
     * Gets the value of the emailBody property.
     * 
     * @return
     *     possible object is
     *     {@link EmailBody }
     *     
     */
    public EmailBody getEmailBody() {
        return emailBody;
    }

    /**
     * Sets the value of the emailBody property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailBody }
     *     
     */
    public void setEmailBody(EmailBody value) {
        this.emailBody = value;
    }

    /**
     * Gets the value of the emailAttachments property.
     * 
     * @return
     *     possible object is
     *     {@link EmailAttachments }
     *     
     */
    public EmailAttachments getEmailAttachments() {
        return emailAttachments;
    }

    /**
     * Sets the value of the emailAttachments property.
     * 
     * @param value
     *     allowed object is
     *     {@link EmailAttachments }
     *     
     */
    public void setEmailAttachments(EmailAttachments value) {
        this.emailAttachments = value;
    }

}
