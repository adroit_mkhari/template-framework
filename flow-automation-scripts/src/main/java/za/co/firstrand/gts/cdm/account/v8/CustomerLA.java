
package za.co.firstrand.gts.cdm.account.v8;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v9.CustomerKeyLA;


/**
 * <p>Java class for CustomerLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="CustomerLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerKey" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}CustomerKeyLA" form="qualified"/&gt;
 *         &lt;element name="relationshipType" type="{http://www.firstrand.co.za/gts/cdm/account/v8}CustomerAccountRelationshipLA" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "CustomerLA", propOrder = {
    "customerKey",
    "relationshipType"
})
public class CustomerLA {

    @XmlElement(required = true)
    protected CustomerKeyLA customerKey;
    protected CustomerAccountRelationshipLA relationshipType;

    /**
     * Gets the value of the customerKey property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerKeyLA }
     *     
     */
    public CustomerKeyLA getCustomerKey() {
        return customerKey;
    }

    /**
     * Sets the value of the customerKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerKeyLA }
     *     
     */
    public void setCustomerKey(CustomerKeyLA value) {
        this.customerKey = value;
    }

    /**
     * Gets the value of the relationshipType property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerAccountRelationshipLA }
     *     
     */
    public CustomerAccountRelationshipLA getRelationshipType() {
        return relationshipType;
    }

    /**
     * Sets the value of the relationshipType property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerAccountRelationshipLA }
     *     
     */
    public void setRelationshipType(CustomerAccountRelationshipLA value) {
        this.relationshipType = value;
    }

}
