
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaritalStatus.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="MaritalStatus"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="DIVORCED"/&gt;
 *     &lt;enumeration value="MARRIED"/&gt;
 *     &lt;enumeration value="SEPARATED"/&gt;
 *     &lt;enumeration value="UNMARRIED"/&gt;
 *     &lt;enumeration value="WIDOWED"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "MaritalStatus")
@XmlEnum
public enum MaritalStatus {

    DIVORCED,
    MARRIED,
    SEPARATED,
    UNMARRIED,
    WIDOWED;

    public String value() {
        return name();
    }

    public static MaritalStatus fromValue(String v) {
        return valueOf(v);
    }

}
