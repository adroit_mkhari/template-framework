
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for rulesV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="rulesV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="conditions" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}conditionV1.2" form="qualified"/&gt;
 *         &lt;element name="anEvent" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}eventV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "rulesV1.2", propOrder = {
    "conditions",
    "anEvent"
})
public class RulesV12 {

    @XmlElement(required = true)
    protected ConditionV12 conditions;
    @XmlElement(required = true)
    protected List<EventV12> anEvent;

    /**
     * Gets the value of the conditions property.
     * 
     * @return
     *     possible object is
     *     {@link ConditionV12 }
     *     
     */
    public ConditionV12 getConditions() {
        return conditions;
    }

    /**
     * Sets the value of the conditions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConditionV12 }
     *     
     */
    public void setConditions(ConditionV12 value) {
        this.conditions = value;
    }

    /**
     * Gets the value of the anEvent property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the anEvent property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnEvent().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventV12 }
     * 
     * 
     */
    public List<EventV12> getAnEvent() {
        if (anEvent == null) {
            anEvent = new ArrayList<EventV12>();
        }
        return this.anEvent;
    }

}
