
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.cdm.customer.v9 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.cdm.customer.v9
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Customer1LA }
     * 
     */
    public Customer1LA createCustomer1LA() {
        return new Customer1LA();
    }

    /**
     * Create an instance of {@link CustomerKeyLA }
     * 
     */
    public CustomerKeyLA createCustomerKeyLA() {
        return new CustomerKeyLA();
    }

    /**
     * Create an instance of {@link PartyLA }
     * 
     */
    public PartyLA createPartyLA() {
        return new PartyLA();
    }

    /**
     * Create an instance of {@link PersonLA }
     * 
     */
    public PersonLA createPersonLA() {
        return new PersonLA();
    }

    /**
     * Create an instance of {@link ContactDetailsLA }
     * 
     */
    public ContactDetailsLA createContactDetailsLA() {
        return new ContactDetailsLA();
    }

    /**
     * Create an instance of {@link MarketingLA }
     * 
     */
    public MarketingLA createMarketingLA() {
        return new MarketingLA();
    }

    /**
     * Create an instance of {@link IdentificationLA }
     * 
     */
    public IdentificationLA createIdentificationLA() {
        return new IdentificationLA();
    }

    /**
     * Create an instance of {@link SourceOfFundsLA }
     * 
     */
    public SourceOfFundsLA createSourceOfFundsLA() {
        return new SourceOfFundsLA();
    }

    /**
     * Create an instance of {@link OrganisationLA }
     * 
     */
    public OrganisationLA createOrganisationLA() {
        return new OrganisationLA();
    }

    /**
     * Create an instance of {@link SegmentLA }
     * 
     */
    public SegmentLA createSegmentLA() {
        return new SegmentLA();
    }

    /**
     * Create an instance of {@link Customer1 }
     * 
     */
    public Customer1 createCustomer1() {
        return new Customer1();
    }

    /**
     * Create an instance of {@link CustomerKey }
     * 
     */
    public CustomerKey createCustomerKey() {
        return new CustomerKey();
    }

    /**
     * Create an instance of {@link Party }
     * 
     */
    public Party createParty() {
        return new Party();
    }

    /**
     * Create an instance of {@link Person }
     * 
     */
    public Person createPerson() {
        return new Person();
    }

    /**
     * Create an instance of {@link ContactDetails }
     * 
     */
    public ContactDetails createContactDetails() {
        return new ContactDetails();
    }

    /**
     * Create an instance of {@link Marketing }
     * 
     */
    public Marketing createMarketing() {
        return new Marketing();
    }

    /**
     * Create an instance of {@link Organisation }
     * 
     */
    public Organisation createOrganisation() {
        return new Organisation();
    }

    /**
     * Create an instance of {@link Segment }
     * 
     */
    public Segment createSegment() {
        return new Segment();
    }

    /**
     * Create an instance of {@link SourceOfFunds }
     * 
     */
    public SourceOfFunds createSourceOfFunds() {
        return new SourceOfFunds();
    }

    /**
     * Create an instance of {@link Identification }
     * 
     */
    public Identification createIdentification() {
        return new Identification();
    }

}
