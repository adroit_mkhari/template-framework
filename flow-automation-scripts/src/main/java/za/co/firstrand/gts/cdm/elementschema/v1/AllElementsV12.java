
package za.co.firstrand.gts.cdm.elementschema.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for allElementsV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="allElementsV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="selectItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}selectV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="inputItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}inputV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="blobItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}blobV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="textItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}textV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="linkItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}linkV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="dateItem" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}dateV1.2" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "allElementsV1.2", propOrder = {
    "selectItem",
    "inputItem",
    "blobItem",
    "textItem",
    "linkItem",
    "dateItem"
})
public class AllElementsV12 {

    @XmlElement(nillable = true)
    protected List<SelectV12> selectItem;
    @XmlElement(nillable = true)
    protected List<InputV12> inputItem;
    @XmlElement(nillable = true)
    protected List<BlobV12> blobItem;
    @XmlElement(nillable = true)
    protected List<TextV12> textItem;
    @XmlElement(nillable = true)
    protected List<LinkV12> linkItem;
    @XmlElement(nillable = true)
    protected List<DateV12> dateItem;

    /**
     * Gets the value of the selectItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the selectItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSelectItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SelectV12 }
     * 
     * 
     */
    public List<SelectV12> getSelectItem() {
        if (selectItem == null) {
            selectItem = new ArrayList<SelectV12>();
        }
        return this.selectItem;
    }

    /**
     * Gets the value of the inputItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the inputItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getInputItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link InputV12 }
     * 
     * 
     */
    public List<InputV12> getInputItem() {
        if (inputItem == null) {
            inputItem = new ArrayList<InputV12>();
        }
        return this.inputItem;
    }

    /**
     * Gets the value of the blobItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the blobItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getBlobItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link BlobV12 }
     * 
     * 
     */
    public List<BlobV12> getBlobItem() {
        if (blobItem == null) {
            blobItem = new ArrayList<BlobV12>();
        }
        return this.blobItem;
    }

    /**
     * Gets the value of the textItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the textItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getTextItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link TextV12 }
     * 
     * 
     */
    public List<TextV12> getTextItem() {
        if (textItem == null) {
            textItem = new ArrayList<TextV12>();
        }
        return this.textItem;
    }

    /**
     * Gets the value of the linkItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the linkItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getLinkItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link LinkV12 }
     * 
     * 
     */
    public List<LinkV12> getLinkItem() {
        if (linkItem == null) {
            linkItem = new ArrayList<LinkV12>();
        }
        return this.linkItem;
    }

    /**
     * Gets the value of the dateItem property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dateItem property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDateItem().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link DateV12 }
     * 
     * 
     */
    public List<DateV12> getDateItem() {
        if (dateItem == null) {
            dateItem = new ArrayList<DateV12>();
        }
        return this.dateItem;
    }

}
