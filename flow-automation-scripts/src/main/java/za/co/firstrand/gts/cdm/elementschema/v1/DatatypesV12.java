
package za.co.firstrand.gts.cdm.elementschema.v1;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for datatypesV1.2.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="datatypesV1.2"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="string"/&gt;
 *     &lt;enumeration value="password"/&gt;
 *     &lt;enumeration value="decimal"/&gt;
 *     &lt;enumeration value="currency"/&gt;
 *     &lt;enumeration value="number"/&gt;
 *     &lt;enumeration value="date"/&gt;
 *     &lt;enumeration value="email"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "datatypesV1.2")
@XmlEnum
public enum DatatypesV12 {

    @XmlEnumValue("string")
    STRING("string"),
    @XmlEnumValue("password")
    PASSWORD("password"),
    @XmlEnumValue("decimal")
    DECIMAL("decimal"),
    @XmlEnumValue("currency")
    CURRENCY("currency"),
    @XmlEnumValue("number")
    NUMBER("number"),
    @XmlEnumValue("date")
    DATE("date"),
    @XmlEnumValue("email")
    EMAIL("email");
    private final String value;

    DatatypesV12(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DatatypesV12 fromValue(String v) {
        for (DatatypesV12 c: DatatypesV12 .values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
