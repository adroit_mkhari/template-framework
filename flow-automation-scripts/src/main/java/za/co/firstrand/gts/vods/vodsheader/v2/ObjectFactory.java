
package za.co.firstrand.gts.vods.vodsheader.v2;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.vods.vodsheader.v2 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.vods.vodsheader.v2
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link VodsResponseHeader }
     * 
     */
    public VodsResponseHeader createVodsResponseHeader() {
        return new VodsResponseHeader();
    }

    /**
     * Create an instance of {@link BatchId }
     * 
     */
    public BatchId createBatchId() {
        return new BatchId();
    }

    /**
     * Create an instance of {@link VodsRequestHeader }
     * 
     */
    public VodsRequestHeader createVodsRequestHeader() {
        return new VodsRequestHeader();
    }

}
