
package za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for RequestRealTimeBalanceResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RequestRealTimeBalanceResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="ilpBalancesResponse" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1}ILPBalancesResponse" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="idsBalancesResponse" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1}IDSBalancesResponse" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="tcsBalancesResponse" type="{http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1}TCSBalancesResponse" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RequestRealTimeBalanceResponse", propOrder = {
    "ilpBalancesResponse",
    "idsBalancesResponse",
    "tcsBalancesResponse"
})
public class RequestRealTimeBalanceResponse
    extends EnterpriseMessage
{

    protected ILPBalancesResponse ilpBalancesResponse;
    protected IDSBalancesResponse idsBalancesResponse;
    protected TCSBalancesResponse tcsBalancesResponse;

    /**
     * Gets the value of the ilpBalancesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link ILPBalancesResponse }
     *     
     */
    public ILPBalancesResponse getIlpBalancesResponse() {
        return ilpBalancesResponse;
    }

    /**
     * Sets the value of the ilpBalancesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link ILPBalancesResponse }
     *     
     */
    public void setIlpBalancesResponse(ILPBalancesResponse value) {
        this.ilpBalancesResponse = value;
    }

    /**
     * Gets the value of the idsBalancesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link IDSBalancesResponse }
     *     
     */
    public IDSBalancesResponse getIdsBalancesResponse() {
        return idsBalancesResponse;
    }

    /**
     * Sets the value of the idsBalancesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link IDSBalancesResponse }
     *     
     */
    public void setIdsBalancesResponse(IDSBalancesResponse value) {
        this.idsBalancesResponse = value;
    }

    /**
     * Gets the value of the tcsBalancesResponse property.
     * 
     * @return
     *     possible object is
     *     {@link TCSBalancesResponse }
     *     
     */
    public TCSBalancesResponse getTcsBalancesResponse() {
        return tcsBalancesResponse;
    }

    /**
     * Sets the value of the tcsBalancesResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link TCSBalancesResponse }
     *     
     */
    public void setTcsBalancesResponse(TCSBalancesResponse value) {
        this.tcsBalancesResponse = value;
    }

}
