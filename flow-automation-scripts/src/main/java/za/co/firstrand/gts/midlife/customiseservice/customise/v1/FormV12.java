
package za.co.firstrand.gts.midlife.customiseservice.customise.v1;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.elementschema.v1.TextV12;
import za.co.firstrand.gts.cdm.midlife.v1.ParametersV12;


/**
 * <p>Java class for formV1.2 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formV1.2"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="heading" type="{http://www.firstrand.co.za/gts/cdm/elementSchema/v1}textV1.2" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="sectionList" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}listOfSectionsV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *         &lt;element name="attributes" type="{http://www.firstrand.co.za/gts/cdm/midlife/v1}ParametersV1.2" maxOccurs="unbounded" form="qualified"/&gt;
 *         &lt;element name="rule" type="{http://www.firstrand.co.za/gts/midlife/CustomiseService/Customise/v1}rulesV1.2" maxOccurs="unbounded" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *       &lt;attribute name="key" type="{http://www.w3.org/2001/XMLSchema}string" /&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formV1.2", propOrder = {
    "heading",
    "sectionList",
    "attributes",
    "rule"
})
public class FormV12 {

    protected TextV12 heading;
    @XmlElement(required = true)
    protected List<ListOfSectionsV12> sectionList;
    @XmlElement(required = true)
    protected List<ParametersV12> attributes;
    @XmlElement(nillable = true)
    protected List<RulesV12> rule;
    @XmlAttribute(name = "key")
    protected String key;

    /**
     * Gets the value of the heading property.
     * 
     * @return
     *     possible object is
     *     {@link TextV12 }
     *     
     */
    public TextV12 getHeading() {
        return heading;
    }

    /**
     * Sets the value of the heading property.
     * 
     * @param value
     *     allowed object is
     *     {@link TextV12 }
     *     
     */
    public void setHeading(TextV12 value) {
        this.heading = value;
    }

    /**
     * Gets the value of the sectionList property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sectionList property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSectionList().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ListOfSectionsV12 }
     * 
     * 
     */
    public List<ListOfSectionsV12> getSectionList() {
        if (sectionList == null) {
            sectionList = new ArrayList<ListOfSectionsV12>();
        }
        return this.sectionList;
    }

    /**
     * Gets the value of the attributes property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the attributes property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAttributes().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ParametersV12 }
     * 
     * 
     */
    public List<ParametersV12> getAttributes() {
        if (attributes == null) {
            attributes = new ArrayList<ParametersV12>();
        }
        return this.attributes;
    }

    /**
     * Gets the value of the rule property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the rule property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getRule().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RulesV12 }
     * 
     * 
     */
    public List<RulesV12> getRule() {
        if (rule == null) {
            rule = new ArrayList<RulesV12>();
        }
        return this.rule;
    }

    /**
     * Gets the value of the key property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getKey() {
        return key;
    }

    /**
     * Sets the value of the key property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setKey(String value) {
        this.key = value;
    }

}
