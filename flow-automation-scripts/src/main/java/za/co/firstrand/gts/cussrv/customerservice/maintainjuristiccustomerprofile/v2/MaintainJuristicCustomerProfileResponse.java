
package za.co.firstrand.gts.cussrv.customerservice.maintainjuristiccustomerprofile.v2;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import za.co.firstrand.gts.cdm.customer.v10.Customer;
import za.co.firstrand.gts.shared.enterprisemessage.v2.EnterpriseMessage;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/shared/enterpriseMessage/v2}EnterpriseMessage"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customer" type="{http://www.firstrand.co.za/gts/cdm/customer/v10}Customer" minOccurs="0" form="qualified"/&gt;
 *         &lt;element name="failureResponse" type="{http://www.firstrand.co.za/gts/cussrv/CustomerService/MaintainJuristicCustomerProfile/v2}FailureResponse" minOccurs="0" form="qualified"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "customer",
    "failureResponse"
})
@XmlRootElement(name = "MaintainJuristicCustomerProfileResponse")
public class MaintainJuristicCustomerProfileResponse
    extends EnterpriseMessage
{

    protected Customer customer;
    protected FailureResponse failureResponse;

    /**
     * Gets the value of the customer property.
     * 
     * @return
     *     possible object is
     *     {@link Customer }
     *     
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the value of the customer property.
     * 
     * @param value
     *     allowed object is
     *     {@link Customer }
     *     
     */
    public void setCustomer(Customer value) {
        this.customer = value;
    }

    /**
     * Gets the value of the failureResponse property.
     * 
     * @return
     *     possible object is
     *     {@link FailureResponse }
     *     
     */
    public FailureResponse getFailureResponse() {
        return failureResponse;
    }

    /**
     * Sets the value of the failureResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link FailureResponse }
     *     
     */
    public void setFailureResponse(FailureResponse value) {
        this.failureResponse = value;
    }

}
