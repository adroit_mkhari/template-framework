
package za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _RequestRealTimeBalanceRequest_QNAME = new QName("http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1", "RequestRealTimeBalanceRequest");
    private final static QName _RequestRealTimeBalanceResponse_QNAME = new QName("http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1", "RequestRealTimeBalanceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.firstrand.gts.accsrv.accountservice.requestrealtimebalance.v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link RequestRealTimeBalanceRequest }
     * 
     */
    public RequestRealTimeBalanceRequest createRequestRealTimeBalanceRequest() {
        return new RequestRealTimeBalanceRequest();
    }

    /**
     * Create an instance of {@link RequestRealTimeBalanceResponse }
     * 
     */
    public RequestRealTimeBalanceResponse createRequestRealTimeBalanceResponse() {
        return new RequestRealTimeBalanceResponse();
    }

    /**
     * Create an instance of {@link ILPBalancesResponse }
     * 
     */
    public ILPBalancesResponse createILPBalancesResponse() {
        return new ILPBalancesResponse();
    }

    /**
     * Create an instance of {@link IDSBalancesResponse }
     * 
     */
    public IDSBalancesResponse createIDSBalancesResponse() {
        return new IDSBalancesResponse();
    }

    /**
     * Create an instance of {@link TCSBalancesResponse }
     * 
     */
    public TCSBalancesResponse createTCSBalancesResponse() {
        return new TCSBalancesResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRealTimeBalanceRequest }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RequestRealTimeBalanceRequest }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1", name = "RequestRealTimeBalanceRequest")
    public JAXBElement<RequestRealTimeBalanceRequest> createRequestRealTimeBalanceRequest(RequestRealTimeBalanceRequest value) {
        return new JAXBElement<RequestRealTimeBalanceRequest>(_RequestRealTimeBalanceRequest_QNAME, RequestRealTimeBalanceRequest.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RequestRealTimeBalanceResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RequestRealTimeBalanceResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://www.firstrand.co.za/gts/accsrv/AccountService/RequestRealTimeBalance/v1", name = "RequestRealTimeBalanceResponse")
    public JAXBElement<RequestRealTimeBalanceResponse> createRequestRealTimeBalanceResponse(RequestRealTimeBalanceResponse value) {
        return new JAXBElement<RequestRealTimeBalanceResponse>(_RequestRealTimeBalanceResponse_QNAME, RequestRealTimeBalanceResponse.class, null, value);
    }

}
