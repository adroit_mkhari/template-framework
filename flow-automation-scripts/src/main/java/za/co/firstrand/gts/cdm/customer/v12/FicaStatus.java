
package za.co.firstrand.gts.cdm.customer.v12;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for FicaStatus complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="FicaStatus"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="screeningStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="bankDecisionInd" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="crsAutofreeze" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="placeHardHold" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="riskRating" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FicaStatus", propOrder = {
    "screeningStatus",
    "bankDecisionInd",
    "crsAutofreeze",
    "placeHardHold",
    "riskRating"
})
public class FicaStatus {

    protected String screeningStatus;
    protected String bankDecisionInd;
    protected String crsAutofreeze;
    protected String placeHardHold;
    protected String riskRating;

    /**
     * Gets the value of the screeningStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getScreeningStatus() {
        return screeningStatus;
    }

    /**
     * Sets the value of the screeningStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setScreeningStatus(String value) {
        this.screeningStatus = value;
    }

    /**
     * Gets the value of the bankDecisionInd property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBankDecisionInd() {
        return bankDecisionInd;
    }

    /**
     * Sets the value of the bankDecisionInd property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBankDecisionInd(String value) {
        this.bankDecisionInd = value;
    }

    /**
     * Gets the value of the crsAutofreeze property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCrsAutofreeze() {
        return crsAutofreeze;
    }

    /**
     * Sets the value of the crsAutofreeze property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCrsAutofreeze(String value) {
        this.crsAutofreeze = value;
    }

    /**
     * Gets the value of the placeHardHold property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPlaceHardHold() {
        return placeHardHold;
    }

    /**
     * Sets the value of the placeHardHold property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPlaceHardHold(String value) {
        this.placeHardHold = value;
    }

    /**
     * Gets the value of the riskRating property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRiskRating() {
        return riskRating;
    }

    /**
     * Sets the value of the riskRating property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRiskRating(String value) {
        this.riskRating = value;
    }

}
