
package za.co.firstrand.gts.cdm.customer.v9;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for PersonLA complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonLA"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://www.firstrand.co.za/gts/cdm/customer/v9}AbstractPartyLA"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="title" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="name" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="middleName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="surname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="gender" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}GenderLA" minOccurs="0"/&gt;
 *         &lt;element name="race" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}RaceLA" minOccurs="0"/&gt;
 *         &lt;element name="dateOfBirth" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="deceasedDate" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="citizenship" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="nationality" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfPermanentResidence" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="maritalStatus" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}MaritalStatusLA" minOccurs="0"/&gt;
 *         &lt;element name="maritalContract" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}MaritalContractLA" minOccurs="0"/&gt;
 *         &lt;element name="residencyStatus" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}ResidencyStatusLA" minOccurs="0"/&gt;
 *         &lt;element name="occupationCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="employerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="periodEmployed" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="grossIncome" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/&gt;
 *         &lt;element name="debtCounselIndicator" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="higherEducationLevel" type="{http://www.firstrand.co.za/gts/cdm/customer/v9}HigherEducationLevelLA" minOccurs="0"/&gt;
 *         &lt;element name="qualifySpec" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="proofOfDegree" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="dateQualified" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="propertyOwnership" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="propertyBond" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="numberOfDependents" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="groupScheme" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="countryOfBirth" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="firstRandStaffIndicator" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="employeeNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonLA", propOrder = {
    "title",
    "name",
    "middleName",
    "surname",
    "gender",
    "race",
    "dateOfBirth",
    "deceasedDate",
    "citizenship",
    "nationality",
    "countryOfPermanentResidence",
    "maritalStatus",
    "maritalContract",
    "residencyStatus",
    "occupationCode",
    "employerName",
    "periodEmployed",
    "grossIncome",
    "debtCounselIndicator",
    "higherEducationLevel",
    "qualifySpec",
    "proofOfDegree",
    "dateQualified",
    "propertyOwnership",
    "propertyBond",
    "numberOfDependents",
    "groupScheme",
    "countryOfBirth",
    "firstRandStaffIndicator",
    "employeeNumber"
})
public class PersonLA
    extends AbstractPartyLA
{

    protected String title;
    protected String name;
    protected String middleName;
    protected String surname;
    @XmlSchemaType(name = "string")
    protected GenderLA gender;
    @XmlSchemaType(name = "string")
    protected RaceLA race;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateOfBirth;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar deceasedDate;
    protected String citizenship;
    protected String nationality;
    protected String countryOfPermanentResidence;
    @XmlSchemaType(name = "string")
    protected MaritalStatusLA maritalStatus;
    @XmlSchemaType(name = "string")
    protected MaritalContractLA maritalContract;
    @XmlSchemaType(name = "string")
    protected ResidencyStatusLA residencyStatus;
    protected String occupationCode;
    protected String employerName;
    protected String periodEmployed;
    protected Long grossIncome;
    protected String debtCounselIndicator;
    @XmlSchemaType(name = "string")
    protected HigherEducationLevelLA higherEducationLevel;
    protected String qualifySpec;
    protected Boolean proofOfDegree;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dateQualified;
    protected String propertyOwnership;
    protected String propertyBond;
    protected Integer numberOfDependents;
    protected String groupScheme;
    protected String countryOfBirth;
    protected Boolean firstRandStaffIndicator;
    protected String employeeNumber;

    /**
     * Gets the value of the title property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the value of the title property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTitle(String value) {
        this.title = value;
    }

    /**
     * Gets the value of the name property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the value of the name property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setName(String value) {
        this.name = value;
    }

    /**
     * Gets the value of the middleName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMiddleName() {
        return middleName;
    }

    /**
     * Sets the value of the middleName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMiddleName(String value) {
        this.middleName = value;
    }

    /**
     * Gets the value of the surname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSurname() {
        return surname;
    }

    /**
     * Sets the value of the surname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSurname(String value) {
        this.surname = value;
    }

    /**
     * Gets the value of the gender property.
     * 
     * @return
     *     possible object is
     *     {@link GenderLA }
     *     
     */
    public GenderLA getGender() {
        return gender;
    }

    /**
     * Sets the value of the gender property.
     * 
     * @param value
     *     allowed object is
     *     {@link GenderLA }
     *     
     */
    public void setGender(GenderLA value) {
        this.gender = value;
    }

    /**
     * Gets the value of the race property.
     * 
     * @return
     *     possible object is
     *     {@link RaceLA }
     *     
     */
    public RaceLA getRace() {
        return race;
    }

    /**
     * Sets the value of the race property.
     * 
     * @param value
     *     allowed object is
     *     {@link RaceLA }
     *     
     */
    public void setRace(RaceLA value) {
        this.race = value;
    }

    /**
     * Gets the value of the dateOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateOfBirth() {
        return dateOfBirth;
    }

    /**
     * Sets the value of the dateOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateOfBirth(XMLGregorianCalendar value) {
        this.dateOfBirth = value;
    }

    /**
     * Gets the value of the deceasedDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDeceasedDate() {
        return deceasedDate;
    }

    /**
     * Sets the value of the deceasedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDeceasedDate(XMLGregorianCalendar value) {
        this.deceasedDate = value;
    }

    /**
     * Gets the value of the citizenship property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCitizenship() {
        return citizenship;
    }

    /**
     * Sets the value of the citizenship property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCitizenship(String value) {
        this.citizenship = value;
    }

    /**
     * Gets the value of the nationality property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNationality() {
        return nationality;
    }

    /**
     * Sets the value of the nationality property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNationality(String value) {
        this.nationality = value;
    }

    /**
     * Gets the value of the countryOfPermanentResidence property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfPermanentResidence() {
        return countryOfPermanentResidence;
    }

    /**
     * Sets the value of the countryOfPermanentResidence property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfPermanentResidence(String value) {
        this.countryOfPermanentResidence = value;
    }

    /**
     * Gets the value of the maritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link MaritalStatusLA }
     *     
     */
    public MaritalStatusLA getMaritalStatus() {
        return maritalStatus;
    }

    /**
     * Sets the value of the maritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalStatusLA }
     *     
     */
    public void setMaritalStatus(MaritalStatusLA value) {
        this.maritalStatus = value;
    }

    /**
     * Gets the value of the maritalContract property.
     * 
     * @return
     *     possible object is
     *     {@link MaritalContractLA }
     *     
     */
    public MaritalContractLA getMaritalContract() {
        return maritalContract;
    }

    /**
     * Sets the value of the maritalContract property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaritalContractLA }
     *     
     */
    public void setMaritalContract(MaritalContractLA value) {
        this.maritalContract = value;
    }

    /**
     * Gets the value of the residencyStatus property.
     * 
     * @return
     *     possible object is
     *     {@link ResidencyStatusLA }
     *     
     */
    public ResidencyStatusLA getResidencyStatus() {
        return residencyStatus;
    }

    /**
     * Sets the value of the residencyStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link ResidencyStatusLA }
     *     
     */
    public void setResidencyStatus(ResidencyStatusLA value) {
        this.residencyStatus = value;
    }

    /**
     * Gets the value of the occupationCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOccupationCode() {
        return occupationCode;
    }

    /**
     * Sets the value of the occupationCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOccupationCode(String value) {
        this.occupationCode = value;
    }

    /**
     * Gets the value of the employerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployerName() {
        return employerName;
    }

    /**
     * Sets the value of the employerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployerName(String value) {
        this.employerName = value;
    }

    /**
     * Gets the value of the periodEmployed property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPeriodEmployed() {
        return periodEmployed;
    }

    /**
     * Sets the value of the periodEmployed property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPeriodEmployed(String value) {
        this.periodEmployed = value;
    }

    /**
     * Gets the value of the grossIncome property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getGrossIncome() {
        return grossIncome;
    }

    /**
     * Sets the value of the grossIncome property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setGrossIncome(Long value) {
        this.grossIncome = value;
    }

    /**
     * Gets the value of the debtCounselIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDebtCounselIndicator() {
        return debtCounselIndicator;
    }

    /**
     * Sets the value of the debtCounselIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDebtCounselIndicator(String value) {
        this.debtCounselIndicator = value;
    }

    /**
     * Gets the value of the higherEducationLevel property.
     * 
     * @return
     *     possible object is
     *     {@link HigherEducationLevelLA }
     *     
     */
    public HigherEducationLevelLA getHigherEducationLevel() {
        return higherEducationLevel;
    }

    /**
     * Sets the value of the higherEducationLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link HigherEducationLevelLA }
     *     
     */
    public void setHigherEducationLevel(HigherEducationLevelLA value) {
        this.higherEducationLevel = value;
    }

    /**
     * Gets the value of the qualifySpec property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQualifySpec() {
        return qualifySpec;
    }

    /**
     * Sets the value of the qualifySpec property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQualifySpec(String value) {
        this.qualifySpec = value;
    }

    /**
     * Gets the value of the proofOfDegree property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isProofOfDegree() {
        return proofOfDegree;
    }

    /**
     * Sets the value of the proofOfDegree property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setProofOfDegree(Boolean value) {
        this.proofOfDegree = value;
    }

    /**
     * Gets the value of the dateQualified property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDateQualified() {
        return dateQualified;
    }

    /**
     * Sets the value of the dateQualified property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDateQualified(XMLGregorianCalendar value) {
        this.dateQualified = value;
    }

    /**
     * Gets the value of the propertyOwnership property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyOwnership() {
        return propertyOwnership;
    }

    /**
     * Sets the value of the propertyOwnership property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyOwnership(String value) {
        this.propertyOwnership = value;
    }

    /**
     * Gets the value of the propertyBond property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPropertyBond() {
        return propertyBond;
    }

    /**
     * Sets the value of the propertyBond property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPropertyBond(String value) {
        this.propertyBond = value;
    }

    /**
     * Gets the value of the numberOfDependents property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getNumberOfDependents() {
        return numberOfDependents;
    }

    /**
     * Sets the value of the numberOfDependents property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setNumberOfDependents(Integer value) {
        this.numberOfDependents = value;
    }

    /**
     * Gets the value of the groupScheme property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGroupScheme() {
        return groupScheme;
    }

    /**
     * Sets the value of the groupScheme property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGroupScheme(String value) {
        this.groupScheme = value;
    }

    /**
     * Gets the value of the countryOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    /**
     * Sets the value of the countryOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCountryOfBirth(String value) {
        this.countryOfBirth = value;
    }

    /**
     * Gets the value of the firstRandStaffIndicator property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isFirstRandStaffIndicator() {
        return firstRandStaffIndicator;
    }

    /**
     * Sets the value of the firstRandStaffIndicator property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setFirstRandStaffIndicator(Boolean value) {
        this.firstRandStaffIndicator = value;
    }

    /**
     * Gets the value of the employeeNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmployeeNumber() {
        return employeeNumber;
    }

    /**
     * Sets the value of the employeeNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmployeeNumber(String value) {
        this.employeeNumber = value;
    }

}
