
package za.co.fnblife.midlife.smsmessage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnblife.midlife.smsmessage package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _SmsMessage_QNAME = new QName("http://midlife.fnblife.co.za/SMSMessage", "SmsMessage");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnblife.midlife.smsmessage
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link SmsMessageType }
     * 
     */
    public SmsMessageType createSmsMessageType() {
        return new SmsMessageType();
    }

    /**
     * Create an instance of {@link SmsMessageType.Info }
     * 
     */
    public SmsMessageType.Info createSmsMessageTypeInfo() {
        return new SmsMessageType.Info();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SmsMessageType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link SmsMessageType }{@code >}
     */
    @XmlElementDecl(namespace = "http://midlife.fnblife.co.za/SMSMessage", name = "SmsMessage")
    public JAXBElement<SmsMessageType> createSmsMessage(SmsMessageType value) {
        return new JAXBElement<SmsMessageType>(_SmsMessage_QNAME, SmsMessageType.class, null, value);
    }

}
