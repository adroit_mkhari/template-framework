
package za.co.fnblife.data.dataservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="DataServiceRequest" type="{http://data.fnblife.co.za/DataService}DataServiceRequestType" minOccurs="0"/&gt;
 *         &lt;element name="DataServiceResponse" type="{http://data.fnblife.co.za/DataService}DataServiceResponseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dataServiceRequest",
    "dataServiceResponse"
})
@XmlRootElement(name = "DataService")
public class DataService {

    @XmlElement(name = "DataServiceRequest")
    protected DataServiceRequestType dataServiceRequest;
    @XmlElement(name = "DataServiceResponse")
    protected DataServiceResponseType dataServiceResponse;

    /**
     * Gets the value of the dataServiceRequest property.
     * 
     * @return
     *     possible object is
     *     {@link DataServiceRequestType }
     *     
     */
    public DataServiceRequestType getDataServiceRequest() {
        return dataServiceRequest;
    }

    /**
     * Sets the value of the dataServiceRequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataServiceRequestType }
     *     
     */
    public void setDataServiceRequest(DataServiceRequestType value) {
        this.dataServiceRequest = value;
    }

    /**
     * Gets the value of the dataServiceResponse property.
     * 
     * @return
     *     possible object is
     *     {@link DataServiceResponseType }
     *     
     */
    public DataServiceResponseType getDataServiceResponse() {
        return dataServiceResponse;
    }

    /**
     * Sets the value of the dataServiceResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link DataServiceResponseType }
     *     
     */
    public void setDataServiceResponse(DataServiceResponseType value) {
        this.dataServiceResponse = value;
    }

}
