
package za.co.fnblife.data.dataservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the za.co.fnblife.data.dataservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DataServiceRequest_QNAME = new QName("http://data.fnblife.co.za/DataService", "DataServiceRequest");
    private final static QName _DataServiceResponse_QNAME = new QName("http://data.fnblife.co.za/DataService", "DataServiceResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: za.co.fnblife.data.dataservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DataServiceResponseType }
     * 
     */
    public DataServiceResponseType createDataServiceResponseType() {
        return new DataServiceResponseType();
    }

    /**
     * Create an instance of {@link DataServiceResponseType.ResultSet }
     * 
     */
    public DataServiceResponseType.ResultSet createDataServiceResponseTypeResultSet() {
        return new DataServiceResponseType.ResultSet();
    }

    /**
     * Create an instance of {@link DataServiceResponseType.ResultSet.Record }
     * 
     */
    public DataServiceResponseType.ResultSet.Record createDataServiceResponseTypeResultSetRecord() {
        return new DataServiceResponseType.ResultSet.Record();
    }

    /**
     * Create an instance of {@link DataServiceRequestType }
     * 
     */
    public DataServiceRequestType createDataServiceRequestType() {
        return new DataServiceRequestType();
    }

    /**
     * Create an instance of {@link DataServiceRequestType.ParamGroup }
     * 
     */
    public DataServiceRequestType.ParamGroup createDataServiceRequestTypeParamGroup() {
        return new DataServiceRequestType.ParamGroup();
    }

    /**
     * Create an instance of {@link DataService }
     * 
     */
    public DataService createDataService() {
        return new DataService();
    }

    /**
     * Create an instance of {@link DataServiceResponseType.ResultSet.Record.Field }
     * 
     */
    public DataServiceResponseType.ResultSet.Record.Field createDataServiceResponseTypeResultSetRecordField() {
        return new DataServiceResponseType.ResultSet.Record.Field();
    }

    /**
     * Create an instance of {@link DataServiceRequestType.ParamGroup.Param }
     * 
     */
    public DataServiceRequestType.ParamGroup.Param createDataServiceRequestTypeParamGroupParam() {
        return new DataServiceRequestType.ParamGroup.Param();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataServiceRequestType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataServiceRequestType }{@code >}
     */
    @XmlElementDecl(namespace = "http://data.fnblife.co.za/DataService", name = "DataServiceRequest")
    public JAXBElement<DataServiceRequestType> createDataServiceRequest(DataServiceRequestType value) {
        return new JAXBElement<DataServiceRequestType>(_DataServiceRequest_QNAME, DataServiceRequestType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DataServiceResponseType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link DataServiceResponseType }{@code >}
     */
    @XmlElementDecl(namespace = "http://data.fnblife.co.za/DataService", name = "DataServiceResponse")
    public JAXBElement<DataServiceResponseType> createDataServiceResponse(DataServiceResponseType value) {
        return new JAXBElement<DataServiceResponseType>(_DataServiceResponse_QNAME, DataServiceResponseType.class, null, value);
    }

}
