//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, v2.3.1 
// See <a href="https://javaee.github.io/jaxb-v2/">https://javaee.github.io/jaxb-v2/</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2020.03.20 at 04:12:40 PM CAT 
//


package generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PrincipalMemberDetailContactDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PrincipalMemberDetailContactDetail"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="cell" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="home" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="work" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="fax" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prefcomcde" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="prefcomdsc" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="cisemail" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="ciscell" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="incontactemail" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="incontactcell" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PrincipalMemberDetailContactDetail", propOrder = {
    "cell",
    "home",
    "work",
    "fax",
    "email",
    "prefcomcde",
    "prefcomdsc",
    "cisemail",
    "ciscell",
    "incontactemail",
    "incontactcell"
})
public class PrincipalMemberDetailContactDetail {

    @XmlElement(required = true)
    protected String cell;
    @XmlElement(required = true)
    protected String home;
    @XmlElement(required = true)
    protected String work;
    @XmlElement(required = true)
    protected String fax;
    @XmlElement(required = true)
    protected String email;
    @XmlElement(required = true)
    protected String prefcomcde;
    @XmlElement(required = true)
    protected String prefcomdsc;
    @XmlElement(required = true)
    protected String cisemail;
    @XmlElement(required = true)
    protected String ciscell;
    @XmlElement(required = true)
    protected String incontactemail;
    @XmlElement(required = true)
    protected String incontactcell;

    /**
     * Gets the value of the cell property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCell() {
        return cell;
    }

    /**
     * Sets the value of the cell property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCell(String value) {
        this.cell = value;
    }

    /**
     * Gets the value of the home property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHome() {
        return home;
    }

    /**
     * Sets the value of the home property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHome(String value) {
        this.home = value;
    }

    /**
     * Gets the value of the work property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWork() {
        return work;
    }

    /**
     * Sets the value of the work property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWork(String value) {
        this.work = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFax() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFax(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the email property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the value of the email property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmail(String value) {
        this.email = value;
    }

    /**
     * Gets the value of the prefcomcde property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefcomcde() {
        return prefcomcde;
    }

    /**
     * Sets the value of the prefcomcde property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefcomcde(String value) {
        this.prefcomcde = value;
    }

    /**
     * Gets the value of the prefcomdsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPrefcomdsc() {
        return prefcomdsc;
    }

    /**
     * Sets the value of the prefcomdsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPrefcomdsc(String value) {
        this.prefcomdsc = value;
    }

    /**
     * Gets the value of the cisemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCisemail() {
        return cisemail;
    }

    /**
     * Sets the value of the cisemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCisemail(String value) {
        this.cisemail = value;
    }

    /**
     * Gets the value of the ciscell property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCiscell() {
        return ciscell;
    }

    /**
     * Sets the value of the ciscell property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCiscell(String value) {
        this.ciscell = value;
    }

    /**
     * Gets the value of the incontactemail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncontactemail() {
        return incontactemail;
    }

    /**
     * Sets the value of the incontactemail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncontactemail(String value) {
        this.incontactemail = value;
    }

    /**
     * Gets the value of the incontactcell property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIncontactcell() {
        return incontactcell;
    }

    /**
     * Sets the value of the incontactcell property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIncontactcell(String value) {
        this.incontactcell = value;
    }

}
