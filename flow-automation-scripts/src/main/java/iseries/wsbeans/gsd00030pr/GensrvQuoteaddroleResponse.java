
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gensrv_quoteaddroleResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gensrv_quoteaddroleResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00030pr.wsbeans.iseries/}gensrvQUOTEADDROLEResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gensrv_quoteaddroleResponse", propOrder = {
    "_return"
})
public class GensrvQuoteaddroleResponse {

    @XmlElement(name = "return", required = true)
    protected GensrvQUOTEADDROLEResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GensrvQUOTEADDROLEResult }
     *     
     */
    public GensrvQUOTEADDROLEResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GensrvQUOTEADDROLEResult }
     *     
     */
    public void setReturn(GensrvQUOTEADDROLEResult value) {
        this._return = value;
    }

}
