
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gensrv_getrefundreasonsResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gensrv_getrefundreasonsResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00030pr.wsbeans.iseries/}gensrvGETREFUNDREASONSResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gensrv_getrefundreasonsResponse", propOrder = {
    "_return"
})
public class GensrvGetrefundreasonsResponse {

    @XmlElement(name = "return", required = true)
    protected GensrvGETREFUNDREASONSResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GensrvGETREFUNDREASONSResult }
     *     
     */
    public GensrvGETREFUNDREASONSResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GensrvGETREFUNDREASONSResult }
     *     
     */
    public void setReturn(GensrvGETREFUNDREASONSResult value) {
        this._return = value;
    }

}
