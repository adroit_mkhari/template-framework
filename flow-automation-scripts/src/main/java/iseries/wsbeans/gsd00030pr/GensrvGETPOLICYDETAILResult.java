
package iseries.wsbeans.gsd00030pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gensrvGETPOLICYDETAILResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gensrvGETPOLICYDETAILResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN" type="{http://gsd00030pr.wsbeans.iseries/}pXMLIN"/&gt;
 *         &lt;element name="p_XMLOUT" type="{http://gsd00030pr.wsbeans.iseries/}pXMLOUT"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gensrvGETPOLICYDETAILResult", propOrder = {
    "pxmlin",
    "pxmlout",
    "returnValue"
})
public class GensrvGETPOLICYDETAILResult {

    @XmlElement(name = "p_XMLIN", required = true)
    protected PXMLIN pxmlin;
    @XmlElement(name = "p_XMLOUT", required = true)
    protected PXMLOUT pxmlout;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN }
     *     
     */
    public PXMLIN getPXMLIN() {
        return pxmlin;
    }

    /**
     * Sets the value of the pxmlin property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN }
     *     
     */
    public void setPXMLIN(PXMLIN value) {
        this.pxmlin = value;
    }

    /**
     * Gets the value of the pxmlout property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT }
     *     
     */
    public PXMLOUT getPXMLOUT() {
        return pxmlout;
    }

    /**
     * Sets the value of the pxmlout property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT }
     *     
     */
    public void setPXMLOUT(PXMLOUT value) {
        this.pxmlout = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
