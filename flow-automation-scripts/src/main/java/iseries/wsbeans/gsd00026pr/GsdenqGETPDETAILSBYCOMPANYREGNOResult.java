
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETPDETAILSBYCOMPANYREGNOResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETPDETAILSBYCOMPANYREGNOResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_2" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN2"/&gt;
 *         &lt;element name="p_XMLOUT_2" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT2"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETPDETAILSBYCOMPANYREGNOResult", propOrder = {
    "pxmlin2",
    "pxmlout2",
    "returnValue"
})
public class GsdenqGETPDETAILSBYCOMPANYREGNOResult {

    @XmlElement(name = "p_XMLIN_2", required = true)
    protected PXMLIN2 pxmlin2;
    @XmlElement(name = "p_XMLOUT_2", required = true)
    protected PXMLOUT2 pxmlout2;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin2 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN2 }
     *     
     */
    public PXMLIN2 getPXMLIN2() {
        return pxmlin2;
    }

    /**
     * Sets the value of the pxmlin2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN2 }
     *     
     */
    public void setPXMLIN2(PXMLIN2 value) {
        this.pxmlin2 = value;
    }

    /**
     * Gets the value of the pxmlout2 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT2 }
     *     
     */
    public PXMLOUT2 getPXMLOUT2() {
        return pxmlout2;
    }

    /**
     * Sets the value of the pxmlout2 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT2 }
     *     
     */
    public void setPXMLOUT2(PXMLOUT2 value) {
        this.pxmlout2 = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
