
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETPOLICYLISTResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETPOLICYLISTResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_6" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN6"/&gt;
 *         &lt;element name="p_XMLOUT_6" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT6"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETPOLICYLISTResult", propOrder = {
    "pxmlin6",
    "pxmlout6",
    "returnValue"
})
public class GsdenqGETPOLICYLISTResult {

    @XmlElement(name = "p_XMLIN_6", required = true)
    protected PXMLIN6 pxmlin6;
    @XmlElement(name = "p_XMLOUT_6", required = true)
    protected PXMLOUT6 pxmlout6;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin6 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN6 }
     *     
     */
    public PXMLIN6 getPXMLIN6() {
        return pxmlin6;
    }

    /**
     * Sets the value of the pxmlin6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN6 }
     *     
     */
    public void setPXMLIN6(PXMLIN6 value) {
        this.pxmlin6 = value;
    }

    /**
     * Gets the value of the pxmlout6 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT6 }
     *     
     */
    public PXMLOUT6 getPXMLOUT6() {
        return pxmlout6;
    }

    /**
     * Sets the value of the pxmlout6 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT6 }
     *     
     */
    public void setPXMLOUT6(PXMLOUT6 value) {
        this.pxmlout6 = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
