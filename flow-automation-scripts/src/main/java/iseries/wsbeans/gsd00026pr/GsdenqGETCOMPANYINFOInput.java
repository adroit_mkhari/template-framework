
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETCOMPANYINFOInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETCOMPANYINFOInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_3" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN3"/&gt;
 *         &lt;element name="p_XMLOUT_3" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT3"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETCOMPANYINFOInput", propOrder = {
    "pxmlin3",
    "pxmlout3"
})
public class GsdenqGETCOMPANYINFOInput {

    @XmlElement(name = "p_XMLIN_3", required = true)
    protected PXMLIN3 pxmlin3;
    @XmlElement(name = "p_XMLOUT_3", required = true)
    protected PXMLOUT3 pxmlout3;

    /**
     * Gets the value of the pxmlin3 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN3 }
     *     
     */
    public PXMLIN3 getPXMLIN3() {
        return pxmlin3;
    }

    /**
     * Sets the value of the pxmlin3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN3 }
     *     
     */
    public void setPXMLIN3(PXMLIN3 value) {
        this.pxmlin3 = value;
    }

    /**
     * Gets the value of the pxmlout3 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT3 }
     *     
     */
    public PXMLOUT3 getPXMLOUT3() {
        return pxmlout3;
    }

    /**
     * Sets the value of the pxmlout3 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT3 }
     *     
     */
    public void setPXMLOUT3(PXMLOUT3 value) {
        this.pxmlout3 = value;
    }

}
