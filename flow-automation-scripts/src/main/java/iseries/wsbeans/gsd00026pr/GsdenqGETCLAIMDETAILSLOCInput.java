
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETCLAIMDETAILSLOCInput complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETCLAIMDETAILSLOCInput"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_5" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN5"/&gt;
 *         &lt;element name="p_XMLOUT_5" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT5"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETCLAIMDETAILSLOCInput", propOrder = {
    "pxmlin5",
    "pxmlout5"
})
public class GsdenqGETCLAIMDETAILSLOCInput {

    @XmlElement(name = "p_XMLIN_5", required = true)
    protected PXMLIN5 pxmlin5;
    @XmlElement(name = "p_XMLOUT_5", required = true)
    protected PXMLOUT5 pxmlout5;

    /**
     * Gets the value of the pxmlin5 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN5 }
     *     
     */
    public PXMLIN5 getPXMLIN5() {
        return pxmlin5;
    }

    /**
     * Sets the value of the pxmlin5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN5 }
     *     
     */
    public void setPXMLIN5(PXMLIN5 value) {
        this.pxmlin5 = value;
    }

    /**
     * Gets the value of the pxmlout5 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT5 }
     *     
     */
    public PXMLOUT5 getPXMLOUT5() {
        return pxmlout5;
    }

    /**
     * Sets the value of the pxmlout5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT5 }
     *     
     */
    public void setPXMLOUT5(PXMLOUT5 value) {
        this.pxmlout5 = value;
    }

}
