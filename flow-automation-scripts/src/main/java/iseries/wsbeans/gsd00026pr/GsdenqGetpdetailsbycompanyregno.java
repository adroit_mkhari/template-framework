
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenq_getpdetailsbycompanyregno complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenq_getpdetailsbycompanyregno"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://gsd00026pr.wsbeans.iseries/}gsdenqGETPDETAILSBYCOMPANYREGNOInput"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenq_getpdetailsbycompanyregno", propOrder = {
    "arg0"
})
public class GsdenqGetpdetailsbycompanyregno {

    @XmlElement(required = true)
    protected GsdenqGETPDETAILSBYCOMPANYREGNOInput arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link GsdenqGETPDETAILSBYCOMPANYREGNOInput }
     *     
     */
    public GsdenqGETPDETAILSBYCOMPANYREGNOInput getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsdenqGETPDETAILSBYCOMPANYREGNOInput }
     *     
     */
    public void setArg0(GsdenqGETPDETAILSBYCOMPANYREGNOInput value) {
        this.arg0 = value;
    }

}
