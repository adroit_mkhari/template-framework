
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenq_getpolicylistbycompanyregno complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenq_getpolicylistbycompanyregno"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="arg0" type="{http://gsd00026pr.wsbeans.iseries/}gsdenqGETPOLICYLISTBYCOMPANYREGNOInput"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenq_getpolicylistbycompanyregno", propOrder = {
    "arg0"
})
public class GsdenqGetpolicylistbycompanyregno {

    @XmlElement(required = true)
    protected GsdenqGETPOLICYLISTBYCOMPANYREGNOInput arg0;

    /**
     * Gets the value of the arg0 property.
     * 
     * @return
     *     possible object is
     *     {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOInput }
     *     
     */
    public GsdenqGETPOLICYLISTBYCOMPANYREGNOInput getArg0() {
        return arg0;
    }

    /**
     * Sets the value of the arg0 property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOInput }
     *     
     */
    public void setArg0(GsdenqGETPOLICYLISTBYCOMPANYREGNOInput value) {
        this.arg0 = value;
    }

}
