
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETCLAIMDETAILSLOCResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETCLAIMDETAILSLOCResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_5" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN5"/&gt;
 *         &lt;element name="p_XMLOUT_5" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT5"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETCLAIMDETAILSLOCResult", propOrder = {
    "pxmlin5",
    "pxmlout5",
    "returnValue"
})
public class GsdenqGETCLAIMDETAILSLOCResult {

    @XmlElement(name = "p_XMLIN_5", required = true)
    protected PXMLIN5 pxmlin5;
    @XmlElement(name = "p_XMLOUT_5", required = true)
    protected PXMLOUT5 pxmlout5;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin5 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN5 }
     *     
     */
    public PXMLIN5 getPXMLIN5() {
        return pxmlin5;
    }

    /**
     * Sets the value of the pxmlin5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN5 }
     *     
     */
    public void setPXMLIN5(PXMLIN5 value) {
        this.pxmlin5 = value;
    }

    /**
     * Gets the value of the pxmlout5 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT5 }
     *     
     */
    public PXMLOUT5 getPXMLOUT5() {
        return pxmlout5;
    }

    /**
     * Sets the value of the pxmlout5 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT5 }
     *     
     */
    public void setPXMLOUT5(PXMLOUT5 value) {
        this.pxmlout5 = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
