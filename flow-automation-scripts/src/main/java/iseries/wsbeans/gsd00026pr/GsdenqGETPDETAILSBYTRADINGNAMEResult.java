
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenqGETPDETAILSBYTRADINGNAMEResult complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenqGETPDETAILSBYTRADINGNAMEResult"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_XMLIN_1" type="{http://gsd00026pr.wsbeans.iseries/}pXMLIN1"/&gt;
 *         &lt;element name="p_XMLOUT_1" type="{http://gsd00026pr.wsbeans.iseries/}pXMLOUT1"/&gt;
 *         &lt;element name="returnValue" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenqGETPDETAILSBYTRADINGNAMEResult", propOrder = {
    "pxmlin1",
    "pxmlout1",
    "returnValue"
})
public class GsdenqGETPDETAILSBYTRADINGNAMEResult {

    @XmlElement(name = "p_XMLIN_1", required = true)
    protected PXMLIN1 pxmlin1;
    @XmlElement(name = "p_XMLOUT_1", required = true)
    protected PXMLOUT1 pxmlout1;
    protected int returnValue;

    /**
     * Gets the value of the pxmlin1 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLIN1 }
     *     
     */
    public PXMLIN1 getPXMLIN1() {
        return pxmlin1;
    }

    /**
     * Sets the value of the pxmlin1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLIN1 }
     *     
     */
    public void setPXMLIN1(PXMLIN1 value) {
        this.pxmlin1 = value;
    }

    /**
     * Gets the value of the pxmlout1 property.
     * 
     * @return
     *     possible object is
     *     {@link PXMLOUT1 }
     *     
     */
    public PXMLOUT1 getPXMLOUT1() {
        return pxmlout1;
    }

    /**
     * Sets the value of the pxmlout1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link PXMLOUT1 }
     *     
     */
    public void setPXMLOUT1(PXMLOUT1 value) {
        this.pxmlout1 = value;
    }

    /**
     * Gets the value of the returnValue property.
     * 
     */
    public int getReturnValue() {
        return returnValue;
    }

    /**
     * Sets the value of the returnValue property.
     * 
     */
    public void setReturnValue(int value) {
        this.returnValue = value;
    }

}
