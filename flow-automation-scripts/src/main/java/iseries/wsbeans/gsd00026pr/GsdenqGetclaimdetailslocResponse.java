
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenq_getclaimdetailslocResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenq_getclaimdetailslocResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00026pr.wsbeans.iseries/}gsdenqGETCLAIMDETAILSLOCResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenq_getclaimdetailslocResponse", propOrder = {
    "_return"
})
public class GsdenqGetclaimdetailslocResponse {

    @XmlElement(name = "return", required = true)
    protected GsdenqGETCLAIMDETAILSLOCResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GsdenqGETCLAIMDETAILSLOCResult }
     *     
     */
    public GsdenqGETCLAIMDETAILSLOCResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsdenqGETCLAIMDETAILSLOCResult }
     *     
     */
    public void setReturn(GsdenqGETCLAIMDETAILSLOCResult value) {
        this._return = value;
    }

}
