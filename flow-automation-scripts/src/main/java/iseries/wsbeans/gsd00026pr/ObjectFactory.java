
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iseries.wsbeans.gsd00026pr package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _GsdenqGetclaimdetailsloc_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getclaimdetailsloc");
    private final static QName _GsdenqGetclaimdetailslocResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getclaimdetailslocResponse");
    private final static QName _GsdenqGetclientlist_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getclientlist");
    private final static QName _GsdenqGetclientlistResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getclientlistResponse");
    private final static QName _GsdenqGetcompanyinfo_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getcompanyinfo");
    private final static QName _GsdenqGetcompanyinfoResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getcompanyinfoResponse");
    private final static QName _GsdenqGetpdetailsbycompanyregno_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpdetailsbycompanyregno");
    private final static QName _GsdenqGetpdetailsbycompanyregnoResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpdetailsbycompanyregnoResponse");
    private final static QName _GsdenqGetpdetailsbytradingname_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpdetailsbytradingname");
    private final static QName _GsdenqGetpdetailsbytradingnameResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpdetailsbytradingnameResponse");
    private final static QName _GsdenqGetphdetailsbycontactnoloc_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getphdetailsbycontactnoloc");
    private final static QName _GsdenqGetphdetailsbycontactnolocResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getphdetailsbycontactnolocResponse");
    private final static QName _GsdenqGetpolicylist_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpolicylist");
    private final static QName _GsdenqGetpolicylistResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpolicylistResponse");
    private final static QName _GsdenqGetpolicylistbycompanyregno_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpolicylistbycompanyregno");
    private final static QName _GsdenqGetpolicylistbycompanyregnoResponse_QNAME = new QName("http://gsd00026pr.wsbeans.iseries/", "gsdenq_getpolicylistbycompanyregnoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iseries.wsbeans.gsd00026pr
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GsdenqGetclaimdetailsloc }
     * 
     */
    public GsdenqGetclaimdetailsloc createGsdenqGetclaimdetailsloc() {
        return new GsdenqGetclaimdetailsloc();
    }

    /**
     * Create an instance of {@link GsdenqGetclaimdetailslocResponse }
     * 
     */
    public GsdenqGetclaimdetailslocResponse createGsdenqGetclaimdetailslocResponse() {
        return new GsdenqGetclaimdetailslocResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetclientlist }
     * 
     */
    public GsdenqGetclientlist createGsdenqGetclientlist() {
        return new GsdenqGetclientlist();
    }

    /**
     * Create an instance of {@link GsdenqGetclientlistResponse }
     * 
     */
    public GsdenqGetclientlistResponse createGsdenqGetclientlistResponse() {
        return new GsdenqGetclientlistResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetcompanyinfo }
     * 
     */
    public GsdenqGetcompanyinfo createGsdenqGetcompanyinfo() {
        return new GsdenqGetcompanyinfo();
    }

    /**
     * Create an instance of {@link GsdenqGetcompanyinfoResponse }
     * 
     */
    public GsdenqGetcompanyinfoResponse createGsdenqGetcompanyinfoResponse() {
        return new GsdenqGetcompanyinfoResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetpdetailsbycompanyregno }
     * 
     */
    public GsdenqGetpdetailsbycompanyregno createGsdenqGetpdetailsbycompanyregno() {
        return new GsdenqGetpdetailsbycompanyregno();
    }

    /**
     * Create an instance of {@link GsdenqGetpdetailsbycompanyregnoResponse }
     * 
     */
    public GsdenqGetpdetailsbycompanyregnoResponse createGsdenqGetpdetailsbycompanyregnoResponse() {
        return new GsdenqGetpdetailsbycompanyregnoResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetpdetailsbytradingname }
     * 
     */
    public GsdenqGetpdetailsbytradingname createGsdenqGetpdetailsbytradingname() {
        return new GsdenqGetpdetailsbytradingname();
    }

    /**
     * Create an instance of {@link GsdenqGetpdetailsbytradingnameResponse }
     * 
     */
    public GsdenqGetpdetailsbytradingnameResponse createGsdenqGetpdetailsbytradingnameResponse() {
        return new GsdenqGetpdetailsbytradingnameResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetphdetailsbycontactnoloc }
     * 
     */
    public GsdenqGetphdetailsbycontactnoloc createGsdenqGetphdetailsbycontactnoloc() {
        return new GsdenqGetphdetailsbycontactnoloc();
    }

    /**
     * Create an instance of {@link GsdenqGetphdetailsbycontactnolocResponse }
     * 
     */
    public GsdenqGetphdetailsbycontactnolocResponse createGsdenqGetphdetailsbycontactnolocResponse() {
        return new GsdenqGetphdetailsbycontactnolocResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetpolicylist }
     * 
     */
    public GsdenqGetpolicylist createGsdenqGetpolicylist() {
        return new GsdenqGetpolicylist();
    }

    /**
     * Create an instance of {@link GsdenqGetpolicylistResponse }
     * 
     */
    public GsdenqGetpolicylistResponse createGsdenqGetpolicylistResponse() {
        return new GsdenqGetpolicylistResponse();
    }

    /**
     * Create an instance of {@link GsdenqGetpolicylistbycompanyregno }
     * 
     */
    public GsdenqGetpolicylistbycompanyregno createGsdenqGetpolicylistbycompanyregno() {
        return new GsdenqGetpolicylistbycompanyregno();
    }

    /**
     * Create an instance of {@link GsdenqGetpolicylistbycompanyregnoResponse }
     * 
     */
    public GsdenqGetpolicylistbycompanyregnoResponse createGsdenqGetpolicylistbycompanyregnoResponse() {
        return new GsdenqGetpolicylistbycompanyregnoResponse();
    }

    /**
     * Create an instance of {@link GsdenqGETCLIENTLISTInput }
     * 
     */
    public GsdenqGETCLIENTLISTInput createGsdenqGETCLIENTLISTInput() {
        return new GsdenqGETCLIENTLISTInput();
    }

    /**
     * Create an instance of {@link PXMLIN7 }
     * 
     */
    public PXMLIN7 createPXMLIN7() {
        return new PXMLIN7();
    }

    /**
     * Create an instance of {@link PXMLOUT7 }
     * 
     */
    public PXMLOUT7 createPXMLOUT7() {
        return new PXMLOUT7();
    }

    /**
     * Create an instance of {@link GsdenqGETCLIENTLISTResult }
     * 
     */
    public GsdenqGETCLIENTLISTResult createGsdenqGETCLIENTLISTResult() {
        return new GsdenqGETCLIENTLISTResult();
    }

    /**
     * Create an instance of {@link GsdenqGETCLAIMDETAILSLOCInput }
     * 
     */
    public GsdenqGETCLAIMDETAILSLOCInput createGsdenqGETCLAIMDETAILSLOCInput() {
        return new GsdenqGETCLAIMDETAILSLOCInput();
    }

    /**
     * Create an instance of {@link PXMLIN5 }
     * 
     */
    public PXMLIN5 createPXMLIN5() {
        return new PXMLIN5();
    }

    /**
     * Create an instance of {@link PXMLOUT5 }
     * 
     */
    public PXMLOUT5 createPXMLOUT5() {
        return new PXMLOUT5();
    }

    /**
     * Create an instance of {@link GsdenqGETCLAIMDETAILSLOCResult }
     * 
     */
    public GsdenqGETCLAIMDETAILSLOCResult createGsdenqGETCLAIMDETAILSLOCResult() {
        return new GsdenqGETCLAIMDETAILSLOCResult();
    }

    /**
     * Create an instance of {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOInput }
     * 
     */
    public GsdenqGETPOLICYLISTBYCOMPANYREGNOInput createGsdenqGETPOLICYLISTBYCOMPANYREGNOInput() {
        return new GsdenqGETPOLICYLISTBYCOMPANYREGNOInput();
    }

    /**
     * Create an instance of {@link PXMLIN }
     * 
     */
    public PXMLIN createPXMLIN() {
        return new PXMLIN();
    }

    /**
     * Create an instance of {@link PXMLOUT }
     * 
     */
    public PXMLOUT createPXMLOUT() {
        return new PXMLOUT();
    }

    /**
     * Create an instance of {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOResult }
     * 
     */
    public GsdenqGETPOLICYLISTBYCOMPANYREGNOResult createGsdenqGETPOLICYLISTBYCOMPANYREGNOResult() {
        return new GsdenqGETPOLICYLISTBYCOMPANYREGNOResult();
    }

    /**
     * Create an instance of {@link GsdenqGETPDETAILSBYTRADINGNAMEInput }
     * 
     */
    public GsdenqGETPDETAILSBYTRADINGNAMEInput createGsdenqGETPDETAILSBYTRADINGNAMEInput() {
        return new GsdenqGETPDETAILSBYTRADINGNAMEInput();
    }

    /**
     * Create an instance of {@link PXMLIN1 }
     * 
     */
    public PXMLIN1 createPXMLIN1() {
        return new PXMLIN1();
    }

    /**
     * Create an instance of {@link PXMLOUT1 }
     * 
     */
    public PXMLOUT1 createPXMLOUT1() {
        return new PXMLOUT1();
    }

    /**
     * Create an instance of {@link GsdenqGETPDETAILSBYTRADINGNAMEResult }
     * 
     */
    public GsdenqGETPDETAILSBYTRADINGNAMEResult createGsdenqGETPDETAILSBYTRADINGNAMEResult() {
        return new GsdenqGETPDETAILSBYTRADINGNAMEResult();
    }

    /**
     * Create an instance of {@link GsdenqGETCOMPANYINFOInput }
     * 
     */
    public GsdenqGETCOMPANYINFOInput createGsdenqGETCOMPANYINFOInput() {
        return new GsdenqGETCOMPANYINFOInput();
    }

    /**
     * Create an instance of {@link PXMLIN3 }
     * 
     */
    public PXMLIN3 createPXMLIN3() {
        return new PXMLIN3();
    }

    /**
     * Create an instance of {@link PXMLOUT3 }
     * 
     */
    public PXMLOUT3 createPXMLOUT3() {
        return new PXMLOUT3();
    }

    /**
     * Create an instance of {@link GsdenqGETCOMPANYINFOResult }
     * 
     */
    public GsdenqGETCOMPANYINFOResult createGsdenqGETCOMPANYINFOResult() {
        return new GsdenqGETCOMPANYINFOResult();
    }

    /**
     * Create an instance of {@link GsdenqGETPDETAILSBYCOMPANYREGNOInput }
     * 
     */
    public GsdenqGETPDETAILSBYCOMPANYREGNOInput createGsdenqGETPDETAILSBYCOMPANYREGNOInput() {
        return new GsdenqGETPDETAILSBYCOMPANYREGNOInput();
    }

    /**
     * Create an instance of {@link PXMLIN2 }
     * 
     */
    public PXMLIN2 createPXMLIN2() {
        return new PXMLIN2();
    }

    /**
     * Create an instance of {@link PXMLOUT2 }
     * 
     */
    public PXMLOUT2 createPXMLOUT2() {
        return new PXMLOUT2();
    }

    /**
     * Create an instance of {@link GsdenqGETPDETAILSBYCOMPANYREGNOResult }
     * 
     */
    public GsdenqGETPDETAILSBYCOMPANYREGNOResult createGsdenqGETPDETAILSBYCOMPANYREGNOResult() {
        return new GsdenqGETPDETAILSBYCOMPANYREGNOResult();
    }

    /**
     * Create an instance of {@link GsdenqGETPHDETAILSBYCONTACTNOLOCInput }
     * 
     */
    public GsdenqGETPHDETAILSBYCONTACTNOLOCInput createGsdenqGETPHDETAILSBYCONTACTNOLOCInput() {
        return new GsdenqGETPHDETAILSBYCONTACTNOLOCInput();
    }

    /**
     * Create an instance of {@link PXMLIN4 }
     * 
     */
    public PXMLIN4 createPXMLIN4() {
        return new PXMLIN4();
    }

    /**
     * Create an instance of {@link PXMLOUT4 }
     * 
     */
    public PXMLOUT4 createPXMLOUT4() {
        return new PXMLOUT4();
    }

    /**
     * Create an instance of {@link GsdenqGETPHDETAILSBYCONTACTNOLOCResult }
     * 
     */
    public GsdenqGETPHDETAILSBYCONTACTNOLOCResult createGsdenqGETPHDETAILSBYCONTACTNOLOCResult() {
        return new GsdenqGETPHDETAILSBYCONTACTNOLOCResult();
    }

    /**
     * Create an instance of {@link GsdenqGETPOLICYLISTInput }
     * 
     */
    public GsdenqGETPOLICYLISTInput createGsdenqGETPOLICYLISTInput() {
        return new GsdenqGETPOLICYLISTInput();
    }

    /**
     * Create an instance of {@link PXMLIN6 }
     * 
     */
    public PXMLIN6 createPXMLIN6() {
        return new PXMLIN6();
    }

    /**
     * Create an instance of {@link PXMLOUT6 }
     * 
     */
    public PXMLOUT6 createPXMLOUT6() {
        return new PXMLOUT6();
    }

    /**
     * Create an instance of {@link GsdenqGETPOLICYLISTResult }
     * 
     */
    public GsdenqGETPOLICYLISTResult createGsdenqGETPOLICYLISTResult() {
        return new GsdenqGETPOLICYLISTResult();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetclaimdetailsloc }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetclaimdetailsloc }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getclaimdetailsloc")
    public JAXBElement<GsdenqGetclaimdetailsloc> createGsdenqGetclaimdetailsloc(GsdenqGetclaimdetailsloc value) {
        return new JAXBElement<GsdenqGetclaimdetailsloc>(_GsdenqGetclaimdetailsloc_QNAME, GsdenqGetclaimdetailsloc.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetclaimdetailslocResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetclaimdetailslocResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getclaimdetailslocResponse")
    public JAXBElement<GsdenqGetclaimdetailslocResponse> createGsdenqGetclaimdetailslocResponse(GsdenqGetclaimdetailslocResponse value) {
        return new JAXBElement<GsdenqGetclaimdetailslocResponse>(_GsdenqGetclaimdetailslocResponse_QNAME, GsdenqGetclaimdetailslocResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetclientlist }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetclientlist }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getclientlist")
    public JAXBElement<GsdenqGetclientlist> createGsdenqGetclientlist(GsdenqGetclientlist value) {
        return new JAXBElement<GsdenqGetclientlist>(_GsdenqGetclientlist_QNAME, GsdenqGetclientlist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetclientlistResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetclientlistResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getclientlistResponse")
    public JAXBElement<GsdenqGetclientlistResponse> createGsdenqGetclientlistResponse(GsdenqGetclientlistResponse value) {
        return new JAXBElement<GsdenqGetclientlistResponse>(_GsdenqGetclientlistResponse_QNAME, GsdenqGetclientlistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetcompanyinfo }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetcompanyinfo }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getcompanyinfo")
    public JAXBElement<GsdenqGetcompanyinfo> createGsdenqGetcompanyinfo(GsdenqGetcompanyinfo value) {
        return new JAXBElement<GsdenqGetcompanyinfo>(_GsdenqGetcompanyinfo_QNAME, GsdenqGetcompanyinfo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetcompanyinfoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetcompanyinfoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getcompanyinfoResponse")
    public JAXBElement<GsdenqGetcompanyinfoResponse> createGsdenqGetcompanyinfoResponse(GsdenqGetcompanyinfoResponse value) {
        return new JAXBElement<GsdenqGetcompanyinfoResponse>(_GsdenqGetcompanyinfoResponse_QNAME, GsdenqGetcompanyinfoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbycompanyregno }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbycompanyregno }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpdetailsbycompanyregno")
    public JAXBElement<GsdenqGetpdetailsbycompanyregno> createGsdenqGetpdetailsbycompanyregno(GsdenqGetpdetailsbycompanyregno value) {
        return new JAXBElement<GsdenqGetpdetailsbycompanyregno>(_GsdenqGetpdetailsbycompanyregno_QNAME, GsdenqGetpdetailsbycompanyregno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbycompanyregnoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbycompanyregnoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpdetailsbycompanyregnoResponse")
    public JAXBElement<GsdenqGetpdetailsbycompanyregnoResponse> createGsdenqGetpdetailsbycompanyregnoResponse(GsdenqGetpdetailsbycompanyregnoResponse value) {
        return new JAXBElement<GsdenqGetpdetailsbycompanyregnoResponse>(_GsdenqGetpdetailsbycompanyregnoResponse_QNAME, GsdenqGetpdetailsbycompanyregnoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbytradingname }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbytradingname }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpdetailsbytradingname")
    public JAXBElement<GsdenqGetpdetailsbytradingname> createGsdenqGetpdetailsbytradingname(GsdenqGetpdetailsbytradingname value) {
        return new JAXBElement<GsdenqGetpdetailsbytradingname>(_GsdenqGetpdetailsbytradingname_QNAME, GsdenqGetpdetailsbytradingname.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbytradingnameResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpdetailsbytradingnameResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpdetailsbytradingnameResponse")
    public JAXBElement<GsdenqGetpdetailsbytradingnameResponse> createGsdenqGetpdetailsbytradingnameResponse(GsdenqGetpdetailsbytradingnameResponse value) {
        return new JAXBElement<GsdenqGetpdetailsbytradingnameResponse>(_GsdenqGetpdetailsbytradingnameResponse_QNAME, GsdenqGetpdetailsbytradingnameResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetphdetailsbycontactnoloc }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetphdetailsbycontactnoloc }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getphdetailsbycontactnoloc")
    public JAXBElement<GsdenqGetphdetailsbycontactnoloc> createGsdenqGetphdetailsbycontactnoloc(GsdenqGetphdetailsbycontactnoloc value) {
        return new JAXBElement<GsdenqGetphdetailsbycontactnoloc>(_GsdenqGetphdetailsbycontactnoloc_QNAME, GsdenqGetphdetailsbycontactnoloc.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetphdetailsbycontactnolocResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetphdetailsbycontactnolocResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getphdetailsbycontactnolocResponse")
    public JAXBElement<GsdenqGetphdetailsbycontactnolocResponse> createGsdenqGetphdetailsbycontactnolocResponse(GsdenqGetphdetailsbycontactnolocResponse value) {
        return new JAXBElement<GsdenqGetphdetailsbycontactnolocResponse>(_GsdenqGetphdetailsbycontactnolocResponse_QNAME, GsdenqGetphdetailsbycontactnolocResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylist }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylist }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpolicylist")
    public JAXBElement<GsdenqGetpolicylist> createGsdenqGetpolicylist(GsdenqGetpolicylist value) {
        return new JAXBElement<GsdenqGetpolicylist>(_GsdenqGetpolicylist_QNAME, GsdenqGetpolicylist.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpolicylistResponse")
    public JAXBElement<GsdenqGetpolicylistResponse> createGsdenqGetpolicylistResponse(GsdenqGetpolicylistResponse value) {
        return new JAXBElement<GsdenqGetpolicylistResponse>(_GsdenqGetpolicylistResponse_QNAME, GsdenqGetpolicylistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistbycompanyregno }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistbycompanyregno }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpolicylistbycompanyregno")
    public JAXBElement<GsdenqGetpolicylistbycompanyregno> createGsdenqGetpolicylistbycompanyregno(GsdenqGetpolicylistbycompanyregno value) {
        return new JAXBElement<GsdenqGetpolicylistbycompanyregno>(_GsdenqGetpolicylistbycompanyregno_QNAME, GsdenqGetpolicylistbycompanyregno.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistbycompanyregnoResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link GsdenqGetpolicylistbycompanyregnoResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsd00026pr.wsbeans.iseries/", name = "gsdenq_getpolicylistbycompanyregnoResponse")
    public JAXBElement<GsdenqGetpolicylistbycompanyregnoResponse> createGsdenqGetpolicylistbycompanyregnoResponse(GsdenqGetpolicylistbycompanyregnoResponse value) {
        return new JAXBElement<GsdenqGetpolicylistbycompanyregnoResponse>(_GsdenqGetpolicylistbycompanyregnoResponse_QNAME, GsdenqGetpolicylistbycompanyregnoResponse.class, null, value);
    }

}
