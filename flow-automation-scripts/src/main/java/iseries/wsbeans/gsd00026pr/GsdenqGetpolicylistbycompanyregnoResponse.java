
package iseries.wsbeans.gsd00026pr;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsdenq_getpolicylistbycompanyregnoResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsdenq_getpolicylistbycompanyregnoResponse"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="return" type="{http://gsd00026pr.wsbeans.iseries/}gsdenqGETPOLICYLISTBYCOMPANYREGNOResult"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsdenq_getpolicylistbycompanyregnoResponse", propOrder = {
    "_return"
})
public class GsdenqGetpolicylistbycompanyregnoResponse {

    @XmlElement(name = "return", required = true)
    protected GsdenqGETPOLICYLISTBYCOMPANYREGNOResult _return;

    /**
     * Gets the value of the return property.
     * 
     * @return
     *     possible object is
     *     {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOResult }
     *     
     */
    public GsdenqGETPOLICYLISTBYCOMPANYREGNOResult getReturn() {
        return _return;
    }

    /**
     * Sets the value of the return property.
     * 
     * @param value
     *     allowed object is
     *     {@link GsdenqGETPOLICYLISTBYCOMPANYREGNOResult }
     *     
     */
    public void setReturn(GsdenqGETPOLICYLISTBYCOMPANYREGNOResult value) {
        this._return = value;
    }

}
