
package iseries.wsbeans.gsddsync00;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the iseries.wsbeans.gsddsync00 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Gsddsync00_QNAME = new QName("http://gsddsync00.wsbeans.iseries/", "gsddsync00");
    private final static QName _Gsddsync00Response_QNAME = new QName("http://gsddsync00.wsbeans.iseries/", "gsddsync00Response");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: iseries.wsbeans.gsddsync00
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Gsddsync00 }
     * 
     */
    public Gsddsync00 createGsddsync00() {
        return new Gsddsync00();
    }

    /**
     * Create an instance of {@link Gsddsync00Response }
     * 
     */
    public Gsddsync00Response createGsddsync00Response() {
        return new Gsddsync00Response();
    }

    /**
     * Create an instance of {@link Gsddsync00Input }
     * 
     */
    public Gsddsync00Input createGsddsync00Input() {
        return new Gsddsync00Input();
    }

    /**
     * Create an instance of {@link Gsddsync00Result }
     * 
     */
    public Gsddsync00Result createGsddsync00Result() {
        return new Gsddsync00Result();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gsddsync00 }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Gsddsync00 }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsddsync00.wsbeans.iseries/", name = "gsddsync00")
    public JAXBElement<Gsddsync00> createGsddsync00(Gsddsync00 value) {
        return new JAXBElement<Gsddsync00>(_Gsddsync00_QNAME, Gsddsync00 .class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Gsddsync00Response }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Gsddsync00Response }{@code >}
     */
    @XmlElementDecl(namespace = "http://gsddsync00.wsbeans.iseries/", name = "gsddsync00Response")
    public JAXBElement<Gsddsync00Response> createGsddsync00Response(Gsddsync00Response value) {
        return new JAXBElement<Gsddsync00Response>(_Gsddsync00Response_QNAME, Gsddsync00Response.class, null, value);
    }

}
