
package iseries.wsbeans.gsddsync00;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for gsddsync00Result complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="gsddsync00Result"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="p_REQUEST" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="p_RESPONSE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *         &lt;element name="p_TYPE" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "gsddsync00Result", propOrder = {
    "prequest",
    "presponse",
    "ptype"
})
public class Gsddsync00Result {

    @XmlElement(name = "p_REQUEST", required = true)
    protected String prequest;
    @XmlElement(name = "p_RESPONSE", required = true)
    protected String presponse;
    @XmlElement(name = "p_TYPE", required = true)
    protected String ptype;

    /**
     * Gets the value of the prequest property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPREQUEST() {
        return prequest;
    }

    /**
     * Sets the value of the prequest property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPREQUEST(String value) {
        this.prequest = value;
    }

    /**
     * Gets the value of the presponse property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPRESPONSE() {
        return presponse;
    }

    /**
     * Sets the value of the presponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPRESPONSE(String value) {
        this.presponse = value;
    }

    /**
     * Gets the value of the ptype property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPTYPE() {
        return ptype;
    }

    /**
     * Sets the value of the ptype property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPTYPE(String value) {
        this.ptype = value;
    }

}
