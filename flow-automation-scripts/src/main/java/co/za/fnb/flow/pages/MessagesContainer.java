package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.MessagesContainerPageObjects;
import com.thoughtworks.selenium.SeleniumException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;

public class MessagesContainer {
    private Logger log  = LogManager.getLogger(MessagesContainer.class);
    MessagesContainerPageObjects messagesContainerPageObjects;
    WebDriver driver;

    // TODO: Investigate if @CacheLookup doe not course any issues.

    private Actions actions;
    private ErrorHandle errorHandle;
    private int messageContainerList;
    private int numberOfResponseChecks = 0;
    // private String expectedResult;

    // This will be the list of errors, warnings and success messages that we will keep in oder for us to check if it was expected at the end:
    // TODO: Check if making this static will solve our problem of every page instance extending base page having is own.
    // TODO: Or maybe just make the whole class static
    static StringBuilder responseTrace = new StringBuilder();
    // This is a shared Expected Result variable that will be used by all test runners.
    static String expectedResult = "";

    public MessagesContainer(WebDriver driver, ErrorHandle errorHandle) {
        this.driver = driver;
        messagesContainerPageObjects = new MessagesContainerPageObjects(driver);
        actions = new Actions(driver);
        this.errorHandle = errorHandle;
    }

    public void getMessageContainers() {
        try {
            log.info("Trying to look for Normal Messages Container.");
            messageContainerList = driver.findElement(messagesContainerPageObjects.getMessageContainerBy()).findElements(messagesContainerPageObjects.getMessageContainerContentBy()).size();
            log.info("Number of Message Containers: " + messageContainerList);
            if (messageContainerList == 0) {
                throw new Exception("No message containers found, Checking mint message containers.");
            }
        } catch (Exception e) {
            getMintMessageContainers();
        }
    }

    public void getMintMessageContainers() {
        try {
            log.info("Trying to look for Mint Messages Container.");
            messageContainerList = driver.findElement(messagesContainerPageObjects.getMintMessageContainerBy()).findElements(messagesContainerPageObjects.getMessageContainerContentBy()).size();
            log.info("Number of Mint Message Containers: " + messageContainerList);
        } catch (Exception ex) {
            log.info("No message containers found.");
            messageContainerList = 0;
        }
    }

    public int getMessegeContainers() {
        return messageContainerList;
    }

    public String getExpectedResult() {
        return expectedResult;
    }

    public void setExpectedResult(String expectedResult) {
        this.expectedResult = expectedResult;
    }

    public ErrorHandle getErrorHandle() {
        return errorHandle;
    }

    public void getResponseMessages(boolean checkExpectedResponse) throws Exception {
        getMessageContainers();
        WebElement messageContainer;
        WebElement messageContainerTitle;
        WebElement messageContainerContent;
        WebElement messageContainerContentText;
        WebElement messageContainerClose;
        WebElement messageContainerUntitledError;
        boolean isError;
        if (messageContainerList >= 1) {
            numberOfResponseChecks = 0;
            try {
                int closeCount = 0;
                while ((!driver.findElement(messagesContainerPageObjects.getMessageContainerBy()).findElements(messagesContainerPageObjects.getMessageContainerContentBy()).isEmpty() && closeCount < 10) ||
                      (!driver.findElement(messagesContainerPageObjects.getMintMessageContainerBy()).findElements(messagesContainerPageObjects.getMessageContainerContentBy()).isEmpty() && closeCount < 10)) {
                    try {
                        String text = "";
                        for (int i = 0; i < messageContainerList; i++) {
                            log.info("Checking For Message Box");
                            messageContainer = driver.findElement(messagesContainerPageObjects.getMessageContainerBy());
                            messageContainerTitle = messageContainer.findElement(messagesContainerPageObjects.getMessageContainerTitleBy());
                            messageContainerContent = messageContainer.findElement(messagesContainerPageObjects.getMessageContainerContentBy());

                            try {
                                messageContainerUntitledError = messageContainer.findElement(messagesContainerPageObjects.getMessagesContainerUntitledErrorBy());
                                isError = true;
                            } catch (Exception e) {
                                log.debug("Message Container Not an Error.");
                                isError = false;
                            }

                            try {
                                actions.moveToElement(messageContainerContent).click().build().perform();
                            } catch (Exception e) {
                                log.debug("Error while clicking on popup message.");
                            }

                            messageContainerContentText = messageContainerContent.findElement(messagesContainerPageObjects.getTextTagBy());
                            String messageTitle = messageContainerTitle.getText();
                            text = messageContainerContentText.getText();
                            log.info("\n" + messageTitle + ": " + text + "\n");
                            if (text.isEmpty()) {
                                if (!messageTitle.isEmpty()) {
                                    responseTrace.append(messageTitle).append("\n");
                                }
                            } else {
                                responseTrace.append(text).append("\n");
                            }

                            // TODO: Handle The Errors
                            if (checkExpectedResponse) {
                                if (!expectedResult.toUpperCase().contains("SUCCESS")) {
                                    if (messageTitle.toUpperCase().contains("ERROR") || isError || text.contains(expectedResult)) {
                                        // reportHandler.setFailureReason("ERROR: " + text);
                                        errorHandle.setError("ERROR: " + text);
                                        throw new Exception("ERROR" + ": " + text);
                                    }
                                } else if (messageTitle.toUpperCase().contains("ERROR") || isError) {
                                    // reportHandler.setFailureReason( messageTitle + ": " + text);
                                    errorHandle.setError(messageTitle + ": " + text);
                                    throw new Exception("ERROR" + ": " + text);
                                }
                            } else if (messageTitle.toUpperCase().contains("ERROR") || isError) {
                                // reportHandler.setFailureReason( messageTitle + ": " + text);
                                errorHandle.setError(messageTitle + ": " + text);
                                throw new Exception(messageTitle + ": " + text);
                            }

                            log.info("Trying to close message box.");
                            // actions.moveToElement(messagesContainerPageObjects.getMessagesContainerClose()).click().build().perform();
                            try {
                                messageContainerClose = messageContainer.findElement(messagesContainerPageObjects.getMessageContainerCloseBy());
                                actions.moveToElement(messageContainerClose).click().build().perform();
                            } catch (Exception e) {
                                log.debug("Exception while closing response message.");
                            }
                            Thread.sleep(1000);
                        }
                        closeCount++;
                        // if there is a frame open, this may imply a successful case, don't bother with the rest.
                        if (text.toLowerCase().contains("successfully")) {
                            driver.switchTo().defaultContent();
                            // int numberOfFrames = driver.findElements(By.tagName("iframe")).size();
                            int numberOfFrames = driver.findElements(messagesContainerPageObjects.getIframeTagBy()).size();
                            if (numberOfFrames > 0) {
                                break;
                            }
                        }
                    } catch (SeleniumException |
                            NoSuchElementException |
                            ElementNotVisibleException |
                            ElementNotSelectableException |
                            NoSuchWindowException |
                            NoAlertPresentException |
                            NoSuchFrameException |
                            TimeoutException |
                            NoSuchSessionException |
                            StaleElementReferenceException e) {
                        log.error("Selenium Element Error(s)");
                    }
                }
            } catch (SeleniumException |
                    NoSuchElementException |
                    ElementNotVisibleException |
                    ElementNotSelectableException |
                    NoSuchWindowException |
                    NoAlertPresentException |
                    NoSuchFrameException |
                    TimeoutException |
                    NoSuchSessionException |
                    StaleElementReferenceException e) {
                log.error("Selenium Element Error(s)");
            }
        } else if (numberOfResponseChecks < 5) {
            numberOfResponseChecks++;
            Thread.sleep(2000);
            getResponseMessages(checkExpectedResponse);
        } else {
            numberOfResponseChecks = 0;
        }
    }

}
