package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.handlers.identity.Gender;
import co.za.fnb.flow.handlers.identity.IdNumberValidatorUtility;
import co.za.fnb.flow.handlers.identity.SouthAfricanIdNumber;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.services.GetPolicyBasicTester;
import cucumber.api.DataTable;
import generated.Error;
import generated.Errors;
import generated.GetPolicyBasicRequestInput;
import generated.GetPolicyBasicResponseOutput;
import generated.ResponseErrors;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class ScenarioTester {
    private Logger log  = LogManager.getLogger(ScenarioTester.class);
    TestComponent testComponent;
    DataTable dataTable;
    WebDriver driver;

    QueryHandler queryHandler = new QueryHandler();
    private ScenarioOperator scenarioOperator;
    private Object[] headers;
    private List<String> dataEntry;
    private String testCaseNumber;
    private String scenarioDescription;
    private String testFunction;
    private String productName;
    private boolean isSuccess;
    private String failureReason;
    private String generalComment;
    private ResponseErrors responseErrors;

    public ScenarioTester(WebDriver driver) {
        this.driver = driver;
    }

    public ScenarioTester(TestComponent testComponent) {
        this.testComponent = testComponent;
    }

    public ScenarioTester(TestComponent testComponent, DataTable dataTable) {
        this.testComponent = testComponent;
        this.dataTable = dataTable;
    }

    public ScenarioTester(TestComponent testComponent, WebDriver driver) {
        this.testComponent = testComponent;
        this.driver = driver;
    }

    public ScenarioTester(DataTable dataTable, WebDriver driver) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public ScenarioTester(TestComponent testComponent, DataTable dataTable, WebDriver driver) {
        this.testComponent = testComponent;
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public ScenarioTester(List<String> dataEntry) {
        this.dataEntry = dataEntry;
    }

    public ScenarioTester(Object[] headers, List<String> dataEntry, ScenarioOperator scenarioOperator) {
        this.headers = headers;
        this.dataEntry = dataEntry;
        this.scenarioOperator = scenarioOperator;
    }

    public TestComponent getTestComponent() {
        return testComponent;
    }

    public void setTestComponent(TestComponent testComponent) {
        this.testComponent = testComponent;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public List<String> getDataEntry() {
        return dataEntry;
    }

    public void setDataEntry(List<String> dataEntry) {
        this.dataEntry = dataEntry;
    }

    public Object[] getHeaders() {
        return headers;
    }

    public void setHeaders(Object[] headers) {
        this.headers = headers;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    private void setSuccess(boolean success) {
        isSuccess = success;
    }

    public String getFailureReason() {
        return failureReason;
    }

    private void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public String getGeneralComment() {
        return generalComment;
    }

    private void setGeneralComment(String generalComment) {
        this.generalComment = generalComment;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public void test() throws IOException {
        log.info("Running " + testComponent + " ScenarioOperator");

        switch (testComponent) {
            case POLICY_TAKE_UP:
                TakeUpServiceLevelRunner takeUpServiceLevelRunner = new TakeUpServiceLevelRunner(dataTable, this);
                try {
                    takeUpServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case WORK_ITEMS:
                WorkItemsRunner workItemsRunner = new WorkItemsRunner(driver, dataTable);
                try {
                    workItemsRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case POLICY_DETAILS:
                PolicyDetailsRunner policyDetailsRunner = new PolicyDetailsRunner(driver, dataTable);
                try {
                    policyDetailsRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case CLAIMS:
                ClaimsRunner claimsRunner = new ClaimsRunner(driver, dataTable);
                try {
                    claimsRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case INDEXING:
                IndexingRunner indexingRunner = new IndexingRunner(driver, dataTable);
                try {
                    indexingRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case POLICY_DETAILS_SERVICE_LEVEL:
                PolicyDetailsServiceLevelRunner policyDetailsServiceLevelRunner = new PolicyDetailsServiceLevelRunner(dataTable, this);
                try {
                    policyDetailsServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case QUOTE_SERVICE_LEVEL:
                QuoteServiceLevelRunner quoteServiceLevelRunner = new QuoteServiceLevelRunner(dataTable, this);
                try {
                    quoteServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case CANCEL_POLICY_SERVICE_LEVEL:
                CancelPolicyServiceLevelRunner cancelPolicyServiceLevelRunner = new CancelPolicyServiceLevelRunner(dataTable, this);
                try {
                    cancelPolicyServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case REINSTATE_POLICY_SERVICE_LEVEL:
                ReinstatePolicyServiceLevelRunner reinstatePolicyServiceLevelRunner = new ReinstatePolicyServiceLevelRunner(dataTable, this);
                try {
                    reinstatePolicyServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case CONVERT_POLICY_SERVICE_LEVEL:
                ConvertPolicyServiceLevelRunner convertPolicyServiceLevelRunner = new ConvertPolicyServiceLevelRunner(dataTable, this);
                try {
                    convertPolicyServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case GET_PRINCIPAL_MEMBER_DETAILS_SERVICE_LEVEL:
                GetPrincipalMemberDetailsServiceLevelRunner getPrincipalMemberDetailsServiceLevelRunner = new GetPrincipalMemberDetailsServiceLevelRunner(dataTable, this);
                try {
                    getPrincipalMemberDetailsServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case UPDATE_PRINCIPAL_MEMBER_DETAILS_SERVICE_LEVEL:
                UpdatePrincipalMemberDetailsServiceLevelRunner updatePrincipalMemberDetailsServiceLevelRunner = new UpdatePrincipalMemberDetailsServiceLevelRunner(dataTable, this);
                try {
                    updatePrincipalMemberDetailsServiceLevelRunner.run();
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case PRINCIPAL_MEMBER_DETAILS:
                // TODO: Implement Principal Member Details Logic
            case LETTERS:
                LettersRunner lettersRunner = new LettersRunner(driver, dataTable);
                try {
                    lettersRunner.run();
                } catch (Exception e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case RETENTIONS:
                RetentionsRunner retentionsRunner = new RetentionsRunner(driver, dataTable);
                try {
                    retentionsRunner.run();
                } catch (Exception e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
                break;
            case LOSS_OF_INCOME:
                LossOfIncomeRunner lossOfIncome = new LossOfIncomeRunner(driver, dataTable);
                try {
                    lossOfIncome.run();
                } catch (Exception e) {
                    log.error(e.getMessage());
                    e.printStackTrace();
                }
        }
    }

    public void setScenarioPolicyNumber(String policyNumber) {
        scenarioOperator.setPolicyNumber(policyNumber);
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    public String getCellValue(String field) throws Exception {
        try {
            return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
        } catch (Exception e) {
            log.error("Error while getting: " + field);
            throw new Exception("Error while getting: " + field);
        }
    }

    public String getCellValue(Object[] headers, List<String> dataEntry, String field) throws Exception {
        try {
            return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
        } catch (Exception e) {
            log.error("Error while getting: " + field);
            throw new Exception("Error while getting: " + field);
        }
    }

    public String getIntegerStringFromDoubleString(String doubleString) {
        if (!doubleString.isEmpty()) {
            if(doubleString.contains(".")) {
                try {
                    String[] split = doubleString.split("\\.");
                    doubleString = split[0];
                } catch (Exception e) {
                    // TODO: TO Handle Exception
                    log.error("getIntegerStringFromDoubleString: " + e.getMessage());
                }
            }
        }
        return doubleString;
    }

    public String getSimpleDateString(String capturedDate) throws Exception {
        if (!capturedDate.isEmpty()) {
            Date date = formatDate(capturedDate);
            capturedDate = dateToString(date, "yyyyMMdd");
        }
        return capturedDate;
    }

    public String getSimpleDateString(String capturedDate, String dateFormat) throws Exception {
        if (!capturedDate.isEmpty()) {
            Date date = formatDate(capturedDate);
            capturedDate = dateToString(date, dateFormat);
        }
        return capturedDate;
    }

    public Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
                dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    public String dateToString(Date date, String dateFormat) {
        // TODO: Fix Date Formatting.
        return new SimpleDateFormat(dateFormat).format(date);
    }

    public String generateRandomSaId(String policyHolderBirthDate, String policyHolderIdType, String policyHolderIdNumber, String policyHolderGender) throws Exception {
        if (policyHolderIdNumber.isEmpty()) {
            if (!policyHolderBirthDate.isEmpty()) {
                char[] policyHolderBirthDateChars = policyHolderBirthDate.toCharArray();
                String year = getCharStringValue(policyHolderBirthDateChars[2]) + getCharStringValue(policyHolderBirthDateChars[3]);
                String month = getCharStringValue(policyHolderBirthDateChars[4]) + getCharStringValue(policyHolderBirthDateChars[5]);
                String day = getCharStringValue(policyHolderBirthDateChars[6]) + getCharStringValue(policyHolderBirthDateChars[7]);

                Gender gender = policyHolderGender.equalsIgnoreCase("M") ? Gender.MALE : Gender.FEMALE;
                boolean citizenship = policyHolderIdType.equalsIgnoreCase("rsaid");

                SouthAfricanIdNumber southAfricanIdNumber = new SouthAfricanIdNumber(year, month, day, gender, citizenship);
                southAfricanIdNumber.generateIdNumber();
                IdNumberValidatorUtility.IDNumberDetails idNumberDetails = southAfricanIdNumber.getIdNumberDetails();

                if (idNumberDetails.isValid()) {
                    policyHolderIdNumber = idNumberDetails.getIdNumber();
                }
            }
        }
        return policyHolderIdNumber;
    }

    public String getCharStringValue(char policyHolderBirthDateChar) {
        return String.valueOf(policyHolderBirthDateChar);
    }

    public void setupPolicyAfterTakeUp(String[] reportableFields) throws Exception {
        String policyNumber = scenarioOperator.getPolicyNumber();
        String username = getCellValue(headers, dataEntry, "Username");
        String updateFicaStatus = getCellValue(headers, dataEntry, "Update Fica Status");

        String trimToDbPolicyNumber = policyNumber.replace("CP", "").replaceAll(" ", ""); // TODO: Add Other Prefixes to trim off.
        queryHandler.setDbPolicyNumber(trimToDbPolicyNumber);

        String updatedTe = getCellValue(headers, dataEntry,"Updatedte");
        String risk = getCellValue(headers, dataEntry,"Risk");
        String sanction = getCellValue(headers, dataEntry,"Sanction");
        String edd = getCellValue(headers, dataEntry,"Edd");
        String kyc = getCellValue(headers, dataEntry,"Kyc");
        String status = getCellValue(headers, dataEntry,"Status");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        String dbPolicyNumber = queryHandler.getDbPolicyNumber();
        if (!updateFicaStatus.isEmpty()) {
            if (updateFicaStatus.equalsIgnoreCase("Yes")) {
                searchPolicyBasicForFicaRecord(dbPolicyNumber, username);
                Thread.sleep(5000);
                updateFicaStatus(dbPolicyNumber, risk,sanction, edd, kyc, status, updatedTe);
            }
        }
    }

    private void searchPolicyBasicForFicaRecord(String policyNumber, String username) throws ServiceException {
        GetPolicyBasicRequestInput getPolicyBasicRequestInput = new GetPolicyBasicRequestInput();
        getPolicyBasicRequestInput.setPolicy(policyNumber);
        getPolicyBasicRequestInput.setUsername(username);
        GetPolicyBasicTester getPolicyBasicTester = new GetPolicyBasicTester(getPolicyBasicRequestInput);

        GetPolicyBasicResponseOutput getPolicyBasicResponseOutput;

        StringBuilder getPolicyBasicErrorStringBuilder = new StringBuilder();
        try {
            getPolicyBasicResponseOutput = getPolicyBasicTester.sendGetPolicyBasicRequest();
        } catch (Exception e) {
            responseErrors = getPolicyBasicTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<generated.Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPolicyBasicErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPolicyBasicErrorStringBuilder.toString());
        }
    }

    private void updateFicaStatus(String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        if (!scenarioOperator.getProductName().toUpperCase().contains("LOC")) {
            Thread.sleep(5000);
            queryHandler.updateFicaStatus(dbPolicyNumber, risk, sanction, edd, kyc, ficaStatus, updatedTe);
            Thread.sleep(5000);
            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + dbPolicyNumber);
            }
        }
    }

}
