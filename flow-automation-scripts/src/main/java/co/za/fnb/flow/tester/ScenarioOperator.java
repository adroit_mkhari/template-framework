package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.Scenario;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.setup.Report;
import co.za.fnb.flow.setup.TestResultReportFlag;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Properties;

import static co.za.fnb.flow.pages.Config.reportSourcePath;

public class ScenarioOperator {
    private String testCaseNumber;
    private String scenarioDescription;
    private String productName;
    private String function;
    private String policyNumber;
    private String result;
    private String failureReason;
    private HashMap<String, Integer> reportFields;
    private String[] reportableFields;
    private Report report;
    private int reportRowIndex = 0;
    private String reportPath;
    private String screenShotFileName;
    private Scenario scenario;

    public ScenarioOperator() {
    }

    public ScenarioOperator(String[] reportableFields) {
        this.reportableFields = reportableFields;
        setReportFields();
    }

    public ScenarioOperator(String testCaseNumber, String scenarioDescription, String productName, String function, String policyNumber, String result, String failureReason) {
        this.testCaseNumber = testCaseNumber;
        this.scenarioDescription = scenarioDescription;
        this.productName = productName;
        this.function = function;
        this.policyNumber = policyNumber;
        this.result = result;
        this.failureReason = failureReason;
    }

    public ScenarioOperator(String testCaseNumber, String scenarioDescription, String productName, String function, String policyNumber, String result, String failureReason, String[] reportableFields) {
        this.testCaseNumber = testCaseNumber;
        this.scenarioDescription = scenarioDescription;
        this.productName = productName;
        this.function = function;
        this.policyNumber = policyNumber;
        this.result = result;
        this.failureReason = failureReason;
        this.reportableFields = reportableFields;
        setReportFields();
    }

    public ScenarioOperator(String testCaseNumber, String scenarioDescription, String productName, String function, String policyNumber, String result, String failureReason, HashMap<String, Integer> reportFields, Report report) {
        this.testCaseNumber = testCaseNumber;
        this.scenarioDescription = scenarioDescription;
        this.productName = productName;
        this.function = function;
        this.policyNumber = policyNumber;
        this.result = result;
        this.failureReason = failureReason;
        this.reportFields = reportFields;
        this.report = report;
    }

    public String getTestCaseNumber() {
        return testCaseNumber;
    }

    public void setTestCaseNumber(String testCaseNumber) {
        this.testCaseNumber = testCaseNumber;
    }

    public String getScenarioDescription() {
        return scenarioDescription;
    }

    public void setScenarioDescription(String scenarioDescription) {
        this.scenarioDescription = scenarioDescription;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getFunction() {
        return function;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getFailureReason() {
        return failureReason;
    }

    public void setFailureReason(String failureReason) {
        this.failureReason = failureReason;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public HashMap<String, Integer> getReportFields() {
        return reportFields;
    }

    public void setReportFields() {
        Integer index = 0;
        reportFields = new HashMap<>();

        for (String field : reportableFields) {
            reportFields.put(field, index++);
        }
    }

    /**
     * This is meant to create all the required reportable fields
     * @param reportableFields
     */
    public void setReportFields(String[] reportableFields) {
        Integer index = 0;
        reportFields = new HashMap<>();

        for (String field : reportableFields) {
            reportFields.put(field, index++);
        }
    }

    public String[] getReportableFields() {
        return reportableFields;
    }

    public void setReportableFields(String[] reportableFields) {
        this.reportableFields = reportableFields;
    }

    public Report getReport() {
        return report;
    }

    public void setReport(Report report) {
        this.report = report;
    }

    public String getScreenShotFileName() {
        return screenShotFileName;
    }

    public void setScreenShotFileName(String screenShotFileName) {
        this.screenShotFileName = screenShotFileName;
    }

    public Scenario getScenario() {
        return scenario;
    }

    public void setScenario(Scenario scenario) {
        this.scenario = scenario;
    }

    public void createReport() {
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        Properties properties = propertiesSetup.getProperties();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        String systemTime = format.format(now);
        // setReportPath(reportSourcePath + "\\" + systemTime + " " + properties.getProperty("FUNCTION_NAME") + " " + properties.getProperty("PRODUCT_NAME") + ".xlsx");
        setReportPath(reportSourcePath + "/" + systemTime + " " + properties.getProperty("FUNCTION_NAME") + " " + properties.getProperty("PRODUCT_NAME") + ".xlsx");
        report = new Report(reportPath, "Test Results", reportFields);
        report.createReportWorkbook();
    }

    public void createReport(String specificFileName) {
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        Properties properties = propertiesSetup.getProperties();
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        String systemTime = format.format(now);
        // setReportPath(reportSourcePath + "\\" + systemTime + " " + specificFileName + ".xlsx");
        setReportPath(reportSourcePath + "/" + systemTime + " " + specificFileName + ".xlsx");
        report = new Report(reportPath, "Test Results", reportFields);
        report.createReportWorkbook();
    }

    public void writeToReport(int fieldIndex, String value, TestResultReportFlag reportFlag) {
        report.write(fieldIndex, value, reportFlag);
    }

    public void increamentReportRowIndex() {
        reportRowIndex = report.getReportRowIndex() + 1;
        report.setReportRowIndex(reportRowIndex);
    }

    public void saveReport() throws IOException {
        report.writeReportFile();
    }

}
