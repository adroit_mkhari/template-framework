package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.ClaimDetails;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.claims.*;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.pages.policy_details.information.BeneficiaryInformation;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;
import co.za.fnb.flow.pages.policy_details.information.QuoteInformation;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Claim {
    private Logger log  = LogManager.getLogger(Claim.class);
    private WebDriver driver;
    private ClaimDetails claimDetails;
    private PolicyInformation policyInformation;
    private BeneficiaryInformation beneficiaryInformation;
    private QuoteInformation quoteInformation;
    private BankInformation bankInformation;
    private ClaimsInformation claimsInformation;
    private CaptureAttorneyPaymentDetails captureAttorneyPaymentDetails;
    private CapturePolicyHolderPaymentDetails capturePolicyHolderPaymentDetails;
    private PolicyHolderClaimsPayment policyHolderClaimsPayment;
    private AttorneyClaimsPayment attorneyClaimsPayment;
    private AwaitDocOrDiarizeItem awaitDocOrDiarizeItem;

    private Claims claims;
    private CaptureNewClaim captureNewClaim;

    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    private ScenarioOperator scenarioOperator;

    public Claim(WebDriver driver, ClaimDetails claimDetails, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.claimDetails = claimDetails;
        this.scenarioOperator = scenarioOperator;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
        quoteInformation = new QuoteInformation(driver, scenarioOperator);
        claims = new Claims(driver, scenarioOperator);
        claimsInformation = new ClaimsInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(claimDetails.getExpectResult());
    }

    public void createNewClaim() throws Exception {
        log.info("Handling Claims logic");
        captureNewClaim = claims.create();
        Thread.sleep(10000);
        captureNewClaim.switchToDefaultContent();
        Thread.sleep(1000);
        // captureNewClaim.switchToFrame(0);
        int numberOfWindows = driver.getWindowHandles().toArray().length;
        if (numberOfWindows == 3) {
            Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows -1];
            driver.switchTo().window((String) currentWindow);
        }
        Thread.sleep(5000);
        int waitsCount = 0;
        while (!captureNewClaim.headerDisplayed() && waitsCount++ < 5) {
            Thread.sleep(2000);
        }

        if (!claimDetails.getEvent().isEmpty()) {
            Thread.sleep(1000);
            captureNewClaim.selectEvent(claimDetails.getEvent());
            Thread.sleep(1000);
        }

        if (!claimDetails.getEventDescription().isEmpty()) {
            captureNewClaim.updateEventDescription(claimDetails.getEventDescription());
            Thread.sleep(1000);
        }

        if (!claimDetails.getEventDate().isEmpty()) {
            captureNewClaim.updategetEventDate(claimDetails.getEventDate());
            Thread.sleep(1000);
        }

        if (!claimDetails.getClaimant().isEmpty()) {
            captureNewClaim.selectClaimant(claimDetails.getClaimant());
            Thread.sleep(1000);
        }

        if (!claimDetails.getClaimType().isEmpty()) {
            captureNewClaim.selectClaimantType(claimDetails.getClaimType());
            Thread.sleep(1000);
        }

        if (!claimDetails.getProvince().isEmpty()) {
            captureNewClaim.scrollToProvince();
            Thread.sleep(1000);
            captureNewClaim.selectProvince(claimDetails.getProvince());
            Thread.sleep(1000);
        }

        if (!claimDetails.getCity().isEmpty()) {
            captureNewClaim.selectCity(claimDetails.getCity());
            Thread.sleep(1000);
        }

        if (!claimDetails.getCellNumber().isEmpty()) {
            captureNewClaim.updateCellNo(claimDetails.getCellNumber());
            Thread.sleep(1000);
        }

        if (!claimDetails.getAttorneyName().isEmpty()) {
            captureNewClaim.updateAttorneyName(claimDetails.getAttorneyName());
            Thread.sleep(1000);
        }

        captureNewClaim.clickSearch();
        Thread.sleep(10000);
        int attorneyInformationSearchTableSize = captureNewClaim.getClaimsInformationTableSize();

        try {
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[contains(@data-ri, '0')]//td")));
        } catch (Exception e) {
            log.debug("Error while waiting for //tr[contains(@data-ri, '0')]//td");
        }

        if (attorneyInformationSearchTableSize > 0) {
            captureNewClaim.selectClaimsInformationTableDataRow(0);
            Thread.sleep(1000);
        } else {
            log.error("No Attorney Record Were Found.");
            throw new Exception("No Attorney Record Were Found.");
        }

        if (!claimDetails.getWithAttorney().isEmpty()) {
            if (claimDetails.getWithAttorney().equalsIgnoreCase("Yes")) {
                captureNewClaim.moveToAddAttorney();
                captureNewClaim.clickAddAttorney();
                Thread.sleep(1000);
            }
        }

        log.info("Done Handling Claims logic (Still to submit).");
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void submit() throws Exception {
        try {
            log.info("Submitting Claims.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {claimDetails.getPopupWorkTypeZero()};
            String[] popUpStatus = {claimDetails.getPopupStatusZero()};
            String[] popUpQueue = {claimDetails.getPopupQueueZero()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                log.info("Clicking on submit logic.");
                captureNewClaim.clickSubmit();
                captureNewClaim.getResponses(true);
                captureNewClaim.switchToDefaultContent();
                policyInformation.submitClaim(popUpWorkType, popUpStatus, popUpQueue);
                captureNewClaim.switchToDefaultContent();

                try {
                    captureNewClaim.clickUiDialogTitleBarCloseUiCornerAll();
                } catch (Exception e) {
                    e.printStackTrace();
                    log.error("Error while clicking clickUiDialogTitleBarCloseUiCornerAll: " + e.getMessage());
                    int numberOfWindows = driver.getWindowHandles().toArray().length;
                    while (numberOfWindows >= 3) {
                        Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows -1];
                        WebDriver window = driver.switchTo().window((String) currentWindow);
                        window.close();
                        currentWindow = driver.getWindowHandles().toArray()[numberOfWindows - 2];
                        driver.switchTo().window((String) currentWindow);
                        numberOfWindows = driver.getWindowHandles().toArray().length;
                    }
                }
            } else {
                policyInformation.submitClaim(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Submit Claim: " + e.getMessage());
        }
    }

    public void update() throws Exception {
        try {
            log.info("Updating Claim.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {claimDetails.getPopupWorkTypeZero()};
            String[] popUpStatus = {claimDetails.getPopupStatusZero()};
            String[] popUpQueue = {claimDetails.getPopupQueueZero()};

            // TODO: Click Update
            claimsInformation.clickUpdate();
            claimsInformation.getResponses(true);

            CreateMultipleMintItemTable createMultipleMintItemTable = new CreateMultipleMintItemTable(driver, scenarioOperator);
            createMultipleMintItemTable.setPopUpWorkType(popUpWorkType);
            createMultipleMintItemTable.setPopUpStatus(popUpStatus);
            createMultipleMintItemTable.setPopUpQueue(popUpQueue);
            createMultipleMintItemTable.createMintItemForm();

            claimsInformation.switchToAndCloseServiceWindow(2);

            testHandle.setSuccess(true);
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Updating Claim: " + e.getMessage());
        }
    }

    public void proceedAfterSubmit() throws Exception {
        try {
            log.info("Proceeding After Submit.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {claimDetails.getSubmitPopUpWorkTypeZero()};
            String[] popUpStatus = {claimDetails.getSubmitPopUpStatusZero()};
            String[] popUpQueue = {claimDetails.getSubmitPopUpQueueZero()};

            captureNewClaim.getResponses(true);
            // TODO: Switch To Active Content.
            policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue);

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Submit Claim: " + e.getMessage());
        }
    }

    // TODO: Add the actual work items.
    public void proceedAfterApprove() throws Exception {
        try {
            log.info("Proceeding After Approve.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {claimDetails.getApprovePopUpWorkTypeZero()};
            String[] popUpStatus = {claimDetails.getApprovePopUpStatusZero()};
            String[] popUpQueue = {claimDetails.getApprovePopUpQueueZero()};

            captureNewClaim.getResponses(true);
            // TODO: Switch To Active Content.
            policyInformation.proceedAfterClickingSubmit(popUpWorkType, popUpStatus, popUpQueue);

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Submit Claim: " + e.getMessage());
        }
    }

    private boolean getMatchingClaim(String referenceNumber, int claimsInformationTableList, String expectedStatus) throws Exception {
        log.info("Getting Matching Claim From Listed Ones.");
        String claimsReferenceNumber;
        String claimant;
        String status;
        for (int i = 0; i < claimsInformationTableList; i++) {
            claimsReferenceNumber = claimsInformation.getClaimsReferenceNumber(i);
            log.info("Work Item Claim Reference Number: " + referenceNumber + " -> Claims Reference Number: " + claimsReferenceNumber);
            claimant = claimsInformation.getClaimant(i);
            status = claimsInformation.getClaimStatus(i);
            if (claimsReferenceNumber.equalsIgnoreCase(referenceNumber)
                    && claimant.equalsIgnoreCase(claimDetails.getClaimant())
                    && status.equalsIgnoreCase(expectedStatus)) {
                log.info("Got Matching Claim on Index " + i);
                claimsInformation.doubleClickMatchingClaim(i);
                Thread.sleep(10000);
                return true;
            }
        }
        return false;
    }

    private boolean getClaimToMaintain(String referenceNumber, int claimsInformationTableList) throws Exception {
        log.info("Getting Matching Claim To Maintain From Listed Ones.");
        String claimsReferenceNumber;
        claimsInformationTableList = claimsInformationTableList + 1;
        for (int i = 1; i < claimsInformationTableList; i++) {
            claimsReferenceNumber = claimsInformation.getClaimsReferenceNumber(i);
            log.info("(Before) Work Item Claim Reference Number: " + referenceNumber + " -> Claims Reference Number: " + claimsReferenceNumber);
            referenceNumber = referenceNumber.trim();
            claimsReferenceNumber = claimsReferenceNumber.trim();
            log.info("(After) Work Item Claim Reference Number: " + referenceNumber + " -> Claims Reference Number: " + claimsReferenceNumber);
            if (claimsReferenceNumber.equalsIgnoreCase(referenceNumber)) {
                log.info("Got Matching Claim on Index " + i);
                claimsInformation.doubleClickMatchingClaim(i);
                Thread.sleep(10000);
                return true;
            }
        }
        return false;
    }

    public void payClaim(String referenceNumber) throws Exception {
        int claimsInformationTableList = claimsInformation.getClaimsInformationTableList();
        boolean gotMatchingClaim = getMatchingClaim(referenceNumber, claimsInformationTableList, "REGISTERED");

        if (gotMatchingClaim) {
            claimsInformation.switchToAnfMaximizeServiceWindow(2);
            Thread.sleep(1000);
            String payee = claimDetails.getPayee();
            if (payee.equalsIgnoreCase("Policy Holder")) {
                capturePolicyHolderPaymentDetails = claimsInformation.clickPayPolicyHolder();
                Thread.sleep(2000);
                String submitPayment = claimDetails.getSubmitPayment();
                if (submitPayment.equalsIgnoreCase("Yes")) {
                    String dateReceived = claimDetails.getDateReceived();
                    String aolAmount = claimDetails.getAolAmount();
                    String paymentStage = claimDetails.getPaymentStage();
                    if (!dateReceived.isEmpty()) {
                        capturePolicyHolderPaymentDetails.updateReceivedDatePolicyHolderInput(dateReceived);
                        Thread.sleep(1000);
                    }

                    if (!aolAmount.isEmpty()) {
                        capturePolicyHolderPaymentDetails.updateAolAmounnt(aolAmount);
                        Thread.sleep(1000);
                    }

                    if (!paymentStage.isEmpty()) {
                        if (paymentStage.equalsIgnoreCase("Interim")) {
                            capturePolicyHolderPaymentDetails.clickPaymentStageInterimPolicyHolder();
                            Thread.sleep(1000);
                        } else {
                            // Assume that its final.
                            capturePolicyHolderPaymentDetails.clickPaymentStageFinalPolicyHolder();
                            Thread.sleep(1000);
                        }
                    }

                    capturePolicyHolderPaymentDetails.clickBtnMakePolicyHolderPaymentSubmit();
                    Thread.sleep(1000);
                    proceedAfterSubmit();

                    awaitDocOrDiarizeItem = new AwaitDocOrDiarizeItem(driver, scenarioOperator);
                    String awaitDocDurationPerDay = claimDetails.getAwaitDocDurationPerDay();
                    String awaitDocComments = claimDetails.getAwaitDocComments();
                    String detailsAwaitDocReason = claimDetails.getAwaitDocReason();

                    driver.switchTo().defaultContent();
                    driver.switchTo().frame(1);

                    if (!awaitDocDurationPerDay.isEmpty()) {
                        awaitDocOrDiarizeItem.selectTime(awaitDocDurationPerDay);
                        Thread.sleep(1000);
                    }

                    if (!awaitDocComments.isEmpty()) {
                        awaitDocOrDiarizeItem.updateComments(awaitDocComments);
                        Thread.sleep(1000);
                    }

                    if (!detailsAwaitDocReason.isEmpty()) {
                        awaitDocOrDiarizeItem.selectReason(detailsAwaitDocReason);
                    }

                    String awaitDocSubmit = claimDetails.getAwaitDocSubmit();
                    if (awaitDocSubmit.equalsIgnoreCase("Yes")) {
                        awaitDocOrDiarizeItem.clickSubmit();
                        Thread.sleep(1000);
                        claimsInformation.switchToAndCloseServiceWindow(2);
                        claimsInformation.switchToAnfMaximizeServiceWindow(1);
                    } else {
                        awaitDocOrDiarizeItem.clickCancel();
                        Thread.sleep(1000);
                        // TODO: Handle the No Case.
                    }

                } else {
                    capturePolicyHolderPaymentDetails.clickBtnMakePolicyHolderPaymentCancel();
                    Thread.sleep(1000);
                    // TODO: Implement Logic For the No case.
                }
            } else {
                // We assume that its for the Attorney
                // TODO: Implement the logic here.
                captureAttorneyPaymentDetails = claimsInformation.clickPayAttorney();
                Thread.sleep(1000);
                String submitPayment = claimDetails.getSubmitPayment();
                if (submitPayment.equalsIgnoreCase("Yes")) {
                    String invoiceNumber = claimDetails.getInvoiceNumber();
                    String dateReceived = claimDetails.getDateReceived();
                    String invoiceDate = claimDetails.getInvoiceDate();
                    String invoiceDetailsServiceType = claimDetails.getInvoiceDetailsServiceType();
                    String invoiceDetailsDescription = claimDetails.getInvoiceDetailsDescription();
                    String invoiceDetailsAmount = claimDetails.getInvoiceDetailsAmount();
                    String invoiceDetailsVAT = claimDetails.getInvoiceDetailsVAT();
                    String totalAttorneyFee = claimDetails.getTotalAttorneyFee();
                    String paymentStage = claimDetails.getPaymentStage();

                    if (!invoiceNumber.isEmpty()) {
                        captureAttorneyPaymentDetails.updateInvoiceNumber(invoiceNumber);
                        Thread.sleep(1000);
                    }

                    if (!invoiceDate.isEmpty()) {
                        captureAttorneyPaymentDetails.updateInvoiceDateInput(invoiceDate);
                        Thread.sleep(1000);
                    }

                    if (!dateReceived.isEmpty()) {
                        captureAttorneyPaymentDetails.updateDateReceivedInput(dateReceived);
                        Thread.sleep(1000);
                    }

                    if (!invoiceDetailsServiceType.isEmpty()) {
                        captureAttorneyPaymentDetails.selectServiceType(invoiceDetailsServiceType);
                        Thread.sleep(1000);
                    }

                    if (!invoiceDetailsDescription.isEmpty()) {
                        captureAttorneyPaymentDetails.selectDescription(invoiceDetailsDescription);
                        Thread.sleep(1000);
                    }

                    if (!invoiceDetailsAmount.isEmpty()) {
                        captureAttorneyPaymentDetails.updateAmount(invoiceDetailsAmount);
                        Thread.sleep(1000);
                    }

                    if (!invoiceDetailsVAT.isEmpty()) {
                        captureAttorneyPaymentDetails.selectVatFlag(invoiceDetailsVAT);
                        Thread.sleep(1000);
                    }

                    captureAttorneyPaymentDetails.clickBtnMakePaymentAdd();
                    Thread.sleep(2000);

                    if (!paymentStage.isEmpty()) {
                        if (paymentStage.equalsIgnoreCase("Interim")) {
                            captureAttorneyPaymentDetails.clickPaymentStageInterim();
                            Thread.sleep(1000);
                        } else {
                            // Assume that its final.
                            captureAttorneyPaymentDetails.clickPaymentStageFinal();
                            Thread.sleep(1000);
                        }
                    }

                    if (!totalAttorneyFee.isEmpty()) {
                        captureAttorneyPaymentDetails.updateTotalAttorneyFee(totalAttorneyFee);
                        Thread.sleep(1000);
                    }

                    captureAttorneyPaymentDetails.clickBtnMakePaymentSubmit();
                    Thread.sleep(1000);
                    driver.switchTo().defaultContent();
                    PayAttorneyVerified payAttorneyVerified = new PayAttorneyVerified(driver, scenarioOperator);
                    payAttorneyVerified.clickSupplierZero();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSupplierOne();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSupplierTwo();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSupplierThree();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSupplierFour();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSupplierFive();
                    Thread.sleep(500);
                    payAttorneyVerified.clickVatConfirmationOne();
                    Thread.sleep(500);
                    payAttorneyVerified.clickFrsZero();
                    Thread.sleep(500);
                    payAttorneyVerified.clickFrsOne();
                    Thread.sleep(500);
                    payAttorneyVerified.clickFrsTwo();
                    Thread.sleep(500);
                    payAttorneyVerified.clickAmountExclusiveRadioZero();
                    Thread.sleep(500);
                    payAttorneyVerified.clickIdt326();
                    Thread.sleep(500);
                    payAttorneyVerified.clickSubmitConfirmation();
                    Thread.sleep(500);

                    proceedAfterSubmit();

                    awaitDocOrDiarizeItem = new AwaitDocOrDiarizeItem(driver, scenarioOperator);
                    String awaitDocDurationPerDay = claimDetails.getAwaitDocDurationPerDay();
                    String awaitDocComments = claimDetails.getAwaitDocComments();
                    String detailsAwaitDocReason = claimDetails.getAwaitDocReason();

                    driver.switchTo().defaultContent();
                    driver.switchTo().frame(1);

                    if (!awaitDocDurationPerDay.isEmpty()) {
                        awaitDocOrDiarizeItem.selectTime(awaitDocDurationPerDay);
                        Thread.sleep(1000);
                    }

                    if (!awaitDocComments.isEmpty()) {
                        awaitDocOrDiarizeItem.updateComments(awaitDocComments);
                        Thread.sleep(1000);
                    }

                    if (!detailsAwaitDocReason.isEmpty()) {
                        awaitDocOrDiarizeItem.selectReason(detailsAwaitDocReason);
                    }

                    String awaitDocSubmit = claimDetails.getAwaitDocSubmit();
                    if (awaitDocSubmit.equalsIgnoreCase("Yes")) {
                        awaitDocOrDiarizeItem.clickSubmit();
                        Thread.sleep(1000);
                        claimsInformation.switchToAndCloseServiceWindow(2);
                        claimsInformation.switchToAnfMaximizeServiceWindow(1);
                    } else {
                        awaitDocOrDiarizeItem.clickCancel();
                        Thread.sleep(1000);
                        // TODO: Handle the No Case.
                    }

                } else {
                    captureAttorneyPaymentDetails.clickBtnMakePaymentCancel();
                    Thread.sleep(1000);
                    // TODO: Implement Logic For the No case.
                }
            }

        } else {
            log.error("No matching claim was found.");
            throw new Exception("No matching claim was found.");
        }
    }

    public void approveOrFailClaimPayment(String referenceNumber, String sequenceNumber) throws Exception {
        int claimsInformationTableList = claimsInformation.getClaimsInformationTableList();
        boolean gotClaim = getMatchingClaim(referenceNumber, claimsInformationTableList, "REGISTERED");

        if (gotClaim) {
            claimsInformation.switchToAnfMaximizeServiceWindow(2);
            String payee = claimDetails.getPayee();
            log.info("Payee: " + payee);
            if (payee.equalsIgnoreCase("Policy Holder")) {
                policyHolderClaimsPayment = new PolicyHolderClaimsPayment(driver, scenarioOperator);
                int policyHolderClaimsPaymentTableList = policyHolderClaimsPayment.getPolicyHolderClaimsPaymentTableList();
                String claimsReferenceNumber;
                String claimPaymentSequenceNumber;
                String status;
                boolean gotMatchingClaim = false;
                for (int i = 0; i < policyHolderClaimsPaymentTableList; i++) {
                    claimsReferenceNumber = policyHolderClaimsPayment.getClaimsNumber(i);
                    claimPaymentSequenceNumber = policyHolderClaimsPayment.getPaymentSequence(i);
                    status = policyHolderClaimsPayment.getStatus(i);
                    log.info("Claim Reference Number: " + claimsReferenceNumber + " == " + referenceNumber + ", Payment Sequence: " + claimPaymentSequenceNumber + " == " + sequenceNumber +  ", Status: " + status+ " == PENDING");
                    if (claimsReferenceNumber.equalsIgnoreCase(referenceNumber)
                            && claimPaymentSequenceNumber.equalsIgnoreCase(sequenceNumber)
                            && status.equalsIgnoreCase("PENDING")) {
                        log.info("Got Matching Claim on index \"" + i + "\"");
                        policyHolderClaimsPayment.clickApproveFail(i);
                        Thread.sleep(2000);
                        gotMatchingClaim = true;
                        break;
                    }
                }

                if (gotMatchingClaim) {
                    ApproveCapturePolicyHolderPaymentDetails approveCapturePolicyHolderPaymentDetails = new ApproveCapturePolicyHolderPaymentDetails(driver, scenarioOperator);
                    if (claimDetails.getApprovePayment().equalsIgnoreCase("Yes")) {
                        approveCapturePolicyHolderPaymentDetails.clickApprove();
                        Thread.sleep(1000);
                        if (claimDetails.getYesApprovePayment().equalsIgnoreCase("Yes")) {
                            approveCapturePolicyHolderPaymentDetails.clickYesApprove();
                            Thread.sleep(1000);
                            proceedAfterApprove();
                            claimsInformation.switchToAndCloseServiceWindow(2);
                            claimsInformation.switchToAnfMaximizeServiceWindow(1);
                        } else {
                            approveCapturePolicyHolderPaymentDetails.clickNoDoNotApprove();
                            Thread.sleep(1000);
                        }
                    } else {
                        approveCapturePolicyHolderPaymentDetails.clickFail();
                        Thread.sleep(1000);
                        // TODO: Logic For Fail
                        if (claimDetails.getYesApprovePayment().equalsIgnoreCase("Yes")) {
                            // approveCapturePolicyHolderPaymentDetails.clickYesFail();
                            Thread.sleep(1000);
                        } else {
                            // approveCapturePolicyHolderPaymentDetails.clickNoDoNotFail();
                            Thread.sleep(1000);
                        }
                    }
                    // else,
                    // approveCapturePolicyHolderPaymentDetails.clickFail();
                } else {
                    log.error("No matching claim payment was found.");
                    throw new Exception("No matching claim payment was found.");
                }
            } else {
                attorneyClaimsPayment = new AttorneyClaimsPayment(driver, scenarioOperator);
                int policyHolderClaimsPaymentTableList = attorneyClaimsPayment.getAttorneyClaimsPaymentTableList();
                String claimsReferenceNumber;
                String claimPaymentSequenceNumber;
                String status;
                boolean gotMatchingClaim = false;
                for (int i = 0; i < policyHolderClaimsPaymentTableList; i++) {
                    claimsReferenceNumber = attorneyClaimsPayment.getClaimsNumber(i);
                    claimPaymentSequenceNumber = attorneyClaimsPayment.getPaymentSequence(i);
                    status = attorneyClaimsPayment.getStatus(i);
                    log.info("Claim Reference Number: " + claimsReferenceNumber + " == " + referenceNumber + ", Payment Sequence: " + claimPaymentSequenceNumber + " == " + sequenceNumber +  ", Status: " + status+ " == PENDING");
                    if (claimsReferenceNumber.equalsIgnoreCase(referenceNumber)
                            && claimPaymentSequenceNumber.equalsIgnoreCase(sequenceNumber)
                            && status.equalsIgnoreCase("PENDING")) {
                        log.info("Got Matching Claim on index \"" + i + "\"");
                        attorneyClaimsPayment.clickApproveFail(i);
                        Thread.sleep(2000);
                        gotMatchingClaim = true;
                        break;
                    }
                }

                if (gotMatchingClaim) {
                    ApproveCaptureAttorneyPaymentDetails approveCaptureAttorneyPaymentDetails = new ApproveCaptureAttorneyPaymentDetails(driver, scenarioOperator);
                    if (claimDetails.getApprovePayment().equalsIgnoreCase("Yes")) {
                        approveCaptureAttorneyPaymentDetails.clickApprove();
                        Thread.sleep(1000);
                        if (claimDetails.getYesApprovePayment().equalsIgnoreCase("Yes")) {
                            approveCaptureAttorneyPaymentDetails.clickYesApprove();
                            Thread.sleep(1000);
                            proceedAfterApprove();
                            claimsInformation.switchToAndCloseServiceWindow(2);
                            claimsInformation.switchToAnfMaximizeServiceWindow(1);
                        } else {
                            approveCaptureAttorneyPaymentDetails.clickNoDoNotApprove();
                            Thread.sleep(1000);
                        }
                    } else {
                        approveCaptureAttorneyPaymentDetails.clickFail();
                        Thread.sleep(1000);
                        // TODO: Logic For Fail
                        if (claimDetails.getYesApprovePayment().equalsIgnoreCase("Yes")) {
                            // approveCapturePolicyHolderPaymentDetails.clickYesFail();
                            Thread.sleep(1000);
                        } else {
                            // approveCapturePolicyHolderPaymentDetails.clickNoDoNotFail();
                            Thread.sleep(1000);
                        }
                    }
                    // else,
                    // approveCapturePolicyHolderPaymentDetails.clickFail();
                } else {
                    log.error("No matching claim payment was found.");
                    throw new Exception("No matching claim payment was found.");
                }
            }
        } else {
            log.error("No matching claim was found.");
            throw new Exception("No matching claim was found.");
        }

    }

    public void maintainClaim(String referenceNumber) throws Exception {
        try {
            log.info("Maintaining Claim.");
            int claimsInformationTableList = claimsInformation.getClaimsInformationTableList();
            boolean gotMatchingClaim = getClaimToMaintain(referenceNumber, claimsInformationTableList);

            if (gotMatchingClaim) {
                claimsInformation.switchToAnfMaximizeServiceWindow(2);
                Thread.sleep(1000);

                String claimStatus = claimDetails.getClaimStatus();
                if (!claimStatus.isEmpty()) {
                    claimsInformation.selectStatus(claimStatus);
                    Thread.sleep(2000);
                }

                String event = claimDetails.getEvent();
                if (!event.isEmpty()) {
                    claimsInformation.selectEvent(event);
                    Thread.sleep(2000);
                }

                String eventDescription = claimDetails.getEventDescription();
                if (!eventDescription.isEmpty()) {
                    claimsInformation.updateEventDescription(eventDescription);
                    Thread.sleep(2000);
                }

                claimsInformation.scrollToProvince();
                Thread.sleep(2000);

                String province = claimDetails.getProvince();
                if (!province.isEmpty()) {
                    claimsInformation.selectProvince(province);
                    Thread.sleep(2000);
                }

                String city = claimDetails.getCity();
                if (!city.isEmpty()) {
                    claimsInformation.selectCity(city);
                    Thread.sleep(2000);
                }

                String cellNumber = claimDetails.getCellNumber();
                if (!cellNumber.isEmpty()) {
                    claimsInformation.updateCellNo(cellNumber);
                    Thread.sleep(2000);
                }

                String attorneyName = claimDetails.getAttorneyName();
                if (!attorneyName.isEmpty()) {
                    claimsInformation.updateAttorneyName(attorneyName);
                    Thread.sleep(2000);
                }

                claimsInformation.clickSearch();
                Thread.sleep(2000);

                try {
                    WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
                    webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//tr[contains(@data-ri, '0')]//td")));
                } catch (Exception e) {
                    log.debug("Error while waiting for //tr[contains(@data-ri, '0')]//td");
                    String text = driver.findElement(By.xpath("//*[@id=\"claimAttorneySearchForm:claimsInformationTable_data\"]/tr/td")).getText();
                    throw new Exception("Error getting attorney information: " + text);
                }

                int attorneyInformationSearchTableSize = claimsInformation.getAttorneyInformationTableSize();

                if (attorneyInformationSearchTableSize > 0) {
                    claimsInformation.selectAttorneyInformationTableDataRow(0);
                    Thread.sleep(1000);
                    claimsInformation.clickAddAttorney();
                    Thread.sleep(1000);
                } else {
                    log.error("No Attorney Record Were Found.");
                    throw new Exception("No Attorney Record Were Found.");
                }

            } else {
                log.error("No matching claim was found.");
                throw new Exception("No matching claim was found.");
            }
        } catch (Exception e) {
            log.error("Error while maintaining claim. " + e.getMessage());
            throw new Exception("Error while maintaining claim. " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}