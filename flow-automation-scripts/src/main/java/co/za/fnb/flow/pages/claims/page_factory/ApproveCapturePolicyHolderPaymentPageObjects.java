package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ApproveCapturePolicyHolderPaymentPageObjects {

    // region Group Information
    @FindBy(id = "btnMakePolicyHolderPaymentApprove")
    WebElement btnMakePolicyHolderPaymentApprove;

    @FindBy(id = "btnMakePolicyHolderPaymentDecline")
    WebElement btnMakePolicyHolderPaymentDecline;

    @FindBy(id = "policyHolderPaymentconfirmationapprove:btnConfirmPolicyHolderMakePaymentApproveYes")
    WebElement btnConfirmPolicyHolderMakePaymentApproveYes;

    @FindBy(id = "policyHolderPaymentconfirmationapprove:btnConfirmPolicyHolderMakePaymentApproveNo")
    WebElement btnConfirmPolicyHolderMakePaymentApproveNo;

    public ApproveCapturePolicyHolderPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getBtnMakePolicyHolderPaymentApprove() {
        return btnMakePolicyHolderPaymentApprove;
    }

    public WebElement getBtnMakePolicyHolderPaymentDecline() {
        return btnMakePolicyHolderPaymentDecline;
    }

    public WebElement getBtnConfirmPolicyHolderMakePaymentApproveYes() {
        return btnConfirmPolicyHolderMakePaymentApproveYes;
    }

    public WebElement getBtnConfirmPolicyHolderMakePaymentApproveNo() {
        return btnConfirmPolicyHolderMakePaymentApproveNo;
    }
}
