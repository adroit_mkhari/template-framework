package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;

public class PrincipalMemberContactInformationPageObjects extends PrincipalMemberDetails {

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:preferredContactMethod')]")
    private WebElement preferredContactMethod;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:preferredContactMethod')]//div[contains (@class, 'ui-selectonemenu-trigger')]")
    private WebElement preferredContactMethodSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:preferredContactMethod_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:preferredContactMethod_items')]")
    private WebElement preferredContactMethodSelectItems;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:emailAddress')]")
    private WebElement emailAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisEmailAddress')]")
    private WebElement cisEmailAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:inContactEmailAddress')]")
    private WebElement inContactEmailAddress;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cellPhoneNumber')]")
    private WebElement cellPhoneNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisCellPhoneNumber')]")
    private WebElement cisCellPhoneNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:inContactCellPhoneNumber')]")
    private WebElement inContactCellPhoneNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:workPhoneNumber')]")
    private WebElement workPhoneNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:contactInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:homePhoneNumber')]")
    private WebElement homePhoneNumber;

    public PrincipalMemberContactInformationPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getPreferredContactMethod() {
        return preferredContactMethod;
    }

    public WebElement getPreferredContactMethodSelect() {
        return preferredContactMethodSelect;
    }

    public WebElement getPreferredContactMethodSelectItems() {
        return preferredContactMethodSelectItems;
    }

    public WebElement getEmailAddress() {
        return emailAddress;
    }

    public WebElement getCisEmailAddress() {
        return cisEmailAddress;
    }

    public WebElement getInContactEmailAddress() {
        return inContactEmailAddress;
    }

    public WebElement getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public WebElement getCisCellPhoneNumber() {
        return cisCellPhoneNumber;
    }

    public WebElement getInContactCellPhoneNumber() {
        return inContactCellPhoneNumber;
    }

    public WebElement getWorkPhoneNumber() {
        return workPhoneNumber;
    }

    public WebElement getHomePhoneNumber() {
        return homePhoneNumber;
    }
}
