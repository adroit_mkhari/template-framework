package co.za.fnb.flow.handlers;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.CellValue;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.xssf.usermodel.*;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Text File Reader.
 */
public class TextFileReader {
    private String filePath;

    public TextFileReader() {
    }

    public TextFileReader(String filePath) {
        this.filePath = filePath;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public List<List<String>> getDataSet() throws IOException{
        List<List<String>> dataSet = new LinkedList<>();
        try {
            BufferedReader reader = new BufferedReader(new FileReader(filePath));
            String line;
            do {
                line = reader.readLine();
                if (line != null) {
                    dataSet.add(Arrays.asList(line.split(",")));
                }
            } while (line != null);
            reader.close();
        } catch (Exception e) {
            // TODO: Handle Errors
            e.printStackTrace();
        }
        return dataSet;
    }
}
