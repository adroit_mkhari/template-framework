package co.za.fnb.flow.tester.services.models;

public class BankingDetails {
    private String bankName;
    private String bankBranchName;
    private String bankBranchCode;
    private String bankAccountNumber;
    private String paymentDay;
    private String dueDate;
    private String bankAccountCode;
    private String bankAccountDesc;

    public BankingDetails() {
    }

    public BankingDetails(String bankName, String bankBranchName, String bankBranchCode, String bankAccountNumber, String paymentDay, String dueDate, String bankAccountCode, String bankAccountDesc) {
        this.bankName = bankName;
        this.bankBranchName = bankBranchName;
        this.bankBranchCode = bankBranchCode;
        this.bankAccountNumber = bankAccountNumber;
        this.paymentDay = paymentDay;
        this.dueDate = dueDate;
        this.bankAccountCode = bankAccountCode;
        this.bankAccountDesc = bankAccountDesc;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankBranchName() {
        return bankBranchName;
    }

    public void setBankBranchName(String bankBranchName) {
        this.bankBranchName = bankBranchName;
    }

    public String getBankBranchCode() {
        return bankBranchCode;
    }

    public void setBankBranchCode(String bankBranchCode) {
        this.bankBranchCode = bankBranchCode;
    }

    public String getBankAccountNumber() {
        return bankAccountNumber;
    }

    public void setBankAccountNumber(String bankAccountNumber) {
        this.bankAccountNumber = bankAccountNumber;
    }

    public String getPaymentDay() {
        return paymentDay;
    }

    public void setPaymentDay(String paymentDay) {
        this.paymentDay = paymentDay;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getBankAccountCode() {
        return bankAccountCode;
    }

    public void setBankAccountCode(String bankAccountCode) {
        this.bankAccountCode = bankAccountCode;
    }

    public String getBankAccountDesc() {
        return bankAccountDesc;
    }

    public void setBankAccountDesc(String bankAccountDesc) {
        this.bankAccountDesc = bankAccountDesc;
    }
}
