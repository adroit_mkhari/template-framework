package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;

public class PrincipalMemberResidentialInformationPageObjects extends PrincipalMemberDetails {

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:residentialInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:streetLine1')]")
    private WebElement streetLine_1;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:residentialInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:streetLine2')]")
    private WebElement streetLine_2;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:residentialInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:streetSuburb')]")
    private WebElement streetSuburb;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:residentialInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:streetCity')]")
    private WebElement streetCity;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:residentialInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:streetCode')]")
    private WebElement streetCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:postalCode')]")
    private WebElement postalCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisPostalCode')]")
    private WebElement cisPostalCode;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:postalInformationPanel_content')]//table//tbody//tr//td//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:cisZipCode')]")
    private WebElement cisZipCode;

    public PrincipalMemberResidentialInformationPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getStreetLine_1() {
        return streetLine_1;
    }

    public WebElement getStreetLine_2() {
        return streetLine_2;
    }

    public WebElement getStreetSuburb() {
        return streetSuburb;
    }

    public WebElement getStreetCity() {
        return streetCity;
    }

    public WebElement getStreetCode() {
        return streetCode;
    }

    public WebElement getPostalCode() {
        return postalCode;
    }

    public WebElement getCisPostalCode() {
        return cisPostalCode;
    }

    public WebElement getCisZipCode() {
        return cisZipCode;
    }
}
