package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.ErrorHandle;
import com.thoughtworks.selenium.SeleniumException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class MessagesContainerPageObjects {
    // TODO: Investigate if @CacheLookup doe not course any issues.

    @FindBy(id = "messages_container")
    private WebElement messagesContainer;

    @FindBy(id = "mintMessages_container")
    private WebElement minMessagesContainer;

    @FindBy(xpath = "//span[contains(@class, 'ui-growl-title')]")
    private WebElement messagesContainerTitle;

    @FindBy(xpath = "//div[contains(@class, 'ui-growl-message')]")
    private WebElement messagesContainerContent;

    @FindBy(xpath = "//div[contains(@class, 'ui-growl-error')]")
    private WebElement messagesContainerUntitledError;

    @FindBy(xpath = "//div[contains(@class, 'ui-growl-icon-close ui-icon ui-icon-closethick')]")
    private WebElement messagesContainerClose;

    @FindBy(tagName = "p")
    private WebElement textTag;

    @FindBy(tagName = "iframe")
    private WebElement iframeTag;

    By messageContainerBy = By.id("messages_container");
    By mintMessageContainerBy = By.id("mintMessages_container");
    By messageContainerTitleBy = By.xpath("//span[contains(@class, 'ui-growl-title')]");
    By messageContainerContentBy = By.xpath("//div[contains(@class, 'ui-growl-message')]");
    By messagesContainerUntitledErrorBy = By.xpath("//div[contains(@class, 'ui-growl-error')]");
    By messageContainerCloseBy = By.xpath("//div[contains(@class, 'ui-growl-icon-close ui-icon ui-icon-closethick')]");
    By textTagBy = By.tagName("p");
    By iframeTagBy = By.tagName("iframe");

    public MessagesContainerPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getMessagesContainer() {
        return messagesContainer;
    }

    public WebElement getMinMessagesContainer() {
        return minMessagesContainer;
    }

    public WebElement getMessagesContainerTitle() {
        return messagesContainerTitle;
    }

    public WebElement getMessagesContainerContent() {
        return messagesContainerContent;
    }

    public WebElement getMessagesContainerUntitledError() {
        return messagesContainerUntitledError;
    }

    public WebElement getMessagesContainerClose() {
        return messagesContainerClose;
    }

    public WebElement getTextTag() {
        return textTag;
    }

    public WebElement getIframeTag() {
        return iframeTag;
    }

    public By getMessageContainerBy() {
        return messageContainerBy;
    }

    public By getMintMessageContainerBy() {
        return mintMessageContainerBy;
    }

    public By getMessageContainerTitleBy() {
        return messageContainerTitleBy;
    }

    public By getMessageContainerContentBy() {
        return messageContainerContentBy;
    }

    public By getMessagesContainerUntitledErrorBy() {
        return messagesContainerUntitledErrorBy;
    }

    public By getMessageContainerCloseBy() {
        return messageContainerCloseBy;
    }

    public By getTextTagBy() {
        return textTagBy;
    }

    public By getIframeTagBy() {
        return iframeTagBy;
    }
}
