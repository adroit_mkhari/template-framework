package co.za.fnb.flow.models.policy_details;

public class PolicyInformationModel {
    private String escalationDate;
    private String policyNumber;
    private String year;
    private String startPremium;
    private String startCover;
    private String previousPremium;
    private String previousCover;
    private String newPremium;
    private String newCover;
    private String optOut;
    private String roleTypeId;

    public PolicyInformationModel() {
    }

    public PolicyInformationModel(String escalationDate, String policyNumber, String year, String startPremium, String startCover, String previousPremium, String previousCover, String newPremium, String newCover, String optOut, String roleTypeId) {
        this.escalationDate = escalationDate;
        this.policyNumber = policyNumber;
        this.year = year;
        this.startPremium = startPremium;
        this.startCover = startCover;
        this.previousPremium = previousPremium;
        this.previousCover = previousCover;
        this.newPremium = newPremium;
        this.newCover = newCover;
        this.optOut = optOut;
        this.roleTypeId = roleTypeId;
    }

    public String getEscalationDate() {
        return escalationDate;
    }

    public void setEscalationDate(String escalationDate) {
        this.escalationDate = escalationDate;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getStartPremium() {
        return startPremium;
    }

    public void setStartPremium(String startPremium) {
        this.startPremium = startPremium;
    }

    public String getStartCover() {
        return startCover;
    }

    public void setStartCover(String startCover) {
        this.startCover = startCover;
    }

    public String getPreviousPremium() {
        return previousPremium;
    }

    public void setPreviousPremium(String previousPremium) {
        this.previousPremium = previousPremium;
    }

    public String getPreviousCover() {
        return previousCover;
    }

    public void setPreviousCover(String previousCover) {
        this.previousCover = previousCover;
    }

    public String getNewPremium() {
        return newPremium;
    }

    public void setNewPremium(String newPremium) {
        this.newPremium = newPremium;
    }

    public String getNewCover() {
        return newCover;
    }

    public void setNewCover(String newCover) {
        this.newCover = newCover;
    }

    public String getOptOut() {
        return optOut;
    }

    public void setOptOut(String optOut) {
        this.optOut = optOut;
    }

    public String getRoleTypeId() {
        return roleTypeId;
    }

    public void setRoleTypeId(String roleTypeId) {
        this.roleTypeId = roleTypeId;
    }

    @Override
    public String toString() {
        return "PolicyInformationModel{" +
                "escalationDate='" + escalationDate + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", year='" + year + '\'' +
                ", startPremium='" + startPremium + '\'' +
                ", startCover='" + startCover + '\'' +
                ", previousPremium='" + previousPremium + '\'' +
                ", previousCover='" + previousCover + '\'' +
                ", newPremium='" + newPremium + '\'' +
                ", newCover='" + newCover + '\'' +
                ", optOut='" + optOut + '\'' +
                ", roleTypeId='" + roleTypeId + '\'' +
                '}';
    }
}
