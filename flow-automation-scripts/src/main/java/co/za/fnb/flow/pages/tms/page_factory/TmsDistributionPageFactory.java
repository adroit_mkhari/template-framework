package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TmsDistributionPageFactory extends BasePage {
    // *Business Unit Owner
    // @id = selBuOwner
    // @xpath = //*[@id="selBuOwner"]
    @FindBy(id = "selBuOwner")
    private WebElement businessUnitOwner;

    // Option
    // @xpath = //*[@id="selBuOwner"]/option[2]
    @FindBy(xpath = "//*[@id=\"selBuOwner\"]/option[2]")
    private WebElement businessUnitOwnerOption;

    // Company Code
    // @id = selCompanyCode
    // @xpath = //*[@id="selCompanyCode"]
    @FindBy(id = "selCompanyCode")
    private WebElement companyCode;

    // Option
    // @xpath = //*[@id="selCompanyCode"]/option[24]
    @FindBy(xpath = "//*[@id=\"selCompanyCode\"]/option[24]")
    private WebElement companyCodeOption;

    // Search
    // @id = btnSearch
    // @xpath = //*[@id="btnSearch"]
    @FindBy(id = "btnSearch")
    private WebElement search;

    // Send All
    // @id = btnSend
    // @xpath = //*[@id="btnSend"]
    @FindBy(id = "btnSend")
    private WebElement sendAll;

    public TmsDistributionPageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getBusinessUnitOwner() {
        return businessUnitOwner;
    }

    public WebElement getBusinessUnitOwnerOption() {
        return businessUnitOwnerOption;
    }

    public WebElement getCompanyCode() {
        return companyCode;
    }

    public WebElement getCompanyCodeOption() {
        return companyCodeOption;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getSendAll() {
        return sendAll;
    }
}
