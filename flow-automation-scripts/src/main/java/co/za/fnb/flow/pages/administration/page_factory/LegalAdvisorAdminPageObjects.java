package co.za.fnb.flow.pages.administration.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LegalAdvisorAdminPageObjects extends BasePage {
    // region Buttons
    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnAddUser")
    private WebElement addUser;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnClearFilters")
    private WebElement clearFilters;
    // endregion

    // region Attorney Table Data List
    @FindBy(id = "frmUserAdmin:tbvUserAdmin:tblMembers_data")
    private WebElement membersTableData;
    // endregion

    private String membersListTableDataRowXpathLocator = "//*[@id=\"frmUserAdmin:tbvUserAdmin:tblMembers_data\"]/tr[@data-ri=\"0\"]/td[1]";

    // region Right Click Menu
    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuItemEditView")
    private WebElement menuItemEditView;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuItemSuspend")
    private WebElement menuItemSuspend;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:MenuItemApprove")
    private WebElement menuItemApprove;
    // endregion

    public LegalAdvisorAdminPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getAddUser() {
        return addUser;
    }

    public WebElement getClearFilters() {
        return clearFilters;
    }

    public WebElement getMembersTableData() {
        return membersTableData;
    }

    public String getMembersListTableDataRowXpathLocator() {
        return membersListTableDataRowXpathLocator;
    }

    public WebElement getMenuItemEditView() {
        return menuItemEditView;
    }

    public WebElement getMenuItemSuspend() {
        return menuItemSuspend;
    }

    public WebElement getMenuItemApprove() {
        return menuItemApprove;
    }

}
