package co.za.fnb.flow.models.policy_details;

import co.za.fnb.flow.models.ScenarioBase;

public class PolicyDetailsBase extends ScenarioBase {
    private String firstPopup;
    private String secondPopup;
    private String debitOrderDate;
    private String nextDueDate;
    private String bankName;
    private String expectResult;
    private String workTypeZero;
    private String statusZero;
    private String queueZero;
    private String popupWorkTypeZero;
    private String popupStatusZero;
    private String popupQueueZero;
    private String auditTrail;
    private String updatedTe;
    private String risk;
    private String sanction;
    private String edd;
    private String kyc;
    private String status;

    public PolicyDetailsBase(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run);
        this.firstPopup = firstPopup;
        this.secondPopup = secondPopup;
        this.debitOrderDate = debitOrderDate;
        this.nextDueDate = nextDueDate;
        this.bankName = bankName;
        this.expectResult = expectResult;
        this.workTypeZero = workTypeZero;
        this.statusZero = statusZero;
        this.queueZero = queueZero;
        this.popupWorkTypeZero = popupWorkTypeZero;
        this.popupStatusZero = popupStatusZero;
        this.popupQueueZero = popupQueueZero;
        this.auditTrail = auditTrail;
        this.updatedTe = updatedTe;
        this.risk = risk;
        this.sanction = sanction;
        this.edd = edd;
        this.kyc = kyc;
        this.status = status;
    }

    public String getFirstPopup() {
        return firstPopup;
    }

    public String getSecondPopup() {
        return secondPopup;
    }

    public String getDebitOrderDate() {
        return debitOrderDate;
    }

    public String getNextDueDate() {
        return nextDueDate;
    }

    public String getBankName() {
        return bankName;
    }

    public String getExpectResult() {
        return expectResult;
    }

    public String getWorkTypeZero() {
        return workTypeZero;
    }

    public String getStatusZero() {
        return statusZero;
    }

    public String getQueueZero() {
        return queueZero;
    }

    public String getPopupWorkTypeZero() {
        return popupWorkTypeZero;
    }

    public String getPopupStatusZero() {
        return popupStatusZero;
    }

    public String getPopupQueueZero() {
        return popupQueueZero;
    }

    public String getAuditTrail() {
        return auditTrail;
    }

    public String getUpdatedTe() {
        return updatedTe;
    }

    public String getRisk() {
        return risk;
    }

    public String getSanction() {
        return sanction;
    }

    public String getEdd() {
        return edd;
    }

    public String getKyc() {
        return kyc;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "PolicyDetailsBase{" +
                "firstPopup='" + firstPopup + '\'' +
                ", secondPopup='" + secondPopup + '\'' +
                ", debitOrderDate='" + debitOrderDate + '\'' +
                ", nextDueDate='" + nextDueDate + '\'' +
                ", bankName='" + bankName + '\'' +
                ", expectResult='" + expectResult + '\'' +
                ", workTypeZero='" + workTypeZero + '\'' +
                ", statusZero='" + statusZero + '\'' +
                ", queueZero='" + queueZero + '\'' +
                ", popupWorkTypeZero='" + popupWorkTypeZero + '\'' +
                ", popupStatusZero='" + popupStatusZero + '\'' +
                ", popupQueueZero='" + popupQueueZero + '\'' +
                ", auditTrail='" + auditTrail + '\'' +
                ", updatedTe='" + updatedTe + '\'' +
                ", risk='" + risk + '\'' +
                ", sanction='" + sanction + '\'' +
                ", edd='" + edd + '\'' +
                ", kyc='" + kyc + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
