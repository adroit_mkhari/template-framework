package co.za.fnb.flow.pages.covid_screening_details.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CovidScreeningDetailsPageObjects {

    // -----------------------------------------------------------------------------------
    // Retrieve New Claim:
    // -----------------------------------------------------------------------------------

    @FindBy(id = "customerSearchCOVID:searchNewClaim")
    private WebElement searchNewClaim;

    @FindBy(xpath = "//*[@id=\"customerSearchCOVID:messages_container\"]/div/div/div[2]/p")
    private WebElement messageBoxText;

    // -----------------------------------------------------------------------------------
    // Client Details:
    // -----------------------------------------------------------------------------------

    @FindBy(id = "customerSearchCOVID:workItemID")
    private WebElement workItemID;

    @FindBy(id = "customerSearchCOVID:clientName")
    private WebElement clientName;

    @FindBy(id = "customerSearchCOVID:claimEffectiveDate")
    private WebElement claimEffectiveDate;

    @FindBy(id = "customerSearchCOVID:coverStartDate")
    private WebElement coverStartDate;

    @FindBy(id = "customerSearchCOVID:policyNumber")
    private WebElement policyNumber;

    @FindBy(id = "customerSearchCOVID:clientIDno")
    private WebElement clientIDNo;

    // ------------------------------------------------------------------------------------
    // Documents
    // ------------------------------------------------------------------------------------

    // Uploaded Date:

    // Document Name:

    // Document Status:

    // Document Reviewed: @id = customerSearchCOVID:j_idt83 , @xpath = //*[@id="customerSearchCOVID:j_idt83"]

    @FindBy(id = "customerSearchCOVID:j_idt83")
    private WebElement documentReviewed;

    // ------------------------------------------------------------------------------------
    // Employment Status
    // ------------------------------------------------------------------------------------

    @FindBy(id = "customerSearchCOVID:employer")
    private WebElement employerName;

    @FindBy(id = "customerSearchCOVID:editEmployerBtn")
    private WebElement editEmployerButton;

    @FindBy(id = "customerSearchCOVID:category_label")
    private WebElement categorySelectLabel;

    @FindBy(id = "customerSearchCOVID:category_items")
    private WebElement categorySelectItems;

    @FindBy(id = "customerSearchCOVID:category_filter")
    private WebElement categoryFilter;

    @FindBy(id = "customerSearchCOVID:subCategory_label")
    private WebElement subCategorySelectLabel;

    @FindBy(id = "customerSearchCOVID:subCategory_items")
    private WebElement subCategorySelectItems;

    @FindBy(id = "customerSearchCOVID:subCategory_filter")
    private WebElement subCategoryFilter;

    @FindBy(id = "customerSearchCOVID:comment")
    private WebElement comment;

    @FindBy(id = "customerSearchCOVID:policySubmitBtn")
    private WebElement policySubmitButton;

    public CovidScreeningDetailsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchNewClaim() {
        return searchNewClaim;
    }

    public WebElement getMessageBoxText() {
        return messageBoxText;
    }

    public WebElement getWorkItemID() {
        return workItemID;
    }

    public WebElement getClientName() {
        return clientName;
    }

    public WebElement getClaimEffectiveDate() {
        return claimEffectiveDate;
    }

    public WebElement getCoverStartDate() {
        return coverStartDate;
    }

    public WebElement getPolicyNumber() {
        return policyNumber;
    }

    public WebElement getClientIDNo() {
        return clientIDNo;
    }

    public WebElement getDocumentReviewed() {
        return documentReviewed;
    }

    public WebElement getEmployerName() {
        return employerName;
    }

    public WebElement getEditEmployerButton() {
        return editEmployerButton;
    }

    public WebElement getCategorySelectLabel() {
        return categorySelectLabel;
    }

    public WebElement getCategorySelectItems() {
        return categorySelectItems;
    }

    public WebElement getCategoryFilter() {
        return categoryFilter;
    }

    public WebElement getSubCategorySelectLabel() {
        return subCategorySelectLabel;
    }

    public WebElement getSubCategorySelectItems() {
        return subCategorySelectItems;
    }

    public WebElement getSubCategoryFilter() {
        return subCategoryFilter;
    }

    public WebElement getComment() {
        return comment;
    }

    public WebElement getPolicySubmitButton() {
        return policySubmitButton;
    }
}
