package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.CaptureNewClaimPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CaptureNewClaim extends BasePage {
    private Logger log  = LogManager.getLogger(CaptureNewClaim.class);
    CaptureNewClaimPageObjects captureNewClaimPageObjects = new CaptureNewClaimPageObjects(driver);

    public CaptureNewClaim(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public boolean headerDisplayed() throws Exception {
        try {
            return captureNewClaimPageObjects.getHeader().isDisplayed();
        } catch (Exception e) {
            log.error("Error while checking capture new claim dialog header.");
           throw new Exception("Error while checking capture new claim dialog header.");
        }
    }

    public void clearPolicyHolderId() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getPolicyHolderId());
        } catch (Exception e) {
            log.error("Error while clearing policy holder id.");
            throw new Exception("Error while clearing policy holder id.");
        }
    }

    public void updatePolicyHolderId(String policyHolderId) throws Exception {
        try {
            clearPolicyHolderId();
            type(captureNewClaimPageObjects.getPolicyHolderId(), policyHolderId);
        } catch (Exception e) {
            log.error("Error while updating policy holder id.");
            throw new Exception("Error while updating policy holder id.");
        }
    }

    public String getPolicyHolderId() throws Exception {
        try {
            return getAttribute(captureNewClaimPageObjects.getPolicyHolderId(), "value");
        } catch (Exception e) {
            log.error("Error while getting policy holder id.");
            throw new Exception("Error while getting policy holder id.");
        }
    }

    public String getPolicyHolderName() throws Exception {
        try {
            return getAttribute(captureNewClaimPageObjects.getPolicyHolderName(), "value");
        } catch (Exception e) {
            log.error("Error while getting policy holder name.");
            throw new Exception("Error while getting policy holder name.");
        }
    }

    public void clearClaimsHandler() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getClaimsHandler());
        } catch (Exception e) {
            log.error("Error while clearing claims handler.");
            throw new Exception("Error while clearing claims handler.");
        }
    }

    public void updateClaimsHandler(String policyHolderId) throws Exception {
        try {
            clearClaimsHandler();
            type(captureNewClaimPageObjects.getClaimsHandler(), policyHolderId);
        } catch (Exception e) {
            log.error("Error while updating claims handler.");
            throw new Exception("Error while updating claims handler.");
        }
    }

    public String getClaimsHandler() throws Exception {
        try {
            return getAttribute(captureNewClaimPageObjects.getClaimsHandler(), "value");
        } catch (Exception e) {
            log.error("Error while getting claims handler.");
            throw new Exception("Error while getting claims handler.");
        }
    }

    public void clickSelectEvent() throws Exception {
        try {
            click(captureNewClaimPageObjects.getSelectEvent());
        } catch (Exception e) {
            log.error("Error while clicking on Select Event.");
            throw new Exception("Error while clicking on Select Event.");
        }
    }

    public void selectEvent(String event) throws Exception {
        try {
            selectMatchingDataLabel(captureNewClaimPageObjects.getSelectEventLabel(), captureNewClaimPageObjects.getSelectEventItems(), event);
        } catch (Exception e) {
            log.error("Error while selecting event.");
            throw new Exception("Error while selecting event.");
        }
    }

    public void clickEventDescription() throws Exception {
        try {
            click(captureNewClaimPageObjects.getEventDescription());
        } catch (Exception e) {
            log.error("Error while clicking event description.");
            throw new Exception("Error while clicking event description.");
        }
    }

    public void clearEventDescription() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getEventDescription());
        } catch (Exception e) {
            log.error("Error while clearing event description.");
            throw new Exception("Error while clearing event description.");
        }
    }

    public void updateEventDescription(String eventDescription) throws Exception {
        try {
            clearEventDescription();
            type(captureNewClaimPageObjects.getEventDescription(), eventDescription);
        } catch (Exception e) {
            log.error("Error while updating event description.");
            throw new Exception("Error while updating event description.");
        }
    }

    public String getEventDescription() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getEventDescription());
        } catch (Exception e) {
            log.error("Error while getting event description.");
            throw new Exception("Error while getting event description.");
        }
    }

    public void clickEventDate() throws Exception {
        try {
            click(captureNewClaimPageObjects.getEventDate());
        } catch (Exception e) {
            log.error("Error while clicking event date.");
            throw new Exception("Error while clicking event date.");
        }
    }

    public void clearEventDate() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getEventDate());
        } catch (Exception e) {
            log.error("Error while clearing event date.");
            throw new Exception("Error while clearing event date.");
        }
    }

    public void updategetEventDate(String eventDate) throws Exception {
        try {
            clearEventDate();
            type(captureNewClaimPageObjects.getEventDate(), eventDate);
        } catch (Exception e) {
            log.error("Error while updating event date.");
            throw new Exception("Error while updating event date.");
        }
    }

    public String getEventDate() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getEventDate());
        } catch (Exception e) {
            log.error("Error while getting event date.");
            throw new Exception("Error while getting event date.");
        }
    }

    public String getClaimsRegDate() throws Exception {
        try {
            return getAttribute(captureNewClaimPageObjects.getClaimsRegDate(), "value");
        } catch (Exception e) {
            log.error("Error while getting Claims Reg Date.");
            throw new Exception("Error while getting Claims Reg Date.");
        }
    }

    public void selectClaimant(String claimant) throws Exception {
        try {
            String policyHolderName = getPolicyHolderName();
            log.info("Policy Holder Name: " + policyHolderName);
            claimant = policyHolderName;
            selectMatchingDataLabel(captureNewClaimPageObjects.getSelectClaimantLabel(), captureNewClaimPageObjects.getSelectClaimantItems(), claimant);
        } catch (Exception e) {
            log.error("Error while selecting claimant.");
            throw new Exception("Error while selecting claimant.");
        }
    }

    public String getStatus() throws Exception {
        try {
            return getAttribute(captureNewClaimPageObjects.getSelectStatus(), "value");
        } catch (Exception e) {
            log.error("Error while getting status.");
            throw new Exception("Error while getting status.");
        }
    }

    public void selectClaimantType(String claimantType) throws Exception {
        try {
            selectMatchingDataLabel(captureNewClaimPageObjects.getSelectTypeLabel(), captureNewClaimPageObjects.getSelectTypeItems(), claimantType);
        } catch (Exception e) {
            log.error("Error while selecting claimant type.");
            throw new Exception("Error while selecting claimant type.");
        }
    }

    public void clickAttorneyCompanyName() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAttorneyCompanyName());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney CompanyName.");
            throw new Exception("Error while clicking on Attorney CompanyName.");
        }
    }

    public void clearAttorneyCompanyName() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getAttorneyCompanyName());
        } catch (Exception e) {
            log.error("Error while clearing Attorney CompanyName.");
            throw new Exception("Error while clearing Attorney CompanyName.");
        }
    }

    public void updateAttorneyCompanyName(String attorneyCompanyName) throws Exception {
        try {
            clearAttorneyCompanyName();
            type(captureNewClaimPageObjects.getAttorneyCompanyName(), attorneyCompanyName);
        } catch (Exception e) {
            log.error("Error while updating Attorney CompanyName.");
            throw new Exception("Error while updating Attorney CompanyName.");
        }
    }

    public String getAttorneyCompanyName() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getAttorneyCompanyName());
        } catch (Exception e) {
            log.error("Error while getting Attorney CompanyName.");
            throw new Exception("Error while getting Attorney CompanyName.");
        }
    }

    public void clickAttorneyCellNumber() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAttorneyCellNumber());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Cell Number.");
            throw new Exception("Error while clicking on Attorney Cell Number.");
        }
    }

    public void clearAttorneyCellNumber() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getAttorneyCellNumber());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Cell Number.");
            throw new Exception("Error while clearing Attorney Cell Number.");
        }
    }

    public void updateAttorneyCellNumber(String attorneyCellNumber) throws Exception {
        try {
            clearAttorneyCellNumber();
            type(captureNewClaimPageObjects.getAttorneyCellNumber(), attorneyCellNumber);
        } catch (Exception e) {
            log.error("Error while updating Attorney Cell Number.");
            throw new Exception("Error while updating Attorney Cell Number.");
        }
    }

    public String getAttorneyCellNumber() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getAttorneyCellNumber());
        } catch (Exception e) {
            log.error("Error while getting Attorney Cell Number.");
            throw new Exception("Error while getting Attorney Cell Number.");
        }
    }

    public void clickAttorneyEmail() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAttorneyEmail());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Email.");
            throw new Exception("Error while clicking on Attorney Email.");
        }
    }

    public void clearAttorneyEmail() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getAttorneyEmail());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Email.");
            throw new Exception("Error while clearing Attorney Email.");
        }
    }

    public void updateAttorneyEmail(String attorneyEmail) throws Exception {
        try {
            clearAttorneyEmail();
            type(captureNewClaimPageObjects.getAttorneyEmail(), attorneyEmail);
        } catch (Exception e) {
            log.error("Error while updating Attorney Email.");
            throw new Exception("Error while updating Attorney Email.");
        }
    }

    public String getAttorneyEmail() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getAttorneyEmail());
        } catch (Exception e) {
            log.error("Error while getting Attorney Email.");
            throw new Exception("Error while getting Attorney Email.");
        }
    }

    public void clickAttorneyType() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAttorneyType());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Type.");
            throw new Exception("Error while clicking on Attorney Type.");
        }
    }

    public void clearAttorneyType() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getAttorneyType());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Type.");
            throw new Exception("Error while clearing Attorney Type.");
        }
    }

    public void updateAttorneyType(String attorneyType) throws Exception {
        try {
            clearAttorneyType();
            type(captureNewClaimPageObjects.getAttorneyType(), attorneyType);
        } catch (Exception e) {
            log.error("Error while updating Attorney Type.");
            throw new Exception("Error while updating Attorney Type.");
        }
    }

    public String getAttorneyType() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getAttorneyType());
        } catch (Exception e) {
            log.error("Error while getting Attorney Type.");
            throw new Exception("Error while getting Attorney Type.");
        }
    }

    public void clickSubmit() throws Exception {
        try {
            click(captureNewClaimPageObjects.getSubmit());
        } catch (Exception e) {
            log.error("Error while clicking on Submit.");
            throw new Exception("Error while clicking on Submit.");
        }
    }

    public void clickClear() throws Exception {
        try {
            click(captureNewClaimPageObjects.getClear());
        } catch (Exception e) {
            log.error("Error while clicking on Clear.");
            throw new Exception("Error while clicking on Clear.");
        }
    }

    public void scrollToProvince() throws Exception {
        try {
            scrollToElement(captureNewClaimPageObjects.getProvince());
        } catch (Exception e) {
            log.error("Error while scrolling on province.");
            throw new Exception("Error while scrolling on province.");
        }
    }

    public void scrollToAddAttorney() throws Exception {
        try {
            scrollToElement(captureNewClaimPageObjects.getAddAttorney());
        } catch (Exception e) {
            log.error("Error while scrolling to add attorney.");
            throw new Exception("Error while scrolling to add attorney.");
        }
    }

    public void moveToAndclickProvince() throws Exception {
        try {
            moveToElementAndClick(captureNewClaimPageObjects.getProvince());
        } catch (Exception e) {
            log.error("Error while clicking on province.");
            throw new Exception("Error while clicking on province.");
        }
    }

    public void clickProvince() throws Exception {
        try {
            click(captureNewClaimPageObjects.getProvince());
        } catch (Exception e) {
            log.error("Error while clicking on province.");
            throw new Exception("Error while clicking on province.");
        }
    }

    public String getProvince() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getProvince());
        } catch (Exception e) {
            log.error("Error while getting Province.");
            throw new Exception("Error while getting Province.");
        }
    }

    public void selectProvince(String province) throws Exception {
        try {
            selectMatchingDataLabel(captureNewClaimPageObjects.getProvinceLabel(), captureNewClaimPageObjects.getProvinceItems(), province.toUpperCase());
        } catch (Exception e) {
            log.error("Error while selecting province.");
            throw new Exception("Error while selecting province.");
        }
    }

    public void selectCity(String city) throws Exception {
        try {
            selectMatchingDataLabel(captureNewClaimPageObjects.getSelectCityLabel(), captureNewClaimPageObjects.getSelectCityItems(), city.toUpperCase());
        } catch (Exception e) {
            log.error("Error while selecting city.");
            throw new Exception("Error while selecting city.");
        }
    }

    public void clickCellNo() throws Exception {
        try {
            click(captureNewClaimPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Search Cell Number.");
            throw new Exception("Error while clicking on Attorney Search Cell Number.");
        }
    }

    public void clearCellNo() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Search Cell Number.");
            throw new Exception("Error while clearing Attorney Search Cell Number.");
        }
    }

    public void updateCellNo(String attorneyCompanyName) throws Exception {
        try {
            clearCellNo();
            type(captureNewClaimPageObjects.getCellNo(), attorneyCompanyName);
        } catch (Exception e) {
            log.error("Error while updating Attorney Search Cell Number.");
            throw new Exception("Error while updating Attorney Search Cell Number.");
        }
    }

    public String getCellNo() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while getting Attorney Search Cell Number.");
            throw new Exception("Error while getting Attorney Search Cell Number.");
        }
    }

    public void clickAttorneyName() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while clicking on Attorney Search Name.");
            throw new Exception("Error while clicking on Attorney Search Name.");
        }
    }

    public void clearAttorneyName() throws Exception {
        try {
            clear(captureNewClaimPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while clearing Attorney Search Name.");
            throw new Exception("Error while clearing Attorney Search Name.");
        }
    }

    public void updateAttorneyName(String attorneyCompanyName) throws Exception {
        try {
            clearAttorneyName();
            type(captureNewClaimPageObjects.getAttorneyName(), attorneyCompanyName);
        } catch (Exception e) {
            log.error("Error while updating Attorney Search Name.");
            throw new Exception("Error while updating Attorney Search Name.");
        }
    }

    public String getAttorneyName() throws Exception {
        try {
            return getText(captureNewClaimPageObjects.getAttorneyName());
        } catch (Exception e) {
            log.error("Error while getting Attorney Search Name.");
            throw new Exception("Error while getting Attorney Search Name.");
        }
    }

    public void clickSearch() throws Exception {
        try {
            click(captureNewClaimPageObjects.getSearch());
        } catch (Exception e) {
            log.error("Error while clicking on Search.");
            throw new Exception("Error while clicking on Search.");
        }
    }

    public void selectClaimsInformationTableDataRow(int row) throws Exception {
        try {
            selectUsingTableRowData(captureNewClaimPageObjects.getClaimsInformationTableData(), row);
        } catch (Exception e) {
            log.error("Error while selecting claims information table row.");
            throw new Exception("Error while selecting claims information table row.");
        }
    }

    public int getClaimsInformationTableSize() throws Exception {
        try {
            return getDataRiSize(captureNewClaimPageObjects.getClaimsInformationTableData());
        } catch (Exception e) {
            log.error("Error while selecting claims information table size.");
            throw new Exception("Error while selecting claims information table size.");
        }
    }

    public int getDataRiSize(WebElement tableDataItems) throws Exception {
        try {
            return tableDataItems.findElements(captureNewClaimPageObjects.getAttorneyInformationTable()).size();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void moveToAddAttorney() throws Exception {
        try {
            scrollToAddAttorney();
        } catch (Exception e) {
            log.error("Error while scrolling to Add Attorney.");
            throw new Exception("Error while scrolling to Add Attorney.");
        }
    }

    public void clickAddAttorney() throws Exception {
        try {
            click(captureNewClaimPageObjects.getAddAttorney());
        } catch (Exception e) {
            log.error("Error while clicking on Add Attorney.");
            throw new Exception("Error while clicking on Add Attorney.");
        }
    }

    public void clickCloseThick() throws Exception {
        try {
            click(captureNewClaimPageObjects.getCloseThick());
        } catch (Exception e) {
            log.error("Error while clicking on Close Thick.");
            throw new Exception("Error while clicking on Close Thick.");
        }
    }

    public void clickUiDialogTitleBarCloseUiCornerAll() throws Exception {
        try {
            Thread.sleep(500);
            moveToElementAndClick(captureNewClaimPageObjects.getUidialogTitleBarCloseUiCornerAll());
            Thread.sleep(500);
        } catch (Exception e) {
            log.error("Error while clicking on Ui dialog Title Bar Close Ui CornerAll.");
            throw new Exception("Error while clicking on Ui dialog Title Bar Close Ui CornerAll.");
        }
    }

}
