package co.za.fnb.flow.pages.policy_details;

import co.za.fnb.flow.pages.policy_details.information.QuoteInformation;
import co.za.fnb.flow.pages.policy_details.page_factory.PolicyDetailsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PolicyDetails extends BasePage {
    private Logger log  = LogManager.getLogger(PolicyDetails.class);
    PolicyDetailsPageObjects policyDetailsPageObjects = new PolicyDetailsPageObjects(driver);

    PolicyDetailsLinkedPoliciesSave policyDetailsLinkedPoliciesSave;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public PolicyDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
        policyDetailsLinkedPoliciesSave = new PolicyDetailsLinkedPoliciesSave(driver, scenarioOperator);
        createMultipleMintItemtable = new CreateMultipleMintItemTable(driver, scenarioOperator);
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return testHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        super.setErrorHandle(errorHandle);
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        super.setTestHandle(testHandle);
    }

    public QuoteInformation newMember() throws Exception {
        try {
            WebElement newMember = policyDetailsPageObjects.getNewMember();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 150);
            webDriverWait.until(ExpectedConditions.visibilityOf(newMember));
            log.debug("Clicking New Member.");
            click(newMember);
            log.debug("Clicked New Member.");
            return new QuoteInformation(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking on New Member.");
            throw new Exception("Error while clicking on New Member.");
        }
    }

    public QuoteInformation addToPolicy() throws Exception {
        try {
            click(policyDetailsPageObjects.getAddToPolicy());
            return new QuoteInformation(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking on Add To Policy.");
            throw new Exception("Error while clicking on Add To Policy.");
        }
    }

    public void deleteMember() throws Exception {
        try {
            click(policyDetailsPageObjects.getMemberDelete());
        } catch (Exception e) {
            log.error("Error while clicking on Delete Member.");
            throw new Exception("Error while clicking on Delete Member.");
        }
    }

    public void cancelPolicy() throws Exception {
        try {
            click(policyDetailsPageObjects.getCancelPolicy());
        } catch (Exception e) {
            log.error("Error while clicking on Cancel Policy.");
            throw new Exception("Error while clicking on Cancel Policy.");
        }
    }

    public void reinstatePolicy() throws Exception {
        try {
            click(policyDetailsPageObjects.getReinstatePolicy());
        } catch (Exception e) {
            log.error("Error while clicking on Reinstate Policy.");
            throw new Exception("Error while clicking on Reinstate Policy.");
        }
    }

    public void getQuote() throws Exception {
        try {
            click(policyDetailsPageObjects.getQuote());
        } catch (Exception e) {
            log.error("Error while clicking on Quote.");
            throw new Exception("Error while clicking on Quote.");
        }
    }

    public String getQuotedPremiumAmount() throws Exception {
        try {
            return getAttribute(policyDetailsPageObjects.getQuotedPremiumAmount(), "value");
        } catch (Exception e) {
            log.error("Error while getting Quoted Premium Amount.");
            throw new Exception("Error while Quoted Premium Amount.");
        }
    }

    public boolean checkShowPolicyDetailPoliciesDialog() {
        try {
            return policyDetailsPageObjects.getShowPolicyDetailPoliciesDialogTitle().isDisplayed();
        } catch (Exception e) {
            return false;
        }
    }

    public void save(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on save.");
        click(policyDetailsPageObjects.getSave());
        Thread.sleep(1000);
        getResponses(true);
        try {
            Thread.sleep(2000);
            policyDetailsLinkedPoliciesSave.save();
            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        // TODO: Decide when to get the audit trail (before or after processing the results)
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void save(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Clicking on save.");
        click(policyDetailsPageObjects.getSave());
        getResponses(true);
        int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }

        try {
            policyDetailsLinkedPoliciesSave.save();
            getResponses(true);
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSave(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Proceeding after save.");
        getResponses(true);
        int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }

        try {
            policyDetailsLinkedPoliciesSave.save();
            getResponses(true);
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSubmit(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        log.info("Proceeding after submit.");
        getResponses(true);
        int numberOfFrames = 0;
        int waitCount = 0;
        boolean policiesDialog = false;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            policiesDialog = waitCount == 1 && checkShowPolicyDetailPoliciesDialog();
            if (policiesDialog) {
                break;
            }

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                if (!checkShowPolicyDetailPoliciesDialog()) {
                    throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
                } else {
                    break;
                }
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);
        if (!policiesDialog && numberOfFrames >= 1) {
            getResponses(true);
        }

        try {
            // policyDetailsLinkedPoliciesSave.save();
            // getResponses(true);
        } catch (Exception e) {
            log.error("Extra Popup: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Extra Popup: " + e.getMessage());
        }
        getResponses(true);
        Thread.sleep(5000);
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemForm();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void proceedAfterClickingSave(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on save.");
        click(policyDetailsPageObjects.getSave());
        Thread.sleep(1000);
        getResponses(true);
        try {
            Thread.sleep(2000);
            policyDetailsLinkedPoliciesSave.save();
            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        // TODO: Decide when to get the audit trail (before or after processing the results)
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void submitClaim(String[] popUpWorkType, String[] popUpStatus,String[] popUpQueue, String commentCategoryValue,String commentValue) throws Exception {
        log.info("Clicking on submit logic.");
        click(policyDetailsPageObjects.getSave());
        Thread.sleep(1000);
        getResponses(true);
        try {
            Thread.sleep(2000);
            policyDetailsLinkedPoliciesSave.save();
            Thread.sleep(3000);
            getResponses(true);
        } catch (Exception e) {
            log.error("Error on save: " + e.getMessage());
            errorHandle = createMultipleMintItemtable.getErrorHandle();
            testHandle = createMultipleMintItemtable.getTestHandle();
            throw new Exception("Error on save: " + e.getMessage());
        }
        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.setCommentCategoryValue(commentCategoryValue);
        createMultipleMintItemtable.setCommentValue(commentValue);
        createMultipleMintItemtable.createMintItemForm();
        // TODO: Decide when to get the audit trail (before or after processing the results)
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void submitClaim(String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue) throws Exception {
        int numberOfFrames = 0;
        int waitCount = 0;
        final int countStop = 10;
        while (numberOfFrames == 0 && waitCount < countStop) {
            log.info("Waiting 2 seconds for create item popup after save.");
            Thread.sleep(2000);
            numberOfFrames = driver.findElements(policyDetailsPageObjects.iFrameTagBy).size();
            waitCount++;

            if (numberOfFrames == 0 && waitCount == countStop - 1) {
                throw new Exception("Create/update popup not yet displayed. Probably a performance issue.");
            }
        }
        log.info("Number of iFrames on screen: " + numberOfFrames);

        createMultipleMintItemtable.setPopUpWorkType(popUpWorkType);
        createMultipleMintItemtable.setPopUpStatus(popUpStatus);
        createMultipleMintItemtable.setPopUpQueue(popUpQueue);
        createMultipleMintItemtable.createMintItemFormSubmit();
        getResponses(true);
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public void getResults() throws Exception {
        createMultipleMintItemtable.getTestResults();
        errorHandle = createMultipleMintItemtable.getErrorHandle();
        testHandle = createMultipleMintItemtable.getTestHandle();
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return createMultipleMintItemtable;
    }
}
