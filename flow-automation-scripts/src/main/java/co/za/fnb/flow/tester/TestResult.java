package co.za.fnb.flow.tester;

public enum TestResult {
    PASS {
        public String toString() {
            return "PASS";
        }
    },
    FAIL {
        public String toString() {
            return "FAIL";
        }
    }
}
