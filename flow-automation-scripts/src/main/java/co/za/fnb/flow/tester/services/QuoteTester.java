package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.*;
import generated.Error;
import generated.ObjectFactory;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.List;
import java.util.Properties;

public class QuoteTester {
    private Logger log  = LogManager.getLogger(QuoteTester.class);
    private QuoteAddRoleRequestInput quoteAddRoleRequestInput;
    private QuoteDeleteRoleRequestInput quoteDeleteRoleRequestInput;
    private QuoteUpdateRoleRequestInput quoteUpdateRoleRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private QuoteAddRoleResponseOutput quoteAddRoleResponseOutput;
    private QuoteDeleteRoleResponseOutput quoteDeleteRoleResponseOutput;
    private QuoteUpdateRoleResponseOutput quoteUpdateRoleResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public QuoteTester(QuoteAddRoleRequestInput quoteAddRoleRequestInput) {
        this.quoteAddRoleRequestInput = quoteAddRoleRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(QuoteAddRoleRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(quoteAddRoleRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(QuoteAddRoleResponseOutput.class);

            setupEnvironmentVariables();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public QuoteTester(QuoteDeleteRoleRequestInput quoteDeleteRoleRequestInput) {
        this.quoteDeleteRoleRequestInput = quoteDeleteRoleRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(QuoteDeleteRoleRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(quoteDeleteRoleRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(QuoteDeleteRoleResponseOutput.class);

            setupEnvironmentVariables();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public QuoteTester(QuoteUpdateRoleRequestInput quoteUpdateRoleRequestInput) {
        this.quoteUpdateRoleRequestInput = quoteUpdateRoleRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(QuoteUpdateRoleRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(quoteUpdateRoleRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(QuoteUpdateRoleResponseOutput.class);

            setupEnvironmentVariables();
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    private void setupEnvironmentVariables() {
        log.info("Loading Properties");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        environment = properties.getProperty("ENVIROMENT");
        log.info("Done Loading Properties");
    }

    public QuoteAddRoleRequestInput getQuoteAddRoleRequestInput() {
        return quoteAddRoleRequestInput;
    }

    public QuoteDeleteRoleRequestInput getQuoteDeleteRoleRequestInput() {
        return quoteDeleteRoleRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public QuoteAddRoleResponseOutput getQuoteAddRoleResponseOutput() {
        return quoteAddRoleResponseOutput;
    }

    public QuoteDeleteRoleResponseOutput getQuoteDeleteRoleResponseOutput() {
        return quoteDeleteRoleResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public QuoteAddRoleResponseOutput sendQuoteAddRoleRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvQUOTEADDROLEInput quoteAddRoleInput = serviceObjectFactory.createGensrvQUOTEADDROLEInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        quoteAddRoleInput.setPXMLIN(pxmlin);
        GensrvQUOTEADDROLEResult gensrvQUOTEADDROLEResult = gsd00030PRServicesPort.gensrvQuoteaddrole(quoteAddRoleInput);
        PXMLOUT quoteAddRoleResult = gensrvQUOTEADDROLEResult.getPXMLOUT();
        String pXmlOutString = quoteAddRoleResult.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, QuoteAddRoleResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             quoteAddRoleResponseOutput = (QuoteAddRoleResponseOutput) unMarshal;
             return quoteAddRoleResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             Errors errors = responseErrors.getErrors();
             List<Error> error = errors.getError();
             StringBuilder stringBuilder = new StringBuilder();
             int size = error.size();
             if (size > 0) {
                 Error firstError = error.get(0);
                 String code = firstError.getCode();
                 String subject = firstError.getSubject();
                 String adjunct = firstError.getAdjunct();
                 String description = firstError.getDescription();

                 stringBuilder.append("Code: " + code).append("\n");
                 stringBuilder.append("Subject: " + subject).append("\n");
                 stringBuilder.append("Adjunct: " + adjunct).append("\n");
                 stringBuilder.append("Description: " + description).append("\n");
             }
             throw new Exception("Error while getting QuoteAddRoleResponseOutput: \n" + stringBuilder.toString());
         }
    }

    public QuoteDeleteRoleResponseOutput sendQuoteDeleteRoleRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvQUOTEDELETEROLEInput quoteDeleteRoleInput = serviceObjectFactory.createGensrvQUOTEDELETEROLEInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        quoteDeleteRoleInput.setPXMLIN(pxmlin);
        GensrvQUOTEDELETEROLEResult gensrvQUOTEDELETEROLEResult = gsd00030PRServicesPort.gensrvQuotedeleterole(quoteDeleteRoleInput);
        PXMLOUT quoteDeleteRoleResult = gensrvQUOTEDELETEROLEResult.getPXMLOUT();
        String pXmlOutString = quoteDeleteRoleResult.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, QuoteDeleteRoleResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             quoteDeleteRoleResponseOutput = (QuoteDeleteRoleResponseOutput) unMarshal;
             return quoteDeleteRoleResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting QuoteDeleteRoleResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }

    public QuoteUpdateRoleResponseOutput sendQuoteUpdateRoleRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvQUOTEUPDATEROLEInput quoteUpdateRoleInput = serviceObjectFactory.createGensrvQUOTEUPDATEROLEInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        quoteUpdateRoleInput.setPXMLIN(pxmlin);
        GensrvQUOTEUPDATEROLEResult gensrvQUOTEDELETEROLEResult = gsd00030PRServicesPort.gensrvQuoteupdaterole(quoteUpdateRoleInput);
        PXMLOUT quoteDeleteRoleResult = gensrvQUOTEDELETEROLEResult.getPXMLOUT();
        String pXmlOutString = quoteDeleteRoleResult.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, QuoteUpdateRoleResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
        if (!(unMarshal instanceof ResponseErrors)) {
            quoteUpdateRoleResponseOutput = (QuoteUpdateRoleResponseOutput) unMarshal;
            return quoteUpdateRoleResponseOutput;
        } else {
            responseErrors = (ResponseErrors) unMarshal;
            throw new Exception("Error while getting QuoteUpdateRoleResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
        }
    }
}
