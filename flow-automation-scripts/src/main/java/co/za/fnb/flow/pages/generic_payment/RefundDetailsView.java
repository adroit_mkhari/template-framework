package co.za.fnb.flow.pages.generic_payment;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.generic_payment.page_factory.RefundDetailsViewPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class RefundDetailsView extends BasePage {
    private Logger log  = LogManager.getLogger(RefundDetailsView.class);
    RefundDetailsViewPageObjects refundDetailsViewPageObjects = new RefundDetailsViewPageObjects(driver);

    public RefundDetailsView(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickApprove() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getApprove());
        } catch (Exception e) {
            log.error("Error while double clicking on Approve.");
            throw new Exception("Error while double clicking on Approve.");
        }
    }

    public void clickYesApprove() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getYesApprove());
        } catch (Exception e) {
            log.error("Error while double clicking on Yes Approve.");
            throw new Exception("Error while double clicking on Yes Approve.");
        }
    }

    public void clickNoDoNotApprove() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getDoNotApprove());
        } catch (Exception e) {
            log.error("Error while double clicking on No Do Not Approve.");
            throw new Exception("Error while double clicking on No Do Not Approve.");
        }
    }

    public void clickDecline() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getDecline());
        } catch (Exception e) {
            log.error("Error while double clicking on Decline.");
            throw new Exception("Error while double clicking on Decline.");
        }
    }

    public void clickYesDecline() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getYesDecline());
        } catch (Exception e) {
            log.error("Error while double clicking on Yes Decline.");
            throw new Exception("Error while double clicking on Yes Decline.");
        }
    }

    public void clickNoDoNotDecline() throws Exception {
        try {
            click(refundDetailsViewPageObjects.getDoNotDecline());
        } catch (Exception e) {
            log.error("Error while double clicking on No Do Not Decline.");
            throw new Exception("Error while double clicking on No Do Not Decline.");
        }
    }
}
