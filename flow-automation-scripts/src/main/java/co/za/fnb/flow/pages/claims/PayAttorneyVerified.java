package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.PayAttorneyPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

public class PayAttorneyVerified extends BasePage {
    private Logger log  = LogManager.getLogger(PayAttorneyVerified.class);
    PayAttorneyPageObjects payAttorneyPageObjects = new PayAttorneyPageObjects(driver);
    Actions actions = new Actions(driver);

    public PayAttorneyVerified(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickSupplierZero() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierZero()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier Zero.");
            throw new Exception("Error while clicking on Supplier Zero.");
        }
    }

    public void clickSupplierOne() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierOne()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier One.");
            throw new Exception("Error while clicking on Supplier One.");
        }
    }

    public void clickSupplierTwo() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierTwo()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier Two.");
            throw new Exception("Error while clicking on Supplier Two.");
        }
    }

    public void clickSupplierThree() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierThree()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier Three.");
            throw new Exception("Error while clicking on Supplier Three.");
        }
    }

    public void clickSupplierFour() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierFour()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier Four.");
            throw new Exception("Error while clicking on Supplier Four.");
        }
    }

    public void clickSupplierFive() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSupplierFive()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Supplier Five.");
            throw new Exception("Error while clicking on Supplier Five.");
        }
    }

    public void clickVatConfirmationZero() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getVatConfirmationZero()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Vat Confirmation Zero.");
            throw new Exception("Error while clicking on Vat Confirmation Zero.");
        }
    }

    public void clickVatConfirmationOne() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getVatConfirmationOne()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Vat Confirmation One.");
            throw new Exception("Error while clicking on Vat Confirmation One.");
        }
    }

    public void clickVatConfirmationTwo() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getVatConfirmationTwo()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Vat Confirmation Two.");
            throw new Exception("Error while clicking on Vat Confirmation Two.");
        }
    }

    public void clickFrsZero() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getFrsZero()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on FRS Zero.");
            throw new Exception("Error while clicking on FRS Zero.");
        }
    }

    public void clickFrsOne() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getFrsOne()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on FRS One.");
            throw new Exception("Error while clicking on FRS One.");
        }
    }

    public void clickFrsTwo() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getFrsTwo()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on FRS Two.");
            throw new Exception("Error while clicking on FRS Two.");
        }
    }

    public void clickAmountExclusiveRadioZero() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getAmountExclusiveRadioZero()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Amount Exclusive Radio Zero.");
            throw new Exception("Error while clicking on Amount Exclusive Radio Zero.");
        }
    }

    public void clickAmountExclusiveRadioOne() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getAmountExclusiveRadioOne()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Amount Exclusive Radio One.");
            throw new Exception("Error while clicking on Amount Exclusive Radio One.");
        }
    }

    public void clickIdt326() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getIdt326()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on idt 326.");
            throw new Exception("Error while clicking on idt 326.");
        }
    }

    public void clickIdt327() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getIdt327()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on idt 327.");
            throw new Exception("Error while clicking on idt 327.");
        }
    }

    public void clickIdt328() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getIdt328()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on idt 328.");
            throw new Exception("Error while clicking on idt 328.");
        }
    }

    public void clickConfirmationCancel() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getConfirmationCancel()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Confirmation Cancel.");
            throw new Exception("Error while clicking on Confirmation Cancel.");
        }
    }

    public void clickSubmitConfirmation() throws Exception {
        try {
            actions.moveToElement(payAttorneyPageObjects.getSubmitConfirmation()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Submit Confirmation.");
            throw new Exception("Error while clicking on Submit Confirmation.");
        }
    }

}
