package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.Query;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.ValidationException;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.services.functions.CancelPolicy;
import co.za.fnb.flow.tester.services.functions.Takeup;
import co.za.fnb.flow.tester.services.functions.UpdatePolicy;
import co.za.fnb.flow.tester.services.models.TakeupScenario;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import generated.*;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class CancelPolicyServiceLevelRunner {
    private Logger log  = LogManager.getLogger(CancelPolicyServiceLevelRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    DataTable dataTable;
    ScenarioTester scenarioTester;

    public CancelPolicyServiceLevelRunner(DataTable dataTable, ScenarioTester scenarioTester) {
        this.dataTable = dataTable;
        this.scenarioTester = scenarioTester;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public ScenarioTester getScenarioTester() {
        return scenarioTester;
    }

    public void setScenarioTester(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        if (!scenarioOperator.getProductName().toUpperCase().contains("LOC")) {
            String updatedTe = getCellValue(headers, dataEntry,"Updatedte");
            String risk = getCellValue(headers, dataEntry,"Risk");
            String sanction = getCellValue(headers, dataEntry,"Sanction");
            String edd = getCellValue(headers, dataEntry,"Edd");
            String kyc = getCellValue(headers, dataEntry,"Kyc");
            String ficaStatus = getCellValue(headers, dataEntry,"Status");
            queryHandler.updateFicaStatus(
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable();
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    private void updatePolicyStatus(ScenarioOperator scenarioOperator, String requiredPolicyStatus) throws Exception {
        String requiredPolicyStatusId = null;

        if (requiredPolicyStatus.equalsIgnoreCase("PREACTIVE")) {
            requiredPolicyStatusId = "1103";
        } else if (requiredPolicyStatus.equalsIgnoreCase("IN FORCE")) {
            requiredPolicyStatusId = "6";
        } else if (requiredPolicyStatus.equalsIgnoreCase("LAPSED")) {
            requiredPolicyStatusId = "7";
        } else if (requiredPolicyStatus.equalsIgnoreCase("CANCELLED")) {
            requiredPolicyStatusId = "9";
        } else if (requiredPolicyStatus.equalsIgnoreCase("DECEASED")) {
            requiredPolicyStatusId = "10";
        } else if (requiredPolicyStatus.equalsIgnoreCase("NTU EXIT")) {
            requiredPolicyStatusId = "1104";
        }

        if (requiredPolicyStatusId != null) {
            queryHandler.changePolicyStatus(scenarioOperator, requiredPolicyStatusId);
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting Scenario Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        scenarioTester.setHeaders(headers);
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber, takeupFirst, takeupCode, updateFicaStatus;

        if (Arrays.asList(headers).contains("Test Case No")) {
            for (Row row : dataTable.getGherkinRows()) {
                if (row.getLine() != 1) {
                    dataEntry = row.getCells();
                    scenarioTester.setDataEntry(dataEntry);

                    run = getCellValue(headers, dataEntry,"Run");
                    if (run.equalsIgnoreCase("YES")) {
                        testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                        scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                        testFunction = getCellValue(headers, dataEntry,"Function");
                        productName = getCellValue(headers, dataEntry,"Product Name");
                        scenarioOperator.setTestCaseNumber(testCaseNumber);
                        scenarioOperator.setScenarioDescription(scenarioDescription);
                        scenarioOperator.setFunction(testFunction);
                        scenarioOperator.setProductName(productName);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                        scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                        // BasePage.setScenarioOperator(scenarioOperator);

                        try {
                            takeupFirst = getCellValue(headers, dataEntry,"Takeup");
                            takeupCode = getCellValue(headers, dataEntry,"Takeup Code");
                            policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                            policyNumberQuery = getCellValue(headers, dataEntry,"Policy Number Query");
                            policyCode = getCellValue(headers, dataEntry,"Policy No");

                            if (takeupFirst.equalsIgnoreCase("YES") && !takeupCode.isEmpty()) {
                                TakeupScenario takeupScenario = TakeupScenarioFactory.getTakeupScenario(takeupCode);
                                Takeup takeup = new Takeup(scenarioTester, takeupScenario);
                                try {
                                    takeup.setupPolicyTakeUpRequestInputFromTakeupScenario();
                                    PolicyTakeUpResponse policyTakeUpResponse = takeup.sendRequest();
                                    PolicyTakeUpResponsePayload response = policyTakeUpResponse.getResponse();
                                    PolicyTakeUpResponsePolicyData policyData = response.getPolicyData();

                                    if (policyData != null) {
                                        policyNumber = policyData.getPolicyNo();
                                        log.info("Successfully Created Policy");
                                        log.info("------------------------------");
                                        log.info("Policy Number: " + policyNumber);
                                        log.info("------------------------------");

                                        queryHandler.setDbPolicyNumber(policyNumber);
                                        policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                        scenarioOperator.setPolicyNumber(policyNumber);
                                    } else {
                                        PolicyTakeUpResponseCommonData commonData = response.getCommonData();
                                        if (commonData != null) {
                                            String errorCode = commonData.getErrorcode();
                                            String resultMsg = commonData.getResultmsg();

                                            log.error("Error Code: " + errorCode + ", Result Message: " + resultMsg);
                                            throw new Exception("Error Code: " + errorCode + ", Result Message: " + resultMsg);
                                        }
                                    }
                                } catch (Exception e) {
                                    log.error("Error while taking up policy. " + e.getMessage());
                                    throw new Exception("Error while taking up policy. " + e.getMessage());
                                }
                            } else {
                                if (policyNumber.isEmpty()) {
                                    try {
                                        if (policyNumberQuery.isEmpty()) {
                                            if (policyCode.isEmpty()) {
                                                policyNumber = "No Policy Number";
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            } else {
                                                Query query = QueryFactory.getQuery(policyCode);
                                                ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(query.getQuery());
                                                if (policyNumberQueryResults.next()) {
                                                    policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                } else {
                                                    policyCode = query.getPolicyCode();
                                                    policyNumberQuery = query.getQuery();
                                                    policyNumber = null;
                                                }

                                                if (policyNumber == null) {
                                                    throw new Exception("Error getting policy number for policy code: " + policyCode);
                                                }
                                                queryHandler.setDbPolicyNumber(policyNumber);
                                                policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            }
                                        } else {
                                            ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(policyNumberQuery);
                                            if (policyNumberQueryResults.next()) {
                                                policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                queryHandler.setDbPolicyNumber(policyNumber);
                                                policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            }
                                        }
                                    } catch (Exception e) {
                                        throw new Exception("Error getting policy number. Policy Code: " + policyCode + " Query: " + policyNumberQuery);
                                    }
                                } else {
                                    scenarioOperator.setPolicyNumber(policyNumber);
                                    queryHandler.setDbPolicyNumber(policyNumber);
                                }
                            }

                            log.info("===========================================================================");
                            log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            log.info("---------------------------------------------------------------------------");

                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

                            // Query For Policy To Ensure It Gets Added To The FICA Table
                            scenarioTester.setScenarioOperator(scenarioOperator);
                            UpdatePolicy updatePolicy = new UpdatePolicy(scenarioTester);
                            String username = scenarioTester.getCellValue("Username");
                            ResponsePolicyBasic policyBasic = updatePolicy.getPolicyBasic(policyNumber, username);

                            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
                            if (dbPolicyNumber != null) {
                                String policyStatus = policyBasic.getPolicystatus(); // Only Change If Its Not The Same As The Expected.
                                String requiredPolicyStatus = scenarioTester.getCellValue("Required Policy Status");
                                if (!policyStatus.equalsIgnoreCase(requiredPolicyStatus)) {
                                    updatePolicyStatus(scenarioOperator, requiredPolicyStatus);
                                }

                                updateFicaStatus = getCellValue(headers, dataEntry,"Update Fica Status");
                                if (updateFicaStatus.equalsIgnoreCase("YES")) {
                                    updateFicaStatus(scenarioOperator, headers, dataEntry);
                                }
                            }

                            CancelPolicy cancelPolicy = new CancelPolicy(scenarioTester);
                            try {
                                cancelPolicy.sendRequest();

                                cancelPolicy.performValidations();

                                // TODO: Proceed To Reporting Success
                                reportSuccess(reportableFields, scenarioOperator);
                            } catch (Exception e) {
                                if (e instanceof ValidationException) {
                                    // TODO: Proceed To Reporting Failure With Exception Message.
                                    reportFailure(reportableFields, scenarioOperator, e.getMessage());
                                } else {
                                    // TODO: Proceed To Reporting Failure With Exception Message. But Flag It As Technical On The Report.
                                    reportTechnicalFailure(reportableFields, scenarioOperator, e.getMessage());
                                    e.printStackTrace();
                                }
                            }

                            log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            e.printStackTrace();
                            reportFailure(reportableFields, scenarioOperator, e.getMessage());
                        } finally {
                            scenarioOperator.increamentReportRowIndex();
                            // TODO: Things that need to be done post the logic above.
                        }
                    }
                }
            }
        }

        log.info("Saving Report.");
        scenarioOperator.saveReport();
        log.info("===========================================================================");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }

    private void reportTechnicalFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.WARNING);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
