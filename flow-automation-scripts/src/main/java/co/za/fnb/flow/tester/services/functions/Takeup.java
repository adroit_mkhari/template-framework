package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.models.custom_exceptions.ValidationException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.PolicyTakeUpTester;
import co.za.fnb.flow.tester.services.models.*;
import co.za.fnb.flow.tester.services.models.Beneficiary;
import co.za.fnb.flow.tester.services.models.PolicyHolder;
import generated.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.LinkedList;
import java.util.List;

public class Takeup {
    private Logger log  = LogManager.getLogger(Takeup.class);
    private PolicyTakeUpRequestInput policyTakeUpRequestInput;
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private TakeupScenario takeupScenario;
    private String policyHolderIdNumber;
    private String authorizedPersonIdNumber;
    private int takeupRetryCount = 0;
    private QueryHandler queryHandler = new QueryHandler();

    public Takeup(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public Takeup(ScenarioTester scenarioTester, TakeupScenario takeupScenario) {
        this.scenarioTester = scenarioTester;
        this.takeupScenario = takeupScenario;
    }

    public PolicyTakeUpRequestInput getPolicyTakeUpRequestInput() {
        return policyTakeUpRequestInput;
    }

    public void setPolicyTakeUpRequestInput(PolicyTakeUpRequestInput policyTakeUpRequestInput) {
        this.policyTakeUpRequestInput = policyTakeUpRequestInput;
    }

    public TakeupScenario getTakeupScenario() {
        return takeupScenario;
    }

    public void setTakeupScenario(TakeupScenario takeupScenario) {
        this.takeupScenario = takeupScenario;
    }

    public PolicyTakeUpResponse sendRequest() throws Exception {
        try {
            if (policyTakeUpRequestInput == null) {
                policyTakeUpRequestInput = new PolicyTakeUpRequestInput();
                PolicyTakeUpCustomer policyTakeUpCustomer = setUpTakeUpCustomer();
                addPolicyHolder(policyTakeUpCustomer);
                addBeneficiaryFromTakeupScenario(policyTakeUpCustomer);

                String requestType = scenarioTester.getCellValue("Request Type");
                if (requestType.equalsIgnoreCase("LAW_ON_CALL_POLICY_CREATE")) {
                    String productName = scenarioTester.getCellValue("Takeup Product Name");
                    if (productName.equalsIgnoreCase("BUSINESS")) {
                        addAuthorizedPerson(policyTakeUpCustomer);
                    }
                }

                String spouseAction = scenarioTester.getCellValue("Spouse Action");
                if (spouseAction.equalsIgnoreCase("AD")) {
                    addSpouse(policyTakeUpCustomer);
                }

                String childRoleValues = scenarioTester.getCellValue("Child Role Id(s) - Action(s)");
                String[] childRoleValuesList = childRoleValues.split("],");
                List<MemberVariableFields> takeUpChildMemberVariableFields = new LinkedList<>();
                getMemberVariableFields(childRoleValuesList, takeUpChildMemberVariableFields);

                for (MemberVariableFields childMemberVariableFields : takeUpChildMemberVariableFields) {
                    addChild(policyTakeUpCustomer, childMemberVariableFields);
                }

                String parentOrExtendedFamilyRoleValues = scenarioTester.getCellValue("Parent/Extended Family Role Id(s) - Action(s)");
                String[] parentOrExtendedFamilyRoleValuesList = parentOrExtendedFamilyRoleValues.split("],");
                List<MemberVariableFields> takeUpParentOrExtendedFamilyMemberVariableFields = new LinkedList<>();
                getMemberVariableFields(parentOrExtendedFamilyRoleValuesList, takeUpParentOrExtendedFamilyMemberVariableFields);

                for (MemberVariableFields parentOrExtendedFamilyMemberVariableFields : takeUpParentOrExtendedFamilyMemberVariableFields) {
                    addParentOrExtendedFamilyMember(policyTakeUpCustomer, parentOrExtendedFamilyMemberVariableFields);
                }

                policyTakeUpRequestInput.setCustomer(policyTakeUpCustomer);
            }

            PolicyTakeUpTester policyTakeUpTester = new PolicyTakeUpTester(policyTakeUpRequestInput);
            PolicyTakeUpResponse policyTakeUpResponse;
            StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
            try {
                String requestType;
                if (takeupScenario != null) {
                    requestType = takeupScenario.getRequestType();
                } else {
                    requestType = scenarioTester.getCellValue("Request Type");
                }

                if (requestType.equalsIgnoreCase("ACCIDENTAL_DEATH_POLICY_CREATE")) {
                    PolicyTakeUpCustomer customer = policyTakeUpRequestInput.getCustomer();
                    String referenceNumber = "OKS" + policyHolderIdNumber;
                    log.debug("Reference Number Set To: " + referenceNumber);
                    customer.setRefnumber(referenceNumber);
                    policyTakeUpRequestInput.setCustomer(customer);
                    policyTakeUpTester = new PolicyTakeUpTester(policyTakeUpRequestInput);
                }

                try {
                    PolicyTakeUpCustomer customer = policyTakeUpRequestInput.getCustomer();

                    if (customer.getLeadsno().isEmpty() || customer.getLeadsno().equalsIgnoreCase("TONYLEAD")) {
                        customer.setLeadsno("AUTOMATION" + getDateTimeStamp("dd_MM_yyyy_HH_mm_ss_nnnnnnnnn")
                                .replaceAll("_", "") + getRandomNumber(0,9));
                    }

                    queryHandler.addLeadNumber(customer.getLeadsno(),
                            customer.getAccountNumber(),
                            customer.getBranchCode(),
                            customer.getAccountType(),
                            "Automation",
                            customer.getUcnno());
                } catch (Exception sqlException) {
                    sqlException.printStackTrace();
                }

                policyTakeUpResponse = policyTakeUpTester.sendPolicyTakeUpRequest(requestType);
                PolicyTakeUpResponsePayload response = policyTakeUpResponse.getResponse();
                PolicyTakeUpResponsePolicyData policyData = response.getPolicyData();
                if (policyData == null) {
                    PolicyTakeUpResponseCommonData commonData = response.getCommonData();
                    if (commonData != null) {
                        String errorcode = commonData.getErrorcode();
                        String resultmsg = commonData.getResultmsg();
                        log.info("Error Code: " + errorcode);
                        throw new Exception("Result Message: " + resultmsg);
                    }
                }

                try {
                    String policyNo = policyData.getPolicyNo();
                    queryHandler.setDbPolicyNumber(policyNo);
                    ScenarioOperator scenarioOperator = new ScenarioOperator();
                    String policyStatus = takeupScenario.getPolicyStatus();
                    String productName = takeupScenario.getProductName();
                    scenarioOperator.setProductName(productName);
                    updatePolicyStatus(scenarioOperator, policyStatus);
                } catch (Exception e) {
                    log.debug("Error while updating policy status.");
                }

                Thread.sleep(10000);
                return policyTakeUpResponse;
            } catch (Exception e) {
                // TODO: Work on ways to handle errors for this service
                responseErrors = policyTakeUpTester.getResponseErrors();
                if (responseErrors != null) {
                    Errors errors = responseErrors.getErrors();
                    List<generated.Error> errorList = errors.getError();
                    for (generated.Error error : errorList) {
                        updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                                .append(", Subject: ").append(error.getSubject())
                                .append(", Description: ").append(error.getDescription())
                                .append(", Adjunct: ").append(error.getAdjunct())
                                .append("\n");
                    }
                    throw new ServiceException(updatePolicyErrorStringBuilder.toString());
                } else {
                    log.error("Tekeup Error: " + e.getMessage());
                    throw new ServiceException("Tekeup Error: " + e.getMessage());
                }
            }

        } catch (Exception e) {
            String message = e.getMessage();
            log.error("Error while testing Policy Takeup. Error: " + message);
            if (takeupRetryCount++ == 0 || (message.contains("CLIENT HAS EXISTING ACTIVE PRODUCT") && takeupRetryCount < 5)
                    || (message.contains("UNSUCCESSFUL-Invalid Date for Member POLICY TAKEUP AUTOMATION") && takeupRetryCount < 5)) {
                if (message.contains("CLIENT HAS EXISTING ACTIVE PRODUCT")
                        || message.contains("UNSUCCESSFUL-Invalid Date for Member POLICY TAKEUP AUTOMATION")) {
                    PolicyTakeUpCustomer customer = policyTakeUpRequestInput.getCustomer();
                    List<PolicyTakeUpCustomerMember> members = customer.getMember();
                    PolicyTakeUpCustomerMember policyTakeUpCustomerMember = members.get(0);
                    String relationship = policyTakeUpCustomerMember.getRelationship();
                    String birthDate = policyTakeUpCustomerMember.getBirthdate();
                    String idType = policyTakeUpCustomerMember.getIdType();
                    String gender = policyTakeUpCustomerMember.getGender();
                    if (relationship.equalsIgnoreCase("PH")) {
                        policyHolderIdNumber = "";
                        policyHolderIdNumber = scenarioTester.generateRandomSaId(birthDate, idType, policyHolderIdNumber, gender);
                        policyTakeUpCustomerMember.setIdNumber(policyHolderIdNumber);

                        birthDate = getBirthDateFromIdNumber();

                        policyTakeUpCustomerMember.setBirthdate(birthDate);
                    } else if (relationship.equalsIgnoreCase("AU")) {
                        authorizedPersonIdNumber = "";
                        authorizedPersonIdNumber = scenarioTester.generateRandomSaId(birthDate, idType, authorizedPersonIdNumber, gender);
                        customer.setCompRegNo(authorizedPersonIdNumber);
                    }
                }
                log.info("Retrying Takeup: " + e.getMessage());
                return sendRequest();
            } else if (e instanceof ValidationException) {
                throw new ValidationException(message);
            } else if (e instanceof ServiceException) {
                throw new ServiceException(message);
            } else {
                throw new Exception("Error while testing Update Policy. Error: " + message);
            }
        }
    }

    private String getBirthDateFromIdNumber() {
        String birthDate;
        String centuaryYears = policyHolderIdNumber.substring(0,2);
        Integer integerCentuaryYears = Integer.valueOf(centuaryYears);

        String dateTimeStamp = getDateTimeStamp();
        String[] splitDateTimeStamp = dateTimeStamp.split("_");
        String year = splitDateTimeStamp[2];
        String yearSubstring = year.substring(2, 4);
        Integer numberOfYears = Integer.valueOf(yearSubstring);

        if (integerCentuaryYears <= numberOfYears) {
            birthDate = "20" + policyHolderIdNumber.substring(0,6);
        } else {
            birthDate = "19" + policyHolderIdNumber.substring(0,6);
        }
        return birthDate;
    }

    private void updatePolicyStatus(ScenarioOperator scenarioOperator, String requiredPolicyStatus) throws Exception {
        String requiredPolicyStatusId = null;

        if (requiredPolicyStatus.equalsIgnoreCase("PREACTIVE")) {
            requiredPolicyStatusId = "1103";
        } else if (requiredPolicyStatus.equalsIgnoreCase("IN FORCE")) {
            requiredPolicyStatusId = "6";
        } else if (requiredPolicyStatus.equalsIgnoreCase("LAPSED")) {
            requiredPolicyStatusId = "7";
        } else if (requiredPolicyStatus.equalsIgnoreCase("CANCELLED")) {
            requiredPolicyStatusId = "9";
        } else if (requiredPolicyStatus.equalsIgnoreCase("DECEASED")) {
            requiredPolicyStatusId = "10";
        } else if (requiredPolicyStatus.equalsIgnoreCase("NTU EXIT")) {
            requiredPolicyStatusId = "1104";
        }

        if (requiredPolicyStatusId != null) {
            queryHandler.changePolicyStatus(scenarioOperator, requiredPolicyStatusId);
        }
    }

    private PolicyTakeUpCustomer setUpTakeUpCustomer() throws Exception {
        String refnumber = scenarioTester.getCellValue("Reference Number");
        String subChannel = scenarioTester.getCellValue("Sub Channel");
        String campaignNumber = scenarioTester.getCellValue("Campaign Number");
        String leadsNumber = scenarioTester.getCellValue("Leads Number");
        String ucnNumber = scenarioTester.getCellValue("UCN Number");
        String productCode = scenarioTester.getCellValue("Product Code");
        String subProdCode = scenarioTester.getCellValue("Sub Prod Code");
        String productName = scenarioTester.getCellValue("Takeup Product Name");
        String salesPerson = scenarioTester.getCellValue("Sales Person");
        String salesPersonNumber = scenarioTester.getCellValue("Sales Person Number");

        String capturedDate = scenarioTester.getCellValue("Captured Date");
        capturedDate = scenarioTester.getSimpleDateString(capturedDate);

        String inceptionDate = scenarioTester.getCellValue("Inception Date");
        inceptionDate = scenarioTester.getSimpleDateString(inceptionDate);

        String grossIncome = scenarioTester.getCellValue("Gross Income");
        String email = scenarioTester.getCellValue("Email");
        String cellNumber = scenarioTester.getCellValue("Cell Number");
        String homeNumber = scenarioTester.getCellValue("Home Number");
        String postalAddressStreet = scenarioTester.getCellValue("Postal Address Street");
        String postalAddressSuburb = scenarioTester.getCellValue("Postal Address Suburb");
        String postalAddresCity = scenarioTester.getCellValue("Postal Addres City");
        String postalAddressCode = scenarioTester.getCellValue("Postal Address Code");
        String bankName = scenarioTester.getCellValue("Bank Name");
        String bankBranchName = scenarioTester.getCellValue("Bank Branch Name");
        String bankBranchCode = scenarioTester.getCellValue("Bank Branch Code");
        String bankAccountNumber = scenarioTester.getCellValue("Bank Account Number");
        String bankAccountCode = scenarioTester.getCellValue("Bank Account Code");

        String paymentDay = scenarioTester.getCellValue("Payment Day");
        paymentDay = scenarioTester.getIntegerStringFromDoubleString(paymentDay);

        String dueDate = scenarioTester.getCellValue("Due Date");
        dueDate = scenarioTester.getSimpleDateString(dueDate);

        PolicyTakeUpCustomer policyTakeUpCustomer = new PolicyTakeUpCustomer();

        String dateTimeStamp = getDateTimeStamp("dd_MM_yyyy_HH_mm_ss_nnnnnnnnn");
        refnumber = subChannel + dateTimeStamp.replaceAll("_", "") + getRandomNumber(0,9);

        policyTakeUpCustomer.setRefnumber(refnumber);
        policyTakeUpCustomer.setSubChannel(subChannel);
        policyTakeUpCustomer.setCampaignNo(campaignNumber);
        policyTakeUpCustomer.setLeadsno(leadsNumber);
        policyTakeUpCustomer.setUcnno(ucnNumber);
        policyTakeUpCustomer.setProductCode(productCode);
        policyTakeUpCustomer.setSubProdCode(subProdCode);
        policyTakeUpCustomer.setProductName(productName);
        policyTakeUpCustomer.setSalesperson(salesPerson);
        policyTakeUpCustomer.setSalespersonno(salesPersonNumber);
        policyTakeUpCustomer.setCapturedDate(capturedDate);
        policyTakeUpCustomer.setInceptdate(inceptionDate);
        policyTakeUpCustomer.setGrossIncome(grossIncome);
        policyTakeUpCustomer.setEmail(email);
        policyTakeUpCustomer.setCellNo(cellNumber);
        policyTakeUpCustomer.setHomeNo(homeNumber);
        policyTakeUpCustomer.setPostaddstreet(postalAddressStreet);
        policyTakeUpCustomer.setPostaddsuburb(postalAddressSuburb);
        policyTakeUpCustomer.setPostaddcity(postalAddresCity);
        policyTakeUpCustomer.setPostaddcode(postalAddressCode);
        policyTakeUpCustomer.setBranchCode(bankBranchCode);
        policyTakeUpCustomer.setAccountNumber(bankAccountNumber);
        policyTakeUpCustomer.setAccountType(bankAccountCode);
        policyTakeUpCustomer.setPaydate(paymentDay);
        policyTakeUpCustomer.setDueDate(dueDate);

        String requestType = scenarioTester.getCellValue("Request Type");
        if (requestType.equalsIgnoreCase("LAW_ON_CALL_POLICY_CREATE")) {
            String coverAmount = scenarioTester.getCellValue("Cover Amount");
            policyTakeUpCustomer.setCoverAmount(coverAmount);

            if (productName.equalsIgnoreCase("BUSINESS")) {
                String salesPersonId = scenarioTester.getCellValue("Sales Person ID");
                String capturedBy = scenarioTester.getCellValue("Captured By");
                String capturedByNo = scenarioTester.getCellValue("Captured By No");
                String companyName = scenarioTester.getCellValue("Company Name");
                String tradingName = scenarioTester.getCellValue("Trading Name");
                String businessVatNo = scenarioTester.getCellValue("Business Vat No");
                String companyRegistrationNo = scenarioTester.getCellValue("Company Registration No");

                if (companyRegistrationNo.isEmpty()) {

                    companyRegistrationNo = authorizedPersonIdNumber;
                }

                policyTakeUpCustomer.setSalespersonID(salesPersonId);
                policyTakeUpCustomer.setCapturedby(capturedBy);
                policyTakeUpCustomer.setCapturedbyno(capturedByNo);
                policyTakeUpCustomer.setCompanyName(companyName);
                policyTakeUpCustomer.setTradingName(tradingName);
                policyTakeUpCustomer.setBusinessVatNo(businessVatNo);
                policyTakeUpCustomer.setCompRegNo(companyRegistrationNo);
            }
        }

        return policyTakeUpCustomer;
    }

    private PolicyTakeUpCustomer setUpTakeUpCustomerFromTakeupScenario() throws Exception {
        String refnumber = takeupScenario.getReferenceNumber();
        String subChannel = takeupScenario.getSubChannel();
        String campaignNumber = takeupScenario.getCampaignNumber();
        String leadsNumber = takeupScenario.getLeadsNumber();
        String ucnNumber = takeupScenario.getUcnNumber();
        String productCode = takeupScenario.getProductCode();
        String subProdCode = takeupScenario.getSubProdCode();
        String productName = takeupScenario.getProductName();
        String salesPerson = takeupScenario.getSalesPerson();
        String salesPersonNumber = takeupScenario.getSalesPersonNumber();

        String capturedDate = takeupScenario.getCapturedDate();
        capturedDate = scenarioTester.getSimpleDateString(capturedDate);

        String inceptionDate = takeupScenario.getInceptionDate();
        inceptionDate = scenarioTester.getSimpleDateString(inceptionDate);

        String grossIncome = takeupScenario.getGrossIncome();

        ContactDetails contactDetails = takeupScenario.getContactDetails();
        String email = contactDetails.getEmail();
        String cellNumber = contactDetails.getCellNumber();
        String homeNumber = contactDetails.getHomeNumber();
        String postalAddressStreet = contactDetails.getPostalAddressStreet();
        String postalAddressSuburb = contactDetails.getPostalAddressSuburb();
        String postalAddresCity = contactDetails.getPostalAddresCity();
        String postalAddressCode = contactDetails.getPostalAddressCode();

        BankingDetails bankingDetails = takeupScenario.getBankingDetails();
        String bankName = bankingDetails.getBankName();
        String bankBranchName = bankingDetails.getBankBranchName();
        String bankBranchCode = bankingDetails.getBankBranchCode();
        String bankAccountNumber = bankingDetails.getBankAccountNumber();
        String bankAccountCode = bankingDetails.getBankAccountCode();

        String paymentDay = bankingDetails.getPaymentDay();
        paymentDay = scenarioTester.getIntegerStringFromDoubleString(paymentDay);

        String dueDate = bankingDetails.getDueDate();
        dueDate = scenarioTester.getSimpleDateString(dueDate);

        PolicyTakeUpCustomer policyTakeUpCustomer = new PolicyTakeUpCustomer();

        // TODO: Generate Random Reference Number
        String dateTimeStamp = getDateTimeStamp("dd_MM_yyyy_HH_mm_ss_nnnnnnnnn");
        refnumber = subChannel + dateTimeStamp.replaceAll("_", "") + getRandomNumber(0,9);

        policyTakeUpCustomer.setRefnumber(refnumber);
        policyTakeUpCustomer.setSubChannel(subChannel);
        policyTakeUpCustomer.setCampaignNo(campaignNumber);
        policyTakeUpCustomer.setLeadsno(leadsNumber);
        policyTakeUpCustomer.setUcnno(ucnNumber);
        policyTakeUpCustomer.setProductCode(productCode);
        policyTakeUpCustomer.setSubProdCode(subProdCode);
        policyTakeUpCustomer.setProductName(productName);
        policyTakeUpCustomer.setSalesperson(salesPerson);
        policyTakeUpCustomer.setSalespersonno(salesPersonNumber);
        policyTakeUpCustomer.setCapturedDate(capturedDate);
        policyTakeUpCustomer.setInceptdate(inceptionDate);
        policyTakeUpCustomer.setGrossIncome(grossIncome);
        policyTakeUpCustomer.setEmail(email);
        policyTakeUpCustomer.setCellNo(cellNumber);
        policyTakeUpCustomer.setHomeNo(homeNumber);
        policyTakeUpCustomer.setPostaddstreet(postalAddressStreet);
        policyTakeUpCustomer.setPostaddsuburb(postalAddressSuburb);
        policyTakeUpCustomer.setPostaddcity(postalAddresCity);
        policyTakeUpCustomer.setPostaddcode(postalAddressCode);
        policyTakeUpCustomer.setBranchCode(bankBranchCode);
        policyTakeUpCustomer.setAccountNumber(bankAccountNumber);
        policyTakeUpCustomer.setAccountType(bankAccountCode);
        policyTakeUpCustomer.setPaydate(paymentDay);
        policyTakeUpCustomer.setDueDate(dueDate);

        String requestType = takeupScenario.getRequestType();
        if (requestType.equalsIgnoreCase("LAW_ON_CALL_POLICY_CREATE")) {
            String coverAmount = takeupScenario.getCoverAmount();
            policyTakeUpCustomer.setCoverAmount(coverAmount);

            if (productName.equalsIgnoreCase("BUSINESS")) {
                String salesPersonId = takeupScenario.getSalesPersonId();
                String capturedBy = takeupScenario.getCapturedBy();
                String capturedByNo = takeupScenario.getCapturedByNo();
                String companyName = takeupScenario.getCompanyName();
                String tradingName = takeupScenario.getTradingName();
                String businessVatNo = takeupScenario.getBusinessVatNo();
                String companyRegistrationNo = takeupScenario.getCompanyRegistrationNo();


                if (companyRegistrationNo.isEmpty()) {
                    AuthorizedPerson authorizedPerson = takeupScenario.getAuthorizedPerson();
                    String idNumber = authorizedPerson.getIdNumber();
                    String birthDate = authorizedPerson.getBirthDate();
                    String idType = authorizedPerson.getIdType();
                    String gender = authorizedPerson.getGender();
                    authorizedPersonIdNumber = idNumber;
                    authorizedPersonIdNumber = scenarioTester.generateRandomSaId(birthDate, idType, authorizedPersonIdNumber, gender);
                    companyRegistrationNo = authorizedPersonIdNumber;

                }

                policyTakeUpCustomer.setSalespersonID(salesPersonId);
                policyTakeUpCustomer.setCapturedby(capturedBy);
                policyTakeUpCustomer.setCapturedbyno(capturedByNo);
                policyTakeUpCustomer.setCompanyName(companyName);
                policyTakeUpCustomer.setTradingName(tradingName);
                policyTakeUpCustomer.setBusinessVatNo(businessVatNo);
                policyTakeUpCustomer.setCompRegNo(companyRegistrationNo);
            }
        }
        return policyTakeUpCustomer;
    }

    private void addPolicyHolder(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        String policyHolderRelationship = scenarioTester.getCellValue("Policy Holder Relationship");
        String policyHolderTittle = scenarioTester.getCellValue("Policy Holder Tittle");
        String policyHolderFirstName = scenarioTester.getCellValue("Policy Holder First Name");
        String policyHolderName = scenarioTester.getCellValue("Policy Holder Name");
        String policyHolderSurname = scenarioTester.getCellValue("Policy Holder Surname");
        String policyHolderMiddleName = scenarioTester.getCellValue("Policy Holder Middle Name");
        String policyHolderBirthDate = scenarioTester.getCellValue("Policy Holder Birth Date");
        String policyHolderIdType = scenarioTester.getCellValue("Policy Holder Id Type");
        policyHolderIdNumber = scenarioTester.getCellValue("Policy Holder Id Number");
        String policyHolderGender = scenarioTester.getCellValue("Policy Holder Gender");
        String policyHolderCoverAmount = scenarioTester.getCellValue("Policy Holder Cover Amount");
        String policyHolderPremium = scenarioTester.getCellValue("Policy Holder Premium");

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(policyHolderRelationship);

        if (!policyHolderTittle.isEmpty()) {
            policyTakeUpCustomerMember.setTitle(policyHolderTittle);
        }

        if (!policyHolderFirstName.isEmpty()) {
            policyTakeUpCustomerMember.setFirstname(policyHolderFirstName);
        }

        policyTakeUpCustomerMember.setName(policyHolderName);

        if (!policyHolderSurname.isEmpty()) {
            policyTakeUpCustomerMember.setSurname(policyHolderSurname);
        }

        policyTakeUpCustomerMember.setMiddleName(policyHolderMiddleName);
        policyTakeUpCustomerMember.setBirthdate(policyHolderBirthDate);
        policyTakeUpCustomerMember.setIdType(policyHolderIdType);

        if (policyHolderIdNumber.isEmpty()) {
            policyHolderIdNumber = scenarioTester.generateRandomSaId(policyHolderBirthDate, policyHolderIdType, policyHolderIdNumber, policyHolderGender);
        }

        policyTakeUpCustomerMember.setIdNumber(policyHolderIdNumber);
        policyTakeUpCustomerMember.setGender(policyHolderGender);
        policyTakeUpCustomerMember.setCoverAmount(policyHolderCoverAmount);
        policyTakeUpCustomerMember.setPremium(policyHolderPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addAuthorizedPerson(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        String authorizedPersonRelationship = scenarioTester.getCellValue("Authorized Person Relationship");
        String authorizedPersonName = scenarioTester.getCellValue("Authorized Person Name");
        String authorizedPersonMiddleName = scenarioTester.getCellValue("Authorized Person Middle Name");
        String authorizedPersonBirthDate = scenarioTester.getCellValue("Authorized Person Birth Date");
        String authorizedPersonIdType = scenarioTester.getCellValue("Authorized Person Id Type");
        authorizedPersonIdNumber = scenarioTester.getCellValue("Authorized Person Id Number");
        String authorizedPersonGender = scenarioTester.getCellValue("Authorized Person Gender");
        String authorizedPersonCoverAmount = scenarioTester.getCellValue("Authorized Person Cover Amount");
        String authorizedPersonPremium = scenarioTester.getCellValue("Authorized Person Premium");

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(authorizedPersonRelationship);
        policyTakeUpCustomerMember.setName(authorizedPersonName);
        policyTakeUpCustomerMember.setMiddleName(authorizedPersonMiddleName);
        policyTakeUpCustomerMember.setBirthdate(authorizedPersonBirthDate);
        policyTakeUpCustomerMember.setIdType(authorizedPersonIdType);

        if (authorizedPersonIdNumber.isEmpty()) {
            authorizedPersonIdNumber = scenarioTester.generateRandomSaId(authorizedPersonBirthDate, authorizedPersonIdType, authorizedPersonIdNumber, authorizedPersonGender);
        }

        policyTakeUpCustomerMember.setIdNumber(authorizedPersonIdNumber);
        policyTakeUpCustomerMember.setGender(authorizedPersonGender);
        policyTakeUpCustomerMember.setCoverAmount(authorizedPersonCoverAmount);
        policyTakeUpCustomerMember.setPremium(authorizedPersonPremium);
        policyTakeUpCustomer.getMember().clear();
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);


    }

    private void addBeneficiaryFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        Beneficiary beneficiary = takeupScenario.getBeneficiary();

        if (beneficiary != null) {
            String addBeneficiary = beneficiary.getAdd();
            boolean addBeneficiaryDetails = addBeneficiary.equalsIgnoreCase("Yes");

            if (addBeneficiaryDetails) {
                String beneficiaryRelationship = beneficiary.getRelationship();
                String beneficiaryName = beneficiary.getName();
                String beneficiaryBirthDate = beneficiary.getBirthDate();
                String beneficiaryIdNumber = beneficiary.getIdNumber();
                String beneficiaryGender = beneficiary.getGender();
                String beneficiaryCellNumber = beneficiary.getCellNumber();
                String beneficiaryEmail = beneficiary.getEmail();

                PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
                policyTakeUpCustomerMember.setRelationship(beneficiaryRelationship);
                policyTakeUpCustomerMember.setName(beneficiaryName);
                policyTakeUpCustomerMember.setBirthdate(beneficiaryBirthDate);
                policyTakeUpCustomerMember.setGender(beneficiaryGender);

                if (beneficiaryIdNumber.isEmpty()) {
                    beneficiaryIdNumber = scenarioTester.generateRandomSaId(beneficiaryBirthDate, "rsaid", beneficiaryIdNumber, beneficiaryGender);
                }

                policyTakeUpCustomerMember.setIdNumber(beneficiaryIdNumber);
                policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);

                policyTakeUpCustomer.setBeneficiaryCellNo(beneficiaryCellNumber);
                policyTakeUpCustomer.setBeneficiaryEmail(beneficiaryEmail);
            }
        }
    }

    private void addBeneficiaryFromDataSheet(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        String addBeneficiary = scenarioTester.getCellValue("Add Beneficiary");
        boolean addBeneficiaryDetails = addBeneficiary.equalsIgnoreCase("Yes");

        if (addBeneficiaryDetails) {
            String beneficiaryRelationship = scenarioTester.getCellValue("Beneficiary Relationship");
            String beneficiaryName = scenarioTester.getCellValue("Beneficiary Name");
            String beneficiaryBirthDate = scenarioTester.getCellValue("Beneficiary Birth Date");
            String beneficiaryIdNumber = scenarioTester.getCellValue("Beneficiary Id Number");
            String beneficiaryGender = scenarioTester.getCellValue("Beneficiary Gender");
            String beneficiaryCellNumber = scenarioTester.getCellValue("Beneficiary Cell Number");
            String beneficiaryEmail = scenarioTester.getCellValue("Beneficiary Email");

            PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
            policyTakeUpCustomerMember.setRelationship(beneficiaryRelationship);
            policyTakeUpCustomerMember.setName(beneficiaryName);
            policyTakeUpCustomerMember.setBirthdate(beneficiaryBirthDate);
            policyTakeUpCustomerMember.setGender(beneficiaryGender);

            if (beneficiaryIdNumber.isEmpty()) {
                beneficiaryIdNumber = scenarioTester.generateRandomSaId(beneficiaryBirthDate, "rsaid", beneficiaryIdNumber, beneficiaryGender);
            }

            policyTakeUpCustomerMember.setIdNumber(beneficiaryIdNumber);
            policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);

            policyTakeUpCustomer.setBeneficiaryCellNo(beneficiaryCellNumber);
            policyTakeUpCustomer.setBeneficiaryEmail(beneficiaryEmail);
        }
    }

    private void addPolicyHolderFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        PolicyHolder policyHolder = takeupScenario.getPolicyHolder();
        String policyHolderRelationship = policyHolder.getRelationship();
        String policyHolderTittle = policyHolder.getTittle();
        String policyHolderFirstname = policyHolder.getFirstname();
        String policyHolderName = policyHolder.getName();
        String policyHolderSurname = policyHolder.getSurname();
        String policyHolderMiddleName = policyHolder.getMiddleName();
        String policyHolderBirthDate = policyHolder.getBirthDate();
        String policyHolderIdType = policyHolder.getIdType();
        policyHolderIdNumber = policyHolder.getIdNumber();
        String policyHolderGender = policyHolder.getGender();
        String policyHolderCoverAmount = policyHolder.getCoverAmount();
        String policyHolderPremium = policyHolder.getPremium();

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(policyHolderRelationship);

        if (!policyHolderTittle.isEmpty()) {
            policyTakeUpCustomerMember.setTitle(policyHolderTittle);
        }

        if (!policyHolderFirstname.isEmpty()) {
            policyTakeUpCustomerMember.setFirstname(policyHolderFirstname);
        }

        policyTakeUpCustomerMember.setName(policyHolderName);

        if (!policyHolderSurname.isEmpty()) {
            policyTakeUpCustomerMember.setSurname(policyHolderSurname);
        }

        policyTakeUpCustomerMember.setMiddleName(policyHolderMiddleName);
        policyTakeUpCustomerMember.setBirthdate(policyHolderBirthDate);
        policyTakeUpCustomerMember.setIdType(policyHolderIdType);

        if (policyHolderIdNumber.isEmpty()) {
            policyHolderIdNumber = scenarioTester.generateRandomSaId(policyHolderBirthDate, policyHolderIdType, policyHolderIdNumber, policyHolderGender);
        } else {
            // TODO: Accommodate 20th
            policyHolderBirthDate = getBirthDateFromIdNumber();
            policyTakeUpCustomerMember.setBirthdate(policyHolderBirthDate);
        }

        policyTakeUpCustomerMember.setIdNumber(policyHolderIdNumber);
        policyTakeUpCustomerMember.setGender(policyHolderGender);
        policyTakeUpCustomerMember.setCoverAmount(policyHolderCoverAmount);
        policyTakeUpCustomerMember.setPremium(policyHolderPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addAuthorizedPersonFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        AuthorizedPerson authorizedPerson = takeupScenario.getAuthorizedPerson();
        String authorizedPersonRelationship = authorizedPerson.getRelationship();
        String authorizedPersonName = authorizedPerson.getName();
        String authorizedPersonMiddleName = authorizedPerson.getMiddleName();
        String authorizedPersonBirthDate = authorizedPerson.getBirthDate();
        String authorizedPersonIdType = authorizedPerson.getIdType();
        authorizedPersonIdNumber = authorizedPerson.getIdNumber();
        String authorizedPersonGender = authorizedPerson.getGender();
        String authorizedPersonCoverAmount = authorizedPerson.getCoverAmount();
        String authorizedPersonPremium = authorizedPerson.getPremium();

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(authorizedPersonRelationship);
        policyTakeUpCustomerMember.setName(authorizedPersonName);
        policyTakeUpCustomerMember.setMiddleName(authorizedPersonMiddleName);
        policyTakeUpCustomerMember.setBirthdate(authorizedPersonBirthDate);
        policyTakeUpCustomerMember.setIdType(authorizedPersonIdType);

        if (authorizedPersonIdNumber.isEmpty()) {
            authorizedPersonIdNumber = scenarioTester.generateRandomSaId(authorizedPersonBirthDate, authorizedPersonIdType, authorizedPersonIdNumber, authorizedPersonGender);
        } else {
            // TODO: Accommodate 20th
            authorizedPersonBirthDate = "19" + authorizedPersonIdNumber.substring(0,6);
            policyTakeUpCustomerMember.setBirthdate(authorizedPersonBirthDate);
        }

        policyTakeUpCustomerMember.setIdNumber(authorizedPersonIdNumber);
        policyTakeUpCustomerMember.setGender(authorizedPersonGender);
        policyTakeUpCustomerMember.setCoverAmount(authorizedPersonCoverAmount);
        policyTakeUpCustomerMember.setPremium(authorizedPersonPremium);
        policyTakeUpCustomer.getMember().clear();
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addChild(PolicyTakeUpCustomer policyTakeUpCustomer, MemberVariableFields childMemberVariableFields) throws Exception {
        String childRelationship = childMemberVariableFields.getRelationship(); // getCellValue("Child Relationship");
        String childName = childMemberVariableFields.getName(); // getCellValue("Child Name");
        String childMiddleName = scenarioTester.getCellValue("Child Middle Name");
        String childBirthDate = childMemberVariableFields.getBirthDate(); // getCellValue("Child Birth Date");
        String childIdType = scenarioTester.getCellValue("Child Id Type");
        String childIdNumber = scenarioTester.getCellValue("Child Id Number");
        String childGender = childMemberVariableFields.getGenderCode(); // getCellValue("Child Gender");
        String childCoverAmount = childMemberVariableFields.getCoverAmount(); // getCellValue("Child Cover Amount");
        String childPremium = childMemberVariableFields.getPremium(); // getCellValue("Child Premium");

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(childRelationship);
        policyTakeUpCustomerMember.setName(childName);
        policyTakeUpCustomerMember.setMiddleName(childMiddleName);
        policyTakeUpCustomerMember.setBirthdate(childBirthDate);
        policyTakeUpCustomerMember.setIdType(childIdType);
        policyTakeUpCustomerMember.setIdNumber(childIdNumber);
        policyTakeUpCustomerMember.setGender(childGender);
        policyTakeUpCustomerMember.setCoverAmount(childCoverAmount);
        policyTakeUpCustomerMember.setPremium(childPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addChildFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer, MemberVariableFields childMemberVariableFields) throws Exception {
        Child child = takeupScenario.getChild();
        String childRelationship = childMemberVariableFields.getRelationship();
        String childName = childMemberVariableFields.getName();
        String childMiddleName = child.getMiddleName();
        String childBirthDate =  childMemberVariableFields.getBirthDate();
        String childIdType = child.getIdType();
        String childIdNumber = child.getIdNumber();
        String childGender = childMemberVariableFields.getGenderCode();
        String childCoverAmount = childMemberVariableFields.getCoverAmount();
        String childPremium = childMemberVariableFields.getPremium();

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(childRelationship);
        policyTakeUpCustomerMember.setName(childName);
        policyTakeUpCustomerMember.setMiddleName(childMiddleName);
        policyTakeUpCustomerMember.setBirthdate(childBirthDate);
        policyTakeUpCustomerMember.setIdType(childIdType);
        policyTakeUpCustomerMember.setIdNumber(childIdNumber);
        policyTakeUpCustomerMember.setGender(childGender);
        policyTakeUpCustomerMember.setCoverAmount(childCoverAmount);
        policyTakeUpCustomerMember.setPremium(childPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addSpouse(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        String spouseRelationship = scenarioTester.getCellValue("Spouse Relationship");
        String spouseName = scenarioTester.getCellValue("Spouse Name");
        String spouseMiddleName = scenarioTester.getCellValue("Spouse Middle Name");
        String spouseBirthDate = scenarioTester.getCellValue("Spouse Birth Date");
        String spouseIdType = scenarioTester.getCellValue("Spouse Id Type");
        String spouseIdNumber = scenarioTester.getCellValue("Spouse Id Number");
        String spouseGender = scenarioTester.getCellValue("Spouse Gender");
        String spouseCoverAmount = scenarioTester.getCellValue("Spouse Cover Amount");
        String spousePremium = scenarioTester.getCellValue("Spouse Premium");

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(spouseRelationship);
        policyTakeUpCustomerMember.setName(spouseName);
        policyTakeUpCustomerMember.setMiddleName(spouseMiddleName);
        policyTakeUpCustomerMember.setBirthdate(spouseBirthDate);
        policyTakeUpCustomerMember.setIdType(spouseIdType);
        policyTakeUpCustomerMember.setIdNumber(spouseIdNumber);
        policyTakeUpCustomerMember.setGender(spouseGender);
        policyTakeUpCustomerMember.setCoverAmount(spouseCoverAmount);
        policyTakeUpCustomerMember.setPremium(spousePremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addSpouseFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer) throws Exception {
        Spouse spouse = takeupScenario.getSpouse();
        String spouseRelationship = spouse.getRelationship();
        String spouseName = spouse.getName();
        String spouseMiddleName = spouse.getMiddleName();
        String spouseBirthDate = spouse.getBirthDate();
        String spouseIdType = spouse.getIdType();
        String spouseIdNumber = spouse.getIdNumber();
        String spouseGender = spouse.getGender();
        String spouseCoverAmount = spouse.getCoverAmount();
        String spousePremium = spouse.getPremium();

        // spouseIdNumber = scenarioTester.generateRandomSaId(spouseBirthDate, spouseIdType, spouseIdNumber, spouseGender);

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(spouseRelationship);
        policyTakeUpCustomerMember.setName(spouseName);
        policyTakeUpCustomerMember.setMiddleName(spouseMiddleName);
        policyTakeUpCustomerMember.setBirthdate(spouseBirthDate);
        policyTakeUpCustomerMember.setIdType(spouseIdType);
        policyTakeUpCustomerMember.setIdNumber(spouseIdNumber);
        policyTakeUpCustomerMember.setGender(spouseGender);
        policyTakeUpCustomerMember.setCoverAmount(spouseCoverAmount);
        policyTakeUpCustomerMember.setPremium(spousePremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addParentOrExtendedFamilyMember(PolicyTakeUpCustomer policyTakeUpCustomer, MemberVariableFields parentOrExtendedFamilyMemberVariableFields) throws Exception {
        String parentOrExtendedFamilyRelationship = parentOrExtendedFamilyMemberVariableFields.getRelationship(); // getCellValue("Parent/Extended Family Relationship");
        String parentOrExtendedFamilyName = parentOrExtendedFamilyMemberVariableFields.getName(); // getCellValue("Parent/Extended Family Name");
        String parentOrExtendedFamilyMiddleName = scenarioTester.getCellValue("Parent/Extended Family Middle Name");
        String parentOrExtendedFamilyBirthDate = parentOrExtendedFamilyMemberVariableFields.getBirthDate(); // getCellValue("Parent/Extended Family Birth Date");
        String parentOrExtendedFamilyIdType = scenarioTester.getCellValue("Parent/Extended Family Id Type");
        String parentOrExtendedFamilyIdNumber = scenarioTester.getCellValue("Parent/Extended Family Id Number");
        String parentOrExtendedFamilyGender = parentOrExtendedFamilyMemberVariableFields.getGenderCode(); // getCellValue("Parent/Extended Family Gender");
        String parentOrExtendedFamilyCoverAmount = parentOrExtendedFamilyMemberVariableFields.getCoverAmount(); // getCellValue("Parent/Extended Family Cover Amount");
        String parentOrExtendedFamilyPremium = parentOrExtendedFamilyMemberVariableFields.getPremium(); // getCellValue("Parent/Extended Family Premium");

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(parentOrExtendedFamilyRelationship);
        policyTakeUpCustomerMember.setName(parentOrExtendedFamilyName);
        policyTakeUpCustomerMember.setMiddleName(parentOrExtendedFamilyMiddleName);
        policyTakeUpCustomerMember.setBirthdate(parentOrExtendedFamilyBirthDate);
        policyTakeUpCustomerMember.setIdType(parentOrExtendedFamilyIdType);
        policyTakeUpCustomerMember.setIdNumber(parentOrExtendedFamilyIdNumber);
        policyTakeUpCustomerMember.setGender(parentOrExtendedFamilyGender);
        policyTakeUpCustomerMember.setCoverAmount(parentOrExtendedFamilyCoverAmount);
        policyTakeUpCustomerMember.setPremium(parentOrExtendedFamilyPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void addParentOrExtendedFamilyMemberFromTakeupScenario(PolicyTakeUpCustomer policyTakeUpCustomer, MemberVariableFields parentOrExtendedFamilyMemberVariableFields) throws Exception {
        ParentExtendedFamily parentExtendedFamily = takeupScenario.getParentExtendedFamily();
        String parentOrExtendedFamilyRelationship = parentOrExtendedFamilyMemberVariableFields.getRelationship();
        String parentOrExtendedFamilyName = parentOrExtendedFamilyMemberVariableFields.getName();
        String parentOrExtendedFamilyMiddleName = parentExtendedFamily.getMiddleName();
        String parentOrExtendedFamilyBirthDate = parentOrExtendedFamilyMemberVariableFields.getBirthDate();
        String parentOrExtendedFamilyIdType = parentExtendedFamily.getIdType();
        String parentOrExtendedFamilyIdNumber = parentExtendedFamily.getIdNumber();
        String parentOrExtendedFamilyGender = parentOrExtendedFamilyMemberVariableFields.getGenderCode();
        String parentOrExtendedFamilyCoverAmount = parentOrExtendedFamilyMemberVariableFields.getCoverAmount();
        String parentOrExtendedFamilyPremium = parentOrExtendedFamilyMemberVariableFields.getPremium();

        PolicyTakeUpCustomerMember policyTakeUpCustomerMember = new PolicyTakeUpCustomerMember();
        policyTakeUpCustomerMember.setRelationship(parentOrExtendedFamilyRelationship);
        policyTakeUpCustomerMember.setName(parentOrExtendedFamilyName);
        policyTakeUpCustomerMember.setMiddleName(parentOrExtendedFamilyMiddleName);
        policyTakeUpCustomerMember.setBirthdate(parentOrExtendedFamilyBirthDate);
        policyTakeUpCustomerMember.setIdType(parentOrExtendedFamilyIdType);
        policyTakeUpCustomerMember.setIdNumber(parentOrExtendedFamilyIdNumber);
        policyTakeUpCustomerMember.setGender(parentOrExtendedFamilyGender);
        policyTakeUpCustomerMember.setCoverAmount(parentOrExtendedFamilyCoverAmount);
        policyTakeUpCustomerMember.setPremium(parentOrExtendedFamilyPremium);
        policyTakeUpCustomer.getMember().add(policyTakeUpCustomerMember);
    }

    private void getMemberVariableFields(String[] roleValuesList, List<MemberVariableFields> memberVariableFields) {
        for (String roleData : roleValuesList) {
            if (!roleData.isEmpty()) {
                try {
                    String trim = roleData.replaceAll("\\[", "")
                            .replaceAll("\\]", "")
                            .trim();
                    String[] roleValues = trim.split(",");
                    memberVariableFields.add(
                            new MemberVariableFields(roleValues[0], roleValues[1], roleValues[2], roleValues[3], roleValues[4], roleValues[5])
                    );
                } catch (Exception e) {
                    // TODO: Handle Whatever Exception
                }
            }
        }
    }

    public void setupPolicyTakeUpRequestInputFromTakeupScenario() throws Exception {
        if (takeupScenario != null) {
            // TODO: Setup The PolicyTakeUpRequestInput From TakeupScenario.
            try {
                policyTakeUpRequestInput = new PolicyTakeUpRequestInput();
                PolicyTakeUpCustomer policyTakeUpCustomer = setUpTakeUpCustomerFromTakeupScenario();
                addPolicyHolderFromTakeupScenario(policyTakeUpCustomer);
                addBeneficiaryFromTakeupScenario(policyTakeUpCustomer);

                String requestType = takeupScenario.getRequestType();
                if (requestType.equalsIgnoreCase("LAW_ON_CALL_POLICY_CREATE")) {
                    String productName = takeupScenario.getProductName();
                    if (productName.equalsIgnoreCase("BUSINESS")) {
                        addAuthorizedPersonFromTakeupScenario(policyTakeUpCustomer);
                    }
                }

                Spouse spouse = takeupScenario.getSpouse();
                String spouseAction = spouse.getAction();
                if (spouseAction.equalsIgnoreCase("AD")) {
                    addSpouseFromTakeupScenario(policyTakeUpCustomer);
                }

                String childRoleValues = takeupScenario.getChildRoleIdActions();
                String[] childRoleValuesList = childRoleValues.split("],");
                List<MemberVariableFields> takeUpChildMemberVariableFields = new LinkedList<>();
                getMemberVariableFields(childRoleValuesList, takeUpChildMemberVariableFields);

                for (MemberVariableFields childMemberVariableFields : takeUpChildMemberVariableFields) {
                    addChildFromTakeupScenario(policyTakeUpCustomer, childMemberVariableFields);
                }

                String parentOrExtendedFamilyRoleValues = takeupScenario.getParentExtendedFamilyRoleIdAction();
                String[] parentOrExtendedFamilyRoleValuesList = parentOrExtendedFamilyRoleValues.split("],");
                List<MemberVariableFields> takeUpParentOrExtendedFamilyMemberVariableFields = new LinkedList<>();
                getMemberVariableFields(parentOrExtendedFamilyRoleValuesList, takeUpParentOrExtendedFamilyMemberVariableFields);

                for (MemberVariableFields parentOrExtendedFamilyMemberVariableFields : takeUpParentOrExtendedFamilyMemberVariableFields) {
                    addParentOrExtendedFamilyMemberFromTakeupScenario(policyTakeUpCustomer, parentOrExtendedFamilyMemberVariableFields);
                }

                policyTakeUpRequestInput.setCustomer(policyTakeUpCustomer);
            } catch (Exception e) {
                // TODO: Handle Exceptions
            }
        } else {
            log.error("Error: TakeupScenario is null. Setup the TakeupScenario and retry.");
            throw new Exception("Error: TakeupScenario is null. Setup the TakeupScenario and retry.");
        }
    }

    private String getDateTimeStamp() {
        log.info("Getting Date Time Stamp.");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        return format.format(now);
    }

    private String getDateTimeStamp(String stringFormat) {
        log.info("Getting Date Time Stamp.");
        DateTimeFormatter format = DateTimeFormatter.ofPattern(stringFormat);
        LocalDateTime now = LocalDateTime.now();
        return format.format(now);
    }

    private int getRandomNumber(int max, int min) {
        int range = max - min + 1;

        return (int)(Math.random() * range) + min;
    }
}
