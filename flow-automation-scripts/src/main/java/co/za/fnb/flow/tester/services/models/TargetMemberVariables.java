package co.za.fnb.flow.tester.services.models;

public class TargetMemberVariables {
    private String relationship;
    private String name;
    private String birthDate;
    private String genderCode;

    public TargetMemberVariables() {
    }

    public TargetMemberVariables(String relationship, String name, String birthDate, String genderCode) {
        this.relationship = relationship;
        this.name = name;
        this.birthDate = birthDate;
        this.genderCode = genderCode;
    }

    public String getRelationship() {
        return relationship;
    }

    public void setRelationship(String relationship) {
        this.relationship = relationship;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getGenderCode() {
        return genderCode;
    }

    public void setGenderCode(String genderCode) {
        this.genderCode = genderCode;
    }

}
