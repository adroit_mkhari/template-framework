package co.za.fnb.flow.pages.financial_administration;

import co.za.fnb.flow.pages.financial_administration.page_factory.FinancialAdministrationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.BasePage;

public class FinancialAdministration extends BasePage {

    FinancialAdministrationPageObjects financialAdministrationPageObjects = new FinancialAdministrationPageObjects(driver);

    public FinancialAdministration(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
