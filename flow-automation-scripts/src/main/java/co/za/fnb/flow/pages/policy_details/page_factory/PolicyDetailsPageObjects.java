package co.za.fnb.flow.pages.policy_details.page_factory;

import co.za.fnb.flow.pages.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PolicyDetailsPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:quoteInfoTable:NewMember")
    private WebElement newMember;

    @FindBy(id = "createSearchItemView:mainTabView:quoteInfoTable:Add")
    private WebElement addToPolicy;

    @FindBy(id = "createSearchItemView:mainTabView:quoteInfoTable:Remove")
    private WebElement quoteDelete;

    @FindBy(id = "createSearchItemView:mainTabView:memberInfoTable:Delete")
    private WebElement memberDelete;

    @FindBy(id = "createSearchItemView:mainTabView:save")
    private WebElement save;

    @FindBy(id = "createSearchItemView:mainTabView:CancelPolicy")
    private WebElement cancelPolicy;

    @FindBy(id = "createSearchItemView:mainTabView:RetainPolicy")
    private WebElement retainPolicy;

    @FindBy(id = "createSearchItemView:mainTabView:ReinstatePolicy")
    private WebElement reinstatePolicy;

    @FindBy(id = "createSearchItemView:mainTabView:Quote")
    private WebElement quote;

    @FindBy(id = "createSearchItemView:mainTabView:quotedPremiumAmount")
    private WebElement quotedPremiumAmount;

    @FindBy(id = "createSearchItemView:mainTabView:showPolicyDetailPoliciesDlg")
    private WebElement showPolicyDetailPoliciesDialog;

    @FindBy(id = "createSearchItemView:mainTabView:showPolicyDetailPoliciesDlg_title")
    private WebElement showPolicyDetailPoliciesDialogTitle;

    public By iFrameTagBy = By.tagName("iframe");

    public PolicyDetailsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getNewMember() {
        return newMember;
    }

    public WebElement getAddToPolicy() {
        return addToPolicy;
    }

    public WebElement getQuoteDelete() {
        return quoteDelete;
    }

    public WebElement getMemberDelete() {
        return memberDelete;
    }

    public WebElement getSave() {
        return save;
    }

    public WebElement getCancelPolicy() {
        return cancelPolicy;
    }

    public WebElement getRetainPolicy() {
        return retainPolicy;
    }

    public WebElement getReinstatePolicy() {
        return reinstatePolicy;
    }

    public WebElement getQuote() {
        return quote;
    }

    public WebElement getQuotedPremiumAmount() {
        return quotedPremiumAmount;
    }

    public By getiFrameTagBy() {
        return iFrameTagBy;
    }

    public WebElement getShowPolicyDetailPoliciesDialog() {
        return showPolicyDetailPoliciesDialog;
    }

    public WebElement getShowPolicyDetailPoliciesDialogTitle() {
        return showPolicyDetailPoliciesDialogTitle;
    }
}
