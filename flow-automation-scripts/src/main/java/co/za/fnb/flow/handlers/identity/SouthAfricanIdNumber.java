package co.za.fnb.flow.handlers.identity;

public class SouthAfricanIdNumber {
    private String year;
    private String month;
    private String date;
    private Gender gender;
    private boolean isSouthAfrican;
    private final int ONE_DIGIT_MIN = 0;
    private final int ONE_DIGIT_MAX = 9;
    private final int FEMALE_MIN = 0;
    private final int FEMALE_MAX = 4;
    private final int MALE_MIN = 5;
    private final int MALE_MAX = 9;
    private final String SA_CITIZEN = "0";
    private final String NON_SA_CITIZEN = "1";
    private IdNumberValidatorUtility.IDNumberDetails idNumberDetails;

    public SouthAfricanIdNumber(String year, String month, String date, Gender gender, boolean isSouthAfrican) throws Exception {
        char[] yearChars = year.toCharArray();
        if (yearChars.length == 2) {
            this.year = year;
        } else {
            throw new Exception("Please provide a 2 digit year value. " + year + " is not a allowed");
        }

        char[] monthChars = month.toCharArray();
        if (monthChars.length == 2) {
            this.month = month;
        } else {
            throw new Exception("Please provide a 2 digit month value. " + month + " is not a allowed");
        }

        char[] dateChars = date.toCharArray();
        if (dateChars.length == 2) {
            this.date = date;
        } else {
            throw new Exception("Please provide a 2 digit date value. " + date + " is not a allowed");
        }

        this.gender = gender;
        this.isSouthAfrican = isSouthAfrican;
    }

    private void setIdNumberDetails(IdNumberValidatorUtility.IDNumberDetails idNumberDetails) {
        this.idNumberDetails = idNumberDetails;
    }

    public IdNumberValidatorUtility.IDNumberDetails getIdNumberDetails() {
        return idNumberDetails;
    }

    private IdNumberValidatorUtility.IDNumberDetails getIdNumberDetails(String idNumber) {
        IdNumberValidatorUtility.IDNumberDetails idNumberDetails;
        idNumberDetails = IdNumberValidatorUtility.extractInformation(idNumber);

        if (idNumberDetails.isValid()) {
            System.out.println("=================================================");
            System.out.println("Id Number: " + idNumberDetails.getIdNumber());
            System.out.println("Birth Date: " + idNumberDetails.getBirthDate());
            System.out.println("Citizen: " + idNumberDetails.isCitizen());
            System.out.println("Male: " + idNumberDetails.isMale());
            System.out.println("Valid: " + idNumberDetails.isValid());
            System.out.println("=================================================");
        } else {
            System.out.println("Id Number: " + idNumberDetails.getIdNumber() + " , Valid: " + idNumberDetails.isValid());
        }
        return idNumberDetails;
    }

    private String getStringNumber(int min, int max, int numberOfDigits) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfDigits; i++) {
            int randomNumber = getRandomNumber(max, min);
            stringBuilder.append(randomNumber);
        }
        return stringBuilder.toString();
    }

    private int getRandomNumber(int max, int min) {
        int range = max - min + 1;

        return (int)(Math.random() * range) + min;
    }

    private String computeRandomIdNumber() {
        String birthDate = year + month + date;
        String genderString, citizenship;

        if (gender == Gender.FEMALE) {
            genderString = getStringNumber(FEMALE_MIN, FEMALE_MAX, 1);
        } else {
            genderString = getStringNumber(MALE_MIN, MALE_MAX, 1);
        }

        if (isSouthAfrican) {
            citizenship = SA_CITIZEN;
        } else {
            citizenship = NON_SA_CITIZEN;
        }

        return birthDate + genderString + getStringNumber(ONE_DIGIT_MIN, ONE_DIGIT_MAX, 3) + citizenship
                + getStringNumber(8, 9, 1) + getStringNumber(ONE_DIGIT_MIN, ONE_DIGIT_MAX, 1);
    }

    public void generateIdNumber() throws Exception {
        try {
            String idNumber;
            idNumber = computeRandomIdNumber();
            IdNumberValidatorUtility.IDNumberDetails idNumberDetails = getIdNumberDetails(idNumber);

            int count = 0;
            while (!idNumberDetails.isValid() && count++ < 250) {
                idNumber = computeRandomIdNumber();
                idNumberDetails = getIdNumberDetails(idNumber);
            }
            setIdNumberDetails(idNumberDetails);
        } catch (Exception e) {
            throw new Exception("Error while generating Id Number.\n" + e.getMessage());
        }
    }

}
