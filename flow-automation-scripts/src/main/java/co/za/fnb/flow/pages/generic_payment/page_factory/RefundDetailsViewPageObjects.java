package co.za.fnb.flow.pages.generic_payment.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class RefundDetailsViewPageObjects {

    @FindBy(id = "refundDetailsView:Userref1")
    WebElement userref1;

    @FindBy(id = "refundDetailsView:Userref2")
    WebElement userref2;

    @FindBy(id = "refundDetailsView:Amount")
    WebElement amount;

    @FindBy(id = "refundDetailsView:Payee")
    WebElement payee;

    @FindBy(id = "refundDetailsView:AcountNumber")
    WebElement acountNumber;

    @FindBy(id = "refundDetailsView:BranchCode")
    WebElement branchCode;

    @FindBy(id = "refundDetailsView:AccountType")
    WebElement accountType;

    @FindBy(id = "refundDetailsView:ActionDate")
    WebElement actionDate;

    @FindBy(id = "refundDetailsView:Reason")
    WebElement reason;

    @FindBy(id = "refundDetailsView:approve")
    WebElement approve;

    @FindBy(id = "refundDetailsView:decline")
    WebElement decline;

    @FindBy(id = "refundDetailsView:fail")
    WebElement fail;

    @FindBy(id = "refundDetailsView:qcinfo")
    WebElement qcinfo;

    @FindBy(id = "refundDetailsView:back")
    WebElement back;

    @FindBy(id = "refundDetailsView:yes_approve")
    WebElement yesApprove;

    @FindBy(id = "refundDetailsView:do_not_approve")
    WebElement doNotApprove;

    @FindBy(id = "refundDetailsView:yes_decline")
    WebElement yesDecline;

    @FindBy(id = "refundDetailsView:do_not_decline")
    WebElement doNotDecline;

    public RefundDetailsViewPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getUserref1() {
        return userref1;
    }

    public WebElement getUserref2() {
        return userref2;
    }

    public WebElement getAmount() {
        return amount;
    }

    public WebElement getPayee() {
        return payee;
    }

    public WebElement getAcountNumber() {
        return acountNumber;
    }

    public WebElement getBranchCode() {
        return branchCode;
    }

    public WebElement getAccountType() {
        return accountType;
    }

    public WebElement getActionDate() {
        return actionDate;
    }

    public WebElement getReason() {
        return reason;
    }

    public WebElement getApprove() {
        return approve;
    }

    public WebElement getDecline() {
        return decline;
    }

    public WebElement getFail() {
        return fail;
    }

    public WebElement getQcinfo() {
        return qcinfo;
    }

    public WebElement getBack() {
        return back;
    }

    public WebElement getYesApprove() {
        return yesApprove;
    }

    public WebElement getDoNotApprove() {
        return doNotApprove;
    }

    public WebElement getYesDecline() {
        return yesDecline;
    }

    public WebElement getDoNotDecline() {
        return doNotDecline;
    }
}
