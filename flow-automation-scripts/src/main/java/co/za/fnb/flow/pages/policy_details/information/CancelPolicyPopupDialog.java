package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.policy_details.page_factory.CancelPolicyPopupDialogPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class CancelPolicyPopupDialog extends PolicyDetails {
    private Logger log  = LogManager.getLogger(CancelPolicyPopupDialog.class);
    CancelPolicyPopupDialogPageObjects cancelPolicyPopupDialogPageObjects = new CancelPolicyPopupDialogPageObjects(driver);

    public CancelPolicyPopupDialog(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void getConfirmCancelPolicy() throws Exception {
        try {
            click(cancelPolicyPopupDialogPageObjects.getConfirmCancelPolicy());
        } catch (Exception e) {
            log.error("Error while clicking on confirm Policy.");
            throw new Exception("Error while clicking on confirm Policy.");
        }
    }

    public void selectCancelPolicyReason(String cancelPolicyReason) throws Exception {
        try {
            selectMatchingDataLabel(cancelPolicyPopupDialogPageObjects.getSelectCancelReasonSelect(), cancelPolicyPopupDialogPageObjects.getSelectCancelReasonSelectItems(), cancelPolicyReason);
        } catch (Exception e) {
            log.error("Error while selecting Cancel Policy Reason: " + cancelPolicyReason);
            throw new Exception("Error while selecting Cancel Policy Reason: " + cancelPolicyReason);
        }
    }

    public void writeOtherConfirmCancelReason(String otherConfirmCancelReason) throws Exception {
        try {
            type(cancelPolicyPopupDialogPageObjects.getOtherTextArea(), otherConfirmCancelReason);
        } catch (Exception e) {
            log.error("Error while writing Other Confirm Cancel Reason.");
            throw new Exception("Error while writing Other Confirm Cancel Reason.");
        }
    }

    public void cancelEnter() throws Exception {
        try {
            click(cancelPolicyPopupDialogPageObjects.getCancelEnter());
        } catch (Exception e) {
            log.error("Error while clicking on cancel enter.");
            throw new Exception("Error while clicking on cancel enter.");
        }
    }

    public void writeReason(String voiceRecordingReason) throws Exception {
        try {
            type(cancelPolicyPopupDialogPageObjects.getReasonTextArea(), voiceRecordingReason);
        } catch (Exception e) {
            log.error("Error while writing Reason.");
            throw new Exception("Error while writing Reason.");
        }
    }

    public void auditTrailEnter() throws Exception {
        try {
            click(cancelPolicyPopupDialogPageObjects.getAuditTrailEnter());
        } catch (Exception e) {
            log.error("Error while clicking on audit trail enter.");
            throw new Exception("Error while clicking on audit trail enter.");
        }
    }
}
