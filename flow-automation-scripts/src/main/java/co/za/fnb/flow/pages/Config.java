package co.za.fnb.flow.pages;

/**
 * This is an interface that will initialize all the needed constants to be used by the framework.
 */
public interface Config {
    String browser = "chrome";
    String targetApplicationPre = "http://lfe-rbflpreap01.fnb.co.za:8080/flow";
    String targetApplicationTst = "http://lfe-rbflintap01.fnb.co.za:8080/flow";
    String username = "F7890125";
    String password = "password";
    String dataSourceType = "excel";

    // Windows:
    /*String dataSourcePath = "src\\test\\resources\\data";
    String reportSourcePath = "src\\test\\resources\\reporting";
    String propertiesFile = "src\\main\\resources\\config.properties";
    String mainResourcesFolder = "src\\main\\resources";
    String driverDownloadPath = "src\\test\\resources\\downloads";
    String templateLettersPath = "src\\test\\resources\\data\\letters\\pdf";*/

    // Linux:
    String dataSourcePath = "src/test/resources/data";
    String reportSourcePath = "src/test/resources/reporting";
    String propertiesFile = "src/main/resources/config.properties";
    String mainResourcesFolder = "src/main/resources";
    String driverDownloadPath = "src/test/resources/downloads";
    String templateLettersPath = "src/test/resources/data/letters/pdf";
}
