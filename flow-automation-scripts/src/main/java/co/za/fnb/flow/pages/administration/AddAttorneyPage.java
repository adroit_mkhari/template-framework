package co.za.fnb.flow.pages.administration;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.administration.page_factory.AddAttorneyPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

public class AddAttorneyPage extends BasePage {
    private Logger log  = LogManager.getLogger(AddAttorneyPage.class);
    AddAttorneyPageObjects addAttorneyPageObjects = new AddAttorneyPageObjects(driver, scenarioOperator);
    Actions actions = new Actions(driver);

    public AddAttorneyPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void doubleClickOnCompanyName() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getCompanyName());
        } catch (Exception e) {
            log.error("Error while double clicking on Company Name.");
            throw new Exception("Error while double clicking on Company Name.");
        }
    }

    public void updateComapanyName(String companyName) throws Exception {
        try {
            type(addAttorneyPageObjects.getCompanyName(), companyName);
        } catch (Exception e) {
            log.error("Error while updating Company Name.");
            throw new Exception("Error while updating Company Name.");
        }
    }

    public void clickVatFlag() throws Exception {
        try {
            moveToElementAndClick(addAttorneyPageObjects.getVatFlag());
        } catch (Exception e) {
            log.error("Error while double clicking on VAT Flag.");
            throw new Exception("Error while double clicking on VAT Flag.");
        }
    }

    public void doubleClickOnVatRegNo() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getVatRegNo());
        } catch (Exception e) {
            log.error("Error while double clicking on VAT Reg. No.");
            throw new Exception("Error while double clicking on VAT Reg. No.");
        }
    }

    public void updateVatRegNo(String vatRegNo) throws Exception {
        try {
            type(addAttorneyPageObjects.getVatRegNo(), vatRegNo);
        } catch (Exception e) {
            log.error("Error while updating VAT Reg. No.");
            throw new Exception("Error while updating VAT Reg. No.");
        }
    }

    public void doubleClickOnEmail() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getEmail());
        } catch (Exception e) {
            log.error("Error while double clicking on VAT Reg. No.");
            throw new Exception("Error while double clicking on VAT Reg. No.");
        }
    }

    public void updateEmail(String email) throws Exception {
        try {
            type(addAttorneyPageObjects.getEmail(), email);
        } catch (Exception e) {
            log.error("Error while updating email.");
            throw new Exception("Error while updating email.");
        }
    }

    public void doubleClickOnCellNo() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getCellNo());
        } catch (Exception e) {
            log.error("Error while double clicking on Cell No.");
            throw new Exception("Error while double clicking on Cell No.");
        }
    }

    public void updateCellNo(String email) throws Exception {
        try {
            type(addAttorneyPageObjects.getCellNo(), email);
        } catch (Exception e) {
            log.error("Error while updating Cell No.");
            throw new Exception("Error while updating Cell No.");
        }
    }

    public void doubleClickOnWorkNo() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getWorkNo());
        } catch (Exception e) {
            log.error("Error while double clicking on Work No.");
            throw new Exception("Error while double clicking on Work No.");
        }
    }

    public void updateWorkNo(String workNo) throws Exception {
        try {
            type(addAttorneyPageObjects.getWorkNo(), workNo);
        } catch (Exception e) {
            log.error("Error while updating Work No.");
            throw new Exception("Error while updating Work No.");
        }
    }

    public void selectAttorneyType(String attorneyType) throws Exception {
        try {
            selectOnDashboard(addAttorneyPageObjects.getAttorneyTypeLabel(), addAttorneyPageObjects.getAttorneyTypeItems(), attorneyType);
        } catch (Exception e) {
            log.error("Error while selecting Attorney Type.");
            throw new Exception("Error while selecting Attorney Type.");
        }
    }

    public void selectAddressType(String addressType) throws Exception {
        try {
            selectOnDashboard(addAttorneyPageObjects.getAddressTypeLabel(), addAttorneyPageObjects.getAddressTypeItems(), addressType);
        } catch (Exception e) {
            log.error("Error while selecting Address Type.");
            throw new Exception("Error while selecting Address Type.");
        }
    }

    public void doubleClickOnAddressLineOne() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getAddressLineOne());
        } catch (Exception e) {
            log.error("Error while double clicking on Address Line One.");
            throw new Exception("Error while double clicking on Address Line One.");
        }
    }

    public void updateAddressLineOne(String addressLineOne) throws Exception {
        try {
            type(addAttorneyPageObjects.getAddressLineOne(), addressLineOne);
        } catch (Exception e) {
            log.error("Error while updating Address Line One.");
            throw new Exception("Error while updating Address Line One.");
        }
    }

    public void doubleClickOnAddressLineTwo() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getAddressLineTwo());
        } catch (Exception e) {
            log.error("Error while double clicking on Address Line Two.");
            throw new Exception("Error while double clicking on Address Line Two.");
        }
    }

    public void updateAddressLineTwo(String addressLineTwo) throws Exception {
        try {
            type(addAttorneyPageObjects.getAddressLineTwo(), addressLineTwo);
        } catch (Exception e) {
            log.error("Error while updating Address Line Two.");
            throw new Exception("Error while updating Address Line Two.");
        }
    }

    public void doubleClickOnSuburb() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getSuburb());
        } catch (Exception e) {
            log.error("Error while double clicking on Suburb.");
            throw new Exception("Error while double clicking on Suburb.");
        }
    }

    public void updateSuburb(String suburb) throws Exception {
        try {
            type(addAttorneyPageObjects.getSuburb(), suburb);
        } catch (Exception e) {
            log.error("Error while updating Suburb.");
            throw new Exception("Error while updating Suburb.");
        }
    }

    public void selectBankName(String bankName) throws Exception {
        try {
            selectOnDashboard(addAttorneyPageObjects.getBankNameLabel(), addAttorneyPageObjects.getBankNameItems(), bankName);
        } catch (Exception e) {
            log.error("Error while selecting Bank Name.");
            throw new Exception("Error while selecting Bank Name.");
        }
    }

    public void doubleClickOnBranchCode() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getBranchCode());
        } catch (Exception e) {
            log.error("Error while double clicking on Branch Code.");
            throw new Exception("Error while double clicking on Branch Code.");
        }
    }

    public void updateBranchCode(String branchCode) throws Exception {
        try {
            doubleClickOnBranchCode();
            type(addAttorneyPageObjects.getBranchCode(), branchCode);
        } catch (Exception e) {
            log.error("Error while updating Branch Code.");
            throw new Exception("Error while updating Branch Code.");
        }
    }

    public void selectAccountType(String accountType) throws Exception {
        try {
            selectOnDashboard(addAttorneyPageObjects.getAccountTypeLabel(), addAttorneyPageObjects.getAccountTypeItems(), accountType);
        } catch (Exception e) {
            log.error("Error while selecting Account Type.");
            throw new Exception("Error while selecting Account Type.");
        }
    }

    public void doubleClickOnAccountNumber() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getAccountNumber());
        } catch (Exception e) {
            log.error("Error while double clicking on Branch Code.");
            throw new Exception("Error while double clicking on Branch Code.");
        }
    }

    public void updateAccountNumber(String accountNumber) throws Exception {
        try {
            doubleClickOnAccountNumber();
            type(addAttorneyPageObjects.getAccountNumber(), accountNumber);
        } catch (Exception e) {
            log.error("Error while updating Account Number.");
            throw new Exception("Error while updating Account Number.");
        }
    }

    public void doubleClickOnAccountHolder() throws Exception {
        try {
            moveToElementAndDoubleClick(addAttorneyPageObjects.getAccountHolder());
        } catch (Exception e) {
            log.error("Error while double clicking on Branch Code.");
            throw new Exception("Error while double clicking on Branch Code.");
        }
    }

    public void updateAccountHolder(String accountHolder) throws Exception {
        try {
            type(addAttorneyPageObjects.getAccountHolder(), accountHolder);
        } catch (Exception e) {
            log.error("Error while updating Account Holder.");
            throw new Exception("Error while updating Account Holder.");
        }
    }

    public void selectProvince(String province) throws Exception {
        try {
            selectOnDashboard(addAttorneyPageObjects.getProvinceLabel(), addAttorneyPageObjects.getProvinceItems(), province);
        } catch (Exception e) {
            log.error("Error while selecting Province.");
            throw new Exception("Error while selecting Province.");
        }
    }

    public void clickCity() throws Exception {
        try {
            moveToElementAndClick(addAttorneyPageObjects.getCity());
        } catch (Exception e) {
            log.error("Error while clicking on City.");
            throw new Exception("Error while clicking on City.");
        }
    }

    public void clickAllCities() throws Exception {
        try {
            moveToElementAndClick(addAttorneyPageObjects.getAllCities());
        } catch (Exception e) {
            log.error("Error while clicking on All Cities.");
            throw new Exception("Error while clicking on All Cities.");
        }
    }

    public void clickFirstCity() throws Exception {
        try {
            moveToElementAndClick(addAttorneyPageObjects.getFirstCity());
        } catch (Exception e) {
            log.error("Error while clicking on First City.");
            throw new Exception("Error while clicking on First City.");
        }
    }

    public void clickAdd() throws Exception {
        try {
            moveToElementAndClick(addAttorneyPageObjects.getAdd());
        } catch (Exception e) {
            log.error("Error while clicking on Add.");
            throw new Exception("Error while clicking on Add.");
        }
    }

    public void clickSubmit() throws Exception {
        try {
            moveToElementAndClickJsExec(addAttorneyPageObjects.getAddUserSubmit());
        } catch (Exception e) {
            log.error("Error while trying to click on Add User Submit.");
            throw new Exception("Error while trying to click on Add User Submit.");
        }
    }

    public void clickCancel() throws Exception {
        try {
            moveToElementAndClickJsExec(addAttorneyPageObjects.getAddUserCancel());
        } catch (Exception e) {
            log.error("Error while trying to click on Add User Cancel.");
            throw new Exception("Error while trying to click on Add User Cancel.");
        }
    }

}
