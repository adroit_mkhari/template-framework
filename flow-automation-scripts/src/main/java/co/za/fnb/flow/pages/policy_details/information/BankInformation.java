package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.page_factory.BankInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;

public class BankInformation extends PolicyDetails {

    BankInformationPageObjects bankInformationPageObjects = new BankInformationPageObjects(driver);

    public BankInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public String getDebitOrderDate() throws Exception {
        try {
            return getAttribute(bankInformationPageObjects.getDebitOrderDate(), "value");
        } catch (Exception e) {
            throw new Exception("Error while getting Debit Order date.");
        }
    }

    public String getNextDueDate() throws Exception {
        try {
            return getAttribute(bankInformationPageObjects.getNextDueDate(), "value");
        } catch (Exception e) {
            throw new Exception("Error while getting Next Due date.");
        }
    }

    public void changeBankName(String option) throws Exception {
        try {
            selectOnDashboard(bankInformationPageObjects.getBankNameSelect(), bankInformationPageObjects.getBankNameSelectItems(), option);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception("Error while changing Bank Name.");
        }
    }

    public void changeDebitOrderDate(String inputText) throws Exception {
        try {
            clear(bankInformationPageObjects.getDebitOrderDate());
            type(bankInformationPageObjects.getDebitOrderDate(), inputText);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception("Error while changing Debit Order Date.");
        }
    }

    public void changeNextDueDate(String inputText) throws Exception {
        try {
            clear(bankInformationPageObjects.getNextDueDate());
            type(bankInformationPageObjects.getNextDueDate(), inputText);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception("Error while changing Next Debit Order Date.");
        }
    }
}
