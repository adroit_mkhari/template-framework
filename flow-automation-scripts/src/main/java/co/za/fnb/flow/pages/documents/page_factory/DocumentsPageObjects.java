package co.za.fnb.flow.pages.documents.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class DocumentsPageObjects {

    @FindBy(id = "some locator")
    WebElement elementName;

    public DocumentsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
