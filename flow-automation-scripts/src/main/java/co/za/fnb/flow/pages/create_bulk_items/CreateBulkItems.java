package co.za.fnb.flow.pages.create_bulk_items;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.create_bulk_items.page_factory.CreateBulkItemsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class CreateBulkItems extends BasePage {
    private Logger log  = LogManager.getLogger(CreateBulkItems.class);
    CreateBulkItemsPageObjects createBulkItemsPageObjects = new CreateBulkItemsPageObjects(driver);

    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public CreateBulkItems(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void choose(String filePath) throws Exception {
        try {
            log.info("Choosing File: " + filePath);
            WebElement choose = createBulkItemsPageObjects.getChoose();
            choose.sendKeys(filePath);
        } catch (Exception e) {
            log.error("Error while trying to choose file: " + e.getMessage());
            throw new Exception("Error while trying to choose file: " + e.getMessage());
        }
    }

    public void upload() throws Exception {
        try {
            log.info("Uploading File.");
            WebElement upload = createBulkItemsPageObjects.getUpload();
            click(upload);
        } catch (Exception e) {
            log.error("Error while uploading file: " + e.getMessage());
            throw new Exception("Error while uploading file: " + e.getMessage());
        }
    }

    public void cancel() throws Exception {
        try {
            log.info("Cancel Uploading File.");
            WebElement cancel = createBulkItemsPageObjects.getCancel();
            click(cancel);
        } catch (Exception e) {
            log.error("Error while canceling uploading file: " + e.getMessage());
            throw new Exception("Error while canceling uploading file: " + e.getMessage());
        }
    }

    public String getFile() throws Exception {
        try {
            log.info("Getting File To Be Uploaded.");
            WebElement file = createBulkItemsPageObjects.getFile();
            String text = getText(file);
            return text;
        } catch (Exception e) {
            log.error("Error while Getting File To Be Uploaded.: " + e.getMessage());
            throw new Exception("Error while Getting File To Be Uploaded.: " + e.getMessage());
        }
    }

    public String getSummary() throws Exception {
        try {
            log.info("Getting Summary.");
            WebElement summary = createBulkItemsPageObjects.getSummary();
            String text = getText(summary);
            return text;
        } catch (Exception e) {
            log.error("Error while getting summary: " + e.getMessage());
            throw new Exception("Error while getting summary: " + e.getMessage());
        }
    }

    public String getFileUploaded() throws Exception {
        try {
            log.info("Getting File Uploaded.");
            WebElement fileUploaded = createBulkItemsPageObjects.getFileUploaded();
            String text = getText(fileUploaded);
            return text;
        } catch (Exception e) {
            log.error("Error while getting File Uploaded: " + e.getMessage());
            throw new Exception("Error while getting File Uploaded: " + e.getMessage());
        }
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}
