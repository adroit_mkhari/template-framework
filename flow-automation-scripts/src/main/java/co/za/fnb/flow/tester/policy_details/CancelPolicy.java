package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class CancelPolicy {
    private Logger log  = LogManager.getLogger(CancelPolicy.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    CancelPolicyPopupDialog cancelPolicyPopupDialog;
    BeneficiaryInformation beneficiaryInformation;
    MemberInformation memberInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public CancelPolicy(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        cancelPolicyPopupDialog = new CancelPolicyPopupDialog(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void cancel(String confirmCancelReason, String otherConfirmCancelReason, String voiceRecordingReason) throws Exception {
        log.info("Updating Beneficial and Banking Details for Cancel Member.");
        String bankName = member.getBankName();
        String debitOrderDate = member.getDebitOrderDate();
        String nextDueDate = member.getNextDueDate();

        if (!bankName.isEmpty()) {
            bankInformation.changeBankName(bankName);
        }

        if (!debitOrderDate.isEmpty()) {
            bankInformation.changeDebitOrderDate(debitOrderDate);
        }

        if (!nextDueDate.isEmpty()) {
            bankInformation.changeNextDueDate(nextDueDate);
        }
        log.info("Done Updating Beneficial and Banking Details for Cancel Member.");

        // region Cancel Policy Logic:
        log.debug("Handling Cancel Policy Logic");
        policyInformation.cancelPolicy();
        Thread.sleep(1000);
        cancelPolicyPopupDialog.getConfirmCancelPolicy();
        Thread.sleep(2000);
        driver.switchTo().frame(0);

        try {
            cancelPolicyPopupDialog.selectCancelPolicyReason(confirmCancelReason);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while trying to select: " + confirmCancelReason);
            throw new Exception("Error while trying to select: " + confirmCancelReason);
        }

        cancelPolicyPopupDialog.writeOtherConfirmCancelReason(otherConfirmCancelReason);
        cancelPolicyPopupDialog.cancelEnter();
        Thread.sleep(1000);

        // TODO: To check if there we are expecting any response
        policyInformation.getResponses(true);
        Thread.sleep(500);

        if (confirmCancelReason.equalsIgnoreCase("FRAUDULENT SALE") || confirmCancelReason.equalsIgnoreCase("DISPUTED SALE") ) {
            log.debug("FRAUDULENT or DISPUTED SALE");
            driver.switchTo().defaultContent();
            Thread.sleep(500);
            driver.switchTo().frame(1);
            Thread.sleep(500);
            cancelPolicyPopupDialog.writeReason(voiceRecordingReason);
            cancelPolicyPopupDialog.auditTrailEnter();
            Thread.sleep(1000);
            policyInformation.getResponses(true);
        }
        log.debug("Policy Cancellation Done");
        // endregion
    }

    private Double getPolicyMembersTotalPremium() throws Exception {
        Double totalPremium = 0.00;
        String premium;
        Double premiumValue;
        try {
            int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

            for(int j = 0; j < numberOfExistingMembers ; j++) {
                premium = memberInformation.getPolicyDetailsPremium(j);
                premium = getDoubleString(premium);
                premiumValue = stringToDouble(premium);
                totalPremium += premiumValue;
            }
            return totalPremium;
        } catch (Exception e) {
            log.error("Error while getting policy members total premium.");
            throw new Exception("Error while getting policy members total premium.");
        }
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    public void save() throws Exception {
        try {
            log.info("Saving Cancel Member.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {member.getPopupWorkTypeZero(), member.getPopupWorkTypeOne()};
            String[] popUpStatus = {member.getPopupStatusZero(), member.getPopupStatusOne()};
            String[] popUpQueue = {member.getPopupQueueZero(), member.getPopupQueueOne()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.proceedAfterClickingSave(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.proceedAfterClickingSave(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() throws Exception {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}