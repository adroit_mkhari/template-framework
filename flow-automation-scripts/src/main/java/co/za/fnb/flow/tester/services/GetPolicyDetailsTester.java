package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.GetPolicyDetailRequestInput;
import generated.GetPolicyDetailResponseOutput;
import generated.ObjectFactory;
import generated.ResponseErrors;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class GetPolicyDetailsTester {
    private Logger log  = LogManager.getLogger(GetPolicyDetailsTester.class);
    private GetPolicyDetailRequestInput getPolicyDetailRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private GetPolicyDetailResponseOutput getPolicyDetailResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public GetPolicyDetailsTester(GetPolicyDetailRequestInput getPolicyDetailRequestInput) {
        this.getPolicyDetailRequestInput = getPolicyDetailRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(GetPolicyDetailRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(getPolicyDetailRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(GetPolicyDetailResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public GetPolicyDetailRequestInput getGetPolicyDetailRequestInput() {
        return getPolicyDetailRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public GetPolicyDetailResponseOutput getGetPolicyDetailResponseOutput() {
        return getPolicyDetailResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public GetPolicyDetailResponseOutput sendGetPolicyDetailsRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvGETPOLICYDETAILInput gensrvGETPOLICYDETAILInput = serviceObjectFactory.createGensrvGETPOLICYDETAILInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(500);
        pxmlin.setString(marshalResult);
        gensrvGETPOLICYDETAILInput.setPXMLIN(pxmlin);
        GensrvGETPOLICYDETAILResult gensrvGETPOLICYDETAILResult = gsd00030PRServicesPort.gensrvGetpolicydetail(gensrvGETPOLICYDETAILInput);
        PXMLOUT getPolicyDetailResultPXMLOUT = gensrvGETPOLICYDETAILResult.getPXMLOUT();
        String pXmlOutString = getPolicyDetailResultPXMLOUT.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, GetPolicyDetailResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             getPolicyDetailResponseOutput = (GetPolicyDetailResponseOutput) unMarshal;
             return getPolicyDetailResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting GetPolicyDetailResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
