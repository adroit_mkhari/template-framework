package co.za.fnb.flow.pages.administration;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.administration.page_factory.ApproveOrRejectAttorneyPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

public class ApproveOrRejectAttorneyPage extends BasePage {
    private Logger log  = LogManager.getLogger(ApproveOrRejectAttorneyPage.class);
    ApproveOrRejectAttorneyPageObjects approveOrRejectAttorneyPageObjects
            = new ApproveOrRejectAttorneyPageObjects(driver, scenarioOperator);
    Actions actions = new Actions(driver);

    public ApproveOrRejectAttorneyPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clickApprove() throws Exception {
        try {
            moveToElementAndClickJsExec(approveOrRejectAttorneyPageObjects.getApproveUser());
        } catch (Exception e) {
            log.error("Error while trying to click on Approve.");
            throw new Exception("Error while trying to click on Approve.");
        }
    }

    public void clickFail() throws Exception {
        try {
            moveToElementAndClickJsExec(approveOrRejectAttorneyPageObjects.getFailUser());
        } catch (Exception e) {
            log.error("Error while trying to click on Fail.");
            throw new Exception("Error while trying to click on Fail.");
        }
    }

    public void clickYesApprove() throws Exception {
        try {
            moveToElementAndClickJsExec(approveOrRejectAttorneyPageObjects.getYesApprove());
        } catch (Exception e) {
            log.error("Error while trying to click on Yes Approve.");
            throw new Exception("Error while trying to click on Yes Approve.");
        }
    }

    public void clickNoDoNotApprove() throws Exception {
        try {
            moveToElementAndClickJsExec(approveOrRejectAttorneyPageObjects.getNoDoNotApprove());
        } catch (Exception e) {
            log.error("Error while trying to click on No Do Not Approve.");
            throw new Exception("Error while trying to click on No Do Not Approve.");
        }
    }

}
