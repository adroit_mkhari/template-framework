package co.za.fnb.flow.pages.generic_payment.page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GenericPaymentPageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:refundListTable_data")
    private WebElement refundListTableData;

    private String policyNumber = "createSearchItemView:mainTabView:refundListTable:0:policyNumber";
    private String status = "createSearchItemView:mainTabView:refundListTable:0:status";
    private String payee = "createSearchItemView:mainTabView:refundListTable:0:payee";
    private String amount = "createSearchItemView:mainTabView:refundListTable:0:amount";

    public GenericPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getRefundListTableData() {
        return refundListTableData;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getStatus() {
        return status;
    }

    public String getPayee() {
        return payee;
    }

    public String getAmount() {
        return amount;
    }
}
