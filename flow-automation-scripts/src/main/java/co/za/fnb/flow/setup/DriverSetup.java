package co.za.fnb.flow.setup;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import sun.awt.OSInfo;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.*;
import java.util.concurrent.TimeUnit;

import static co.za.fnb.flow.pages.Config.driverDownloadPath;

/**
 * This is a Browser Automator Helper.
 * It contains a set of common functions that we can possibly want to perform while writing test automation scripts
 * @properties is a properties file that the framework requires.
 * @BROWSER_NAME is the name of the preferred browser to use.
 * @CHROME_BROWSER_SWITCHES is the switches settings for chrome driver.
 */
public class DriverSetup {
    private Logger log  = LogManager.getLogger(DriverSetup.class);
    private final OSInfo.OSType OPERATING_SYSTEM = OSInfo.getOSType();
    private WebDriver driver;
    private DesiredCapabilities desiredCapabilities;
    private String browserDriver;
    Properties properties;

    public DriverSetup() {
        log.info("Creating DriverSetup instance.");
        log.info("OPERATING SYSTEM: " + OPERATING_SYSTEM);
    }

    public DriverSetup(Properties properties) {
        this();
        this.properties = properties;
    }

    public DriverSetup(String browserDriver) {
        this();
        this.browserDriver = browserDriver;
    }

    public DriverSetup(DesiredCapabilities desiredCapabilities, String browserDriver) {
        this();
        this.desiredCapabilities = desiredCapabilities;
        this.browserDriver = browserDriver;
    }

    public DriverSetup(DesiredCapabilities desiredCapabilities, String browserDriver, Properties properties) {
        this();
        this.desiredCapabilities = desiredCapabilities;
        this.browserDriver = browserDriver;
        this.properties = properties;
    }

    public Properties getProperties() {
        log.info("Getting Properties.");
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public WebDriver getDriver() {
        log.info("Getting Driver.");
        return driver;
    }

    public DesiredCapabilities getDesiredCapabilities() {
        log.info("Getting Desired Capabilities.");
        return desiredCapabilities;
    }

    public void setDesiredCapabilities(DesiredCapabilities desiredCapabilities) {
        this.desiredCapabilities = desiredCapabilities;
    }

    public String getBrowserDriver() {
        log.info("Getting Browser Driver.");
        return browserDriver;
    }

    public void setBrowserDriver(String browserDriver) {
        this.browserDriver = browserDriver;
    }

    /**
     * This will explicitely set the browser capabilities for the browser driver to be used.
     */
    public void setBrowserCapabilities() {
        log.info("Setting Browser Capabilities.");
        String browserName = properties.getProperty("BROWSER_NAME").trim();
        String operatingSystemName = OPERATING_SYSTEM.name();
        String switches = properties.getProperty("CHROME_BROWSER_SWITCHES");
        String[] switchList = switches.split(",");
        String downloadPath = properties.getProperty("DRIVER_DOWNLOAD_DIRECTORY");
        String driverPath = properties.getProperty("DRIVER_PATH");
        if (downloadPath == null) {
            downloadPath = driverDownloadPath;
        }

        browserName = browserDriver == null ? browserName : browserDriver;

        if (browserName.toLowerCase().contains("chrome")) {
            log.info("Configuring Chrome Driver Capabilities.");
            if (operatingSystemName.contains("LIN")) {
                log.info("Setting Linux System Property: ");
                Path chromeDriverPath = Paths.get(driverPath);
                boolean exists = Files.exists(chromeDriverPath);
                if (exists) {
                    log.error("Path Exists: " + chromeDriverPath);
                } else {
                    log.error("Path Does Not Exists: " + chromeDriverPath);
                }
                System.setProperty("webdriver.chrome.driver", driverPath);
            } else {
                log.info("Setting Windows System Property: ");
                System.setProperty("webdriver.chrome.driver", "src/main/resources/drivers/chromedriver.exe");
            }

            log.info("Setting Browser Desired Capabilities: ");
            log.info("Desired Capabilities: " + desiredCapabilities);
            desiredCapabilities = desiredCapabilities != null ? desiredCapabilities : new DesiredCapabilities() ;
            log.info("Desired Capabilities: " + desiredCapabilities);
            log.info("Setting Chrome Options: ");
            ChromeOptions options = new ChromeOptions();

            HashMap<String, Object> chromePreferences = new HashMap<>();
            options.setCapability("chrome.switches", Arrays.asList(switchList));
            options.addArguments("test-type");
            chromePreferences.put("profile.default_content_settings.popups", 0);

            Path path = Paths.get(downloadPath);
            log.info("Download Path: " + path);
            boolean pathExists = Files.exists(path);
            if (pathExists) {
                log.info("Download Path Exists.");
                Path absolutePath = path.toAbsolutePath();
                String absolutePathString = absolutePath.toString();
                chromePreferences.put("download.default_directory", absolutePathString);
            } else {
                log.info("Download Path Does Not Exists.");
                Path absolutePath = path.toAbsolutePath();
                String absolutePathString = absolutePath.toString();
                try {
                    log.info("Creating Download Directory.");
                    Files.createDirectories(absolutePath);
                    chromePreferences.put("download.default_directory", absolutePathString);
                } catch (IOException e) {
                    log.error("Error while creating download directories: \n" + e.getMessage());
                    e.printStackTrace();
                }
            }

            if (operatingSystemName.contains("LIN")) {
                options.addArguments("--headless");
                options.addArguments("--no-sandbox");
                options.addArguments("window-size=1920,1080");
            }
            options.setExperimentalOption("prefs", chromePreferences);

            log.info("Creating A New Chrome Driver Instance.");
            try {
                if (operatingSystemName.contains("LIN")) {
                    ChromeWebDriverVersionManager chromeWebDriverVersionManager = new ChromeWebDriverVersionManager();
                    String defaultBrowserVersion = chromeWebDriverVersionManager.getDefaultBrowserVersion();
                    log.debug("Default Browser Version: " + defaultBrowserVersion);
                    WebDriverManager.chromedriver().clearCache();
                    WebDriverManager.chromedriver().version("85").forceDownload().setup();
                    driver = new ChromeDriver(options);
                } else {
                    ChromeWebDriverVersionManager chromeWebDriverVersionManager = new ChromeWebDriverVersionManager();
                    String defaultBrowserVersion = chromeWebDriverVersionManager.getDefaultBrowserVersion();
                    WebDriverManager.chromedriver().version(defaultBrowserVersion).setup();
                    driver = new ChromeDriver(options);
                }
            } catch (Exception e) {
                log.error("Error while trying to create A New Chrome Instance. \n" + e.getMessage());
            }
            log.info("Chrome Driver Instance: " + driver);

        } else if (browserName.toLowerCase().contains("internet")) {
            log.info("Configuring Internet Explorer Driver Capabilities.");
            System.setProperty("webdriver.ie.driver", "src/main/resources/drivers/IEDriverServer.exe");
            new DesiredCapabilities();
            desiredCapabilities = desiredCapabilities != null ? desiredCapabilities : DesiredCapabilities.internetExplorer() ;
            desiredCapabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
            driver = new InternetExplorerDriver();
        } else if (browserName.toLowerCase().contains("firefox")) {
            log.info("Configuring FireFox Driver Capabilities.");
            // System.setProperty("webdriver.firefox.marionette","src/main/resources/drivers/geckodriver.exe");
            // new DesiredCapabilities();
            // desiredCapabilities = desiredCapabilities != null ? desiredCapabilities : DesiredCapabilities.firefox();
            WebDriverManager.firefoxdriver().setup();
            driver = new FirefoxDriver();
        } else if (browserName.toLowerCase().contains("phantom")) {
            log.info("Configuring Phantom Driver Capabilities.");
            File phantomjs = new File("src/main/resources/drivers/phantomjs.exe");
            new DesiredCapabilities();
            desiredCapabilities = desiredCapabilities != null ? desiredCapabilities : DesiredCapabilities.iphone();
            desiredCapabilities.setCapability(PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY, phantomjs.getAbsolutePath());
            driver = new PhantomJSDriver(desiredCapabilities);
        }
    }

    /**
     * This will launch the browser given a target application url.
     * @param targetApplicationUrl The url for the target application.
     */
    public void launchBrowser(String targetApplicationUrl) {
        log.info("Launching Browser. Target Application: " + targetApplicationUrl);
        driver.get(targetApplicationUrl);
        driver.manage().timeouts().implicitlyWait(10L, TimeUnit.SECONDS);
    }
}