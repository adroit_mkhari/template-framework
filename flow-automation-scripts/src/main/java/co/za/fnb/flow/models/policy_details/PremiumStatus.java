package co.za.fnb.flow.models.policy_details;

public class PremiumStatus extends PolicyDetailsBase {
    private String premiumStatus;

    public PremiumStatus(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public PremiumStatus(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String premiumStatus) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.premiumStatus = premiumStatus;
    }

    public String getPremiumStatus() {
        return premiumStatus;
    }

    @Override
    public String toString() {
        return "PremiumStatus{" +
                "premiumStatus='" + premiumStatus + '\'' +
                '}';
    }
}
