package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.HomePageObjects;
import co.za.fnb.flow.pages.root_page_factory.LoginPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class LoginPage extends BasePage {
    private Logger log  = LogManager.getLogger(LoginPage.class);
    LoginPageObjects loginPageObjects = new LoginPageObjects(driver);
    HomePageObjects homePageObjects = new HomePageObjects(driver);

    public LoginPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void open(String targetApplicationLoginPageUrl) throws Exception {
        log.info("Opening Target Application: " + targetApplicationLoginPageUrl);
        try {
            visit(targetApplicationLoginPageUrl);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 60);
            webDriverWait.until(ExpectedConditions.visibilityOf(loginPageObjects.getUsername()));
        } catch (Exception e) {
            log.error("Error while Starting Application on: " + targetApplicationLoginPageUrl + "\t -> " + e.getMessage());
            e.printStackTrace();
            System.exit(-1);
        }
    }

    public HomePage login(String username, String password) throws Exception {
        log.info("Logging user in.");
        try {
            type(loginPageObjects.getUsername(), username);
            type(loginPageObjects.getPassword(), password);
            click(loginPageObjects.getLoginButton());
            WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
            webDriverWait.until(ExpectedConditions.visibilityOf(homePageObjects.getPersonalQueue()));
            log.info("User successfully logged in.");
            Thread.sleep(3000);
            return new HomePage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while logging user in.");
            e.printStackTrace();
            driver.quit();
            System.exit(-1);
            throw new Exception("Error while trying to login.");
        }
    }
}
