package co.za.fnb.flow.pages.policy_details.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CancelPolicyPopupDialogPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:yes_cancelPolicy")
    private WebElement confirmCancelPolicy;

    @FindBy(id = "cancelPolicyView:selectCancelReason_label")
    private WebElement selectCancelReasonSelect;

    @FindBy(id = "cancelPolicyView:selectCancelReason_items")
    private WebElement selectCancelReasonSelectItems;

    @FindBy(id = "cancelPolicyView:otherTextArea")
    private WebElement otherTextArea;

    @FindBy(id = "cancelPolicyView:Enter")
    private WebElement cancelEnter;

    @FindBy(id = "auditTrailDetailView:reasonTextArea")
    private WebElement reasonTextArea;

    @FindBy(id = "auditTrailDetailView:Enter")
    private WebElement auditTrailEnter;

    public CancelPolicyPopupDialogPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getConfirmCancelPolicy() {
        return confirmCancelPolicy;
    }

    public WebElement getSelectCancelReasonSelect() {
        return selectCancelReasonSelect;
    }

    public WebElement getSelectCancelReasonSelectItems() {
        return selectCancelReasonSelectItems;
    }

    public WebElement getOtherTextArea() {
        return otherTextArea;
    }

    public WebElement getCancelEnter() {
        return cancelEnter;
    }

    public WebElement getReasonTextArea() {
        return reasonTextArea;
    }

    public WebElement getAuditTrailEnter() {
        return auditTrailEnter;
    }
}
