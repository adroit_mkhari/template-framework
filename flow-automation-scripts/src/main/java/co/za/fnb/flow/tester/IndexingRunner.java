package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.Query;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.index_items.IndexDocuments;
import co.za.fnb.flow.pages.index_items.IndexItems;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.services.functions.Takeup;
import co.za.fnb.flow.tester.services.models.TakeupScenario;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import generated.PolicyTakeUpResponse;
import generated.PolicyTakeUpResponsePayload;
import generated.PolicyTakeUpResponsePolicyData;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class IndexingRunner {
    private Logger log  = LogManager.getLogger(IndexingRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;
    HomePage homePage;
    PolicyDetails policyDetails;
    CreateSearchItemPage createSearchItemPage;

    public IndexingRunner() {
    }

    public IndexingRunner(WebDriver driver) {
        this.driver = driver;
    }

    public IndexingRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public IndexingRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        if (!scenarioOperator.getProductName().toUpperCase().contains("LOC")) {
            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber, takeupFirst, takeupCode;

        if (Arrays.asList(headers).contains("Test Case No")) {
            for (Row row : dataTable.getGherkinRows()) {
                if (row.getLine() != 1) {
                    dataEntry = row.getCells();
                    run = getCellValue(headers, dataEntry,"Run");
                    if (run.equalsIgnoreCase("YES")) {
                        testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                        scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                        testFunction = getCellValue(headers, dataEntry,"Function");
                        productName = getCellValue(headers, dataEntry,"Product Name");
                        scenarioOperator.setTestCaseNumber(testCaseNumber);
                        scenarioOperator.setScenarioDescription(scenarioDescription);
                        scenarioOperator.setFunction(testFunction);
                        scenarioOperator.setProductName(productName);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                        scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                        // BasePage.setScenarioOperator(scenarioOperator);

                        try {
                            takeupFirst = getCellValue(headers, dataEntry,"Takeup");
                            takeupCode = getCellValue(headers, dataEntry,"Takeup Code");
                            policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                            policyNumberQuery = getCellValue(headers, dataEntry,"Policy Number Query");
                            policyCode = getCellValue(headers, dataEntry,"Policy No");

                            if (takeupFirst.equalsIgnoreCase("YES") && !takeupCode.isEmpty()) {
                                TakeupScenario takeupScenario = TakeupScenarioFactory.getTakeupScenario(takeupCode);
                                ScenarioTester scenarioTester = new ScenarioTester(TestComponent.LETTERS, dataTable, driver);
                                Takeup takeup = new Takeup(scenarioTester, takeupScenario);
                                try {
                                    takeup.setupPolicyTakeUpRequestInputFromTakeupScenario();
                                    PolicyTakeUpResponse policyTakeUpResponse = takeup.sendRequest();
                                    PolicyTakeUpResponsePayload response = policyTakeUpResponse.getResponse();
                                    PolicyTakeUpResponsePolicyData policyData = response.getPolicyData();
                                    policyNumber = policyData.getPolicyNo();
                                    log.info("Successfully Created Policy");
                                    log.info("------------------------------");
                                    log.info("Policy Number: " + policyNumber);
                                    log.info("------------------------------");

                                    // TODO: Check Policy Status Or Just Instantly Change It To Active
                                    queryHandler.setDbPolicyNumber(policyNumber);
                                    policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                    scenarioOperator.setPolicyNumber(policyNumber);
                                } catch (Exception e) {
                                    log.error("Error while taking up policy. " + e.getMessage());
                                    throw new Exception("Error while taking up policy. " + e.getMessage());
                                }
                            } else {
                                if (policyNumber.isEmpty()) {
                                    try {
                                        if (policyNumberQuery.isEmpty()) {
                                            if (policyCode.isEmpty()) {
                                                policyNumber = "No Policy Number";
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            } else {
                                                Query query = QueryFactory.getQuery(policyCode);
                                                ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(query.getQuery());
                                                if (policyNumberQueryResults.next()) {
                                                    policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                } else {
                                                    policyCode = query.getPolicyCode();
                                                    policyNumberQuery = query.getQuery();
                                                    policyNumber = null;
                                                }

                                                if (policyNumber == null) {
                                                    throw new Exception("Error getting policy number for policy code: " + policyCode);
                                                }
                                                queryHandler.setDbPolicyNumber(policyNumber);
                                                policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            }
                                        } else {
                                            ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(policyNumberQuery);
                                            if (policyNumberQueryResults.next()) {
                                                policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                queryHandler.setDbPolicyNumber(policyNumber);
                                                policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                scenarioOperator.setPolicyNumber(policyNumber);
                                            }
                                        }
                                    } catch (Exception e) {
                                        throw new Exception("Error getting policy number. Policy Code: " + policyCode + " Query: " + policyNumberQuery);
                                    }
                                } else {
                                    scenarioOperator.setPolicyNumber(policyNumber);
                                    queryHandler.setDbPolicyNumber(policyNumber);
                                }
                            }

                            log.info("===========================================================================");
                            log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            log.info("---------------------------------------------------------------------------");

                            if (testFunction.equalsIgnoreCase("Indexing")) {
                                indexing(reportableFields, scenarioOperator, headers, dataEntry);
                            }
                            log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            e.printStackTrace();
                            reportFailure(reportableFields, scenarioOperator, e.getMessage());
                            scenarioOperator.increamentReportRowIndex();
                        } finally {
                            scenarioOperator.increamentReportRowIndex();

                            try {
                                int length = driver.getWindowHandles().toArray().length;
                                log.debug("Number Of Open Windows: " + length);
                                if (homePage == null) {
                                    homePage = new HomePage(driver, scenarioOperator);
                                }

                                int windowIndex = length;
                                for (int i = 1; i < length; i++) {
                                    try {
                                        homePage.closeOpenWindow(--windowIndex);
                                    } catch (Exception e) {
                                        log.debug("Error Closing Window. " + e.getMessage());
                                    }
                                }
                            } catch (Exception e) {
                                log.debug("Error Closing Window.");
                            }
                        }
                    }
                }
            }
        }

        log.info("Saving Report.");
        scenarioOperator.saveReport();
        log.info("===========================================================================");
    }

    private void indexing(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Indexing Logic");
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;

        String policyNumber = scenarioOperator.getPolicyNumber();
        String emailAddress = getCellValue(headers, dataEntry, "Email Address");
        String subject = getCellValue(headers, dataEntry, "Subject");
        String fromDate = getCellValue(headers, dataEntry, "From Date");
        String toDate = getCellValue(headers, dataEntry, "To Date");

        String documentName = getCellValue(headers, dataEntry, "Document Name");
        String documentStatus = getCellValue(headers, dataEntry, "Document Status");
        String workType = getCellValue(headers, dataEntry, "Work Type");
        String workQueue = getCellValue(headers, dataEntry, "Work Queue");

        String addItemExpectedMessage = getCellValue(headers, dataEntry, "Add Item Expected Message");
        String indexExpectedMessage = getCellValue(headers, dataEntry, "Index Expected Message");

        String idNumber = getCellValue(headers, dataEntry, "Id Number");

        scenarioOperator.setPolicyNumber(policyNumber);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        try {
            homePage = new HomePage(driver, scenarioOperator);
            createSearchItemPage = homePage.goToIndexWork();

            IndexItems indexItems = new IndexItems(driver, scenarioOperator);
            indexItems.clickClear();

            if (!emailAddress.isEmpty()) {
                indexItems.inputEmailAddress(emailAddress);
            }

            if (!subject.isEmpty()) {
                indexItems.inputSubject(subject);
            }

            if (!fromDate.isEmpty()) {
                indexItems.inputFromDate(fromDate);
            }

            if (!toDate.isEmpty()) {
                indexItems.inputToDate(toDate);
            }

            indexItems.clickFilter();

            int itemsToIndexList = indexItems.getItemsToIndexList();

            String emailItemsTableDataNoItems = "";
            try {
                emailItemsTableDataNoItems = indexItems.getEmailItemsTableDataNoItems();
            } catch (Exception e) {
                log.debug("Error while getting getEmailItemsTableDataNoItems. " + e.getMessage());
            }

            boolean isSelected = false;
            if (!emailItemsTableDataNoItems.equalsIgnoreCase("No items are currently available to index")) {
                String indexDocumentEmailAddress, indexDocumentSubject;
                for (int i = 0; i < itemsToIndexList; i++) {
                    indexDocumentEmailAddress = indexItems.getIndexDocumentEmailAddress(i);
                    indexDocumentSubject = indexItems.getIndexDocumentSubject(i);

                    log.debug("emailAddress: " + emailAddress + " -> indexDocumentEmailAddress:" + indexDocumentEmailAddress);
                    log.debug("subject: " + subject + " -> indexDocumentSubject:" + indexDocumentSubject);
                    if (indexDocumentEmailAddress.equalsIgnoreCase(emailAddress)
                            && indexDocumentSubject.equalsIgnoreCase(subject)) {
                        indexItems.doubleClickIndexDocumentHash(i);
                        isSelected = true;
                        break;
                    }
                }
            }

            if (isSelected) {
                IndexDocuments indexDocuments = indexItems.switchToIndexDocumentsWindow();
                indexDocuments.inputPolicyNumber(policyNumber);
                indexDocuments.clickSearch();
                Thread.sleep(4000);
                String indexDocumentsIdNumber = indexDocuments.getIdNumber();

                String validateIdNumber = getCellValue(headers, dataEntry, "Validate Id Number");
                if (validateIdNumber.equalsIgnoreCase("YES")) {
                    if (!indexDocumentsIdNumber.equalsIgnoreCase(idNumber)) {
                        log.error("Expected id Number Not Correct. Expected: " + idNumber + " -> Found: " + indexDocumentsIdNumber);
                        throw new Exception("Expected id Number Not Correct. Expected: " + idNumber + " -> Found: " + indexDocumentsIdNumber);
                    }
                }

                indexDocuments.clickSelectDocType();
                indexDocuments.inputSelectDocTypeFilter(documentName);
                indexDocuments.selectDocType(documentName);

                indexDocuments.clickSelectDocStatus();
                indexDocuments.inputSelectDocStatusFilter(documentStatus);
                indexDocuments.selectDocStatus(documentStatus);

                indexDocuments.clickSelectWorkTypes();
                indexDocuments.inputSelectWorkTypesFilter(workType);
                indexDocuments.selectWorkTypes(workType);

                indexDocuments.clickSelectQueues();
                indexDocuments.inputSelectQueuesFilter(workQueue);
                indexDocuments.selectQueues(workQueue);

                indexDocuments.inputItemPolicyNumber(policyNumber);

                indexDocuments.clickDocumentCheckBoxesAll();

                indexDocuments.clickAddItem();
                Thread.sleep(2000);
                indexDocuments.setExpectedResults(addItemExpectedMessage);
                indexDocuments.getResponses(true);

                indexDocuments.clickIndex();
                Thread.sleep(5000);
                indexDocuments.setExpectedResults(indexExpectedMessage);
                indexDocuments.getResponses(true);

                indexDocuments.closeIndexDocumentsWindow();

                indexDocuments.switchBackToDashboard();

                homePage = new HomePage(driver, scenarioOperator);
                createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

                createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), false);

                WorkItems workItems = new WorkItems(driver, scenarioOperator);
                int workItemTableSize = workItems.getWorkItemTableSize();
                workItems.getWorkItems(workItemTableSize);
                String[] createdDates = workItems.getCreatedDate();
                String[] itemsWorkTypes = workItems.getWorkType();
                String[] statuses = workItems.getStatus();

                String expectedWorkItemType = getCellValue(headers, dataEntry, "Expected Work Item Type");
                String expectedWorkItemStatus = getCellValue(headers, dataEntry, "Expected Work Item Status");

                String createdDate, itemsWorkType, status;
                boolean found = false;
                for (int i = 0; i < workItemTableSize; i++) {
                    createdDate = createdDates[i];

                    String[] timestampString = createdDate.split(" ");
                    String dateString = timestampString[0];
                    String[] splitDate = dateString.split("-");
                    String year = splitDate[0];
                    String month = splitDate[1];
                    String day = splitDate[2];

                    // TODO: Move This To A Reusable Method
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDateTime now = LocalDateTime.now();
                    String systemTime = format.format(now);
                    String[] splitSystemTime = systemTime.split("-");
                    String systemTimeYear = splitSystemTime[0];
                    String systemTimeMonth = splitSystemTime[1];
                    String systemTimeDay = splitSystemTime[2];

                    if (year.equalsIgnoreCase(systemTimeYear)
                        && month.equalsIgnoreCase(systemTimeMonth)
                        && day.equalsIgnoreCase(systemTimeDay)) {
                        itemsWorkType = itemsWorkTypes[i];
                        status = statuses[i];

                        if (itemsWorkType.equalsIgnoreCase(expectedWorkItemType)
                            && status.equalsIgnoreCase(expectedWorkItemStatus)) {
                            workItems.doubleClickWorkItem(i);
                            found = true;
                            break;
                        }
                    }
                }

                if (found) {
                    String expectedDocumentName = getCellValue(headers, dataEntry, "Expected Document Name");
                    String expectedDocumentStatus = getCellValue(headers, dataEntry, "Expected Document Status");

                    String firstWorkItemDocumentsUploadedDate = workItems.getFirstWorkItemDocumentsUploadedDate();
                    String[] timestampString = firstWorkItemDocumentsUploadedDate.split(" ");
                    String dateString = timestampString[0];
                    String[] splitDate = dateString.split("-");
                    String year = splitDate[0];
                    String month = splitDate[1];
                    String day = splitDate[2];

                    // TODO: Move This To A Reusable Method
                    DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                    LocalDateTime now = LocalDateTime.now();
                    String systemTime = format.format(now);
                    String[] splitSystemTime = systemTime.split("-");
                    String systemTimeYear = splitSystemTime[0];
                    String systemTimeMonth = splitSystemTime[1];
                    String systemTimeDay = splitSystemTime[2];

                    if (year.equalsIgnoreCase(systemTimeYear)
                        && month.equalsIgnoreCase(systemTimeMonth)
                        && day.equalsIgnoreCase(systemTimeDay)) {
                        String firstWorkItemDocumentName = workItems.getFirstWorkItemDocumentName();
                        String firstWorkItemDocumentStatus = workItems.getFirstWorkItemDocumentStatus();

                        if (firstWorkItemDocumentName.equalsIgnoreCase(expectedDocumentName)
                            && firstWorkItemDocumentStatus.equalsIgnoreCase(expectedDocumentStatus)) {
                            workItems.doubleClickFirstWorkItemDocumentsUploadedDate();
                            Thread.sleep(3000);

                            int length = driver.getWindowHandles().toArray().length;
                            log.debug("Number Of Open Windows: " + length);
                            if (length == 3) {
                                indexDocuments.closeIndexDocumentsOpenWindow(2);
                                Thread.sleep(3000);
                                indexDocuments.closeIndexDocumentsOpenWindow(1);
                            }

                            // If It Gets Here With No Errors Then We Must Report A Success Case:
                            scenarioOperator.setResult("Pass");
                            reportSuccess(reportableFields, scenarioOperator);
                        }
                    } else {
                        log.error("Document Upload Date Does Not Match Today's Date. Expected: " + systemTime + " -> Found: " + dateString);
                        throw new Exception("Document Upload Date Does Not Match Today's Date. Expected: " + systemTime + " -> Found: " + dateString);
                    }
                } else {
                    log.error("No matching work item found.");
                    throw new Exception("No matching work item found.");
                }

            } else {
                log.error("No Index Item Matched Filters.");
                throw new Exception("No Index Item Matched Filters.");
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Indexing Logic");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
