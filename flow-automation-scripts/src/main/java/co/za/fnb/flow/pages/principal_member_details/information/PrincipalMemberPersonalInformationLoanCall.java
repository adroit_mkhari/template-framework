package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberPersonalInformationLoanCallPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPersonalInformationLoanCall extends PrincipalMemberPersonalInformation {
    private Logger log  = LogManager.getLogger(PrincipalMemberPersonalInformationLoanCall.class);
    PrincipalMemberPersonalInformationLoanCallPageObjects principalMemberPersonalInformationLoanCallPageObjects =
            new PrincipalMemberPersonalInformationLoanCallPageObjects(driver, scenarioOperator);

    public PrincipalMemberPersonalInformationLoanCall(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clearIdNumberCompany() throws Exception {
        try {
            clear(principalMemberPersonalInformationLoanCallPageObjects.getIdNumberCompany());
        } catch (Exception e) {
            log.error("Error while clearing Id Number Company.");
            throw new Exception("Error while clearing Id Number Company.");
        }
    }

    public void updateIdNumberCompany(String idNumberCompany) throws Exception {
        try {
            type(principalMemberPersonalInformationLoanCallPageObjects.getIdNumberCompany(), idNumberCompany);
        } catch (Exception e) {
            log.error("Error while clearing Id Number Company.");
            throw new Exception("Error while clearing Id Number Company.");
        }
    }

    public void clearVatNumber() throws Exception {
        try {
            clear(principalMemberPersonalInformationLoanCallPageObjects.getVatNumber());
        } catch (Exception e) {
            log.error("Error while clearing Vat Number.");
            throw new Exception("Error while clearing Vat Number.");
        }
    }

    public void updateVatNumber(String vatNumber) throws Exception {
        try {
            type(principalMemberPersonalInformationLoanCallPageObjects.getVatNumber(), vatNumber);
        } catch (Exception e) {
            log.error("Error while clearing Vat Number.");
            throw new Exception("Error while clearing Vat Number.");
        }
    }

    public String getFNumber() throws Exception {
        try {
            String fNumber = getAttribute(principalMemberPersonalInformationLoanCallPageObjects.getFnumber(), "value");
            return fNumber;
        } catch (Exception e) {
            log.error("Error while getting FNumber.");
            throw new Exception("Error while getting FNumber.");
        }
    }

    public void clearFNumber() throws Exception {
        try {
            clear(principalMemberPersonalInformationLoanCallPageObjects.getFnumber());
        } catch (Exception e) {
            log.error("Error while clearing FNumber.");
            throw new Exception("Error while clearing FNumber.");
        }
    }

    public void updateFNumber(String FNumber) throws Exception {
        try {
            type(principalMemberPersonalInformationLoanCallPageObjects.getFnumber(), FNumber);
        } catch (Exception e) {
            log.error("Error while clearing FNumber.");
            throw new Exception("Error while clearing FNumber.");
        }
    }

    public void selectGenderLoc(String gender) throws Exception {
        try {
            selectUsingDataLabel(principalMemberPersonalInformationLoanCallPageObjects.getGenderSelect(),
                                 principalMemberPersonalInformationLoanCallPageObjects.getGenderSelectItems(),
                                 gender);
        } catch (Exception e) {
            log.error("Error while selecting Gender.");
            throw new Exception("Error while selecting Gender.");
        }
    }
}
