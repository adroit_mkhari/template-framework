package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberPostalInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPostalInformation extends PrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(PrincipalMemberPostalInformation.class);
    PrincipalMemberPostalInformationPageObjects principalMemberPostalInformationPageObjects
            = new PrincipalMemberPostalInformationPageObjects(driver, scenarioOperator);

    public PrincipalMemberPostalInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clearPostalLineOne() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getPostalLine_1());
        } catch (Exception e) {
            log.error("Error while clearing Postal Line One.");
            throw new Exception("Error while clearing Postal Line One.");
        }
    }

    public void updatePostalLineOne(String postalLineOne) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getPostalLine_1(), postalLineOne);
        } catch (Exception e) {
            log.error("Error while updating Postal Line One.");
            throw new Exception("Error while updating Postal Line One.");
        }
    }

    public void clearPostalLineTwo() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getPostalLine_2());
        } catch (Exception e) {
            log.error("Error while clearing Postal Line Two.");
            throw new Exception("Error while clearing Postal Line Two.");
        }
    }

    public void updatePostalLineTwo(String postalLineTwo) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getPostalLine_2(), postalLineTwo);
        } catch (Exception e) {
            log.error("Error while updating Postal Line Two.");
            throw new Exception("Error while updating Postal Line Two.");
        }
    }

    public void clearPostalSuburb() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getPostalSuburb());
        } catch (Exception e) {
            log.error("Error while clearing Postal Suburb.");
            throw new Exception("Error while clearing Postal Suburb.");
        }
    }

    public void updatePostalSuburb(String postalSuburb) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getPostalSuburb(), postalSuburb);
        } catch (Exception e) {
            log.error("Error while updating Postal Suburb.");
            throw new Exception("Error while updating Postal Suburb.");
        }
    }

    public void clearPostalCity() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getPostalCity());
        } catch (Exception e) {
            log.error("Error while clearing Postal City.");
            throw new Exception("Error while clearing Postal City.");
        }
    }

    public void updatePostalCity(String postalSuburb) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getPostalCity(), postalSuburb);
        } catch (Exception e) {
            log.error("Error while updating Postal City.");
            throw new Exception("Error while updating Postal City.");
        }
    }

    public void clearPostalCode() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getPostalCode());
        } catch (Exception e) {
            log.error("Error while clearing Postal Code.");
            throw new Exception("Error while clearing Postal Code.");
        }
    }

    public void updatePostalCode(String postalCode) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getPostalCode(), postalCode);
        } catch (Exception e) {
            log.error("Error while updating Postal Code.");
            throw new Exception("Error while updating Postal Code.");
        }
    }

    public void clickSearchPostalAddress() throws Exception {
        try {
            click(principalMemberPostalInformationPageObjects.getSearchPostalAddress());
        } catch (Exception e) {
            log.error("Error while clicking on Search Postal Address.");
            throw new Exception("Error while clicking on Search Postal Address.");
        }
    }

    public void clearCisPostalAddressLineOne() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalAddressLine_1());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal Address Line One.");
            throw new Exception("Error while clearing Cis Postal Address Line One.");
        }
    }

    public void updateCisPostalAddressLineOne(String cisPostalAddressLineOne) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalAddressLine_1(), cisPostalAddressLineOne);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal Address Line One.");
            throw new Exception("Error while updating Cis Postal Address Line One.");
        }
    }

    public void clearCisPostalAddressLineTwo() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalAddressLine_2());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal Address Line Two.");
            throw new Exception("Error while clearing Cis Postal Address Line Two.");
        }
    }

    public void updateCisPostalAddressLineTwo(String cisPostalAddressLineTwo) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalAddressLine_2(), cisPostalAddressLineTwo);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal Address Line Two.");
            throw new Exception("Error while updating Cis Postal Address Line Two.");
        }
    }

    public void clearCisPostalCity() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalCity());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal City.");
            throw new Exception("Error while clearing Cis Postal City.");
        }
    }

    public void updateCisPostalCity(String cisPostalCity) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalCity(), cisPostalCity);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal City.");
            throw new Exception("Error while updating Cis Postal City.");
        }
    }

    public void clearCisPostalCode() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalCode());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal Code.");
            throw new Exception("Error while clearing Cis Postal Code.");
        }
    }

    public void updateCisPostalCode(String cisPostalCode) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalCode(), cisPostalCode);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal Code.");
            throw new Exception("Error while updating Cis Postal Code.");
        }
    }

    public void clickCisSearchPostalAddress() throws Exception {
        try {
            click(principalMemberPostalInformationPageObjects.getCisSearchPostalAddress());
        } catch (Exception e) {
            log.error("Error while clicking on Cis Search Postal Address.");
            throw new Exception("Error while clicking on Cis Search Postal Address.");
        }
    }

    public void clearCisPostalState() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalState());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal State.");
            throw new Exception("Error while clearing Cis Postal State.");
        }
    }

    public void updateCisPostalState(String cisPostalState) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalState(), cisPostalState);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal State.");
            throw new Exception("Error while updating Cis Postal State.");
        }
    }

    public void clearCisPostalCountry() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisPostalCountry());
        } catch (Exception e) {
            log.error("Error while clearing Cis Postal Country.");
            throw new Exception("Error while clearing Cis Postal Country.");
        }
    }

    public void updateCisPostalCountry(String cisPostalCountry) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisPostalCountry(), cisPostalCountry);
        } catch (Exception e) {
            log.error("Error while updating Cis Postal Country.");
            throw new Exception("Error while updating Cis Postal Country.");
        }
    }

    public void clearCisZipCode() throws Exception {
        try {
            clear(principalMemberPostalInformationPageObjects.getCisZipCode());
        } catch (Exception e) {
            log.error("Error while clearing Cis Zip Code.");
            throw new Exception("Error while clearing Cis Zip Code.");
        }
    }

    public void updateCisZipCode(String cisZipCode) throws Exception {
        try {
            type(principalMemberPostalInformationPageObjects.getCisZipCode(), cisZipCode);
        } catch (Exception e) {
            log.error("Error while updating Cis Zip Code.");
            throw new Exception("Error while updating Cis Zip Code.");
        }
    }

    public void clickCisSearchCisZipCode() throws Exception {
        try {
            click(principalMemberPostalInformationPageObjects.getCisSearchCisZipCode());
        } catch (Exception e) {
            log.error("Error while clicking on Cis Search Cis Zip Code.");
            throw new Exception("Error while clicking on Cis Search Cis Zip Code.");
        }
    }

    public String getPostalCode() throws Exception {
        try {
            String postalCode = getAttribute(principalMemberPostalInformationPageObjects.getPostalCode(), "value");
            return postalCode;
        } catch (Exception e) {
            log.error("Error while getting Postal Code.");
            throw new Exception("Error while getting Postal Code.");
        }
    }

}
