package co.za.fnb.flow.handlers.identity;

public enum Gender {
    MALE,
    FEMALE
}
