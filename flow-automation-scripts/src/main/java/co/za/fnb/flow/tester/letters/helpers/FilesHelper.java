package co.za.fnb.flow.tester.letters.helpers;

import com.testautomationguru.utility.CompareMode;
import com.testautomationguru.utility.PDFUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.io.RandomAccessRead;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;

import java.io.File;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.attribute.FileTime;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import static java.nio.file.Files.*;

public class FilesHelper {
    private Logger log  = LogManager.getLogger(FilesHelper.class);
    private String url;
    private Path path;

    public FilesHelper() {
    }

    public FilesHelper(String url) {
        this.url = url;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public boolean checkExistence() throws Exception {
        try {
            if (url != null) {
                path = Paths.get(url);
                return Files.exists(path);
            } else {
                throw new Exception("Url is " + null);
            }
        } catch (Exception e) {
            log.error("Error while checking existence: " + e.getMessage());
            throw new Exception("Error while checking existence: " + e.getMessage());
        }
    }

    public DirectoryStream<Path> getDirectoryStream() throws Exception {
        try {
            if (url != null) {
                if (path == null) {
                    path = Paths.get(url);
                }
                return newDirectoryStream(path);
            } else {
                throw new Exception("Url is " + null);
            }
        } catch (Exception e) {
            log.error("Error while getting DirectoryStream: " + e.getMessage());
            throw new Exception("Error while getting DirectoryStream: " + e.getMessage());
        }
    }

    public Path getTargetFile(String targetFileName) throws Exception {
        try {
            DirectoryStream<Path> directoryStream = getDirectoryStream();
            for (Path path : directoryStream) {
                Path fileName = path.getFileName();
                String fileNameString = fileName.toString();
                if (fileNameString.equals(targetFileName)) {
                    return path;
                }
            }
        } catch (Exception e) {
            log.error("Error while getting Target File: " + e.getMessage());
            throw new Exception("Error while getting Target File: " + e.getMessage());
        }
        return null;
    }

    public FileTime getTargetFileLastModified(String targetFileName) throws Exception {
        try {
            DirectoryStream<Path> directoryStream = getDirectoryStream();
            for (Path path : directoryStream) {
                Path fileName = path.getFileName();
                String fileNameString = fileName.toString();
                if (fileNameString.equals(targetFileName)) {
                    return Files.getLastModifiedTime(path);
                }
            }
        } catch (Exception e) {
            log.error("Error while getting Target File Last Modified Time: " + e.getMessage());
            throw new Exception("Error while getting Target File Last Modified Time: " + e.getMessage());
        }
        return null;
    }

    public List<String> getFileLineContent(String targetFileName) throws Exception {
        try {
            Path targetFilePath = getTargetFile(targetFileName);
            if (targetFilePath != null) {
                return readAllLines(targetFilePath);
            }
        } catch (Exception e) {
            log.error("Error while getting File Content: " + e.getMessage());
            throw new Exception("Error while getting File Content: " + e.getMessage());
        }
        return null;
    }

    public byte[] getFileByteContent(String targetFileName) throws Exception {
        try {
            Path targetFilePath = getTargetFile(targetFileName);
            if (targetFilePath != null) {
                return readAllBytes(targetFilePath);
            }
        } catch (Exception e) {
            log.error("Error while getting File Byte Content: " + e.getMessage());
            throw new Exception("Error while getting File Byte Content: " + e.getMessage());
        }
        return null;
    }

    public String getPDFtext() throws Exception {
        try {
            if (path != null) {
                File file = path.toFile();
                RandomAccessRead randomAccessRead = new RandomAccessFile(file, "r");
                String pdfTextStripperOutputTwo = null;
                try {
                    PDFParser pdfParser = new PDFParser(randomAccessRead);
                    pdfParser.parse();
                    PDDocument pdfDocument = pdfParser.getPDDocument();

                    PDFTextStripper pdfTextStripper = new PDFTextStripper();
                    pdfTextStripperOutputTwo = pdfTextStripper.getText(pdfDocument);
                    log.info("Pdf Text: " + pdfTextStripperOutputTwo);
                    return pdfTextStripperOutputTwo;
                } catch (Exception e) {
                    log.error("Error while reading Pdf document.");
                    throw new Exception("Error while reading Pdf document.");
                }
            }
        } catch (Exception e) {
            log.error("Error while getting PDF Content.");
            throw new Exception("Error while getting PDF Content.");
        }
        return null;
    }

    public boolean compareTexts(Path fileOne, Path fileTwo, boolean compareTokens) throws Exception {
        try {
            java.io.RandomAccessFile randomAccessFileOne = new java.io.RandomAccessFile(fileOne.toFile(), "r");
            FileChannel fileChannelOne = randomAccessFileOne.getChannel();

            java.io.RandomAccessFile randomAccessFileTwo = new java.io.RandomAccessFile(fileTwo.toFile(), "r");
            FileChannel fileChannelTwo = randomAccessFileTwo.getChannel();

            boolean contentsIdentical = true;
            if ((fileChannelOne.size() == fileChannelTwo.size()) && compareTokens) {
                long fileChannelOneSize = fileChannelOne.size();
                MappedByteBuffer mappedByteBufferOne = fileChannelOne.map(FileChannel.MapMode.READ_ONLY, 0L, fileChannelOneSize);
                MappedByteBuffer mappedByteBufferTwo = fileChannelTwo.map(FileChannel.MapMode.READ_ONLY, 0L, fileChannelOneSize);

                StringBuilder stringBuilderOne = new StringBuilder("File 1 Positions With Differences:\n");
                StringBuilder stringBuilderTwo = new StringBuilder("File 2 Positions With Differences:\n");
                for (int i = 0; i < fileChannelOneSize; i++) {
                    if (mappedByteBufferOne.get(i) != mappedByteBufferTwo.get(i)) {
                        contentsIdentical = false;
                        stringBuilderOne.append(mappedByteBufferOne.get(i));
                        stringBuilderTwo.append(mappedByteBufferTwo.get(i));
                    }
                }
                log.debug(stringBuilderOne.toString());
                log.debug(stringBuilderTwo.toString());
            } else {
                return false;
            }
            return contentsIdentical;
        } catch (Exception e) {
            log.error("Error while comparing texts:\t " + e.getMessage());
            throw new Exception("Error while comparing texts:\t " + e.getMessage());
        }
    }

    public boolean compareFilesPDFUtilTextMode(Path fileOne, Path fileTwo, String exclusions) throws Exception {
        try {
            PDFUtil pdfUtil = new PDFUtil();
            pdfUtil.setCompareMode(CompareMode.TEXT_MODE);

            if (!exclusions.equalsIgnoreCase("")) {
                String[] regexes = exclusions.split("\n");
                pdfUtil.excludeText(regexes);
            }

            Integer fileOnePageCount = pdfUtil.getPageCount(fileOne.toString());
            Integer fileTwoPageCount = pdfUtil.getPageCount(fileTwo.toString());
            boolean equalsPageCount = fileOnePageCount.equals(fileTwoPageCount);
            boolean compare;

            if (equalsPageCount) {
                compare = pdfUtil.compare(fileOne.toString(), fileTwo.toString());
                return compare;
            } else {
                log.error("Files Page Count Not Equal.");
                throw new Exception("Files Page Count Not Equal.");
            }
        } catch (Exception e) {
            log.error("Error while comparing texts:\t " + e.getMessage());
            throw new Exception("Error while comparing texts:\t " + e.getMessage());
        }
    }

    public boolean compareFilesPDFUtilVisualMode(Path fileOne, Path fileTwo, Path resultsPath) throws Exception {
        try {
            PDFUtil pdfUtil = new PDFUtil();
            pdfUtil.setCompareMode(CompareMode.VISUAL_MODE);
            pdfUtil.highlightPdfDifference(true);
            pdfUtil.setImageDestinationPath(resultsPath.toString());

            Integer fileOnePageCount = pdfUtil.getPageCount(fileOne.toString());
            Integer fileTwoPageCount = pdfUtil.getPageCount(fileTwo.toString());
            boolean equalsPageCount = fileOnePageCount.equals(fileTwoPageCount);
            boolean compare;

            if (equalsPageCount) {
                compare = pdfUtil.compare(fileOne.toString(), fileTwo.toString());
                return compare;
            } else {
                log.error("Files Page Count Not Equal.");
                throw new Exception("Files Page Count Not Equal.");
            }
        } catch (Exception e) {
            log.error("Error while comparing pdf on visual mode:\t " + e.getMessage());
            throw new Exception("Error while comparing pdf on visual mode:\t " + e.getMessage());
        }
    }

    public boolean comparePdfFiles(Path actualFile, Path expectedFile, boolean compareTokens) throws Exception {
        try {
            String actualPdfData, expectedPdfData;
            List<String> actualListData = new ArrayList<>();
            List<String> expectedListData = new ArrayList<>();

            PDDocument actualPdfDocument = PDDocument.load(new File(actualFile.toString()));
            PDDocument expectedPdfDocument = PDDocument.load(new File(expectedFile.toString()));

            Integer actualFileNumberOfPages = actualPdfDocument.getNumberOfPages();
            Integer expectedFileNumberOfPages = expectedPdfDocument.getNumberOfPages();

            if (actualFileNumberOfPages.equals(expectedFileNumberOfPages)) {
                PDFTextStripperByArea stripper = new PDFTextStripperByArea();
                stripper.setSortByPosition(true);
                PDFTextStripper pdfTextStripper = new PDFTextStripper();
                actualPdfData = pdfTextStripper.getText(actualPdfDocument);
                expectedPdfData = pdfTextStripper.getText(expectedPdfDocument);

                if (!actualPdfData.equals(expectedPdfData)) {
                    log.info("File Contents Are Not Equal/The Same.");
                    if (compareTokens) {
                        StringTokenizer actualPdfDataStringTokenizer = new StringTokenizer(actualPdfData,"\n");
                        StringTokenizer expectedPdfDataStringTokenizer = new StringTokenizer(expectedPdfData,"\n");

                        while (actualPdfDataStringTokenizer.hasMoreTokens()) {
                            actualListData.add(actualPdfDataStringTokenizer.nextToken());
                        }

                        while (expectedPdfDataStringTokenizer.hasMoreTokens()) {
                            expectedListData.add(expectedPdfDataStringTokenizer.nextToken());
                        }

                        for (int i = 0; i < actualListData.size(); i++) {
                            if ((!actualListData.get(i).equals(expectedListData.get(i)))) {
                                log.debug(i + ": ACTUAL PDF FILE -> " + actualListData.get(i));
                                log.debug(i + ": EXPECTED PDF FILE -> " + expectedListData.get(i));
                            }
                        }
                    }
                }
            } else {
                log.info("Number of pages not equal. Actual: " + actualFileNumberOfPages + " , Expected: " + expectedFileNumberOfPages);
                return false;
            }

            return true;
        } catch (Exception e) {
            log.error("Error while comparing texts:\n " + e.getMessage());
            throw new Exception("Error while comparing texts:\n " + e.getMessage());
        }
    }

}
