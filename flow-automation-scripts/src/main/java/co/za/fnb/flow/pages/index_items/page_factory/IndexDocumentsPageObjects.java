package co.za.fnb.flow.pages.index_items.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class IndexDocumentsPageObjects {
    // Index Documents
    // id = frmIndexData:policyNumber
    @FindBy(id = "frmIndexData:policyNumber")
    private WebElement policyNumber;

    // id = frmIndexData:Search
    @FindBy(id = "frmIndexData:Search")
    private WebElement search;

    // //*[@id="frmIndexData:customers_data"]/tr/td[1]
    @FindBy(xpath = "//*[@id=\"frmIndexData:customers_data\"]/tr/td[1]")
    private WebElement idNumber;

    // //*[@id="frmIndexData:customers_data"]/tr/td[2]
    @FindBy(xpath = "//*[@id=\"frmIndexData:customers_data\"]/tr/td[2]")
    private WebElement ucn;

    // //*[@id="frmIndexData:customers_data"]/tr/td[3]
    @FindBy(xpath = "//*[@id=\"frmIndexData:customers_data\"]/tr/td[3]")
    private WebElement fullName;

    // //*[@id="frmIndexData:customers_data"]/tr/td[4]
    @FindBy(xpath = "//*[@id=\"frmIndexData:customers_data\"]/tr/td[4]")
    private WebElement dateOfBirth;

    // //*[@id="frmIndexData:customers_data"]/tr/td[5]
    @FindBy(xpath = "//*[@id=\"frmIndexData:customers_data\"]/tr/td[5]")
    private WebElement contactNumber;

    // //*[@id="frmIndexData:selectDocType"]/div[3]/span
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectDocType\"]/div[3]/span")
    private WebElement selectDocType;

    // //*[@id="frmIndexData:selectDocType_filter"]
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectDocType_filter\"]")
    private WebElement selectDocTypeFilter;

    // id = frmIndexData:selectDocType_items
    @FindBy(id = "frmIndexData:selectDocType_items")
    private WebElement selectDocTypeItems;

    // //*[@id="frmIndexData:selectDocStatus"]/div[3]/span
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectDocStatus\"]/div[3]/span")
    private WebElement selectDocStatus;

    // //*[@id="frmIndexData:selectDocStatus_filter"]
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectDocStatus_filter\"]")
    private WebElement selectDocStatusFilter;

    // id = frmIndexData:selectDocStatus_items
    @FindBy(id = "frmIndexData:selectDocStatus_items")
    private WebElement selectDocStatusItems;

    // //*[@id="frmIndexData:selectWorkTypes"]/div[3]/span
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectWorkTypes\"]/div[3]/span")
    private WebElement selectWorkTypes;

    // //*[@id="frmIndexData:selectWorkTypes_filter"]
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectWorkTypes_filter\"]")
    private WebElement selectWorkTypesFilter;

    // frmIndexData:selectWorkTypes_items
    @FindBy(id = "frmIndexData:selectWorkTypes_items")
    private WebElement selectWorkTypesItems;

    // //*[@id="frmIndexData:selectQueues"]/div[3]/span
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectQueues\"]/div[3]/span")
    private WebElement selectQueues;
    // //*[@id="frmIndexData:selectQueues_filter"]
    @FindBy(xpath = "//*[@id=\"frmIndexData:selectQueues_filter\"]")
    private WebElement selectQueuesFilter;

    // id = frmIndexData:selectQueues_items
    @FindBy(id = "frmIndexData:selectQueues_items")
    private WebElement selectQueuesItems;

    // //*[@id="frmIndexData:itemPolicyNumber"]/div[3]/span
    // //*[@id="frmIndexData:itemPolicyNumber"]/input
    @FindBy(xpath = "//*[@id=\"frmIndexData:itemPolicyNumber\"]/input")
    private WebElement itemPolicyNumber;

    // id = frmIndexData:AddItem
    @FindBy(id = "frmIndexData:AddItem")
    private WebElement addItem;

    // id = frmIndexData:UpdateItem
    @FindBy(id = "frmIndexData:UpdateItem")
    private WebElement updateItem;

    // id = frmIndexData:RemoveItem
    @FindBy(id = "frmIndexData:RemoveItem")
    private WebElement removeItem;

    // id = frmIndexData:ClearItem
    @FindBy(id = "frmIndexData:ClearItem")
    private WebElement clearItem;

    // Doc Check Boxes
    // //*[@id="frmDocumentView:j_idt12"]/div[2]/span
    @FindBy(xpath = "//*[@id=\"frmDocumentView:j_idt12\"]/div[2]/span")
    private WebElement documentCheckBoxesAll;

    // id = frmIndexData:Bin
    @FindBy(id = "frmIndexData:Bin")
    private WebElement bin;

    // id = frmIndexData:Index
    @FindBy(id = "frmIndexData:Index")
    private WebElement index;

    public IndexDocumentsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPolicyNumber() {
        return policyNumber;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getIdNumber() {
        return idNumber;
    }

    public WebElement getUcn() {
        return ucn;
    }

    public WebElement getFullName() {
        return fullName;
    }

    public WebElement getDateOfBirth() {
        return dateOfBirth;
    }

    public WebElement getContactNumber() {
        return contactNumber;
    }

    public WebElement getSelectDocType() {
        return selectDocType;
    }

    public WebElement getSelectDocTypeFilter() {
        return selectDocTypeFilter;
    }

    public WebElement getSelectDocTypeItems() {
        return selectDocTypeItems;
    }

    public WebElement getSelectDocStatus() {
        return selectDocStatus;
    }

    public WebElement getSelectDocStatusFilter() {
        return selectDocStatusFilter;
    }

    public WebElement getSelectDocStatusItems() {
        return selectDocStatusItems;
    }

    public WebElement getSelectWorkTypes() {
        return selectWorkTypes;
    }

    public WebElement getSelectWorkTypesFilter() {
        return selectWorkTypesFilter;
    }

    public WebElement getSelectWorkTypesItems() {
        return selectWorkTypesItems;
    }

    public WebElement getSelectQueues() {
        return selectQueues;
    }

    public WebElement getSelectQueuesFilter() {
        return selectQueuesFilter;
    }

    public WebElement getSelectQueuesItems() {
        return selectQueuesItems;
    }

    public WebElement getItemPolicyNumber() {
        return itemPolicyNumber;
    }

    public WebElement getAddItem() {
        return addItem;
    }

    public WebElement getUpdateItem() {
        return updateItem;
    }

    public WebElement getRemoveItem() {
        return removeItem;
    }

    public WebElement getClearItem() {
        return clearItem;
    }

    public WebElement getDocumentCheckBoxesAll() {
        return documentCheckBoxesAll;
    }

    public WebElement getBin() {
        return bin;
    }

    public WebElement getIndex() {
        return index;
    }
}
