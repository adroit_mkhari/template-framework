package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.CaptureAttorneyPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class CaptureAttorneyPaymentDetails extends BasePage {
    private Logger log  = LogManager.getLogger(CaptureAttorneyPaymentDetails.class);
    CaptureAttorneyPaymentPageObjects captureAttorneyPaymentPageObjects = new CaptureAttorneyPaymentPageObjects(driver);

    public CaptureAttorneyPaymentDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickInvoiceNumber() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getInvoiceNumber());
        } catch (Exception e) {
            log.error("Error while clicking Invoice Number.");
            throw new Exception("Error while clicking Invoice Number.");
        }
    }

    public void updateInvoiceNumber(String invoiceNumber) throws Exception {
        try {
            log.info("Updating Invoice Number.");
            type(captureAttorneyPaymentPageObjects.getInvoiceNumber(), invoiceNumber);
        } catch (Exception e) {
            log.error("Error while updating Invoice Number.");
            throw new Exception("Error while updating Invoice Number.");
        }
    }

    public void clickInvoiceDateInput() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getInvoiceDateInput());
        } catch (Exception e) {
            log.error("Error while clicking Invoice Date.");
            throw new Exception("Error while clicking Invoice Date.");
        }
    }

    public void updateInvoiceDateInput(String invoiceDate) throws Exception {
        try {
            log.info("Updating Invoice Date.");
            type(captureAttorneyPaymentPageObjects.getInvoiceDateInput(), invoiceDate);
        } catch (Exception e) {
            log.error("Error while updating Invoice Date.");
            throw new Exception("Error while updating Invoice Date.");
        }
    }

    public void clickDateReceivedInput() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getDateReceivedInput());
        } catch (Exception e) {
            log.error("Error while clicking Date Received.");
            throw new Exception("Error while clicking Date Received.");
        }
    }

    public void updateDateReceivedInput(String dateReceived) throws Exception {
        try {
            log.info("Updating Date Received.");
            type(captureAttorneyPaymentPageObjects.getDateReceivedInput(), dateReceived);
        } catch (Exception e) {
            log.error("Error while updating Date Received.");
            throw new Exception("Error while updating Date Received.");
        }
    }

    public void clickServiceType() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getServiceType());
        } catch (Exception e) {
            log.error("Error while clicking on Service Type.");
            throw new Exception("Error while clicking on Service Type.");
        }
    }

    public void doubleClickServiceType() throws Exception {
        try {
            moveToElementAndDoubleClick(captureAttorneyPaymentPageObjects.getServiceType());
        } catch (Exception e) {
            log.error("Error while clicking on Service Type.");
            throw new Exception("Error while clicking on Service Type.");
        }
    }

    public void selectServiceType(String serviceType) throws Exception {
        try {
            log.info("Selecting Service Type.");
            selectOnDashboard(captureAttorneyPaymentPageObjects.getServiceTypeLabel(), captureAttorneyPaymentPageObjects.getServiceTypeItems(), serviceType);
        } catch (Exception e) {
            log.error("Error while selecting Service Type.");
            throw new Exception("Error while selecting Service Type.");
        }
    }

    public void clickDescription() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getDescription());
        } catch (Exception e) {
            log.error("Error while clicking on Description.");
            throw new Exception("Error while clicking on Description.");
        }
    }

    public void doubleClickDescription() throws Exception {
        try {
            moveToElementAndDoubleClick(captureAttorneyPaymentPageObjects.getDescription());
        } catch (Exception e) {
            log.error("Error while clicking on Description.");
            throw new Exception("Error while clicking on Description.");
        }
    }

    public void selectDescription(String description) throws Exception {
        try {
            log.info("Selecting Description.");
            selectOnDashboard(captureAttorneyPaymentPageObjects.getDescriptionLabel(), captureAttorneyPaymentPageObjects.getDescriptionItems(), description);
        } catch (Exception e) {
            log.error("Error while selecting Description.");
            throw new Exception("Error while selecting Description.");
        }
    }

    public void clickAmount() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getAmountInput());
        } catch (Exception e) {
            log.error("Error while clicking on Amount.");
            throw new Exception("Error while clicking on Amount.");
        }
    }

    public void doubleClickAmount() throws Exception {
        try {
            moveToElementAndDoubleClick(captureAttorneyPaymentPageObjects.getAmountInput());
        } catch (Exception e) {
            log.error("Error while double clicking on Amount.");
            throw new Exception("Error while double clicking on Amount.");
        }
    }

    public void updateAmount(String amount) throws Exception {
        try {
            log.info("Updating Amount.");
            type(captureAttorneyPaymentPageObjects.getAmountInput(), amount);
        } catch (Exception e) {
            log.error("Error while updating Amount.");
            throw new Exception("Error while updating Amount.");
        }
    }

    public void clickVatFlag() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getVatFlag());
        } catch (Exception e) {
            log.error("Error while clicking on Vat Flag.");
            throw new Exception("Error while clicking on Vat Flag.");
        }
    }

    public void selectVatFlag(String vatFlag) throws Exception {
        try {
            log.info("Selecting VAT Flag.");
            selectOnDashboard(captureAttorneyPaymentPageObjects.getVatFlagLabel(), captureAttorneyPaymentPageObjects.getVatFlagItems(), vatFlag);
        } catch (Exception e) {
            log.error("Error while selecting Vat Flag.");
            throw new Exception("Error while selecting Vat Flag.");
        }
    }

    public void clickBtnMakePaymentAdd() throws Exception {
        try {
            log.info("Clicking Make Payment Add.");
            click(captureAttorneyPaymentPageObjects.getBtnMakePaymentAdd());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Payment Add.");
            throw new Exception("Error while clicking on Btn Make Payment Add.");
        }
    }

    public void clickPaymentStageInterim() throws Exception {
        try {
            log.info("Clicking Payment Stage: Interim");
            click(captureAttorneyPaymentPageObjects.getPaymentStageInterim());
        } catch (Exception e) {
            log.error("Error while clicking on Payment Stage Interim.");
            throw new Exception("Error while clicking on Payment Stage Interim.");
        }
    }

    public void clickPaymentStageFinal() throws Exception {
        try {
            log.info("Clicking Payment Stage: Final");
            click(captureAttorneyPaymentPageObjects.getPaymentStageFinal());
        } catch (Exception e) {
            log.error("Error while clicking on Payment Stage Final.");
            throw new Exception("Error while clicking on Payment Stage Final.");
        }
    }

    public void clickTotalAttorneyFee() throws Exception {
        try {
            click(captureAttorneyPaymentPageObjects.getTotalAttorneyFeeInput());
        } catch (Exception e) {
            log.error("Error while clicking Total Attorney Fee.");
            throw new Exception("Error while clicking Total Attorney Fee.");
        }
    }

    public void clickBtnMakePaymentSubmit() throws Exception {
        try {
            log.info("Clicking Submit.");
            moveToElementAndClickJsExec(captureAttorneyPaymentPageObjects.getBtnMakePaymentSubmit());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Payment Submit.");
            throw new Exception("Error while clicking on Btn Make Payment Submit.");
        }
    }

    public void clickBtnMakePaymentCancel() throws Exception {
        try {
            log.info("Clicking Cancel.");
            click(captureAttorneyPaymentPageObjects.getBtnMakePaymentCancel());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Payment Cancel.");
            throw new Exception("Error while clicking on Btn Make Payment Cancel.");
        }
    }

    public void doubleClickTotalAttorneyFee() throws Exception {
        try {
            log.info("Double Clicking on Total Attorney Fee.");
            moveToElementAndDoubleClick(captureAttorneyPaymentPageObjects.getTotalAttorneyFeeInput());
        } catch (Exception e) {
            log.error("Error while double clicking Total Attorney Fee.");
            throw new Exception("Error while double clicking Total Attorney Fee.");
        }
    }

    public void updateTotalAttorneyFee(String totalAttorneyFee) throws Exception {
        try {
            doubleClickTotalAttorneyFee();
            log.info("Updating Total Attorney Fee.");
            type(captureAttorneyPaymentPageObjects.getTotalAttorneyFeeInput(), totalAttorneyFee);
        } catch (Exception e) {
            log.error("Error while updating Total Attorney Fee.");
            throw new Exception("Error while updating Total Attorney Fee.");
        }
    }

}
