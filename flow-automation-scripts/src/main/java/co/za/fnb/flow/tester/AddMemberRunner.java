package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.models.policy_details.*;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.tester.policy_details.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import co.za.fnb.flow.handlers.Scenario;

public class AddMemberRunner {
    private Logger log  = LogManager.getLogger(AddMemberRunner.class);
    private QueryHandler queryHandler = new QueryHandler();
    private WebDriver driver;
    private HomePage homePage;
    private PolicyDetails policyDetails;
    private CreateSearchItemPage createSearchItemPage;
    private CreateMultipleMintItemTable createMultipleMintItemtable;
    private WorkItems workItems;
    private AuditTrail auditTrail;
    private String policyNumber;
    private Scenario scenario;
    private ScenarioOperator scenarioOperator;

    public AddMemberRunner(WebDriver driver, String policyNumber, Scenario scenario, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.policyNumber = policyNumber;
        this.scenario = scenario;
        this.scenarioOperator = scenarioOperator;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(String productName, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        if (!productName.toUpperCase().contains("LOC")) {
            queryHandler.insertFicaRecord(
                    productName,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (!status.equalsIgnoreCase(ficaStatus)) {
                    throw new FicaStatusException("WRONG FICA STATUS OR ERROR status on policy Number: " + policy +
                            ". Required Status: " + ficaStatus + " -> Found: " + status);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + dbPolicyNumber);
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting Scenario Tests.");

        String testCaseNumber = scenario.getCellValue("Test Case No");
        String testFunction = scenario.getCellValue("Function");
        try {
            log.info("===========================================================================");
            log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
            log.info("---------------------------------------------------------------------------");

            if (testFunction.equalsIgnoreCase("Add Member")) {
                addMember();
            }
            log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            scenario.setResult(String.valueOf(TestResult.FAIL));
            scenario.setFailureReason(e.getMessage());
        } finally {
            if (driver.getWindowHandles().toArray().length > 1) {
                int numberOfWindows = driver.getWindowHandles().toArray().length;
                while (numberOfWindows > 1) {
                    Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows - 1];
                    WebDriver window = driver.switchTo().window((String) currentWindow);
                    window.close();
                    numberOfWindows = driver.getWindowHandles().toArray().length;
                }
            }
        }

        log.info("===========================================================================");
    }

    private void addMember() throws Exception {
        log.info("Running Add Member Logic");
        int testCaseNumber = getIntegerValue(scenario.getCellValue("Test Case No")) != -1 ? getIntegerValue(scenario.getCellValue("Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(scenario.getCellValue("Debit Order Date")) != -1 ? getIntegerValue(scenario.getCellValue("Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(scenario.getCellValue("NoD")) != -1 ? getIntegerValue(scenario.getCellValue("NoD")) : 1;
        Member member = new Member(
                // region Add Member Model Fields
                String.valueOf(testCaseNumber),
                scenario.getCellValue("Product Name"),
                scenario.getCellValue("Scenario Description"),
                scenario.getCellValue("Policy No"),
                policyNumber,
                scenario.getCellValue("Function"),
                scenario.getCellValue("Run"),
                scenario.getCellValue("First Popup"),
                scenario.getCellValue("Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(scenario.getCellValue("Next Due Date"), "dd-MM-yyyy"),
                scenario.getCellValue("Bank Name"),
                scenario.getCellValue("Expected Result"),
                scenario.getCellValue("Work Type0"),
                scenario.getCellValue("Status0"),
                scenario.getCellValue("Queue0"),
                scenario.getCellValue("Work Type1"),
                scenario.getCellValue("Status1"),
                scenario.getCellValue("Queue1"),
                scenario.getCellValue("POP UP Work Type0"),
                scenario.getCellValue("POP UP Status0"),
                scenario.getCellValue("POP UP Queue0"),
                scenario.getCellValue("POP UP Work Type1"),
                scenario.getCellValue("POP UP Status1"),
                scenario.getCellValue("POP UP Queue1"),
                scenario.getCellValue("Audit Trail Events"),
                scenario.getCellValue("Updatedte"),
                scenario.getCellValue("Risk"),
                scenario.getCellValue("Sanction"),
                scenario.getCellValue("Edd"),
                scenario.getCellValue("Kyc"),
                scenario.getCellValue("Status"),
                scenario.getCellValue("Relationship"),
                scenario.getCellValue("Company Name / Full Name"),
                scenario.getCellValue("Trading as Name/ Middle Name"),
                scenario.getCellValue("Company Reg Number / ID Number"),
                scenario.getCellValue("DOB"),
                scenario.getCellValue("Gender"),
                scenario.getCellValue("Cover Amount"),
                scenario.getCellValue("Premium Amount"),
                scenario.getCellValue("Discount"),
                scenario.getCellValue("Email Address"),
                scenario.getCellValue("Cell Phone Number"),
                scenario.getCellValue("Bank Name"),
                String.valueOf(numberOfMembers),
                scenario.getCellValue("Beneficiary Name"),
                scenario.getCellValue("Beneficiary DOB"),
                scenario.getCellValue("Beneficiary ID Number"),
                scenario.getCellValue("Beneficiary Contact Number"),
                scenario.getCellValue("Beneficiary Email Address"),
                scenario.getCellValue("Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        AddMember addMember = new AddMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            String dbPolicyNumber = policyNumber;
            if (dbPolicyNumber != null) {
                queryHandler.setDbPolicyNumber(dbPolicyNumber);
                // region Update FICA Status
                updateFicaStatus(member.getProductName(), dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            createSearchItemPage.searchPolicyNumber(policyNumber, true);

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            addMember.setExpectedResults();
            log.info("Adding Member, Policy Number: " + policyNumber);
            addMember.add();
            log.info("Saving Added Member(s).");
            addMember.save();

            testHandle = addMember.getTestHandle();
            errorHandle = addMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                addMember.validate();
                if (!addMember.isValid()) {
                    scenario.setResult(String.valueOf(TestResult.FAIL));
                    scenario.setFailureReason(addMember.getComment());
                } else {
                    createMultipleMintItemtable = addMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    Thread.sleep(3000);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        Thread.sleep(3000);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            scenario.setResult(String.valueOf(TestResult.PASS));
                        } else {
                            scenario.setResult(String.valueOf(TestResult.FAIL));
                            scenario.setFailureReason(errorHandle.getError());
                        }
                    } else {
                        scenario.setResult(String.valueOf(TestResult.FAIL));
                        scenario.setFailureReason(errorHandle.getError());
                    }
                }
            } else {
                scenario.setResult(String.valueOf(TestResult.FAIL));
                scenario.setFailureReason(errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                scenario.setResult(String.valueOf(TestResult.FAIL));
                scenario.setFailureReason(e.getMessage());
            } else if (e instanceof ServicePageException) {
                scenario.setResult(String.valueOf(TestResult.FAIL));
                scenario.setFailureReason(e.getMessage());
            } else if (e instanceof NegativeTestException) {
                scenario.setResult(String.valueOf(TestResult.PASS));
                scenario.setFailureReason(e.getMessage());
            } else {
                scenario.setResult(String.valueOf(TestResult.FAIL));
                scenario.setFailureReason(e.getMessage());
            }
        }
        log.info("End Of Add Member Logic");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

}
