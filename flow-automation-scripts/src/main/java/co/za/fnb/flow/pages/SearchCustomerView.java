package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.SearchCustomerViewPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class SearchCustomerView extends BasePage {
    SearchCustomerViewPageObjects searchCustomerViewPageObjects = new SearchCustomerViewPageObjects(driver);

    public SearchCustomerView(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
