package co.za.fnb.flow.setup;

public enum TestResultReportFlag {
    SUCCESS,
    WARNING,
    FAIL,
    DEFAULT,
    PICTURE
}
