package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class SearchCustomerViewPageObjects {
    @FindBy(id = "customerSearchView:idNumber")
    private WebElement idNumber;

    @FindBy(id = "customerSearchView:ucn")
    private WebElement ucn;

    @FindBy(id = "customerSearchView:searchPolicyNumber")
    private WebElement searchPolicyNumber;

    @FindBy(id = "customerSearchView:name")
    private WebElement name;

    @FindBy(id = "customerSearchView:lastName")
    private WebElement lastName;

    @FindBy(id = "customerSearchView:dateOfBirth_input")
    private WebElement dateOfBirthInput;

    @FindBy(id = "customerSearchView:Clear")
    private WebElement clear;

    @FindBy(id = "customerSearchView:Search")
    private WebElement search;

    @FindBy(id = "customerSearchView:customers_data")
    private WebElement customersData;

    @FindBy(id = "customerSearchView:customerPolicyDocuments:tablePolicies")
    private WebElement customerPolicyDocumentsTablePolicies;

    public SearchCustomerViewPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }
}
