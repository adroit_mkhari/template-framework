package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AwaitDocOrDiarizeItemPageObjects {

    // region Cover Details
    @FindBy(id = "awaitDocDiarizeView:time")
    WebElement time; // Duration

    @FindBy(id = "awaitDocDiarizeView:time_label")
    WebElement timeLabel; // Duration

    @FindBy(id = "awaitDocDiarizeView:time_items")
    WebElement timeItems; // Duration, Uses @data-label

    @FindBy(id = "awaitDocDiarizeView:comments")
    WebElement comments;

    @FindBy(id = "awaitDocDiarizeView:reasons")
    WebElement reasons;

    @FindBy(id = "awaitDocDiarizeView:reasons_label")
    WebElement reasonsLabel;

    @FindBy(id = "awaitDocDiarizeView:reasons_items")
    WebElement reasonsItems;

    @FindBy(id = "awaitDocDiarizeView:Submit")
    WebElement submit;

    @FindBy(id = "awaitDocDiarizeView:Cancel")
    WebElement cancel;
    // endregion

    public AwaitDocOrDiarizeItemPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getTime() {
        return time;
    }

    public WebElement getTimeLabel() {
        return timeLabel;
    }

    public WebElement getTimeItems() {
        return timeItems;
    }

    public WebElement getComments() {
        return comments;
    }

    public WebElement getReasons() {
        return reasons;
    }

    public WebElement getReasonsLabel() {
        return reasonsLabel;
    }

    public WebElement getReasonsItems() {
        return reasonsItems;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getCancel() {
        return cancel;
    }
}
