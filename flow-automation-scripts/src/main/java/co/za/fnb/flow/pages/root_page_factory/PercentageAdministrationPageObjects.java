package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PercentageAdministrationPageObjects {
    @FindBy(linkText = "Percentage Administration")
    private WebElement percentageAdministration;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:selectUser")
    private WebElement selectUser;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:selectUser_items")
    private WebElement selectUserItems;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnAddSettings")
    private WebElement addSettings;

    @FindBy(id = "frmAddSetting:selectWorkType")
    private WebElement selectWorkType;

    @FindBy(id = "frmAddSetting:selectWorkType_items")
    private WebElement selectWorkTypeItems;

    @FindBy(id = "frmAddSetting:selectStatus")
    private WebElement selectStatus;

    @FindBy(id = "frmAddSetting:selectStatus_items")
    private WebElement selectStatusItems;

    @FindBy(id = "frmAddSetting:selectQueue")
    private WebElement selectQueue;

    @FindBy(id = "frmAddSetting:selectQueue_items")
    private WebElement selectQueueItems;

    @FindBy(id = "frmAddSetting:selectPercentage")
    private WebElement selectPercentage;

    @FindBy(id = "frmAddSetting:selectPercentage_items")
    private WebElement selectPercentageItems;

    @FindBy(id = "btnAddSettingSubmit")
    private WebElement submit;

    public PercentageAdministrationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPercentageAdministration() {
        return percentageAdministration;
    }

    public WebElement getSelectUser() {
        return selectUser;
    }

    public WebElement getSelectUserItems() {
        return selectUserItems;
    }

    public WebElement getAddSettings() {
        return addSettings;
    }

    public WebElement getSelectWorkType() {
        return selectWorkType;
    }

    public WebElement getSelectWorkTypeItems() {
        return selectWorkTypeItems;
    }

    public WebElement getSelectStatus() {
        return selectStatus;
    }

    public WebElement getSelectStatusItems() {
        return selectStatusItems;
    }

    public WebElement getSelectQueue() {
        return selectQueue;
    }

    public WebElement getSelectQueueItems() {
        return selectQueueItems;
    }

    public WebElement getSelectPercentage() {
        return selectPercentage;
    }

    public WebElement getSelectPercentageItems() {
        return selectPercentageItems;
    }

    public WebElement getSubmit() {
        return submit;
    }
}
