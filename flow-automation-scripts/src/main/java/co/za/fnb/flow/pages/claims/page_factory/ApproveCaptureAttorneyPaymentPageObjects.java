package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ApproveCaptureAttorneyPaymentPageObjects {

    // region Group Information
    @FindBy(id = "btnMakePaymentApprove")
    WebElement approve;

    @FindBy(id = "btnMakePaymentDecline")
    WebElement decline;

    @FindBy(id = "paymentconfirmationapprove:btnConfirmMakePaymentApproveYes")
    WebElement yesApprove;

    @FindBy(id = "paymentconfirmationapprove:btnConfirmMakePaymentApproveNo")
    WebElement noApprove;

    public ApproveCaptureAttorneyPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getApprove() {
        return approve;
    }

    public WebElement getDecline() {
        return decline;
    }

    public WebElement getYesApprove() {
        return yesApprove;
    }

    public WebElement getNoApprove() {
        return noApprove;
    }
}
