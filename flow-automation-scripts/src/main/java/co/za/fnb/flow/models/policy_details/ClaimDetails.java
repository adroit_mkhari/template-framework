package co.za.fnb.flow.models.policy_details;

public class ClaimDetails extends PolicyDetailsBase {
    private String policyHolderId;
    private String claimHandler;
    private String event;
    private String eventDescription;
    private String eventDate;
    private String claimRegistrationDate;
    private String claimant;
    private String claimStatus;
    private String claimType;
    private String attorneyCompanyName;
    private String attorneyCellNumber;
    private String attorneyEmail;
    private String attorneyType;
    private String province;
    private String city;
    private String cellNumber;
    private String attorneyName;
    private String withAttorney;
    private String payee;
    private String submitPayment;
    private String invoiceNumber;
    private String invoiceDate;
    private String dateReceived;
    private String invoiceDetailsServiceType;
    private String invoiceDetailsDescription;
    private String invoiceDetailsAmount;
    private String invoiceDetailsVAT;
    private String totalAttorneyFee;
    private String aolAmount;
    private String paymentStage;
    private String awaitDocDurationPerDay;
    private String awaitDocComments;
    private String awaitDocReason;
    private String awaitDocSubmit;
    private String submitPopUpWorkTypeZero;
    private String submitPopUpStatusZero;
    private String submitPopUpQueueZero;
    private String submitWorkTypeZero;
    private String submitStatusZero;
    private String submitQueueZero;
    private String paymentWorkItemsStatus;
    private String paymentWorkItemsQueue;
    private String approvePayment;
    private String yesApprovePayment;
    private String approvePopUpWorkTypeZero;
    private String approvePopUpStatusZero;
    private String approvePopUpQueueZero;
    private String approveWorkTypeZero;
    private String approveStatusZero;
    private String approveQueueZero;

    public ClaimDetails(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public ClaimDetails(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String policyHolderId, String claimHandler, String event, String eventDescription, String eventDate, String claimRegistrationDate, String claimant, String claimStatus, String claimType, String attorneyCompanyName, String attorneyCellNumber, String attorneyEmail, String attorneyType, String province, String city, String cellNumber, String attorneyName, String withAttorney, String payee, String submitPayment, String invoiceNumber, String invoiceDate, String dateReceived, String invoiceDetailsServiceType, String invoiceDetailsDescription, String invoiceDetailsAmount, String invoiceDetailsVAT, String totalAttorneyFee, String aolAmount, String paymentStage, String awaitDocDurationPerDay, String awaitDocComments, String awaitDocReason, String awaitDocSubmit, String submitPopUpWorkTypeZero, String submitPopUpStatusZero, String submitPopUpQueueZero, String submitWorkTypeZero, String submitStatusZero, String submitQueueZero, String paymentWorkItemsStatus, String paymentWorkItemsQueue, String approvePayment, String yesApprovePayment, String approvePopUpWorkTypeZero, String approvePopUpStatusZero, String approvePopUpQueueZero, String approveWorkTypeZero, String approveStatusZero, String approveQueueZero) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.policyHolderId = policyHolderId;
        this.claimHandler = claimHandler;
        this.event = event;
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.claimRegistrationDate = claimRegistrationDate;
        this.claimant = claimant;
        this.claimStatus = claimStatus;
        this.claimType = claimType;
        this.attorneyCompanyName = attorneyCompanyName;
        this.attorneyCellNumber = attorneyCellNumber;
        this.attorneyEmail = attorneyEmail;
        this.attorneyType = attorneyType;
        this.province = province;
        this.city = city;
        this.cellNumber = cellNumber;
        this.attorneyName = attorneyName;
        this.withAttorney = withAttorney;
        this.payee = payee;
        this.submitPayment = submitPayment;
        this.invoiceNumber = invoiceNumber;
        this.invoiceDate = invoiceDate;
        this.dateReceived = dateReceived;
        this.invoiceDetailsServiceType = invoiceDetailsServiceType;
        this.invoiceDetailsDescription = invoiceDetailsDescription;
        this.invoiceDetailsAmount = invoiceDetailsAmount;
        this.invoiceDetailsVAT = invoiceDetailsVAT;
        this.totalAttorneyFee = totalAttorneyFee;
        this.aolAmount = aolAmount;
        this.paymentStage = paymentStage;
        this.awaitDocDurationPerDay = awaitDocDurationPerDay;
        this.awaitDocComments = awaitDocComments;
        this.awaitDocReason = awaitDocReason;
        this.awaitDocSubmit = awaitDocSubmit;
        this.submitPopUpWorkTypeZero = submitPopUpWorkTypeZero;
        this.submitPopUpStatusZero = submitPopUpStatusZero;
        this.submitPopUpQueueZero = submitPopUpQueueZero;
        this.submitWorkTypeZero = submitWorkTypeZero;
        this.submitStatusZero = submitStatusZero;
        this.submitQueueZero = submitQueueZero;
        this.paymentWorkItemsStatus = paymentWorkItemsStatus;
        this.paymentWorkItemsQueue = paymentWorkItemsQueue;
        this.approvePayment = approvePayment;
        this.yesApprovePayment = yesApprovePayment;
        this.approvePopUpWorkTypeZero = approvePopUpWorkTypeZero;
        this.approvePopUpStatusZero = approvePopUpStatusZero;
        this.approvePopUpQueueZero = approvePopUpQueueZero;
        this.approveWorkTypeZero = approveWorkTypeZero;
        this.approveStatusZero = approveStatusZero;
        this.approveQueueZero = approveQueueZero;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getPolicyHolderId() {
        return policyHolderId;
    }

    public void setPolicyHolderId(String policyHolderId) {
        this.policyHolderId = policyHolderId;
    }

    public String getClaimHandler() {
        return claimHandler;
    }

    public void setClaimHandler(String claimHandler) {
        this.claimHandler = claimHandler;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getEventDescription() {
        return eventDescription;
    }

    public void setEventDescription(String eventDescription) {
        this.eventDescription = eventDescription;
    }

    public String getEventDate() {
        return eventDate;
    }

    public void setEventDate(String eventDate) {
        this.eventDate = eventDate;
    }

    public String getClaimRegistrationDate() {
        return claimRegistrationDate;
    }

    public void setClaimRegistrationDate(String claimRegistrationDate) {
        this.claimRegistrationDate = claimRegistrationDate;
    }

    public String getClaimant() {
        return claimant;
    }

    public void setClaimant(String claimant) {
        this.claimant = claimant;
    }

    public String getClaimStatus() {
        return claimStatus;
    }

    public void setClaimStatus(String claimStatus) {
        this.claimStatus = claimStatus;
    }

    public String getClaimType() {
        return claimType;
    }

    public void setClaimType(String claimType) {
        this.claimType = claimType;
    }

    public String getAttorneyCompanyName() {
        return attorneyCompanyName;
    }

    public void setAttorneyCompanyName(String attorneyCompanyName) {
        this.attorneyCompanyName = attorneyCompanyName;
    }

    public String getAttorneyCellNumber() {
        return attorneyCellNumber;
    }

    public void setAttorneyCellNumber(String attorneyCellNumber) {
        this.attorneyCellNumber = attorneyCellNumber;
    }

    public String getAttorneyEmail() {
        return attorneyEmail;
    }

    public void setAttorneyEmail(String attorneyEmail) {
        this.attorneyEmail = attorneyEmail;
    }

    public String getAttorneyType() {
        return attorneyType;
    }

    public void setAttorneyType(String attorneyType) {
        this.attorneyType = attorneyType;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAttorneyName() {
        return attorneyName;
    }

    public void setAttorneyName(String attorneyName) {
        this.attorneyName = attorneyName;
    }

    public String getWithAttorney() {
        return withAttorney;
    }

    public void setWithAttorney(String withAttorney) {
        this.withAttorney = withAttorney;
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getSubmitPayment() {
        return submitPayment;
    }

    public void setSubmitPayment(String submitPayment) {
        this.submitPayment = submitPayment;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getDateReceived() {
        return dateReceived;
    }

    public void setDateReceived(String dateReceived) {
        this.dateReceived = dateReceived;
    }

    public String getInvoiceDetailsServiceType() {
        return invoiceDetailsServiceType;
    }

    public void setInvoiceDetailsServiceType(String invoiceDetailsServiceType) {
        this.invoiceDetailsServiceType = invoiceDetailsServiceType;
    }

    public String getInvoiceDetailsDescription() {
        return invoiceDetailsDescription;
    }

    public void setInvoiceDetailsDescription(String invoiceDetailsDescription) {
        this.invoiceDetailsDescription = invoiceDetailsDescription;
    }

    public String getInvoiceDetailsAmount() {
        return invoiceDetailsAmount;
    }

    public void setInvoiceDetailsAmount(String invoiceDetailsAmount) {
        this.invoiceDetailsAmount = invoiceDetailsAmount;
    }

    public String getInvoiceDetailsVAT() {
        return invoiceDetailsVAT;
    }

    public void setInvoiceDetailsVAT(String invoiceDetailsVAT) {
        this.invoiceDetailsVAT = invoiceDetailsVAT;
    }

    public String getTotalAttorneyFee() {
        return totalAttorneyFee;
    }

    public void setTotalAttorneyFee(String totalAttorneyFee) {
        this.totalAttorneyFee = totalAttorneyFee;
    }

    public String getAolAmount() {
        return aolAmount;
    }

    public void setAolAmount(String aolAmount) {
        this.aolAmount = aolAmount;
    }

    public String getPaymentStage() {
        return paymentStage;
    }

    public void setPaymentStage(String paymentStage) {
        this.paymentStage = paymentStage;
    }

    public String getAwaitDocDurationPerDay() {
        return awaitDocDurationPerDay;
    }

    public void setAwaitDocDurationPerDay(String awaitDocDurationPerDay) {
        this.awaitDocDurationPerDay = awaitDocDurationPerDay;
    }

    public String getAwaitDocComments() {
        return awaitDocComments;
    }

    public void setAwaitDocComments(String awaitDocComments) {
        this.awaitDocComments = awaitDocComments;
    }

    public String getAwaitDocReason() {
        return awaitDocReason;
    }

    public void setAwaitDocReason(String awaitDocReason) {
        this.awaitDocReason = awaitDocReason;
    }

    public String getAwaitDocSubmit() {
        return awaitDocSubmit;
    }

    public void setAwaitDocSubmit(String awaitDocSubmit) {
        this.awaitDocSubmit = awaitDocSubmit;
    }

    public String getSubmitPopUpWorkTypeZero() {
        return submitPopUpWorkTypeZero;
    }

    public void setSubmitPopUpWorkTypeZero(String submitPopUpWorkTypeZero) {
        this.submitPopUpWorkTypeZero = submitPopUpWorkTypeZero;
    }

    public String getSubmitPopUpStatusZero() {
        return submitPopUpStatusZero;
    }

    public void setSubmitPopUpStatusZero(String submitPopUpStatusZero) {
        this.submitPopUpStatusZero = submitPopUpStatusZero;
    }

    public String getSubmitPopUpQueueZero() {
        return submitPopUpQueueZero;
    }

    public void setSubmitPopUpQueueZero(String submitPopUpQueueZero) {
        this.submitPopUpQueueZero = submitPopUpQueueZero;
    }

    public String getSubmitWorkTypeZero() {
        return submitWorkTypeZero;
    }

    public void setSubmitWorkTypeZero(String submitWorkTypeZero) {
        this.submitWorkTypeZero = submitWorkTypeZero;
    }

    public String getSubmitStatusZero() {
        return submitStatusZero;
    }

    public void setSubmitStatusZero(String submitStatusZero) {
        this.submitStatusZero = submitStatusZero;
    }

    public String getSubmitQueueZero() {
        return submitQueueZero;
    }

    public void setSubmitQueueZero(String submitQueueZero) {
        this.submitQueueZero = submitQueueZero;
    }

    public String getApprovePopUpWorkTypeZero() {
        return approvePopUpWorkTypeZero;
    }

    public void setApprovePopUpWorkTypeZero(String approvePopUpWorkTypeZero) {
        this.approvePopUpWorkTypeZero = approvePopUpWorkTypeZero;
    }

    public String getApprovePopUpStatusZero() {
        return approvePopUpStatusZero;
    }

    public void setApprovePopUpStatusZero(String approvePopUpStatusZero) {
        this.approvePopUpStatusZero = approvePopUpStatusZero;
    }

    public String getApprovePopUpQueueZero() {
        return approvePopUpQueueZero;
    }

    public void setApprovePopUpQueueZero(String approvePopUpQueueZero) {
        this.approvePopUpQueueZero = approvePopUpQueueZero;
    }

    public String getPaymentWorkItemsStatus() {
        return paymentWorkItemsStatus;
    }

    public void setPaymentWorkItemsStatus(String paymentWorkItemsStatus) {
        this.paymentWorkItemsStatus = paymentWorkItemsStatus;
    }

    public String getPaymentWorkItemsQueue() {
        return paymentWorkItemsQueue;
    }

    public void setPaymentWorkItemsQueue(String paymentWorkItemsQueue) {
        this.paymentWorkItemsQueue = paymentWorkItemsQueue;
    }

    public String getApprovePayment() {
        return approvePayment;
    }

    public void setApprovePayment(String approvePayment) {
        this.approvePayment = approvePayment;
    }

    public String getYesApprovePayment() {
        return yesApprovePayment;
    }

    public void setYesApprovePayment(String yesApprovePayment) {
        this.yesApprovePayment = yesApprovePayment;
    }

    public String getApproveWorkTypeZero() {
        return approveWorkTypeZero;
    }

    public void setApproveWorkTypeZero(String approveWorkTypeZero) {
        this.approveWorkTypeZero = approveWorkTypeZero;
    }

    public String getApproveStatusZero() {
        return approveStatusZero;
    }

    public void setApproveStatusZero(String approveStatusZero) {
        this.approveStatusZero = approveStatusZero;
    }

    public String getApproveQueueZero() {
        return approveQueueZero;
    }

    public void setApproveQueueZero(String approveQueueZero) {
        this.approveQueueZero = approveQueueZero;
    }

    @Override
    public String toString() {
        return "ClaimDetails{" +
                "policyHolderId='" + policyHolderId + '\'' +
                ", claimHandler='" + claimHandler + '\'' +
                ", event='" + event + '\'' +
                ", eventDescription='" + eventDescription + '\'' +
                ", eventDate='" + eventDate + '\'' +
                ", claimRegistrationDate='" + claimRegistrationDate + '\'' +
                ", claimant='" + claimant + '\'' +
                ", claimStatus='" + claimStatus + '\'' +
                ", claimType='" + claimType + '\'' +
                ", attorneyCompanyName='" + attorneyCompanyName + '\'' +
                ", attorneyCellNumber='" + attorneyCellNumber + '\'' +
                ", attorneyEmail='" + attorneyEmail + '\'' +
                ", attorneyType='" + attorneyType + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", cellNumber='" + cellNumber + '\'' +
                ", attorneyName='" + attorneyName + '\'' +
                ", withAttorney='" + withAttorney + '\'' +
                ", payee='" + payee + '\'' +
                ", submitPayment='" + submitPayment + '\'' +
                ", invoiceNumber='" + invoiceNumber + '\'' +
                ", invoiceDate='" + invoiceDate + '\'' +
                ", dateReceived='" + dateReceived + '\'' +
                ", invoiceDetailsServiceType='" + invoiceDetailsServiceType + '\'' +
                ", invoiceDetailsDescription='" + invoiceDetailsDescription + '\'' +
                ", invoiceDetailsAmount='" + invoiceDetailsAmount + '\'' +
                ", invoiceDetailsVAT='" + invoiceDetailsVAT + '\'' +
                ", totalAttorneyFee='" + totalAttorneyFee + '\'' +
                ", aolAmount='" + aolAmount + '\'' +
                ", paymentStage='" + paymentStage + '\'' +
                ", awaitDocDurationPerDay='" + awaitDocDurationPerDay + '\'' +
                ", awaitDocComments='" + awaitDocComments + '\'' +
                ", awaitDocReason='" + awaitDocReason + '\'' +
                ", awaitDocSubmit='" + awaitDocSubmit + '\'' +
                ", submitPopUpWorkTypeZero='" + submitPopUpWorkTypeZero + '\'' +
                ", submitPopUpStatusZero='" + submitPopUpStatusZero + '\'' +
                ", submitPopUpQueueZero='" + submitPopUpQueueZero + '\'' +
                ", submitWorkTypeZero='" + submitWorkTypeZero + '\'' +
                ", submitStatusZero='" + submitStatusZero + '\'' +
                ", submitQueueZero='" + submitQueueZero + '\'' +
                ", paymentWorkItemsStatus='" + paymentWorkItemsStatus + '\'' +
                ", paymentWorkItemsQueue='" + paymentWorkItemsQueue + '\'' +
                ", approvePayment='" + approvePayment + '\'' +
                ", yesApprovePayment='" + yesApprovePayment + '\'' +
                ", approvePopUpWorkTypeZero='" + approvePopUpWorkTypeZero + '\'' +
                ", approvePopUpStatusZero='" + approvePopUpStatusZero + '\'' +
                ", approvePopUpQueueZero='" + approvePopUpQueueZero + '\'' +
                ", approveWorkTypeZero='" + approveWorkTypeZero + '\'' +
                ", approveStatusZero='" + approveStatusZero + '\'' +
                ", approveQueueZero='" + approveQueueZero + '\'' +
                '}';
    }

}
