package co.za.fnb.flow.pages.policy_details.page_factory;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class BeneficiaryInformationPageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:benName")
    private WebElement name;

    @FindBy(id = "createSearchItemView:mainTabView:benContactNumber")
    private WebElement contactNumber;

    @FindBy(id = "createSearchItemView:mainTabView:benDateOfBirth_input")
    private WebElement dateOfBirth;

    @FindBy(id = "createSearchItemView:mainTabView:benEmailAddress")
    private WebElement emailAddress;

    @FindBy(id = "createSearchItemView:mainTabView:benIDNumber")
    private WebElement idNumber;

    public BeneficiaryInformationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getName() {
        return name;
    }

    public WebElement getContactNumber() {
        return contactNumber;
    }

    public WebElement getDateOfBirth() {
        return dateOfBirth;
    }

    public WebElement getEmailAddress() {
        return emailAddress;
    }

    public WebElement getIdNumber() {
        return idNumber;
    }
}
