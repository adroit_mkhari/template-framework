package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.PolicyDetailsLinkedPoliciesSavePageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class PolicyDetailsLinkedPoliciesSave extends BasePage {
    private Logger log  = LogManager.getLogger(PolicyDetailsLinkedPoliciesSave.class);
    PolicyDetailsLinkedPoliciesSavePageObjects policyDetailsLinkedPoliciesSavePageObjects = new PolicyDetailsLinkedPoliciesSavePageObjects(driver);

    public PolicyDetailsLinkedPoliciesSave(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public boolean isDisplayed() {
        return policyDetailsLinkedPoliciesSavePageObjects.getPolicyDetailsLinkedPoliciesSave().isDisplayed();
    }

    public void save() throws Exception {
        if (isDisplayed()) {
            log.info("Clicking save on extra popup.");
            click(policyDetailsLinkedPoliciesSavePageObjects.getPolicyDetailsLinkedPoliciesSave());
            Thread.sleep(2000);
        }
    }
}
