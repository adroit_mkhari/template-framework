package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class LoginPageObjects {
    @FindBy(xpath = "//*[contains(@id, 'username')]")
    private WebElement username;

    @FindBy(xpath = "//*[contains(@id, 'password')]")
    private WebElement password;

    @FindBy(xpath = "//*[contains(@class, 'ui-button ui-widget ui-state-default')]")
    private WebElement loginButton;

    @FindBy(linkText = "Logout")
    private WebElement logoutButton;

    @FindBy(xpath = "//*[contains(@class, 'popupContent)]")
    private WebElement loginFailure;

    @FindBy(id = "btnConfirmLogoutYes")
    private WebElement confirmLogout;

    @FindBy(linkText = "Administration")
    private WebElement logoutVerification;

    public LoginPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getUsername() {
        return username;
    }

    public WebElement getPassword() {
        return password;
    }

    public WebElement getLoginButton() {
        return loginButton;
    }

    public WebElement getLogoutButton() {
        return logoutButton;
    }

    public WebElement getLoginFailure() {
        return loginFailure;
    }

    public WebElement getConfirmLogout() {
        return confirmLogout;
    }

    public WebElement getLogoutVerification() {
        return logoutVerification;
    }
}
