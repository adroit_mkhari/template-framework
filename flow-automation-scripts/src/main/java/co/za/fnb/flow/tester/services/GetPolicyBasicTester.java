package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.GetPolicyBasicRequestInput;
import generated.GetPolicyBasicResponseOutput;
import generated.ObjectFactory;
import generated.ResponseErrors;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class GetPolicyBasicTester {
    private Logger log  = LogManager.getLogger(GetPolicyBasicTester.class);
    private GetPolicyBasicRequestInput getPolicyBasicRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private GetPolicyBasicResponseOutput getPolicyBasicResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public GetPolicyBasicTester(GetPolicyBasicRequestInput getPolicyBasicRequestInput) {
        this.getPolicyBasicRequestInput = getPolicyBasicRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(GetPolicyBasicRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(getPolicyBasicRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(GetPolicyBasicResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public GetPolicyBasicRequestInput getGetPolicyBasicRequestInput() {
        return getPolicyBasicRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public GetPolicyBasicResponseOutput getGetPolicyBasicResponseOutput() {
        return getPolicyBasicResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public GetPolicyBasicResponseOutput sendGetPolicyBasicRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvGETPOLICYBASICInput gensrvGETPOLICYBASICInput = serviceObjectFactory.createGensrvGETPOLICYBASICInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(500);
        pxmlin.setString(marshalResult);
        gensrvGETPOLICYBASICInput.setPXMLIN(pxmlin);
        GensrvGETPOLICYBASICResult gensrvGETPOLICYBASICResult = gsd00030PRServicesPort.gensrvGetpolicybasic(gensrvGETPOLICYBASICInput);
        PXMLOUT getPolicyBasicResultPXMLOUT = gensrvGETPOLICYBASICResult.getPXMLOUT();
        String pXmlOutString = getPolicyBasicResultPXMLOUT.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, GetPolicyBasicResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             getPolicyBasicResponseOutput = (GetPolicyBasicResponseOutput) unMarshal;
             return getPolicyBasicResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting GetPolicyBasicResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
