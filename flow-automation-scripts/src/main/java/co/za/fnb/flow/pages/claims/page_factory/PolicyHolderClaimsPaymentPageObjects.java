package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PolicyHolderClaimsPaymentPageObjects {

    // region Claim Payment Details
    @FindBy(id = "frmPolicyHolderClaimPayment:policyHolderClaimPaymentListTable_data")
    WebElement policyHolderClaimPaymentListTableData;

    String policyHolderClaimPaymentListTableDataRowXpathLocator = "//*[@id=\"frmPolicyHolderClaimPayment:policyHolderClaimPaymentListTable_data\"]/tr[@data-ri=\"0\"]/td[1]";
    String btnphApproveFailLocatorTemplateId = "frmPolicyHolderClaimPayment:policyHolderClaimPaymentListTable:0:btnphApproveFail";
    String btnpheditClaimPaymentTemplateId = "frmPolicyHolderClaimPayment:policyHolderClaimPaymentListTable:0:btnpheditClaimPayment";
    String btnphApproveViewTemplateId = "frmPolicyHolderClaimPayment:policyHolderClaimPaymentListTable:0:btnphApproveView";
    // endregion

    public PolicyHolderClaimsPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPolicyHolderClaimPaymentListTableData() {
        return policyHolderClaimPaymentListTableData;
    }

    public String getPolicyHolderClaimPaymentListTableDataRowXpathLocator() {
        return policyHolderClaimPaymentListTableDataRowXpathLocator;
    }

    public String getBtnphApproveFailLocatorTemplateId() {
        return btnphApproveFailLocatorTemplateId;
    }

    public String getBtnpheditClaimPaymentTemplateId() {
        return btnpheditClaimPaymentTemplateId;
    }

    public String getBtnphApproveViewTemplateId() {
        return btnphApproveViewTemplateId;
    }

}
