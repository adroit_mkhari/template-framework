package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsLoginPageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class TmsLoginPage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsLoginPage.class);
    private TmsLoginPageFactory tmsLoginPageFactory = new TmsLoginPageFactory(driver, scenarioOperator);

    public TmsLoginPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void maximizeWindow(int windowIndex) throws Exception {
        try {
            switchToAnfMaximizeServiceWindow(windowIndex);
        } catch (Exception e) {
            log.error("Error while inputting F Number.");
            throw new Exception("Error while inputting F Number.");
        }
    }

    public void inputFNumber(String fNumber) throws Exception {
        try {
            type(tmsLoginPageFactory.getfNumber(), fNumber);
        } catch (Exception e) {
            log.error("Error while inputting F Number.");
            throw new Exception("Error while inputting F Number.");
        }
    }

    public void inputPassword(String password) throws Exception {
        try {
            type(tmsLoginPageFactory.getPassword(), password);
        } catch (Exception e) {
            log.error("Error while inputting Password.");
            throw new Exception("Error while inputting Password.");
        }
    }

    public TmsHomePage clickSignIn() throws Exception {
        try {
            click(tmsLoginPageFactory.getLoginSubmit());
            return new TmsHomePage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking Sign In.");
            throw new Exception("Error while clicking Sign In.");
        }
    }

}
