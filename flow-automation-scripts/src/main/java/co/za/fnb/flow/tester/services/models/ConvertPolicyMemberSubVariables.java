package co.za.fnb.flow.tester.services.models;

/**
 *  Add An Array Of Role, with [] enclosing the Value separated by a comma for a single member:
 *  ["Role Id","Role Type", "Action","Full Name","Middle Name","Birth Date","New Cover Amount", "New Premium", "Comment"]
 */
public class ConvertPolicyMemberSubVariables {
    private String roleId;
    private String roleType;
    private String Action;
    private String fullName;
    private String middleName;
    private String birthDate;
    private String newCoverAmount;
    private String newPremium;
    private String comment;

    public ConvertPolicyMemberSubVariables() {
    }

    public ConvertPolicyMemberSubVariables(String roleId, String roleType, String action, String fullName, String middleName, String birthDate, String newCoverAmount, String newPremium, String comment) {
        this.roleId = roleId;
        this.roleType = roleType;
        Action = action;
        this.fullName = fullName;
        this.middleName = middleName;
        this.birthDate = birthDate;
        this.newCoverAmount = newCoverAmount;
        this.newPremium = newPremium;
        this.comment = comment;
    }

    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    public String getRoleType() {
        return roleType;
    }

    public void setRoleType(String roleType) {
        this.roleType = roleType;
    }

    public String getAction() {
        return Action;
    }

    public void setAction(String action) {
        Action = action;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getNewCoverAmount() {
        return newCoverAmount;
    }

    public void setNewCoverAmount(String newCoverAmount) {
        this.newCoverAmount = newCoverAmount;
    }

    public String getNewPremium() {
        return newPremium;
    }

    public void setNewPremium(String newPremium) {
        this.newPremium = newPremium;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

}
