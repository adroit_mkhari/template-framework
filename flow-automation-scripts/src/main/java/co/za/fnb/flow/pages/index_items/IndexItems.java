package co.za.fnb.flow.pages.index_items;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.index_items.page_factory.IndexItemsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IndexItems extends BasePage {
    private Logger log  = LogManager.getLogger(IndexItems.class);
    IndexItemsPageObjects indexItemsPageObjects = new IndexItemsPageObjects(driver);

    public IndexItems(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickClear() throws Exception {
        log.info("Clicking Clear.");
        try {
            Thread.sleep(2000);
            WebElement filter = indexItemsPageObjects.getClear();
            click(filter);
            Thread.sleep(2000);
        } catch (Exception e) {
            log.error("Error while clicking Clear. " + e.getMessage());
            throw new Exception("Error while clicking Clear. " + e.getMessage());
        }
    }

    public void inputEmailAddress(String emailAddress) throws Exception {
        try {
            WebElement emailField = indexItemsPageObjects.getEmailId();
            type(emailField, emailAddress);
        } catch (Exception e) {
            log.error("Error while inputting Email Address. " + e.getMessage());
            throw new Exception("Error while inputting Email Address. " + e.getMessage());
        }
    }

    public void inputSubject(String subject) throws Exception {
        try {
            WebElement subjectField = indexItemsPageObjects.getSubject();
            type(subjectField, subject);
        } catch (Exception e) {
            log.error("Error while inputting Subject. " + e.getMessage());
            throw new Exception("Error while inputting Subject. " + e.getMessage());
        }
    }

    public void inputFromDate(String date) throws Exception {
        try {
            WebElement fromDateInputField = indexItemsPageObjects.getFromDateInput();
            type(fromDateInputField, date);
        } catch (Exception e) {
            log.error("Error while inputting From Date. " + e.getMessage());
            throw new Exception("Error while inputting From Date. " + e.getMessage());
        }
    }

    public void inputToDate(String date) throws Exception {
        try {
            WebElement toDateInputField = indexItemsPageObjects.getToDateInput();
            type(toDateInputField, date);
        } catch (Exception e) {
            log.error("Error while inputting To Date. " + e.getMessage());
            throw new Exception("Error while inputting To Date. " + e.getMessage());
        }
    }

    public void clickFilter() throws Exception {
        try {
            WebElement filter = indexItemsPageObjects.getFilter();
            click(filter);
        } catch (Exception e) {
            log.error("Error while clicking Filter. " + e.getMessage());
            throw new Exception("Error while clicking Filter. " + e.getMessage());
        }
    }

    public int getItemsToIndexList() throws Exception {
        try {
            WebElement emailItemsTableData = indexItemsPageObjects.getEmailItemsTableData();
            int tableDataRiSize = getTableDataRiSize(emailItemsTableData);
            return tableDataRiSize;
        } catch (Exception e) {
            log.error("Error while getting Items To Index List. " + e.getMessage());
            throw new Exception("Error while getting Items To Index List. " + e.getMessage());
        }
    }

    public String getEmailItemsTableDataNoItems() throws Exception {
        try {
            WebElement emailItemsTableDataNoItemsField = indexItemsPageObjects.getEmailItemsTableDataNoItems();
            String emailItemsTableDataNoItems = getText(emailItemsTableDataNoItemsField);
            return emailItemsTableDataNoItems;
        } catch (Exception e) {
            log.error("Error while getting Email Items Table Data No Items.");
            throw new Exception("Error while getting Email Items Table Data No Items.");
        }
    }

    public void clickIndexDocumentHash(int rowIndex) throws Exception {
        try {
            String indexDocumentHashTemplateXpath = indexItemsPageObjects.getIndexDocumentHashTemplateXpath();
            //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumenthash"]
            String targetTextRegex = "emailItemsTable:0:indexDocumenthash";
            String matchPrefix = "emailItemsTable:";
            String matchSuffix = ":indexDocumenthash";
            String indexDocumentHashLocatorForIndex = getLocatorForIndex(indexDocumentHashTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentHashField = findXPath(indexDocumentHashLocatorForIndex);
            click(indexDocumentHashField);
        } catch (Exception e) {
            log.error("Error while clicking Index Document Hash. " + e.getMessage());
            throw new Exception("Error while clicking Index Document Hash. " + e.getMessage());
        }
    }

    public void doubleClickIndexDocumentHash(int rowIndex) throws Exception {
        try {
            String indexDocumentHashTemplateXpath = indexItemsPageObjects.getIndexDocumentHashTemplateXpath();
            //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumenthash"]
            String targetTextRegex = "emailItemsTable:0:indexDocumenthash";
            String matchPrefix = "emailItemsTable:";
            String matchSuffix = ":indexDocumenthash";
            String indexDocumentHashLocatorForIndex = getLocatorForIndex(indexDocumentHashTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentHashField = findXPath(indexDocumentHashLocatorForIndex);
            moveToElementAndDoubleClick(indexDocumentHashField);
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while clicking Index Document Hash. " + e.getMessage());
            throw new Exception("Error while clicking Index Document Hash. " + e.getMessage());
        }
    }

    public IndexDocuments switchToIndexDocumentsWindow() throws Exception {
        log.info("Switching to Index Documents Window.");
        try {
            switchToAnfMaximizeServiceWindow(1);
            return new IndexDocuments(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while switching to Index Documents Window.");
            throw new Exception("Error while switching to Index Documents Window.");
        }
    }

    public String getIndexDocumentDateAndTime(int rowIndex) throws Exception {
        try {
            String indexDocumentDateAndTimeTemplateXpath = indexItemsPageObjects.getIndexDocumentDateAndTimeTemplateXpath();
            //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentDateandTime"]
            String targetTextRegex = "emailItemsTable:0:indexDocumentDateandTime";
            String matchPrefix = "emailItemsTable:";
            String matchSuffix = ":indexDocumentDateandTime";
            String indexDocumentDateAndTimeLocatorForIndex = getLocatorForIndex(indexDocumentDateAndTimeTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentDateAndTimeField = findXPath(indexDocumentDateAndTimeLocatorForIndex);
            String indexDocumentDateAndTime = getText(indexDocumentDateAndTimeField);
            return indexDocumentDateAndTime;
        } catch (Exception e) {
            log.error("Error while getting Index Document Date And Time. " + e.getMessage());
            throw new Exception("Error while getting Index Document Date And Time. " + e.getMessage());
        }
    }

    public String getIndexDocumentEmailAddress(int rowIndex) throws Exception {
        try {
            String indexDocumentEmailAddressTemplateXpath = indexItemsPageObjects.getIndexDocumentEmailAddressTemplateXpath();
            //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentEmailAddress"]
            String targetTextRegex = "emailItemsTable:0:indexDocumentEmailAddress";
            String matchPrefix = "emailItemsTable:";
            String matchSuffix = ":indexDocumentEmailAddress";
            String indexDocumentEmailAddressLocatorForIndex = getLocatorForIndex(indexDocumentEmailAddressTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentEmailAddressField = findXPath(indexDocumentEmailAddressLocatorForIndex);
            String indexDocumentEmailAddress = getText(indexDocumentEmailAddressField);
            return indexDocumentEmailAddress;
        } catch (Exception e) {
            log.error("Error while getting Index Document Email Address. " + e.getMessage());
            throw new Exception("Error while getting Index Document Email Address. " + e.getMessage());
        }
    }

    public String getIndexDocumentSubject(int rowIndex) throws Exception {
        try {
            String indexDocumentSubjectTemplateXpath = indexItemsPageObjects.getIndexDocumentSubjectTemplateXpath();
            //*[@id="indexItemsForm:tabViewId:0:emailItemsTable:0:indexDocumentSubject"]
            String targetTextRegex = "emailItemsTable:0:indexDocumentSubject";
            String matchPrefix = "emailItemsTable:";
            String matchSuffix = ":indexDocumentSubject";
            String subjectLocatorForIndex = getLocatorForIndex(indexDocumentSubjectTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentSubjectField = findXPath(subjectLocatorForIndex);
            String indexDocumentSubject = getText(indexDocumentSubjectField);
            return indexDocumentSubject;
        } catch (Exception e) {
            log.error("Error while getting Index Document Subject. " + e.getMessage());
            throw new Exception("Error while getting Index Document Subject. " + e.getMessage());
        }
    }

    public void clickEmailItemsTableDataSelect(int rowIndex) throws Exception {
        try {
            String emailItemsTableDataSelectTemplateXpath = indexItemsPageObjects.getEmailItemsTableDataSelectTemplateXpath();
            // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[1]/td[5]/div/div[2]/span
            // //*[@id="indexItemsForm:tabViewId:0:emailItemsTable_data"]/tr[6]/td[5]/div/div[2]/span
            String targetTextRegex = "tr[1]/td";
            String matchPrefix = "tr[";
            String matchSuffix = "]/td";
            String indexDocumentHashLocatorForIndex = getLocatorForIndex(emailItemsTableDataSelectTemplateXpath, targetTextRegex, matchPrefix, matchSuffix, rowIndex);
            WebElement indexDocumentHashField = findXPath(indexDocumentHashLocatorForIndex);
            click(indexDocumentHashField);
        } catch (Exception e) {
            log.error("Error while clicking Email Items Table Data Select. " + e.getMessage());
            throw new Exception("Error while clicking Email Items Table Data Select. " + e.getMessage());
        }
    }

    public void clickIndexDocumentsSelectAll() throws Exception {
        try {
            WebElement indexDocumentSelectAll = indexItemsPageObjects.getIndexDocumentSelectAll();
            click(indexDocumentSelectAll);
        } catch (Exception e) {
            log.error("Error while clicking Index Document Select All. " + e.getMessage());
            throw new Exception("Error while clicking Index Document Select All. " + e.getMessage());
        }
    }

}
