package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.VerifyClientDialogPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.util.concurrent.TimeUnit.SECONDS;

public class VerifyClientDialog extends BasePage {
    private Logger log  = LogManager.getLogger(VerifyClientDialog.class);
    VerifyClientDialogPageObjects verifyClientDialogPageObjects = new VerifyClientDialogPageObjects(driver);
    Actions actions;
    private String firstPopup;
    private String secondPopup;

    public VerifyClientDialog(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        actions = new Actions(driver);
    }

    public VerifyClientDialog(WebDriver driver, ScenarioOperator scenarioOperator, String firstPopup, String secondPopup) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
        actions = new Actions(driver);
        this.firstPopup = firstPopup;
        this.secondPopup = secondPopup;
    }

    public void setFirstPopup(String firstPopup) {
        this.firstPopup = firstPopup;
    }

    public void setSecondPopup(String secondPopup) {
        this.secondPopup = secondPopup;
    }

    public Actions moveTo(WebElement webElement) throws Exception {
        try {
            return actions.moveToElement(webElement);
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickWrittenInstruction() throws Exception {
        try {
            verifyClientDialogPageObjects.getWrittenInstruction().click();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void moveToAndClickWrittenInstruction() throws Exception {
        moveTo(verifyClientDialogPageObjects.getWrittenInstruction()).click().build().perform();
    }

    public void clickFirstPopUpProceed() throws Exception {
        try {
            log.info("Clicking First Pop Up Proceed.");
            verifyClientDialogPageObjects.getInfoVerifiedFormVerifyBranchCallVerified().click();
        } catch (Exception e) {
            log.error("Error on Clicking First Pop Up Proceed: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickEmailAddressMatch() throws Exception {
        try {
            log.info("Clicking Email Address Match");
            WebElement emailAddressMatch = verifyClientDialogPageObjects.getEmailAddressMatch();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(emailAddressMatch));
            emailAddressMatch.click();
        } catch (Exception e) {
            log.error("Error On Clicking Email Address Match: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickEmailAddressNameMatch() throws Exception {
        try {
            log.info("Clicking Email Address Name Match");
            WebElement emailAddressNameMatch = verifyClientDialogPageObjects.getEmailAddressNameMatch();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(emailAddressNameMatch));
            emailAddressNameMatch.click();
        } catch (Exception e) {
            log.error("Error On Clicking Email Address Name Match: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickClientSignatureVerified() throws Exception {
        try {
            log.info("Clicking Client Signature Verified");
            WebElement clientSignatureVerified = verifyClientDialogPageObjects.getClientSignatureVerified();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(clientSignatureVerified));
            clientSignatureVerified.click();
        } catch (Exception e) {
            log.error("Error On Clicking Client Signature Verified: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickThirdPartyClientVerified() throws Exception {
        try {
            log.info("Clicking Third Party Client Verified");
            WebElement thirdPartyClientVerified = verifyClientDialogPageObjects.getThirdPartyClientVerified();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(thirdPartyClientVerified));
            thirdPartyClientVerified.click();
        } catch (Exception e) {
            log.error("Error On Clicking Third Party Client Verified: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickClientNotVerified() throws Exception {
        try {
            log.info("Clicking Client Not Verified");
            WebElement clientNotVerified = verifyClientDialogPageObjects.getClientNotVerified();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(clientNotVerified));
            clientNotVerified.click();
        } catch (Exception e) {
            log.error("Error On Clicking Client Not Verified: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void clickWrittenFormProceed() throws Exception {
        try {
            log.info("Clicking Written Form Proceed");
            WebElement verifyWritten = verifyClientDialogPageObjects.getVerifyWritten();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOf(verifyWritten));
            verifyWritten.click();
        } catch (Exception e) {
            log.error("Error On Clicking Written Form Proceed: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    public void handlePopUp() throws Exception {
        log.info("Handing Popup logic.");
        String firstPopUp = firstPopup;
        driver.manage().timeouts().pageLoadTimeout(1, SECONDS);
        driver.switchTo().activeElement();

        if (firstPopUp.contains("Written")) {
            handleWritten();
        } else if (firstPopUp.contains("Inbound / Outbound Call?")) {
            // TODO: Implement Logic
            // handleInboundCall();
        } else if (firstPopUp.contains("Call from a Branch / Banker?")) {
            // TODO: Implement Logic
            // handleCallFromBranchOrBanker();
        }
        log.info("Done Handing Popup logic.");
    }

    public void selectWrittenInstruction() throws Exception {
        try {
            log.info("Clicking Written Instruction");
            WebElement writtenInstruction = verifyClientDialogPageObjects.getWrittenInstruction();
            click(writtenInstruction);
        } catch (Exception e) {
            log.error("Error On Clicking Written Instruction: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception(e.getMessage());
        }
    }

    private void handleWritten() throws Exception {
        try {
            log.info("Handling Written:");
            String popupDetails = secondPopup;
            String[] detailList = popupDetails.split(",");
            Thread.sleep(500);
            log.info("Moving to and clicking: verifyClientDialogPageObjects.getWrittenInstruction()");
            selectWrittenInstruction();
            Thread.sleep(3000);
            clickFirstPopUpProceed();
            Thread.sleep(2000);

            for (String detail : detailList) {
                Thread.sleep(500);
                if (detail.contains("Email address received from matches the email address on record?")) {
                    if (!detail.contains("(No)")) {
                        clickEmailAddressMatch();
                        Thread.sleep(500);
                    } else {
                        log.info("Moving To And Clicking: verifyClientDialogPageObjects.getEmailAddressMatch()");
                        moveTo(verifyClientDialogPageObjects.getEmailAddressMatch()).doubleClick().build().perform();
                        Thread.sleep(500);
                        moveTo(verifyClientDialogPageObjects.getEmailAddressMatch()).doubleClick().build().perform();
                        Thread.sleep(500);
                    }
                }

                Thread.sleep(500);
                if (detail.contains("Clients signature can be verified (attach verification source)?")) {
                    if (!detail.contains("(No)")) {
                        clickClientSignatureVerified();
                        Thread.sleep(500);
                    } else {
                        log.info("Moving To And Clicking: " + verifyClientDialogPageObjects.getClientSignatureVerified());
                        moveTo(verifyClientDialogPageObjects.getClientSignatureVerified()).doubleClick().build().perform();
                        moveTo(verifyClientDialogPageObjects.getClientSignatureVerified()).doubleClick().build().perform();
                        Thread.sleep(500);
                    }
                }

                Thread.sleep(500);
                if (detail.contains("Email address is in the name of the client?")) {
                    if (!detail.contains("(No)")) {
                        clickEmailAddressNameMatch();
                        Thread.sleep(500);
                    } else {
                        log.info("Moving To And Clicking: verifyClientDialogPageObjects.getEmailAddressNameMatch()");
                        moveTo(verifyClientDialogPageObjects.getEmailAddressNameMatch()).doubleClick().build().perform();
                        moveTo(verifyClientDialogPageObjects.getEmailAddressNameMatch()).doubleClick().build().perform();
                        Thread.sleep(500);
                    }
                }

                Thread.sleep(500);
                if (detail.contains("3rd party client signature verified for authority (attach verification source)?")) {
                    if (!detail.contains("(No)")) {
                        clickThirdPartyClientVerified();
                        Thread.sleep(500);
                    } else {
                        log.info("Moving To And Clicking: verifyClientDialogPageObjects.getThirdPartyClientVerified()");
                        moveTo(verifyClientDialogPageObjects.getThirdPartyClientVerified()).doubleClick().build().perform();
                        moveTo(verifyClientDialogPageObjects.getThirdPartyClientVerified()).doubleClick().build().perform();
                        Thread.sleep(500);
                    }
                }

                Thread.sleep(500);
                if (detail.contains("Client not verified?")) {
                    if (!detail.contains("(No)")) {
                        clickClientNotVerified();
                        Thread.sleep(500);
                    } else {
                        log.info("Moving To And Clicking: verifyClientDialogPageObjects.getClientNotVerified()");
                        moveTo(verifyClientDialogPageObjects.getClientNotVerified()).doubleClick().build().perform();
                        moveTo(verifyClientDialogPageObjects.getClientNotVerified()).doubleClick().build().perform();
                        Thread.sleep(500);
                    }
                }
            }
            clickWrittenFormProceed();
            Thread.sleep(1000);
        } catch (Exception e) {
            takeScreenShot("");
            // log.error("Error on popUp handleWritten: " + e.getMessage());
            e.printStackTrace();
            // TODO: Implement Error Handling Logic.
            throw new Exception("Error while handling popup logic.");
        }
    }

}
