package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClaimsPageObjects {

    // region Cover Details
    @FindBy(id = "createSearchItemView:mainTabView:totalCoverAmount")
    WebElement totalCoverAmount; // Lifetime Cover

    @FindBy(id = "createSearchItemView:mainTabView:outstandingCoverAmount")
    WebElement outstandingCoverAmount; // Remaining Lifetime Cover Limit
    // endregion

    // region Claim Details
    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:claimnumber:filter")
    WebElement claimReferenceNumberFilter;

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:eventdate:filter")
    WebElement eventDateFilter;

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:description:filter")
    WebElement descriptionFilter; // Event Type

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:datereg:filter")
    WebElement dateRegisteredFilter;

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:claimant:filter")
    WebElement claimantFilter;

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:approved:filter")
    WebElement approvedFilter; // Claim Status

    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable:statusid:filter")
    WebElement statusidFilter; // Claim Amount

    @FindBy(id ="createSearchItemView:mainTabView:claimsInformationTable:caselimit:filter")
    WebElement caseLimitFilter;
    // endregion

    // region Create New Claim
    @FindBy(id = "createSearchItemView:mainTabView:Create")
    WebElement create;
    // endregion

    public ClaimsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getTotalCoverAmount() {
        return totalCoverAmount;
    }

    public WebElement getOutstandingCoverAmount() {
        return outstandingCoverAmount;
    }

    public WebElement getClaimReferenceNumberFilter() {
        return claimReferenceNumberFilter;
    }

    public WebElement getEventDateFilter() {
        return eventDateFilter;
    }

    public WebElement getDescriptionFilter() {
        return descriptionFilter;
    }

    public WebElement getDateRegisteredFilter() {
        return dateRegisteredFilter;
    }

    public WebElement getClaimantFilter() {
        return claimantFilter;
    }

    public WebElement getApprovedFilter() {
        return approvedFilter;
    }

    public WebElement getStatusidFilter() {
        return statusidFilter;
    }

    public WebElement getCaseLimitFilter() {
        return caseLimitFilter;
    }

    public WebElement getCreate() {
        return create;
    }
}
