package co.za.fnb.flow.pages.tms;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.tms.page_factory.TmsHomePageFactory;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class TmsHomePage extends BasePage {
    private Logger log  = LogManager.getLogger(TmsHomePage.class);
    private TmsHomePageFactory tmsHomePageFactory = new TmsHomePageFactory(driver, scenarioOperator);

    public TmsHomePage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public TmsDistributionPage clickQADistribution() throws Exception {
        try {
            click(tmsHomePageFactory.getqADistribution());
            return new TmsDistributionPage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking QA Distribution.");
            throw new Exception("Error while clicking QA Distribution.");
        }
    }

    public void clickUserMenu() throws Exception {
        try {
            click(tmsHomePageFactory.getUserMenu());
        } catch (Exception e) {
            log.error("Error while clicking User Menu.");
            throw new Exception("Error while clicking User Menu.");
        }
    }

    public void clickHelp() throws Exception {
        try {
            click(tmsHomePageFactory.getHelp());
        } catch (Exception e) {
            log.error("Error while clicking Help.");
            throw new Exception("Error while clicking Help.");
        }
    }

    public void clickWorkQueue() throws Exception {
        try {
            click(tmsHomePageFactory.getWorkQueue());
        } catch (Exception e) {
            log.error("Error while clicking Work Queue.");
            throw new Exception("Error while clicking Work Queue.");
        }
    }

    public void clickLogout() throws Exception {
        try {
            click(tmsHomePageFactory.getWorkQueue());
        } catch (Exception e) {
            log.error("Error while clicking Logout.");
            throw new Exception("Error while clicking Logout.");
        }
    }

}
