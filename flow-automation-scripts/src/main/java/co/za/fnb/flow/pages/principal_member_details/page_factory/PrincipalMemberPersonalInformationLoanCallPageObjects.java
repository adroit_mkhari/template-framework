package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPersonalInformationLoanCallPageObjects extends PrincipalMemberPersonalInformationPageObjects {

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:idNumberCompany')]")
    private WebElement idNumberCompany;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:fnumber')]")
    private WebElement fnumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//input[contains (@id, 'createSearchItemView:mainTabView:vatNumber')]")
    private WebElement vatNumber;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:personalInformationPanel_content')]//table//tbody//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:gender')]//div[contains (@class, 'ui-selectonemenu-trigger')]")
    private WebElement genderSelect;

    @FindBy(xpath = "//*//div[contains (@id, 'createSearchItemView:mainTabView:gender_panel')]//div//ul[contains (@id, 'createSearchItemView:mainTabView:gender_items')]")
    private WebElement genderSelectItems;

    public PrincipalMemberPersonalInformationLoanCallPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getIdNumberCompany() {
        return idNumberCompany;
    }

    public WebElement getFnumber() {
        return fnumber;
    }

    public WebElement getVatNumber() {
        return vatNumber;
    }

    @Override
    public WebElement getGenderSelect() {
        return genderSelect;
    }

    @Override
    public WebElement getGenderSelectItems() {
        return genderSelectItems;
    }
}
