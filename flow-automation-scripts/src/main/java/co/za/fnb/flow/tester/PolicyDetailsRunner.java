package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.Query;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.models.policy_details.*;
import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.claims.Claims;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.generic_payment.GenericPayment;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.tester.administration.AttorneyManagement;
import co.za.fnb.flow.tester.policy_details.*;
import co.za.fnb.flow.tester.services.functions.Takeup;
import co.za.fnb.flow.tester.services.models.AuthorizedPerson;
import co.za.fnb.flow.tester.services.models.TakeupScenario;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import generated.PolicyTakeUpResponse;
import generated.PolicyTakeUpResponsePayload;
import generated.PolicyTakeUpResponsePolicyData;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.setup.TestResultReportFlag;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Properties;

public class PolicyDetailsRunner {
    private Logger log  = LogManager.getLogger(PolicyDetailsRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;
    HomePage homePage;
    PolicyDetails policyDetails;
    PrincipalMemberDetails principalMemberDetails;
    Claims claims;
    GenericMaintenance genericMaintenance;
    CreateSearchItemPage createSearchItemPage;
    LegalAdvisorAdminPage legalAdvisorAdminPage;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    CreateOrUpdateWorkItem createOrUpdateWorkItem;
    WorkItems workItems;
    AuditTrail auditTrail;

    public PolicyDetailsRunner() {
    }

    public PolicyDetailsRunner(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetailsRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public PolicyDetailsRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        String productName = scenarioOperator.getProductName();
        if (!productName.toUpperCase().contains("LOC")) {
            queryHandler.insertFicaRecord(
                    productName,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber, takeupFirst, takeupCode;

        try {
            if (Arrays.asList(headers).contains("Test Case No")) {
                for (Row row : dataTable.getGherkinRows()) {
                    if (row.getLine() != 1) {
                        dataEntry = row.getCells();
                        run = getCellValue(headers, dataEntry,"Run");
                        if (run.equalsIgnoreCase("YES")) {
                            testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                            scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                            testFunction = getCellValue(headers, dataEntry,"Function");
                            productName = getCellValue(headers, dataEntry,"Product Name");
                            scenarioOperator.setTestCaseNumber(testCaseNumber);
                            scenarioOperator.setScenarioDescription(scenarioDescription);
                            scenarioOperator.setFunction(testFunction);
                            scenarioOperator.setProductName(productName);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                            scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                            // BasePage.setScenarioOperator(scenarioOperator);

                            try {
                                takeupFirst = getCellValue(headers, dataEntry,"Takeup");
                                takeupCode = getCellValue(headers, dataEntry,"Takeup Code");
                                policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                                policyNumberQuery = getCellValue(headers, dataEntry,"Policy Number Query");
                                policyCode = getCellValue(headers, dataEntry,"Policy No");

                                if (takeupFirst.equalsIgnoreCase("YES") && !takeupCode.isEmpty()) {
                                    TakeupScenario takeupScenario = TakeupScenarioFactory.getTakeupScenario(takeupCode);
                                    ScenarioTester scenarioTester = new ScenarioTester(TestComponent.LETTERS, dataTable, driver);
                                    Takeup takeup = new Takeup(scenarioTester, takeupScenario);

                                    try {
                                        takeup.setupPolicyTakeUpRequestInputFromTakeupScenario();
                                        PolicyTakeUpResponse policyTakeUpResponse = takeup.sendRequest();
                                        PolicyTakeUpResponsePayload response = policyTakeUpResponse.getResponse();
                                        PolicyTakeUpResponsePolicyData policyData = response.getPolicyData();
                                        policyNumber = policyData.getPolicyNo();
                                        log.info("Successfully Created Policy");
                                        log.info("------------------------------");
                                        log.info("Policy Number: " + policyNumber);
                                        log.info("------------------------------");

                                        if (productName.equalsIgnoreCase("BUSINESS")) {
                                            if (takeupScenario !=null ){
                                                AuthorizedPerson authorizedPerson = takeupScenario.getAuthorizedPerson();
                                                String birthDate = authorizedPerson.getBirthDate();
                                                String gender = authorizedPerson.getGender();
                                                queryHandler.updateDOBPrincipalMember(policyNumber,birthDate,gender);
                                                Thread.sleep(100);
                                            }
                                        }

                                        if (productName.equalsIgnoreCase("Accidental Death") || productName.equalsIgnoreCase("Personal Accident")) {
                                            String databasePolicyNumber = policyNumber.replaceFirst("CP", "");
                                            queryHandler.setDbPolicyNumber(databasePolicyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, databasePolicyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        } else {
                                            queryHandler.setDbPolicyNumber(policyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        }
                                        log.info("Product Name: " + productName + ", Database Policy Number: " + queryHandler.getDbPolicyNumber()) ;
                                    } catch (Exception e) {
                                        log.error("Error while taking up policy. " + e.getMessage());
                                        throw new Exception("Error while taking up policy. " + e.getMessage());
                                    }
                                } else {
                                    if (policyNumber.isEmpty()) {
                                        try {
                                            if (policyNumberQuery.isEmpty()) {
                                                if (policyCode.isEmpty()) {
                                                    policyNumber = "No Policy Number";
                                                    scenarioOperator.setPolicyNumber(policyNumber);
                                                } else {
                                                    Query query = QueryFactory.getQuery(policyCode);
                                                    ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(query.getQuery());
                                                    if (policyNumberQueryResults.next()) {
                                                        policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                    } else {
                                                        policyCode = query.getPolicyCode();
                                                        policyNumberQuery = query.getQuery();
                                                        policyNumber = null;
                                                    }

                                                    if (policyNumber == null) {
                                                        throw new Exception("Error getting policy number for policy code: " + policyCode);
                                                    }
                                                    queryHandler.setDbPolicyNumber(policyNumber);
                                                    policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                    scenarioOperator.setPolicyNumber(policyNumber);
                                                }
                                            } else {
                                                ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(policyNumberQuery);
                                                if (policyNumberQueryResults.next()) {
                                                    policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                                    queryHandler.setDbPolicyNumber(policyNumber);
                                                    policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                                    scenarioOperator.setPolicyNumber(policyNumber);
                                                }
                                            }
                                        } catch (Exception e) {
                                            throw new Exception("Error getting policy number. Policy Code: " + policyCode + " Query: " + policyNumberQuery);
                                        }
                                    } else {
                                        scenarioOperator.setPolicyNumber(policyNumber);
                                        queryHandler.setDbPolicyNumber(policyNumber);
                                    }
                                }

                                log.info("===========================================================================");
                                log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                                log.info("---------------------------------------------------------------------------");

                                if (testFunction.equalsIgnoreCase("Search Policy")) {
                                    searchPolicy(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Premium Status")) {
                                    changePremiumStatus(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Escalate Member")) {
                                    escalateMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Update Principal Member")) {
                                    updatePrincipalMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Add Member")) {
                                    addMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Quote Member")) {
                                    quoteMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Delete Member")) {
                                    deleteMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Cancel Policy")) {
                                    cancelPolicy(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Edit Member")) {
                                    editMember(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Reinstate Policy")) {
                                    reinstatePolicy(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Claims")) {
                                    try {
                                        claims(reportableFields, scenarioOperator, headers, dataEntry);
                                        scenarioOperator.increamentReportRowIndex();
                                    } catch (Exception e) {
                                        log.error("Error On Claims: " + e.getMessage());
                                        throw new Exception(e);
                                    } finally {
                                        int numberOfWindows = driver.getWindowHandles().toArray().length;
                                        while (numberOfWindows > 1) {
                                            Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows -1];
                                            WebDriver window = driver.switchTo().window((String) currentWindow);
                                            window.close();
                                            numberOfWindows = driver.getWindowHandles().toArray().length;
                                        }
                                    }
                                } else if (testFunction.equalsIgnoreCase("Refunds")) {
                                    refunds(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                } else if (testFunction.equalsIgnoreCase("Attorney Management")) {
                                    attorneyManagement(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                }
                                log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            } catch (Exception e) {
                                log.error(e.getMessage());
                                e.printStackTrace();
                                reportFailure(reportableFields, scenarioOperator, e.getMessage());
                                scenarioOperator.increamentReportRowIndex();
                            } finally {
                                if (driver.getWindowHandles().toArray().length > 1) {
                                    int numberOfWindows = driver.getWindowHandles().toArray().length;
                                    while (numberOfWindows > 1) {
                                        Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows - 1];
                                        WebDriver window = driver.switchTo().window((String) currentWindow);
                                        window.close();
                                        numberOfWindows = driver.getWindowHandles().toArray().length;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                headers = new String[]{"Product Name", "Policy Number", "Action", "Debit Order Date", "Next Due Date", "Waiting Period", "Areas"};
                for (Row row : dataTable.getGherkinRows()) {
                    dataEntry = row.getCells();

                    testCaseNumber = String.valueOf(row.getLine());
                    scenarioDescription = "Check Debit Date";
                    testFunction = "Check Debit Date";
                    productName = getCellValue(headers, dataEntry,"Product Name");
                    scenarioOperator.setTestCaseNumber(testCaseNumber);
                    scenarioOperator.setScenarioDescription(scenarioDescription);
                    scenarioOperator.setFunction(testFunction);
                    scenarioOperator.setProductName(productName);
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                    scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                    // BasePage.setScenarioOperator(scenarioOperator);

                    try {

                        log.info("===========================================================================");
                        log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                        log.info("---------------------------------------------------------------------------");

                        checkDebitDate(reportableFields, scenarioOperator, headers, dataEntry);
                        scenarioOperator.increamentReportRowIndex();

                        log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        e.printStackTrace();
                        reportFailure(reportableFields, scenarioOperator, e.getMessage());
                        scenarioOperator.increamentReportRowIndex();
                    } finally {
                        driver.close();
                    }
                }
            }
        } catch (Exception e) {
            // TODO: Handle Whole Test Exception
        } finally {
            log.info("Saving Report.");
            scenarioOperator.saveReport();
        }
        log.info("===========================================================================");
    }

    private void searchPolicy(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Search Policy Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        Member member = new Member(
                // region Add Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        AddMember addMember = new AddMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);
            Thread.sleep(3000);
            String policyHolder = createSearchItemPage.getPolicyHolder();

            if (!policyHolder.isEmpty()) {
                reportSuccess(reportableFields, scenarioOperator);
            } else {
                reportFailure(reportableFields, scenarioOperator, "Policy Not Found.");
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Search Policy Logic");
    }

    private void addMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Add Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        Member member = new Member(
                // region Add Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        AddMember addMember = new AddMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            addMember.setExpectedResults();
            log.info("Adding Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            addMember.add();
            log.info("Saving Added Member(s).");
            addMember.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = addMember.getTestHandle();
            errorHandle = addMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                addMember.validate();
                if (!addMember.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, addMember.getComment());
                } else {
                    createMultipleMintItemtable = addMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    Thread.sleep(3000);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        Thread.sleep(3000);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Add Member Logic");
    }

    private void editMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Edit Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        EditMember editMember = new EditMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            editMember.setExpectedResults();
            log.info("Edit Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            editMember.edit();
            log.info("Saving Edited Member(s).");
            editMember.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = editMember.getTestHandle();
            errorHandle = editMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                editMember.validate();
                if (!editMember.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, editMember.getComment());
                } else {
                    createMultipleMintItemtable = editMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Edit Member Logic");
    }

    private void deleteMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Delete Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        String deletionComment = getCellValue(headers, dataEntry,"Member Deletion Comment");
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        DeleteMember deleteMember = new DeleteMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            deleteMember.setExpectedResults();
            log.info("Deleting Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            deleteMember.delete(deletionComment);
            log.info("Saving Delete Member(s).");
            deleteMember.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = deleteMember.getTestHandle();
            errorHandle = deleteMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                deleteMember.validate();
                if (!deleteMember.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, deleteMember.getComment());
                } else {
                    createMultipleMintItemtable = deleteMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Delete Member Logic");
    }

    private void cancelPolicy(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Delete Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        String confirmCancelReason = getCellValue(headers, dataEntry,"Confirm Cancel Reason");
        String otherConfirmCancelReason = getCellValue(headers, dataEntry,"Other");
        String voiceRecordingReason = getCellValue(headers, dataEntry,"Voice recording reason");
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        CancelPolicy cancelPolicy = new CancelPolicy(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            cancelPolicy.setExpectedResults();
            log.info("Deleting Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            cancelPolicy.cancel(confirmCancelReason, otherConfirmCancelReason, voiceRecordingReason);
            log.info("Saving Delete Member(s).");
            cancelPolicy.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = cancelPolicy.getTestHandle();
            errorHandle = cancelPolicy.getErrorHandle();
            if (testHandle.isSuccess()) {
                cancelPolicy.validate();
                if (!cancelPolicy.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, cancelPolicy.getComment());
                } else {
                    createMultipleMintItemtable = cancelPolicy.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Delete Member Logic");
    }

    private void escalateMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Escalate Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        String escalationAction = getCellValue(headers, dataEntry,"Escalation Action");
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        EscalateMember escalateMember = new EscalateMember(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            escalateMember.setExpectedResults();
            log.info("Escalating Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            escalateMember.escalate(dbPolicyNumber, escalationAction);
            log.info("Saving Escalated Member(s).");
            escalateMember.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = escalateMember.getTestHandle();
            errorHandle = escalateMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                escalateMember.validate(dbPolicyNumber, escalationAction);
                if (!escalateMember.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, escalateMember.getComment());
                } else {
                    createMultipleMintItemtable = escalateMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Escalate Member Logic");
    }

    private void reinstatePolicy(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Delete Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        String reinstate = getCellValue(headers, dataEntry,"Want to reinstate the policy?");
        String confirmWithArrears = getCellValue(headers, dataEntry,"Reinstate Policy With Arrears Confirmation");
        String reinstatePolicyConfirmation = getCellValue(headers, dataEntry,"Reinstate Policy Confirmation");
        String acceptTerms = getCellValue(headers, dataEntry,"Accept Terms");
        String confirmArrears = getCellValue(headers, dataEntry,"Confirm Arrears");
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        ReinstatePolicy reinstatePolicy = new ReinstatePolicy(driver, member, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            reinstatePolicy.setExpectedResults();
            log.info("Deleting Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            reinstatePolicy.reinstate(reinstate, confirmWithArrears, reinstatePolicyConfirmation, acceptTerms, confirmArrears);
            log.info("Saving Delete Member(s).");
            reinstatePolicy.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = reinstatePolicy.getTestHandle();
            errorHandle = reinstatePolicy.getErrorHandle();
            if (testHandle.isSuccess()) {
                reinstatePolicy.validate();
                if (!reinstatePolicy.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, reinstatePolicy.getComment());
                } else {
                    createMultipleMintItemtable = reinstatePolicy.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Delete Member Logic");
    }

    private void quoteMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Quote Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        Member member = new Member(
                // region Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"Work Type1"),
                getCellValue(headers, dataEntry,"Status1"),
                getCellValue(headers, dataEntry,"Queue1"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type1"),
                getCellValue(headers, dataEntry,"POP UP Status1"),
                getCellValue(headers, dataEntry,"POP UP Queue1"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Relationship"),
                getCellValue(headers, dataEntry,"Company Name / Full Name"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number / ID Number"),
                getCellValue(headers, dataEntry,"DOB"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Cover Amount"),
                getCellValue(headers, dataEntry,"Premium Amount"),
                getCellValue(headers, dataEntry,"Discount"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"Bank Name"),
                String.valueOf(numberOfMembers),
                getCellValue(headers, dataEntry,"Beneficiary Name"),
                getCellValue(headers, dataEntry,"Beneficiary DOB"),
                getCellValue(headers, dataEntry,"Beneficiary ID Number"),
                getCellValue(headers, dataEntry,"Beneficiary Contact Number"),
                getCellValue(headers, dataEntry,"Beneficiary Email Address"),
                getCellValue(headers, dataEntry, "Family Bundle").equalsIgnoreCase("Yes")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, member.getFirstPopup(), member.getSecondPopup());
        QuoteMember quoteMember = new QuoteMember(driver, member, scenarioOperator);
        String newMember = getCellValue(headers, dataEntry,"New Member").trim();
        String discount = getCellValue(headers, dataEntry,"Discount");
        String expectedPremiumAmount = getCellValue(headers, dataEntry,"Premium Amount");
        String expectedQuotedPremiumAmount = getCellValue(headers, dataEntry,"Expected Quoted Premium Amount");

        boolean isNewMember = newMember.equalsIgnoreCase("Yes");
        discount = getDoubleString(discount);
        Double discountValue = stringToDouble(discount);
        expectedPremiumAmount = getDoubleString(expectedPremiumAmount);
        Double expectedPremiumAmountValue = stringToDouble(expectedPremiumAmount);
        expectedQuotedPremiumAmount = getDoubleString(expectedQuotedPremiumAmount);
        Double expectedQuotedPremiumAmountValue = stringToDouble(expectedQuotedPremiumAmount);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, member.getRisk(), member.getSanction(), member.getEdd(), member.getKyc(), member.getStatus(), member.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            quoteMember.setExpectedResults();
            log.info("Adding Member, Policy Number: " + scenarioOperator.getPolicyNumber());
            if (isNewMember) {
                quoteMember.quoteNewMember(discountValue, expectedPremiumAmountValue, expectedQuotedPremiumAmountValue);
            } else {
                quoteMember.quoteExistingMember(discountValue, expectedPremiumAmountValue, expectedQuotedPremiumAmountValue);
            }
            log.info("Saving Added Member(s).");
            quoteMember.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = quoteMember.getTestHandle();
            errorHandle = quoteMember.getErrorHandle();
            if (testHandle.isSuccess()) {
                quoteMember.validate();
                if (!quoteMember.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, quoteMember.getComment());
                } else {
                    createMultipleMintItemtable = quoteMember.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(member);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = member.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Quote Member Logic");
    }

    private void changePremiumStatus(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Change Premium Status Logic");
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        PremiumStatus premiumStatus = new PremiumStatus(
                // region Premium Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getCellValue(headers, dataEntry,"Next Due Date"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Premium Status")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(premiumStatus.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, premiumStatus.getFirstPopup(), premiumStatus.getSecondPopup());
        ChangePremiumStatus changePremiumStatus = new ChangePremiumStatus(driver, premiumStatus, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, premiumStatus.getRisk(), premiumStatus.getSanction(), premiumStatus.getEdd(), premiumStatus.getKyc(), premiumStatus.getStatus(), premiumStatus.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            changePremiumStatus.setExpectedResults();
            changePremiumStatus.change();
            changePremiumStatus.save();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = changePremiumStatus.getTestHandle();
            errorHandle = changePremiumStatus.getErrorHandle();
            if (testHandle.isSuccess()) {
                changePremiumStatus.validate();
                if (!changePremiumStatus.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, changePremiumStatus.getComment());
                } else {
                    createMultipleMintItemtable = changePremiumStatus.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(premiumStatus);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = premiumStatus.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Change Premium Status Logic");
    }

    private void updatePrincipalMember(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Update Principal Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        String escalationAction = getCellValue(headers, dataEntry,"Escalation Action");
        PrincipalMember principalMember = new PrincipalMember(
                // region Principal Member Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Principal Member Information To Update"),
                getCellValue(headers, dataEntry,"Policy Holder"),
                getCellValue(headers, dataEntry,"Trading as Name/ Middle Name"),
                getCellValue(headers, dataEntry,"Company Reg Number/ ID Number"),
                getCellValue(headers, dataEntry,"Vat Reg Number"),
                getCellValue(headers, dataEntry,"Type Of ID"),
                getCellValue(headers, dataEntry,"Income"),
                getCellValue(headers, dataEntry,"F Number"),
                getCellValue(headers, dataEntry,"Expected Premium Amount Value"),
                getCellValue(headers, dataEntry,"Date of Birth"),
                getCellValue(headers, dataEntry,"Gender"),
                getCellValue(headers, dataEntry,"Correspondence Language"),
                getCellValue(headers, dataEntry,"Marital Status"),
                getCellValue(headers, dataEntry,"Residential Street/Building Line 1"),
                getCellValue(headers, dataEntry,"Residential Street/Building Line 2"),
                getCellValue(headers, dataEntry,"Residential Suburb"),
                getCellValue(headers, dataEntry,"Residential City"),
                getCellValue(headers, dataEntry,"Residential Postal Code"),
                getCellValue(headers, dataEntry,"Preferred Contact Method"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"CIS Email Address"),
                getCellValue(headers, dataEntry,"In Contact Email Address"),
                getCellValue(headers, dataEntry,"Cell Phone Number"),
                getCellValue(headers, dataEntry,"CIS Cell Phone Number"),
                getCellValue(headers, dataEntry,"In Contact Cell Phone Number"),
                getCellValue(headers, dataEntry,"Work Phone Number"),
                getCellValue(headers, dataEntry,"Home Phone Number"),
                getCellValue(headers, dataEntry,"Postal Same as Residential Address 1"),
                getCellValue(headers, dataEntry,"Postal Street/Building Line 1"),
                getCellValue(headers, dataEntry,"Postal Street/Building Line 2"),
                getCellValue(headers, dataEntry,"Postal Suburb"),
                getCellValue(headers, dataEntry,"Postal City"),
                getCellValue(headers, dataEntry,"Postal Postal Code"),
                getCellValue(headers, dataEntry,"Postal Postal Code Search"),
                getCellValue(headers, dataEntry,"Postal Postal Code Search Location"),
                getCellValue(headers, dataEntry,"Postal Postal Code Search Code"),
                getCellValue(headers, dataEntry,"Postal Same as Residential Address 2"),
                getCellValue(headers, dataEntry,"Postal CIS Street/Building Line 1"),
                getCellValue(headers, dataEntry,"Postal CIS Street/Building Line 2"),
                getCellValue(headers, dataEntry,"Postal CIS Postal City"),
                getCellValue(headers, dataEntry,"Postal CIS Postal Code"),
                getCellValue(headers, dataEntry,"Postal CIS Postal Code Search"),
                getCellValue(headers, dataEntry,"Postal CIS Postal Code Search Location"),
                getCellValue(headers, dataEntry,"Postal CIS Postal Code Search Code"),
                getCellValue(headers, dataEntry,"Postal CIS Postal State"),
                getCellValue(headers, dataEntry,"Postal CIS Postal Country"),
                getCellValue(headers, dataEntry,"Postal CIS Zip Code"),
                getCellValue(headers, dataEntry,"Postal CIS Zip Code Search"),
                getCellValue(headers, dataEntry,"Postal CIS Zip Code Search Location"),
                getCellValue(headers, dataEntry,"Postal CIS Zip Code Search Code")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(principalMember.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, principalMember.getFirstPopup(), principalMember.getSecondPopup());
        UpdatePrincipalMemberDetails updatePrincipalMemberDetails = new UpdatePrincipalMemberDetails(driver, principalMember, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, principalMember.getRisk(), principalMember.getSanction(), principalMember.getEdd(), principalMember.getKyc(), principalMember.getStatus(), principalMember.getUpdatedTe());
                // endregion
            }

            principalMemberDetails = createSearchItemPage.openPrincipalMemberDetails(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            updatePrincipalMemberDetails.setExpectedResults();
            log.info("Updating Principal Member Details on, Policy Number: " + scenarioOperator.getPolicyNumber());
            updatePrincipalMemberDetails.update();
            log.info("Saving Update Principal Member(s).");
            updatePrincipalMemberDetails.save();

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = updatePrincipalMemberDetails.getTestHandle();
            errorHandle = updatePrincipalMemberDetails.getErrorHandle();
            if (testHandle.isSuccess()) {
                updatePrincipalMemberDetails.validate();
                if (!updatePrincipalMemberDetails.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, updatePrincipalMemberDetails.getComment());
                } else {
                    createMultipleMintItemtable = updatePrincipalMemberDetails.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(principalMember);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = principalMember.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Update Principal Member Logic");
    }

    private void claims(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Claims Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        ClaimDetails claimDetails = new ClaimDetails(
                // region Claim Details Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Policy Holder Id"),
                getCellValue(headers, dataEntry,"Claim Handler"),
                getCellValue(headers, dataEntry,"Event"),
                getCellValue(headers, dataEntry,"Event Description"),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Event Date"), "dd-MM-yyyy"),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Claim Reg. Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Claimant"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Claim Type"),
                getCellValue(headers, dataEntry,"Attorney Company Name"),
                getCellValue(headers, dataEntry,"Attorney Cell Number"),
                getCellValue(headers, dataEntry,"Attorney Email"),
                getCellValue(headers, dataEntry,"Attorney Type"),
                getCellValue(headers, dataEntry,"Province"),
                getCellValue(headers, dataEntry,"City"),
                getCellValue(headers, dataEntry,"Cell Number"),
                getCellValue(headers, dataEntry,"Attorney Name"),
                getCellValue(headers, dataEntry,"With Attorney"),
                getCellValue(headers, dataEntry,"Payee"),
                getCellValue(headers, dataEntry,"Submit Payment"),
                getCellValue(headers, dataEntry,"Invoice Number"),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Invoice Date"), "yyyy-MM-dd"),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Date Received"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Invoice Details ServiceType"),
                getCellValue(headers, dataEntry,"Invoice Details Description"),
                getCellValue(headers, dataEntry,"Invoice Details Amount"),
                getCellValue(headers, dataEntry,"Invoice Details VAT"),
                getCellValue(headers, dataEntry,"Total Attorney Fee"),
                getCellValue(headers, dataEntry,"AOL Amount"),
                getCellValue(headers, dataEntry,"Payment Stage"),
                getCellValue(headers, dataEntry,"Await Doc Duration Per Day"),
                getCellValue(headers, dataEntry,"Await Doc Comments"),
                getCellValue(headers, dataEntry,"Await Doc Reason"),
                getCellValue(headers, dataEntry,"Await Doc Submit"),
                getCellValue(headers, dataEntry,"Submit POP UP Work Type0"),
                getCellValue(headers, dataEntry,"Submit POP UP Status0"),
                getCellValue(headers, dataEntry,"Submit POP UP Queue0"),
                getCellValue(headers, dataEntry,"Submit Work Type0"),
                getCellValue(headers, dataEntry,"Submit Status0"),
                getCellValue(headers, dataEntry,"Submit Queue0"),
                getCellValue(headers, dataEntry,"Payment Work Items Status"),
                getCellValue(headers, dataEntry,"Payment Work Items Queue"),
                getCellValue(headers, dataEntry,"Approve Payment"),
                getCellValue(headers, dataEntry,"Yes Approve Payment"),
                getCellValue(headers, dataEntry,"Approve POP UP Work Type0"),
                getCellValue(headers, dataEntry,"Approve POP UP Status0"),
                getCellValue(headers, dataEntry,"Approve POP UP Queue0"),
                getCellValue(headers, dataEntry,"Approve Work Type0"),
                getCellValue(headers, dataEntry,"Approve Status0"),
                getCellValue(headers, dataEntry,"Approve Queue0")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(claimDetails.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, claimDetails.getFirstPopup(), claimDetails.getSecondPopup());
        Claim claim = new Claim(driver, claimDetails, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                // updateFicaStatus(scenarioOperator, dbPolicyNumber, claimDetails.getRisk(), claimDetails.getSanction(), claimDetails.getEdd(), claimDetails.getKyc(), claimDetails.getStatus(), claimDetails.getUpdatedTe());
                // endregion
            }

            if (claimDetails.getClaimant().isEmpty()) {
                String policyHolder = createSearchItemPage.getPolicyHolder();
                claimDetails.setClaimant(policyHolder);
            }

            claims = createSearchItemPage.openClaims(true);
            verifyClientDialog.handlePopUp();

            // TODO: Uncomment The below code block
            claim.setExpectedResults();
            log.info("Claim, Policy Number: " + scenarioOperator.getPolicyNumber());
            claim.createNewClaim();
            log.info("Submitting claim.");
            claim.submit();

            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = claim.getTestHandle();
            errorHandle = claim.getErrorHandle();
            if (testHandle.isSuccess()) {
                claim.validate();
                if (!claim.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, claim.getComment());
                } else {
                    createMultipleMintItemtable = claim.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(claimDetails);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = claimDetails.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            workItems = createSearchItemPage.openWorkItems(false);
                            workItems.getWorkItems(mintItemCommentCESize);
                            createOrUpdateWorkItem = workItems.selectPendingClaimLOCWhereWorkTypeMatches(claimDetails);
                            createSearchItemPage.openClaims(false);

                            String referenceNumber = createOrUpdateWorkItem.getReferenceNumber();
                            claim.payClaim(referenceNumber);

                            workItems = createSearchItemPage.openWorkItems(false);
                            workItems.getWorkItems(mintItemCommentCESize);
                            createOrUpdateWorkItem = workItems.selectNewlyCreatedWorkItem(claimDetails);
                            createOrUpdateWorkItem.selectQueue(claimDetails.getPaymentWorkItemsQueue());
                            createOrUpdateWorkItem.selectStatus(claimDetails.getPaymentWorkItemsStatus());
                            createSearchItemPage.clickUpdate();

                            createSearchItemPage.switchToAndCloseServiceWindow(1);
                            createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();
                            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);
                            workItems.getWorkItems(mintItemCommentCESize);
                            createOrUpdateWorkItem = workItems.selectUpdatedPendingCreatedWorkItem(claimDetails);
                            String sequenceNumber = createOrUpdateWorkItem.getClaimSequenceNumber();

                            createSearchItemPage.openClaims(false);
                            verifyClientDialog.handlePopUp();
                            claim.approveOrFailClaimPayment(referenceNumber, sequenceNumber);

                            workItems = createSearchItemPage.openWorkItems(false);
                            workItems.getWorkItems(mintItemCommentCESize);
                            workItems.compareApprovePaymentWorkItems(claimDetails);
                            testHandle = workItems.getTestHandle();
                            errorHandle = workItems.getErrorHandle();
                            if (testHandle.isSuccess()) {
                                reportSuccess(reportableFields, scenarioOperator);
                            } else {
                                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                            }
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Claims Logic");
    }

    private void refunds(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Refunds Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        Refund refund = new Refund(
                // region Refund Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "yyyy-MM-dd"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Transaction Name"),
                getCellValue(headers, dataEntry,"Reason"),
                getCellValue(headers, dataEntry,"Please Provide Details"),
                getCellValue(headers, dataEntry,"Amount"),
                getCellValue(headers, dataEntry,"Refund Type"),
                getCellValue(headers, dataEntry,"Payee"),
                getCellValue(headers, dataEntry,"Account Number"),
                getCellValue(headers, dataEntry,"Branch Code"),
                getCellValue(headers, dataEntry,"Account Type"),
                getCellValue(headers, dataEntry,"Approve"),
                getCellValue(headers, dataEntry,"Yes Do"),
                getCellValue(headers, dataEntry,"Second Expected Result")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(refund.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, refund.getFirstPopup(), refund.getSecondPopup());
        Refunds refunds = new Refunds(driver, refund, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String dbPolicyNumber = queryHandler.getDbPolicyNumber();
            if (dbPolicyNumber != null) {
                // region Update FICA Status
                updateFicaStatus(scenarioOperator, dbPolicyNumber, refund.getRisk(), refund.getSanction(), refund.getEdd(), refund.getKyc(), refund.getStatus(), refund.getUpdatedTe());
                // endregion
            }

            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            genericMaintenance = createSearchItemPage.openGenericMaintenance(false);

            refunds.setExpectedResults();
            log.info("Refunds, Policy Number: " + scenarioOperator.getPolicyNumber());
            refunds.refund();
            log.info("Saving Refund.");
            refunds.save();

            testHandle = refunds.getTestHandle();
            errorHandle = refunds.getErrorHandle();
            if (testHandle.isSuccess()) {
                refunds.validate();
                if (!refunds.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, refunds.getComment());
                } else {
                    createMultipleMintItemtable = refunds.getCreateMultipleMintItemTable();
                    int mintItemCommentCESize = createMultipleMintItemtable.getMintItemCommentCESize();
                    workItems = createSearchItemPage.openWorkItems(false);
                    workItems.getWorkItems(mintItemCommentCESize);
                    workItems.compareWorkItems(refund);

                    testHandle = workItems.getTestHandle();
                    errorHandle = workItems.getErrorHandle();
                    if (testHandle.isSuccess()) {
                        // Note: We don't necessarily need the comments. So we can comments the below line of code out.
                        // workItems.getComments(premiumStatus.productName, premiumStatus.testCaseNumber, premiumStatus.function, mintItemCommentCESize);
                        auditTrail = createSearchItemPage.openAuditTrail(false);
                        String expectAuditTrail = refund.getAuditTrail();
                        auditTrail.audit(expectAuditTrail);

                        testHandle = auditTrail.getTestHandle();
                        errorHandle = auditTrail.getErrorHandle();
                        if (testHandle.isSuccess()) {
                            approveRefunds(refund, scenarioOperator);
                            // TODO: Implement The Logic For Test Results.
                            reportSuccess(reportableFields, scenarioOperator);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                        }
                    } else {
                        reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                    }
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Refunds Logic");
    }

    private void approveRefunds(Refund refund, ScenarioOperator scenarioOperator) throws Exception {
        log.info("Setting up a Second Driver Instance.");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        Properties properties = propertiesSetup.getProperties();
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        WebDriver driver = driverSetup.getDriver();
        log.info("Done Setting Second Driver Instance Setup");

        log.info("Authenticating Auth User.");
        LoginPage loginPage = new LoginPage(driver, scenarioOperator);
        loginPage.open(Config.targetApplicationPre);
        String auth_username = properties.getProperty("AUTH_USERNAME");
        String auth_password = properties.getProperty("AUTH_PASSWORD");
        loginPage.login(auth_username, auth_password);
        log.info("Done Authenticating Auth User.");

        log.info("Running Approve Refunds Logic");
        try {
            // TODO: Use existing instances instead of creating new ones.
            HomePage homePage = new HomePage(driver, scenarioOperator);
            CreateSearchItemPage createSearchItemPage;
            GenericPayment genericPayment;
            VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, refund.getFirstPopup(), refund.getSecondPopup());

            createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();
            createSearchItemPage.searchPolicyNumber(refund.getPolicyNumber(), false);

            WorkItems workItems = new WorkItems(driver, scenarioOperator);
            workItems.setWorkType(this.workItems.getWorkType());
            workItems.setStatus(this.workItems.getStatus());
            workItems.setQueue(this.workItems.getQueue());
            workItems.checkAndClickOnWorkItem(refund);

            genericPayment = createSearchItemPage.openGenericPayment(false);

            if (refund.getPayee().isEmpty()) {
                String policyHolder = createSearchItemPage.getPolicyHolder();
                refund.setPayee(policyHolder);
            }
            Refunds refunds = new Refunds(driver, refund, scenarioOperator);
            refunds.resetExpectedResults();
            log.info("Approve Refunds, Policy Number: " + refund.getPolicyNumber());
            refunds.approve(genericPayment);

        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            driver.quit();
        }
        log.info("End Of Approve Refunds Logic");
    }

    private void approveAttorney(AttorneyDetails attorneyDetails, ScenarioOperator scenarioOperator) throws Exception {
        log.info("Setting up a Second Driver Instance.");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        Properties properties = propertiesSetup.getProperties();
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        WebDriver driver = driverSetup.getDriver();
        log.info("Done Setting Second Driver Instance Setup");

        log.info("Authenticating Auth User.");
        LoginPage loginPage = new LoginPage(driver, scenarioOperator);
        loginPage.open(Config.targetApplicationPre);
        String auth_username = properties.getProperty("AUTH_USERNAME");
        String auth_password = properties.getProperty("AUTH_PASSWORD");
        loginPage.login(auth_username, auth_password);
        log.info("Done Authenticating Auth User.");

        log.info("Running Approve Refunds Logic");
        try {
            // TODO: Use existing instances instead of creating new ones.
            HomePage homePage = new HomePage(driver, scenarioOperator);
            CreateSearchItemPage createSearchItemPage;
            AttorneyManagement attorneyManagement = new AttorneyManagement(driver, attorneyDetails, scenarioOperator);
            VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, attorneyDetails.getFirstPopup(), attorneyDetails.getSecondPopup());

            createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();
            createSearchItemPage.searchPolicyNumberAndAvoidResponses(attorneyDetails.getPolicyNumber(), false);

            WorkItems workItems = new WorkItems(driver, scenarioOperator);
            // TODO: make sure we get all the work items that we need to check.
            workItems.getWorkItems(1);
            workItems.checkAndClickOnWorkItem(attorneyDetails);
            workItems.switchToAnfMaximizeServiceWindow(0);
            homePage.selectLegalAdvisorOrAdmin();
            attorneyManagement.approveAttorney(scenarioOperator);

        } catch (Exception e) {
            log.error(e.getMessage());
        } finally {
            driver.quit();
        }
        log.info("End Of Approve Refunds Logic");
    }

    private void attorneyManagement(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Add Member Logic");
        // TODO: Fix Date Of Birth Format
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        int debitOrderDate = getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Debit Order Date")) : 1;
        int numberOfMembers = getIntegerValue(getCellValue(headers, dataEntry, "NoD")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "NoD")) : 1;
        AttorneyDetails attorneyDetails = new AttorneyDetails(
                // region Attorney Details Model Fields
                String.valueOf(testCaseNumber),
                getCellValue(headers, dataEntry,"Product Name"),
                getCellValue(headers, dataEntry,"Scenario Description"),
                getCellValue(headers, dataEntry,"Policy No"),
                scenarioOperator.getPolicyNumber(),
                getCellValue(headers, dataEntry,"Function"),
                getCellValue(headers, dataEntry,"Run"),
                getCellValue(headers, dataEntry,"First Popup"),
                getCellValue(headers, dataEntry,"Second Popup"),
                String.valueOf(debitOrderDate),
                getDateStringWithFormat(getCellValue(headers, dataEntry,"Next Due Date"), "dd-MM-yyyy"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Expected Result"),
                getCellValue(headers, dataEntry,"Work Type0"),
                getCellValue(headers, dataEntry,"Status0"),
                getCellValue(headers, dataEntry,"Queue0"),
                getCellValue(headers, dataEntry,"POP UP Work Type0"),
                getCellValue(headers, dataEntry,"POP UP Status0"),
                getCellValue(headers, dataEntry,"POP UP Queue0"),
                getCellValue(headers, dataEntry,"Audit Trail Events"),
                getCellValue(headers, dataEntry,"Updatedte"),
                getCellValue(headers, dataEntry,"Risk"),
                getCellValue(headers, dataEntry,"Sanction"),
                getCellValue(headers, dataEntry,"Edd"),
                getCellValue(headers, dataEntry,"Kyc"),
                getCellValue(headers, dataEntry,"Status"),
                getCellValue(headers, dataEntry,"Company Name"),
                getCellValue(headers, dataEntry,"Vat Registered"),
                getCellValue(headers, dataEntry,"Vat Registration Number"),
                getCellValue(headers, dataEntry,"Email Address"),
                getCellValue(headers, dataEntry,"Cell Number"),
                getCellValue(headers, dataEntry,"Work Number"),
                getCellValue(headers, dataEntry,"Attorney Type"),
                getCellValue(headers, dataEntry,"Address Type"),
                getCellValue(headers, dataEntry,"Address Line One"),
                getCellValue(headers, dataEntry,"Address Line Two"),
                getCellValue(headers, dataEntry,"Suburb"),
                getCellValue(headers, dataEntry,"Bank Name"),
                getCellValue(headers, dataEntry,"Branch Code"),
                getCellValue(headers, dataEntry,"Account Type"),
                getCellValue(headers, dataEntry,"Account Number"),
                getCellValue(headers, dataEntry,"Account Holder"),
                getCellValue(headers, dataEntry,"Province"),
                getCellValue(headers, dataEntry,"City"),
                getCellValue(headers, dataEntry,"Add Attorney Submit"),
                getCellValue(headers, dataEntry,"Add Attorney Yes Submit"),
                getCellValue(headers, dataEntry,"Add Attorney Pop Up Work Type"),
                getCellValue(headers, dataEntry,"Add Attorney Pop Up Status"),
                getCellValue(headers, dataEntry,"Add Attorney Pop Up Queue"),
                getCellValue(headers, dataEntry,"Add Attorney Work Item Status Update"),
                getCellValue(headers, dataEntry,"Approve"),
                getCellValue(headers, dataEntry,"Yes Approve"),
                getCellValue(headers, dataEntry,"Approve Pop Up Work Type"),
                getCellValue(headers, dataEntry,"Approve Pop Up Status"),
                getCellValue(headers, dataEntry,"Approve Pop Up Queue")
                // endregion
        );

        // TODO: Use this otherwise, to record the created Policy Number/Attorney Reference Number
        scenarioOperator.setPolicyNumber(attorneyDetails.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, attorneyDetails.getFirstPopup(), attorneyDetails.getSecondPopup());
        AttorneyManagement attorneyManagement = new AttorneyManagement(driver, attorneyDetails, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            homePage = new HomePage(driver, scenarioOperator);
            legalAdvisorAdminPage = homePage.selectLegalAdvisorOrAdmin();

            attorneyManagement.setExpectedResults();
            log.info("Attorney Management, Policy Number: " + scenarioOperator.getPolicyNumber());
            attorneyManagement.addAttorney();
            log.info("Saving Attorney Management.");
            attorneyManagement.save();
            String attorneyReference = attorneyManagement.getAttorneyReference();
            createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();
            attorneyDetails.setPolicyNumber(attorneyReference);
            scenarioOperator.setPolicyNumber(attorneyReference);
            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);
            createSearchItemPage.searchPolicyNumberAndAvoidResponses(scenarioOperator.getPolicyNumber(), true);

            workItems = new WorkItems(driver, scenarioOperator);
            // Assume we will only see the newly Added Attorney, we assume we have one work item to check against.
            // TODO: Have a way to get the resulting table list.
            workItems.getWorkItems(1);
            createOrUpdateWorkItem = workItems.selectNewlyCreatedWorkItem(attorneyDetails);
            createOrUpdateWorkItem.selectStatus(attorneyDetails.getAddAttorneyWorkItemStatusUpdate());
            createSearchItemPage.clickUpdate();
            approveAttorney(attorneyDetails, scenarioOperator);


            // It's at this point that you would maybe want to check if the expected policy status is correct.
            // String policyStatus = createSearchItemPage.getPolicyStatus();
            // Then do all the checks against the expected.
            // if is not as expected then fail the test.

            // TODO: Check Both Change Premium Status Results Handles and Audit Trail Results Handles.
            testHandle = attorneyManagement.getTestHandle();
            errorHandle = attorneyManagement.getErrorHandle();
            if (testHandle.isSuccess()) {
                attorneyManagement.validate();
                if (!attorneyManagement.isValid()) {
                    reportFailure(reportableFields, scenarioOperator, attorneyManagement.getComment());
                } else {
                    reportSuccess(reportableFields, scenarioOperator);
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Add Member Logic");
    }

    private void checkDebitDate(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Debit Date Check Logic");
        headers = new String[]{"Product Name", "Policy Number", "Action", "Debit Order Date", "Next Due Date", "Waiting Period", "Areas"};
        String productName = getCellValue(headers, dataEntry,"Product Name");
        String policyNumber = getCellValue(headers, dataEntry,"Policy Number");
        // TODO: Implement Logic for getting flow policy numbers.
        String flowPolicyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
        DebitDateCheck debitDateCheck = new DebitDateCheck(
                // region Debit Date Check Fields
                productName,
                flowPolicyNumber,
                getCellValue(headers, dataEntry,"Action"),
                getCellValue(headers, dataEntry,"Debit Order Date"),
                getCellValue(headers, dataEntry,"Next Due Date"),
                getCellValue(headers, dataEntry,"Waiting Period"),
                getCellValue(headers, dataEntry,"Areas")
                // endregion
        );

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.setPolicyNumber(debitDateCheck.getPolicyNumber());
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        VerifyClientDialog verifyClientDialog = new VerifyClientDialog(driver, scenarioOperator, "Written", "Email address received from matches the email address on record?");
        CheckDebitDate checkDebitDate = new CheckDebitDate(driver, debitDateCheck, scenarioOperator);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);
            policyDetails = createSearchItemPage.openPolicyDetails(true);
            verifyClientDialog.handlePopUp();

            checkDebitDate.setExpectedResults();
            checkDebitDate.check();

            testHandle = checkDebitDate.getTestHandle();
            errorHandle = checkDebitDate.getErrorHandle();
            if (testHandle.isSuccess()) {
                auditTrail = createSearchItemPage.openAuditTrail(false);
                String expectAuditTrail = "Retention SMS sent for action: DA, Retention SMS sent for action: DP, Retention SMS sent for action: DR, Retention SMS sent for action: DF, Retention SMS sent for action: RI, Retention SMS sent for action: DK";
                auditTrail.check(expectAuditTrail);

                testHandle = auditTrail.getTestHandle();
                errorHandle = auditTrail.getErrorHandle();
                if (testHandle.isSuccess()) {
                    reportSuccess(reportableFields, scenarioOperator);
                } else {
                    reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
                }
            } else {
                reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            }
        } catch (Exception e) {
            reportFailure(reportableFields, scenarioOperator, e.getMessage());
        }
        log.info("End Of Debit Date Check Logic");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
