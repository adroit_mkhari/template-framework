package co.za.fnb.flow.tester.letters.helpers;

public enum TemplateLettersIgnore {
    AD_Welcome_Family_Post {
        public String toString() {
            return "-*- Demonstration Powered by OpenText Exstream (.*), Version 16.6.0 64-bit -*-\n" +
                    "\\s?First National Bank – a division of FirstRand Bank Limited. An Authorised Financial Services and Credit Provider \\(NCRCP20\\).Insured by FirstRand Life Assurance Limited.\\s?\n" +
                   "\\d{2}\\s(January|February|March|April|May|June|July|August|September|October|November|December)\\s\\d{4}\n" +
                   "\\d{4}/\\d{2}/\\d{2}\n" +
                   "^(.*?)FNB Accidental Death Welcome Letter\n" +
                   "Dear\\s(.+)FNB Life would like to welcome you as the new owner of an FNB Accidental Death Family Plan.\n" +
                   "Plan Number\\s+:\\s+(.*)Start Date of Plan/Cover\\s+:\\s+(.*)Monthly Premium\\s+:\\s+(.*)Premium Collection Date\\s+:\\s+(.*)Bank\\s+:\\s+(.*)Account Number\\s+:\\s+(.*)Billing Reference Number\\s+:\\s+(.*)See the Terms and Conditions for further details\n" +
                   "An Accidental Death benefit that pays out a lump sum of R(\\d+)\\.(\\d{2}) in the unfortunate event of\n" +
                   "R(\\d+)\\.(\\d{2})\\s+Funeral Cover that will pay out whether\n" +
                   "The following people(.)are insured under this Plan:(.*)Children Ages 0 - 5: R5000.00 Ages 6 - older: R10000.00\n" +
                   "Below is an illustration of(.*) projected premiums and cover amount for the next\\s+(\\d+.?)years:(.*)The premiums and cover amount shown in the table above\n" +
                   "Name and Surname\\s+:\\s+(.*)ID No / Date of Birth\\s+:\\s+(.*)Contact Number\\s+:\\s+(.*)The Plan and its benefits are subject to the Terms,";
        }
    },
    AD_Welcome_Family_Email {
        public String toString() {
            return "";
        }
    },
    PP_Welcome {
        public String toString() {
            return "\\s?First National Bank – a division of FirstRand Bank Limited. An Authorised Financial Services and Credit Provider \\(NCRCP20\\).Insured by FirstRand Life Assurance Limited.\\s?\n" +
                    "\\d{2}\\s(January|February|March|April|May|June|July|August|September|October|November|December)\\s\\d{4}\n" +
                    "^(.*?)FNB Pay Protect Plan Welcome Letter\n" +
                    "Dear\\s(.+)FNB Life would like to welcome you as the new owner of FNB’s Pay Protect Plan.\n" +
                    "Plan Number\\s+:\\s+(.*)Start Date of Plan/Cover\\s+:\\s+(.*)Monthly Premium\\s+:\\s+(.*)Premium Collection Date\\s+:\\s+(.*)Bank\\s+:\\s+(.*)Account Number\\s+:\\s+(.*)Billing Reference Number\\s+:\\s+(.*)See the Terms and Conditions for further details\n" +
                    "An Accidental Death benefit that pays out a lump sum of R(\\d+)\\.(\\d{2}) in the unfortunate event of\n" +
                    "R(\\d+)\\.(\\d{2})\\s+Funeral Cover that will pay out whether\n" +
                    "The following people(.)are insured under this Plan:(.*)Children Ages 0 - 5: R5000.00 Ages 6 - older: R10000.00\n" +
                    "Below is an illustration of(.*) projected premiums and cover amount for the next\\s+(\\d+.?)years:(.*)The premiums and cover amount shown in the table above\n" +
                    "Name and Surname\\s+:\\s+(.*)ID No / Date of Birth\\s+:\\s+(.*)Contact Number\\s+:\\s+(.*)The Plan and its benefits are subject to the Terms,";
        }
    },
}
