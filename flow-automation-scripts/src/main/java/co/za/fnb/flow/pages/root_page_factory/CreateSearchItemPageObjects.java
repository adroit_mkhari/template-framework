package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateSearchItemPageObjects extends BasePage {
    // region Policy Details
    @FindBy(xpath = "//*[contains(@id, 'searchPolicyNumber')]")
    private WebElement searchPolicyNumber;

    @FindBy(id = "createSearchItemView:policyHolder")
    private WebElement policyHolder;

    @FindBy(id = "createSearchItemView:policyStatus")
    private WebElement policyStatus;

    @FindBy(id = "createSearchItemView:productName")
    private WebElement productName;

    @FindBy(id = "createSearchItemView:inceptionDate")
    private WebElement inceptionDate;

    @FindBy(id = "createSearchItemView:policyInArrears")
    private WebElement policyInArrears;

    @FindBy(id = "createSearchItemView:premiumstatus")
    private WebElement premiumStatus;

    @FindBy(xpath = "//*[contains(@class, 'ui-corner-all ui-button-text-icon-left')]")
    private WebElement searchButton;
    // endregion

    // region Create Or Update Work Item
    @FindBy(id = "createSearchItemView:category_label")
    private WebElement categorySelectLabel;

    @FindBy(id = "createSearchItemView:category_items")
    private WebElement categorySelectItems;

    @FindBy(id = "createSearchItemView:subCategory_label")
    private WebElement subCategorySelectLabel;

    @FindBy(id = "createSearchItemView:subCategory_items")
    private WebElement subCategorySelectItems;

    @FindBy(id = "createSearchItemView:search")
    private WebElement search;

    @FindBy(id = "createSearchItemView:create")
    private WebElement create;

    @FindBy(id = "createSearchItemView:update")
    private WebElement update;

    @FindBy(id = "createSearchItemView:clone")
    private WebElement clone;

    @FindBy(id = "createSearchItemView:clear")
    private WebElement clear;
    // endregion

    // region Links
    @FindBy(linkText = "Work Items")
    private WebElement workItems;

    @FindBy(linkText = "Principal Member Details")
    private WebElement principalMemberDetails;

    @FindBy(linkText = "Policy Details")
    private WebElement policyDetails;

    @FindBy(linkText = "Financial Administration")
    private WebElement financialAdministration;

    @FindBy(linkText = "Sales Entity")
    private WebElement salesEntity;

    @FindBy(linkText = "Generic Maintanence")
    private WebElement genericMaintenance;

    @FindBy(linkText = "Generic Letter")
    private WebElement genericLetter;

    @FindBy(linkText = "Audit Trail")
    private WebElement auditTrail;

    @FindBy(linkText = "Generic Payment")
    private WebElement genericPayment;

    @FindBy(linkText = "Claims")
    private WebElement claims;
    //endregion

    // region Premiums
    @FindBy(id = "createSearchItemView:mainTabView:TotalPremiumAmount")
    WebElement totalPremiumAmount;
    // endregion

    @FindBy(linkText = "Documents")
    private WebElement documents;

    @FindBy(id = "createSearchItemView:mainTabView:policyDocumentsTable:0:dragIcon")
    private WebElement firstPolicyDocumentTableDragIcon;

    @FindBy(id = "createSearchItemView:retentionsMsg")
    private WebElement retentionsMessage;

    public CreateSearchItemPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getSearchPolicyNumber() {
        return searchPolicyNumber;
    }

    public WebElement getPolicyHolder() {
        return policyHolder;
    }

    public WebElement getPolicyStatus() {
        return policyStatus;
    }

    public WebElement getProductName() {
        return productName;
    }

    public WebElement getInceptionDate() {
        return inceptionDate;
    }

    public WebElement getPolicyInArrears() {
        return policyInArrears;
    }

    public WebElement getPremiumStatus() {
        return premiumStatus;
    }

    public WebElement getSearchButton() {
        return searchButton;
    }

    public WebElement getCategorySelectLabel() {
        return categorySelectLabel;
    }

    public WebElement getCategorySelectItems() {
        return categorySelectItems;
    }

    public WebElement getSubCategorySelectLabel() {
        return subCategorySelectLabel;
    }

    public WebElement getSubCategorySelectItems() {
        return subCategorySelectItems;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getCreate() {
        return create;
    }

    public WebElement getUpdate() {
        return update;
    }

    public WebElement getClone() {
        return clone;
    }

    public WebElement getClear() {
        return clear;
    }

    public WebElement getWorkItems() {
        return workItems;
    }

    public WebElement getPrincipalMemberDetails() {
        return principalMemberDetails;
    }

    public WebElement getPolicyDetails() {
        return policyDetails;
    }

    public WebElement getFinancialAdministration() {
        return financialAdministration;
    }

    public WebElement getSalesEntity() {
        return salesEntity;
    }

    public WebElement getGenericMaintenance() {
        return genericMaintenance;
    }

    public WebElement getGenericLetter() {
        return genericLetter;
    }

    public WebElement getAuditTrail() {
        return auditTrail;
    }

    public WebElement getGenericPayment() {
        return genericPayment;
    }

    public WebElement getClaims() {
        return claims;
    }

    public WebElement getTotalPremiumAmount() {
        return totalPremiumAmount;
    }

    public WebElement getDocuments() {
        return documents;
    }

    public WebElement getFirstPolicyDocumentTableDragIcon() {
        return firstPolicyDocumentTableDragIcon;
    }

    public WebElement getRetentionsMessage() {
        return retentionsMessage;
    }

}
