package co.za.fnb.flow.pages.generic_maintanence;

import co.za.fnb.flow.pages.generic_maintanence.page_factory.GenericMaintenancePageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.CreateSearchItemPage;
import org.openqa.selenium.WebElement;

public class GenericMaintenance extends CreateSearchItemPage {
    private Logger log  = LogManager.getLogger(GenericMaintenance.class);
    GenericMaintenancePageObjects genericMaintenancePageObjects = new GenericMaintenancePageObjects(driver);

    public GenericMaintenance(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void selectTransactionName(String transactionName) throws Exception {
        try {
            selectExactMatchingDataLabel(genericMaintenancePageObjects.getTransactionNameSelect(), genericMaintenancePageObjects.getTransactionNameSelectItems(), transactionName);
        } catch (Exception e) {
            log.error("Error while selecting Transaction Name: " + transactionName);
            throw new Exception("Error while selecting Transaction Name: " + transactionName);
        }
    }

    public void selectReason(String reason) throws Exception {
        try {
            selectMatchingDataLabel(genericMaintenancePageObjects.getReasonSelect(), genericMaintenancePageObjects.getReasonSelectItems(), reason);
        } catch (Exception e) {
            log.error("Error while selecting Reason: " + reason);
            throw new Exception("Error while selecting Reason: " + reason);
        }
    }

    public void clickOther() throws Exception {
        try {
            click(genericMaintenancePageObjects.getOtherTextArea());
        } catch (Exception e) {
            log.error("Error while clicking other (provide details).");
            throw new Exception("Error while clicking other (provide details).");
        }
    }

    public void clearOther() throws Exception {
        try {
            clear(genericMaintenancePageObjects.getOtherTextArea());
        } catch (Exception e) {
            log.error("Error while clearing other (provide details).");
            throw new Exception("Error while clearing other (provide details).");
        }
    }

    public void updateOther(String other) throws Exception {
        try {
            type(genericMaintenancePageObjects.getOtherTextArea(), other);
        } catch (Exception e) {
            log.error("Error while updating other (provide details).");
            throw new Exception("Error while updating other (provide details).");
        }
    }

    public void clickAmount() throws Exception {
        try {
            click(genericMaintenancePageObjects.getAmount());
        } catch (Exception e) {
            log.error("Error while clicking Amount.");
            throw new Exception("Error while clicking Amount.");
        }
    }

    public void doubleClickAmount() throws Exception {
        try {
            moveToElementAndDoubleClick(genericMaintenancePageObjects.getAmount());
        } catch (Exception e) {
            log.error("Error while double clicking Amount.");
            throw new Exception("Error while double clicking Amount.");
        }
    }

    public void clearAmount() throws Exception {
        try {
            clear(genericMaintenancePageObjects.getAmountInput());
        } catch (Exception e) {
            log.error("Error while clearing Amount.");
            throw new Exception("Error while clearing Amount.");
        }
    }

    public void updateAmount(String amount) throws Exception {
        try {
            clearAmount();
            type(genericMaintenancePageObjects.getAmountInput(), amount);
        } catch (Exception e) {
            log.error("Error while updating Amount.");
            throw new Exception("Error while updating Amount.");
        }
    }

    // TODO: Include Refunds Type For Auto Refunds logic.

    public void clickPayee() throws Exception {
        try {
            click(genericMaintenancePageObjects.getPayee());
        } catch (Exception e) {
            log.error("Error while clicking Payee.");
            throw new Exception("Error while clicking Payee.");
        }
    }

    public void doubleClickPayee() throws Exception {
        try {
            moveToElementAndDoubleClick(genericMaintenancePageObjects.getPayee());
        } catch (Exception e) {
            log.error("Error while double clicking Payee.");
            throw new Exception("Error while double clicking Payee.");
        }
    }

    public void clearPayee() throws Exception {
        try {
            clear(genericMaintenancePageObjects.getPayee());
        } catch (Exception e) {
            log.error("Error while clearing Payee.");
            throw new Exception("Error while clearing Payee.");
        }
    }

    public void updatePayee(String payee) throws Exception {
        try {
            clearPayee();
            type(genericMaintenancePageObjects.getPayee(), payee);
        } catch (Exception e) {
            log.error("Error while updating Payee.");
            throw new Exception("Error while updating Payee.");
        }
    }

    public void clickAccountNumber() throws Exception {
        try {
            click(genericMaintenancePageObjects.getAccountNumber());
        } catch (Exception e) {
            log.error("Error while clicking Account Number.");
            throw new Exception("Error while clicking Account Number.");
        }
    }

    public void clearAccountNumber() throws Exception {
        try {
            clear(genericMaintenancePageObjects.getAccountNumber());
        } catch (Exception e) {
            log.error("Error while clearing Account Number.");
            throw new Exception("Error while clearing Account Number.");
        }
    }

    public void updateAccountNumber(String payee) throws Exception {
        try {
            type(genericMaintenancePageObjects.getAccountNumber(), payee);
        } catch (Exception e) {
            log.error("Error while updating Account Number.");
            throw new Exception("Error while updating Account Number.");
        }
    }

    public void clickBranchCode() throws Exception {
        try {
            click(genericMaintenancePageObjects.getBranchCode());
        } catch (Exception e) {
            log.error("Error while clicking Branch Code.");
            throw new Exception("Error while clicking Branch Code.");
        }
    }

    public void clearBranchCode() throws Exception {
        try {
            clear(genericMaintenancePageObjects.getBranchCode());
        } catch (Exception e) {
            log.error("Error while clearing Branch Code.");
            throw new Exception("Error while clearing Branch Code.");
        }
    }

    public void updateBranchCode(String payee) throws Exception {
        try {
            type(genericMaintenancePageObjects.getBranchCode(), payee);
        } catch (Exception e) {
            log.error("Error while updating Branch Code.");
            throw new Exception("Error while updating Branch Code.");
        }
    }

    public void clickAccountType() throws Exception {
        try {
            click(genericMaintenancePageObjects.getAccountType());
        } catch (Exception e) {
            log.error("Error while clicking Account Type.");
            throw new Exception("Error while clicking Account Type.");
        }
    }

    public void selectAccountType(String accountType) throws Exception {
        try {
            // selectMatchingDataLabel(genericMaintenancePageObjects.getAccountTypeSelect(), genericMaintenancePageObjects.getAccountTypeSelectItems(), accountType);
            selectMatchingDataLabelAtIndexZeroOrOne(genericMaintenancePageObjects.getAccountTypeSelect(), genericMaintenancePageObjects.getAccountTypeSelectItems(), accountType);
        } catch (Exception e) {
            log.error("Error while selecting Account Type: " + accountType);
            throw new Exception("Error while selecting Account Type: " + accountType);
        }
    }

    public void selectMatchingDataLabelAtIndexZeroOrOne(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                click(dropdown);
                try {
                    dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(0).click();
                } catch (Exception e) {
                    try {
                        dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(1).click();
                    } catch (Exception ex) {
                        throw new Exception(e.getMessage());
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void clickSave() throws Exception {
        try {
            click(genericMaintenancePageObjects.getSave());
        } catch (Exception e) {
            log.error("Error while clicking on Save.");
            throw new Exception("Error while clicking on Save.");
        }
    }

    public void scrollToSave() throws Exception {
        try {
            scrollToElement(genericMaintenancePageObjects.getSave());
        } catch (Exception e) {
            log.error("Error while clicking on Save.");
            throw new Exception("Error while clicking on Save.");
        }
    }

}
