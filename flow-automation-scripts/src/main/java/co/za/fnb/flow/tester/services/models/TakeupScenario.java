package co.za.fnb.flow.tester.services.models;

public class TakeupScenario {
    private String takeupCode;
    private String scenarioDescriptionFunction;
    private String productName;
    private String premiumStatus;
    private String policyStatus;
    private String username;
    private String updateFicaStatus;
    private String requestType;
    private String childRoleIdActions;
    private String parentExtendedFamilyRoleIdAction;
    private String referenceNumber;
    private String subChannel;
    private String campaignNumber;
    private String leadsNumber;
    private String ucnNumber;
    private String productCode;
    private String subProdCode;
    private String salesPerson;
    private String salesPersonId;
    private String salesPersonNumber;
    private String capturedBy;
    private String capturedByNo;
    private String capturedDate;
    private String inceptionDate;
    private String companyName;
    private String tradingName;
    private String businessVatNo;
    private String companyRegistrationNo;
    private String grossIncome;
    private String coverAmount;
    private ContactDetails contactDetails;
    private BankingDetails bankingDetails;
    private PolicyHolder policyHolder;
    private AuthorizedPerson authorizedPerson;
    private Spouse spouse;
    private Child child;
    private ParentExtendedFamily parentExtendedFamily;
    private Beneficiary beneficiary;

    public TakeupScenario(String takeupCode) {
        this.takeupCode = takeupCode;
    }

    public TakeupScenario(String takeupCode, String scenarioDescriptionFunction, String productName, String premiumStatus, String policyStatus, String username, String updateFicaStatus, String requestType, String childRoleIdActions, String parentExtendedFamilyRoleIdAction, String referenceNumber, String subChannel, String campaignNumber, String leadsNumber, String ucnNumber, String productCode, String subProdCode, String salesPerson, String salesPersonId, String salesPersonNumber, String capturedBy, String capturedByNo, String capturedDate, String inceptionDate, String companyName, String tradingName, String businessVatNo, String companyRegistrationNo, String grossIncome, String coverAmount, ContactDetails contactDetails, BankingDetails bankingDetails, PolicyHolder policyHolder, AuthorizedPerson authorizedPerson, Spouse spouse, Child child, ParentExtendedFamily parentExtendedFamily, Beneficiary beneficiary) {
        this.takeupCode = takeupCode;
        this.scenarioDescriptionFunction = scenarioDescriptionFunction;
        this.productName = productName;
        this.premiumStatus = premiumStatus;
        this.policyStatus = policyStatus;
        this.username = username;
        this.updateFicaStatus = updateFicaStatus;
        this.requestType = requestType;
        this.childRoleIdActions = childRoleIdActions;
        this.parentExtendedFamilyRoleIdAction = parentExtendedFamilyRoleIdAction;
        this.referenceNumber = referenceNumber;
        this.subChannel = subChannel;
        this.campaignNumber = campaignNumber;
        this.leadsNumber = leadsNumber;
        this.ucnNumber = ucnNumber;
        this.productCode = productCode;
        this.subProdCode = subProdCode;
        this.salesPerson = salesPerson;
        this.salesPersonId = salesPersonId;
        this.salesPersonNumber = salesPersonNumber;
        this.capturedBy = capturedBy;
        this.capturedByNo = capturedByNo;
        this.capturedDate = capturedDate;
        this.inceptionDate = inceptionDate;
        this.companyName = companyName;
        this.tradingName = tradingName;
        this.businessVatNo = businessVatNo;
        this.companyRegistrationNo = companyRegistrationNo;
        this.grossIncome = grossIncome;
        this.coverAmount = coverAmount;
        this.contactDetails = contactDetails;
        this.bankingDetails = bankingDetails;
        this.policyHolder = policyHolder;
        this.authorizedPerson = authorizedPerson;
        this.spouse = spouse;
        this.child = child;
        this.parentExtendedFamily = parentExtendedFamily;
        this.beneficiary = beneficiary;
    }

    public String getTakeupCode() {
        return takeupCode;
    }

    public void setTakeupCode(String takeupCode) {
        this.takeupCode = takeupCode;
    }

    public String getScenarioDescriptionFunction() {
        return scenarioDescriptionFunction;
    }

    public void setScenarioDescriptionFunction(String scenarioDescriptionFunction) {
        this.scenarioDescriptionFunction = scenarioDescriptionFunction;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getPremiumStatus() {
        return premiumStatus;
    }

    public void setPremiumStatus(String premiumStatus) {
        this.premiumStatus = premiumStatus;
    }

    public String getPolicyStatus() {
        return policyStatus;
    }

    public void setPolicyStatus(String policyStatus) {
        this.policyStatus = policyStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUpdateFicaStatus() {
        return updateFicaStatus;
    }

    public void setUpdateFicaStatus(String updateFicaStatus) {
        this.updateFicaStatus = updateFicaStatus;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public String getChildRoleIdActions() {
        return childRoleIdActions;
    }

    public void setChildRoleIdActions(String childRoleIdActions) {
        this.childRoleIdActions = childRoleIdActions;
    }

    public String getParentExtendedFamilyRoleIdAction() {
        return parentExtendedFamilyRoleIdAction;
    }

    public void setParentExtendedFamilyRoleIdAction(String parentExtendedFamilyRoleIdAction) {
        this.parentExtendedFamilyRoleIdAction = parentExtendedFamilyRoleIdAction;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getSubChannel() {
        return subChannel;
    }

    public void setSubChannel(String subChannel) {
        this.subChannel = subChannel;
    }

    public String getCampaignNumber() {
        return campaignNumber;
    }

    public void setCampaignNumber(String campaignNumber) {
        this.campaignNumber = campaignNumber;
    }

    public String getLeadsNumber() {
        return leadsNumber;
    }

    public void setLeadsNumber(String leadsNumber) {
        this.leadsNumber = leadsNumber;
    }

    public String getUcnNumber() {
        return ucnNumber;
    }

    public void setUcnNumber(String ucnNumber) {
        this.ucnNumber = ucnNumber;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getSubProdCode() {
        return subProdCode;
    }

    public void setSubProdCode(String subProdCode) {
        this.subProdCode = subProdCode;
    }

    public String getSalesPerson() {
        return salesPerson;
    }

    public void setSalesPerson(String salesPerson) {
        this.salesPerson = salesPerson;
    }

    public String getSalesPersonId() {
        return salesPersonId;
    }

    public void setSalesPersonId(String salesPersonId) {
        this.salesPersonId = salesPersonId;
    }

    public String getSalesPersonNumber() {
        return salesPersonNumber;
    }

    public void setSalesPersonNumber(String salesPersonNumber) {
        this.salesPersonNumber = salesPersonNumber;
    }

    public String getCapturedBy() {
        return capturedBy;
    }

    public void setCapturedBy(String capturedBy) {
        this.capturedBy = capturedBy;
    }

    public String getCapturedByNo() {
        return capturedByNo;
    }

    public void setCapturedByNo(String capturedByNo) {
        this.capturedByNo = capturedByNo;
    }

    public String getCapturedDate() {
        return capturedDate;
    }

    public void setCapturedDate(String capturedDate) {
        this.capturedDate = capturedDate;
    }

    public String getInceptionDate() {
        return inceptionDate;
    }

    public void setInceptionDate(String inceptionDate) {
        this.inceptionDate = inceptionDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getTradingName() {
        return tradingName;
    }

    public void setTradingName(String tradingName) {
        this.tradingName = tradingName;
    }

    public String getBusinessVatNo() {
        return businessVatNo;
    }

    public void setBusinessVatNo(String businessVatNo) {
        this.businessVatNo = businessVatNo;
    }

    public String getCompanyRegistrationNo() {
        return companyRegistrationNo;
    }

    public void setCompanyRegistrationNo(String companyRegistrationNo) {
        this.companyRegistrationNo = companyRegistrationNo;
    }

    public String getGrossIncome() {
        return grossIncome;
    }

    public void setGrossIncome(String grossIncome) {
        this.grossIncome = grossIncome;
    }

    public String getCoverAmount() {
        return coverAmount;
    }

    public void setCoverAmount(String coverAmount) {
        this.coverAmount = coverAmount;
    }

    public ContactDetails getContactDetails() {
        return contactDetails;
    }

    public void setContactDetails(ContactDetails contactDetails) {
        this.contactDetails = contactDetails;
    }

    public BankingDetails getBankingDetails() {
        return bankingDetails;
    }

    public void setBankingDetails(BankingDetails bankingDetails) {
        this.bankingDetails = bankingDetails;
    }

    public PolicyHolder getPolicyHolder() {
        return policyHolder;
    }

    public void setPolicyHolder(PolicyHolder policyHolder) {
        this.policyHolder = policyHolder;
    }

    public AuthorizedPerson getAuthorizedPerson() {
        return authorizedPerson;
    }

    public void setAuthorizedPerson(AuthorizedPerson authorizedPerson) {
        this.authorizedPerson = authorizedPerson;
    }

    public Spouse getSpouse() {
        return spouse;
    }

    public void setSpouse(Spouse spouse) {
        this.spouse = spouse;
    }

    public Child getChild() {
        return child;
    }

    public void setChild(Child child) {
        this.child = child;
    }

    public ParentExtendedFamily getParentExtendedFamily() {
        return parentExtendedFamily;
    }

    public void setParentExtendedFamily(ParentExtendedFamily parentExtendedFamily) {
        this.parentExtendedFamily = parentExtendedFamily;
    }

    public Beneficiary getBeneficiary() {
        return beneficiary;
    }

    public void setBeneficiary(Beneficiary beneficiary) {
        this.beneficiary = beneficiary;
    }
}
