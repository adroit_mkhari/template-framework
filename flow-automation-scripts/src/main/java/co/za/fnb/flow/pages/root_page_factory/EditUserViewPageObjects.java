package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class EditUserViewPageObjects {
    @FindBy(id = "frmEditUser:firstname")
    private WebElement firstName;

    @FindBy(id = "frmEditUser:lastname")
    private WebElement lastName;

    @FindBy(id = "frmEditUser:username")
    private WebElement username;

    @FindBy(id = "frmEditUser:email")
    private WebElement email;

    @FindBy(id = "frmEditUser:pckListEditUserDepartment")
    private WebElement department;

    @FindBy(id = "frmEditUser:pckListEditUserRole")
    private WebElement role;

    @FindBy(id = "frmEditUser:pckListEditUserPermission")
    private WebElement permission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button")
    private WebElement addDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button[2]")
    private WebElement addAllDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button")
    private WebElement addRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button[2]")
    private WebElement addAllRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button")
    private WebElement addPermission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button[2]")
    private WebElement addAllPermission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button[3]")
    private WebElement removeDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button[4]")
    private WebElement removeAllDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button[3]")
    private WebElement removeRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button[4]")
    private WebElement removeAllRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button[3]")
    private WebElement removePermission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button[4]")
    private WebElement removeAllPermission;

    @FindBy(id = "btnEditUserSubmit")
    private WebElement submit;

    public EditUserViewPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getLastName() {
        return lastName;
    }

    public WebElement getUsername() {
        return username;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getDepartment() {
        return department;
    }

    public WebElement getRole() {
        return role;
    }

    public WebElement getPermission() {
        return permission;
    }

    public WebElement getAddDepartment() {
        return addDepartment;
    }

    public WebElement getAddAllDepartment() {
        return addAllDepartment;
    }

    public WebElement getAddRole() {
        return addRole;
    }

    public WebElement getAddAllRole() {
        return addAllRole;
    }

    public WebElement getAddPermission() {
        return addPermission;
    }

    public WebElement getAddAllPermission() {
        return addAllPermission;
    }

    public WebElement getRemoveDepartment() {
        return removeDepartment;
    }

    public WebElement getRemoveAllDepartment() {
        return removeAllDepartment;
    }

    public WebElement getRemoveRole() {
        return removeRole;
    }

    public WebElement getRemoveAllRole() {
        return removeAllRole;
    }

    public WebElement getRemovePermission() {
        return removePermission;
    }

    public WebElement getRemoveAllPermission() {
        return removeAllPermission;
    }

    public WebElement getSubmit() {
        return submit;
    }
}
