package co.za.fnb.flow.models;

public class ScenarioBase {
    private String testCaseNumber;
    private String productName;
    private String scenarioDescription;
    private String policyNumberCode;
    private String policyNumber;
    private String function;
    private String run;

    public ScenarioBase(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run) {
        this.testCaseNumber = testCaseNumber;
        this.productName = productName;
        this.scenarioDescription = scenarioDescription;
        this.policyNumberCode = policyNumberCode;
        this.policyNumber = policyNumber;
        this.function = function;
        this.run = run;
    }

    public void setTestCaseNumber(String testCaseNumber) {
        this.testCaseNumber = testCaseNumber;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public void setScenarioDescription(String scenarioDescription) {
        this.scenarioDescription = scenarioDescription;
    }

    public void setPolicyNumberCode(String policyNumberCode) {
        this.policyNumberCode = policyNumberCode;
    }

    public void setPolicyNumber(String policyNumber) {
        this.policyNumber = policyNumber;
    }

    public void setFunction(String function) {
        this.function = function;
    }

    public void setRun(String run) {
        this.run = run;
    }

    public String getTestCaseNumber() {
        return testCaseNumber;
    }

    public String getProductName() {
        return productName;
    }

    public String getScenarioDescription() {
        return scenarioDescription;
    }

    public String getPolicyNumberCode() {
        return policyNumberCode;
    }

    public String getPolicyNumber() {
        return policyNumber;
    }

    public String getFunction() {
        return function;
    }

    public String getRun() {
        return run;
    }

    @Override
    public String toString() {
        return "ScenarioBase{" +
                "testCaseNumber='" + testCaseNumber + '\'' +
                ", productName='" + productName + '\'' +
                ", scenarioDescription='" + scenarioDescription + '\'' +
                ", policyNumberCode='" + policyNumberCode + '\'' +
                ", policyNumber='" + policyNumber + '\'' +
                ", function='" + function + '\'' +
                ", run='" + run + '\'' +
                '}';
    }
}
