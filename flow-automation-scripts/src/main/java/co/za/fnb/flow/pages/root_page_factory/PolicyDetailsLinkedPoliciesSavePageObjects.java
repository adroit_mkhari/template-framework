package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PolicyDetailsLinkedPoliciesSavePageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:showPolicyDetailPoliciesView:policyDetailsLinkedPoliciesSave")
    private WebElement policyDetailsLinkedPoliciesSave;

    public PolicyDetailsLinkedPoliciesSavePageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPolicyDetailsLinkedPoliciesSave() {
        return policyDetailsLinkedPoliciesSave;
    }
}
