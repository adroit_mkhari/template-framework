package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.CreateUserViewPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class CreateUserView extends BasePage {
    CreateUserViewPageObjects createUserViewPageObjects = new CreateUserViewPageObjects(driver);

    public CreateUserView(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
