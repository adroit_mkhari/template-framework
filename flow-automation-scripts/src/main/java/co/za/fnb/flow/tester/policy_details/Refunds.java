package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Refund;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.generic_payment.GenericPayment;
import co.za.fnb.flow.pages.generic_payment.RefundDetailsView;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.pages.policy_details.information.BeneficiaryInformation;
import co.za.fnb.flow.pages.policy_details.information.PolicyInformation;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class Refunds {
    private Logger log  = LogManager.getLogger(Refunds.class);
    WebDriver driver;
    Refund refund;
    PolicyInformation policyInformation;
    GenericMaintenance genericMaintenance;
    BeneficiaryInformation beneficiaryInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    private ScenarioOperator scenarioOperator;

    public Refunds(WebDriver driver, Refund refund, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.refund = refund;
        this.scenarioOperator = scenarioOperator;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        genericMaintenance = new GenericMaintenance(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(refund.getExpectResult());
    }

    public void resetExpectedResults() {
        policyInformation.setExpectedResults(refund.getSecondExpectedResult());
    }

    // Note, Reasons: CANCELLATION OF POLICY/ COVER NOT DONE ON TIME,CANCELLED WITHIN THE COOLING-OFF PERIOD,COLLECTION OF PREMIUM AFTER DATE OF DEATH,FORCED SALE,FRAUD,FNB LIFE/ FNB PROCESS FAILURE,INCORRECT AGE CAPTURED AT SALE/ ALTERATION STAGE,INCORRECT DEBIT AMOUNT/DATE,INCORRECT ROLE CAPTURED,NOT AGREE TO TAKE POLICY,OTHER,PREMIUM PAYER/ ACCOUNT HOLDER DIFFERENT,SALES RECORD NOT AVAILABLE,COLLECTION AFTER CANCELLATION/ POLICY AMENDMENT

    public void refund() throws Exception {
        log.info("Handling Refunds logic");

        if (!refund.getTransactionName().isEmpty()) {
            genericMaintenance.selectTransactionName(refund.getTransactionName());
            Thread.sleep(1000);
        }

        if (!refund.getReason().isEmpty()) {
            genericMaintenance.selectReason(refund.getReason());
            Thread.sleep(1000);

            if (refund.getReason().equalsIgnoreCase("OTHER")) {
                if (!refund.getPleaseProvideDetails().isEmpty()) {
                    genericMaintenance.clickOther();
                    Thread.sleep(1000);
                    genericMaintenance.updateOther(refund.getPleaseProvideDetails());
                    Thread.sleep(1000);
                }
            }
        }

        if (!refund.getAmount().isEmpty()) {
            genericMaintenance.doubleClickAmount();
            Thread.sleep(1000);
            genericMaintenance.updateAmount(refund.getAmount());
            Thread.sleep(1000);
        }

        if (!refund.getPayee().isEmpty()) {
            genericMaintenance.doubleClickPayee();
            Thread.sleep(1000);
            genericMaintenance.updatePayee(refund.getPayee());
            Thread.sleep(1000);
        }

        if (!refund.getAccountNumber().isEmpty()) {
            genericMaintenance.updateAccountNumber(refund.getAccountNumber());
            Thread.sleep(1000);
        }

        if (!refund.getBranchCode().isEmpty()) {
            genericMaintenance.updateBranchCode(refund.getBranchCode());
            Thread.sleep(1000);
        }

        genericMaintenance.scrollToSave();

        if (!refund.getAccountType().isEmpty()) {
            genericMaintenance.selectAccountType(refund.getAccountType());
            Thread.sleep(1000);
        }

        log.info("Done Handling Refunds logic");
    }

    public void approve(GenericPayment genericPayment) throws Exception {
        log.info("Handling Approve Refunds logic");
        String status, payee,  amount;
        Double amountValue, refundValue;
        int refundList = genericPayment.getRefundList();
        boolean flag = false;

        for (int i = 0; i < refundList; i++) {
            status = genericPayment.getStatus(i);
            payee = genericPayment.getPayee(i);
            amount = genericPayment.getAmount(i);
            amountValue = Double.valueOf(amount); // TODO: Implement This properly to avoid errors
            refundValue = Double.valueOf(refund.getAmount()); // TODO: Implement This properly to avoid errors

            if (status.equalsIgnoreCase("AWAIT APPROVAL") && payee.equalsIgnoreCase(refund.getPayee()) && amountValue.equals(refundValue)) {
                genericPayment.doublePolicyNumber(i);
                flag = true;
                break;
            }
        }
        Thread.sleep(3000);

        if (!flag) {
            log.error("Target Refund not on Refund List.");
            throw new Exception("Target Refund not on Refund List.");
        }

        driver.switchTo().frame(0);

        RefundDetailsView refundDetailsView = new RefundDetailsView(driver, scenarioOperator);

        if (refund.getApprove().equalsIgnoreCase("Yes")) {
            refundDetailsView.clickApprove();
            Thread.sleep(3000);
            if (refund.getYesDo().equalsIgnoreCase("Yes")) {
                refundDetailsView.clickYesApprove();
                Thread.sleep(1000);
            } else {
                refundDetailsView.clickNoDoNotApprove();
                Thread.sleep(1000);
            }
        } else {
            refundDetailsView.clickDecline();
            Thread.sleep(3000);
            if (refund.getYesDo().equalsIgnoreCase("Yes")) {
                refundDetailsView.clickYesDecline();
                Thread.sleep(1000);
            } else {
                refundDetailsView.clickNoDoNotDecline();
                Thread.sleep(1000);
            }
        }

        policyInformation.getResponses(true);
        log.info("Done Handling Approve Refunds logic");
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Saving Refunds.");
            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {refund.getPopupWorkTypeZero()};
            String[] popUpStatus = {refund.getPopupStatusZero()};
            String[] popUpQueue = {refund.getPopupQueueZero()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            genericMaintenance.clickSave();

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.proceedAfterClickingSave(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.proceedAfterClickingSave(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}