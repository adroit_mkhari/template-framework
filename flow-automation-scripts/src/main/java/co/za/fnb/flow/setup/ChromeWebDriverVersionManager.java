package co.za.fnb.flow.setup;

import io.github.bonigarcia.wdm.ChromeDriverManager;

import java.util.Optional;

public class ChromeWebDriverVersionManager extends ChromeDriverManager {

    public String getDefaultBrowserVersion() {
        Optional<String> browserVersion = getBrowserVersion();
        System.out.println("Browser Version: " + browserVersion.toString());
        String version = browserVersion.get();
        return version;
    }

}
