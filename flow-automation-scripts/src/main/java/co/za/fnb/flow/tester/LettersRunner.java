package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.Query;
import co.za.fnb.flow.models.QueryFactory;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.letters.MintUserInterface;
import co.za.fnb.flow.tester.letters.TmsLetterDownload;
import co.za.fnb.flow.tester.letters.helpers.*;
import co.za.fnb.flow.tester.services.GetPolicyDetailsTester;
import cucumber.api.DataTable;
import de.redsix.pdfcompare.CompareResult;
import de.redsix.pdfcompare.PageArea;
import de.redsix.pdfcompare.PdfComparator;
import generated.*;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;
import za.co.fnb.fnblife.midlife.FNBLifeInboundProxy;
import za.co.fnb.fnblife.midlife.FNBLifeInboundProxyServicePre;
import za.co.fnb.fnblife.midlife.FNBLifeInboundProxyServiceTst;
import za.co.fnb.fnblife.midlife.midlife_types.MidLifeHeaderType;
import za.co.fnb.fnblife.midlife.midlifemessage.MidLifeMessage;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.File;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static co.za.fnb.flow.pages.Config.*;

public class LettersRunner {
    private Logger log  = LogManager.getLogger(LettersRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;

    public LettersRunner() {
    }

    public LettersRunner(WebDriver driver) {
        this.driver = driver;
    }

    public LettersRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public LettersRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        if (!scenarioOperator.getProductName().toUpperCase().contains("LOC")) {
            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason", "Template Path", "Actual Pdf Path" , "Pdf Compare Results"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber;

        if (Arrays.asList(headers).contains("Test Case No")) {
            for (Row row : dataTable.getGherkinRows()) {
                if (row.getLine() != 1) {
                    dataEntry = row.getCells();
                    run = getCellValue(headers, dataEntry,"Run");
                    if (run.equalsIgnoreCase("YES")) {
                        testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                        scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                        testFunction = getCellValue(headers, dataEntry,"Function");
                        productName = getCellValue(headers, dataEntry,"Product Name");
                        scenarioOperator.setTestCaseNumber(testCaseNumber);
                        scenarioOperator.setScenarioDescription(scenarioDescription);
                        scenarioOperator.setFunction(testFunction);
                        scenarioOperator.setProductName(productName);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                        scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                        // BasePage.setScenarioOperator(scenarioOperator);

                        try {
                            policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                            policyNumberQuery = getCellValue(headers, dataEntry,"Policy Number Query");
                            policyCode = getCellValue(headers, dataEntry,"Policy No");
                            if (policyNumber.isEmpty()) {
                                try {
                                    if (policyNumberQuery.isEmpty()) {
                                        if (policyCode.isEmpty()) {
                                            policyNumber = "No Policy Number";
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        } else {
                                            Query query = QueryFactory.getQuery(policyCode);
                                            ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(query.getQuery());
                                            if (policyNumberQueryResults.next()) {
                                                policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                            } else {
                                                policyCode = query.getPolicyCode();
                                                policyNumberQuery = query.getQuery();
                                                policyNumber = null;
                                            }

                                            if (policyNumber == null) {
                                                throw new Exception("Error getting policy number for policy code: " + policyCode);
                                            }
                                            queryHandler.setDbPolicyNumber(policyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        }
                                    } else {
                                        ResultSet policyNumberQueryResults = queryHandler.getPolicyNumberQueryResults(policyNumberQuery);
                                        if (policyNumberQueryResults.next()) {
                                            policyNumber = policyNumberQueryResults.getString("POLICYNO").trim();
                                            queryHandler.setDbPolicyNumber(policyNumber);
                                            policyNumber = getFlowPolicyNumberFormat(productName, policyNumber);
                                            scenarioOperator.setPolicyNumber(policyNumber);
                                        }
                                    }
                                } catch (Exception e) {
                                    throw new Exception("Error getting policy number. Policy Code: " + policyCode + " Query: " + policyNumberQuery);
                                }
                            } else {
                                scenarioOperator.setPolicyNumber(policyNumber);
                                queryHandler.setDbPolicyNumber(policyNumber);
                            }

                            log.info("===========================================================================");
                            log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            log.info("---------------------------------------------------------------------------");

                            if (testFunction.equalsIgnoreCase("Letters")) {
                                letters(reportableFields, scenarioOperator, headers, dataEntry);
                                scenarioOperator.increamentReportRowIndex();
                            } else if (testFunction.equalsIgnoreCase("Pdf Comparison")) {
                                comparison(reportableFields, scenarioOperator, headers, dataEntry);
                                scenarioOperator.increamentReportRowIndex();
                            }
                            log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                        } catch (Exception e) {
                            log.error(e.getMessage());
                            e.printStackTrace();
                            reportFailure(reportableFields, scenarioOperator, e.getMessage());
                            scenarioOperator.increamentReportRowIndex();
                        } finally {
                            try {
                                if (driver.getWindowHandles().toArray().length > 1) {
                                    driver.close();
                                }
                            } catch (Exception e) {
                                // TODO
                            }
                        }
                    }
                }
            }
        }

        log.info("Saving Report.");
        scenarioOperator.saveReport();
        log.info("===========================================================================");
    }

    public void comparison(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        String scenarioDescription = getCellValue(headers, dataEntry, "Scenario Description");
        String productName = getCellValue(headers, dataEntry, "Product Name");
        String mintJob = getCellValue(headers, dataEntry, "Mint Job");
        String planType = getCellValue(headers, dataEntry, "Plan Type");
        String letterName = getCellValue(headers, dataEntry, "Letter Name");
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;

        String policyNumber = scenarioOperator.getPolicyNumber();
        scenarioOperator.setPolicyNumber(policyNumber);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        MintBatchJobNames mintBatchJobNames = MintBatchJobNames.valueOf(mintJob);

        String productLettersPath = templateLettersPath + "\\" + productName;
        String templateLetterName = "";
        templateLetterName = getTemplateDocumentName(mintBatchJobNames, templateLetterName, planType);
        Path templateLetterPath = Paths.get(productLettersPath + "\\templates" + "\\" + templateLetterName + ".pdf");
        Path templateLetterIgnorePath = Paths.get(productLettersPath + "\\ignore" + "\\" + templateLetterName + ".conf");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Template Path"), templateLetterPath.toString(), TestResultReportFlag.WARNING);

        Path toPath = getPath(productLettersPath + "\\actual\\named");
        boolean pathExistence = Files.exists(toPath);

        if (pathExistence) {
            String stringToPath = toPath.toString();

            Path letterPathName = getPath(stringToPath + "\\" + letterName + ".pdf");
            boolean letterPathNameExists = Files.exists(letterPathName);

            if (letterPathNameExists) {
                scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Actual Pdf Path"), letterPathName.toString(), TestResultReportFlag.WARNING);

                PdfCompare pdfCompare = performPdfComparison(testCaseNumber, scenarioDescription, letterPathName,
                        templateLetterPath, templateLetterIgnorePath, productLettersPath, templateLetterName);

                boolean pdfComparison = pdfCompare.isSame();
                if (pdfComparison) {
                    reportSuccess(reportableFields, scenarioOperator);
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Pdf Compare Results"), "No Compare Results File Gets Generated When The Files Are The Same.", TestResultReportFlag.WARNING);
                } else {
                    reportFailure(reportableFields, scenarioOperator, "Pdf Visual Comparison failed.");
                    scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Pdf Compare Results"), pdfCompare.getResultPath(), TestResultReportFlag.WARNING);
                }
            } else {
                log.error("No such file '" + letterName + "' on " + toPath);
                throw new Exception("No such file '" + letterName + "' on " + toPath);
            }
        } else {
            log.error("No such file path: " + toPath);
            throw new Exception("No such file path: " + toPath);
        }

    }

    private Path getPath(String path) throws Exception {
        Path pathName;
        try {
            pathName = Paths.get(path);
        } catch (Exception e) {
            log.error("Error while trying to get path: " + e.getMessage());
            throw new Exception("Error while trying to get path: " + e.getMessage());
        }
        return pathName;
    }

    private void letters(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Loading Properties");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        Properties properties = propertiesSetup.getProperties();
        String application = properties.getProperty("TSM_TARGET_APPLICATION");
        String csvDocumentsDrivePath = properties.getProperty("CSV_DOCUMENTS_DRIVE_PATH");
        String usernameForSerives = properties.getProperty("USERNAME");
        log.info("Running Add Member Logic");
        // region Data:
        int testCaseNumber = getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) != -1 ? getIntegerValue(getCellValue(headers, dataEntry, "Test Case No")) : 0;
        String tmsUserNameConfig = properties.getProperty("TMS_USERNAME");
        String tmsPasswordConfig = properties.getProperty("TMS_PASSWORD");

        String tmsUserNameDataSheet = getCellValue(headers, dataEntry, "Tms User Name");
        String tmsPasswordDataSheet = getCellValue(headers, dataEntry, "Tms Password");

        String tmsUserName = "";
        String tmsPassword = "";

        if (tmsUserNameConfig.isEmpty()) {
            if (!tmsUserNameDataSheet.isEmpty()) {
                tmsUserName = tmsUserNameDataSheet;
            } else {
                log.error("Error: No TMS User Name Found.");
                throw new Exception("Error: No TMS User Name Found.");
            }
        } else {
            tmsUserName = tmsUserNameConfig;
        }

        if (tmsPasswordConfig.isEmpty()) {
            if (!tmsPasswordDataSheet.isEmpty()) {
                tmsPassword = tmsPasswordDataSheet;
            } else {
                log.error("Error: No TMS Password Found.");
                throw new Exception("Error: No TMS Password Found.");
            }
        } else {
            tmsPassword = tmsPasswordConfig;
        }

        String scenarioDescription = getCellValue(headers, dataEntry, "Scenario Description");
        String productName = getCellValue(headers, dataEntry, "Product Name");
        String mintUserName = getCellValue(headers, dataEntry, "Mint User Name");
        String mintPassword = getCellValue(headers, dataEntry, "Mint Password");
        String mintJob = getCellValue(headers, dataEntry, "Mint Job");
        String mintJobId = getCellValue(headers, dataEntry, "Mint Job Id");
        String planType = getCellValue(headers, dataEntry, "Plan Type");
        String midLifeMessageId = getCellValue(headers, dataEntry, "Mid Life Message Id");
        String midLifeTimestamp = getCellValue(headers, dataEntry, "Mid Life Timestamp");
        String midLifeTargetServiceName = getCellValue(headers, dataEntry, "Mid Life Target Service Name");
        String midLifeTargetServiceOperation = getCellValue(headers, dataEntry, "Mid Life Target Service Operation");
        String midLifeOriginatingSystemName = getCellValue(headers, dataEntry, "Mid Life Originating System Name");
        String midLifeCorrelationId = getCellValue(headers, dataEntry, "Mid Life Correlation Id");
        String updateUcn = getCellValue(headers, dataEntry, "Update UCN");
        String flagLetters = getCellValue(headers, dataEntry, "Flag Letters");
        String insertFlaggingRecords = getCellValue(headers, dataEntry, "Insert Flagging Records");
        String runBatchOnMint = getCellValue(headers, dataEntry, "Run Batch On Mint");
        String checkLastSentCsv = getCellValue(headers, dataEntry, "Check Last Sent CSV");
        String runForceService = getCellValue(headers, dataEntry, "Run Force Service");
        String goToTms = getCellValue(headers, dataEntry, "Go To TMS");
        String sendAllLetters = getCellValue(headers, dataEntry, "Send All Selected Letters");
        String targetSendAddress = getCellValue(headers, dataEntry, "Target Send Address");
        String downloadLetter = getCellValue(headers, dataEntry, "Download Letter");
        String recordId = getCellValue(headers, dataEntry, "RECORD_ID");
        String flagtypId = getCellValue(headers, dataEntry, "FLAGTYP_ID");
        String flag = getCellValue(headers, dataEntry, "FLAG");
        String emailAddr = getCellValue(headers, dataEntry, "EMAILADDR");
        String usercapt = getCellValue(headers, dataEntry, "USERCAPT");
        String ucnNumber = getCellValue(headers, dataEntry, "UCN Number");
        // endregion

        String policyNumber = scenarioOperator.getPolicyNumber();
        scenarioOperator.setPolicyNumber(policyNumber);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            // Get The Policy Details:
            String policyHolderRoleId = "";
            String policyHolderIdNumber = "";
            String email = "";
            try {
                GetPolicyDetailRequestInput getPolicyDetailRequestInput = new GetPolicyDetailRequestInput();
                String flowPolicyNumberFormat = getFlowPolicyNumberFormat(productName, policyNumber);
                getPolicyDetailRequestInput.setPolicy(flowPolicyNumberFormat);
                getPolicyDetailRequestInput.setUsername(usernameForSerives);
                GetPolicyDetailsTester getPolicyDetailsTester = new GetPolicyDetailsTester(getPolicyDetailRequestInput);
                GetPolicyDetailResponseOutput getPolicyDetailResponseOutput = getPolicyDetailsTester.sendGetPolicyDetailsRequest();
                ResponsePolicy policy = getPolicyDetailResponseOutput.getPolicy();
                String policyStatus = policy.getPolicystatus();

                if (!policyStatus.equalsIgnoreCase("IN FORCE")) {
                    throw new Exception("Policy Status Not Correct: Found -> " + policyStatus);
                }

                ResponseRoles roles = policy.getRoles();

                List<ResponsePolicyRoleType> roleList = roles.getRole();
                for (ResponsePolicyRoleType role: roleList) {
                    String roleType = role.getRoletype();
                    if (!roleType.isEmpty()) {
                        if (roleType.equalsIgnoreCase("POLICY HOLDER")) {
                            policyHolderRoleId = role.getRoleid();
                            policyHolderIdNumber = role.getIdnumber();
                            email = role.getEmail();
                            break;
                        }
                    }
                }
            } catch (Exception e) {
                log.error("Error on Get Policy Details. Error: " + e.getMessage());
                throw new Exception("Error on Get Policy Details. Error: " + e.getMessage());
            }

            // Flag The Policy For Letters:
            QueryHandler queryHandler = new QueryHandler();

            if (updateUcn.equalsIgnoreCase("Yes")) {
                queryHandler.updateUCNNumber(policyNumber, policyHolderRoleId, policyHolderIdNumber, ucnNumber);
            }

            if (flagLetters.equalsIgnoreCase("Yes")) {
                queryHandler.unflagAllCurrentlyFlaggedPolicyForLetters(productName);
                if (insertFlaggingRecords.equalsIgnoreCase("Yes")) {
                    boolean hasLetterFlag = queryHandler.checkPolicyLetterFlag(productName, flagtypId, policyNumber);
                    if (!hasLetterFlag) {
                        queryHandler.insertLetterFlag(recordId, productName, flagtypId, flag, emailAddr, usercapt, policyNumber);
                    }
                }

                boolean isLetterFlagged = queryHandler.checkPolicyFlagForLetters(productName, flagtypId, policyNumber);
                if (!isLetterFlagged) {
                    queryHandler.flagPolicyForLetters(productName, flagtypId, policyNumber);
                }
            }

            // Get CSV File Text from the Network Drive.
            FilesHelper filesHelper = new FilesHelper(csvDocumentsDrivePath);
            boolean existence = filesHelper.checkExistence();
            List<Path> directoryStreamBefore = new ArrayList<>();
            List<Path> directoryStreamAfter = new ArrayList<>();

            String documentName = "";
            MintBatchJobNames mintBatchJobNames = MintBatchJobNames.valueOf(mintJob);
            documentName = getDocumentName(mintBatchJobNames, documentName, planType);

            String downloadDocumentName = "";
            downloadDocumentName = getDownloadDocumentName(mintBatchJobNames, downloadDocumentName, planType);

            if (existence) {
                DirectoryStream<Path> directoryStream = filesHelper.getDirectoryStream();

                try {
                    for (Path path : directoryStream) {
                        Path fileName = path.getFileName();
                        String fileNameString = fileName.toString();
                        if (fileNameString.toUpperCase().contains(documentName.toUpperCase())) {
                            directoryStreamBefore.add(path);
                        }
                    }
                } catch (Exception e) {
                    log.error("Error while iterating through directory stream before: " + e.getMessage());
                    // TODO: Uncomment this.
                    // throw new Exception("Error while iterating through directory stream before: " + e.getMessage());
                }
            }

            CsvQueueItem lastSentCsvQueueItemBeforeBatch = queryHandler.getLastSentCsvQueueItem();

            // Run The Letters Generations Batch Job On Mint:
            log.info("Running Job For: " + policyNumber);

            if (runBatchOnMint.equalsIgnoreCase("Yes")) {
                MintUserInterface mintUserInterface = new MintUserInterface();
                mintUserInterface.signIn(mintUserName, mintPassword);
                mintUserInterface.continueSignIn(mintUserName, mintPassword);

                mintUserInterface.openJobTerminal(mintJobId);

                if (mintJob.equalsIgnoreCase("GSGSTMT1BH")) {
                    String prodCode = getBatchProductCode(productName);
                    log.debug("Computed Prod Code: " + prodCode);
                    mintUserInterface.runBatchJob("CALL " + mintJob + "('" + prodCode + "')" );
                } else {
                    mintUserInterface.runBatchJob("CALL " + mintJob);
                }

                // TODO: Check Annual Statement Batch Job Name.
                mintUserInterface.closeMint();
            }

            log.info("Done Running Job For: " + policyNumber);
            Thread.sleep(5000);

            CsvQueueItem lastSentCsvQueueItemAfterBatch = queryHandler.getLastSentCsvQueueItem();
            String lastSentCsvQueueItemBeforeBatchId = lastSentCsvQueueItemBeforeBatch.getId();
            String lastSentCsvQueueItemAfterBatchId = lastSentCsvQueueItemAfterBatch.getId();
            int traceCounter = 10;
            while (lastSentCsvQueueItemBeforeBatchId.equalsIgnoreCase(lastSentCsvQueueItemAfterBatchId) && traceCounter-- > 1) {
                Thread.sleep(5000);
                lastSentCsvQueueItemAfterBatch = queryHandler.getLastSentCsvQueueItem();
                lastSentCsvQueueItemAfterBatchId = lastSentCsvQueueItemAfterBatch.getId();
            }

            if (checkLastSentCsv.equalsIgnoreCase("Yes")) {
                boolean gotNewCsvQueueItem = !lastSentCsvQueueItemBeforeBatchId.equalsIgnoreCase(lastSentCsvQueueItemAfterBatchId);
                if (!gotNewCsvQueueItem) {
                    throw new Exception("No New Csv Item Was Added On The Queue.");
                }

                filesHelper = new FilesHelper(csvDocumentsDrivePath);
                existence = filesHelper.checkExistence();
                if (existence) {
                    DirectoryStream<Path> directoryStream = filesHelper.getDirectoryStream();
                    try {
                        for (Path path : directoryStream) {
                            Path fileName = path.getFileName();
                            String fileNameString = fileName.toString();
                            if (fileNameString.toUpperCase().contains(documentName.toUpperCase())) {
                                directoryStreamAfter.add(path);
                            }
                        }
                    } catch (Exception e) {
                        log.error("Error while iterating through directory stream before: " + e.getMessage());
                        // TODO: Uncomment this.
                        // throw new Exception("Error while iterating through directory stream before: " + e.getMessage());
                    }
                }

                Integer directoryStreamSizeBefore = directoryStreamBefore.size();
                Integer directoryStreamSizeAfter = directoryStreamAfter.size();

                Path targetFilePath = null;
                if (directoryStreamSizeBefore.equals(directoryStreamSizeAfter)) {
                    Instant lastModifiedTime = null;
                    boolean first = true;
                    for (Path path: directoryStreamBefore) {
                        if (first) {
                            first = false;
                            lastModifiedTime = Files.getLastModifiedTime(path).toInstant();
                            targetFilePath = path;
                            continue;
                        }

                        Instant fileLastModifiedTime = Files.getLastModifiedTime(path).toInstant();
                        if (fileLastModifiedTime.isAfter(lastModifiedTime) || fileLastModifiedTime.equals(lastModifiedTime)) {
                            targetFilePath = path;
                            lastModifiedTime = fileLastModifiedTime;
                        }
                    }
                } else {
                    for (Path path: directoryStreamAfter) {
                        if (!directoryStreamBefore.contains(path)) {
                            log.info("Got Path: " + path);
                            targetFilePath = path;
                            break;
                        }
                    }
                }

                String csvQueueItemAfterBatchFileName = lastSentCsvQueueItemAfterBatch.getFileName();
                Path targetFilePathFileName = targetFilePath.getFileName();
                String targetFileName = "";

                if (targetFilePathFileName !=  null) {
                    targetFileName = targetFilePathFileName.toString();
                }

                if (!csvQueueItemAfterBatchFileName.contains(targetFileName)) {
                    log.debug("Csv File Name Not Correct. Network Drive: " + targetFileName + " , Database: " + csvQueueItemAfterBatchFileName);
                    // throw new Exception("Csv File Name Not Correct. Network Drive: "     + targetFileName + " , Database: " + csvQueueItemAfterBatchFileName);
                }

                List<String> fileContent = null;
                if (targetFilePath != null) {
                    String fileName = targetFilePath.getFileName().toString();
                    fileContent = filesHelper.getFileLineContent(fileName);
                } else {
                    log.error("No Target CSV File Was Generated.");
                    // TODO: Uncomment this.
                    // throw new Exception("No Target CSV File Was Generated.");
                }

                if (fileContent != null) {
                    for (String line : fileContent) {
                        log.info(line);
                    }
                }
            }

            // region Run The Force Service:
            if (runForceService.equalsIgnoreCase("Yes")) {
                String lastSentRequestId = queryHandler.getLastSentRequestId();
                Integer lastSentRequestIdValue = Integer.valueOf(lastSentRequestId);

                FNBLifeInboundProxyServicePre fnbLifeInboundProxyServicePre = new FNBLifeInboundProxyServicePre();
                FNBLifeInboundProxyServiceTst fnbLifeInboundProxyServiceTst = new FNBLifeInboundProxyServiceTst();
                FNBLifeInboundProxy fnbLifeInboundProxyServiceGSD00030PRServicesPort;

                if (application.contains("ecm-pp")) {
                    fnbLifeInboundProxyServiceGSD00030PRServicesPort = fnbLifeInboundProxyServicePre.getFNBLifeInboundProxyPort();
                } else {
                    fnbLifeInboundProxyServiceGSD00030PRServicesPort = fnbLifeInboundProxyServiceTst.getFNBLifeInboundProxyPort();
                }

                MidLifeMessage midLifeMessage = new MidLifeMessage();
                MidLifeHeaderType midLifeHeaderType = new MidLifeHeaderType();
                midLifeHeaderType.setMessageId(midLifeMessageId);
                XMLGregorianCalendar xmlGregorianCalendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(midLifeTimestamp);
                midLifeHeaderType.setTimestamp(xmlGregorianCalendar);
                midLifeHeaderType.setTargetServiceName(midLifeTargetServiceName);
                midLifeHeaderType.setTargetServiceOperation(midLifeTargetServiceOperation);
                midLifeHeaderType.setOriginatingSystemName(midLifeOriginatingSystemName);
                midLifeHeaderType.setCorrelationId(midLifeCorrelationId);
                midLifeMessage.setHeader(midLifeHeaderType);
                fnbLifeInboundProxyServiceGSD00030PRServicesPort.sendAsyncRequest(midLifeMessage);

                Thread.sleep(60000);

                if (checkLastSentCsv.equalsIgnoreCase("Yes")) {
                    CsvQueueItem lastSentCsvQueueItemAfterForceService = queryHandler.getCsvQueueItem(lastSentCsvQueueItemAfterBatchId);
                    String procFlag = lastSentCsvQueueItemAfterForceService.getProcFlag();
                    traceCounter = 5;
                    while (!procFlag.equalsIgnoreCase("D") && traceCounter-- > 1) {
                        Thread.sleep(5000);
                        lastSentCsvQueueItemAfterForceService = queryHandler.getCsvQueueItem(lastSentCsvQueueItemAfterBatchId);
                        procFlag = lastSentCsvQueueItemAfterForceService.getProcFlag();
                    }

                    boolean csvDelivered = procFlag.equalsIgnoreCase("D");

                    String sentRequestId = queryHandler.getLastSentRequestId();
                    Integer sentRequestIdValue = Integer.valueOf(sentRequestId);

                    if (csvDelivered) {
                        String xmlin = "", xmlout = "";
                        if (!lastSentRequestIdValue.equals(sentRequestIdValue)) {
                            ResultSet sentRequest = queryHandler.getSentRequest(policyNumber, lastSentRequestId);
                            if (sentRequest.next()) {
                                xmlin = sentRequest.getString("XMLIN");
                                xmlout = sentRequest.getString("XMLOUT");
                                log.info("XMLIN: " + xmlin);
                                log.info("XMLOUT: " + xmlout);
                            } else {
                                log.info("Record Found For This Policy On The TMS Table.");
                                // TODO: Retry Once More
                            }
                        } else {
                            log.error("No Record Was Added After The Record Id " + lastSentRequestId + " For Policy Number " + policyNumber);
                            // TODO: Check If The TMS Response Table Is Working.
                            // throw new Exception("No Record Was Added After The Record Id " + lastSentRequestId + " For Policy Number " + policyNumber);
                        }

                        if (!xmlin.isEmpty()) {
                            if (xmlin.contains("<header>TMSLetterResponse - Failure</header>")) {
                                throw new Exception("Error while forcing letter generation: " + xmlin);
                            }
                        } else if (!xmlout.isEmpty()) {
                            if (xmlout.contains("error")) {
                                throw new Exception("Error while forcing letter generation: " + xmlout);
                            }
                        }
                    } else {
                        log.debug("No TMS Response After Running The Force Service. Letter Delivery Proc. Flag: " + procFlag);
                        // TODO: Do Not Go To TMS When It's Not Yet Delivered. But For Now Go To TMS Anyway.
                        // throw new Exception("No TMS Response After Running The Force Service. Letter Delivery Proc. Flag: " + procFlag);
                    }
                }

            }
            // endregion

            // region Go To TMS:
            if (goToTms.equalsIgnoreCase("Yes")) {
                DriverSetup driverSetup = new DriverSetup();
                driverSetup.setProperties(properties);
                driverSetup.setBrowserCapabilities();
                driver = driverSetup.getDriver();
                driver.get(application);

                TmsLetterDownload tmsLetterDownload = new TmsLetterDownload(driver, scenarioOperator);
                tmsLetterDownload.maximizeWindow(0);
                tmsLetterDownload.login(tmsUserName, tmsPassword);

                String expectedResults = null;
                tmsLetterDownload.setExpectedResults(expectedResults);
                log.info("Tms Letter Download, Policy Number: " + scenarioOperator.getPolicyNumber());
                email = targetSendAddress;
                boolean isEmail = !email.isEmpty();
                String documentDescription = "";
                documentDescription = getTmsDocumentDescription(mintBatchJobNames, documentDescription, isEmail, planType);

                tmsLetterDownload.searchTargetLetters(documentDescription);

                boolean download = downloadLetter.equalsIgnoreCase("Yes");
                boolean sendAll = sendAllLetters.equalsIgnoreCase("Yes");

                if (sendAll) {
                    tmsLetterDownload.sendAll(downloadDocumentName, targetSendAddress);
                }

                if (download) {
                    tmsLetterDownload.download(downloadDocumentName);
                }

                log.info("Saving Tms Letter Download.");
                tmsLetterDownload.save();

                driver.close();

                if (download) {
                    filesHelper = new FilesHelper(driverDownloadPath);
                    existence = filesHelper.checkExistence();
                    Path newFilePath = null;
                    if (existence) {
                        DirectoryStream<Path> directoryStream = filesHelper.getDirectoryStream();

                        try {
                            Instant lastModifiedTime = null;
                            boolean first = true;
                            for (Path path: directoryStream) {
                                if (first) {
                                    first = false;
                                    lastModifiedTime = Files.getLastModifiedTime(path).toInstant();
                                    newFilePath = path;
                                    continue;
                                }

                                Instant fileLastModifiedTime = Files.getLastModifiedTime(path).toInstant();
                                if (fileLastModifiedTime.isAfter(lastModifiedTime)) {
                                    newFilePath = path;
                                    lastModifiedTime = fileLastModifiedTime;
                                }
                            }
                        } catch (Exception e) {
                            log.error("Error while iterating through downloads directory stream: " + e.getMessage());
                            throw new Exception("Error while iterating through downloads directory stream: " + e.getMessage());
                        }

                        String productLettersPath = templateLettersPath + "\\" + productName;
                        if (newFilePath != null) {
                            File sourceFile = newFilePath.toFile();
                            Path toPath = Paths.get(productLettersPath + "\\actual");
                            boolean pathExistence = Files.exists(toPath);

                            if (!pathExistence) {
                                Files.createDirectories(toPath);
                            }

                            DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy HH_mm_ss");
                            LocalDateTime now = LocalDateTime.now();
                            String systemTime = format.format(now);

                            File targetFile = new File(toPath.toString() + "\\" + documentName + " " + systemTime + ".pdf");
                            boolean renamedTo = sourceFile.renameTo(targetFile);
                            if (renamedTo) {
                                newFilePath = targetFile.toPath();
                                log.error("Successfully Renamed File To: " + newFilePath);
                            } else {
                                log.error("Failed To Rename File.");
                            }
                        }

                        String templateLetterName = "";
                        templateLetterName = getTemplateDocumentName(mintBatchJobNames, templateLetterName, planType);
                        Path templateLetterPath = Paths.get(productLettersPath + "\\templates" + "\\" + templateLetterName + ".pdf");
                        Path templateLetterIgnorePath = Paths.get(productLettersPath + "\\ignore" + "\\" + templateLetterName + ".conf");

                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Template Path"), templateLetterPath.toString(), TestResultReportFlag.WARNING);
                        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Actual Pdf Path"), newFilePath.toString(), TestResultReportFlag.WARNING);

                        PdfCompare pdfCompare = performPdfComparison(testCaseNumber, scenarioDescription, newFilePath,
                                templateLetterPath, templateLetterIgnorePath, productLettersPath, templateLetterName);
                        boolean pdfComparison = pdfCompare.isSame();

                        if (pdfComparison) {
                            reportSuccess(reportableFields, scenarioOperator);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Pdf Compare Results"), "No Compare Results File Gets Generated When The Files Are The Same.", TestResultReportFlag.WARNING);
                        } else {
                            reportFailure(reportableFields, scenarioOperator, "Pdf Visual Comparison failed.");
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Pdf Compare Results"), pdfCompare.getResultPath(), TestResultReportFlag.WARNING);
                        }
                    }
                }
            }
            // endregion

            // TODO: Reporting For Letters
            // testHandle = tmsLetterDownload.getTestHandle();
            // errorHandle = tmsLetterDownload.getErrorHandle();
            // if (testHandle.isSuccess()) {
            //     reportSuccess(reportableFields, scenarioOperator);
            // } else {
            //     reportFailure(reportableFields, scenarioOperator, errorHandle.getError());
            // }
            // endregion
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of TMS Letter Download Logic");
    }

    private String getBatchProductCode(String productName) {
        if (productName.equalsIgnoreCase("Funeral Insurance")) {
            return "FI";
        } else if (productName.equalsIgnoreCase("Old Funeral Insurance")) {
        } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
            return "HCP";
        } else if (productName.contains("LOC")) {
        } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
        } else if (productName.equalsIgnoreCase("Pay Protect")) {
        } else if (productName.equalsIgnoreCase("Cover For Life")) {
        }
        return "";
    }

    private PdfCompare performPdfComparison(int testCaseNumber, String scenarioDescription, Path newFilePath, Path templateLetterPath, Path templateLetterIgnorePath, String productLettersPath, String templateLetterName) throws Exception {
        FilesHelper filesHelper;
        if ((newFilePath != null) && (templateLetterPath != null)) {
            filesHelper = new FilesHelper();
            filesHelper.setPath(newFilePath);

            if (newFilePath.toString().contains(".pdf")) {
                // filesHelper = new FilesHelper();
                // boolean comparePdfFiles = filesHelper.comparePdfFiles(templateLetterPath, newFilePath, true);
                // boolean compareTexts = filesHelper.compareTexts(templateLetterPath, newFilePath, true);

                String pdfLettersCompareResults = productLettersPath + "\\results\\" + templateLetterName;
                String scenarioTest = pdfLettersCompareResults + "\\" + scenarioDescription + "_" + testCaseNumber;

                DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy HH_mm_ss");
                LocalDateTime now = LocalDateTime.now();
                String systemTime = format.format(now);
                String scenarioCompareResults = scenarioTest + "\\" + systemTime;
                getTargetPath(scenarioCompareResults);
                Path compareResultsPath = Paths.get(scenarioCompareResults);

                boolean templateLetterPathExists = Files.exists(templateLetterPath);
                if (!templateLetterPathExists) {
                    throw new Exception("Template Path Does Not Exist. Path -> " + templateLetterPath);
                }

                boolean newFilePathExists = Files.exists(newFilePath);
                if (!newFilePathExists) {
                    throw new Exception("Actual File (downloaded/named added) Path Does Not Exist. Path -> " + newFilePath);
                }

                boolean templateLetterIgnorePathExists = Files.exists(templateLetterIgnorePath);
                if (!templateLetterIgnorePathExists) {
                    throw new Exception("Template Ignore Path Does Not Exist. Path -> " + templateLetterIgnorePath);
                }

                try {
                    PdfComparator pdfComparator = new PdfComparator(templateLetterPath, newFilePath).withIgnore(templateLetterIgnorePath);
                    CompareResult compare = pdfComparator.compare();
                    boolean notEqual = compare.isNotEqual();

                    if (notEqual) {
                        Collection<PageArea> differences = compare.getDifferences();
                        for (PageArea pageArea : differences) {
                            log.info("{");

                            try {
                                log.info("page: " + pageArea.getPage());
                            } catch (Exception e) {
                                log.debug("Failed To Get Page Area Page");
                            }

                            try {
                                log.info("x1: " + pageArea.getX1());
                            } catch (Exception e) {
                                log.debug("Failed To Get Page Area x1");
                            }

                            try {
                                log.info("y1: " + pageArea.getY1());
                            } catch (Exception e) {
                                log.debug("Failed To Get Page Area y1");
                            }

                            try {
                                log.info("x2: " + pageArea.getX2());
                            } catch (Exception e) {
                                log.debug("Failed To Get Page Area x2");
                            }

                            try {
                                log.info("y2: " + pageArea.getY2());
                            } catch (Exception e) {
                                log.debug("Failed To Get Page Area y2");
                            }

                            log.info("},");
                        }

                        String resultPath = compareResultsPath.toString() + "\\" + templateLetterName + "_Compare_Results.pdf";
                        compare.writeTo(resultPath);
                        return new PdfCompare(false, resultPath);
                    }  else {
                        return new PdfCompare(true, "");
                    }

                    // TemplateLettersIgnore templateLettersIgnore;
                    // String templateLettersIgnoreRegexes;

                    // try {
                    //     templateLettersIgnore = TemplateLettersIgnore.valueOf(templateLetterName);
                    //     templateLettersIgnoreRegexes = templateLettersIgnore.toString();
                    // } catch (IllegalArgumentException e) {
                    //     log.debug("No TemplateLettersIgnore Values Set For: " + templateLetterName);
                    //     templateLettersIgnoreRegexes = "";
                    // }
                    // boolean compareFilesPDFUtilTextMode = filesHelper.compareFilesPDFUtilTextMode(templateLetterPath, newFilePath, templateLettersIgnoreRegexes);

                    // if (!compareFilesPDFUtilTextMode) {
                    //    log.error("Pdf Content Comparison Failed.");
                    // } else {
                    //     log.info("Pdf Content Comparison Passed.");
                    // }
                } catch (IOException e) {
                    // TODO: Handle Errors
                    e.printStackTrace();
                }
            }
        }
        return new PdfCompare(false, "");
    }

    private void getTargetPath(String pdfLettersCompareResults) {
        Path pdfLettersCompareResultsPath = Paths.get(pdfLettersCompareResults);
        boolean pathExists = Files.exists(pdfLettersCompareResultsPath);
        if (!pathExists) {
            Path absolutePath = pdfLettersCompareResultsPath.toAbsolutePath();
            try {
                Files.createDirectories(absolutePath);
            } catch (IOException e) {
                // TODO: ...
                e.printStackTrace();
            }
        }
    }

    private Path getTemplateLetterPath(String mintJob) {
        if (mintJob.contains("FILET01BH")) {
            // FNB Life Insurance Policy
            return Paths.get(templateLettersPath + "\\UND WELCOME LETTER.pdf");
        } else if (mintJob.contains("FILET03BH")) {
            // FNB Life Insurance Policy
            return Paths.get(templateLettersPath + "\\UND ENDORSEMENT LETTER.pdf");
        } else if (mintJob.contains("FILET04BH")) {
            // documentName = "CONTINUATION";
            return Paths.get(templateLettersPath + "\\template path");
        } else if (mintJob.contains("LCGENLTBH ('WELCOME')")) {
            // FNB Law on Call Business Plan
            return Paths.get(templateLettersPath + "\\WELCOME LETTER.pdf");
        } else if (mintJob.contains("LCGENLTBH ('ENDORSEMENT')")) {
            // FNB Law on Call Personal Plan
            return Paths.get(templateLettersPath + "\\ENDORSEMENT LETTER (CLIENT COMM).pdf");
        } else if (mintJob.contains("CALL  LCGENLTBH ('CANCEL')")) {
            // documentName = "CANCEL";
            return Paths.get(templateLettersPath + "\\template path");
        } else if (mintJob.contains("PGM(LCGENLTBH) PARM('REINSTATE')")) {
            // FNB Law on Call Personal Plan
            return Paths.get(templateLettersPath + "\\REINSTATEMENT LETTER.pdf");
        }
        return null;
    }

    private String getTemplateDocumentName(MintBatchJobNames mintJob, String templateLetterName, String planType) {
        switch (mintJob) {
            case FILET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case FILET03BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case FILET04BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case FILET14BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case FICLAIMSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case FSGLET01BH:
                templateLetterName = "Letter_FuneralOld_Welcome_";
                break;
            case FSGLET03BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "OLDFUN_ENDORSEMENT_";
                } else {
                    templateLetterName = "Letter_FuneralOld_Endorsement_";
                }
                break;
            case FSGLET10BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "ADHOC_GENERIC_OLDFUN_TMS_";
                } else {
                    templateLetterName = "ADHOC_GENERIC_OLDFUN_";
                }
                break;
            case HCPCLAIMSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case HCPGENLTSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case HCPLET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case HCPLET04BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case HCPLET06BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case HCPPRNSTSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case C4LLET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case C4LLET02BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case C4LLET03BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case C4LLET04BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PSGLET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PSGLET02BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PSGLET03BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PSGLET99BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PA_GENLTSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL POST")) {
                    templateLetterName = "AD_Generic Letter_Individual_Post";
                } else if (planType.equalsIgnoreCase("INDIVIDUAL EMAIL")) {
                    templateLetterName = "AD_Generic_Individual_Email";
                } else if (planType.equalsIgnoreCase("FAMILY POST")) {
                    templateLetterName = "AD_Generic_Family_Post";
                } else if (planType.equalsIgnoreCase("FAMILY EMAIL")) {
                    templateLetterName = "AD_Generic Letter_Family_Email";
                }
                break;
            case PA_LET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL POST")) {
                    templateLetterName = "AD_Welcome Letter_Individual_Post";
                } else if (planType.equalsIgnoreCase("INDIVIDUAL EMAIL")){
                    templateLetterName = "AD_Welcome_Individual_Email";
                }else if (planType.equalsIgnoreCase("FAMILY POST")){
                    templateLetterName = "AD_Welcome_Family_Post";
                }else if (planType.equalsIgnoreCase("FAMILY EMAIL")){
                    templateLetterName = "AD_Welcome_Family_Email";
                }
                break;
            case PA_LET02BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL POST")) {
                    templateLetterName = "AD_Amendment Letter_Individual_Post";
                } else if (planType.equalsIgnoreCase("INDIVIDUAL EMAIL")) {
                    templateLetterName = "AD_Amendment_Individual_Email";
                } else if (planType.equalsIgnoreCase("FAMILY POST")){
                    templateLetterName = "AD_Amendment_Family_Post";
                } else if (planType.equalsIgnoreCase("FAMILY EMAIL")){
                    templateLetterName = "AD_Amendment Letter_Family_Email";
                } else if (planType.equalsIgnoreCase("NO CHILD POST")){
                    templateLetterName = "AD_Amendment Letter No Child_Post";
                } else if (planType.equalsIgnoreCase("NO CHILD EMAIL")){
                    templateLetterName = "AD_Amendment Letter No Child_Email";
                }
                break;
            case LP_CLAIMSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case LP_LET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case LP_LET02BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case LP_GENLTSH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case OL_LET01BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case OL_LET02BH:
                if (planType.equalsIgnoreCase("INDIVIDUAL")) {
                    templateLetterName = "";
                } else {
                    templateLetterName = "";
                }
                break;
            case PPLET01BH:
                templateLetterName = "PP_Welcome";
                break;
            case PPLET04BH:
                templateLetterName = "PP_Amendment";
                break;
            case PPLET06BH:
                templateLetterName = "PP_Generic";
                break;
            case PPCLAIMSH:
                templateLetterName = "Pay_Prot_Claim_Notification_xPresso";
                break;
            case GSGSTMT1BH:
                if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL POST")) {
                    templateLetterName = "PP_Annual_Statement";
                } else if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL EMAIL")) {
                    templateLetterName = "PP_Annual_Statement";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY POST")) {
                    templateLetterName = "PP_Annual_Statement";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY EMAIL")) {
                    templateLetterName = "PP_Annual_Statement";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL POST")) {
                    templateLetterName = "AD_Annual_Statement_Individual_Post";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL EMAIL")) {
                    templateLetterName = "AD_Amendment Letter_Individual_Post";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY POST")) {
                    templateLetterName = "AD_Annual_Statement_Family_Post";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY EMAIL")) {
                    templateLetterName = "AD_Amendment Letter_Individual_Post";
                }
                break;
        }

        return templateLetterName;
    }

    private String getDocumentName(MintBatchJobNames mintJob, String documentName, String planType) {
        switch (mintJob) {
            case FILET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case FILET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case FILET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case FILET14BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case FICLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case FSGLET01BH:
                documentName = "Letter_FuneralOld_Welcome_";
                break;
            case FSGLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "OLDFUN_ENDORSEMENT_";
                } else {
                    documentName = "Letter_FuneralOld_Endorsement_";
                }
                break;
            case FSGLET10BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "ADHOC_GENERIC_OLDFUN_TMS_";
                } else {
                    documentName = "ADHOC_GENERIC_OLDFUN_";
                }
                break;
            case HCPCLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case HCPGENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case HCPLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "WELCOME LETTER";
                } else {
                    documentName = "WELCOME LETTER";
                }
                break;
            case HCPLET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "ENDORSEMENT LETTER (CLIENT COMM)";
                } else {
                    documentName = "ENDORSEMENT LETTER (CLIENT COMM)";
                }
                break;
            case HCPLET06BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case HCPPRNSTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case C4LLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case C4LLET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case C4LLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case C4LLET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PSGLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PSGLET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PSGLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PSGLET99BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PA_GENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "ADHOC_GENERIC_PA_INDIVIDUAL_";
                } else {
                    documentName = "ADHOC_GENERIC_PA_FAMILY_";
                }
                break;
            case PA_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "Letter_Personal_Accident_Welcome_PANO_";
                } else {
                    documentName = "Letter_Personal_Accident_Welcome_PAWITH_";
                }
                break;
            case PA_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "Letter_Personal_Accident_Endorsement_PANO_";
                } else {
                    documentName = "Letter_Personal_Accident_Endorsement_PAWITH_";
                }
                break;
            case LP_CLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case LP_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case LP_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case LP_GENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case OL_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case OL_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    documentName = "";
                } else {
                    documentName = "";
                }
                break;
            case PPLET01BH:
                documentName = "PAYPROTECT_WELCOME_";
                break;
            case PPLET04BH:
                documentName = "PAYPROTECT_ENDORSEMENT_";
                break;
            case PPLET06BH:
                documentName = "ADHOC_GENERIC_PAYP_";
                break;
            case PPCLAIMSH:
                documentName = "PAYPROTECT_CLAIMS_INITIAL_NOTIFICATION_";
                break;
            case GSGSTMT1BH:
                // TODO: Get CSV Document Names For Annual
                if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL POST")) {
                    documentName = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL EMAIL")) {
                    documentName = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY POST")) {
                    documentName = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY EMAIL")) {
                    documentName = "";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL POST")) {
                    documentName = "ANNUAL_STATEMENTS_PA_SINGLE_";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL EMAIL")) {
                    documentName = "ANNUAL_STATEMENTS_PA_SINGLE_";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY POST")) {
                    documentName = "ANNUAL_STATEMENTS_PA_FAMILY_";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY EMAIL")) {
                    documentName = "ANNUAL_STATEMENTS_PA_FAMILY_";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY EMAIL")) {
                    documentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY POST")) {
                    documentName = "ANNUAL STATEMENT";
                }
                break;
        }

        return documentName;
    }

    private String getDownloadDocumentName(MintBatchJobNames mintJob, String downloadDocumentName, String planType) {
        switch (mintJob) {
            case FILET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case FILET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case FILET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case FILET14BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case FICLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case FSGLET01BH:
                downloadDocumentName = "Welcome";
                break;
            case FSGLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "ENDORSEMENT";
                } else {
                    downloadDocumentName = "Endorsement";
                }
                break;
            case FSGLET10BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "GENERIC";
                } else {
                    downloadDocumentName = "GENERIC";
                }
                break;
            case HCPCLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case HCPGENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case HCPLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "WELCOME LETTER";
                } else {
                    downloadDocumentName = "WELCOME LETTER";
                }
                break;
            case HCPLET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "ENDORSEMENT LETTER (CLIENT COMM)";
                } else {
                    downloadDocumentName = "ENDORSEMENT LETTER (CLIENT COMM)";
                }
                break;
            case HCPLET06BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case HCPPRNSTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case C4LLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case C4LLET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case C4LLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case C4LLET04BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PSGLET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PSGLET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PSGLET03BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PSGLET99BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PA_GENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "ADHOC";
                } else {
                    downloadDocumentName = "ADHOC";
                }
                break;
            case PA_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "WELCOME LETTER";
                } else {
                    downloadDocumentName = "WELCOME LETTER";
                }
                break;
            case PA_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "Endorsement";
                } else {
                    downloadDocumentName = "Endorsement";
                }
                break;
            case LP_CLAIMSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case LP_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case LP_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case LP_GENLTSH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case OL_LET01BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case OL_LET02BH:
                if (planType.toUpperCase().contains("INDIVIDUAL")) {
                    downloadDocumentName = "";
                } else {
                    downloadDocumentName = "";
                }
                break;
            case PPLET01BH:
                downloadDocumentName = "WELCOME";
                break;
            case PPLET04BH:
                downloadDocumentName = "ENDORSEMENT_";
                break;
            case PPLET06BH:
                downloadDocumentName = "GENERIC";
                break;
            case PPCLAIMSH:
                downloadDocumentName = "CLAIMS";
                break;
            case GSGSTMT1BH:
                if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL POST")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL EMAIL")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY POST")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY EMAIL")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL POST")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL EMAIL")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY POST")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY EMAIL")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY EMAIL")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY POST")) {
                    downloadDocumentName = "ANNUAL STATEMENT";
                }
                break;
        }

        return downloadDocumentName;
    }

    private String getTmsDocumentDescription(MintBatchJobNames mintJob, String tmsDocumentDescription, boolean isEmail, String planType) {
        switch (mintJob) {
            case FILET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FILET03BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FILET04BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FILET14BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FICLAIMSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FSGLET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FSGLET03BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case FSGLET10BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case HCPCLAIMSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case HCPGENLTSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case HCPLET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "FNB_LIFE_HEALTH_CASH_V1";
               } 
               else {                   
                   tmsDocumentDescription = "FNB_LIFE_HEALTH_CASH_V1";
               }
               break;
            case HCPLET04BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "FNB_LIFE_HEALTH_CASH_V1";
               } 
               else {                   
                   tmsDocumentDescription = "FNB_LIFE_HEALTH_CASH_V1";
               }
               break;
            case HCPLET06BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case HCPPRNSTSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case C4LLET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case C4LLET02BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case C4LLET03BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case C4LLET04BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PSGLET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PSGLET02BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PSGLET03BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PSGLET99BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PA_GENLTSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1";
               } 
               else {                   
                   tmsDocumentDescription = "FNB_LIFE_ACC_DEATH_V1";
               }
               break;
            case PA_LET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1";
               } 
               else {                   
                   tmsDocumentDescription = "FNB_LIFE_ACC_DEATH_V1";
               }
               break;
            case PA_LET02BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1";
               } 
               else {                   
                   tmsDocumentDescription = "FNB_LIFE_ACC_DEATH_V1";
               }
               break;
            case LP_CLAIMSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case LP_LET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case LP_LET02BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case LP_GENLTSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case OL_LET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case OL_LET02BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PPLET01BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PPLET04BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PPLET06BH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case PPCLAIMSH:
               if (isEmail) {                    
                   tmsDocumentDescription = "";
               } 
               else {                   
                   tmsDocumentDescription = "";
               }
               break;
            case GSGSTMT1BH:
                if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL POST")) {
                    tmsDocumentDescription = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL INDIVIDUAL EMAIL")) {
                    tmsDocumentDescription = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY POST")) {
                    tmsDocumentDescription = "";
                } else if (planType.equalsIgnoreCase("PP ANNUAL FAMILY EMAIL")) {
                    tmsDocumentDescription = "";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL POST")) {
                    tmsDocumentDescription = "FNB_LIFE_ACC_DEATH_V1";
                } else if (planType.equalsIgnoreCase("AD ANNUAL INDIVIDUAL EMAIL")) {
                    tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1 ";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY POST")) {
                    tmsDocumentDescription = "FNB_LIFE_ACC_DEATH_V1";
                } else if (planType.equalsIgnoreCase("AD ANNUAL FAMILY EMAIL")) {
                    tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY EMAIL")) {
                    tmsDocumentDescription = "FNB_LIFE_CLP_EMAIL_V1";
                } else if (planType.equalsIgnoreCase("HCP ANNUAL FAMILY POST")) {
                    tmsDocumentDescription = "FNB_LIFE_HEALTH_CASH_V1";
                }
                break;
        }

        return tmsDocumentDescription;
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
