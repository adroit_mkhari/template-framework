package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateUserViewPageObjects {

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnClearFilters")
    private WebElement clearUser;

    @FindBy(id = "frmUserAdmin:tbvUserAdmin:btnAddUser")
    private WebElement addUser;

    @FindBy(id = "frmAddUser:firstname")
    private WebElement firstName;

    @FindBy(id = "frmAddUser:lastname")
    private WebElement lastName;

    @FindBy(id = "frmAddUser:username")
    private WebElement userName;

    @FindBy(id = "frmAddUser:email")
    private WebElement email;

    @FindBy(id = "frmAddUser:active")
    private WebElement active;

    @FindBy(id = "frmAddUser:pckListAddUserDepartment")
    private WebElement department;

    @FindBy(id = "frmAddUser:pckListAddUserRole")
    private WebElement role;

    @FindBy(id = "frmAddUser:pckListAddUserPermission")
    private WebElement permission;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserDepartment']/div[2]/div/button")
    private WebElement addDepartment;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserDepartment']/div[2]/div/button[2]")
    private WebElement addAllDepartment;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserRole']/div[2]/div/button")
    private WebElement addRole;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserRole']/div[2]/div/button[2]")
    private WebElement addAllRole;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserPermission']/div[2]/div/button")
    private WebElement addPermission;

    @FindBy(xpath = "//div[@id='frmAddUser:pckListAddUserPermission']/div[2]/div/button[2]")
    private WebElement addAllPermission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button[3]")
    private WebElement removeDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserDepartment']/div[2]/div/button[4]")
    private WebElement removeAllDepartment;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button[3]")
    private WebElement removeRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserRole']/div[2]/div/button[4]")
    private WebElement removeAllRole;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button[3]")
    private WebElement removePermission;

    @FindBy(xpath = "//div[@id='frmEditUser:pckListEditUserPermission']/div[2]/div/button[4]")
    private WebElement removeAllPermission;

    @FindBy(id = "btnAddUserSubmit")
    private WebElement submit;

    public CreateUserViewPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getClearUser() {
        return clearUser;
    }

    public WebElement getAddUser() {
        return addUser;
    }

    public WebElement getFirstName() {
        return firstName;
    }

    public WebElement getLastName() {
        return lastName;
    }

    public WebElement getUserName() {
        return userName;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getActive() {
        return active;
    }

    public WebElement getDepartment() {
        return department;
    }

    public WebElement getRole() {
        return role;
    }

    public WebElement getPermission() {
        return permission;
    }

    public WebElement getAddDepartment() {
        return addDepartment;
    }

    public WebElement getAddAllDepartment() {
        return addAllDepartment;
    }

    public WebElement getAddRole() {
        return addRole;
    }

    public WebElement getAddAllRole() {
        return addAllRole;
    }

    public WebElement getAddPermission() {
        return addPermission;
    }

    public WebElement getAddAllPermission() {
        return addAllPermission;
    }

    public WebElement getRemoveDepartment() {
        return removeDepartment;
    }

    public WebElement getRemoveAllDepartment() {
        return removeAllDepartment;
    }

    public WebElement getRemoveRole() {
        return removeRole;
    }

    public WebElement getRemoveAllRole() {
        return removeAllRole;
    }

    public WebElement getRemovePermission() {
        return removePermission;
    }

    public WebElement getRemoveAllPermission() {
        return removeAllPermission;
    }

    public WebElement getSubmit() {
        return submit;
    }
}
