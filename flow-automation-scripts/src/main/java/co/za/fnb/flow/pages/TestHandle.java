package co.za.fnb.flow.pages;

public class TestHandle {
    private boolean success;

    public TestHandle() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
