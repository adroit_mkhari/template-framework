package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.*;
import generated.ObjectFactory;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class ReinstatePolicyTester {
    private Logger log  = LogManager.getLogger(ReinstatePolicyTester.class);
    private ReinstatePolicyRequestInput reinstatePolicyRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private ReinstatePolicyResponseOutput reinstatePolicyResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public ReinstatePolicyTester(ReinstatePolicyRequestInput reinstatePolicyRequestInput) {
        this.reinstatePolicyRequestInput = reinstatePolicyRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(ReinstatePolicyRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(reinstatePolicyRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(ReinstatePolicyResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public ReinstatePolicyRequestInput getReinstatePolicyRequestInput() {
        return reinstatePolicyRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public ReinstatePolicyResponseOutput getReinstatePolicyResponseOutput() {
        return reinstatePolicyResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public ReinstatePolicyResponseOutput sendReinstatePolicyRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvREINSTATEPOLICYInput reinstatePolicyInput = serviceObjectFactory.createGensrvREINSTATEPOLICYInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        reinstatePolicyInput.setPXMLIN(pxmlin);
        GensrvREINSTATEPOLICYResult gensrvREINSTATEPOLICYResult = gsd00030PRServicesPort.gensrvReinstatepolicy(reinstatePolicyInput);
        PXMLOUT reinstatePolicyResponseOutput = gensrvREINSTATEPOLICYResult.getPXMLOUT();
        String pXmlOutString = reinstatePolicyResponseOutput.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, ReinstatePolicyResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             this.reinstatePolicyResponseOutput = (ReinstatePolicyResponseOutput) unMarshal;
             return this.reinstatePolicyResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting ReinstatePolicyResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
