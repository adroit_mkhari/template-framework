package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.page_factory.MemberInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.interactions.Actions;

public class MemberInformation extends PolicyDetails {
    private Logger log  = LogManager.getLogger(QuoteInformation.class);
    MemberInformationPageObjects memberInformationPageObjects = new MemberInformationPageObjects(driver);
    Actions actions = new Actions(driver);

    public MemberInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public int getMemberInformationTableItemsSize() throws Exception {
        try {
            return driver.findElements(memberInformationPageObjects.getMemberInformationTableItems()).size();
        } catch (Exception e) {
            log.error("Error while getting Member Information Table Items Size");
            throw new Exception("Error while getting Member Information Table Items Size");
        }
    }

    public void selectMember(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsHash(), index);
            WebElement memberHash = driver.findElement(By.id(locator));
            click(memberHash);
        } catch (Exception e) {
            log.error("Error while selecting member at index: " + index);
            throw new Exception("Error while selecting member at index: " + index);
        }
    }

    public int getNumberOfExistingMembers() throws Exception {
        try {
            return driver.findElements(memberInformationPageObjects.getNumberOfExistingMembers()).size();
        } catch (Exception e) {
            log.error("Error while getting Member Of Existing Members.");
            throw new Exception("Error while getting Member Information Table Items Size");
        }
    }

    public String getMemberRelationship(int index) throws Exception {
        try {
            String memberRelationshipLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsRelationship(), index);
            WebElement memberRelationshipField = driver.findElement(By.id(memberRelationshipLocator));
            return getText(memberRelationshipField);
        } catch (Exception e) {
            log.error("Error while getting member relationship at: " + index);
            throw new Exception("Error while getting member relationship at: " + index);
        }
    }

    public String getPolicyDetailsPremium(int index) throws Exception {
        try {
            String policyDetailsPremiumLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsPremium(), index);
            WebElement policyDetailsPremiumField = driver.findElement(By.id(policyDetailsPremiumLocator));
            return getText(policyDetailsPremiumField);
        } catch (Exception e) {
            log.error("Error while getting Policy Details Premium at: " + index);
            throw new Exception("Error while getting Policy Details Premium at: " + index);
        }
    }

    public void loadCoverAmount(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountCE(), index);
            WebElement coverAmountFiled = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountRefresh(), index);
            WebElement coverAmountRefresh = driver.findElement(By.id(locator));

            actions.moveToElement(coverAmountFiled).doubleClick().build().perform();
            click(coverAmountRefresh);
            Thread.sleep(2000);
        } catch (Exception e) {
            log.error("Error while loading Cover Amount.");
            throw new Exception("Error while loading Cover Amount.");
        }
    }

    public void doubleClickCoverAmount(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountCE(), index);
            WebElement coverAmountFiled = driver.findElement(By.id(locator));

            actions.moveToElement(coverAmountFiled).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while clearing Cover Amount.");
            throw new Exception("Error while clearing Cover Amount.");
        }
    }

    public void updateCoverAmount(int index, String coverAmount) throws Exception {
        try {
            doubleClickCoverAmount(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountCE(), index);
            WebElement coverAmountFiled = driver.findElement(By.id(locator));
            // type(coverAmountFiled, coverAmount);
            actions.moveToElement(coverAmountFiled).sendKeys(coverAmount).build().perform();
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while updating Cover Amount.");
            throw new Exception("Error while updating Cover Amount.");
        }
    }

    public String getMemberCoverAmount(int index) throws Exception {
        try {
            String memberCoverLocator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountCE(), index);
            WebElement memberCoverField = driver.findElement(By.id(memberCoverLocator));
            return getText(memberCoverField);
        } catch (Exception e) {
            log.error("Error while getting member cover amount at: " + index);
            throw new Exception("Error while getting member cover amount at: " + index);
        }
    }

    public void moveToMemberCoverAmountAndDoubleClick(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getMemberCoverAmountCE(), index);
            WebElement memberRelationship = driver.findElement(By.id(locator));
            actions.moveToElement(memberRelationship).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while moving / clicking on member cover amount at: " + index);
            throw new Exception("Error while moving / clicking on member cover amount at: " + index);
        }
    }

    public void selectMemberInfoCoverAmount(String coverAmount, int index) throws Exception {
        WebElement coverAmountValuesSelectLister;
        WebElement coverAmountValuesSelectListItems;

        try {
            log.debug("Failed to input the correct cover amount. Now trying to select.");
            selectMember(index);
            moveToMemberCoverAmountAndDoubleClick(index);
            Thread.sleep(2000);
            coverAmountValuesSelectLister = driver.findElement(By.xpath("//*//tbody[contains (@id, 'createSearchItemView:mainTabView:memberInfoTable_data')]//tr//td//div[contains (@id, 'createSearchItemView:mainTabView:memberInfoTable:" + index + ":memberCoverAmountCE')]//div//div//div[contains (@class, 'ui-selectonemenu-trigger')]"));
            coverAmountValuesSelectListItems = driver.findElement(By.xpath("//*//div[contains (@id, 'createSearchItemView:mainTabView:memberInfoTable:" + index + ":memberCoverAmountID_panel')]//div[contains (@class, 'ui-selectonemenu-items-wrapper')]//ul[contains (@id, 'createSearchItemView:mainTabView:memberInfoTable:" + index + ":memberCoverAmountID_items')]"));
            selectMatchingDataLabel(coverAmountValuesSelectLister, coverAmountValuesSelectListItems, coverAmount);
        } catch (Exception e) {
            log.error("Failed to select the correct cover amount value \"" + coverAmount + "\".");
            throw new Exception("Failed to select the correct cover amount value \"" + coverAmount + "\".");
        }
    }

    public void moveToMemberRelationshipAndDoubleClick(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsRelationship(), index);
            WebElement memberRelationship = driver.findElement(By.id(locator));
            actions.moveToElement(memberRelationship).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while moving / clicking on member relationship at: " + index);
            throw new Exception("Error while moving / clicking on member relationship at: " + index);
        }
    }

    public void selectMemberRelationship(int index, String relationship) throws Exception {
        try {
            String dropDownLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsRelationshipSelect(), index);
            String dropDownItemsLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsRelationshipSelectItems(), index);
            WebElement memberRelationshipSelect = driver.findElement(By.id(dropDownLocator));
            WebElement memberRelationshipSelectItems = driver.findElement(By.id(dropDownItemsLocator));
            selectOnDashboard(memberRelationshipSelect, memberRelationshipSelectItems, relationship);
        } catch (Exception e) {
            log.error("Error while selecting member relationship at: " + index);
            throw new Exception("Error while selecting member relationship at: " + index);
        }
    }

    public void clearCompanyOrFullName(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsFullNameCE(), index);
            WebElement companyOrFullNameInputFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsFullName(), index);
            WebElement companyOrFullNameInputField = driver.findElement(By.id(locator));

            actions.moveToElement(companyOrFullNameInputFieldCE).doubleClick().build().perform();
            clear(companyOrFullNameInputField);
        } catch (Exception e) {
            log.error("Error while clearing Company or Full Name.");
            throw new Exception("Error while clearing Company or Full Name.");
        }
    }

    public void updateCompanyOrFullName(int index, String name) throws Exception {
        try {
            clearCompanyOrFullName(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsFullName(), index);
            WebElement companyOrFullNameInputField = driver.findElement(By.id(locator));
            type(companyOrFullNameInputField, name);
        } catch (Exception e) {
            log.error("Error while updating Company or Full Name.");
            throw new Exception("Error while updating Company or Full Name.");
        }
    }

    public void clearMiddleName(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsMiddleNameCE(), index);
            WebElement middleFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsMiddleName(), index);
            WebElement middleField = driver.findElement(By.id(locator));

            actions.moveToElement(middleFieldCE).doubleClick().build().perform();
            clear(middleField);
        } catch (Exception e) {
            log.error("Error while clearing Middle Name.");
            throw new Exception("Error while clearing Middle Name.");
        }
    }

    public void updateMiddleName(int index, String middleName) throws Exception {
        try {
            clearMiddleName(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsMiddleName(), index);
            WebElement companyMiddleField = driver.findElement(By.id(locator));
            type(companyMiddleField, middleName);
        } catch (Exception e) {
            log.error("Error while updating Middle Name.");
            throw new Exception("Error while updating Middle Name.");
        }
    }

    public void clearIdNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsIDNumberCE(), index);
            WebElement idNumberFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsIDNumberCE(), index);
            WebElement idNumberField = driver.findElement(By.id(locator));

            actions.moveToElement(idNumberFieldCE).doubleClick().build().perform();
            clear(idNumberField);
        } catch (Exception e) {
            log.error("Error while clearing Id Number.");
            throw new Exception("Error while clearing Id Number.");
        }
    }

    public void updateIdNumber(int index, String idNumber) throws Exception {
        try {
            clearIdNumber(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsIDNumberCE(), index);
            WebElement idNumberField = driver.findElement(By.id(locator));
            type(idNumberField, idNumber);
        } catch (Exception e) {
            log.error("Error while updating Id Number.");
            throw new Exception("Error while updating Id Number.");
        }
    }

    public void clearDateOfBirth(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsDOBCE(), index);
            WebElement dateOfBirthFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsDOB(), index);
            WebElement dateOfBirthField = driver.findElement(By.id(locator));

            actions.moveToElement(dateOfBirthFieldCE).doubleClick().build().perform();
            clear(dateOfBirthField);
        } catch (Exception e) {
            log.error("Error while clearing Date Of Birth.");
            throw new Exception("Error while clearing Date Of Birth.");
        }
    }

    public void updateDateOfBirth(int index, String dateOfBirth) throws Exception {
        try {
            clearDateOfBirth(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsDOB(), index);
            WebElement dateOfBirthField = driver.findElement(By.id(locator));
            type(dateOfBirthField, dateOfBirth);
        } catch (Exception e) {
            log.error("Error while updating Date Of Birth.");
            throw new Exception("Error while updating Date Of Birth.");
        }
    }

    public void moveToMemberGenderAndDoubleClick(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsGenderCE(), index);
            WebElement memberGender = driver.findElement(By.id(locator));
            actions.moveToElement(memberGender).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while moving / clicking on member gender at: " + index);
            throw new Exception("Error while moving / clicking on member gender at: " + index);
        }
    }

    public void selectMemberGender(int index, String gender) throws Exception {
        try {
            String dropDownLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsGenderSelect(), index);
            String dropDownItemsLocator = getLocatorForIndex(memberInformationPageObjects.getPolicyDetailsGenderSelectItems(), index);
            WebElement memberGenderSelect = driver.findElement(By.id(dropDownLocator));
            WebElement memberGenderSelectItems = driver.findElement(By.id(dropDownItemsLocator));
            selectOnDashboard(memberGenderSelect, memberGenderSelectItems, gender);
        } catch (Exception e) {
            log.error("Error while selecting member gender at: " + index);
            throw new Exception("Error while selecting member gender at: " + index);
        }
    }

    public void clearEmailAddress(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getEmailAddressCE(), index);
            WebElement emailAddressFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getEmailAddress(), index);
            WebElement emailAddressField = driver.findElement(By.id(locator));

            actions.moveToElement(emailAddressFieldCE).doubleClick().build().perform();
            clear(emailAddressField);
        } catch (Exception e) {
            log.error("Error while clearing email address.");
            throw new Exception("Error while clearing email address.");
        }
    }

    public void updateEmailAddress(int index, String emailAddress) throws Exception {
        try {
            clearEmailAddress(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getEmailAddress(), index);
            WebElement emailAddressField = driver.findElement(By.id(locator));
            type(emailAddressField, emailAddress);
        } catch (Exception e) {
            log.error("Error while updating email address.");
            throw new Exception("Error while updating email address.");
        }
    }

    public void clearCellPhoneNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(memberInformationPageObjects.getCellPhoneNumberCE(), index);
            WebElement cellPhoneNumberFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(memberInformationPageObjects.getCellPhoneNumber(), index);
            WebElement cellPhoneNumberField = driver.findElement(By.id(locator));

            actions.moveToElement(cellPhoneNumberFieldCE).doubleClick().build().perform();
            clear(cellPhoneNumberField);
        } catch (Exception e) {
            log.error("Error while clearing Cell Phone Number.");
            throw new Exception("Error while clearing Cell Phone Number.");
        }
    }

    public void updateCellPhoneNumber(int index, String cellPhoneNumber) throws Exception {
        try {
            clearCellPhoneNumber(index);
            String locator = getLocatorForIndex(memberInformationPageObjects.getCellPhoneNumber(), index);
            WebElement cellPhoneNumberField = driver.findElement(By.id(locator));
            type(cellPhoneNumberField, cellPhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Cell Phone Number.");
            throw new Exception("Error while updating Cell Phone Number.");
        }
    }

}
