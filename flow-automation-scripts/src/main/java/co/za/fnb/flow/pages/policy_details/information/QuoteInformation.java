package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.page_factory.QuoteInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class QuoteInformation extends PolicyDetails {
    private Logger log  = LogManager.getLogger(QuoteInformation.class);
    QuoteInformationPageObjects quoteInformationPageObjects = new QuoteInformationPageObjects(driver);
    Actions actions = new Actions(driver);

    public QuoteInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void selectMember(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsHash(), index);
            WebElement memberHash = driver.findElement(By.id(locator));
            click(memberHash);
        } catch (Exception e) {
            log.error("Error while selecting member at index: " + index);
            throw new Exception("Error while selecting member at index: " + index);
        }
    }

    public String getPolicyDetailsRelationship(int index) throws Exception {
        try {
            String memberRelationshipLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsRelationshipCE(), index);
            WebElement memberRelationshipField = driver.findElement(By.id(memberRelationshipLocator));
            return getText(memberRelationshipField);
        } catch (Exception e) {
            log.error("Error while getting member relationship at: " + index);
            throw new Exception("Error while getting on member relationship at: " + index);
        }
    }

    public void moveToMemberRelationshipAndDoubleClick(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsRelationshipCE(), index);
            WebElement memberRelationship = driver.findElement(By.id(locator));
            // actions.moveToElement(memberRelationship).doubleClick().build().perform();
            actions.moveToElement(memberRelationship).click().build().perform();
        } catch (Exception e) {
            log.error("Error while moving / clicking on member relationship at: " + index);
            throw new Exception("Error while moving / clicking on member relationship at: " + index);
        }
    }

    public void selectMemberRelationship(int index, String relationship) throws Exception {
        try {
            String dropDownLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsRelationshipSelect(), index);
            String dropDownItemsLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsRelationshipSelectItems(), index);
            WebElement memberRelationshipSelect = driver.findElement(By.id(dropDownLocator));
            WebElement memberRelationshipSelectItems = driver.findElement(By.id(dropDownItemsLocator));
            // selectOnDashboard(memberRelationshipSelect, memberRelationshipSelectItems, relationship);
            selectRelationshipOnFlow(memberRelationshipSelect, memberRelationshipSelectItems, relationship);
        } catch (Exception e) {
            log.error("Error while selecting member relationship at: " + index);
            throw new Exception("Error while selecting member relationship at: " + index);
        }
    }

    public void clearCompanyOrFullName(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsFullNameCE(), index);
            WebElement companyOrFullNameInputFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsFullName(), index);
            WebElement companyOrFullNameInputField = driver.findElement(By.id(locator));

            actions.moveToElement(companyOrFullNameInputFieldCE).doubleClick().build().perform();
            clear(companyOrFullNameInputField);
        } catch (Exception e) {
            log.error("Error while clearing Company or Full Name.");
            throw new Exception("Error while clearing Company or Full Name.");
        }
    }

    public void updateCompanyOrFullName(int index, String name) throws Exception {
        try {
            clearCompanyOrFullName(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsFullName(), index);
            WebElement companyOrFullNameInputField = driver.findElement(By.id(locator));
            type(companyOrFullNameInputField, name);
        } catch (Exception e) {
            log.error("Error while updating Company or Full Name.");
            throw new Exception("Error while updating Company or Full Name.");
        }
    }

    public void clearMiddleName(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsMiddleNameCE(), index);
            WebElement middleFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsMiddleName(), index);
            WebElement middleField = driver.findElement(By.id(locator));

            actions.moveToElement(middleFieldCE).doubleClick().build().perform();
            clear(middleField);
        } catch (Exception e) {
            log.error("Error while clearing Middle Name.");
            throw new Exception("Error while clearing Middle Name.");
        }
    }

    public void updateMiddleName(int index, String middleName) throws Exception {
        try {
            clearMiddleName(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsMiddleName(), index);
            WebElement companyMiddleField = driver.findElement(By.id(locator));
            type(companyMiddleField, middleName);
        } catch (Exception e) {
            log.error("Error while updating Middle Name.");
            throw new Exception("Error while updating Middle Name.");
        }
    }

    public void clearIdNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsIDNumberCE(), index);
            locator = locator.replaceFirst("j_idt\\d*", "j_idt");
            locator = "//*[contains (@id, '" + locator + "')]";
            WebElement idNumberFieldCE = driver.findElement(By.xpath(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsIDNumber(), index);
            WebElement idNumberField = driver.findElement(By.id(locator));

            actions.moveToElement(idNumberFieldCE).doubleClick().build().perform();
            clear(idNumberField);
        } catch (Exception e) {
            log.error("Error while clearing Id Number.");
            throw new Exception("Error while clearing Id Number.");
        }
    }

    public void updateIdNumber(int index, String idNumber) throws Exception {
        try {
            clearIdNumber(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsIDNumber(), index);
            WebElement idNumberField = driver.findElement(By.id(locator));
            type(idNumberField, idNumber);
        } catch (Exception e) {
            log.error("Error while updating Id Number.");
            throw new Exception("Error while updating Id Number.");
        }
    }

    public void clearDateOfBirth(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getDateOfBirthCE(), index);
            WebElement dateOfBirthFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getDateOfBirth(), index);
            WebElement dateOfBirthField = driver.findElement(By.id(locator));

            actions.moveToElement(dateOfBirthFieldCE).doubleClick().build().perform();
            clear(dateOfBirthField);
        } catch (Exception e) {
            log.error("Error while clearing Date Of Birth.");
            throw new Exception("Error while clearing Date Of Birth.");
        }
    }

    public void updateDateOfBirth(int index, String dateOfBirth) throws Exception {
        try {
            clearDateOfBirth(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getDateOfBirth(), index);
            WebElement dateOfBirthField = driver.findElement(By.id(locator));
            type(dateOfBirthField, dateOfBirth);
        } catch (Exception e) {
            log.error("Error while updating Date Of Birth.");
            throw new Exception("Error while updating Date Of Birth.");
        }
    }

    public void moveToMemberGenderAndDoubleClick(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsGenderCE(), index);
            WebElement memberGender = driver.findElement(By.id(locator));
            actions.moveToElement(memberGender).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while moving / clicking on member gender at: " + index);
            throw new Exception("Error while moving / clicking on member gender at: " + index);
        }
    }

    public void selectMemberGender(int index, String gender) throws Exception {
        try {
            String dropDownLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsGenderSelect(), index);
            String dropDownItemsLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsGenderSelectItems(), index);
            WebElement memberGenderSelect = driver.findElement(By.id(dropDownLocator));
            WebElement memberGenderSelectItems = driver.findElement(By.id(dropDownItemsLocator));
            Thread.sleep(3000);
            click(memberGenderSelect);
            Thread.sleep(3000);
            selectOnDashboard(memberGenderSelect, memberGenderSelectItems, gender);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while selecting member gender at: " + index);
            throw new Exception("Error while selecting member gender at: " + index);
        }
    }

    public void clearEmailAddress(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getEmailAddressCE(), index);
            WebElement emailAddressFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getEmailAddress(), index);
            WebElement emailAddressField = driver.findElement(By.id(locator));

            actions.moveToElement(emailAddressFieldCE).doubleClick().build().perform();
            clear(emailAddressField);
        } catch (Exception e) {
            log.error("Error while clearing email address.");
            throw new Exception("Error while clearing email address.");
        }
    }

    public void updateEmailAddress(int index, String emailAddress) throws Exception {
        try {
            clearEmailAddress(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getEmailAddress(), index);
            WebElement emailAddressField = driver.findElement(By.id(locator));
            type(emailAddressField, emailAddress);
        } catch (Exception e) {
            log.error("Error while updating email address.");
            throw new Exception("Error while updating email address.");
        }
    }

    public void clearCellPhoneNumber(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getCellPhoneNumberCE(), index);
            WebElement cellPhoneNumberFieldCE = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getCellPhoneNumber(), index);
            WebElement cellPhoneNumberField = driver.findElement(By.id(locator));

            actions.moveToElement(cellPhoneNumberFieldCE).doubleClick().build().perform();
            clear(cellPhoneNumberField);
        } catch (Exception e) {
            log.error("Error while clearing Cell Phone Number.");
            throw new Exception("Error while clearing Cell Phone Number.");
        }
    }

    public void updateCellPhoneNumber(int index, String cellPhoneNumber) throws Exception {
        try {
            clearCellPhoneNumber(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getCellPhoneNumber(), index);
            WebElement cellPhoneNumberField = driver.findElement(By.id(locator));
            type(cellPhoneNumberField, cellPhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Cell Phone Number.");
            throw new Exception("Error while updating Cell Phone Number.");
        }
    }

    public void loadCoverAmount(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsCoverAmount(), index);
            WebElement coverAmountFiled = driver.findElement(By.id(locator));

            locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsCoverAmountRefresh(), index);
            WebElement coverAmountRefresh = driver.findElement(By.id(locator));

            actions.moveToElement(coverAmountFiled).doubleClick().build().perform();
            click(coverAmountRefresh);
            Thread.sleep(2000);
        } catch (Exception e) {
            log.error("Error while loading Cover Amount.");
            throw new Exception("Error while loading Cover Amount.");
        }
    }

    public void doubleClickCoverAmount(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsCoverAmount(), index);
            WebElement coverAmountFiled = driver.findElement(By.id(locator));

            actions.moveToElement(coverAmountFiled).doubleClick().build().perform();
        } catch (Exception e) {
            log.error("Error while clearing Cover Amount.");
            throw new Exception("Error while clearing Cover Amount.");
        }
    }

    public void updateCoverAmount(int index, String coverAmount) throws Exception {
        try {
            if (coverAmount.contains(".")) {
                String[] splitAmountValues = coverAmount.split("\\.");
                coverAmount = splitAmountValues[0] + ".00";
            }
            doubleClickCoverAmount(index);
            String locator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsCoverAmount(), index);
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id(locator)));

            WebElement coverAmountFiled = driver.findElement(By.id(locator));
            // type(coverAmountFiled, coverAmount);
            actions.moveToElement(coverAmountFiled).sendKeys(coverAmount).build().perform();
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while updating Cover Amount.");
            throw new Exception("Error while updating Cover Amount.");
        }
    }

    public String getPolicyDetailsCoverAmount(int index) throws Exception {
        try {
            String coverAmountLocator = getLocatorForIndex(quoteInformationPageObjects.getPolicyDetailsCoverAmount(), index);
            WebElement coverAmountField = driver.findElement(By.id(coverAmountLocator));
            return getText(coverAmountField);
        } catch (Exception e) {
            log.error("Error while getting cover amount at: " + index);
            throw new Exception("Error while getting cover amount at: " + index);
        }
    }

    public String getQuotePremium(int index) throws Exception {
        try {
            String locator = getLocatorForIndex(quoteInformationPageObjects.getQuotePremium(), index);
            WebElement quotePremiumField = driver.findElement(By.id(locator));
            return getText(quotePremiumField);
        } catch (Exception e) {
            log.error("Error while getting quote premium.");
            throw new Exception("Error while getting quote premium.");
        }
    }

    public int getMemberInformationTableItemsSize() throws Exception {
        try {
            return driver.findElements(quoteInformationPageObjects.getQuoteInformationTableItems()).size();
        } catch (Exception e) {
            log.error("Error while getting Quote Information Table Items Size");
            throw new Exception("Error while getting Quote Information Table Items Size");
        }
    }

}
