package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.services.GetPrincipalMemberDetailsTester;
import cucumber.api.DataTable;
import generated.*;
import generated.Error;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class GetPrincipalMemberDetailsServiceLevelRunner {
    private Logger log  = LogManager.getLogger(GetPrincipalMemberDetailsServiceLevelRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    DataTable dataTable;
    ScenarioTester scenarioTester;
    private GetPrincipalMemberRequestInput getPrincipalMemberRequestInput;
    private ResponseErrors responseErrors;

    public GetPrincipalMemberDetailsServiceLevelRunner(DataTable dataTable, ScenarioTester scenarioTester) {
        this.dataTable = dataTable;
        this.scenarioTester = scenarioTester;
    }

    public DataTable getDataTable() {
        return dataTable;
    }

    public void setDataTable(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public ScenarioTester getScenarioTester() {
        return scenarioTester;
    }

    public void setScenarioTester(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private PrincipalMemberResponseOutput getPrincipalMemberDetails(String policyNumber, String username) throws ServiceException {
        getPrincipalMemberRequestInput = new GetPrincipalMemberRequestInput();
        getPrincipalMemberRequestInput.setUsername(username);
        getPrincipalMemberRequestInput.setPolicy(policyNumber);
        GetPrincipalMemberDetailsTester getPrincipalMemberDetailsTester = new GetPrincipalMemberDetailsTester(getPrincipalMemberRequestInput);

        StringBuilder getPrincipalMemberErrorStringBuilder = new StringBuilder();
        try {
            PrincipalMemberResponseOutput principalMemberResponseOutput = getPrincipalMemberDetailsTester.sendGetPrincipalDetailsRequest();
            return principalMemberResponseOutput;
        } catch (Exception e) {
            responseErrors = getPrincipalMemberDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPrincipalMemberErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPrincipalMemberErrorStringBuilder.toString());
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Policy Number", "Full Name", "Middle Name", "Id Number", "Date Of Birth", "Policy Holder UCN", "Cis UCN"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        scenarioTester.setHeaders(headers);
        List<String> dataEntry;
        String run, policyNumber = "No Policy Number", userName;

        for (Row row : dataTable.getGherkinRows()) {
            if (row.getLine() != 1) {
                dataEntry = row.getCells();
                scenarioTester.setDataEntry(dataEntry);

                run = getCellValue(headers, dataEntry,"Run");
                if (run.equalsIgnoreCase("YES")) {
                    try {
                        policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                        userName = getCellValue(headers, dataEntry,"Username");
                        try {
                            PrincipalMemberResponseOutput principalMemberDetails = getPrincipalMemberDetails(policyNumber, userName);
                            PrincipalMemberDetailPolicyHolder policyholder = principalMemberDetails.getPolicyholder();
                            String ucn = policyholder.getUcn();
                            String fullName = policyholder.getFullname();
                            String middleName = policyholder.getMiddlename();
                            String idNumber = policyholder.getIdnumber();
                            String birthDate = policyholder.getBirthdate();
                            PrincipalMemberDetailCis cis = policyholder.getCis();
                            String cisUcn = cis.getUcn();

                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), policyNumber, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Full Name"), fullName, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Middle Name"), middleName, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Id Number"), idNumber, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Date Of Birth"), birthDate, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Holder UCN"), ucn, TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Cis UCN"), cisUcn, TestResultReportFlag.DEFAULT);
                        } catch (Exception e) {
                            reportFailure(reportableFields, scenarioOperator, policyNumber, e.getMessage());
                        }

                        log.info("End Of Test Case Number: " + policyNumber + ", Function: Get Principal Member Details");
                    } catch (Exception e) {
                        log.error(e.getMessage());
                        e.printStackTrace();
                        reportFailure(reportableFields, scenarioOperator, policyNumber, e.getMessage());
                    } finally {
                        scenarioOperator.increamentReportRowIndex();
                        // TODO: Things that need to be done post the logic above.
                    }
                }
            }
        }

        log.info("Saving Report.");
        scenarioOperator.saveReport();
        log.info("===========================================================================");
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String policyNumber, String error) {
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), policyNumber, TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Full Name"), error, TestResultReportFlag.FAIL);
    }

    private void reportTechnicalFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.WARNING);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
