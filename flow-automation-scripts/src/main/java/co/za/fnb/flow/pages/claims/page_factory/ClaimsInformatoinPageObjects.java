package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ClaimsInformatoinPageObjects {

    // region Claim Information Table
    @FindBy(id = "createSearchItemView:mainTabView:claimsInformationTable_data")
    WebElement claimsInformationTableData;

    String claimsInformationTableDataXpathLocator = "//*[@id=\"createSearchItemView:mainTabView:claimsInformationTable_data\"]/tr[1]/td[1]";
    // endregion

    // region Claims Details
    @FindBy(id = "updateClaimForm:policyHolderId")
    WebElement policyHolderId; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:claimsHandler")
    WebElement claimsHandler; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:claimsRegDate_input")
    WebElement claimsRegDateInput; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:dateSettled_input")
    WebElement dateSettledInput; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:selectStatus")
    WebElement selectStatus;

    @FindBy(id = "updateClaimForm:selectStatus_label")
    WebElement selectStatusLabel;

    @FindBy(id = "updateClaimForm:selectStatus_items")
    WebElement selectStatusItems;

    @FindBy(id = "updateClaimForm:selectClaimant")
    WebElement selectClaimant;

    @FindBy(id = "updateClaimForm:selectClaimant_label")
    WebElement selectClaimantLabel;

    @FindBy(id = "updateClaimForm:claimAmount")
    WebElement claimAmount;

    @FindBy(id = "updateClaimForm:selectApproved")
    WebElement selectApproved;

    @FindBy(id = "updateClaimForm:selectApproved_label")
    WebElement selectApprovedLabel;

    @FindBy(id = "updateClaimForm:lastUpdate_input")
    WebElement lastUpdateInput; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:claimRefNumber")
    WebElement claimRefNumber; // Has attribute 'value'
    // endregion

    // region Claim Event Details
    @FindBy(id = "updateClaimForm:selectEvent")
    WebElement selectEvent;

    @FindBy(id = "updateClaimForm:selectEvent_label")
    WebElement selectEventLabel;

    @FindBy(id = "updateClaimForm:selectEvent_items")
    WebElement selectEventItems;

    @FindBy(id = "updateClaimForm:eventDate_input")
    WebElement eventDateInput; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:eventDescription")
    WebElement eventDescription;
    // endregion

    // region Attorney Details
    @FindBy(id = "updateClaimForm:companyName")
    WebElement companyName; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:vatRegNumber")
    WebElement vatRegNumber; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:cellNumber")
    WebElement cellNumber; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:emailAddress")
    WebElement emailAddress; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:workNumber")
    WebElement workNumber; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:accountNumber")
    WebElement accountNumber; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:branch")
    WebElement branch; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:attorneyType")
    WebElement attorneyType; // Has attribute 'value'

    @FindBy(id = "updateClaimForm:update")
    WebElement update;

    @FindBy(id = "updateClaimForm:btnMakePayment")
    WebElement btnMakePayment; // Pay Attorney

    @FindBy(id = "updateClaimForm:btnMakePaymentOther")
    WebElement btnMakePaymentOther; // Pay PolicyHolder
    // endregion

    @FindBy(id = "claimAttorneyForm:province")
    WebElement province;

    @FindBy(id = "claimAttorneyForm:province_items")
    WebElement provinceItems;

    @FindBy(id = "claimAttorneyForm:province_label")
    WebElement provinceLabel;

    @FindBy(id = "claimAttorneyForm:city_label")
    WebElement selectCityLabel;

    @FindBy(id = "claimAttorneyForm:city_items")
    WebElement selectCityItems; // Uses data-label

    @FindBy(id = "claimAttorneyForm:cellNo")
    WebElement cellNo;

    @FindBy(id = "claimAttorneyForm:attName")
    WebElement attorneyName;

    @FindBy(id = "claimAttorneyForm:search")
    WebElement search;

    // region Attorney Information
    @FindBy(id = "claimAttorneySearchForm:claimsInformationTable_data")
    WebElement attorneyInformationTableData; // Pass this to selectUsingTableRowData(int Row)

    @FindBy(id = "claimAttorneySearchForm:addAttorney")
    WebElement addAttorney;
    // endregion

    // region Close
    @FindBy(xpath = "//div[contains(@id, 'createSearchItemView:mainTabView:Create_dlg')]//div//a//span[contains(@class, 'ui-icon ui-icon-closethick')]")
    WebElement closethick;

    @FindBy(xpath = "//div[contains(@id, 'createSearchItemView:mainTabView:Create_dlg')]//div//a[contains(@class, 'ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all')]")
    WebElement uidialogTitleBarCloseUiCornerAll;
    // endregion

    By attorneyInformationTable = By.xpath("//tbody[contains(@id, 'claimAttorneySearchForm:claimsInformationTable_data')]//tr[contains(@data-ri, '')]");

    public ClaimsInformatoinPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getClaimsInformationTableData() {
        return claimsInformationTableData;
    }

    public String getClaimsInformationTableDataXpathLocator() {
        return claimsInformationTableDataXpathLocator;
    }

    public WebElement getPolicyHolderId() {
        return policyHolderId;
    }

    public WebElement getClaimsHandler() {
        return claimsHandler;
    }

    public WebElement getClaimsRegDateInput() {
        return claimsRegDateInput;
    }

    public WebElement getDateSettledInput() {
        return dateSettledInput;
    }

    public WebElement getSelectStatus() {
        return selectStatus;
    }

    public WebElement getSelectStatusLabel() {
        return selectStatusLabel;
    }

    public WebElement getSelectStatusItems() {
        return selectStatusItems;
    }

    public WebElement getSelectClaimant() {
        return selectClaimant;
    }

    public WebElement getSelectClaimantLabel() {
        return selectClaimantLabel;
    }

    public WebElement getClaimAmount() {
        return claimAmount;
    }

    public WebElement getSelectApproved() {
        return selectApproved;
    }

    public WebElement getSelectApprovedLabel() {
        return selectApprovedLabel;
    }

    public WebElement getLastUpdateInput() {
        return lastUpdateInput;
    }

    public WebElement getClaimRefNumber() {
        return claimRefNumber;
    }

    public WebElement getSelectEvent() {
        return selectEvent;
    }

    public WebElement getSelectEventLabel() {
        return selectEventLabel;
    }

    public WebElement getSelectEventItems() {
        return selectEventItems;
    }

    public WebElement getEventDateInput() {
        return eventDateInput;
    }

    public WebElement getEventDescription() {
        return eventDescription;
    }

    public WebElement getCompanyName() {
        return companyName;
    }

    public WebElement getVatRegNumber() {
        return vatRegNumber;
    }

    public WebElement getCellNumber() {
        return cellNumber;
    }

    public WebElement getEmailAddress() {
        return emailAddress;
    }

    public WebElement getWorkNumber() {
        return workNumber;
    }

    public WebElement getAccountNumber() {
        return accountNumber;
    }

    public WebElement getBranch() {
        return branch;
    }

    public WebElement getAttorneyType() {
        return attorneyType;
    }

    public WebElement getUpdate() {
        return update;
    }

    public WebElement getBtnMakePayment() {
        return btnMakePayment;
    }

    public WebElement getBtnMakePaymentOther() {
        return btnMakePaymentOther;
    }

    public WebElement getProvince() {
        return province;
    }

    public WebElement getProvinceItems() {
        return provinceItems;
    }

    public WebElement getProvinceLabel() {
        return provinceLabel;
    }

    public WebElement getSelectCityLabel() {
        return selectCityLabel;
    }

    public WebElement getSelectCityItems() {
        return selectCityItems;
    }

    public WebElement getCellNo() {
        return cellNo;
    }

    public WebElement getAttorneyName() {
        return attorneyName;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getAttorneyInformationTableData() {
        return attorneyInformationTableData;
    }

    public WebElement getAddAttorney() {
        return addAttorney;
    }

    public WebElement getClosethick() {
        return closethick;
    }

    public WebElement getUidialogTitleBarCloseUiCornerAll() {
        return uidialogTitleBarCloseUiCornerAll;
    }

    public By getAttorneyInformationTable() {
        return attorneyInformationTable;
    }

}
