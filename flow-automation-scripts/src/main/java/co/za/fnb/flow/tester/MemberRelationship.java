package co.za.fnb.flow.tester;

public enum MemberRelationship {
    POLICY_HOLDER("POLICY HOLDER"),
    SPOUSE("SPOUSE"),
    CHILD("CHILD"),
    PARENTS("PARENTS"),
    EXTENDED_FAMILY("EXTENDED FAMILY");

    private final String relationship;

    /**
     * @param relationship
     */
    MemberRelationship(final String relationship) {
        this.relationship = relationship;
    }

    @Override
    public String toString() {
        return relationship;
    }
}
