package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.GetAuditTrailTester;
import co.za.fnb.flow.tester.services.GetPrincipalMemberDetailsTester;
import co.za.fnb.flow.tester.services.UpdatePrincipalMemberDetailsTester;
import generated.*;
import generated.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class UpdatePrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(UpdatePrincipalMemberDetails.class);
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private GetPrincipalMemberRequestInput getPrincipalMemberRequestInput;
    private UpdatePrincipalMemberDetailRequestInput updatePrincipalMemberDetailRequestInput;
    private PrincipalMemberResponseOutput principalMemberResponseOutputBefore;
    private PrincipalMemberResponseOutput principalMemberResponseOutputAfter;
    private UpdatePrincipalMemberDetailResponseOutput updatePrincipalMemberDetailResponseOutput;
    private GetAuditTrailRequestInput getAuditTrailRequestInput;
    private GetAuditTrailResponseOutput getAuditTrailResponseOutput;
    private final String CLEAR = "CLEAR";
    private ScenarioOperator scenarioOperator;
    private String username;
    private String policyNumber;

    public UpdatePrincipalMemberDetails(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void sendRequest() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        policyNumber = scenarioOperator.getPolicyNumber();
        username = scenarioTester.getCellValue("Username");
        String function = scenarioOperator.getFunction();

        principalMemberResponseOutputBefore = getPrincipalMemberDetails(policyNumber);

        PrincipalMemberDetailPolicyHolder policyHolder = null;
        if (principalMemberResponseOutputBefore != null) {
            policyHolder = principalMemberResponseOutputBefore.getPolicyholder();
        }

        if (updatePrincipalMemberDetailRequestInput == null) {
            updatePrincipalMemberDetailRequestInput = new UpdatePrincipalMemberDetailRequestInput();
            updatePrincipalMemberDetailRequestInput.setUsername(username);
            updatePrincipalMemberDetailRequestInput.setPolicy(policyNumber);

            PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder = new PrincipalMemberDetailPolicyHolder();

            setPolicyHolderGeneralDetailValues(policyHolder, principalMemberDetailPolicyHolder);

            setPolicyHolderContactDetailValues(policyHolder, principalMemberDetailPolicyHolder);

            setPolicyHolderAddressValues(policyHolder, principalMemberDetailPolicyHolder);

            setPolicyHolderCisValues(policyHolder, principalMemberDetailPolicyHolder);

            setPolicyHolderExtendedDetailsValues(policyHolder, principalMemberDetailPolicyHolder);

            updatePrincipalMemberDetailRequestInput.setPolicyholder(principalMemberDetailPolicyHolder);
        }

        UpdatePrincipalMemberDetailsTester updatePrincipalMemberDetailsTester = new UpdatePrincipalMemberDetailsTester(updatePrincipalMemberDetailRequestInput);

        StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
        try {
            updatePrincipalMemberDetailResponseOutput = updatePrincipalMemberDetailsTester.sendUpdatePrincipalMemberRequest();
        } catch (Exception e) {
            responseErrors = updatePrincipalMemberDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(updatePolicyErrorStringBuilder.toString());
        }

        principalMemberResponseOutputAfter = getPrincipalMemberDetails(policyNumber);
    }

    private void setPolicyHolderExtendedDetailsValues(PrincipalMemberDetailPolicyHolder policyHolder, PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder) throws Exception {
        String staff = scenarioTester.getCellValue("Staff");
        String vatNo = scenarioTester.getCellValue("Vat No");
        String fNumber = scenarioTester.getCellValue("F Number");
        String companyRegNo = scenarioTester.getCellValue("Company Reg No");
        String policyCover = scenarioTester.getCellValue("Policy Cover");
        String availableCover = scenarioTester.getCellValue("Available Cover");
        String insurererName = scenarioTester.getCellValue("Insurerer Name");

        PrincipalMemberDetailExtendedDetails policyHolderExtendedDetails = null;
        if (policyHolder != null) {
            policyHolderExtendedDetails = policyHolder.getExtendeddetails();
        }

        PrincipalMemberDetailExtendedDetails principalMemberDetailExtendedDetails = new PrincipalMemberDetailExtendedDetails();

        if (!staff.isEmpty()) {
            principalMemberDetailExtendedDetails.setStaff(staff);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsStaff = policyHolderExtendedDetails.getStaff();
                principalMemberDetailExtendedDetails.setStaff(policyHolderExtendedDetailsStaff);
            }
        }

        if (!vatNo.isEmpty()) {
            principalMemberDetailExtendedDetails.setVatno(vatNo);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsVatno = policyHolderExtendedDetails.getVatno();
                principalMemberDetailExtendedDetails.setVatno(policyHolderExtendedDetailsVatno);
            }
        }

        if (!fNumber.isEmpty()) {
            principalMemberDetailExtendedDetails.setFnumber(fNumber);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsFNumber = policyHolderExtendedDetails.getFnumber();
                principalMemberDetailExtendedDetails.setFnumber(policyHolderExtendedDetailsFNumber);
            }
        }

        if (!companyRegNo.isEmpty()) {
            principalMemberDetailExtendedDetails.setCompanyregno(companyRegNo);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsCompanyRegNo = policyHolderExtendedDetails.getCompanyregno();
                principalMemberDetailExtendedDetails.setCompanyregno(policyHolderExtendedDetailsCompanyRegNo);
            }
        }

        if (!policyCover.isEmpty()) {
            principalMemberDetailExtendedDetails.setPolicycover(policyCover);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsPolicyCover = policyHolderExtendedDetails.getPolicycover();
                principalMemberDetailExtendedDetails.setPolicycover(policyHolderExtendedDetailsPolicyCover);
            }
        }

        if (!availableCover.isEmpty()) {
            principalMemberDetailExtendedDetails.setAvailablecover(availableCover);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsAvailableCover = policyHolderExtendedDetails.getAvailablecover();
                principalMemberDetailExtendedDetails.setAvailablecover(policyHolderExtendedDetailsAvailableCover);
            }
        }

        if (!insurererName.isEmpty()) {
            principalMemberDetailExtendedDetails.setInsurername(insurererName);
        } else {
            if (policyHolderExtendedDetails != null) {
                String policyHolderExtendedDetailsInsurerName = policyHolderExtendedDetails.getInsurername();
                principalMemberDetailExtendedDetails.setInsurername(policyHolderExtendedDetailsInsurerName);
            }
        }

        principalMemberDetailPolicyHolder.setExtendeddetails(principalMemberDetailExtendedDetails);
    }

    private void setPolicyHolderCisValues(PrincipalMemberDetailPolicyHolder policyHolder, PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder) throws Exception {
        String cisUcn = scenarioTester.getCellValue("Cis Ucn");
        String cisLineOne = scenarioTester.getCellValue("Cis Line 1");
        String cisLineTwo = scenarioTester.getCellValue("Cis Line 2");
        String cisCity = scenarioTester.getCellValue("Cis City");
        String cisState = scenarioTester.getCellValue("Cis State");
        String cisPostalCode = scenarioTester.getCellValue("Cis Postal Code");
        String cisZipCode = scenarioTester.getCellValue("Cis Zip Code");
        String cisCountry = scenarioTester.getCellValue("Cis Country");
        String cisCellPhoneNumber = scenarioTester.getCellValue("Cis Cell Phone Number");
        String cisEmailAddress = scenarioTester.getCellValue("Cis Email Address");
        String cisUpdate = scenarioTester.getCellValue("Cis Update");

        PrincipalMemberDetailCis policyHolderCis = null;
        if (policyHolder != null) {
            policyHolderCis = policyHolder.getCis();
        }

        PrincipalMemberDetailCis principalMemberDetailCis = new PrincipalMemberDetailCis();

        if (!cisUcn.isEmpty()) {
            principalMemberDetailCis.setUcn(cisUcn);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisUcn = policyHolderCis.getUcn();
                principalMemberDetailCis.setUcn(policyHolderCisUcn);
            }
        }

        if (!cisLineOne.isEmpty()) {
            principalMemberDetailCis.setLine1(cisLineOne);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisLine1 = policyHolderCis.getLine1();
                principalMemberDetailCis.setLine1(policyHolderCisLine1);
            }
        }

        if (!cisLineTwo.isEmpty()) {
            principalMemberDetailCis.setLine2(cisLineTwo);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisLine2 = policyHolderCis.getLine2();
                principalMemberDetailCis.setLine2(policyHolderCisLine2);
            }
        }

        if (!cisCity.isEmpty()) {
            principalMemberDetailCis.setCity(cisCity);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisCity = policyHolderCis.getCity();
                principalMemberDetailCis.setCity(policyHolderCisCity);
            }
        }

        if (!cisState.isEmpty()) {
            principalMemberDetailCis.setState(cisState);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisState = policyHolderCis.getState();
                principalMemberDetailCis.setState(policyHolderCisState);
            }
        }

        if (!cisPostalCode.isEmpty()) {
            principalMemberDetailCis.setPostalcode(cisPostalCode);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisPostalCode = policyHolderCis.getPostalcode();
                principalMemberDetailCis.setPostalcode(policyHolderCisPostalCode);
            }
        }

        if (!cisZipCode.isEmpty()) {
            principalMemberDetailCis.setZipcode(cisZipCode);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisZipCode = policyHolderCis.getZipcode();
                principalMemberDetailCis.setZipcode(policyHolderCisZipCode);
            }
        }

        if (!cisCountry.isEmpty()) {
            principalMemberDetailCis.setCountry(cisCountry);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisCountry = policyHolderCis.getCountry();
                principalMemberDetailCis.setCountry(policyHolderCisCountry);
            }
        }

        if (!cisCellPhoneNumber.isEmpty()) {
            principalMemberDetailCis.setCellphonenumber(cisCellPhoneNumber);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisCellPhoneNumber = policyHolderCis.getCellphonenumber();
                principalMemberDetailCis.setCellphonenumber(policyHolderCisCellPhoneNumber);
            }
        }

        if (!cisEmailAddress.isEmpty()) {
            principalMemberDetailCis.setEmailaddress(cisEmailAddress);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisEmailAddress = policyHolderCis.getEmailaddress();
                principalMemberDetailCis.setEmailaddress(policyHolderCisEmailAddress);
            }
        }

        if (!cisUpdate.isEmpty()) {
            principalMemberDetailCis.setUpdate(cisUpdate);
        } else {
            if (policyHolderCis != null) {
                String policyHolderCisUpdate = policyHolderCis.getUpdate();
                principalMemberDetailCis.setUpdate(policyHolderCisUpdate);
            }
        }

        principalMemberDetailPolicyHolder.setCis(principalMemberDetailCis);
    }

    private void setPolicyHolderAddressValues(PrincipalMemberDetailPolicyHolder policyHolder, PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder) throws Exception {
        String addressId = scenarioTester.getCellValue("Address Id");
        String addressType = scenarioTester.getCellValue("Address Type");
        String addressLineOne = scenarioTester.getCellValue("Address Line 1");
        String addressLineTwo = scenarioTester.getCellValue("Address Line 2");
        String addressSuburb = scenarioTester.getCellValue("Address Suburb");
        String addressCity = scenarioTester.getCellValue("Address City");
        String addressCode = scenarioTester.getCellValue("Address Code");

        PrincipalMemberDetailAddress policyHolderAddress = null;
        if (policyHolder != null) {
            policyHolderAddress = policyHolder.getAddress();
        }

        PrincipalMemberDetailAddress principalMemberDetailAddress = new PrincipalMemberDetailAddress();

        if (!addressId.isEmpty()) {
            principalMemberDetailAddress.setAddressid(addressId);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressAddressId = policyHolderAddress.getAddressid();
                principalMemberDetailAddress.setAddressid(policyHolderAddressAddressId);
            }
        }

        if (!addressType.isEmpty()) {
            principalMemberDetailAddress.setType(addressType);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressType = policyHolderAddress.getType();
                principalMemberDetailAddress.setType(policyHolderAddressType);
            }
        }

        if (!addressLineOne.isEmpty()) {
            principalMemberDetailAddress.setLine1(addressLineOne);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressLine1 = policyHolderAddress.getLine1();
                principalMemberDetailAddress.setLine1(policyHolderAddressLine1);
            }
        }

        if (!addressLineTwo.isEmpty()) {
            principalMemberDetailAddress.setLine2(addressLineTwo);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressLine2 = policyHolderAddress.getLine2();
                principalMemberDetailAddress.setLine2(policyHolderAddressLine2);
            }
        }

        if (!addressSuburb.isEmpty()) {
            principalMemberDetailAddress.setSuburb(addressSuburb);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressSuburb = policyHolderAddress.getSuburb();
                principalMemberDetailAddress.setSuburb(policyHolderAddressSuburb);
            }
        }

        if (!addressCity.isEmpty()) {
            principalMemberDetailAddress.setCity(addressCity);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressCity = policyHolderAddress.getCity();
                principalMemberDetailAddress.setCity(policyHolderAddressCity);
            }
        }

        if (!addressCode.isEmpty()) {
            principalMemberDetailAddress.setCode(addressCode);
        } else {
            if (policyHolderAddress != null) {
                String policyHolderAddressCode = policyHolderAddress.getCode();
                principalMemberDetailAddress.setCode(policyHolderAddressCode);
            }
        }

        principalMemberDetailPolicyHolder.setAddress(principalMemberDetailAddress);
    }

    private void setPolicyHolderContactDetailValues(PrincipalMemberDetailPolicyHolder policyHolder, PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder) throws Exception {
        String cell = scenarioTester.getCellValue("Cell");
        String home = scenarioTester.getCellValue("Home");
        String work = scenarioTester.getCellValue("Work");
        String fax = scenarioTester.getCellValue("Fax");
        String email = scenarioTester.getCellValue("Email");
        String prefComCode = scenarioTester.getCellValue("Pref Com Code");
        String prefComDesc = scenarioTester.getCellValue("Pref Com Desc");
        String cisEmail = scenarioTester.getCellValue("Cis Email");
        String cisCell = scenarioTester.getCellValue("Cis Cell");
        String inContactEmail = scenarioTester.getCellValue("In Contact Email");
        String inContactCell = scenarioTester.getCellValue("In Contact Cell");

        PrincipalMemberDetailContactDetail policyHolderContactDetail = null;
        if (policyHolder != null) {
            policyHolderContactDetail = policyHolder.getContactdetail();
        }

        PrincipalMemberDetailContactDetail principalMemberDetailContactDetail = new PrincipalMemberDetailContactDetail();

        if (!cell.isEmpty()) {
            principalMemberDetailContactDetail.setCell(cell);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailCell = policyHolderContactDetail.getCell();
                principalMemberDetailContactDetail.setCell(policyHolderContactDetailCell);
            }
        }

        if (!home.isEmpty()) {
            principalMemberDetailContactDetail.setHome(home);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailHome = policyHolderContactDetail.getHome();
                principalMemberDetailContactDetail.setHome(policyHolderContactDetailHome);
            }
        }

        if (!work.isEmpty()) {
            principalMemberDetailContactDetail.setWork(work);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailWork = policyHolderContactDetail.getWork();
                principalMemberDetailContactDetail.setWork(policyHolderContactDetailWork);
            }
        }

        if (!fax.isEmpty()) {
            principalMemberDetailContactDetail.setFax(fax);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailFax = policyHolderContactDetail.getFax();
                principalMemberDetailContactDetail.setFax(policyHolderContactDetailFax);
            }
        }

        if (!email.isEmpty()) {
            principalMemberDetailContactDetail.setEmail(email);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailEmail = policyHolderContactDetail.getEmail();
                principalMemberDetailContactDetail.setEmail(policyHolderContactDetailEmail);
            }
        }

        if (!prefComCode.isEmpty()) {
            principalMemberDetailContactDetail.setPrefcomcde(prefComCode);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailPrefComCode = policyHolderContactDetail.getPrefcomcde();
                principalMemberDetailContactDetail.setPrefcomcde(policyHolderContactDetailPrefComCode);
            }
        }

        if (!prefComDesc.isEmpty()) {
            principalMemberDetailContactDetail.setPrefcomdsc(prefComDesc);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailPrefComDesc = policyHolderContactDetail.getPrefcomdsc();
                principalMemberDetailContactDetail.setPrefcomdsc(policyHolderContactDetailPrefComDesc);
            }
        }

        if (!cisEmail.isEmpty()) {
            principalMemberDetailContactDetail.setCisemail(cisEmail);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailCisEmail = policyHolderContactDetail.getCisemail();
                principalMemberDetailContactDetail.setCisemail(policyHolderContactDetailCisEmail);
            }
        }

        if (!cisCell.isEmpty()) {
            principalMemberDetailContactDetail.setCiscell(cisCell);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailCisCell = policyHolderContactDetail.getCiscell();
                principalMemberDetailContactDetail.setCiscell(policyHolderContactDetailCisCell);
            }
        }

        if (!inContactEmail.isEmpty()) {
            principalMemberDetailContactDetail.setIncontactemail(inContactEmail);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailInContactEmail = policyHolderContactDetail.getIncontactemail();
                principalMemberDetailContactDetail.setIncontactemail(policyHolderContactDetailInContactEmail);
            }
        }

        if (!inContactCell.isEmpty()) {
            principalMemberDetailContactDetail.setIncontactcell(inContactCell);
        } else {
            if (policyHolderContactDetail != null) {
                String policyHolderContactDetailInContactCell = policyHolderContactDetail.getIncontactcell();
                principalMemberDetailContactDetail.setIncontactcell(policyHolderContactDetailInContactCell);
            }
        }

        principalMemberDetailPolicyHolder.setContactdetail(principalMemberDetailContactDetail);
    }

    private void setPolicyHolderGeneralDetailValues(PrincipalMemberDetailPolicyHolder policyHolder, PrincipalMemberDetailPolicyHolder principalMemberDetailPolicyHolder) throws Exception {
        String roleId = scenarioTester.getCellValue("Role Id");
        String fullName = scenarioTester.getCellValue("Full Name");
        String middleName = scenarioTester.getCellValue("Middle Name");
        String idNumber = scenarioTester.getCellValue("Id Number");
        String idType = scenarioTester.getCellValue("Id Type");
        String income = scenarioTester.getCellValue("Income");
        String ucn = scenarioTester.getCellValue("Ucn");
        String birthDate = scenarioTester.getCellValue("Birth Date");
        String genderCode = scenarioTester.getCellValue("Gender Code");
        String genderDesc = scenarioTester.getCellValue("Gender Desc");
        String language = scenarioTester.getCellValue("Language");
        String marstCode = scenarioTester.getCellValue("Marst Code");
        String marstDesc = scenarioTester.getCellValue("Marst Desc");

        if (!roleId.isEmpty()) {
            principalMemberDetailPolicyHolder.setRoleid(roleId);
        } else {
            if (policyHolder != null) {
                String policyHolderRoleId = policyHolder.getRoleid();
                principalMemberDetailPolicyHolder.setRoleid(policyHolderRoleId);
            }
        }

        if (!fullName.isEmpty()) {
            principalMemberDetailPolicyHolder.setFullname(fullName);
        } else {
            if (policyHolder != null) {
                String policyHolderFullName = policyHolder.getFullname();
                principalMemberDetailPolicyHolder.setFullname(policyHolderFullName);
            }
        }

        if (!middleName.isEmpty()) {
            principalMemberDetailPolicyHolder.setMiddlename(middleName);
        } else {
            if (policyHolder != null) {
                String policyHolderMiddleName = policyHolder.getMiddlename();
                principalMemberDetailPolicyHolder.setMiddlename(policyHolderMiddleName);
            }
        }

        if (!idNumber.isEmpty()) {
            principalMemberDetailPolicyHolder.setIdnumber(idNumber);
        } else {
            if (policyHolder != null) {
                String policyHolderIdNumber = policyHolder.getIdnumber();
                principalMemberDetailPolicyHolder.setIdnumber(policyHolderIdNumber);
            }
        }

        if (!idType.isEmpty()) {
            principalMemberDetailPolicyHolder.setIdtype(idType);
        } else {
            if (policyHolder != null) {
                String policyHolderIdType = policyHolder.getIdtype();
                principalMemberDetailPolicyHolder.setIdtype(policyHolderIdType);
            }
        }

        if (!income.isEmpty()) {
            principalMemberDetailPolicyHolder.setIncome(income);
        } else {
            if (policyHolder != null) {
                String policyHolderIncome = policyHolder.getIncome();
                principalMemberDetailPolicyHolder.setIncome(policyHolderIncome);
            }
        }

        if (!ucn.isEmpty()) {
            principalMemberDetailPolicyHolder.setUcn(ucn);
        } else {
            if (policyHolder != null) {
                String policyHolderUcn = policyHolder.getUcn();
                principalMemberDetailPolicyHolder.setUcn(policyHolderUcn);
            }
        }

        if (!birthDate.isEmpty()) {
            principalMemberDetailPolicyHolder.setBirthdate(birthDate);
        } else {
            if (policyHolder != null) {
                String policyHolderBirthDate = policyHolder.getBirthdate();
                principalMemberDetailPolicyHolder.setBirthdate(policyHolderBirthDate);
            }
        }

        if (!genderCode.isEmpty()) {
            principalMemberDetailPolicyHolder.setGendercode(genderCode);
        } else {
            if (policyHolder != null) {
                String policyHolderGenderCode = policyHolder.getGendercode();
                principalMemberDetailPolicyHolder.setGendercode(policyHolderGenderCode);
            }
        }

        if (!genderDesc.isEmpty()) {
            principalMemberDetailPolicyHolder.setGenderdesc(genderDesc);
        } else {
            if (policyHolder != null) {
                String policyHolderGenderDesc = policyHolder.getGenderdesc();
                principalMemberDetailPolicyHolder.setGenderdesc(policyHolderGenderDesc);
            }
        }

        if (!language.isEmpty()) {
            principalMemberDetailPolicyHolder.setLanguage(language);
        } else {
            if (policyHolder != null) {
                String policyHolderLanguage = policyHolder.getLanguage();
                principalMemberDetailPolicyHolder.setLanguage(policyHolderLanguage);
            }
        }

        if (!marstCode.isEmpty()) {
            principalMemberDetailPolicyHolder.setMarstscde(marstCode);
        } else {
            if (policyHolder != null) {
                String policyHolderMarstsCode = policyHolder.getMarstscde();
                principalMemberDetailPolicyHolder.setMarstscde(policyHolderMarstsCode);
            }
        }

        if (!marstDesc.isEmpty()) {
            principalMemberDetailPolicyHolder.setMarstsdsc(marstDesc);
        } else {
            if (policyHolder != null) {
                String policyHolderMarstsDesc = policyHolder.getMarstsdsc();
                principalMemberDetailPolicyHolder.setMarstsdsc(policyHolderMarstsDesc);
            }

        }
    }

    private PrincipalMemberResponseOutput getPrincipalMemberDetails(String policyNumber) throws ServiceException {
        if (getPrincipalMemberRequestInput == null) {
            getPrincipalMemberRequestInput = new GetPrincipalMemberRequestInput();
            getPrincipalMemberRequestInput.setUsername(username);
            getPrincipalMemberRequestInput.setPolicy(policyNumber);
        }
        GetPrincipalMemberDetailsTester getPrincipalMemberDetailsTester = new GetPrincipalMemberDetailsTester(getPrincipalMemberRequestInput);

        StringBuilder getPrincipalMemberErrorStringBuilder = new StringBuilder();
        try {
            PrincipalMemberResponseOutput principalMemberResponseOutput = getPrincipalMemberDetailsTester.sendGetPrincipalDetailsRequest();
            return principalMemberResponseOutput;
        } catch (Exception e) {
            responseErrors = getPrincipalMemberDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPrincipalMemberErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPrincipalMemberErrorStringBuilder.toString());
        }
    }

    public void performValidations() throws Exception {

    }

    public void checkAuditTrail() throws ServiceException {
        if (getAuditTrailRequestInput == null) {
            getAuditTrailRequestInput = new GetAuditTrailRequestInput();
            getAuditTrailRequestInput.setUsername(username);
            getAuditTrailRequestInput.setPolicy(policyNumber);
        }

        GetAuditTrailTester getAuditTrailTester = new GetAuditTrailTester(getAuditTrailRequestInput);

        StringBuilder getAuditTrailErrorStringBuilder = new StringBuilder();
        try {
            getAuditTrailResponseOutput = getAuditTrailTester.sendGetAuditTrailRequest();
        } catch (Exception e) {
            responseErrors = getAuditTrailTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getAuditTrailErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getAuditTrailErrorStringBuilder.toString());
        }

        // TODO: Audit Trail Validations
    }
}
