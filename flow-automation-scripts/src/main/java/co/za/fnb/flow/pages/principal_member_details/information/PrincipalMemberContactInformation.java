package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberContactInformationPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberContactInformation extends PrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(PrincipalMemberContactInformation.class);
    PrincipalMemberContactInformationPageObjects principalMemberContactInformationPageObjects =
            new PrincipalMemberContactInformationPageObjects(driver, scenarioOperator);

    public PrincipalMemberContactInformation(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void moveToPreferredContactMethod() throws Exception {
        try {
            moveToElement(principalMemberContactInformationPageObjects.getPreferredContactMethod());
        } catch (Exception e) {
            log.error("Error while moving to Preferred Contact Method.");
            throw new Exception("Error while moving to Preferred Contact Method.");
        }
    }

    public void selectPreferredContactMethod(String gender) throws Exception {
        try {
            selectOnDashboard(principalMemberContactInformationPageObjects.getPreferredContactMethodSelect(),
                    principalMemberContactInformationPageObjects.getPreferredContactMethodSelectItems(),
                    gender);
        } catch (Exception e) {
            log.error("Error while selecting Preferred Contact Method.");
            throw new Exception("Error while selecting Preferred Contact Method.");
        }
    }

    public void clearEmailAddress() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getEmailAddress());
        } catch (Exception e) {
            log.error("Error while clearing email.");
            throw new Exception("Error while clearing email.");
        }
    }

    public void updateEmailAddress(String email) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getEmailAddress(), email);
        } catch (Exception e) {
            log.error("Error while updating email");
            throw new Exception("Error while updating email");
        }
    }

    public void clearCisEmailAddress() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getCisEmailAddress());
        } catch (Exception e) {
            log.error("Error while clearing cis email.");
            throw new Exception("Error while clearing cis email.");
        }
    }

    public void updateCisEmailAddress(String cisEmail) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getCisEmailAddress(), cisEmail);
        } catch (Exception e) {
            log.error("Error while updating cis email");
            throw new Exception("Error while updating cis email");
        }
    }

    public String getInContactEmailAddress() throws Exception {
        try {
            String inContactEmailAddress = getAttribute(principalMemberContactInformationPageObjects.getInContactEmailAddress(), "value");
            return inContactEmailAddress;
        } catch (Exception e) {
            log.error("Error while getting In Contact Email Address.");
            throw new Exception("Error while getting In Contact Email Address.");
        }
    }

    public void clearCellPhoneNumber() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getCellPhoneNumber());
        } catch (Exception e) {
            log.error("Error while clearing Cell Phone Number.");
            throw new Exception("Error while clearing Cell Phone Number.");
        }
    }

    public void updateCellPhoneNumber(String cellPhoneNumber) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getCellPhoneNumber(), cellPhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Cell Phone Number");
            throw new Exception("Error while updating Cell Phone Number");
        }
    }

    public String getCellPhoneNumber() throws Exception {
        try {
            String cellPhoneNumber = getText(principalMemberContactInformationPageObjects.getCellPhoneNumber());
            return cellPhoneNumber;
        } catch (Exception e) {
            log.error("Error while getting Cell Phone Number.");
            throw new Exception("Error while getting Cell Phone Number.");
        }
    }

    public void clearCisCellPhoneNumber() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getCisCellPhoneNumber());
        } catch (Exception e) {
            log.error("Error while clearing Cis Cell Phone Number.");
            throw new Exception("Error while clearing Cis Cell Phone Number.");
        }
    }

    public void updateCisCellPhoneNumber(String cisCellPhoneNumber) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getCisCellPhoneNumber(), cisCellPhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Cis Cell Phone Number");
            throw new Exception("Error while updating CisCell Phone Number");
        }
    }

    public String getCisCellPhoneNumber() throws Exception {
        try {
            String cisCellPhoneNumber = getText(principalMemberContactInformationPageObjects.getCisCellPhoneNumber());
            return cisCellPhoneNumber;
        } catch (Exception e) {
            log.error("Error while getting Cis Cell Phone Number.");
            throw new Exception("Error while getting Cis Cell Phone Number.");
        }
    }

    public String getInContactCellPhoneNumber() throws Exception {
        try {
            String inContactCellPhoneNumber = getAttribute(principalMemberContactInformationPageObjects.getInContactCellPhoneNumber(), "value");
            return inContactCellPhoneNumber;
        } catch (Exception e) {
            log.error("Error while getting In Contact Cell Phone Number.");
            throw new Exception("Error while getting In Contact Cell Phone Number.");
        }
    }

    public void clearWorkPhoneNumber() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getWorkPhoneNumber());
        } catch (Exception e) {
            log.error("Error while clearing Work Phone Number.");
            throw new Exception("Error while clearing Work Phone Number.");
        }
    }

    public void updateWorkPhoneNumber(String workCellPhoneNumber) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getWorkPhoneNumber(), workCellPhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Work Phone Number");
            throw new Exception("Error while updating Work Phone Number");
        }
    }

    public String getWorkPhoneNumber() throws Exception {
        try {
            String workCellPhoneNumber = getText(principalMemberContactInformationPageObjects.getWorkPhoneNumber());
            return workCellPhoneNumber;
        } catch (Exception e) {
            log.error("Error while getting Work Phone Number.");
            throw new Exception("Error while getting Work Phone Number.");
        }
    }

    public void clearHomePhoneNumber() throws Exception {
        try {
            clear(principalMemberContactInformationPageObjects.getHomePhoneNumber());
        } catch (Exception e) {
            log.error("Error while clearing Work Phone Number.");
            throw new Exception("Error while clearing Work Phone Number.");
        }
    }

    public void updateHomePhoneNumber(String homePhoneNumber) throws Exception {
        try {
            type(principalMemberContactInformationPageObjects.getHomePhoneNumber(), homePhoneNumber);
        } catch (Exception e) {
            log.error("Error while updating Work Phone Number");
            throw new Exception("Error while updating Work Phone Number");
        }
    }

    public String getHomePhoneNumber() throws Exception {
        try {
            String workCellPhoneNumber = getText(principalMemberContactInformationPageObjects.getHomePhoneNumber());
            return workCellPhoneNumber;
        } catch (Exception e) {
            log.error("Error while getting Work Phone Number.");
            throw new Exception("Error while getting Work Phone Number.");
        }
    }

}
