package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CaptureNewClaimPageObjects {

    @FindBy(id = "createNewClaimPanel_header")
    WebElement header;

    @FindBy(id = "claimForm:policyHolderName")
    WebElement policyHolderName;

    // region Capture New Claim
    @FindBy(id = "claimForm:policyHolderId")
    WebElement policyHolderId;

    @FindBy(id = "claimForm:claimsHandler")
    WebElement claimsHandler;

    @FindBy(id = "claimForm:selectEvent")
    WebElement selectEvent;

    @FindBy(id = "claimForm:selectEvent_label")
    WebElement selectEventLabel;

    @FindBy(id = "claimForm:selectEvent_items")
    WebElement selectEventItems; // Uses data-label

    @FindBy(id = "claimForm:eventDescription")
    WebElement eventDescription;

    @FindBy(id = "claimForm:eventDate_input")
    WebElement eventDate;

    @FindBy(id = "claimForm:claimsRegDate")
    WebElement claimsRegDate;

    @FindBy(id = "claimForm:selectClaimant_label")
    WebElement selectClaimantLabel;

    @FindBy(id = "claimForm:selectClaimant_items")
    WebElement selectClaimantItems; // Uses data-label

    @FindBy(id = "claimForm:selectStatus")
    WebElement selectStatus; // Status

    @FindBy(id = "claimForm:selectType_label")
    WebElement selectTypeLabel;

    @FindBy(id = "claimForm:selectType_items")
    WebElement selectTypeItems; // Uses data-label

    @FindBy(id = "claimForm:attorneyCompanyName")
    WebElement attorneyCompanyName;

    @FindBy(id = "claimForm:attorneyCellNumber")
    WebElement attorneyCellNumber;

    @FindBy(id = "claimForm:attorneyEmail")
    WebElement attorneyEmail;

    @FindBy(id = "claimForm:attorneyType")
    WebElement attorneyType;

    @FindBy(id = "claimForm:submit")
    WebElement submit;

    @FindBy(id = "claimForm:clear")
    WebElement clear;
    // endregion

    // region Search Attorney
    @FindBy(id = "claimAttorneyForm:province")
    WebElement province;

    @FindBy(id = "claimAttorneyForm:province_label")
    WebElement provinceLabel;

    @FindBy(id = "claimAttorneyForm:province_panel")
    WebElement provincePanel;

    @FindBy(id = "claimAttorneyForm:province_filter")
    WebElement provinceFilter;

    @FindBy(id = "ui-icon ui-icon-search")
    WebElement provinceFilterSearch;

    @FindBy(id = "claimAttorneyForm:province_items")
    WebElement provinceItems;

    @FindBy(id = "claimAttorneyForm:city_label")
    WebElement selectCityLabel;

    @FindBy(id = "claimAttorneyForm:city_items")
    WebElement selectCityItems; // Uses data-label

    @FindBy(id = "claimAttorneyForm:cellNo")
    WebElement cellNo;

    @FindBy(id = "claimAttorneyForm:attName")
    WebElement attorneyName;

    @FindBy(id = "claimAttorneyForm:search")
    WebElement search;
    // endregion

    // region Attorney Information
    @FindBy(id = "claimAttorneySearchForm:claimsInformationTable_data")
    WebElement claimsInformationTableData; // Pass this to selectUsingTableRowData(int Row)

    @FindBy(id = "claimAttorneySearchForm:addAttorney")
    WebElement addAttorney;
    // endregion

    // region Close
    @FindBy(xpath = "//div[contains(@id, 'createSearchItemView:mainTabView:Create_dlg')]//div//a//span[contains(@class, 'ui-icon ui-icon-closethick')]")
    WebElement closethick;

    @FindBy(xpath = "//div[contains(@id, 'createSearchItemView:mainTabView:Create_dlg')]//div//a[contains(@class, 'ui-dialog-titlebar-icon ui-dialog-titlebar-close ui-corner-all')]")
    WebElement uidialogTitleBarCloseUiCornerAll;
    // endregion

    By attorneyInformationTable = By.xpath("//tbody[contains(@id, 'claimAttorneySearchForm:claimsInformationTable_data')]//tr[contains(@data-ri, '')]");

    public CaptureNewClaimPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getHeader() {
        return header;
    }

    public WebElement getPolicyHolderName() {
        return policyHolderName;
    }

    public WebElement getPolicyHolderId() {
        return policyHolderId;
    }

    public WebElement getClaimsHandler() {
        return claimsHandler;
    }

    public WebElement getSelectEvent() {
        return selectEvent;
    }

    public WebElement getSelectEventLabel() {
        return selectEventLabel;
    }

    public WebElement getSelectEventItems() {
        return selectEventItems;
    }

    public WebElement getEventDescription() {
        return eventDescription;
    }

    public WebElement getEventDate() {
        return eventDate;
    }

    public WebElement getClaimsRegDate() {
        return claimsRegDate;
    }

    public WebElement getSelectClaimantLabel() {
        return selectClaimantLabel;
    }

    public WebElement getSelectClaimantItems() {
        return selectClaimantItems;
    }

    public WebElement getSelectStatus() {
        return selectStatus;
    }

    public WebElement getSelectTypeLabel() {
        return selectTypeLabel;
    }

    public WebElement getSelectTypeItems() {
        return selectTypeItems;
    }

    public WebElement getAttorneyCompanyName() {
        return attorneyCompanyName;
    }

    public WebElement getAttorneyCellNumber() {
        return attorneyCellNumber;
    }

    public WebElement getAttorneyEmail() {
        return attorneyEmail;
    }

    public WebElement getAttorneyType() {
        return attorneyType;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getClear() {
        return clear;
    }

    public WebElement getProvince() {
        return province;
    }

    public WebElement getProvinceLabel() {
        return provinceLabel;
    }

    public WebElement getProvincePanel() {
        return provincePanel;
    }

    public WebElement getProvinceFilter() {
        return provinceFilter;
    }

    public WebElement getProvinceFilterSearch() {
        return provinceFilterSearch;
    }

    public WebElement getProvinceItems() {
        return provinceItems;
    }

    public WebElement getSelectCityLabel() {
        return selectCityLabel;
    }

    public WebElement getSelectCityItems() {
        return selectCityItems;
    }

    public WebElement getCellNo() {
        return cellNo;
    }

    public WebElement getAttorneyName() {
        return attorneyName;
    }

    public WebElement getSearch() {
        return search;
    }

    public WebElement getClaimsInformationTableData() {
        return claimsInformationTableData;
    }

    public WebElement getAddAttorney() {
        return addAttorney;
    }

    public WebElement getCloseThick() {
        return closethick;
    }

    public WebElement getUidialogTitleBarCloseUiCornerAll() {
        return uidialogTitleBarCloseUiCornerAll;
    }

    public By getAttorneyInformationTable() {
        return attorneyInformationTable;
    }
}
