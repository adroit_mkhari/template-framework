package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.models.custom_exceptions.ValidationException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.GetPolicyBasicTester;
import co.za.fnb.flow.tester.services.GetPolicyDetailsTester;
import co.za.fnb.flow.tester.services.UpdatePolicyTester;
import co.za.fnb.flow.tester.services.models.MemberVariableFields;
import co.za.fnb.flow.tester.services.models.TakeupScenario;
import co.za.fnb.flow.tester.services.models.TargetMemberVariables;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import generated.*;
import generated.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import java.util.*;

public class UpdatePolicy {
    private Logger log  = LogManager.getLogger(UpdatePolicy.class);
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private TakeupScenario takeupScenario;
    private UpdatePolicyRequestInput updatePolicyRequestInput;
    private GetPolicyDetailRequestInput getPolicyDetailRequestInput;
    private GetPolicyDetailResponseOutput getPolicyDetailResponseOutputBefore;
    private ResponsePolicyBasic policyBasicBefore;
    private ResponsePolicyBasic policyBasicAfter;
    private GetPolicyDetailResponseOutput getPolicyDetailResponseOutputAfter;
    private ResponsePolicyRoleType targetRoleBefore;
    private ResponsePolicyRoleType targetRoleAfter;
    private final String CLEAR = "CLEAR";
    private ScenarioOperator scenarioOperator;
    private String username;

    public UpdatePolicy(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public UpdatePolicyRequestInput getUpdatePolicyRequestInput() {
        return updatePolicyRequestInput;
    }

    public void setUpdatePolicyRequestInput(UpdatePolicyRequestInput updatePolicyRequestInput) {
        this.updatePolicyRequestInput = updatePolicyRequestInput;
    }

    public GetPolicyDetailRequestInput getGetPolicyDetailRequestInput() {
        return getPolicyDetailRequestInput;
    }

    public void setGetPolicyDetailRequestInput(GetPolicyDetailRequestInput getPolicyDetailRequestInput) {
        this.getPolicyDetailRequestInput = getPolicyDetailRequestInput;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void sendRequest() throws Exception {
        try {
            scenarioOperator = scenarioTester.getScenarioOperator();
            String policyNumber = scenarioOperator.getPolicyNumber();
            String function = scenarioOperator.getFunction();
            username = scenarioTester.getCellValue("Username");

            if (updatePolicyRequestInput == null) {
                updatePolicyRequestInput = new UpdatePolicyRequestInput();

                updatePolicyRequestInput.setPolicy(policyNumber);
                updatePolicyRequestInput.setUsername(username);

                getPolicyDetailResponseOutputBefore = getPolicyDetails(policyNumber, username);
                policyBasicBefore = getPolicyBasic(policyNumber, username);

                //  Add Member
                //  Add Authorised Person
                //  Edit Member
                //  Quote - New member
                //  Quote - Existing member
                //  Delete Member
                //  Cancel Plan
                //  Update Banking Information
                //  Update Beneficiary Details
                //  Reinstate Plan
                //  Escalation
                //  Activate Premium status
                //  Convert
                //  Retain Policy
                if (getPolicyDetailResponseOutputBefore != null) {
                    if (function.equalsIgnoreCase("Update Policy")) {
                        updatePolicy();
                    } else if (function.equalsIgnoreCase("Add Member")) {
                        addMember();
                    } else if (function.equalsIgnoreCase("Add Authorised Person")) {
                        // TODO: Add The Correct Method For It
                        addMember();
                    } else if (function.equalsIgnoreCase("Edit Member")) {
                        editMember();
                    } else if (function.equalsIgnoreCase("Delete Member")) {
                        // TODO: Add The Correct Method For It
                        addMember();
                    } else if (function.equalsIgnoreCase("Update Banking Information")) {
                        editBanking();
                    } else if (function.equalsIgnoreCase("Update Beneficiary Details")) {
                        editBeneficiaryDetails();
                    } else if (function.equalsIgnoreCase("Escalation")) {
                        escalation();
                    } else if (function.equalsIgnoreCase("Activate Premium status")) {
                        updatePremiumStatus();
                    }
                } else {
                    log.error("Error while getting policy. Policy possibly does now exist.");
                    throw new Exception("Error while getting policy. Policy possibly does now exist.");
                }
            }

            UpdatePolicyTester updatePolicyTester = new UpdatePolicyTester(updatePolicyRequestInput);

            StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
            try {
                updatePolicyTester.sendUpdatePolicyRequest();
            } catch (Exception e) {
                responseErrors = updatePolicyTester.getResponseErrors();
                Errors errors = responseErrors.getErrors();
                List<generated.Error> errorList = errors.getError();
                for (Error error : errorList) {
                    updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                            .append(", Subject: ").append(error.getSubject())
                            .append(", Description: ").append(error.getDescription())
                            .append(", Adjunct: ").append(error.getAdjunct())
                            .append("\n");
                }
                throw new ServiceException(updatePolicyErrorStringBuilder.toString());
            }

            getPolicyDetailResponseOutputAfter = getPolicyDetails(policyNumber, username);
            policyBasicAfter = getPolicyBasic(policyNumber, username);
        } catch (Exception e) {
            log.error("Error while send request to Update Policy. " + e.getMessage());
            throw new Exception("Error while send request to Update Policy. " + e.getMessage());
        }
    }

    public void performValidations() throws Exception {
        try {
            String function = scenarioOperator.getFunction();
            if (scenarioOperator != null) {
                if (function.equalsIgnoreCase("Update Policy")) {
                    updatePolicyValidations();
                } else if (function.equalsIgnoreCase("Add Member")) {
                    addMemberValidations();
                } else if (function.equalsIgnoreCase("Add Authorised Person")) {
                    // TODO: Add The Correct Method For It
                    addMemberValidations();
                } else if (function.equalsIgnoreCase("Edit Member")) {
                    editMemberValidations();
                } else if (function.equalsIgnoreCase("Delete Member")) {
                    // TODO: Add The Correct Method For It
                    addMemberValidations();
                } else if (function.equalsIgnoreCase("Update Banking Information")) {
                    editBankingValidations();
                } else if (function.equalsIgnoreCase("Update Beneficiary Details")) {
                    editBeneficiaryDetailsValidations();
                } else if (function.equalsIgnoreCase("Escalation")) {
                    escalationValidations();
                } else if (function.equalsIgnoreCase("Activate Premium status")) {
                    updatePremiumStatusValidations();
                }
            } else {
                log.error("No ScenarioOperator Set For Validations.");
                throw new Exception("No ScenarioOperator Set For Validations.");
            }
        } catch (Exception e) {
            String exceptionMessage = e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void updatePolicyValidations() {
        // TODO: Implement Validation Logic
    }

    private void getCurrentPremiumStatus() {
        String currentPremiumStatus = policyBasicBefore.getPremiumstatus();
        updatePolicyRequestInput.setPremiumstatus(currentPremiumStatus);
    }

    private void getCurrentEscalationStatus() {
        String currentEscalationStatus = policyBasicBefore.getEscalationStatus();
        updatePolicyRequestInput.setEscalation(currentEscalationStatus);
    }

    public ResponsePolicyBasic getPolicyBasic(String policyNumber, String username) throws Exception {
        try {
            GetPolicyBasicRequestInput getPolicyBasicRequestInput = new GetPolicyBasicRequestInput();
            getPolicyBasicRequestInput.setPolicy(policyNumber);
            getPolicyBasicRequestInput.setUsername(username);
            GetPolicyBasicTester getPolicyBasicTester = new GetPolicyBasicTester(getPolicyBasicRequestInput);
            GetPolicyBasicResponseOutput getPolicyBasicResponseOutput = getPolicyBasicTester.sendGetPolicyBasicRequest();
            ResponsePolicyBasic policyBasic = getPolicyBasicResponseOutput.getPolicy();
            return policyBasic;
        } catch (Exception e) {
            log.error("Error while getting Policy Basic for Policy Number. " + policyNumber);
            throw new Exception("Error while getting Policy Basic for Policy Number. " + policyNumber);
        }
    }

    public GetPolicyDetailResponseOutput getPolicyDetails(String policyNumber, String username) throws ServiceException {
        if (getPolicyDetailRequestInput == null) {
            getPolicyDetailRequestInput = new GetPolicyDetailRequestInput();
            getPolicyDetailRequestInput.setPolicy(policyNumber);
            getPolicyDetailRequestInput.setUsername(username);
        }
        GetPolicyDetailsTester getPolicyDetailsTester = new GetPolicyDetailsTester(getPolicyDetailRequestInput);

        StringBuilder getPolicyDetailsErrorStringBuilder = new StringBuilder();
        try {
            GetPolicyDetailResponseOutput getPolicyDetailResponseOutput = getPolicyDetailsTester.sendGetPolicyDetailsRequest();
            return getPolicyDetailResponseOutput;
        } catch (Exception e) {
            responseErrors = getPolicyDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPolicyDetailsErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPolicyDetailsErrorStringBuilder.toString());
        }
    }

    private void updatePolicy() throws Exception {
        String escalation = scenarioTester.getCellValue("Escalation");
        String premiumStatus = scenarioTester.getCellValue("Premium Status");
        updatePolicyRequestInput.setEscalation(escalation);
        updatePolicyRequestInput.setPremiumstatus(premiumStatus);

        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
        String policyCoverAmountBefore = policyBefore.getCoveramount();
        String policyPremiumBefore = policyBefore.getPremium();
        ResponseRoles policyRolesBefore = policyBefore.getRoles();
        List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

        Banking banking = computeBanking();
        updatePolicyRequestInput.setBanking(banking);

        PremiumPayer premiumPayer = computePremiumPayer();
        updatePolicyRequestInput.setPremiumpayer(premiumPayer);

        Beneficiary beneficiary = computeBeneficiary();
        updatePolicyRequestInput.setBeneficiary(beneficiary);

        UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();

        UpdatePolicyRoleType updatePolicyRoleType = new UpdatePolicyRoleType();
        computePolicyHolderRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

        updatePolicyRoleType = new UpdatePolicyRoleType();
        computeSpouseRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

        String childRoleActions = scenarioTester.getCellValue("Child Role Id(s) - Action(s)");
        String[] childRoleActionsList = childRoleActions.split("],");

        List<MemberVariableFields> childMemberVariableFields = new LinkedList<>();
        getMemberVariableFields(childRoleActionsList, childMemberVariableFields);

        Multimap<String, String> childActions = ArrayListMultimap.create();
        getRoleActions(childRoleActionsList, childActions);

        childActions = childActions.keys().contains("-1")
                ? getExistingChildRoleIds(rolesBefore, childActions)
                : childActions;

        int childIndex = 0;
        for (Map.Entry<String, String> entry : childActions.entries()) {
            String roleId = entry.getKey();
            String action = entry.getValue();
            log.info("Child Role Id: " + roleId + ", Action: " + action);
            updatePolicyRoleType = new UpdatePolicyRoleType();
            MemberVariableFields memberVariableFields = childMemberVariableFields.get(childIndex++);
            computeChildRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, roleId, action);
        }

        String parentExtendedFamilyRoleActions = scenarioTester.getCellValue("Parent/Extended Family Role Id(s) - Action(s)");
        String[] parentExtendedFamilyRoleActionsList = parentExtendedFamilyRoleActions.split("],");

        List<MemberVariableFields> parentOrExtendedFamilyMemberVariableFields = new LinkedList<>();
        getMemberVariableFields(parentExtendedFamilyRoleActionsList, parentOrExtendedFamilyMemberVariableFields);

        Multimap<String, String> parentExtendedFamilyActions = ArrayListMultimap.create();
        getRoleActions(parentExtendedFamilyRoleActionsList, parentExtendedFamilyActions);

        parentExtendedFamilyActions = parentExtendedFamilyActions.keys().contains("-1")
                ? getExistingParentOrExtendedFamilyRoleIds(rolesBefore, parentExtendedFamilyActions)
                : parentExtendedFamilyActions;

        int memberIndex = 0;
        for (Map.Entry<String, String> entry : parentExtendedFamilyActions.entries()) {
            String roleId = entry.getKey();
            String action = entry.getValue();
            log.info("Parent/Extended Family Role Id: " + roleId + ", Action: " + action);
            updatePolicyRoleType = new UpdatePolicyRoleType();
            MemberVariableFields memberVariableFields = parentOrExtendedFamilyMemberVariableFields.get(memberIndex++);
            computeParentOrExtendedFamilyRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, roleId, action);
        }

        updatePolicyRequestInput.setRoles(updatePolicyRoles);
    }

    public void addMember() throws Exception {
        try {
            getCurrentEscalationStatus();
            getCurrentPremiumStatus();

            ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
            ResponseRoles policyRolesBefore = policyBefore.getRoles();
            List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

            Banking banking = getCurrentBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = getCurrentBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();

            UpdatePolicyRoleType updatePolicyRoleType = new UpdatePolicyRoleType();
            computePolicyHolderRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

            updatePolicyRoleType = new UpdatePolicyRoleType();
            computeSpouseRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

            String childRoleActions = scenarioTester.getCellValue("Child Role Id(s) - Action(s)");
            String[] childRoleActionsList = childRoleActions.split("],");

            List<MemberVariableFields> childMemberVariableFields = new LinkedList<>();
            getMemberVariableFields(childRoleActionsList, childMemberVariableFields);

            Multimap<String, String> childActions = ArrayListMultimap.create();
            getRoleActions(childRoleActionsList, childActions);

            childActions = childActions.keys().contains("-1")
                    ? getExistingChildRoleIds(rolesBefore, childActions)
                    : childActions;

            int childIndex = 0;
            for (Map.Entry<String, String> entry : childActions.entries()) {
                String roleId = entry.getKey();
                String action = entry.getValue();
                log.info("Child Role Id: " + roleId + ", Action: " + action);
                updatePolicyRoleType = new UpdatePolicyRoleType();
                MemberVariableFields memberVariableFields = childMemberVariableFields.get(childIndex++);
                computeChildRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, roleId, action);
            }

            String parentExtendedFamilyRoleActions = scenarioTester.getCellValue("Parent/Extended Family Role Id(s) - Action(s)");
            String[] parentExtendedFamilyRoleActionsList = parentExtendedFamilyRoleActions.split("],");

            List<MemberVariableFields> parentOrExtendedFamilyMemberVariableFields = new LinkedList<>();
            getMemberVariableFields(parentExtendedFamilyRoleActionsList, parentOrExtendedFamilyMemberVariableFields);

            Multimap<String, String> parentExtendedFamilyActions = ArrayListMultimap.create();
            getRoleActions(parentExtendedFamilyRoleActionsList, parentExtendedFamilyActions);

            parentExtendedFamilyActions = parentExtendedFamilyActions.keys().contains("-1")
                    ? getExistingParentOrExtendedFamilyRoleIds(rolesBefore, parentExtendedFamilyActions)
                    : parentExtendedFamilyActions;

            int memberIndex = 0;
            for (Map.Entry<String, String> entry : parentExtendedFamilyActions.entries()) {
                String roleId = entry.getKey();
                String action = entry.getValue();
                log.info("Parent/Extended Family Role Id: " + roleId + ", Action: " + action);
                updatePolicyRoleType = new UpdatePolicyRoleType();
                MemberVariableFields memberVariableFields = parentOrExtendedFamilyMemberVariableFields.get(memberIndex++);
                computeParentOrExtendedFamilyRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, roleId, action);
            }

            updatePolicyRequestInput.setRoles(updatePolicyRoles);

        } catch (Exception e) {
            log.error("Error while setting up Add Member. " + e.getMessage());
            throw new Exception("Error while setting up Add Member. " + e.getMessage());
        }
    }

    public void escalation() throws Exception {
        try {
            String escalation = scenarioTester.getCellValue("Escalation");
            if (!escalation.isEmpty()) {
                updatePolicyRequestInput.setEscalation(escalation);
                if (escalation.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRequestInput.setEscalation("");
                }
            }

            getCurrentPremiumStatus();

            ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
            ResponseRoles policyRolesBefore = policyBefore.getRoles();
            List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

            Banking banking = policyBefore.getBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = getCurrentBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();
            updatePolicyRequestInput.setRoles(updatePolicyRoles);

        } catch (Exception e) {
            log.error("Error while updating Escalation. " + e.getMessage());
            throw new Exception("Error while updating Escalation. " + e.getMessage());
        }
    }

    public void updatePremiumStatus() throws Exception {
        try {
            getCurrentEscalationStatus();

            String premiumStatus = scenarioTester.getCellValue("Premium Status");
            if (!premiumStatus.isEmpty()) {
                updatePolicyRequestInput.setPremiumstatus(premiumStatus);
                if (premiumStatus.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRequestInput.setPremiumstatus("");
                }
            }

            ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
            ResponseRoles policyRolesBefore = policyBefore.getRoles();
            List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

            Banking banking = policyBefore.getBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = getCurrentBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();
            updatePolicyRequestInput.setRoles(updatePolicyRoles);

        } catch (Exception e) {
            log.error("Error while updating Premium Status. " + e.getMessage());
            throw new Exception("Error while updating Premium Status. " + e.getMessage());
        }
    }

    public void editMember() throws Exception {
        try {
            getCurrentEscalationStatus();
            getCurrentPremiumStatus();

            ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
            ResponseRoles policyRolesBefore = policyBefore.getRoles();
            List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

            Banking banking = getCurrentBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = getCurrentBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();

            UpdatePolicyRoleType updatePolicyRoleType = new UpdatePolicyRoleType();
            editPolicyHolderRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

            updatePolicyRoleType = new UpdatePolicyRoleType();
            editSpouseRole(updatePolicyRoles, updatePolicyRoleType, rolesBefore);

            String childRoleActions = scenarioTester.getCellValue("Child Role Id(s) - Action(s)");
            String[] childRoleActionsList = childRoleActions.split("],");

            List<MemberVariableFields> childMemberVariableFields = new LinkedList<>();
            getMemberVariableFields(childRoleActionsList, childMemberVariableFields);

            Multimap<String, String> childActions = ArrayListMultimap.create();
            getRoleActions(childRoleActionsList, childActions);

            childActions = childActions.keys().contains("-1")
                    ? getExistingChildRoleIds(rolesBefore, childActions)
                    : childActions;

            TargetMemberVariables targetMemberVariables = getTargetMemberVariables();

            int childIndex = 0;
            for (Map.Entry<String, String> entry : childActions.entries()) {
                String roleId = entry.getKey();
                ResponsePolicyRoleType targetRole = getMatchingTargetRole(rolesBefore, targetMemberVariables, roleId);

                if (targetRole != null) {
                    targetRoleBefore = targetRole;
                    String action = entry.getValue();
                    log.info("Child Role Id: " + roleId + ", Action: " + action);
                    updatePolicyRoleType = new UpdatePolicyRoleType();
                    MemberVariableFields memberVariableFields = childMemberVariableFields.get(childIndex++);
                    editChildRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, targetRole, "CH");
                }
            }

            String parentExtendedFamilyRoleActions = scenarioTester.getCellValue("Parent/Extended Family Role Id(s) - Action(s)");
            String[] parentExtendedFamilyRoleActionsList = parentExtendedFamilyRoleActions.split("],");

            List<MemberVariableFields> parentOrExtendedFamilyMemberVariableFields = new LinkedList<>();
            getMemberVariableFields(parentExtendedFamilyRoleActionsList, parentOrExtendedFamilyMemberVariableFields);

            Multimap<String, String> parentExtendedFamilyActions = ArrayListMultimap.create();
            getRoleActions(parentExtendedFamilyRoleActionsList, parentExtendedFamilyActions);

            parentExtendedFamilyActions = parentExtendedFamilyActions.keys().contains("-1")
                    ? getExistingParentOrExtendedFamilyRoleIds(rolesBefore, parentExtendedFamilyActions)
                    : parentExtendedFamilyActions;

            int memberIndex = 0;
            for (Map.Entry<String, String> entry : parentExtendedFamilyActions.entries()) {
                String roleId = entry.getKey();
                ResponsePolicyRoleType targetRole = getMatchingTargetRole(rolesBefore, targetMemberVariables, roleId);

                if (targetRole != null) {
                    targetRoleBefore = targetRole;
                    String action = entry.getValue();
                    log.info("Parent/Extended Family Role Id: " + roleId + ", Action: " + action);
                    updatePolicyRoleType = new UpdatePolicyRoleType();
                    MemberVariableFields memberVariableFields = parentOrExtendedFamilyMemberVariableFields.get(memberIndex++);
                    editParentOrExtendedFamilyRole(updatePolicyRoles, memberVariableFields, updatePolicyRoleType, targetRole, "CH");
                }
            }

            updatePolicyRequestInput.setRoles(updatePolicyRoles);

        } catch (Exception e) {
            log.error("Error while setting up Add Member. " + e.getMessage());
            throw new Exception("Error while setting up Add Member. " + e.getMessage());
        }
    }

    private ResponsePolicyRoleType getMatchingTargetRole(List<ResponsePolicyRoleType> rolesBefore, TargetMemberVariables targetMemberVariables, String roleId) {
        ResponsePolicyRoleType targetRole = null;
        String relationship = targetMemberVariables.getRelationship();
        String name = targetMemberVariables.getName();
        String birthDate = targetMemberVariables.getBirthDate();
        String genderCode = targetMemberVariables.getGenderCode();
        log.debug("Target Role: \n" + "Relationship: " + relationship + ", Name: " + name + ", BirthDate: " + birthDate + ", Gender Code: " + genderCode);

        String roleType, roleFullName, roleBirthDate, roleGenderCode;
        for (ResponsePolicyRoleType roleBefore : rolesBefore) {
            String currentRoleId = roleBefore.getRoleid();
            if (currentRoleId.equalsIgnoreCase(roleId)) {
                roleType = roleBefore.getRoletype();
                roleFullName = roleBefore.getFullname();
                roleBirthDate = roleBefore.getBirthdate();
                roleGenderCode = roleBefore.getGendercode();
                log.debug("Checking Role: \n" + "Relationship: " + roleType + ", Name: " + roleFullName + ", BirthDate: " + roleBirthDate + ", Gender Code: " + roleGenderCode);

                if (roleType.equalsIgnoreCase(relationship)
                    && roleFullName.equalsIgnoreCase(name)
                    && roleBirthDate.equalsIgnoreCase(birthDate)
                    && roleGenderCode.equalsIgnoreCase(genderCode)) {
                    targetRole = roleBefore;
                    break;
                }
            }
        }
        return targetRole;
    }

    private TargetMemberVariables getTargetMemberVariables() throws Exception {
        String relationship = scenarioTester.getCellValue("Target Relationship");
        String name = scenarioTester.getCellValue("Target Name");
        String birthDate = scenarioTester.getCellValue("Target Birth Date");
        String genderCode = scenarioTester.getCellValue("Target Gender Code");
        return new TargetMemberVariables(relationship, name, birthDate, genderCode);
    }

    public void editBeneficiaryDetails() throws Exception {
        try {
            getCurrentEscalationStatus();
            getCurrentPremiumStatus();

            ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
            ResponseRoles policyRolesBefore = policyBefore.getRoles();
            List<ResponsePolicyRoleType> rolesBefore = policyRolesBefore.getRole();

            Banking banking = getCurrentBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = computeBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();
            updatePolicyRequestInput.setRoles(updatePolicyRoles);

        } catch (Exception e) {
            log.error("Error while running Edit Beneficiary Details. " + e.getMessage());
            throw new Exception("Error while running Edit Beneficiary Details. " + e.getMessage());
        }
    }

    public void editBanking() throws Exception {
        try {
            getCurrentEscalationStatus();
            getCurrentPremiumStatus();

            Banking banking = computeBanking();
            updateBankingPaymentAndDueDate(banking);
            updatePolicyRequestInput.setBanking(banking);

            PremiumPayer premiumPayer = getCurrentPremiumPayer();
            updatePolicyRequestInput.setPremiumpayer(premiumPayer);

            Beneficiary beneficiary = getCurrentBeneficiary();
            updatePolicyRequestInput.setBeneficiary(beneficiary);

            UpdatePolicyRoles updatePolicyRoles = new UpdatePolicyRoles();
            updatePolicyRequestInput.setRoles(updatePolicyRoles);
        } catch (Exception e) {
            log.error("Error while running Edit Banking. " + e.getMessage());
            throw new Exception("Error while running Edit Banking. " + e.getMessage());
        }
    }

    private Beneficiary getCurrentBeneficiary() {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
        Beneficiary beneficiary = new Beneficiary();
        if (policyBefore != null) {
            ResponseBeneficiary beforeBeneficiary = policyBefore.getBeneficiary();
            if (beforeBeneficiary != null) {
                beneficiary.setFullname(beforeBeneficiary.getFullname());
                beneficiary.setEmail(beforeBeneficiary.getEmail());
                beneficiary.setContactno(beforeBeneficiary.getContactno());
                beneficiary.setBirthdate(beforeBeneficiary.getBirthdate());
                beneficiary.setIdnumber(beforeBeneficiary.getIdnumber());
            }
        }
        return beneficiary;
    }

    private PremiumPayer getCurrentPremiumPayer() {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
        ResponsePremiumPayer responsePremiumPayer = policyBefore.getPremiumpayer();
        PremiumPayer premiumPayer = new PremiumPayer();

        if (policyBefore != null) {
            if (responsePremiumPayer != null) {
                premiumPayer.setFullname(responsePremiumPayer.getFullname());
                premiumPayer.setIdtype(responsePremiumPayer.getIdtype());
                premiumPayer.setIdnumber(responsePremiumPayer.getIdnumber());
            }
        }

        return premiumPayer;
    }

    private void updateBankingPaymentAndDueDate(Banking banking) throws Exception {
        String paymentDay = scenarioTester.getCellValue("Payment Day");
        String dueDate = scenarioTester.getCellValue("Due Date");

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime nowPlusFourDays = now.plusDays(4L);
        String systemTime = format.format(nowPlusFourDays);
        String[] systemTimeValues = systemTime.split("-");

        if (!dueDate.isEmpty()) {
            banking.setDuedate(dueDate);
            if (dueDate.equalsIgnoreCase(CLEAR)) {
                banking.setDuedate("");
            }
        } else {
            banking.setDuedate(systemTimeValues[0] + systemTimeValues[1] + systemTimeValues[2]);
        }

        if (!paymentDay.isEmpty()) {
            banking.setPaymentday(paymentDay);
            if (paymentDay.equalsIgnoreCase(CLEAR)) {
                banking.setPaymentday("");
            }
        } else {
            banking.setPaymentday(systemTimeValues[2]);
        }
    }

    private Banking getCurrentBanking() throws Exception {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();
        Banking beforeBanking = policyBefore.getBanking();
        Banking banking = new Banking();

        if (beforeBanking != null) {
            banking.setName(beforeBanking.getName());
            banking.setBranchname(beforeBanking.getBranchname());
            banking.setBranchcode(beforeBanking.getBranchcode());
            banking.setAccountno(beforeBanking.getAccountno());
            banking.setAccountcode(beforeBanking.getAccountcode());
            banking.setAccountdesc(beforeBanking.getAccountdesc());
            banking.setDuedate(beforeBanking.getDuedate());
            banking.setPaymentday(beforeBanking.getPaymentday());
        }

        return banking;
    }

    private Banking computeBanking() throws Exception {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();

        Banking beforeBanking = null;
        if (policyBefore != null) {
            beforeBanking = policyBefore.getBanking();
        }

        String bankName = scenarioTester.getCellValue("Bank Name");
        String bankBranchName = scenarioTester.getCellValue("Bank Branch Name");
        String bankBranchCode = scenarioTester.getCellValue("Bank Branch Code");
        String bankAccountNumber = scenarioTester.getCellValue("Bank Account Number");
        String paymentDay = scenarioTester.getCellValue("Payment Day");
        String dueDate = scenarioTester.getCellValue("Due Date");
        String bankAccountCode = scenarioTester.getCellValue("Bank Account Code");
        String bankAccountDesc = scenarioTester.getCellValue("Bank Account Desc");

        Banking banking = new Banking();
        if (!bankName.isEmpty()) {
            banking.setName(bankName);
            if (bankName.equalsIgnoreCase(CLEAR)) {
                banking.setName("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setName(beforeBanking.getName());
            }
        }

        if (!bankBranchName.isEmpty()) {
            banking.setBranchname(bankBranchName);
            if (bankBranchName.equalsIgnoreCase(CLEAR)) {
                banking.setBranchname("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setBranchname(beforeBanking.getBranchname());
            }
        }

        if (!bankBranchCode.isEmpty()) {
            banking.setBranchcode(bankBranchCode);
            if (bankBranchCode.equalsIgnoreCase(CLEAR)) {
                banking.setBranchcode("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setBranchcode(beforeBanking.getBranchcode());
            }
        }

        if (!bankAccountNumber.isEmpty()) {
            banking.setAccountno(bankAccountNumber);
            if (bankAccountNumber.equalsIgnoreCase(CLEAR)) {
                banking.setAccountno("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setAccountno(beforeBanking.getAccountno());
            }
        }

        if (!dueDate.isEmpty()) {
            banking.setDuedate(dueDate);
            if (dueDate.equalsIgnoreCase(CLEAR)) {
                banking.setDuedate("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setDuedate(beforeBanking.getDuedate());
            }
        }

        if (!paymentDay.isEmpty()) {
            banking.setPaymentday(paymentDay);
            if (paymentDay.equalsIgnoreCase(CLEAR)) {
                banking.setPaymentday("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setPaymentday(beforeBanking.getPaymentday());
            }
        }

        if (!bankAccountCode.isEmpty()) {
            banking.setAccountcode(bankAccountCode);
            if (bankAccountCode.equalsIgnoreCase(CLEAR)) {
                banking.setAccountcode("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setAccountcode(beforeBanking.getAccountcode());
            }
        }

        if (!bankAccountDesc.isEmpty()) {
            banking.setAccountdesc(bankAccountDesc);
            if (bankAccountDesc.equalsIgnoreCase(CLEAR)) {
                banking.setAccountdesc("");
            }
        } else {
            if (beforeBanking != null) {
                banking.setAccountdesc(beforeBanking.getAccountdesc());
            }
        }

        return banking;
    }

    private PremiumPayer computePremiumPayer() throws Exception {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();

        ResponsePremiumPayer beforePremiumPayer = null;
        if (policyBefore != null) {
            beforePremiumPayer = policyBefore.getPremiumpayer();
        }

        String premiumPayerFullName = scenarioTester.getCellValue("Premium Payer Full Name");
        String premiumPayerIdNumber = scenarioTester.getCellValue("Premium Payer ID Number");
        String premiumPayerIdType = scenarioTester.getCellValue("Premium Payer ID Type");

        PremiumPayer premiumPayer = new PremiumPayer();

        if (!premiumPayerFullName.isEmpty()) {
            premiumPayer.setFullname(premiumPayerFullName);
            if (premiumPayerFullName.equalsIgnoreCase(CLEAR)) {
                premiumPayer.setFullname("");
            }
        } else {
            if (beforePremiumPayer != null) {
                premiumPayer.setFullname(beforePremiumPayer.getFullname());
            }
        }

        if (!premiumPayerIdNumber.isEmpty()) {
            premiumPayer.setIdnumber(premiumPayerIdNumber);
            if (premiumPayerIdNumber.equalsIgnoreCase(CLEAR)) {
                premiumPayer.setIdnumber("");
            }
        } else {
            if (beforePremiumPayer != null) {
                premiumPayer.setIdnumber(beforePremiumPayer.getIdnumber());
            }
        }

        if (!premiumPayerIdType.isEmpty()) {
            premiumPayer.setIdtype(premiumPayerIdType);
            if (premiumPayerIdType.equalsIgnoreCase(CLEAR)) {
                premiumPayer.setIdtype("");
            }
        } else {
            if (beforePremiumPayer != null) {
                premiumPayer.setIdtype(beforePremiumPayer.getIdtype());
            }
        }

        return premiumPayer;
    }

    private Beneficiary computeBeneficiary() throws Exception {
        ResponsePolicy policyBefore = getPolicyDetailResponseOutputBefore.getPolicy();

        ResponseBeneficiary beforeBeneficiary = null;
        if (policyBefore != null) {
            beforeBeneficiary = policyBefore.getBeneficiary();
        }

        String beneficiaryFullName = scenarioTester.getCellValue("Beneficiary Full Name");
        String beneficiaryIdNumber = scenarioTester.getCellValue("Beneficiary ID Number");
        String beneficiaryBirthDate = scenarioTester.getCellValue("Beneficiary Birth Date");
        String beneficiaryContactNumber = scenarioTester.getCellValue("Beneficiary Contact Number");
        String beneficiaryEmail = scenarioTester.getCellValue("Beneficiary Email");

        Beneficiary beneficiary = new Beneficiary();

        if (!beneficiaryFullName.isEmpty()) {
            beneficiary.setFullname(beneficiaryFullName);
            if (beneficiaryFullName.equalsIgnoreCase(CLEAR)) {
                beneficiary.setFullname("");
            }
        } else {
            if (beforeBeneficiary != null) {
                beneficiary.setFullname(beforeBeneficiary.getFullname());
            }
        }

        if (!beneficiaryIdNumber.isEmpty()) {
            beneficiary.setIdnumber(beneficiaryIdNumber);
            if (beneficiaryIdNumber.equalsIgnoreCase(CLEAR)) {
                beneficiary.setIdnumber("");
            }
        } else {
            if (beforeBeneficiary != null) {
                beneficiary.setIdnumber(beforeBeneficiary.getIdnumber());
            }
        }

        if (!beneficiaryBirthDate.isEmpty()) {
            beneficiary.setBirthdate(beneficiaryBirthDate);
            if (beneficiaryBirthDate.equalsIgnoreCase(CLEAR)) {
                beneficiary.setBirthdate("");
            }
        } else {
            if (beforeBeneficiary != null) {
                beneficiary.setBirthdate(beforeBeneficiary.getBirthdate());
            }
        }

        if (!beneficiaryContactNumber.isEmpty()) {
            beneficiary.setContactno(beneficiaryContactNumber);
            if (beneficiaryContactNumber.equalsIgnoreCase(CLEAR)) {
                beneficiary.setContactno("");
            }
        } else {
            if (beforeBeneficiary != null) {
                beneficiary.setContactno(beforeBeneficiary.getContactno());
            }
        }

        if (!beneficiaryEmail.isEmpty()) {
            beneficiary.setEmail(beneficiaryEmail);
            if (beneficiaryEmail.equalsIgnoreCase(CLEAR)) {
                beneficiary.setEmail("");
            }
        } else {
            if (beforeBeneficiary != null) {
                beneficiary.setEmail(beforeBeneficiary.getEmail());
            }
        }

        return beneficiary;
    }

    private void computePolicyHolderRole(UpdatePolicyRoles updatePolicyRoles, UpdatePolicyRoleType updatePolicyRoleType, List<ResponsePolicyRoleType> rolesBefore) throws Exception {
        String policyHolderRoleId = scenarioTester.getCellValue("Policy Holder Role ID");
        String policyHolderAction = scenarioTester.getCellValue("Policy Holder Action");

        if (!(!policyHolderAction.isEmpty() && policyHolderAction.equalsIgnoreCase("N/A"))) {
            String policyHolderRoleType = scenarioTester.getCellValue("Policy Holder Role Type");
            String policyHolderFullName = scenarioTester.getCellValue("Policy Holder Full Name");
            String policyHolderMiddleName = scenarioTester.getCellValue("Policy Holder Middle Name");
            String policyHolderIdType = scenarioTester.getCellValue("Policy Holder Id Type");
            String policyHolderIdNumber = scenarioTester.getCellValue("Policy Holder ID Number");
            String policyHolderBirthDate = scenarioTester.getCellValue("Policy Holder Birth Date");
            String policyHolderGenderCode = scenarioTester.getCellValue("Policy Holder Gender Code");
            String policyHolderGenderDesc = scenarioTester.getCellValue("Policy Holder Gender Desc");
            String policyHolderContactNumber = scenarioTester.getCellValue("Policy Holder Contact Number");
            String policyHolderIncome = scenarioTester.getCellValue("Policy Holder Income");
            String policyHolderCoverAmount = scenarioTester.getCellValue("Policy Holder Cover Amount");
            String policyHolderPremium = scenarioTester.getCellValue("Policy Holder Premium");
            String policyHolderComment = scenarioTester.getCellValue("Policy Holder Comment");
            String policyHolderEmail = scenarioTester.getCellValue("Policy Holder Email");
            String policyHolderCoverAction = scenarioTester.getCellValue("Policy Holder Cover Action");
            String policyHolderCoverId = scenarioTester.getCellValue("Policy Holder Cover ID");
            String policyHolderCoverInceptionDate = scenarioTester.getCellValue("Policy Holder Cover Inception Date");
            String policyHolderCoverWaitEndDate = scenarioTester.getCellValue("Policy Holder Cover Wait End Date");

            if (policyHolderIdNumber.isEmpty()) {
                policyHolderIdNumber = scenarioTester.generateRandomSaId(policyHolderBirthDate, policyHolderIdType, policyHolderIdNumber, policyHolderGenderCode);
            }

            if (policyHolderRoleId.isEmpty()) {
                for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                    if (roleBefore.getRoletype().equalsIgnoreCase("POLICY HOLDER")) {
                        policyHolderRoleId = roleBefore.getRoleid();
                        break;
                    }
                }
            }

            updatePolicyRoleType.setRoleid(policyHolderRoleId);
            updatePolicyRoleType.setAction(policyHolderAction);
            updatePolicyRoleType.setRoletype(policyHolderRoleType);
            updatePolicyRoleType.setFullname(policyHolderFullName);
            updatePolicyRoleType.setMiddlename(policyHolderMiddleName);
            updatePolicyRoleType.setIdnumber(policyHolderIdNumber);
            updatePolicyRoleType.setBirthdate(policyHolderBirthDate);
            updatePolicyRoleType.setGendercode(policyHolderGenderCode);
            updatePolicyRoleType.setGenderdesc(policyHolderGenderDesc);
            updatePolicyRoleType.setContactno(policyHolderContactNumber);
            updatePolicyRoleType.setIncome(policyHolderIncome);
            updatePolicyRoleType.setCoveramnt(policyHolderCoverAmount);
            updatePolicyRoleType.setPremium(policyHolderPremium);
            updatePolicyRoleType.setComment(policyHolderComment);
            updatePolicyRoleType.setEmail(policyHolderEmail);

            CoverDetail coverDetail = new CoverDetail();
            Cover cover = new Cover();
            cover.setAction(policyHolderCoverAction);
            cover.setCoverid(policyHolderCoverId);
            cover.setInceptdate(policyHolderCoverInceptionDate);
            cover.setWaitenddate(policyHolderCoverWaitEndDate);
            List<Cover> covers = coverDetail.getCover();
            covers.add(cover);
            updatePolicyRoleType.setCoverdetail(coverDetail);
            updatePolicyRoles.getRole().add(updatePolicyRoleType);
        }

    }

    private void editPolicyHolderRole(UpdatePolicyRoles updatePolicyRoles, UpdatePolicyRoleType updatePolicyRoleType, List<ResponsePolicyRoleType> rolesBefore) throws Exception {
        String policyHolderRoleId = scenarioTester.getCellValue("Policy Holder Role ID");
        String policyHolderAction = scenarioTester.getCellValue("Policy Holder Action");

        if (!(!policyHolderAction.isEmpty() && policyHolderAction.equalsIgnoreCase("N/A"))) {
            if (policyHolderAction.equalsIgnoreCase("CH")) {
                String policyHolderRoleType = scenarioTester.getCellValue("Policy Holder Role Type");
                String policyHolderFullName = scenarioTester.getCellValue("Policy Holder Full Name");
                String policyHolderMiddleName = scenarioTester.getCellValue("Policy Holder Middle Name");
                String policyHolderIdType = scenarioTester.getCellValue("Policy Holder Id Type");
                String policyHolderIdNumber = scenarioTester.getCellValue("Policy Holder ID Number");
                String policyHolderBirthDate = scenarioTester.getCellValue("Policy Holder Birth Date");
                String policyHolderGenderCode = scenarioTester.getCellValue("Policy Holder Gender Code");
                String policyHolderGenderDesc = scenarioTester.getCellValue("Policy Holder Gender Desc");
                String policyHolderContactNumber = scenarioTester.getCellValue("Policy Holder Contact Number");
                String policyHolderIncome = scenarioTester.getCellValue("Policy Holder Income");
                String policyHolderCoverAmount = scenarioTester.getCellValue("Policy Holder Cover Amount");
                String policyHolderPremium = scenarioTester.getCellValue("Policy Holder Premium");
                String policyHolderComment = scenarioTester.getCellValue("Policy Holder Comment");
                String policyHolderEmail = scenarioTester.getCellValue("Policy Holder Email");
                String policyHolderCoverAction = scenarioTester.getCellValue("Policy Holder Cover Action");
                String policyHolderCoverId = scenarioTester.getCellValue("Policy Holder Cover ID");
                String policyHolderCoverInceptionDate = scenarioTester.getCellValue("Policy Holder Cover Inception Date");
                String policyHolderCoverWaitEndDate = scenarioTester.getCellValue("Policy Holder Cover Wait End Date");

                ResponsePolicyRoleType policyHolderRoleBefore = null;
                if (policyHolderRoleId.isEmpty()) {
                    for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                        if (roleBefore.getRoletype().equalsIgnoreCase("POLICY HOLDER")) {
                            targetRoleBefore = roleBefore;
                            policyHolderRoleId = roleBefore.getRoleid();
                            policyHolderRoleBefore = roleBefore;
                            break;
                        }
                    }
                }

                if (!policyHolderRoleId.isEmpty()) {
                    updatePolicyRoleType.setRoleid(policyHolderRoleId);
                    if (policyHolderRoleId.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setRoleid("");
                    }
                }

                updatePolicyRoleType.setAction(policyHolderAction);

                // TODO: Just Use Policy Holder Role Type
                if (!policyHolderRoleType.isEmpty()) {
                    updatePolicyRoleType.setRoletype(policyHolderRoleType);
                    if (policyHolderRoleType.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setRoletype("");
                    }
                }
                
                if (!policyHolderFullName.isEmpty()) {
                    updatePolicyRoleType.setFullname(policyHolderFullName);
                    if (policyHolderFullName.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setFullname("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String fullName = policyHolderRoleBefore.getFullname();
                        updatePolicyRoleType.setFullname(fullName);
                    }
                }
                
                if (!policyHolderMiddleName.isEmpty()) {
                    updatePolicyRoleType.setMiddlename(policyHolderMiddleName);
                    if (policyHolderMiddleName.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setMiddlename("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String middleName = policyHolderRoleBefore.getMiddlename();
                        updatePolicyRoleType.setMiddlename(middleName);
                    }
                }
                
                if (!policyHolderIdNumber.isEmpty()) {
                    updatePolicyRoleType.setIdnumber(policyHolderIdNumber);
                    if (policyHolderIdNumber.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setIdnumber("");
                    } else if (policyHolderIdNumber.equalsIgnoreCase("UPDATE")) {
                        policyHolderIdNumber = scenarioTester.generateRandomSaId(policyHolderBirthDate, policyHolderIdType, policyHolderIdNumber, policyHolderGenderCode);
                        updatePolicyRoleType.setIdnumber(policyHolderIdNumber);
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String idNumber = policyHolderRoleBefore.getIdnumber();
                        updatePolicyRoleType.setIdnumber(idNumber);
                    }
                }

                if (!policyHolderBirthDate.isEmpty()) {
                    updatePolicyRoleType.setBirthdate(policyHolderBirthDate);
                    if (policyHolderBirthDate.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setBirthdate("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String birthDate = policyHolderRoleBefore.getBirthdate();
                        updatePolicyRoleType.setBirthdate(birthDate);
                    }
                }

                if (!policyHolderGenderCode.isEmpty()) {
                    updatePolicyRoleType.setGendercode(policyHolderGenderCode);
                    if (policyHolderGenderCode.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setGendercode("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String genderCode = policyHolderRoleBefore.getGendercode();
                        updatePolicyRoleType.setGendercode(genderCode);
                    }
                }

                if (!policyHolderGenderDesc.isEmpty()) {
                    updatePolicyRoleType.setGenderdesc(policyHolderGenderDesc);
                    if (policyHolderGenderDesc.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setGenderdesc("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String genderDesc = policyHolderRoleBefore.getGenderdesc();
                        updatePolicyRoleType.setGenderdesc(genderDesc);
                    }
                }

                if (!policyHolderContactNumber.isEmpty()) {
                    updatePolicyRoleType.setContactno(policyHolderContactNumber);
                    if (policyHolderContactNumber.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setContactno("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String contactNo = policyHolderRoleBefore.getContactno();
                        updatePolicyRoleType.setContactno(contactNo);
                    }
                }

                if (!policyHolderIncome.isEmpty()) {
                    updatePolicyRoleType.setIncome(policyHolderIncome);
                    if (policyHolderIncome.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setIncome("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String income = policyHolderRoleBefore.getIncome();
                        updatePolicyRoleType.setIncome(income);
                    }
                }

                if (!policyHolderCoverAmount.isEmpty()) {
                    updatePolicyRoleType.setCoveramnt(policyHolderCoverAmount);
                    if (policyHolderCoverAmount.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setCoveramnt("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String coverAmnt = policyHolderRoleBefore.getCoveramnt();
                        updatePolicyRoleType.setCoveramnt(coverAmnt);
                    }
                }

                if (!policyHolderPremium.isEmpty()) {
                    updatePolicyRoleType.setPremium(policyHolderPremium);
                    if (policyHolderPremium.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setPremium("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        String premium = policyHolderRoleBefore.getPremium();
                        updatePolicyRoleType.setPremium(premium);
                    }
                }

                if (!policyHolderComment.isEmpty()) {
                    updatePolicyRoleType.setComment(policyHolderComment);
                    if (policyHolderComment.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setComment("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        // TODO: Find a way to get the current comment!
                    }
                }

                if (!policyHolderEmail.isEmpty()) {
                    updatePolicyRoleType.setEmail(policyHolderEmail);
                    if (policyHolderEmail.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setEmail("");
                    }
                } else {
                    if (policyHolderRoleBefore != null) {
                        // TODO: Update the email using the Other Service @Wiets suggested!
                        String email = policyHolderRoleBefore.getEmail();
                        updatePolicyRoleType.setEmail(email);
                    }
                }

                // TODO: Review This Logic
                CoverDetail coverDetail = new CoverDetail();

                ResponseCoverDetail coverDetailBefore;
                List<ResponseCover> coversBefore = null;
                if (policyHolderRoleBefore != null) {
                    coverDetailBefore = policyHolderRoleBefore.getCoverdetail();
                    if (coverDetailBefore != null) {
                        coversBefore = coverDetailBefore.getCover();
                    }
                }

                List<Cover> covers = coverDetail.getCover();

                for (ResponseCover coverBefore: coversBefore) {
                    Cover cover = new Cover();
                    if (!policyHolderCoverAction.isEmpty()) {
                        cover.setAction(policyHolderCoverAction);
                        if (policyHolderCoverAction.equalsIgnoreCase(CLEAR)) {
                            cover.setAction("");
                        }
                    } else {
                        if (coverBefore != null) {
                            // TODO: Find A Way To Get The Cover Current Action!
                        }
                    }

                    if (!policyHolderCoverId.isEmpty()) {
                        cover.setCoverid(policyHolderCoverId);
                        if (policyHolderCoverId.equalsIgnoreCase(CLEAR)) {
                            cover.setCoverid("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String coverId = coverBefore.getCoverid();
                            cover.setCoverid(coverId);
                        }
                    }

                    if (!policyHolderCoverInceptionDate.isEmpty()) {
                        cover.setInceptdate(policyHolderCoverInceptionDate);
                        if (policyHolderCoverInceptionDate.equalsIgnoreCase(CLEAR)) {
                            cover.setInceptdate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String inceptDate = coverBefore.getInceptdate();
                            cover.setInceptdate(inceptDate);
                        }
                    }

                    if (!policyHolderCoverWaitEndDate.isEmpty()) {
                        cover.setWaitenddate(policyHolderCoverWaitEndDate);
                        if (policyHolderCoverWaitEndDate.equalsIgnoreCase(CLEAR)) {
                            cover.setWaitenddate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String waitEndDate = coverBefore.getWaitenddate();
                            cover.setWaitenddate(waitEndDate);
                        }
                    }

                    // TODO: TODO: Revisit Cover Update Logic!
                    covers.add(cover);
                }

                updatePolicyRoleType.setCoverdetail(coverDetail);

                updatePolicyRoles.getRole().add(updatePolicyRoleType);
            }
        }

    }

    private void computeSpouseRole(UpdatePolicyRoles updatePolicyRoles, UpdatePolicyRoleType updatePolicyRoleType, List<ResponsePolicyRoleType> rolesBefore) throws Exception {
        String spouseRoleId = scenarioTester.getCellValue("Spouse Role ID");
        String spouseAction = scenarioTester.getCellValue("Spouse Action");

        if (!(!spouseAction.isEmpty() && spouseAction.equalsIgnoreCase("N/A"))) {
            String spouseRoleType = scenarioTester.getCellValue("Spouse Role Type");
            String spouseFullName = scenarioTester.getCellValue("Spouse Full Name");
            String spouseMiddleName = scenarioTester.getCellValue("Spouse Middle Name");
            String spouseIdNumber = scenarioTester.getCellValue("Spouse ID Number");
            String spouseBirthDate = scenarioTester.getCellValue("Spouse Birth Date");
            String spouseGenderCode = scenarioTester.getCellValue("Spouse Gender Code");
            String spouseGenderDesc = scenarioTester.getCellValue("Spouse Gender Desc");
            String spouseContactNumber = scenarioTester.getCellValue("Spouse Contact Number");
            String spouseIncome = scenarioTester.getCellValue("Spouse Income");
            String spouseCoverAmount = scenarioTester.getCellValue("Spouse Cover Amount");
            String spousePremium = scenarioTester.getCellValue("Spouse Premium");
            String spouseComment = scenarioTester.getCellValue("Spouse Comment");
            String spouseEmail = scenarioTester.getCellValue("Spouse Email");
            String spouseCoverAction = scenarioTester.getCellValue("Spouse Cover Action");
            String spouseCoverId = scenarioTester.getCellValue("Spouse Cover ID");
            String spouseCoverInceptionDate = scenarioTester.getCellValue("Spouse Cover Inception Date");
            String spouseCoverWaitEndDate = scenarioTester.getCellValue("Spouse Cover Wait End Date");

            if (spouseRoleId.isEmpty()) {
                for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                    if (roleBefore.getRoletype().equalsIgnoreCase("SPOUSE")) {
                        spouseRoleId = roleBefore.getRoleid();
                        break;
                    }
                }
            }

            updatePolicyRoleType.setRoleid(spouseRoleId);
            updatePolicyRoleType.setAction(spouseAction);
            updatePolicyRoleType.setRoletype(spouseRoleType);
            updatePolicyRoleType.setFullname(spouseFullName);
            updatePolicyRoleType.setMiddlename(spouseMiddleName);
            updatePolicyRoleType.setIdnumber(spouseIdNumber);
            updatePolicyRoleType.setBirthdate(spouseBirthDate);
            updatePolicyRoleType.setGendercode(spouseGenderCode);
            updatePolicyRoleType.setGenderdesc(spouseGenderDesc);
            updatePolicyRoleType.setContactno(spouseContactNumber);
            updatePolicyRoleType.setIncome(spouseIncome);
            updatePolicyRoleType.setCoveramnt(spouseCoverAmount);
            updatePolicyRoleType.setPremium(spousePremium);
            updatePolicyRoleType.setComment(spouseComment);
            updatePolicyRoleType.setEmail(spouseEmail);

            CoverDetail coverDetail = new CoverDetail();
            Cover cover = new Cover();
            cover.setAction(spouseCoverAction);
            cover.setCoverid(spouseCoverId);
            cover.setInceptdate(spouseCoverInceptionDate);
            cover.setWaitenddate(spouseCoverWaitEndDate);

            List<Cover> covers = coverDetail.getCover();
            covers.add(cover);

            updatePolicyRoleType.setCoverdetail(coverDetail);
            updatePolicyRoles.getRole().add(updatePolicyRoleType);
        }

    }

    private void editSpouseRole(UpdatePolicyRoles updatePolicyRoles, UpdatePolicyRoleType updatePolicyRoleType, List<ResponsePolicyRoleType> rolesBefore) throws Exception {
        String spouseRoleId = scenarioTester.getCellValue("Spouse Role ID");
        String spouseAction = scenarioTester.getCellValue("Spouse Action");

        if (!(!spouseAction.isEmpty() && spouseAction.equalsIgnoreCase("N/A"))) {
            if (spouseAction.equalsIgnoreCase("CH")) {
                String spouseRoleType = scenarioTester.getCellValue("Spouse Role Type");
                String spouseFullName = scenarioTester.getCellValue("Spouse Full Name");
                String spouseMiddleName = scenarioTester.getCellValue("Spouse Middle Name");
                String spouseIdNumber = scenarioTester.getCellValue("Spouse ID Number");
                String spouseBirthDate = scenarioTester.getCellValue("Spouse Birth Date");
                String spouseGenderCode = scenarioTester.getCellValue("Spouse Gender Code");
                String spouseGenderDesc = scenarioTester.getCellValue("Spouse Gender Desc");
                String spouseContactNumber = scenarioTester.getCellValue("Spouse Contact Number");
                String spouseIncome = scenarioTester.getCellValue("Spouse Income");
                String spouseCoverAmount = scenarioTester.getCellValue("Spouse Cover Amount");
                String spousePremium = scenarioTester.getCellValue("Spouse Premium");
                String spouseComment = scenarioTester.getCellValue("Spouse Comment");
                String spouseEmail = scenarioTester.getCellValue("Spouse Email");
                String spouseCoverAction = scenarioTester.getCellValue("Spouse Cover Action");
                String spouseCoverId = scenarioTester.getCellValue("Spouse Cover ID");
                String spouseCoverInceptionDate = scenarioTester.getCellValue("Spouse Cover Inception Date");
                String spouseCoverWaitEndDate = scenarioTester.getCellValue("Spouse Cover Wait End Date");

                ResponsePolicyRoleType spouseBefore = null;
                if (spouseRoleId.isEmpty()) {
                    for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                        if (roleBefore.getRoletype().equalsIgnoreCase("SPOUSE")) {
                            targetRoleBefore = roleBefore;
                            spouseRoleId = roleBefore.getRoleid();
                            spouseBefore = roleBefore;
                            break;
                        }
                    }
                }

                if (!spouseRoleId.isEmpty()) {
                    updatePolicyRoleType.setRoleid(spouseRoleId);
                    if (spouseRoleId.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setRoleid("");
                    }
                }

                updatePolicyRoleType.setAction(spouseAction);

                if (!spouseRoleType.isEmpty()) {
                    updatePolicyRoleType.setRoletype(spouseRoleType);
                    if (spouseRoleType.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setRoletype("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String roleType = spouseBefore.getRoletype();
                        updatePolicyRoleType.setRoletype(roleType);
                    }
                }

                if (!spouseFullName.isEmpty()) {
                    updatePolicyRoleType.setFullname(spouseFullName);
                    if (spouseFullName.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setFullname("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String fullName = spouseBefore.getFullname();
                        updatePolicyRoleType.setFullname(fullName);
                    }
                }

                if (!spouseMiddleName.isEmpty()) {
                    updatePolicyRoleType.setMiddlename(spouseMiddleName);
                    if (spouseMiddleName.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setMiddlename("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String middleName = spouseBefore.getMiddlename();
                        updatePolicyRoleType.setMiddlename(middleName);
                    }
                }

                if (!spouseIdNumber.isEmpty()) {
                    updatePolicyRoleType.setIdnumber(spouseIdNumber);
                    if (spouseIdNumber.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setIdnumber("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String idNumber = spouseBefore.getIdnumber();
                        updatePolicyRoleType.setIdnumber(idNumber);
                    }
                }

                if (!spouseBirthDate.isEmpty()) {
                    updatePolicyRoleType.setBirthdate(spouseBirthDate);
                    if (spouseBirthDate.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setBirthdate("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String birthDate = spouseBefore.getBirthdate();
                        updatePolicyRoleType.setBirthdate(birthDate);
                    }
                }

                if (!spouseGenderCode.isEmpty()) {
                    updatePolicyRoleType.setGendercode(spouseGenderCode);
                    if (spouseGenderCode.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setGendercode("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String genderCode = spouseBefore.getGendercode();
                        updatePolicyRoleType.setGendercode(genderCode);
                    }
                }

                if (!spouseGenderDesc.isEmpty()) {
                    updatePolicyRoleType.setGenderdesc(spouseGenderDesc);
                    if (spouseGenderDesc.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setGenderdesc("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String genderDesc = spouseBefore.getGenderdesc();
                        updatePolicyRoleType.setGenderdesc(genderDesc);
                    }
                }

                if (!spouseContactNumber.isEmpty()) {
                    updatePolicyRoleType.setContactno(spouseContactNumber);
                    if (spouseContactNumber.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setContactno("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String contactNo = spouseBefore.getContactno();
                        updatePolicyRoleType.setContactno(contactNo);
                    }
                }

                if (!spouseIncome.isEmpty()) {
                    updatePolicyRoleType.setIncome(spouseIncome);
                    if (spouseIncome.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setIncome("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String income = spouseBefore.getIncome();
                        updatePolicyRoleType.setIncome(income);
                    }
                }

                if (!spouseCoverAmount.isEmpty()) {
                    updatePolicyRoleType.setCoveramnt(spouseCoverAmount);
                    if (spouseCoverAmount.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setCoveramnt("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String coverAmnt = spouseBefore.getCoveramnt();
                        updatePolicyRoleType.setCoveramnt(coverAmnt);
                    }
                }

                if (!spousePremium.isEmpty()) {
                    updatePolicyRoleType.setPremium(spousePremium);
                    if (spousePremium.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setPremium("");
                    }
                } else {
                    if (spouseBefore != null) {
                        String premium = spouseBefore.getPremium();
                        updatePolicyRoleType.setPremium(premium);
                    }
                }

                if (!spouseComment.isEmpty()) {
                    updatePolicyRoleType.setComment(spouseComment);
                    if (spouseComment.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setComment("");
                    }
                } else {
                    if (spouseBefore != null) {
                        // TODO: Find A Way To Get The Current Comment.
                    }
                }

                if (!spouseEmail.isEmpty()) {
                    updatePolicyRoleType.setEmail(spouseEmail);
                    if (spouseEmail.equalsIgnoreCase(CLEAR)) {
                        updatePolicyRoleType.setEmail("");
                    }
                } else {
                    // TODO: Get The Email From The Other Service As Suggested By @Wiets.
                    if (spouseBefore != null) {
                        String email = spouseBefore.getEmail();
                        updatePolicyRoleType.setEmail(email);
                    }
                }

                // TODO: Get Company Registration Number.

                // TODO: Review This Logic
                CoverDetail coverDetail = new CoverDetail();

                ResponseCoverDetail coverDetailBefore;
                List<ResponseCover> beforeCovers = null;
                if (spouseBefore != null) {
                    coverDetailBefore = spouseBefore.getCoverdetail();
                    if (coverDetailBefore != null) {
                        beforeCovers = coverDetailBefore.getCover();
                    }
                }

                List<Cover> covers = coverDetail.getCover();
                if (beforeCovers != null) {
                    for (ResponseCover beforeCover: beforeCovers) {
                        Cover cover = new Cover();
                        if (!spouseCoverAction.isEmpty()) {
                            cover.setAction(spouseCoverAction);
                            if (spouseCoverAction.equalsIgnoreCase(CLEAR)) {
                                cover.setAction("");
                            }
                        } else {
                            if (beforeCover != null) {
                                // TODO: Find A Way To Get Current Action!
                            }
                        }

                        if (!spouseCoverId.isEmpty()) {
                            cover.setCoverid(spouseCoverId);
                            if (spouseCoverId.equalsIgnoreCase(CLEAR)) {
                                cover.setCoverid("");
                            }
                        } else {
                            if (beforeCover != null) {
                                String coverId = beforeCover.getCoverid();
                                cover.setCoverid(coverId);
                            }
                        }

                        if (!spouseCoverInceptionDate.isEmpty()) {
                            cover.setInceptdate(spouseCoverInceptionDate);
                            if (spouseCoverInceptionDate.equalsIgnoreCase(CLEAR)) {
                                cover.setInceptdate("");
                            }
                        } else {
                            if (beforeCover != null) {
                                String inceptDate = beforeCover.getInceptdate();
                                cover.setInceptdate(inceptDate);
                            }
                        }

                        if (!spouseCoverWaitEndDate.isEmpty()) {
                            cover.setWaitenddate(spouseCoverWaitEndDate);
                            if (spouseCoverWaitEndDate.equalsIgnoreCase(CLEAR)) {
                                cover.setWaitenddate("");
                            }
                        } else {
                            if (beforeCover != null) {
                                String waitEndDate = beforeCover.getWaitenddate();
                                cover.setWaitenddate(waitEndDate);
                            }
                        }

                        // TODO: Revist Cover Logic

                        covers.add(cover);
                    }
                }

                updatePolicyRoleType.setCoverdetail(coverDetail);

                updatePolicyRoles.getRole().add(updatePolicyRoleType);
            }
        }

    }

    private void getMemberVariableFields(String[] roleValuesList, List<MemberVariableFields> memberVariableFields) {
        for (String roleData : roleValuesList) {
            if (!roleData.isEmpty()) {
                try {
                    String trim = roleData.replaceAll("\\[", "")
                            .replaceAll("\\]", "")
                            .trim();
                    String[] roleValues = trim.split(",");
                    memberVariableFields.add(
                            new MemberVariableFields(roleValues[0], roleValues[1], roleValues[2], roleValues[3], roleValues[4], roleValues[5])
                    );
                } catch (Exception e) {
                    // TODO: Handle Whatever Exception
                }
            }
        }
    }

    private void getRoleActions(String[] roleActionsList, Multimap<String, String> roleActions) {
        for (String roleAction : roleActionsList) {
            if (!roleAction.isEmpty()) {
                try {
                    String trim = roleAction.replaceAll("\\[", "")
                            .replaceAll("\\]", "")
                            .replaceAll(" ", "");

                    String[] roleIdAction = trim.split(",");
                    String roleId = roleIdAction[6];
                    String action = roleIdAction[7];
                    roleActions.put(roleId, action);
                } catch (Exception e) {
                    // TODO: Handle Whatever Exception
                }
            }
        }
    }

    private Multimap<String, String> getExistingChildRoleIds(List<ResponsePolicyRoleType> rolesBefore, Multimap<String, String> childActions) {
        Multimap<String, String> tempChildActions = ArrayListMultimap.create();
        List<Integer> childRoleIds = new ArrayList<>();

        Integer currentRoleId;
        for (ResponsePolicyRoleType roleBefore : rolesBefore) {
            if (roleBefore.getRoletype().equalsIgnoreCase("CHILD")
                    && roleBefore.getStatus().equalsIgnoreCase("ACTIVE")
            ) {
                currentRoleId = Integer.valueOf(roleBefore.getRoleid());
                childRoleIds.add(currentRoleId);
            }
        }
        Collections.sort(childRoleIds);

        for (Map.Entry<String, String> childActionEntry: childActions.entries()) {
            if (childActionEntry.getKey().equals("-1")) {
                for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                    if (roleBefore.getRoletype().equalsIgnoreCase("CHILD")) {
                        String action = childActionEntry.getValue();
                        boolean roleIdExists = tempChildActions.keys().stream().anyMatch(key -> key.contains(roleBefore.getRoleid()));
                        if (!roleIdExists) {
                            if (action.contains("{")) {
                                String[] actionAndPremium = action.split("\\{");
                                action = actionAndPremium[0];
                                String targetRoleIndex = actionAndPremium[1].replace("}", "");

                                try {
                                    Integer targetRoleIndexValue = Integer.valueOf(targetRoleIndex) - 1;
                                    Integer integerRoleId = childRoleIds.get(targetRoleIndexValue);
                                    String stringRoleId = String.valueOf(integerRoleId);
                                    tempChildActions.put(stringRoleId, action);
                                } catch (Exception e) {
                                    log.error("No Child Role On Index " + targetRoleIndex);
                                    // TODO: Handle Error
                                }
                                break;
                            } else {
                                tempChildActions.put(roleBefore.getRoleid(), action);
                                break;
                            }
                        }
                    }
                }
            } else {
                tempChildActions.put(childActionEntry.getKey(), childActionEntry.getValue());
            }
        }
        childActions = tempChildActions;
        return childActions;
    }

    private void computeChildRole(UpdatePolicyRoles updatePolicyRoles, MemberVariableFields memberVariableFields, UpdatePolicyRoleType updatePolicyRoleType, String roleId, String action) throws Exception {
        String childRoleType = memberVariableFields.getRelationship(); // getCellValue("Child Role Type");
        String childFullName = memberVariableFields.getName(); // getCellValue("Child Full Name");
        String childMiddleName = scenarioTester.getCellValue("Child Middle Name");
        String childIdNumber = scenarioTester.getCellValue("Child ID Number");
        String childBirthDate = memberVariableFields.getBirthDate(); // getCellValue("Child Birth Date");
        String childGenderCode = memberVariableFields.getGenderCode(); // getCellValue("Child Gender Code");

        String childGenderDesc = "";
        if (!childGenderCode.isEmpty()) {
            if (childGenderCode.equalsIgnoreCase("M")) {
                childGenderDesc = "MALE"; // getCellValue("Child Gender Desc");
            } else {
                childGenderDesc = "FEMALE";
            }
        }

        String childContactNumber = scenarioTester.getCellValue("Child Contact Number");
        String childIncome = scenarioTester.getCellValue("Child Income");
        String childCoverAmount = memberVariableFields.getCoverAmount(); // getCellValue("Child Cover Amount");
        String childPremium = memberVariableFields.getPremium(); // getCellValue("Child Premium");
        String childComment = scenarioTester.getCellValue("Child Comment");
        String childEmail = scenarioTester.getCellValue("Child Email");
        String childCoverAction = scenarioTester.getCellValue("Child Cover Action");
        String childCoverId = scenarioTester.getCellValue("Child Cover ID");
        String childCoverInceptionDate = scenarioTester.getCellValue("Child Cover Inception Date");
        String childCoverWaitEndDate = scenarioTester.getCellValue("Child Cover Wait End Date");

        updatePolicyRoleType.setRoleid(roleId);
        updatePolicyRoleType.setAction(action);
        updatePolicyRoleType.setRoletype(childRoleType);
        updatePolicyRoleType.setFullname(childFullName);
        updatePolicyRoleType.setMiddlename(childMiddleName);
        updatePolicyRoleType.setIdnumber(childIdNumber);
        updatePolicyRoleType.setBirthdate(childBirthDate);
        updatePolicyRoleType.setGendercode(childGenderCode);
        updatePolicyRoleType.setGenderdesc(childGenderDesc);
        updatePolicyRoleType.setContactno(childContactNumber);
        updatePolicyRoleType.setIncome(childIncome);
        updatePolicyRoleType.setCoveramnt(childCoverAmount);
        updatePolicyRoleType.setPremium(childPremium);
        updatePolicyRoleType.setComment(childComment);
        updatePolicyRoleType.setEmail(childEmail);

        CoverDetail coverDetail = new CoverDetail();
        Cover cover = new Cover();
        cover.setAction(childCoverAction);
        cover.setCoverid(childCoverId);
        cover.setInceptdate(childCoverInceptionDate);
        cover.setWaitenddate(childCoverWaitEndDate);

        List<Cover> covers = coverDetail.getCover();
        covers.add(cover);

        updatePolicyRoleType.setCoverdetail(coverDetail);
        updatePolicyRoles.getRole().add(updatePolicyRoleType);
    }

    private void editChildRole(UpdatePolicyRoles updatePolicyRoles, MemberVariableFields memberVariableFields, UpdatePolicyRoleType updatePolicyRoleType, ResponsePolicyRoleType targetRole, String action) throws Exception {
        String childRoleType = memberVariableFields.getRelationship(); // getCellValue("Child Role Type");
        String childFullName = memberVariableFields.getName(); // getCellValue("Child Full Name");
        String childMiddleName = scenarioTester.getCellValue("Child Middle Name");
        String childIdNumber = scenarioTester.getCellValue("Child ID Number");
        String childBirthDate = memberVariableFields.getBirthDate(); // getCellValue("Child Birth Date");
        String childGenderCode = memberVariableFields.getGenderCode(); // getCellValue("Child Gender Code");

        String childGenderDesc = "";
        if (!childGenderCode.isEmpty()) {
            if (childGenderCode.equalsIgnoreCase("M")) {
                childGenderDesc = "MALE"; // getCellValue("Child Gender Desc");
            } else {
                childGenderDesc = "FEMALE";
            }
        }

        String childContactNumber = scenarioTester.getCellValue("Child Contact Number");
        String childIncome = scenarioTester.getCellValue("Child Income");
        String childCoverAmount = memberVariableFields.getCoverAmount(); // getCellValue("Child Cover Amount");
        String childPremium = memberVariableFields.getPremium(); // getCellValue("Child Premium");
        String childComment = scenarioTester.getCellValue("Child Comment");
        String childEmail = scenarioTester.getCellValue("Child Email");
        String childCoverAction = scenarioTester.getCellValue("Child Cover Action");
        String childCoverId = scenarioTester.getCellValue("Child Cover ID");
        String childCoverInceptionDate = scenarioTester.getCellValue("Child Cover Inception Date");
        String childCoverWaitEndDate = scenarioTester.getCellValue("Child Cover Wait End Date");

        String roleId = null;
        if (targetRole != null) {
            roleId = targetRole.getRoleid();
        }

        if (roleId != null) {
            if (!roleId.isEmpty()) {
                updatePolicyRoleType.setRoleid(roleId);
                if (roleId.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setRoleid("");
                }
            }

            if (!action.isEmpty()) {
                updatePolicyRoleType.setAction(action);
                if (action.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setAction(action);
                }
            }

            if (!childRoleType.isEmpty()) {
                updatePolicyRoleType.setRoletype(childRoleType);
                if (childRoleType.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setRoletype(childRoleType);
                }
            } else {
                String roleType = targetRole.getRoletype();
                updatePolicyRoleType.setRoletype(roleType);
            }

            if (!childFullName.isEmpty()) {
                updatePolicyRoleType.setFullname(childFullName);
                if (childFullName.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setFullname(childFullName);
                }
            } else {
                String fullName = targetRole.getFullname();
                updatePolicyRoleType.setFullname(fullName);
            }

            if (!childMiddleName.isEmpty()) {
                updatePolicyRoleType.setMiddlename(childMiddleName);
                if (childMiddleName.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setMiddlename("");
                }
            } else {
                String middleName = targetRole.getMiddlename();
                updatePolicyRoleType.setMiddlename(middleName);
            }

            if (!childIdNumber.isEmpty()) {
                updatePolicyRoleType.setIdnumber(childIdNumber);
                if (childIdNumber.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setIdnumber("");
                }
            } else {
                String idNumber = targetRole.getIdnumber();
                updatePolicyRoleType.setIdnumber(idNumber);
            }

            if (!childBirthDate.isEmpty()) {
                updatePolicyRoleType.setBirthdate(childBirthDate);
                if (childBirthDate.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setBirthdate("");
                }
            } else {
                String birthDate = targetRole.getBirthdate();
                updatePolicyRoleType.setBirthdate(birthDate);
            }

            if (!childGenderCode.isEmpty()) {
                updatePolicyRoleType.setGendercode(childGenderCode);
                if (childGenderCode.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setGendercode("");
                }
            } else {
                String genderCode = targetRole.getGendercode();
                updatePolicyRoleType.setGendercode(genderCode);
            }

            if (!childGenderDesc.isEmpty()) {
                updatePolicyRoleType.setGenderdesc(childGenderDesc);
            } // TODO: Find Out If We'll Ever Need To Clear This.

            if (!childContactNumber.isEmpty()) {
                updatePolicyRoleType.setContactno(childContactNumber);
                if (childContactNumber.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setContactno(childContactNumber);
                }
            } else {
                String contactNo = targetRole.getContactno();
                updatePolicyRoleType.setContactno(contactNo);
            }

            if (!childIncome.isEmpty()) {
                updatePolicyRoleType.setIncome(childIncome);
                if (childIncome.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setIncome("");
                }
            } else {
                String income = targetRole.getIncome();
                updatePolicyRoleType.setIncome(income);
            }

            if (!childCoverAmount.isEmpty()) {
                updatePolicyRoleType.setCoveramnt(childCoverAmount);
                if (childCoverAmount.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setCoveramnt("");
                }
            } else {
                String coverAmnt = targetRole.getCoveramnt();
                updatePolicyRoleType.setCoveramnt(coverAmnt);
            }

            if (!childPremium.isEmpty()) {
                updatePolicyRoleType.setPremium(childPremium);
                if (childPremium.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setPremium("");
                }
            } else {
                String premium = targetRole.getPremium();
                updatePolicyRoleType.setPremium(premium);
            }

            if (!childComment.isEmpty()) {
                updatePolicyRoleType.setComment(childComment);
                if (childComment.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setComment("");
                }
            } else {
                // TODO: Find A Way To Get The Current Comment.
            }

            if (!childEmail.isEmpty()) {
                updatePolicyRoleType.setEmail(childEmail);
                if (childEmail.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setEmail("");
                }
            } else {
                // TODO: Get The Email From The Other Service As Suggested By @Wiets.
                String email = targetRole.getEmail();
                updatePolicyRoleType.setEmail(email);
            }

            // TODO: Get Company Registration Number.

            // TODO: Review This Logic
            CoverDetail coverDetail = new CoverDetail();

            ResponseCoverDetail coverDetailBefore = targetRole.getCoverdetail();

            List<ResponseCover> coversBefore = null;
            if (coverDetailBefore != null) {
                coversBefore = coverDetailBefore.getCover();
            }

            List<Cover> covers = coverDetail.getCover();
            if (coversBefore != null) {
                for (ResponseCover coverBefore: coversBefore) {
                    Cover cover = new Cover();
                    if (!childCoverAction.isEmpty()) {
                        cover.setAction(childCoverAction);
                        if (childCoverAction.equalsIgnoreCase(CLEAR)) {
                            cover.setAction("");
                        }
                    } else {
                        // TODO: Find A Way To Get Cover Action!
                    }

                    if (!childCoverId.isEmpty()) {
                        cover.setCoverid(childCoverId);
                        if (childCoverId.equalsIgnoreCase(CLEAR)) {
                            cover.setCoverid("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String coverId = coverBefore.getCoverid();
                            cover.setCoverid(coverId);
                        }
                    }

                    if (!childCoverInceptionDate.isEmpty()) {
                        cover.setInceptdate(childCoverInceptionDate);
                        if (childCoverInceptionDate.equalsIgnoreCase(CLEAR)) {
                            cover.setInceptdate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String inceptDate = coverBefore.getInceptdate();
                            cover.setInceptdate(inceptDate);
                        }
                    }

                    if (!childCoverWaitEndDate.isEmpty()) {
                        cover.setWaitenddate(childCoverWaitEndDate);
                        if (childCoverWaitEndDate.equalsIgnoreCase(CLEAR)) {
                            cover.setWaitenddate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String waitEndDate = coverBefore.getWaitenddate();
                            cover.setWaitenddate(waitEndDate);
                        }
                    }

                    covers.add(cover);
                }
            }

            updatePolicyRoleType.setCoverdetail(coverDetail);

            updatePolicyRoles.getRole().add(updatePolicyRoleType);
        }

    }

    private Multimap<String, String> getExistingParentOrExtendedFamilyRoleIds(List<ResponsePolicyRoleType> rolesBefore, Multimap<String, String> parentExtendedFamilyActions) {
        Multimap<String, String> tempParentExtendedFamilyActions = ArrayListMultimap.create();
        List<Integer> parentExtendedFamilyRoleIds = new ArrayList<>();

        Integer currentRoleId;
        for (ResponsePolicyRoleType roleBefore : rolesBefore) {
            if ((roleBefore.getRoletype().equalsIgnoreCase("PARENT")
                    || roleBefore.getRoletype().equalsIgnoreCase("EXTENDED FAMILY"))
                    && roleBefore.getStatus().equalsIgnoreCase("ACTIVE")
            ) {
                currentRoleId = Integer.valueOf(roleBefore.getRoleid());
                parentExtendedFamilyRoleIds.add(currentRoleId);
            }
        }
        Collections.sort(parentExtendedFamilyRoleIds);


        for (Map.Entry<String, String> parentExtendedFamilyActionEntry: parentExtendedFamilyActions.entries()) {
            if (parentExtendedFamilyActionEntry.getKey().equals("-1")) {
                for (ResponsePolicyRoleType roleBefore : rolesBefore) {
                    if (roleBefore.getRoletype().equalsIgnoreCase("PARENT")
                            || roleBefore.getRoletype().equalsIgnoreCase("EXTENDED FAMILY")
                    ) {
                        String action = parentExtendedFamilyActionEntry.getValue();
                        boolean roleIdExists = tempParentExtendedFamilyActions.keys().stream().anyMatch(key -> key.contains(roleBefore.getRoleid()));
                        if (!roleIdExists) {
                            if (action.contains("{")) {
                                String[] actionAndPremium = action.split("\\{");
                                action = actionAndPremium[0];
                                String targetRoleIndex = actionAndPremium[1].replace("}", "");

                                try {
                                    Integer targetRoleIndexValue = Integer.valueOf(targetRoleIndex) - 1;
                                    Integer integerRoleId = parentExtendedFamilyRoleIds.get(targetRoleIndexValue);
                                    String stringRoleId = String.valueOf(integerRoleId);
                                    tempParentExtendedFamilyActions.put(stringRoleId, action);
                                } catch (Exception e) {
                                    log.error("No Parent or Extended Family Role On Index " + targetRoleIndex);
                                    // TODO: handle Error
                                }
                                break;
                            } else {
                                tempParentExtendedFamilyActions.put(roleBefore.getRoleid(), action);
                                break;
                            }
                        }
                    }
                }
            } else {
                tempParentExtendedFamilyActions.put(parentExtendedFamilyActionEntry.getKey(), parentExtendedFamilyActionEntry.getValue());
            }
        }
        parentExtendedFamilyActions = tempParentExtendedFamilyActions;
        return parentExtendedFamilyActions;
    }

    private void computeParentOrExtendedFamilyRole(UpdatePolicyRoles updatePolicyRoles, MemberVariableFields memberVariableFields, UpdatePolicyRoleType updatePolicyRoleType, String roleId, String action) throws Exception {
        String parentOrExtendedFamilyRoleType = memberVariableFields.getRelationship(); // getCellValue("Parent/Extended Family Role Type");
        String parentOrExtendedFamilyFullName = memberVariableFields.getName(); // getCellValue("Parent/Extended Family Full Name");
        String parentOrExtendedFamilyMiddleName = scenarioTester.getCellValue("Parent/Extended Family Middle Name");
        String parentOrExtendedFamilyIdNumber = scenarioTester.getCellValue("Parent/Extended Family ID Number");
        String parentOrExtendedFamilyBirthDate = memberVariableFields.getBirthDate(); // getCellValue("Parent/Extended Family Birth Date");
        String parentOrExtendedFamilyGenderCode = memberVariableFields.getGenderCode(); // getCellValue("Parent/Extended Family Gender Code");

        String parentOrExtendedFamilyGenderDesc = "";
        if (!parentOrExtendedFamilyGenderCode.isEmpty()) {
            if (parentOrExtendedFamilyGenderCode.equalsIgnoreCase("M")) {
                parentOrExtendedFamilyGenderDesc = "MALE"; // getCellValue("Parent/Extended Family Gender Desc");
            } else {
                parentOrExtendedFamilyGenderDesc = "FEMALE";
            }
        }

        String parentOrExtendedFamilyContactNumber = scenarioTester.getCellValue("Parent/Extended Family Contact Number");
        String parentOrExtendedFamilyIncome = scenarioTester.getCellValue("Parent/Extended Family Income");
        String parentOrExtendedFamilyCoverAmount = memberVariableFields.getCoverAmount(); // getCellValue("Parent/Extended Family Cover Amount");
        String parentOrExtendedFamilyPremium = memberVariableFields.getPremium(); // getCellValue("Parent/Extended Family Premium");
        String parentOrExtendedFamilyComment = scenarioTester.getCellValue("Parent/Extended Family Comment");
        String parentOrExtendedFamilyEmail = scenarioTester.getCellValue("Parent/Extended Family Email");
        String parentOrExtendedFamilyCoverAction = scenarioTester.getCellValue("Parent/Extended Family Cover Action");
        String parentOrExtendedFamilyCoverId = scenarioTester.getCellValue("Parent/Extended Family Cover ID");
        String parentOrExtendedFamilyCoverInceptionDate = scenarioTester.getCellValue("Parent/Extended Family Cover Inception Date");
        String parentOrExtendedFamilyCoverWaitEndDate = scenarioTester.getCellValue("Parent/Extended Family Cover Wait End Date");

        updatePolicyRoleType.setRoleid(roleId);
        updatePolicyRoleType.setAction(action);
        updatePolicyRoleType.setRoletype(parentOrExtendedFamilyRoleType);
        updatePolicyRoleType.setFullname(parentOrExtendedFamilyFullName);
        updatePolicyRoleType.setMiddlename(parentOrExtendedFamilyMiddleName);
        updatePolicyRoleType.setIdnumber(parentOrExtendedFamilyIdNumber);
        updatePolicyRoleType.setBirthdate(parentOrExtendedFamilyBirthDate);
        updatePolicyRoleType.setGendercode(parentOrExtendedFamilyGenderCode);
        updatePolicyRoleType.setGenderdesc(parentOrExtendedFamilyGenderDesc);
        updatePolicyRoleType.setContactno(parentOrExtendedFamilyContactNumber);
        updatePolicyRoleType.setIncome(parentOrExtendedFamilyIncome);
        updatePolicyRoleType.setCoveramnt(parentOrExtendedFamilyCoverAmount);
        updatePolicyRoleType.setPremium(parentOrExtendedFamilyPremium);
        updatePolicyRoleType.setComment(parentOrExtendedFamilyComment);
        updatePolicyRoleType.setEmail(parentOrExtendedFamilyEmail);

        CoverDetail coverDetail = new CoverDetail();
        Cover cover = new Cover();
        cover.setAction(parentOrExtendedFamilyCoverAction);
        cover.setCoverid(parentOrExtendedFamilyCoverId);
        cover.setInceptdate(parentOrExtendedFamilyCoverInceptionDate);
        cover.setWaitenddate(parentOrExtendedFamilyCoverWaitEndDate);

        List<Cover> covers = coverDetail.getCover();
        covers.add(cover);

        updatePolicyRoleType.setCoverdetail(coverDetail);
        updatePolicyRoles.getRole().add(updatePolicyRoleType);
    }

    private void editParentOrExtendedFamilyRole(UpdatePolicyRoles updatePolicyRoles, MemberVariableFields memberVariableFields, UpdatePolicyRoleType updatePolicyRoleType, ResponsePolicyRoleType targetRole, String action) throws Exception {
        String parentOrExtendedFamilyRoleType = memberVariableFields.getRelationship(); // getCellValue("Parent/Extended Family Role Type");
        String parentOrExtendedFamilyFullName = memberVariableFields.getName(); // getCellValue("Parent/Extended Family Full Name");
        String parentOrExtendedFamilyMiddleName = scenarioTester.getCellValue("Parent/Extended Family Middle Name");
        String parentOrExtendedFamilyIdNumber = scenarioTester.getCellValue("Parent/Extended Family ID Number");
        String parentOrExtendedFamilyBirthDate = memberVariableFields.getBirthDate(); // getCellValue("Parent/Extended Family Birth Date");
        String parentOrExtendedFamilyGenderCode = memberVariableFields.getGenderCode(); // getCellValue("Parent/Extended Family Gender Code");

        String parentOrExtendedFamilyGenderDesc = "";
        if (!parentOrExtendedFamilyGenderCode.isEmpty()) {
            if (parentOrExtendedFamilyGenderCode.equalsIgnoreCase("M")) {
                parentOrExtendedFamilyGenderDesc = "MALE"; // getCellValue("Parent/Extended Family Gender Desc");
            } else {
                parentOrExtendedFamilyGenderDesc = "FEMALE";
            }
        }

        String parentOrExtendedFamilyContactNumber = scenarioTester.getCellValue("Parent/Extended Family Contact Number");
        String parentOrExtendedFamilyIncome = scenarioTester.getCellValue("Parent/Extended Family Income");
        String parentOrExtendedFamilyCoverAmount = memberVariableFields.getCoverAmount(); // getCellValue("Parent/Extended Family Cover Amount");
        String parentOrExtendedFamilyPremium = memberVariableFields.getPremium(); // getCellValue("Parent/Extended Family Premium");
        String parentOrExtendedFamilyComment = scenarioTester.getCellValue("Parent/Extended Family Comment");
        String parentOrExtendedFamilyEmail = scenarioTester.getCellValue("Parent/Extended Family Email");
        String parentOrExtendedFamilyCoverAction = scenarioTester.getCellValue("Parent/Extended Family Cover Action");
        String parentOrExtendedFamilyCoverId = scenarioTester.getCellValue("Parent/Extended Family Cover ID");
        String parentOrExtendedFamilyCoverInceptionDate = scenarioTester.getCellValue("Parent/Extended Family Cover Inception Date");
        String parentOrExtendedFamilyCoverWaitEndDate = scenarioTester.getCellValue("Parent/Extended Family Cover Wait End Date");

        String roleId = null;
        if (targetRole != null) {
            roleId = targetRole.getRoleid();
        }

        if (roleId != null) {
            if (!roleId.isEmpty()) {
                updatePolicyRoleType.setRoleid(roleId);
                if (roleId.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setRoleid("");
                }
            }

            if (!action.isEmpty()) {
                updatePolicyRoleType.setAction(action);
                if (action.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setAction("");
                }
            }

            if (!parentOrExtendedFamilyRoleType.isEmpty()) {
                updatePolicyRoleType.setRoletype(parentOrExtendedFamilyRoleType);
                if (parentOrExtendedFamilyRoleType.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setRoletype("");
                }
            } else {
                String roleType = targetRole.getRoletype();
                updatePolicyRoleType.setRoletype(roleType);
            }

            if (!parentOrExtendedFamilyFullName.isEmpty()) {
                updatePolicyRoleType.setFullname(parentOrExtendedFamilyFullName);
                if (parentOrExtendedFamilyFullName.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setFullname("");
                }
            } else {
                String fullName = targetRole.getFullname();
                updatePolicyRoleType.setFullname(fullName);
            }

            if (!parentOrExtendedFamilyMiddleName.isEmpty()) {
                updatePolicyRoleType.setMiddlename(parentOrExtendedFamilyMiddleName);
                if (parentOrExtendedFamilyMiddleName.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setMiddlename("");
                }
            } else {
                String middleName = targetRole.getMiddlename();
                updatePolicyRoleType.setMiddlename(middleName);
            }

            if (!parentOrExtendedFamilyIdNumber.isEmpty()) {
                updatePolicyRoleType.setIdnumber(parentOrExtendedFamilyIdNumber);
                if (parentOrExtendedFamilyIdNumber.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setIdnumber("");
                }
            } else {
                String idNumber = targetRole.getIdnumber();
                updatePolicyRoleType.setIdnumber(idNumber);
            }

            if (!parentOrExtendedFamilyBirthDate.isEmpty()) {
                updatePolicyRoleType.setBirthdate(parentOrExtendedFamilyBirthDate);
                if (parentOrExtendedFamilyBirthDate.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setBirthdate("");
                }
            } else {
                String birthDate = targetRole.getBirthdate();
                updatePolicyRoleType.setBirthdate(birthDate);
            }

            if (!parentOrExtendedFamilyGenderCode.isEmpty()) {
                updatePolicyRoleType.setGendercode(parentOrExtendedFamilyGenderCode);
                if (parentOrExtendedFamilyGenderCode.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setGendercode("");
                }
            } else {
                String genderCode = targetRole.getGendercode();
                updatePolicyRoleType.setGendercode(genderCode);
            }

            if (!parentOrExtendedFamilyGenderDesc.isEmpty()) {
                updatePolicyRoleType.setGenderdesc(parentOrExtendedFamilyGenderDesc);
            } // TODO: Find Out If We'll Ever Need To Change This.

            if (!parentOrExtendedFamilyContactNumber.isEmpty()) {
                updatePolicyRoleType.setContactno(parentOrExtendedFamilyContactNumber);
                if (parentOrExtendedFamilyContactNumber.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setContactno("");
                }
            } else {
                String contactNo = targetRole.getContactno();
                updatePolicyRoleType.setContactno(contactNo);
            }

            if (!parentOrExtendedFamilyIncome.isEmpty()) {
                updatePolicyRoleType.setIncome(parentOrExtendedFamilyIncome);
                if (parentOrExtendedFamilyIncome.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setIncome("");
                }
            } else {
                String income = targetRole.getIncome();
                updatePolicyRoleType.setIncome(income);
            }

            if (!parentOrExtendedFamilyCoverAmount.isEmpty()) {
                updatePolicyRoleType.setCoveramnt(parentOrExtendedFamilyCoverAmount);
                if (parentOrExtendedFamilyCoverAmount.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setCoveramnt("");
                }
            } else {
                String coverAmnt = targetRole.getCoveramnt();
                updatePolicyRoleType.setCoveramnt(coverAmnt);
            }

            if (!parentOrExtendedFamilyPremium.isEmpty()) {
                updatePolicyRoleType.setPremium(parentOrExtendedFamilyPremium);
                if (parentOrExtendedFamilyPremium.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setPremium("");
                }
            } else {
                String premium = targetRole.getPremium();
                updatePolicyRoleType.setPremium(premium);
            }

            if (!parentOrExtendedFamilyComment.isEmpty()) {
                updatePolicyRoleType.setComment(parentOrExtendedFamilyComment);
                if (parentOrExtendedFamilyComment.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setComment("");
                }
            } else {
                // TODO: Find A Way To Get The Current Comment.
            }

            if (!parentOrExtendedFamilyEmail.isEmpty()) {
                updatePolicyRoleType.setEmail(parentOrExtendedFamilyEmail);
                if (parentOrExtendedFamilyEmail.equalsIgnoreCase(CLEAR)) {
                    updatePolicyRoleType.setEmail("");
                }
            } else {
                // TODO: Use The Other Service To Update The Email As Suggested By @Wiets.
                String email = targetRole.getEmail();
                updatePolicyRoleType.setEmail(email);
            }

            // TODO: Get Company Registration Number.

            // TODO: Review This Logic
            CoverDetail coverDetail = new CoverDetail();

            ResponseCoverDetail coverDetailBefore = targetRole.getCoverdetail();
            List<ResponseCover> coversBefore = null;
            if (coverDetailBefore != null) {
                coversBefore = coverDetailBefore.getCover();
            }

            List<Cover> covers = coverDetail.getCover();
            if (coversBefore != null) {
                for (ResponseCover coverBefore: coversBefore) {
                    Cover cover = new Cover();
                    if (!parentOrExtendedFamilyCoverAction.isEmpty()) {
                        cover.setAction(parentOrExtendedFamilyCoverAction);
                        if (parentOrExtendedFamilyCoverAction.equalsIgnoreCase(CLEAR)) {
                            cover.setAction("");
                        }
                    } else {
                        // TODO: Find A Way To Get The Current Action Action.
                    }

                    if (!parentOrExtendedFamilyCoverId.isEmpty()) {
                        cover.setCoverid(parentOrExtendedFamilyCoverId);
                        if (parentOrExtendedFamilyCoverId.equalsIgnoreCase(CLEAR)) {
                            cover.setCoverid("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String coverId = coverBefore.getCoverid();
                            cover.setCoverid(coverId);
                        }
                    }

                    if (!parentOrExtendedFamilyCoverInceptionDate.isEmpty()) {
                        cover.setInceptdate(parentOrExtendedFamilyCoverInceptionDate);
                        if (parentOrExtendedFamilyCoverInceptionDate.equalsIgnoreCase(CLEAR)) {
                            cover.setInceptdate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String inceptDate = coverBefore.getInceptdate();
                            cover.setInceptdate(inceptDate);
                        }
                    }

                    if (!parentOrExtendedFamilyCoverWaitEndDate.isEmpty()) {
                        cover.setWaitenddate(parentOrExtendedFamilyCoverWaitEndDate);
                        if (parentOrExtendedFamilyCoverWaitEndDate.equalsIgnoreCase(CLEAR)) {
                            cover.setWaitenddate("");
                        }
                    } else {
                        if (coverBefore != null) {
                            String waitEndDate = coverBefore.getWaitenddate();
                            cover.setWaitenddate(waitEndDate);
                        }
                    }

                    // TODO: Revisit Cover Detail Logic

                    covers.add(cover);
                }
            }

            updatePolicyRoleType.setCoverdetail(coverDetail);

            updatePolicyRoles.getRole().add(updatePolicyRoleType);
        }
    }

    private void updatePremiumStatusValidations() throws Exception {
        try {
            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            if (policy != null) {
                String premiumStatus = policy.getPremiumstatus();
                String expectedPolicyPremiumStatus = scenarioTester.getCellValue("Expected Policy Premium Status");

                if (!expectedPolicyPremiumStatus.isEmpty()) {
                    if (!expectedPolicyPremiumStatus.equalsIgnoreCase(premiumStatus)) {
                        log.error("Expected Policy Premium Status Not Correct. Expected: " + expectedPolicyPremiumStatus + " -> Found: " + premiumStatus);
                        throw new ValidationException("Expected Policy Premium Status Not Correct. Expected: " + expectedPolicyPremiumStatus + " -> Found: " + premiumStatus);
                    }
                }
            } else {
                log.error("No Policy Found or Policy is null.");
                throw new Exception("No Policy Found or Policy is null.");
            }
        } catch (Exception e) {
            String exceptionMessage = "Error while performing Update Premium Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void escalationValidations() throws Exception {
        try {
            validateEscalationStatus();

            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            if (policy != null) {
                ResponseRoles policyRoles = policy.getRoles();
                List<ResponsePolicyRoleType> roles = policyRoles.getRole();

                validatePolicyCoverAmount(policy);

                validatePolicyPremium(policy);

                validateNumberOfMembers(roles);

                validateRolePremiums(roles);

                validateRoleCoverAmounts(roles);
            } else {
                log.error("No Policy Found or Policy is null.");
                throw new Exception("No Policy Found or Policy is null.");
            }
        } catch (Exception e) {
            String exceptionMessage = "Error while performing Escalation Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void validateEscalationStatus() throws Exception {
        if (policyBasicAfter != null) {
            String premiumStatus = policyBasicAfter.getEscalationStatus();
            String expectedEscalation = scenarioTester.getCellValue("Expected Escalation");

            if (!expectedEscalation.isEmpty()) {
                if (!expectedEscalation.equalsIgnoreCase(premiumStatus)) {
                    log.error("Expected Escalation Not Correct. Expected: " + expectedEscalation + " -> Found: " + premiumStatus);
                    throw new Exception("Expected Escalation Not Correct. Expected: " + expectedEscalation + " -> Found: " + premiumStatus);
                }
            }
        } else {
            log.error("No Policy Found or Policy Basic is null.");
            throw new Exception("No Policy Found or Policy Basic is null.");
        }
    }

    private void editBeneficiaryDetailsValidations() throws Exception {
        try {
            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            if (policy != null) {
                validateBeneficiaryDetails(policy);
            } else {
                log.error("No Policy Found or Policy is null.");
                throw new Exception("No Policy Found or Policy is null.");
            }
        } catch (Exception e) {
            String exceptionMessage = "Error while performing Edit Beneficiary Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void editBankingValidations() throws Exception {
        try {
            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            if (policy != null) {
                validateBankingDetails(policy);
            } else {
                log.error("No Policy Found or Policy is null.");
                throw new Exception("No Policy Found or Policy is null.");
            }
        } catch (Exception e) {
            String exceptionMessage = "Error while performing Edit Banking Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void editMemberValidations() throws Exception {
        try {
            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            ResponseRoles policyRoles = policy.getRoles();
            List<ResponsePolicyRoleType> roles = policyRoles.getRole();

            if (targetRoleBefore != null) {
                String targetRoleId = targetRoleBefore.getRoleid();

                String roleId;
                for (ResponsePolicyRoleType role : roles) {
                    if (role != null) {
                        roleId = role.getRoleid();
                        if (roleId.equalsIgnoreCase(targetRoleId)) {
                            targetRoleAfter = role;
                            break;
                        }
                    }
                }

                if (targetRoleAfter != null) {
                    validateRole(targetRoleAfter);
                } else {
                    log.error("Retrieved Taret Role Not Valid ie., May Be Null.");
                    throw new Exception("Retrieved Taret Role Not Valid ie., May Be Null.");
                }
            } else {
                log.error("No Target Role To Be Validated.");
                throw new Exception("No Target Role To Be Validated.");
            }

            validatePolicyCoverAmount(policy);

            validatePolicyPremium(policy);
        } catch (Exception e) {
            String exceptionMessage = "Error while performing Edit Member Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void handleValidationException(Exception e, String exceptionMessage) throws Exception {
        log.error(exceptionMessage);
        if (e instanceof ValidationException) {
            throw new ValidationException(exceptionMessage);
        } else {
            throw new Exception(exceptionMessage);
        }
    }

    private void validateRole(ResponsePolicyRoleType targetRoleAfter) throws Exception {
        String expectedRoleType = scenarioTester.getCellValue("Expected Role Type");
        String expectedFullName = scenarioTester.getCellValue("Expected Full Name");
        String expectedMiddleName = scenarioTester.getCellValue("Expected Middle Name");
        String expectedIdNumber = scenarioTester.getCellValue("Expected ID Number");
        String expectedBirthDate = scenarioTester.getCellValue("Expected Birth Date");
        String expectedGenderCode = scenarioTester.getCellValue("Expected Gender Code");
        String expectedGenderDesc = scenarioTester.getCellValue("Expected Gender Desc");
        String expectedContactNumber = scenarioTester.getCellValue("Expected Contact Number");
        String expectedIncome = scenarioTester.getCellValue("Expected Income");
        String expectedRoleCoverAmount = scenarioTester.getCellValue("Expected Role Cover Amount");
        String expectedRolePremium = scenarioTester.getCellValue("Expected Role Premium");
        String expectedComment = scenarioTester.getCellValue("Expected Comment");
        String expectedEmail = scenarioTester.getCellValue("Expected Email");
        String expectedCoverId = scenarioTester.getCellValue("Expected Cover ID");
        String expectedCoverInceptionDate = scenarioTester.getCellValue("Expected Cover Inception Date");
        String expectedCoverWaitEndDate = scenarioTester.getCellValue("Expected Cover Wait End Date");
        String expectedCoverCoverAmount = scenarioTester.getCellValue("Expected Cover Cover Amount");

        if (!expectedRoleType.isEmpty()) {
            String roleType = targetRoleAfter.getRoletype();
            if (!roleType.equalsIgnoreCase(expectedRoleType)) {
                log.error("Expected Role Type Not Correct. Expected: " + expectedRoleType + " -> Found: " + roleType);
                throw new ValidationException("Expected Role Type Not Correct. Expected: " + expectedRoleType + " -> Found: " + roleType);
            }
        }

        if (!expectedFullName.isEmpty()) {
            String fullName = targetRoleAfter.getFullname();
            if (!fullName.equalsIgnoreCase(expectedFullName)) {
                log.error("Expected Full Name Not Correct. Expected: " + expectedFullName + " -> Found: " + fullName);
                throw new ValidationException("Expected Full Name Not Correct. Expected: " + expectedFullName + " -> Found: " + fullName);
            }
        }

        if (!expectedMiddleName.isEmpty()) {
            String middleName = targetRoleAfter.getMiddlename();
            if (!middleName.equalsIgnoreCase(expectedMiddleName)) {
                log.error("Expected Middle Name Not Correct. Expected: " + expectedMiddleName + " -> Found: " + middleName);
                throw new ValidationException("Expected Middle Name Not Correct. Expected: " + expectedMiddleName + " -> Found: " + middleName);
            }
        }

        if (!expectedIdNumber.isEmpty()) {
            String idNumber = targetRoleAfter.getIdnumber();
            if (!idNumber.equalsIgnoreCase(expectedIdNumber)) {
                log.error("Expected ID Number Not Correct. Expected: " + expectedIdNumber + " -> Found: " + idNumber);
                throw new ValidationException("Expected ID Number Not Correct. Expected: " + expectedIdNumber + " -> Found: " + idNumber);
            }
        }

        if (!expectedBirthDate.isEmpty()) {
            String birthDate = targetRoleAfter.getBirthdate();
            if (!birthDate.equalsIgnoreCase(expectedBirthDate)) {
                log.error("Expected Birth Date Not Correct. Expected: " + expectedBirthDate + " -> Found: " + birthDate);
                throw new ValidationException("Expected Birth Date Not Correct. Expected: " + expectedBirthDate + " -> Found: " + birthDate);
            }
        }

        if (!expectedGenderCode.isEmpty()) {
            String genderCode = targetRoleAfter.getGendercode();
            if (!genderCode.equalsIgnoreCase(expectedGenderCode)) {
                log.error("Expected Gender Code Not Correct. Expected: " + expectedGenderCode + " -> Found: " + genderCode);
                throw new ValidationException("Expected Gender Code Not Correct. Expected: " + expectedGenderCode + " -> Found: " + genderCode);
            }
        }

        if (!expectedGenderDesc.isEmpty()) {
            String genderDesc = targetRoleAfter.getGenderdesc();
            if (!genderDesc.equalsIgnoreCase(expectedGenderDesc)) {
                log.error("Expected Gender Desc Not Correct. Expected: " + expectedGenderDesc + " -> Found: " + genderDesc);
                throw new ValidationException("Expected Gender Desc Not Correct. Expected: " + expectedGenderDesc + " -> Found: " + genderDesc);
            }
        }

        if (!expectedContactNumber.isEmpty()) {
            String contactNo = targetRoleAfter.getContactno();
            if (!contactNo.equalsIgnoreCase(expectedContactNumber)) {
                log.error("Expected Contact Number Not Correct. Expected: " + expectedContactNumber + " -> Found: " + contactNo);
                throw new ValidationException("Expected Contact Number Not Correct. Expected: " + expectedContactNumber + " -> Found: " + contactNo);
            }
        }

        if (!expectedIncome.isEmpty()) {
            String income = targetRoleAfter.getIncome();
            if (!income.equalsIgnoreCase(expectedIncome)) {
                log.error("Expected Income Not Correct. Expected: " + expectedIncome + " -> Found: " + income);
                throw new ValidationException("Expected Income Not Correct. Expected: " + expectedIncome + " -> Found: " + income);
            }
        }

        if (!expectedRoleCoverAmount.isEmpty()) {
            String coverAmnt = targetRoleAfter.getCoveramnt();
            Double expectedCoverAmountValue = 0.00, coverAmountValue = 0.00;

            try {
                expectedCoverAmountValue = Double.valueOf(expectedRoleCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedRoleCoverAmount): " + e.getMessage());
            }

            if (!coverAmnt.isEmpty()) {
                try {
                    coverAmountValue = Double.valueOf(coverAmnt);
                } catch (Exception e) {
                    log.error("Double.valueOf(roleCoverAmount): " + e.getMessage());
                }
            }

            if (!coverAmountValue.equals(expectedCoverAmountValue)) {
                log.error("Expected Role Cover Amount Not Correct. Expected: " + expectedCoverAmountValue + " -> Found: " + coverAmountValue);
                throw new ValidationException("Expected Role Cover Amount Not Correct. Expected: " + expectedCoverAmountValue + " -> Found: " + coverAmountValue);
            }
        }

        if (!expectedRolePremium.isEmpty()) {
            String premium = targetRoleAfter.getPremium();
            Double expectedRolePremiumValue = 0.00, premiumValue = 0.00;

            try {
                expectedRolePremiumValue = Double.valueOf(expectedRolePremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedRolePremium): " + e.getMessage());
            }

            if (!premium.isEmpty()) {
                try {
                    premiumValue = Double.valueOf(premium);
                } catch (Exception e) {
                    log.error("Double.valueOf(premium): " + e.getMessage());
                }
            }

            if (!premiumValue.equals(expectedRolePremiumValue)) {
                log.error("Expected Role Premium Not Correct. Expected: " + expectedRolePremiumValue + " -> Found: " + premiumValue);
                throw new ValidationException("Expected Role Premium Not Correct. Expected: " + expectedRolePremiumValue + " -> Found: " + premiumValue);
            }
        }

        if (!expectedComment.isEmpty()) {
            // TODO: Find A Way To Get The Comment.
        }

        if (!expectedEmail.isEmpty()) {
            String email = targetRoleAfter.getEmail();
            if (!email.equalsIgnoreCase(expectedEmail)) {
                log.error("Expected Email Not Correct. Expected: " + expectedEmail + " -> Found: " + email);
                throw new ValidationException("Expected Email Not Correct. Expected: " + expectedEmail + " -> Found: " + email);
            }
        }

        ResponseCoverDetail coverDetail = targetRoleAfter.getCoverdetail();
        List<ResponseCover> covers = null;
        if (coverDetail != null) {
            covers = coverDetail.getCover();
        }

        ResponseCover targetCover = null;
        if (covers != null) {
            for (ResponseCover cover: covers) {
                String coverId = cover.getCoverid();
                if (coverId.equalsIgnoreCase(expectedCoverId)) {
                    targetCover = cover;
                }
            }
        }

        if (targetCover != null) {
            if (!expectedCoverId.isEmpty()) {
                String coverId = targetCover.getCoverid();
                if (!coverId.equalsIgnoreCase(expectedCoverId)) {
                    log.error("Expected Cover ID Not Correct. Expected: " + expectedCoverId + " -> Found: " + coverId);
                    throw new ValidationException("Expected Cover ID Not Correct. Expected: " + expectedCoverId + " -> Found: " + coverId);
                }
            }

            if (!expectedCoverInceptionDate.isEmpty()) {
                String inceptDate = targetCover.getInceptdate();
                if (!inceptDate.equalsIgnoreCase(expectedCoverInceptionDate)) {
                    log.error("Expected Cover Inception Date Not Correct. Expected: " + expectedCoverInceptionDate + " -> Found: " + inceptDate);
                    throw new ValidationException("Expected Cover Inception Date ID Not Correct. Expected: " + expectedCoverInceptionDate + " -> Found: " + inceptDate);
                }
            }

            if (!expectedCoverWaitEndDate.isEmpty()) {
                String waitEndDate = targetCover.getWaitenddate();
                if (!waitEndDate.equalsIgnoreCase(expectedCoverWaitEndDate)) {
                    log.error("Expected Cover Wait End Date Not Correct. Expected: " + expectedCoverWaitEndDate + " -> Found: " + waitEndDate);
                    throw new ValidationException("Expected Cover Wait End Date Not Correct. Expected: " + expectedCoverWaitEndDate + " -> Found: " + waitEndDate);
                }
            }

            if (!expectedCoverCoverAmount.isEmpty()) {
                String coverAmount = targetCover.getAmount();
                if (!coverAmount.equalsIgnoreCase(expectedCoverCoverAmount)) {
                    log.error("Expected Cover Cover Amount Not Correct. Expected: " + expectedCoverCoverAmount + " -> Found: " + coverAmount);
                    throw new ValidationException("Expected Cover Cover Amount Not Correct. Expected: " + expectedCoverCoverAmount + " -> Found: " + coverAmount);
                }
            }
        }
    }

    private void addMemberValidations() throws Exception {
        try {
            ResponsePolicy policy = getPolicyDetailResponseOutputAfter.getPolicy();
            ResponseRoles policyRoles = policy.getRoles();
            List<ResponsePolicyRoleType> roles = policyRoles.getRole();

            validatePolicyCoverAmount(policy);

            validatePolicyPremium(policy);

            validateNumberOfMembers(roles);

            validateRolePremiums(roles);

            validateRoleCoverAmounts(roles);

        } catch (Exception e) {
            String exceptionMessage = "Error while performing Add Member Validations. " + e.getMessage();
            handleValidationException(e, exceptionMessage);
        }
    }

    private void validateBeneficiaryDetails(ResponsePolicy policy) throws Exception {
        ResponseBeneficiary beneficiary = policy.getBeneficiary();
        if (beneficiary != null) {
            String expectedBeneficiaryFullName = scenarioTester.getCellValue("Expected Beneficiary Full Name");

            if (!expectedBeneficiaryFullName.isEmpty()) {
                String fullName = beneficiary.getFullname();
                if (!expectedBeneficiaryFullName.equalsIgnoreCase(fullName)) {
                    log.error("Expected Beneficiary Full Name Not Correct. Expected: " + expectedBeneficiaryFullName + " -> Found: " + fullName);
                    throw new ValidationException("Expected Beneficiary Full Name Not Correct. Expected: " + expectedBeneficiaryFullName + " -> Found: " + fullName);
                }
            }
            String expectedBeneficiaryEmail = scenarioTester.getCellValue("Expected Beneficiary Email");

            if (!expectedBeneficiaryEmail.isEmpty()) {
                String email = beneficiary.getEmail();
                if (!expectedBeneficiaryEmail.equalsIgnoreCase(email)) {
                    log.error("Expected Beneficiary Email Not Correct. " + expectedBeneficiaryEmail + " -> Found: " + email);
                    throw new ValidationException("Expected Beneficiary Email Not Correct. " + expectedBeneficiaryEmail + " -> Found: " + email);
                }
            }

            String expectedBeneficiaryContactNumber = scenarioTester.getCellValue("Expected Beneficiary Contact Number");

            if (!expectedBeneficiaryContactNumber.isEmpty()) {
                String contactNo = beneficiary.getContactno();
                if (!expectedBeneficiaryContactNumber.equalsIgnoreCase(contactNo)) {
                    log.error("Expected Beneficiary Contact Number Not Correct. " + expectedBeneficiaryContactNumber + " -> Found: " + contactNo);
                    throw new ValidationException("Expected Beneficiary Contact Number Not Correct. " + expectedBeneficiaryContactNumber + " -> Found: " + contactNo);
                }
            }

            String expectedBeneficiaryBirthDate = scenarioTester.getCellValue("Expected Beneficiary Birth Date");

            if (!expectedBeneficiaryBirthDate.isEmpty()) {
                String birthDate = beneficiary.getBirthdate();
                if (!expectedBeneficiaryBirthDate.equalsIgnoreCase(birthDate)) {
                    log.error("Expected Beneficiary Birth Date Not Correct. " + expectedBeneficiaryBirthDate + " -> Found: " + birthDate);
                    throw new ValidationException("Expected Beneficiary Birth Date Not Correct. " + expectedBeneficiaryBirthDate + " -> Found: " + birthDate);
                }
            }

            String expectedBeneficiaryIdNumber = scenarioTester.getCellValue("Expected Beneficiary Id Number");

            if (!expectedBeneficiaryIdNumber.isEmpty()) {
                String idNumber = beneficiary.getIdnumber();
                if (!expectedBeneficiaryIdNumber.equalsIgnoreCase(idNumber)) {
                    log.error("Expected Beneficiary Id Number Not Correct. " + expectedBeneficiaryIdNumber + " -> Found: " + idNumber);
                    throw new ValidationException("Expected Beneficiary Id Number Not Correct. " + expectedBeneficiaryIdNumber + " -> Found: " + idNumber);
                }
            }
        } else {
            log.error("No Beneficiary Found.");
            throw new Exception("No Beneficiary Found.");
        }
    }

    private void validateBankingDetails(ResponsePolicy policy) throws Exception {
        Banking banking = policy.getBanking();
        if (banking != null) {
            String expectedBankName = scenarioTester.getCellValue("Expected Bank Name");
            if (!expectedBankName.isEmpty()) {
                String name = banking.getName();
                if (!expectedBankName.equalsIgnoreCase(name)) {
                    log.error("Expected Bank Name Not Correct. Expected: " + expectedBankName + " -> Found: " + name);
                    throw new ValidationException("Expected Bank Name Not Correct. Expected: " + expectedBankName + " -> Found: " + name);
                }
            }

            String expectedBranchName = scenarioTester.getCellValue("Expected Branch Name");
            if (!expectedBranchName.isEmpty()) {
                String branchName = banking.getBranchname();
                if (!expectedBranchName.equalsIgnoreCase(branchName)) {
                    log.error("Expected Branch Name Not Correct. Expected: " + expectedBankName + " -> Found: " + branchName);
                    throw new ValidationException("Expected Branch Name Not Correct. Expected: " + expectedBankName + " -> Found: " + branchName);
                }
            }

            String expectedBranchCode = scenarioTester.getCellValue("Expected Branch Code");
            if (!expectedBranchCode.isEmpty()) {
                String branchCode = banking.getBranchcode();
                if (!expectedBranchCode.equalsIgnoreCase(branchCode)) {
                    log.error("Expected Branch Code Not Correct. Expected: " + expectedBranchCode + " -> Found: " + branchCode);
                    throw new ValidationException("Expected Branch Code Not Correct. Expected: " + expectedBranchCode + " -> Found: " + branchCode);
                }
            }

            String expectedAccountNumber = scenarioTester.getCellValue("Expected Account Number");
            if (!expectedAccountNumber.isEmpty()) {
                String accountNumber = banking.getAccountno();
                if (!expectedAccountNumber.equalsIgnoreCase(accountNumber)) {
                    log.error("Expected Account Number Not Correct. Expected: " + expectedAccountNumber + " -> Found: " + accountNumber);
                    throw new ValidationException("Expected Account Number Not Correct. Expected: " + expectedAccountNumber + " -> Found: " + accountNumber);
                }
            }

            String expectedAccountCode = scenarioTester.getCellValue("Expected Account Code");
            if (!expectedAccountCode.isEmpty()) {
                String accountCode = banking.getAccountcode();
                if (!expectedAccountCode.equalsIgnoreCase(accountCode)) {
                    log.error("Expected Account Code Not Correct. Expected: " + expectedAccountCode + " -> Found: " + accountCode);
                    throw new ValidationException("Expected Account Code Not Correct. Expected: " + expectedAccountCode + " -> Found: " + accountCode);
                }
            }

            String expectedAccountDesc = scenarioTester.getCellValue("Expected Account Desc");
            if (!expectedAccountDesc.isEmpty()) {
                String accountDesc = banking.getAccountdesc();
                if (!expectedAccountDesc.equalsIgnoreCase(accountDesc)) {
                    log.error("Expected Account Desc Not Correct. Expected: " + expectedAccountDesc + " -> Found: " + accountDesc);
                    throw new ValidationException("Expected Account Desc Not Correct. Expected: " + expectedAccountDesc + " -> Found: " + accountDesc);
                }
            }

            String expectedDueDate = scenarioTester.getCellValue("Expected Due Date");
            if (!expectedDueDate.isEmpty()) {
                String dueDate = banking.getDuedate();
                String formatedExpectedDueDate = expectedDueDate;
                if (expectedDueDate.contains("-")) {
                    formatedExpectedDueDate = scenarioTester.getSimpleDateString(expectedDueDate, "yyyyMMdd");
                }

                if (!formatedExpectedDueDate.equalsIgnoreCase(dueDate)) {
                    log.error("Expected Due Date Not Correct. Expected: " + formatedExpectedDueDate + " -> Found: " + dueDate);
                    throw new ValidationException("Expected Due Date Not Correct. Expected: " + formatedExpectedDueDate + " -> Found: " + dueDate);
                }
            }

            String expectedPaymentDay = scenarioTester.getCellValue("Expected Payment Day");
            if (!expectedPaymentDay.isEmpty()) {
                String paymentDay = banking.getPaymentday();
                if (!expectedPaymentDay.equalsIgnoreCase(paymentDay)) {
                    log.error("Expected Payment Day Not Correct. Expected: " + expectedPaymentDay + " -> Found: " + paymentDay);
                    throw new ValidationException("Expected Payment Day Not Correct. Expected: " + expectedPaymentDay + " -> Found: " + paymentDay);
                }
            }
        }
    }

    private void validateMembersData(List<ResponsePolicyRoleType> roles) throws Exception {
        String expectedNumberOfMembers = scenarioTester.getCellValue("Expected Number Of Members");
        String expectedNumberOfChildren = scenarioTester.getCellValue("Expected Number Of Children");
        String expectedNumberOfParentOrExtendedFamily = scenarioTester.getCellValue("Expected Number Of Parent/Extended Family");

        String expectedPolicyHolderPremium = scenarioTester.getCellValue("Expected Policy Holder Premium");
        String expectedSpousePremium = scenarioTester.getCellValue("Expected Spouse Premium");
        String expectedChildPremium = scenarioTester.getCellValue("Expected Child Premium");

        Integer expectedNumberOfMembersValue = 0, expectedNumberOfChildrenValue = 0, expectedNumberOfParentOrExtendedFamilyValue = 0;

        if (!expectedNumberOfMembers.isEmpty()) {
            try {
                expectedNumberOfMembersValue = Integer.valueOf(expectedNumberOfMembers);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfMembers): " + e.getMessage());
            }
        }

        if (!expectedNumberOfChildren.isEmpty()) {
            try {
                expectedNumberOfChildrenValue = Integer.valueOf(expectedNumberOfChildren);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfChildren): " + e.getMessage());
            }
        }

        if (!expectedNumberOfParentOrExtendedFamily.isEmpty()) {
            try {
                expectedNumberOfParentOrExtendedFamilyValue = Integer.valueOf(expectedNumberOfParentOrExtendedFamily);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfParentOrExtendedFamily): " + e.getMessage());
            }
        }

        Integer numberOfMembers = 0 , numberOfChildren = 0, numberOfParentOrExtendedFamily = 0;
        Double expectedPolicyHolderPremiumValue = 0.00, expectedSpousePremiumValue = 0.00, expectedChildPremiumValue = 0.00;

        if (!expectedPolicyHolderPremium.isEmpty()) {
            try {
                expectedPolicyHolderPremiumValue = Double.valueOf(expectedPolicyHolderPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedPolicyHolderPremium): " + e.getMessage());
            }
        }

        if (!expectedSpousePremium.isEmpty()) {
            try {
                expectedSpousePremiumValue = Double.valueOf(expectedSpousePremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedSpousePremium): " + e.getMessage());
            }
        }

        if (!expectedChildPremium.isEmpty()) {
            try {
                expectedChildPremiumValue = Double.valueOf(expectedChildPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedChildPremium): " + e.getMessage());
            }
        }

        String rolePremium, roleType, roleStatus;
        Double rolePremiumValue =  0.00;
        for (ResponsePolicyRoleType role : roles) {
            roleStatus = role.getStatus();
            rolePremium = role.getPremium();
            roleType = role.getRoletype();

            if (!roleStatus.isEmpty()) {
                if (roleStatus.equalsIgnoreCase("ACTIVE")) {
                    numberOfMembers++;

                    if (!rolePremium.isEmpty()) {
                        try {
                            rolePremiumValue = Double.valueOf(rolePremium);
                        } catch (Exception e) {
                            log.error("Double.valueOf(rolePremium): " + e.getMessage());
                        }
                    }

                    if (!roleType.isEmpty()) {
                        if (roleType.equalsIgnoreCase("POLICY HOLDER")) {
                            if (!expectedPolicyHolderPremium.isEmpty()) {
                                if (!rolePremiumValue.equals(expectedPolicyHolderPremiumValue)) {
                                    throw new ValidationException("Expected Policy Holder Premium Not Correct: " + rolePremiumValue + " != " + expectedPolicyHolderPremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("SPOUSE")) {
                            if (!expectedSpousePremium.isEmpty()) {
                                if (!rolePremiumValue.equals(expectedSpousePremiumValue)) {
                                    throw new ValidationException("Expected Spouse Premium Not Correct: " + rolePremiumValue + " != " + expectedSpousePremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("CHILD")) {
                            numberOfChildren++;
                            if (!expectedChildPremium.isEmpty()){
                                if (!rolePremiumValue.equals(expectedChildPremiumValue)) {
                                    throw new ValidationException("Expected Child Premium Not Correct: " + rolePremiumValue + " != " + expectedChildPremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("PARENT") || roleType.equalsIgnoreCase("EXTENDED FAMILY")) {
                            numberOfParentOrExtendedFamily++;
                        }
                    }
                }
            }
        }

        if (!expectedNumberOfMembers.isEmpty()) {
            if (!numberOfMembers.equals(expectedNumberOfMembersValue)) {
                throw new ValidationException("Expected Number Of Members Not Correct: " + numberOfMembers + " != " + expectedNumberOfMembersValue);
            }
        }

        if (!expectedNumberOfChildren.isEmpty()) {
            if (!numberOfChildren.equals(expectedNumberOfChildrenValue)) {
                throw new ValidationException("Expected Number Of Children Not Correct: " + numberOfChildren + " != " + expectedNumberOfChildrenValue);
            }
        }

        if (!expectedNumberOfParentOrExtendedFamily.isEmpty()) {
            if (!numberOfParentOrExtendedFamily.equals(expectedNumberOfParentOrExtendedFamilyValue)) {
                throw new ValidationException("Expected Number Of Parent/Extended Family Not Correct: " + numberOfParentOrExtendedFamily + " != " + expectedNumberOfParentOrExtendedFamilyValue);
            }
        }
    }

    private void validateRoleCoverAmounts(List<ResponsePolicyRoleType> roles) throws Exception {
        String expectedPolicyHolderCoverAmount = scenarioTester.getCellValue("Expected Policy Holder Cover Amount");
        String expectedSpouseCoverAmount = scenarioTester.getCellValue("Expected Spouse Cover Amount");
        String expectedChildCoverAmount = scenarioTester.getCellValue("Expected Child Cover Amount");

        Double expectedPolicyHolderCoverAmountValue = 0.00, expectedSpouseCoverAmountValue = 0.00, expectedChildCoverAmountValue = 0.00;

        if (!expectedPolicyHolderCoverAmount.isEmpty()) {
            try {
                expectedPolicyHolderCoverAmountValue = Double.valueOf(expectedPolicyHolderCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedPolicyHolderCoverAmount): " + e.getMessage());
            }
        }

        if (!expectedSpouseCoverAmount.isEmpty()) {
            try {
                expectedSpouseCoverAmountValue = Double.valueOf(expectedSpouseCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedSpouseCoverAmount): " + e.getMessage());
            }
        }

        if (!expectedChildCoverAmount.isEmpty()) {
            try {
                expectedChildCoverAmountValue = Double.valueOf(expectedChildCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedChildCoverAmount): " + e.getMessage());
            }
        }

        String roleCoverAmount, roleType, roleStatus;
        Double roleCoverAmountValue =  0.00;
        for (ResponsePolicyRoleType role : roles) {
            roleStatus = role.getStatus();
            roleCoverAmount = role.getCoveramnt();
            roleType = role.getRoletype();

            if (!roleStatus.isEmpty()) {
                if (roleStatus.equalsIgnoreCase("ACTIVE")) {
                    if (!roleCoverAmount.isEmpty()) {
                        try {
                            roleCoverAmountValue = Double.valueOf(roleCoverAmount);
                        } catch (Exception e) {
                            log.error("Double.valueOf(roleCoverAmount): " + e.getMessage());
                        }
                    }

                    if (!roleType.isEmpty()) {
                        if (roleType.equalsIgnoreCase("POLICY HOLDER")) {
                            if (!expectedPolicyHolderCoverAmount.isEmpty()) {
                                if (!roleCoverAmountValue.equals(expectedPolicyHolderCoverAmountValue)) {
                                    throw new ValidationException("Expected Policy Holder Premium Not Correct: " + roleCoverAmountValue + " != " + expectedPolicyHolderCoverAmountValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("SPOUSE")) {
                            if (!expectedSpouseCoverAmount.isEmpty()) {
                                if (!roleCoverAmountValue.equals(expectedSpouseCoverAmountValue)) {
                                    throw new ValidationException("Expected Spouse Premium Not Correct: " + roleCoverAmountValue + " != " + expectedSpouseCoverAmountValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("CHILD")) {
                            if (!expectedChildCoverAmount.isEmpty()){
                                if (!roleCoverAmountValue.equals(expectedChildCoverAmountValue)) {
                                    throw new ValidationException("Expected Child Premium Not Correct: " + roleCoverAmountValue + " != " + expectedChildCoverAmountValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("PARENT") || roleType.equalsIgnoreCase("EXTENDED FAMILY")) {
                            // TODO: Validate PARENT and EXTENDED FAMILY Roles Cover Amounts.
                        }
                    }
                }
            }
        }
    }

    private void validateRolePremiums(List<ResponsePolicyRoleType> roles) throws Exception {
        String expectedPolicyHolderPremium = scenarioTester.getCellValue("Expected Policy Holder Premium");
        String expectedSpousePremium = scenarioTester.getCellValue("Expected Spouse Premium");
        String expectedChildPremium = scenarioTester.getCellValue("Expected Child Premium");

        Double expectedPolicyHolderPremiumValue = 0.00, expectedSpousePremiumValue = 0.00, expectedChildPremiumValue = 0.00;

        if (!expectedPolicyHolderPremium.isEmpty()) {
            try {
                expectedPolicyHolderPremiumValue = Double.valueOf(expectedPolicyHolderPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedPolicyHolderPremium): " + e.getMessage());
            }
        }

        if (!expectedSpousePremium.isEmpty()) {
            try {
                expectedSpousePremiumValue = Double.valueOf(expectedSpousePremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedSpousePremium): " + e.getMessage());
            }
        }

        if (!expectedChildPremium.isEmpty()) {
            try {
                expectedChildPremiumValue = Double.valueOf(expectedChildPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedChildPremium): " + e.getMessage());
            }
        }

        String rolePremium, roleType, roleStatus;
        Double rolePremiumValue =  0.00;
        for (ResponsePolicyRoleType role : roles) {
            roleStatus = role.getStatus();
            rolePremium = role.getPremium();
            roleType = role.getRoletype();

            if (!roleStatus.isEmpty()) {
                if (roleStatus.equalsIgnoreCase("ACTIVE")) {
                    if (!rolePremium.isEmpty()) {
                        try {
                            rolePremiumValue = Double.valueOf(rolePremium);
                        } catch (Exception e) {
                            log.error("Double.valueOf(rolePremium): " + e.getMessage());
                        }
                    }

                    if (!roleType.isEmpty()) {
                        if (roleType.equalsIgnoreCase("POLICY HOLDER")) {
                            if (!expectedPolicyHolderPremium.isEmpty()) {
                                if (!rolePremiumValue.equals(expectedPolicyHolderPremiumValue)) {
                                    throw new ValidationException("Expected Policy Holder Premium Not Correct: " + rolePremiumValue + " != " + expectedPolicyHolderPremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("SPOUSE")) {
                            if (!expectedSpousePremium.isEmpty()) {
                                if (!rolePremiumValue.equals(expectedSpousePremiumValue)) {
                                    throw new ValidationException("Expected Spouse Premium Not Correct: " + rolePremiumValue + " != " + expectedSpousePremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("CHILD")) {
                            if (!expectedChildPremium.isEmpty()){
                                if (!rolePremiumValue.equals(expectedChildPremiumValue)) {
                                    throw new ValidationException("Expected Child Premium Not Correct: " + rolePremiumValue + " != " + expectedChildPremiumValue);
                                }
                            }
                        } else if (roleType.equalsIgnoreCase("PARENT") || roleType.equalsIgnoreCase("EXTENDED FAMILY")) {
                            // TODO: Validate PARENT and EXTENDED FAMILY Roles Premiums.
                        }
                    }
                }
            }
        }
    }

    private void validateNumberOfMembers(List<ResponsePolicyRoleType> roles) throws Exception {
        String expectedNumberOfMembers = scenarioTester.getCellValue("Expected Number Of Members");
        String expectedNumberOfChildren = scenarioTester.getCellValue("Expected Number Of Children");
        String expectedNumberOfParentOrExtendedFamily = scenarioTester.getCellValue("Expected Number Of Parent/Extended Family");

        Integer expectedNumberOfMembersValue = 0, expectedNumberOfChildrenValue = 0, expectedNumberOfParentOrExtendedFamilyValue = 0;

        if (!expectedNumberOfMembers.isEmpty()) {
            try {
                expectedNumberOfMembersValue = Integer.valueOf(expectedNumberOfMembers);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfMembers): " + e.getMessage());
            }
        }

        if (!expectedNumberOfChildren.isEmpty()) {
            try {
                expectedNumberOfChildrenValue = Integer.valueOf(expectedNumberOfChildren);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfChildren): " + e.getMessage());
            }
        }

        if (!expectedNumberOfParentOrExtendedFamily.isEmpty()) {
            try {
                expectedNumberOfParentOrExtendedFamilyValue = Integer.valueOf(expectedNumberOfParentOrExtendedFamily);
            } catch (Exception e) {
                log.error("Integer.valueOf(expectedNumberOfParentOrExtendedFamily): " + e.getMessage());
            }
        }

        Integer numberOfMembers = 0 , numberOfChildren = 0, numberOfParentOrExtendedFamily = 0;

        String roleType, roleStatus;
        for (ResponsePolicyRoleType role : roles) {
            roleStatus = role.getStatus();
            roleType = role.getRoletype();

            if (!roleStatus.isEmpty()) {
                if (roleStatus.equalsIgnoreCase("ACTIVE")) {
                    numberOfMembers++;

                    if (!roleType.isEmpty()) {
                        if (roleType.equalsIgnoreCase("POLICY HOLDER")) {
                            // TODO: Count To Detect If We Have More Than 1
                        } else if (roleType.equalsIgnoreCase("SPOUSE")) {
                            // TODO: Count To Detect If We Have More Than 1
                        } else if (roleType.equalsIgnoreCase("CHILD")) {
                            numberOfChildren++;
                        } else if (roleType.equalsIgnoreCase("PARENT") || roleType.equalsIgnoreCase("EXTENDED FAMILY")) {
                            numberOfParentOrExtendedFamily++;
                        }
                    }
                }
            }
        }

        if (!expectedNumberOfMembers.isEmpty()) {
            if (!numberOfMembers.equals(expectedNumberOfMembersValue)) {
                throw new ValidationException("Expected Number Of Members Not Correct: " + numberOfMembers + " != " + expectedNumberOfMembersValue);
            }
        }

        if (!expectedNumberOfChildren.isEmpty()) {
            if (!numberOfChildren.equals(expectedNumberOfChildrenValue)) {
                throw new ValidationException("Expected Number Of Children Not Correct: " + numberOfChildren + " != " + expectedNumberOfChildrenValue);
            }
        }

        if (!expectedNumberOfParentOrExtendedFamily.isEmpty()) {
            if (!numberOfParentOrExtendedFamily.equals(expectedNumberOfParentOrExtendedFamilyValue)) {
                throw new ValidationException("Expected Number Of Parent/Extended Family Not Correct: " + numberOfParentOrExtendedFamily + " != " + expectedNumberOfParentOrExtendedFamilyValue);
            }
        }
    }

    private void validatePolicyPremium(ResponsePolicy policy) throws Exception {
        String expectedPremium = scenarioTester.getCellValue("Expected Premium");
        Double expectedPremiumValue = 0.00, policyPremiumValue = 0.00;
        String policyPremium = policy.getPremium();

        if (!policyPremium.isEmpty()) {
            try {
                policyPremiumValue = Double.valueOf(policyPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(policyPremium): " + e.getMessage());
            }
        }

        if (!expectedPremium.isEmpty()) {
            try {
                expectedPremiumValue = Double.valueOf(expectedPremium);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedPremium): " + e.getMessage());
            }
        }

        if (!expectedPremium.isEmpty()) {
            if (!policyPremiumValue.equals(expectedPremiumValue)) {
                throw new ValidationException("Expected Policy Premium Not Correct: " + policyPremiumValue + " != " + expectedPremiumValue);
            }
        }
    }

    private void validatePolicyCoverAmount(ResponsePolicy policy) throws Exception {
        String expectedCoverAmount = scenarioTester.getCellValue("Expected Cover Amount");
        Double expectedCoverAmountValue = 0.00, policyCoverAmountValue = 0.00;
        String policyCoverAmount = policy.getCoveramount();

        if (!policyCoverAmount.isEmpty()) {
            try {
                policyCoverAmountValue = Double.valueOf(policyCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(policyCoverAmount): " + e.getMessage());
            }
        }

        if (!expectedCoverAmount.isEmpty()) {
            try {
                expectedCoverAmountValue = Double.valueOf(expectedCoverAmount);
            } catch (Exception e) {
                log.error("Double.valueOf(expectedCoverAmount): " + e.getMessage());
            }
        }

        if (!expectedCoverAmount.isEmpty()) {
            if (!policyCoverAmountValue.equals(expectedCoverAmountValue)) {
                throw new ValidationException("Expected Policy Cover Amount Not Correct: " + policyCoverAmountValue + " != " + expectedCoverAmountValue);
            }
        }
    }

}
