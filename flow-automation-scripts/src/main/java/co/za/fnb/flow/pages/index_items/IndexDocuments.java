package co.za.fnb.flow.pages.index_items;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.index_items.page_factory.IndexDocumentsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class IndexDocuments extends BasePage {
    private Logger log  = LogManager.getLogger(IndexDocuments.class);
    IndexDocumentsPageObjects indexDocumentsPageObjects = new IndexDocumentsPageObjects(driver);

    public IndexDocuments(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void inputPolicyNumber(String policyNumber) throws Exception {
        log.info("Inputting Policy Number.");
        try {
            WebElement policyNumberField = indexDocumentsPageObjects.getPolicyNumber();
            type(policyNumberField, policyNumber);
        } catch (Exception e) {
            log.error("Error while inputting Policy Number. " + e.getMessage());
            throw new Exception("Error while inputting Policy Number. " + e.getMessage());
        }
    }

    public void clickSearch() throws Exception {
        log.info("Clicking Search.");
        try {
            WebElement search = indexDocumentsPageObjects.getSearch();
            click(search);
        } catch (Exception e) {
            log.error("Error while clicking Search. " + e.getMessage());
            throw new Exception("Error while clicking Search. " + e.getMessage());
        }
    }

    public String getIdNumber() throws Exception {
        log.info("Getting Id Number.");
        try {
            WebElement idNumberField = indexDocumentsPageObjects.getIdNumber();
            String idNumber = getText(idNumberField);
            return idNumber;
        } catch (Exception e) {
            log.error("Error while getting Id Number. " + e.getMessage());
            throw new Exception("Error while getting Id Number. " + e.getMessage());
        }
    }

    public void clickSelectDocType() throws Exception {
        log.info("Clicking Select Doc Type.");
        try {
            // TODO: Add An Explicit Wait For The Element Before Clicking
            WebElement selectDocType = indexDocumentsPageObjects.getSelectDocType();
            click(selectDocType);
        } catch (Exception e) {
            log.error("Error while clicking Select Doc Type. " + e.getMessage());
            throw new Exception("Error while clicking Select Doc Type. " + e.getMessage());
        }
    }

    public void inputSelectDocTypeFilter(String filterText) throws Exception {
        log.info("Inputting Doc Type Filter.");
        try {
            WebElement selectDocTypeFilter = indexDocumentsPageObjects.getSelectDocTypeFilter();
            type(selectDocTypeFilter, filterText);
        } catch (Exception e) {
            log.error("Error while inputting Select Doc Type Filter. " + e.getMessage());
            throw new Exception("Error while inputting Select Doc Type Filter. " + e.getMessage());
        }
    }

    public void selectDocType(String targetDocType) throws Exception {
        log.info("Clicking On The Exact Doc Type.");
        try {
            WebElement selectDocTypeItems = indexDocumentsPageObjects.getSelectDocTypeItems();
            selectExactMatchingDataLabel(selectDocTypeItems, targetDocType);
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while clicking Exact Doc Type. " + e.getMessage());
            throw new Exception("Error while clicking Exact Doc Type. " + e.getMessage());
        }
    }

    public void clickSelectDocStatus() throws Exception {
        log.info("Clicking Select Doc Status.");
        try {
            // TODO: Add An Explicit Wait For The Element Before Clicking
            WebElement selectDocStatus = indexDocumentsPageObjects.getSelectDocStatus();
            click(selectDocStatus);
        } catch (Exception e) {
            log.error("Error while clicking Select Doc Status. " + e.getMessage());
            throw new Exception("Error while clicking Select Doc Status. " + e.getMessage());
        }
    }

    public void inputSelectDocStatusFilter(String filterText) throws Exception {
        log.info("Inputting Select Doc Status Filter.");
        try {
            WebElement selectDocStatusFilter = indexDocumentsPageObjects.getSelectDocStatusFilter();
            type(selectDocStatusFilter, filterText);
        } catch (Exception e) {
            log.error("Error while inputting Select Doc Type Status. " + e.getMessage());
            throw new Exception("Error while inputting Select Doc Type Status. " + e.getMessage());
        }
    }

    public void selectDocStatus(String targetDocStatus) throws Exception {
        log.info("Clicking On The Exact Doc Status.");
        try {
            WebElement selectDocStatusItems = indexDocumentsPageObjects.getSelectDocStatusItems();
            selectExactMatchingDataLabel(selectDocStatusItems, targetDocStatus);
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while clicking Exact Doc Status. " + e.getMessage());
            throw new Exception("Error while clicking Exact Doc Status. " + e.getMessage());
        }
    }

    public void clickSelectWorkTypes() throws Exception {
        log.info("Clicking On Select Work Types.");
        try {
            // TODO: Add An Explicit Wait For The Element Before Clicking
            WebElement selectWorkTypes = indexDocumentsPageObjects.getSelectWorkTypes();
            click(selectWorkTypes);
        } catch (Exception e) {
            log.error("Error while clicking Select Work Types. " + e.getMessage());
            throw new Exception("Error while clicking Select Work Types. " + e.getMessage());
        }
    }

    public void inputSelectWorkTypesFilter(String filterText) throws Exception {
        log.info("Inputting Select Work Types Filter.");
        try {
            WebElement selectWorkTypesFilter = indexDocumentsPageObjects.getSelectWorkTypesFilter();
            type(selectWorkTypesFilter, filterText);
        } catch (Exception e) {
            log.error("Error while inputting Select Work Types. " + e.getMessage());
            throw new Exception("Error while inputting Select Work Types. " + e.getMessage());
        }
    }

    public void selectWorkTypes(String targetWorkType) throws Exception {
        log.info("Clicking Exact Work Type.");
        try {
            WebElement selectWorkTypesItems = indexDocumentsPageObjects.getSelectWorkTypesItems();
            selectExactMatchingDataLabel(selectWorkTypesItems, targetWorkType);
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while clicking Exact Work Type. " + e.getMessage());
            throw new Exception("Error while clicking Exact Work Type. " + e.getMessage());
        }
    }

    public void clickSelectQueues() throws Exception {
        log.info("Clicking On Select Work Queue.");
        try {
            // TODO: Add An Explicit Wait For The Element Before Clicking
            WebElement selectQueues = indexDocumentsPageObjects.getSelectQueues();
            click(selectQueues);
        } catch (Exception e) {
            log.error("Error while clicking Select Queues. " + e.getMessage());
            throw new Exception("Error while clicking Select Queues. " + e.getMessage());
        }
    }

    public void inputSelectQueuesFilter(String filterText) throws Exception {
        log.info("Inputting Select Work Queue Filter.");
        try {
            WebElement selectQueuesFilter = indexDocumentsPageObjects.getSelectQueuesFilter();
            type(selectQueuesFilter, filterText);
        } catch (Exception e) {
            log.error("Error while inputting Select Queues. " + e.getMessage());
            throw new Exception("Error while inputting Select Queues. " + e.getMessage());
        }
    }

    public void selectQueues(String targetQueue) throws Exception {
        log.info("Clicking On The Exact Work Queue.");
        try {
            Thread.sleep(2000); // Note: This Takes some time to be attached.
            WebElement selectQueuesItems = indexDocumentsPageObjects.getSelectQueuesItems();
            selectExactMatchingDataLabel(selectQueuesItems, targetQueue);
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while clicking Exact Queue. " + e.getMessage());
            throw new Exception("Error while clicking Exact Queues. " + e.getMessage());
        }
    }

    public void inputItemPolicyNumber(String itemPolicyNumber) throws Exception {
        log.info("Inputting Policy Number.");
        try {
            WebElement itemPolicyNumberField = indexDocumentsPageObjects.getItemPolicyNumber();
            type(itemPolicyNumberField, itemPolicyNumber);
        } catch (Exception e) {
            log.error("Error while inputting Item Policy Number. " + e.getMessage());
            throw new Exception("Error while inputting Item Policy Number. " + e.getMessage());
        }
    }

    public void clickDocumentCheckBoxesAll() throws Exception {
        log.info("Clicking On Document Check Boxes All.");
        try {
            WebElement documentCheckBoxesAll = indexDocumentsPageObjects.getDocumentCheckBoxesAll();
            click(documentCheckBoxesAll);
        } catch (Exception e) {
            log.error("Error while clicking Document Check Boxes All. " + e.getMessage());
            throw new Exception("Error while clicking Document Check Boxes All. " + e.getMessage());
        }
    }

    public void clickAddItem() throws Exception {
        log.info("Clicking On Add Item.");
        try {
            WebElement addItem = indexDocumentsPageObjects.getAddItem();
            click(addItem);
            Thread.sleep(2000);
        } catch (Exception e) {
            log.error("Error while clicking Add Item. " + e.getMessage());
            throw new Exception("Error while clicking Add Item. " + e.getMessage());
        }
    }

    public void clickIndex() throws Exception {
        log.info("Clicking On Index.");
        try {
            WebElement addItem = indexDocumentsPageObjects.getIndex();
            click(addItem);
            Thread.sleep(2000);
        } catch (Exception e) {
            log.error("Error while clicking Index. " + e.getMessage());
            throw new Exception("Error while clicking Index. " + e.getMessage());
        }
    }

    public void switchBackToDashboard() throws Exception {
        log.info("Switching Back To Dashboard.");
        try {
            switchToAnfMaximizeServiceWindow(0);
        } catch (Exception e) {
            log.error("Error while switching Back To Dashboard.");
            throw new Exception("Error while switching Back To Dashboard.");
        }
    }

    public IndexDocuments closeIndexDocumentsWindow() throws Exception {
        log.info("Closing Index Documents Window.");
        try {
            switchToAndCloseServiceWindow(1);
            return new IndexDocuments(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while closing Index Documents Window.");
            throw new Exception("Error while closing Index Documents Window.");
        }
    }

    public void closeIndexDocumentsOpenWindow(int index) throws Exception {
        log.info("Closing Index Documents Window At Index " + index);
        try {
            switchToAndCloseServiceWindow(index);
        } catch (Exception e) {
            log.error("Error while closing Index Documents Window At Index " + index);
            throw new Exception("Error while closing Index Documents Window At Index " + index);
        }
    }

}
