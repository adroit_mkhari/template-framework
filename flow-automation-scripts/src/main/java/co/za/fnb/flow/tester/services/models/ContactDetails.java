package co.za.fnb.flow.tester.services.models;

public class ContactDetails {
    private String email;
    private String cellNumber;
    private String homeNumber;
    private String postalAddressStreet;
    private String postalAddressSuburb;
    private String postalAddresCity;
    private String postalAddressCode;

    public ContactDetails() {
    }

    public ContactDetails(String email, String cellNumber, String homeNumber, String postalAddressStreet, String postalAddressSuburb, String postalAddresCity, String postalAddressCode) {
        this.email = email;
        this.cellNumber = cellNumber;
        this.homeNumber = homeNumber;
        this.postalAddressStreet = postalAddressStreet;
        this.postalAddressSuburb = postalAddressSuburb;
        this.postalAddresCity = postalAddresCity;
        this.postalAddressCode = postalAddressCode;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getPostalAddressStreet() {
        return postalAddressStreet;
    }

    public void setPostalAddressStreet(String postalAddressStreet) {
        this.postalAddressStreet = postalAddressStreet;
    }

    public String getPostalAddressSuburb() {
        return postalAddressSuburb;
    }

    public void setPostalAddressSuburb(String postalAddressSuburb) {
        this.postalAddressSuburb = postalAddressSuburb;
    }

    public String getPostalAddresCity() {
        return postalAddresCity;
    }

    public void setPostalAddresCity(String postalAddresCity) {
        this.postalAddresCity = postalAddresCity;
    }

    public String getPostalAddressCode() {
        return postalAddressCode;
    }

    public void setPostalAddressCode(String postalAddressCode) {
        this.postalAddressCode = postalAddressCode;
    }
}
