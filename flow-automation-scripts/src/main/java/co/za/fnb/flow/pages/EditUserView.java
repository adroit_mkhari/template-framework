package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.EditUserViewPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;

public class EditUserView extends BasePage {
    EditUserViewPageObjects editUserViewPageObjects = new EditUserViewPageObjects(driver);

    public EditUserView(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
