package co.za.fnb.flow.pages;

import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.claims.Claims;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.generic_payment.GenericPayment;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.root_page_factory.CreateSearchItemPageObjects;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static java.util.concurrent.TimeUnit.SECONDS;

public class CreateSearchItemPage extends BasePage {
    private Logger log  = LogManager.getLogger(CreateSearchItemPage.class);
    CreateSearchItemPageObjects createSearchItemPageObjects = new CreateSearchItemPageObjects(driver, scenarioOperator);

    public CreateSearchItemPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clear() throws Exception {
        log.info("Clicking Clear");
        click(createSearchItemPageObjects.getClear());
    }

    public void search() throws Exception {
        log.info("Clicking Search.");
        WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
        WebElement search = createSearchItemPageObjects.getSearch();
        webDriverWait.until(ExpectedConditions.visibilityOf(search));
        click(search);
    }

    public void searchPolicyNumber(String policyNumber, boolean clearWorkItems) throws Exception {
        log.info("Searching By Policy Number");
        Actions actions = new Actions(driver);
        try {
            driver.manage().timeouts().pageLoadTimeout(1, SECONDS);
            Thread.sleep(3000);
            actions.moveToElement(createSearchItemPageObjects.getWorkItems()).doubleClick().build().perform();
            Thread.sleep(3000);
            type(createSearchItemPageObjects.getSearchPolicyNumber(), policyNumber);
            Thread.sleep(3000);
            search();
            Thread.sleep(3000);
            getResponses(false);
            // Clear the work items before performing any actions.
            // TODO: Inquire about it clearing everything when you clear work items.
            /*if (clearWorkItems) {
                log.info("Clearing Work Items");
                clear();
            }*/
            Thread.sleep(5000); // TODO: Make this a variable that can be easily changed when performance is down.
        } catch (Exception e) {
            log.error("Error while trying to search by policy number: " + e.getMessage());
            e.printStackTrace();
            throw new Exception("Error while trying to search by policy number: " + e.getMessage());
        }
    }

    public String getRetentionsMessage() throws Exception {
        try {
            WebElement retentionsMessageField = createSearchItemPageObjects.getRetentionsMessage();
            String retentionsMessage = retentionsMessageField.getText();
            return retentionsMessage;
        } catch (Exception e) {
            log.error("Error while getting Retentions Message: " + e.getMessage());
            throw new Exception("Error while getting Retentions Message: " + e.getMessage());
        }
    }

    public void searchPolicyNumberAndAvoidResponses(String policyNumber, boolean clearWorkItems) throws Exception {
        log.info("Searching By Policy Number");
        Actions actions = new Actions(driver);
        try {
            driver.manage().timeouts().pageLoadTimeout(1, SECONDS);
            Thread.sleep(2000);
            actions.moveToElement(createSearchItemPageObjects.getWorkItems()).doubleClick().build().perform();
            Thread.sleep(1000);
            type(createSearchItemPageObjects.getSearchPolicyNumber(), policyNumber);
            Thread.sleep(1000);
            search();
            Thread.sleep(5000);
        } catch (Exception e) {
            log.error("Error while trying to search by policy number: " + e.getMessage());
            e.printStackTrace();
            throw new Exception("Error while trying to search by policy number: " + e.getMessage());
        }
    }

    public void selectValidationCategory(String category) throws Exception {
        log.info("Selecting Validation Category.");
        try {
            WebElement categorySelectLabel = createSearchItemPageObjects.getCategorySelectLabel();
            WebElement categorySelectItems = createSearchItemPageObjects.getCategorySelectItems();
            selectExactMatchingDataLabel(categorySelectLabel, categorySelectItems, category);
        } catch (Exception e) {
            log.error("Error while Selecting Validation Category.");
            throw new Exception("Error while Selecting Validation Category.");
        }
    }

    public void selectValidationSubCategory(String category) throws Exception {
        log.info("Selecting Validation Sub Category.");
        try {
            WebElement categorySelectLabel = createSearchItemPageObjects.getSubCategorySelectLabel();
            WebElement categorySelectItems = createSearchItemPageObjects.getSubCategorySelectItems();
            selectExactMatchingDataLabel(categorySelectLabel, categorySelectItems, category);
        } catch (Exception e) {
            log.error("Error while Selecting Validation Sub Category.");
            throw new Exception("Error while Selecting Validation Sub Category.");
        }
    }

    public String getPolicyInAreas() throws Exception {
        return getText(createSearchItemPageObjects.getPolicyInArrears());
    }

    public String getPolicyHolder() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getPolicyHolder(), "value");
        } catch (Exception e) {
            log.error("Error while getting policy holder value attribute");
            e.printStackTrace();
            throw new Exception("Error while getting policy holder value attribute");
        }
    }

    public String getPolicyStatus() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getPolicyStatus(), "value");
        } catch (Exception e) {
            log.error("Error while getting policy status value attribute");
            e.printStackTrace();
            throw new Exception("Error while getting status holder value attribute");
        }
    }

    public String getProductName() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getProductName(), "value");
        } catch (Exception e) {
            log.error("Error while getting product name value attribute");
            e.printStackTrace();
            throw new Exception("Error while getting product name value attribute");
        }
    }

    public String getInceptionDate() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getInceptionDate(), "value");
        } catch (Exception e) {
            log.error("Error while getting inception date value attribute");
            e.printStackTrace();
            throw new Exception("Error while getting inception date value attribute");
        }
    }

    public String getPolicyInArrears() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getPolicyInArrears(), "value");
        } catch (Exception e) {
            log.error("Error while getting policy in arrears value attribute");
            e.printStackTrace();
            throw new Exception("Error while getting policy in arrears value attribute");
        }
    }

    public String getPremiumStatus() throws Exception {
        try {
            return createSearchItemPageObjects.getPremiumStatus().getText();
        } catch (Exception e) {
            log.error("Error while getting premium status text");
            e.printStackTrace();
            throw new Exception("Error while getting premium status text");
        }
    }

    public PolicyDetails openPolicyDetails() throws Exception {
        log.info("Opening on Policy Details");
        Actions actions = new Actions(driver);
        try {
            log.info("Clicking on Policy Details");
            createSearchItemPageObjects.getPolicyDetails().click();
            Thread.sleep(2000);
            // Note: Decide between if and while on this case
            int count = 0;
            int retries = 0;
            while (!driver.findElement(By.id("verifyClientDlg")).isDisplayed() && retries < 3) {
                log.info("Retrying click action on Policy_Details");
                if (retries == 2) {
                    log.error("Failing Test Because it can not open Policy Details.");
                    throw new Exception("Failing Test Because it can not open Policy Details.");
                }
                if (count < 10) {
                    actions.moveToElement(createSearchItemPageObjects.getPolicyDetails()).doubleClick().build().perform() ;
                    Thread.sleep(5000);
                } else {
                    // TODO: Remove this block of code.
                    count = 0;
                    // commonSeleniumTester.getDriver().close();
                    // openSearchWindow();
                    // searchByPolicyNumber(true);
                    Thread.sleep(2000);
                    // log.debug("Clicking on policy Details");
                    createSearchItemPageObjects.getPolicyDetails().click();
                    Thread.sleep(5000);
                }
                Thread.sleep(1000);
                count++;
                retries++;
            }
        } catch (Exception e) {
            log.error("Failing Test Because it can not open Policy Details.");
            e.printStackTrace();
            throw new Exception("Failing Test Because it can not open Policy Details.");
        }

        return new PolicyDetails(driver, scenarioOperator);
    }

    private void openServicePage(WebElement servicePage, boolean isOnNewWindow) throws Exception {
        Thread.sleep(2000);
        if (isOnNewWindow) {
            Actions actions = new Actions(driver);
            try {
                servicePage.click();
                Thread.sleep(10000);
                int count = 0;
                int retries = 0;
                WebDriverWait webDriverWait = new WebDriverWait(driver, 120);
                webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.id("verifyClientDlg")));
                while (!driver.findElement(By.id("verifyClientDlg")).isDisplayed() && retries < 3) {
                    if (retries == 2) {
                        log.error("Failing Test Because it can not open Service Page.");
                        throw new ServicePageException("Failing Test Because it can not open Service Page.");
                    }
                    if (count < 10) {
                        actions.moveToElement(servicePage).doubleClick().build().perform() ;
                        Thread.sleep(5000);
                    } else {
                        // TODO: Remove this block of code.
                        count = 0;
                        // commonSeleniumTester.getDriver().close();
                        // openSearchWindow();
                        // searchByPolicyNumber(true);
                        Thread.sleep(2000);
                        // log.debug("Clicking on policy Details");
                        servicePage.click();
                        Thread.sleep(5000);
                    }
                    Thread.sleep(1000);
                    count++;
                    retries++;
                }
            } catch (Exception e) {
                log.error("Failing Test Because it can not open Service Page.");
                e.printStackTrace();
                throw new ServicePageException("Failing Test Because it can not open Service Page.");
            }
        } else {
            try {
                servicePage.click();
                Thread.sleep(10000);
            } catch (Exception e) {
                log.error("Failing Test Because it can not open Service Page.");
                e.printStackTrace();
                throw new ServicePageException("Failing Test Because it can not open Service Page.");
            }
        }
    }

    public WorkItems openWorkItems(boolean isOnNewWindow) throws Exception {
        try {
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            WebElement workItems = createSearchItemPageObjects.getWorkItems();
            webDriverWait.until(ExpectedConditions.visibilityOf(workItems));
            openServicePage(workItems, isOnNewWindow);
            Thread.sleep(2000);
            return new WorkItems(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Work Items Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Work Items Page");
        }
    }

    public PolicyDetails openPolicyDetails(boolean isOnNewWindow) throws Exception {
        try {
            openServicePage(createSearchItemPageObjects.getPolicyDetails(), isOnNewWindow);
            return new PolicyDetails(driver, scenarioOperator);
        } catch (Exception e) {
            takeScreenShot("");
            log.error("Error while opening Policy Details Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Policy Details Page");
        }
    }

    public AuditTrail openAuditTrail(boolean isOnNewWindow) throws Exception {
        try {
            WebDriverWait webDriverWait = new WebDriverWait(driver, 90);
            WebElement auditTrail = createSearchItemPageObjects.getAuditTrail();
            webDriverWait.until(ExpectedConditions.visibilityOf(auditTrail));
            openServicePage(auditTrail, isOnNewWindow);
            return new AuditTrail(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Audit Trail Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Audit Trail Page");
        }
    }

    public Claims openClaims(boolean isOnNewWindow) throws Exception {
        try {
            openServicePage(createSearchItemPageObjects.getClaims(), isOnNewWindow);
            return new Claims(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Claims Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Claims Page");
        }
    }

    public GenericPayment openGenericPayment(boolean isOnNewWindow) throws Exception {
        try {
            openServicePage(createSearchItemPageObjects.getGenericPayment(), isOnNewWindow);
            return new GenericPayment(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Generic Payment Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Generic Payment Page");
        }
    }

    public GenericMaintenance openGenericMaintenance(boolean isOnNewWindow) throws Exception {
        try {
            openServicePage(createSearchItemPageObjects.getGenericMaintenance(), isOnNewWindow);
            return new GenericMaintenance(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Generic Maintenance");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Generic Maintenance");
        }
    }

    public PrincipalMemberDetails openPrincipalMemberDetails(boolean isOnNewWindow) throws Exception {
        try {
            openServicePage(createSearchItemPageObjects.getPrincipalMemberDetails(), isOnNewWindow);
            return new PrincipalMemberDetails(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while opening Principal Member Details Page");
            e.printStackTrace();
            throw new ServicePageException("Error while opening Principal Member Details Page");
        }
    }

    public String getTotalPremiumAmount() throws Exception {
        try {
            return getAttribute(createSearchItemPageObjects.getTotalPremiumAmount(), "value");
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while getting total premium amount: " + e.getMessage());
            throw new Exception("Error while getting total premium amount: " + e.getMessage());
        }
    }

    public void clickUpdate() throws Exception {
        try {
            click(createSearchItemPageObjects.getUpdate());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking update work item.");
            throw new Exception("Error while clicking update work item.");
        }
    }

    public void clickCreate() throws Exception {
        try {
            click(createSearchItemPageObjects.getCreate());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking create work item.");
            throw new Exception("Error while clicking create work item.");
        }
    }

    public void clickClone() throws Exception {
        try {
            click(createSearchItemPageObjects.getClone());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking clone work item.");
            throw new Exception("Error while clicking clone work item.");
        }
    }

    public void clickDocuments() throws Exception {
        try {
            click(createSearchItemPageObjects.getDocuments());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking Documents.");
            throw new Exception("Error while clicking Documents.");
        }
    }

    public void moveToAndDoubleClickTheFirstPolicyDocument() throws Exception {
        try {
            moveToElementAndDoubleClick(createSearchItemPageObjects.getFirstPolicyDocumentTableDragIcon());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking First Policy Document Table Drag Icon.");
            throw new Exception("Error while clicking First Policy Document Table Drag Icon.");
        }
    }

    public void switchToWindowAndMaximize(int index) throws Exception {
        try {
            switchToAnfMaximizeServiceWindow(index);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while moving to and Maximize window at index " + index);
            throw new Exception("Error while moving to and Maximize window at index " + index);
        }
    }

    public void switchToAndCloseWindow(int index) throws Exception {
        try {
            switchToAndCloseServiceWindow(index);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while moving to and closing window at index " + index);
            throw new Exception("Error while moving to and closing window at index " + index);
        }
    }

    public void refreshServicePage(int index) throws Exception {
        log.info("Refreshing Service Page");
        try {
            refreshPage(index);
        } catch (Exception e) {
            log.error("Error while Refreshing Service Page.");
            throw new Exception("Error while Refreshing Service Page.");
        }
    }

}
