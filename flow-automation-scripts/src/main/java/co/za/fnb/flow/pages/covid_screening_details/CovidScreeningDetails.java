package co.za.fnb.flow.pages.covid_screening_details;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.covid_screening_details.page_factory.CovidScreeningDetailsPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class CovidScreeningDetails extends BasePage {
    private Logger log  = LogManager.getLogger(CovidScreeningDetails.class);
    CovidScreeningDetailsPageObjects covidScreeningDetailsPageObjects = new CovidScreeningDetailsPageObjects(driver);

    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public CovidScreeningDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void retrieveNewClaim() throws Exception {
        try {
            log.info("Retrieving New Claim.");
            WebElement searchNewClaim = covidScreeningDetailsPageObjects.getSearchNewClaim();
            click(searchNewClaim);
        } catch (Exception e) {
            log.error("Error while retrieving new claim.");
            throw new Exception("Error while retrieving new claim.");
        }
    }

    public String getMessageBoxText() throws Exception {
        try {
            log.info("Getting Message Box Text.");
            WebElement messageBoxText = covidScreeningDetailsPageObjects.getMessageBoxText();
            String text = messageBoxText.getText();
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Message Box Text: " + e.getMessage());
            // throw new Exception("Error while Getting Message Box Text.");
            return "";
        }
    }

    public String getWorkItemID() throws Exception {
        try {
            log.info("Getting Work Item ID.");
            WebElement workItemID = covidScreeningDetailsPageObjects.getWorkItemID();
            String text = getAttribute(workItemID, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Work Item ID.");
            throw new Exception("Error while Getting Work Item ID.");
        }
    }

    public String getClientName() throws Exception {
        try {
            log.info("Getting Client Name.");
            WebElement clientName = covidScreeningDetailsPageObjects.getClientName();
            String text = getAttribute(clientName, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Client Name.");
            throw new Exception("Error while Getting Client Name.");
        }
    }

    public String getClaimEffectiveDate() throws Exception {
        try {
            log.info("Getting Claim Effective Date.");
            WebElement claimEffectiveDate = covidScreeningDetailsPageObjects.getClaimEffectiveDate();
            String text = getAttribute(claimEffectiveDate, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Claim Effective Date.");
            throw new Exception("Error while Getting Claim Effective Date.");
        }
    }

    public String getCoverStartDate() throws Exception {
        try {
            log.info("Getting Cover Start Date.");
            WebElement coverStartDate = covidScreeningDetailsPageObjects.getCoverStartDate();
            String text = getAttribute(coverStartDate, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Cover Start Date.");
            throw new Exception("Error while Getting Cover Start Date.");
        }
    }

    public String getPolicyNumber() throws Exception {
        try {
            log.info("Getting Policy Number.");
            WebElement policyNumber = covidScreeningDetailsPageObjects.getPolicyNumber();
            String text = getAttribute(policyNumber, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Policy Number.");
            throw new Exception("Error while Getting Policy Number.");
        }
    }

    public String getClientIDNo() throws Exception {
        try {
            log.info("Getting Client ID No.");
            WebElement clientIDNo = covidScreeningDetailsPageObjects.getClientIDNo();
            String text = getAttribute(clientIDNo, "value");
            return text;
        } catch (Exception e) {
            log.error("Error while Getting Client ID No.");
            throw new Exception("Error while Getting Client ID No.");
        }
    }

    public void getDocumentReviewed() throws Exception {
        try {
            log.info("Clicking Document Reviewed.");
            WebElement documentReviewed = covidScreeningDetailsPageObjects.getDocumentReviewed();
            click(documentReviewed);
        } catch (Exception e) {
            log.error("Error while Clicking Document Reviewed.");
            throw new Exception("Error while Clicking Document Reviewed.");
        }
    }

    public void inputEmployerName(String employerName) throws Exception {
        try {
            log.info("Updating employer's Name.");
            WebElement employerNameField = covidScreeningDetailsPageObjects.getEmployerName();
            clear(employerNameField);
            type(employerNameField, employerName);
        } catch (Exception e) {
            log.error("Error while Updating employer's Name.");
            throw new Exception("Error while Updating employer's Name.");
        }
    }

    public void clickEditEmployerButton() throws Exception {
        try {
            log.info("Clicking Edit Employer Button.");
            WebElement documentReviewed = covidScreeningDetailsPageObjects.getEditEmployerButton();
            click(documentReviewed);
        } catch (Exception e) {
            log.error("Error while Clicking Edit Employer Button.");
            throw new Exception("Error while Clicking Edit Employer Button.");
        }
    }

    public void selectCategory(String category) throws Exception {
        try {
            log.info("Selecting Category.");
            WebElement categorySelectLabel = covidScreeningDetailsPageObjects.getCategorySelectLabel();
            WebElement categorySelectItems = covidScreeningDetailsPageObjects.getCategorySelectItems();
            selectExactMatchingDataLabelWithFilter(categorySelectLabel, categorySelectItems, category, "customerSearchCOVID:subCategory_filter");
        } catch (Exception e) {
            log.error("Error while Selecting Category.");
            throw new Exception("Error while Selecting Category.");
        }
    }

    public void selectSubCategory(String subCategory) throws Exception {
        try {
            log.info("Selecting Sub Category.");
            WebElement subCategorySelectLabel = covidScreeningDetailsPageObjects.getSubCategorySelectLabel();
            WebElement subCategorySelectItems = covidScreeningDetailsPageObjects.getSubCategorySelectItems();
            selectExactMatchingDataLabelWithFilter(subCategorySelectLabel, subCategorySelectItems, subCategory, "customerSearchCOVID:subCategory_filter");
        } catch (Exception e) {
            log.error("Error while Selecting Sub Category.");
            throw new Exception("Error while Selecting Sub Category.");
        }
    }

    public void inputComment(String comment) throws Exception {
        try {
            log.info("Updating Comment.");
            WebElement commentField = covidScreeningDetailsPageObjects.getComment();
            type(commentField, comment);
        } catch (Exception e) {
            log.error("Error while Updating Comment.");
            throw new Exception("Error while Updating Comment.");
        }
    }

    public void clickPolicySubmitButton() throws Exception {
        try {
            log.info("Clicking Policy Submit Button.");
            WebElement policySubmitButton = covidScreeningDetailsPageObjects.getPolicySubmitButton();
            click(policySubmitButton);
        } catch (Exception e) {
            log.error("Error while Clicking Policy Submit Button.");
            throw new Exception("Error while Clicking Policy Submit Button.");
        }
    }

    public void maximizePage() throws Exception {
        log.info("Maximizing Covid Screening Page");
        try {
            switchToAnfMaximizeServiceWindow(0);
        } catch (Exception e) {
            log.error("Error while Maximizing Covid Screening Page.");
            throw new Exception("Error while Maximizing Covid Screening Page.");
        }
    }

    public void refreshPage() throws Exception {
        log.info("Refreshing Covid Screening Page");
        try {
            refreshPage(0);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("Error while Refreshing Covid Screening Page.");
            throw new Exception("Error while Refreshing Covid Screening Page.");
        }
    }

    @Override
    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    @Override
    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    @Override
    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    @Override
    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}
