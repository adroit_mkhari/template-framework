package co.za.fnb.flow.pages.policy_details.information;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.policy_details.page_factory.ReinstatePolicyPopupDialogPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ReinstatePolicyPopupDialog extends PolicyDetails {
    private Logger log  = LogManager.getLogger(ReinstatePolicyPopupDialog.class);
    ReinstatePolicyPopupDialogPageObjects reinstatePolicyPageObjects = new ReinstatePolicyPopupDialogPageObjects(driver);

    public ReinstatePolicyPopupDialog(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void yesCalculateArrears() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getYesCalculateArrears());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Calculate Arrears.");
            throw new Exception("Error while clicking on Yes Calculate Arrears.");
        }
    }

    public void yesContinue() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getYesContinue());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Continue.");
            throw new Exception("Error while clicking on Yes Continue.");
        }
    }

    public void clearDebitOrderDate() throws Exception {
        try {
            clear(reinstatePolicyPageObjects.getDebitOrderDate());
        } catch (Exception e) {
            log.error("Error while clearing Debit Order Date.");
            throw new Exception("Error while clearing Debit Order Date.");
        }
    }

    public void updateDebitOrderDate(String debitOrderDate) throws Exception {
        try {
            clearDebitOrderDate();
            type(reinstatePolicyPageObjects.getDebitOrderDate(), debitOrderDate);
        } catch (Exception e) {
            log.error("Error while updating Debit Order Date.");
            throw new Exception("Error while updating Debit Order Date.");
        }
    }

    public void clearNextDueDate() throws Exception {
        try {
            clear(reinstatePolicyPageObjects.getNextDueDate());
        } catch (Exception e) {
            log.error("Error while clearing Next Due Date.");
            throw new Exception("Error while clearing Next Due Date.");
        }
    }

    public void updateNextDueDate(String debitOrderDate) throws Exception {
        try {
            clearNextDueDate();
            type(reinstatePolicyPageObjects.getNextDueDate(), debitOrderDate);
        } catch (Exception e) {
            log.error("Error while updating Next Due Date.");
            throw new Exception("Error while updating Next Due Date.");
        }
    }

    public void yesReinstateWithArrears() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getYesReinstateWithArrears());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Reinstate With Arrears.");
            throw new Exception("Error while clicking on Yes Reinstate With Arrears.");
        }
    }

    public void noReinstatePolicy() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getNoReinstatePolicy());
        } catch (Exception e) {
            log.error("Error while clicking on No Reinstate.");
            throw new Exception("Error while clicking on No Reinstate.");
        }
    }

    public void yesReinstatePolicy() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getYesReinstatePolicy());
        } catch (Exception e) {
            log.error("Error while clicking on Yes Reinstate Policy.");
            throw new Exception("Error while clicking on Yes Reinstate Policy.");
        }
    }

    public void deleteCommentsViewButton() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getDeleteCommentsViewButton());
        } catch (Exception e) {
            log.error("Error while clicking on Delete Comments View Button.");
            throw new Exception("Error while clicking on Delete Comments View Button.");
        }
    }

    public void deleteCommentsView() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getDeleteCommentsView());
        } catch (Exception e) {
            log.error("Error while clicking on Delete Comments View.");
            throw new Exception("Error while clicking on Delete Comments View.");
        }
    }

    public void noHide() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getNoHide());
        } catch (Exception e) {
            log.error("Error while clicking on No Hide.");
            throw new Exception("Error while clicking on No Hide.");
        }
    }

    public void noCalculateArrears() throws Exception {
        try {
            click(reinstatePolicyPageObjects.getNoCalculateArrears());
        } catch (Exception e) {
            log.error("Error while clicking on No Calculate Arrears.");
            throw new Exception("Error while clicking on No Calculate Arrears.");
        }
    }

}
