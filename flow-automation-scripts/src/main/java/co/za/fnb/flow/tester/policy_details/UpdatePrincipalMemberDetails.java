package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.policy_details.PolicyInformationModel;
import co.za.fnb.flow.models.policy_details.PrincipalMember;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.CreateSearchItemPage;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.principal_member_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;

public class UpdatePrincipalMemberDetails {
    private Logger log  = LogManager.getLogger(UpdatePrincipalMemberDetails.class);
    WebDriver driver;
    PrincipalMember principalMember;
    PolicyInformation policyInformation;
    ReinstatePolicyPopupDialog reinstatePolicyPopupDialog;
    BeneficiaryInformation beneficiaryInformation;
    MemberInformation memberInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    QueryHandler queryHandler = new QueryHandler();
    PolicyInformationModel policyInformationModel;
    PrincipalMemberDetails principalMemberDetails;
    PrincipalMemberPersonalInformation principalMemberPersonalInformation;
    PrincipalMemberPersonalInformationLoanCall principalMemberPersonalInformationLoanCall;
    PrincipalMemberResidentialInformation principalMemberResidentialInformation;
    PrincipalMemberContactInformation principalMemberContactInformation;
    PrincipalMemberPostalInformation principalMemberPostalInformation;
    PrincipalMemberPostalInformationAddressSearch principalMemberPostalInformationAddressSearch;

    PolicyDetails policyDetails;
    CreateSearchItemPage createSearchItemPage;
    private String fNumberText;

    public UpdatePrincipalMemberDetails(WebDriver driver, PrincipalMember principalMember, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.principalMember = principalMember;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        reinstatePolicyPopupDialog = new ReinstatePolicyPopupDialog(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
        principalMemberDetails = new PrincipalMemberDetails(driver, scenarioOperator);
        principalMemberPersonalInformation = new PrincipalMemberPersonalInformation(driver, scenarioOperator);
        principalMemberPersonalInformationLoanCall = new PrincipalMemberPersonalInformationLoanCall(driver, scenarioOperator);
        principalMemberResidentialInformation = new PrincipalMemberResidentialInformation(driver, scenarioOperator);
        principalMemberContactInformation = new PrincipalMemberContactInformation(driver, scenarioOperator);
        principalMemberPostalInformation = new PrincipalMemberPostalInformation(driver, scenarioOperator);
        principalMemberPostalInformationAddressSearch = new PrincipalMemberPostalInformationAddressSearch(driver, scenarioOperator);
        createSearchItemPage = new CreateSearchItemPage(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(principalMember.getExpectResult());
    }

    public void update() throws Exception {
        String productName = principalMember.getProductName();
        String principalMemberInformationToUpdate = principalMember.getPrincipalMemberInformationToUpdate();

        if (principalMemberInformationToUpdate.equalsIgnoreCase("Personal Information")) {
            String policyHolder = principalMember.getPolicyHolder();
            String tradingAsNameOrMiddleName = principalMember.getTradingAsNameOrMiddleName();
            String companyRegNumberOrIdNumber = principalMember.getCompanyRegNumberOrIdNumber();
            String vatRegNumber = principalMember.getVatRegNumber();
            String typeOfId = principalMember.getTypeOfId();
            String income = principalMember.getIncome();
            String fNumber = principalMember.getfNumber();
            String expectedPremiumAmountValue = principalMember.getExpectedPremiumAmountValue();
            String dateOfBirth = principalMember.getDateOfBirth();
            String gender = principalMember.getGender();
            String correspondenceLanguage = principalMember.getCorrespondenceLanguage();
            String maritalStatus = principalMember.getMaritalStatus();

            principalMemberDetails.clickPersonalInformation();

            if (!policyHolder.isEmpty()) {
                principalMemberPersonalInformation.clearPolicyHolder();
                principalMemberPersonalInformation.updatePolicyHolder(policyHolder);
            }

            if (!tradingAsNameOrMiddleName.isEmpty()) {
                principalMemberPersonalInformation.clearMiddleName();
                principalMemberPersonalInformation.updateMiddleName(tradingAsNameOrMiddleName);
            }

            if (productName.equalsIgnoreCase("LOC-Business")) {
                if (!companyRegNumberOrIdNumber.isEmpty()) {
                    principalMemberPersonalInformationLoanCall.clearIdNumberCompany();
                    principalMemberPersonalInformationLoanCall.updateIdNumberCompany(companyRegNumberOrIdNumber);
                }

                if (!vatRegNumber.isEmpty()) {
                    principalMemberPersonalInformationLoanCall.clearVatNumber();
                    principalMemberPersonalInformationLoanCall.updateVatNumber(vatRegNumber);
                }
            } else {
                if (!companyRegNumberOrIdNumber.isEmpty()) {
                    principalMemberPersonalInformationLoanCall.clearIdNumberCompany();
                    principalMemberPersonalInformationLoanCall.updateIdNumberCompany(companyRegNumberOrIdNumber);
                }
            }

            if (!productName.equalsIgnoreCase("LOC-Business")) {
                if (!typeOfId.isEmpty()) {
                    String typeOfIdValue = principalMemberPersonalInformation.getTypeOfId();
                    if (!typeOfIdValue.equalsIgnoreCase(typeOfId)) {
                        // reportHandler.setFailureReason("The expected type of id is not correct. Got " + typeOfIdValue + " instead of " + typeOfId);
                        throw new Exception("The expected type of id is not correct. Got " + typeOfIdValue + " instead of " + typeOfId);
                    }
                }
            }

            if (!productName.equalsIgnoreCase("LOC-Personal")) {
                if (!income.isEmpty()) {
                    principalMemberPersonalInformation.clearIncome();
                    principalMemberPersonalInformation.updateIncome(income);
                }
            } else {
                if (!fNumber.isEmpty()) {
                    principalMemberPersonalInformationLoanCall.clearFNumber();
                    principalMemberPersonalInformationLoanCall.updateFNumber(fNumber);
                    fNumberText = principalMemberPersonalInformationLoanCall.getFNumber();
                }
            }

            if (productName.equalsIgnoreCase("LOC-Personal")) {
                if (!gender.isEmpty()) {
                    // Note: The Gender is a locked field, maybe it can't be changed.
                    // principalMemberPersonalInformationLoanCall.moveToGender();
                    // principalMemberPersonalInformationLoanCall.selectGenderLoc(gender);
                }
            }

            if (!correspondenceLanguage.isEmpty()) {
                principalMemberPersonalInformation.moveToCorrespondenceLanguage();
                principalMemberPersonalInformation.selectCorrespondenceLanguage(correspondenceLanguage);
            }

            if (!maritalStatus.isEmpty()) {
                principalMemberPersonalInformation.moveToMaritalStatus();
                principalMemberPersonalInformation.selectMaritalStatus(maritalStatus);
            }

            // TODO: Save and do Validations
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Residential Information")) {
            String residentialStreetOrBuildingLineOne = principalMember.getResidentialStreetOrBuildingLineOne();
            String residentialStreetOrBuildingLineTwo = principalMember.getResidentialStreetOrBuildingLineTwo();
            String residentialSuburb = principalMember.getResidentialSuburb();
            String residentialCity = principalMember.getResidentialCity();
            String residentialPostalCode = principalMember.getResidentialPostalCode();

            principalMemberDetails.clickResidentialInformation();

            /*if (!residentialStreetOrBuildingLineOne.isEmpty()) {
                principalMemberResidentialInformation.clearStreetLineOne();
                principalMemberResidentialInformation.updateStreetLineOne(residentialStreetOrBuildingLineOne);
            }

            if (!residentialStreetOrBuildingLineTwo.isEmpty()) {
                principalMemberResidentialInformation.clearStreetLineTwo();
                principalMemberResidentialInformation.updateStreetLineTwo(residentialStreetOrBuildingLineTwo);
            }

            if (!residentialSuburb.isEmpty()) {
                principalMemberResidentialInformation.clearStreetSuburb();
                principalMemberResidentialInformation.updateStreetSuburb(residentialSuburb);
            }

            if (!residentialCity.isEmpty()) {
                principalMemberResidentialInformation.clearStreetCity();
                principalMemberResidentialInformation.updateStreetCity(residentialCity);
            }

            if (!residentialPostalCode.isEmpty()) {
                principalMemberResidentialInformation.clearPostalCode();
                principalMemberResidentialInformation.updatePostalCode(residentialPostalCode);
            }*/

            // TODO: Save and do Validations
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Contact Information")) {
            String preferredContactMethod = principalMember.getPreferredContactMethod();
            String emailAddress = principalMember.getEmailAddress();
            String cisEmailAddress = principalMember.getCisEmailAddress();
            String inContactEmailAddress = principalMember.getInContactEmailAddress();
            String cellPhoneNumber = principalMember.getCellPhoneNumber();
            String cisCellPhoneNumber = principalMember.getCisCellPhoneNumber();
            String inContactCellPhoneNumber = principalMember.getInContactCellPhoneNumber();
            String workPhoneNumber = principalMember.getWorkPhoneNumber();
            String homePhoneNumber = principalMember.getHomePhoneNumber();

            principalMemberDetails.clickContactInformation();

            if (!preferredContactMethod.isEmpty()) {
                principalMemberContactInformation.moveToPreferredContactMethod();
                principalMemberContactInformation.selectPreferredContactMethod(preferredContactMethod);
            }

            if (!emailAddress.isEmpty()) {
                principalMemberContactInformation.clearEmailAddress();
                principalMemberContactInformation.updateEmailAddress(emailAddress);
            }

            if (!cisEmailAddress.isEmpty()) {
                principalMemberContactInformation.clearCisEmailAddress();
                principalMemberContactInformation.updateCisEmailAddress(cisEmailAddress);
            }

            if (!inContactCellPhoneNumber.isEmpty()) {
                String inContactCellPhoneNumberValue = principalMemberContactInformation.getInContactCellPhoneNumber();
            }

            if (!cellPhoneNumber.isEmpty()) {
                principalMemberContactInformation.clearCellPhoneNumber();
                principalMemberContactInformation.updateCellPhoneNumber(cellPhoneNumber);
            } else {
                String cellPhoneNumberValue = principalMemberContactInformation.getCellPhoneNumber();
                if (!cellPhoneNumberValue.isEmpty()) {
                    // principalMemberContactInformation.clearCellPhoneNumber();
                    principalMemberContactInformation.updateCellPhoneNumber(cellPhoneNumberValue.replaceAll("\\s", ""));
                }

                cellPhoneNumberValue = principalMemberContactInformation.getCellPhoneNumber(); // Or use cisCellPhoneNumber.getAttribute("value");
                if (!cellPhoneNumberValue.isEmpty()) {
                    int numberOfDigits = cellPhoneNumberValue.length();
                    char firstCharactor = cellPhoneNumberValue.charAt(0);
                    if (numberOfDigits != 10 && firstCharactor != '0') {
                        if (numberOfDigits == 9) {
                            principalMemberContactInformation.updateCellPhoneNumber("0" + cellPhoneNumberValue);
                        } else {
                            // reportHandler.setFailureReason("Contact Cell Phone Number Text Not Valid");
                            throw new Exception("Contact Cell Phone Number Text Not Valid");
                        }
                    }
                }
            }

            if (!cisCellPhoneNumber.isEmpty()) {
                principalMemberContactInformation.clearCisCellPhoneNumber();
                principalMemberContactInformation.updateCisCellPhoneNumber(cisCellPhoneNumber);
            } else {
                String cisCellPhoneNumberValue = principalMemberContactInformation.getCisCellPhoneNumber();
                if (!cisCellPhoneNumberValue.isEmpty()) {
                    principalMemberContactInformation.updateCisCellPhoneNumber(cisCellPhoneNumberValue.replaceAll("\\s", ""));
                }

                cisCellPhoneNumberValue = principalMemberContactInformation.getCisCellPhoneNumber(); // Or use cisCellPhoneNumber.getAttribute("value");
                if (!cisCellPhoneNumberValue.isEmpty()) {
                    int numberOfDigits = cisCellPhoneNumberValue.length();
                    char firstCharactor = cisCellPhoneNumberValue.charAt(0);
                    if (numberOfDigits != 10 && firstCharactor != '0') {
                        if (numberOfDigits == 9) {
                            principalMemberContactInformation.updateCisCellPhoneNumber("0" + cisCellPhoneNumberValue);
                        } else {
                            // reportHandler.setFailureReason("CIS Cell Phone Number Text Not Valid");
                            throw new Exception("CIS Cell Phone Number Text Not Valid");
                        }
                    }
                }
            }

            if (!inContactCellPhoneNumber.isEmpty()) {
                String inContactCellPhoneNumberValue = principalMemberContactInformation.getInContactCellPhoneNumber();
            }

            if (!workPhoneNumber.isEmpty()) {
                principalMemberContactInformation.clearWorkPhoneNumber();
                principalMemberContactInformation.updateWorkPhoneNumber(workPhoneNumber);
            } else {
                String workPhoneNumberValue = principalMemberContactInformation.getWorkPhoneNumber();
                if (!workPhoneNumberValue.isEmpty()) {
                    principalMemberContactInformation.updateWorkPhoneNumber(workPhoneNumberValue.replaceAll("\\s", ""));
                }

                workPhoneNumberValue = principalMemberContactInformation.getWorkPhoneNumber();
                if (!workPhoneNumberValue.isEmpty()) {
                    int numberOfDigits = workPhoneNumberValue.length();
                    char firstCharactor = workPhoneNumberValue.charAt(0);
                    if (numberOfDigits != 10 && firstCharactor != '0') {
                        if (numberOfDigits == 9) {
                            principalMemberContactInformation.updateWorkPhoneNumber("0" + workPhoneNumberValue);
                        } else {
                            // reportHandler.setFailureReason("Work Phone Number Text Not Valid");
                            throw new Exception("Work Phone Number Text Not Valid");
                        }
                    }
                }
            }

            if (!homePhoneNumber.isEmpty()) {
                principalMemberContactInformation.clearHomePhoneNumber();
                principalMemberContactInformation.updateHomePhoneNumber(homePhoneNumber);
            } else {
                String homePhoneNumberValue = principalMemberContactInformation.getHomePhoneNumber();
                if (!homePhoneNumberValue.isEmpty()) {
                    principalMemberContactInformation.updateHomePhoneNumber(homePhoneNumberValue.replaceAll("\\s", ""));
                }

                homePhoneNumberValue = principalMemberContactInformation.getHomePhoneNumber();
                if (!homePhoneNumberValue.isEmpty()) {
                    int numberOfDigits = homePhoneNumberValue.length();
                    char firstCharactor = homePhoneNumberValue.charAt(0);
                    if (numberOfDigits != 10 && firstCharactor != '0') {
                        if (numberOfDigits == 9) {
                            principalMemberContactInformation.updateHomePhoneNumber("0" + homePhoneNumberValue);
                        } else {
                            // reportHandler.setFailureReason("Home Phone Number Text Not Valid");
                            throw new Exception("Home Phone Number Text Not Valid");
                        }
                    }
                }
            }

            // TODO: Save and do Validations
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Postal Information")) {
            String postalSameAsResidentialAddressOne = principalMember.getPostalSameAsResidentialAddressOne();
            String postalStreetOrBuildingLineOne = principalMember.getPostalStreetOrBuildingLineOne();
            String postalStreetOrBuildingLineTwo = principalMember.getPostalStreetOrBuildingLineTwo();
            String postalSuburb = principalMember.getPostalSuburb();
            String postalCity = principalMember.getPostalCity();
            String postalPostalCode = principalMember.getPostalPostalCode();
            String postalPostalCodeSearch = principalMember.getPostalPostalCodeSearch();
            String postalPostalCodeSearchLocation = principalMember.getPostalPostalCodeSearchLocation();
            String postalPostalCodeSearchCode = principalMember.getPostalPostalCodeSearchCode();

            String postalSameAsResidentialAddressTwo = principalMember.getPostalSameAsResidentialAddressTwo();
            String postalCisStreetOrBuildingLineOne = principalMember.getPostalCisStreetOrBuildingLineOne();
            String postalCisStreetOrBuildingLineTwo = principalMember.getPostalCisStreetOrBuildingLineTwo();
            String postalCisPostalCity = principalMember.getPostalCisPostalCity();
            String postalCisPostalCode = principalMember.getPostalCisPostalCode();
            String postalCisPostalCodeSearch = principalMember.getPostalCisPostalCodeSearch();
            String postalCisPostalCodeSearchLocation = principalMember.getPostalCisPostalCodeSearchLocation();
            String postalCisPostalCodeSearchCode = principalMember.getPostalCisPostalCodeSearchCode();

            String postalCisPostalState = principalMember.getPostalCisPostalState();
            String postalCisPostalCountry = principalMember.getPostalCisPostalCountry();
            String postalCisZipCode = principalMember.getPostalCisZipCode();
            String postalCisZipCodeSearch = principalMember.getPostalCisZipCodeSearch();
            String postalCisZipCodeSearchLocation = principalMember.getPostalCisZipCodeSearchLocation();
            String postalCisZipCodeSearchCode = principalMember.getPostalCisZipCodeSearchCode();

            principalMemberDetails.clickPostalInformation();

            if (!postalStreetOrBuildingLineOne.isEmpty()) {
                principalMemberPostalInformation.clearPostalLineOne();
                principalMemberPostalInformation.updatePostalLineOne(postalStreetOrBuildingLineOne);
            }

            if (!postalStreetOrBuildingLineTwo.isEmpty()) {
                principalMemberPostalInformation.clearPostalLineTwo();
                principalMemberPostalInformation.updatePostalLineTwo(postalStreetOrBuildingLineTwo);
            }

            if (!postalSuburb.isEmpty()) {
                principalMemberPostalInformation.clearPostalSuburb();
                principalMemberPostalInformation.updatePostalSuburb(postalSuburb);
            }

            if (!postalCity.isEmpty()) {
                principalMemberPostalInformation.clearPostalCity();
                principalMemberPostalInformation.updatePostalCity(postalCity);
            }

            if (!postalPostalCode.isEmpty()) {
                principalMemberPostalInformation.clearPostalCode();
                principalMemberPostalInformation.updatePostalCode(postalPostalCode);
            }

            if (postalPostalCodeSearch.equalsIgnoreCase("YES")) {
                principalMemberPostalInformation.clickSearchPostalAddress();

                boolean switched = false;
                int frameIndex = 0;
                do {
                    log.debug(frameIndex);
                    boolean frameFlag = false;
                    int numberOfTries = 1;
                    while (!frameFlag && frameIndex < 10) {
                        log.debug("locating work item log frame with index: " + frameIndex);
                        try {
                            driver.switchTo().defaultContent();
                            Thread.sleep(2000);
                            driver.switchTo().frame(frameIndex++);
                            frameFlag = true;
                        } catch (NoSuchFrameException e) {
                            log.warn("NoSuchFrameException thrown.");
                            if (frameIndex == 9 && numberOfTries < 2) {
                                frameIndex = 0;
                                numberOfTries++;
                            } else {
                                // reportHandler.setFailureReason("Failed to process address search. Issues with the popup");
                                throw new Exception("Failed to process address search. Issues with the popup");
                            }
                        }
                        Thread.sleep(3000);
                    }
                    switched = frameFlag;
                } while (!switched && frameIndex < 10);

                // Then do the following:
                if (!postalPostalCodeSearchLocation.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearLocation();
                    principalMemberPostalInformationAddressSearch.updateLocation(postalPostalCodeSearchLocation);
                }

                if (!postalPostalCodeSearchCode.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearPostalCode();
                    principalMemberPostalInformationAddressSearch.updatePostalCode(postalPostalCodeSearchCode);
                }

                // Then Click on the search:
                principalMemberPostalInformationAddressSearch.clickSearchAddressButton();
                Thread.sleep(3000);

                principalMemberPostalInformationAddressSearch.moveToAndDoubleClickAddressSearchFirstItem();
                Thread.sleep(3000);
            }

            if (!postalCisStreetOrBuildingLineOne.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalAddressLineOne();
                principalMemberPostalInformation.updateCisPostalAddressLineOne(postalCisStreetOrBuildingLineOne);
            }

            if (!postalCisStreetOrBuildingLineTwo.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalAddressLineTwo();
                principalMemberPostalInformation.updateCisPostalAddressLineTwo(postalCisStreetOrBuildingLineTwo);
            }

            if (!postalCisPostalCity.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalCity();
                principalMemberPostalInformation.updateCisPostalCity(postalCisPostalCity);
            }

            if (!postalCisPostalCode.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalCode();
                principalMemberPostalInformation.updateCisPostalCode(postalCisPostalCode);
            }

            if (postalCisPostalCodeSearch.equalsIgnoreCase("YES")) {
                principalMemberPostalInformation.clickCisSearchPostalAddress();

                boolean switched = false;
                int frameIndex = 0;
                do {
                    log.debug(frameIndex);
                    boolean frameFlag = false;
                    int numberOfTries = 1;
                    while (!frameFlag && frameIndex < 10) {
                        log.debug("locating work item log frame with index: " + frameIndex);
                        try {
                            driver.switchTo().defaultContent();
                            Thread.sleep(2000);
                            driver.switchTo().frame(frameIndex++);
                            frameFlag = true;
                        } catch (NoSuchFrameException e) {
                            log.warn("NoSuchFrameException thrown.");
                            if (frameIndex == 9 && numberOfTries < 2) {
                                frameIndex = 0;
                                numberOfTries++;
                            } else {
                                // reportHandler.setFailureReason("Failed to process address search. Issues with the popup");
                                throw new Exception("Failed to process address search. Issues with the popup");
                            }
                        }
                        Thread.sleep(3000);
                    }
                    switched = frameFlag;
                } while (!switched && frameIndex < 10);

                // Then do the following:
                if (!postalCisPostalCodeSearchLocation.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearLocation();
                    principalMemberPostalInformationAddressSearch.updateLocation(postalCisPostalCodeSearchLocation);
                }

                if (!postalCisPostalCodeSearchCode.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearPostalCode();
                    principalMemberPostalInformationAddressSearch.updatePostalCode(postalCisPostalCodeSearchCode);
                }

                principalMemberPostalInformationAddressSearch.clickSearchAddressButton();
                Thread.sleep(3000);

                principalMemberPostalInformationAddressSearch.moveToAndDoubleClickAddressSearchFirstItem();
                Thread.sleep(3000);
            }

            if (!postalCisPostalState.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalState();
                principalMemberPostalInformation.updateCisPostalState(postalCisPostalState);
            }

            if (!postalCisPostalCountry.isEmpty()) {
                principalMemberPostalInformation.clearCisPostalCountry();
                principalMemberPostalInformation.updateCisPostalCountry(postalCisPostalCountry);
            }

            if (!postalCisZipCode.isEmpty()) {
                principalMemberPostalInformation.clearCisZipCode();
                principalMemberPostalInformation.updateCisZipCode(postalCisZipCode);
            }

            if (postalCisZipCodeSearch.equalsIgnoreCase("YES")) {
                principalMemberPostalInformation.clickCisSearchCisZipCode();

                boolean switched = false;
                int frameIndex = 0;
                do {
                    log.debug(frameIndex);
                    boolean frameFlag = false;
                    int numberOfTries = 1;
                    while (!frameFlag && frameIndex < 10) {
                        log.debug("locating work item log frame with index: " + frameIndex);
                        try {
                            driver.switchTo().defaultContent();
                            Thread.sleep(2000);
                            driver.switchTo().frame(frameIndex++);
                            frameFlag = true;
                        } catch (NoSuchFrameException e) {
                            log.warn("NoSuchFrameException thrown.");
                            if (frameIndex == 9 && numberOfTries < 2) {
                                frameIndex = 0;
                                numberOfTries++;
                            } else {
                                // reportHandler.setFailureReason("Failed to process address search. Issues with the popup");
                                throw new Exception("Failed to process address search. Issues with the popup");
                            }
                        }
                        Thread.sleep(3000);
                    }
                    switched = frameFlag;
                } while (!switched && frameIndex < 10);

                // Then do the following:
                if (!postalCisZipCodeSearchLocation.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearLocation();
                    principalMemberPostalInformationAddressSearch.updateLocation(postalCisZipCodeSearchLocation);
                }

                if (!postalCisZipCodeSearchCode.isEmpty()) {
                    principalMemberPostalInformationAddressSearch.clearPostalCode();
                    principalMemberPostalInformationAddressSearch.updatePostalCode(postalCisZipCodeSearchCode);
                }

                // Then Click on the search:
                principalMemberPostalInformationAddressSearch.clickSearchAddressButton();
                Thread.sleep(3000);

                principalMemberPostalInformationAddressSearch.moveToAndDoubleClickAddressSearchFirstItem();
                Thread.sleep(3000);
            }

            // TODO: Save and do Validations
        }
    }

    private Double getPolicyMembersTotalPremium() throws Exception {
        Double totalPremium = 0.00;
        String premium;
        Double premiumValue;
        try {
            int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

            for(int j = 0; j < numberOfExistingMembers ; j++) {
                premium = memberInformation.getPolicyDetailsPremium(j);
                premium = getDoubleString(premium);
                premiumValue = stringToDouble(premium);
                totalPremium += premiumValue;
            }
            return totalPremium;
        } catch (Exception e) {
            log.error("Error while getting policy members total premium.");
            throw new Exception("Error while getting policy members total premium.");
        }
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    public void save() throws Exception {
        try {
            log.info("Saving Update Principal Member.");

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {principalMember.getPopupWorkTypeZero()};
            String[] popUpStatus = {principalMember.getPopupStatusZero()};
            String[] popUpQueue = {principalMember.getPopupQueueZero()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                principalMemberDetails.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                principalMemberDetails.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() throws Exception {
        String productName = principalMember.getProductName();
        String principalMemberInformationToUpdate = principalMember.getPrincipalMemberInformationToUpdate();

        if (principalMemberInformationToUpdate.equalsIgnoreCase("Personal Information")) {
            String policyHolder = principalMember.getPolicyHolder();
            String tradingAsNameOrMiddleName = principalMember.getTradingAsNameOrMiddleName();
            String companyRegNumberOrIdNumber = principalMember.getCompanyRegNumberOrIdNumber();
            String vatRegNumber = principalMember.getVatRegNumber();
            String typeOfId = principalMember.getTypeOfId();
            String income = principalMember.getIncome();
            String fNumber = principalMember.getfNumber();
            String expectedPremiumAmount = principalMember.getExpectedPremiumAmountValue();
            String dateOfBirth = principalMember.getDateOfBirth();
            String gender = principalMember.getGender();
            String correspondenceLanguage = principalMember.getCorrespondenceLanguage();
            String maritalStatus = principalMember.getMaritalStatus();

            if (productName.equalsIgnoreCase("LOC-Personal")) {
                log.info("Clicking on policy Details");
                policyDetails = createSearchItemPage.openPolicyDetails(false);
                Thread.sleep(3000);
                String totalPremiumAmount = policyInformation.getTotalPremiumAmount();

                if (totalPremiumAmount.isEmpty()) {
                    totalPremiumAmount = "0.00";
                }
                Double totalPremiumAmountValue = Double.valueOf(totalPremiumAmount);

                if (expectedPremiumAmount.isEmpty()) {
                    expectedPremiumAmount = "0.00";
                }
                Double expectedPremiumAmountValue = Double.valueOf(expectedPremiumAmount);

                if (!fNumberText.isEmpty()) {
                    if (!totalPremiumAmountValue.equals(expectedPremiumAmountValue)) {
                        // reportHandler.setFailureReason("The expected total premium is not correct. Got " + totalPremiumAmountValue + " instead of " + expectedPremiumAmountValue);
                        setValid(false);
                        throw new Exception("The expected total premium is not correct. Got " + totalPremiumAmountValue + " instead of " + expectedPremiumAmountValue);
                    }
                } else {
                    if (!totalPremiumAmountValue.equals(expectedPremiumAmountValue)) {
                        // reportHandler.setFailureReason("The expected total premium is not correct. Got " + totalPremiumAmountValue + " instead of " + expectedPremiumAmountValue);
                        setValid(false);
                        throw new Exception("The expected total premium is not correct. Got " + totalPremiumAmountValue + " instead of " + expectedPremiumAmountValue);
                    }
                }

                log.debug("Clicking on Principal Member Details");
                createSearchItemPage.openPrincipalMemberDetails(false);
                Thread.sleep(3000);
            }

            if (!productName.equalsIgnoreCase("LOC-Business")) {
                log.debug("Clicking on Principal Member Details");
                createSearchItemPage.openPrincipalMemberDetails(false);
                Thread.sleep(3000);
                if (!dateOfBirth.isEmpty()) {
                    String dateOfBirthValue = principalMemberPersonalInformation.getDateOfBirth();
                    boolean isValidDateOfBirth = validateDateOfBirth(dateOfBirthValue, companyRegNumberOrIdNumber);
                    if (!isValidDateOfBirth || !dateOfBirthValue.equalsIgnoreCase(dateOfBirth)) {
                        // reportHandler.setFailureReason("The expected date of birth is not correct. Got " + dateOfBirthValue + " instead of as per ID");
                        setValid(false);
                        throw new Exception("The expected date of birth is not correct. Got " + dateOfBirthValue + " instead of as per ID");
                    }
                }
            }
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Residential Information")) {
            // TODO: Validations for "Residential Information"
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Contact Information")) {
            // TODO: Validations "Contact Information"
        } else if (principalMemberInformationToUpdate.equalsIgnoreCase("Postal Information")) {
            String postalSameAsResidentialAddressOne = principalMember.getPostalSameAsResidentialAddressOne();
            String postalStreetOrBuildingLineOne = principalMember.getPostalStreetOrBuildingLineOne();
            String postalStreetOrBuildingLineTwo = principalMember.getPostalStreetOrBuildingLineTwo();
            String postalSuburb = principalMember.getPostalSuburb();
            String postalCity = principalMember.getPostalCity();
            String postalPostalCode = principalMember.getPostalPostalCode();
            String postalPostalCodeSearch = principalMember.getPostalPostalCodeSearch();
            String postalPostalCodeSearchLocation = principalMember.getPostalPostalCodeSearchLocation();
            String postalPostalCodeSearchCode = principalMember.getPostalPostalCodeSearchCode();

            String postalSameAsResidentialAddressTwo = principalMember.getPostalSameAsResidentialAddressTwo();
            String postalCisStreetOrBuildingLineOne = principalMember.getPostalCisStreetOrBuildingLineOne();
            String postalCisStreetOrBuildingLineTwo = principalMember.getPostalCisStreetOrBuildingLineTwo();
            String postalCisPostalCity = principalMember.getPostalCisPostalCity();
            String postalCisPostalCode = principalMember.getPostalCisPostalCode();
            String postalCisPostalCodeSearch = principalMember.getPostalCisPostalCodeSearch();
            String postalCisPostalCodeSearchLocation = principalMember.getPostalCisPostalCodeSearchLocation();
            String postalCisPostalCodeSearchCode = principalMember.getPostalCisPostalCodeSearchCode();

            String postalCisPostalState = principalMember.getPostalCisPostalState();
            String postalCisPostalCountry = principalMember.getPostalCisPostalCountry();
            String postalCisZipCode = principalMember.getPostalCisZipCode();
            String postalCisZipCodeSearch = principalMember.getPostalCisZipCodeSearch();
            String postalCisZipCodeSearchLocation = principalMember.getPostalCisZipCodeSearchLocation();
            String postalCisZipCodeSearchCode = principalMember.getPostalCisZipCodeSearchCode();

            log.debug("Clicking on Principal Member Details");
            createSearchItemPage.openPrincipalMemberDetails(false);
            Thread.sleep(3000);

            if (!postalPostalCode.isEmpty() || postalPostalCodeSearch.equalsIgnoreCase("YES")) {
                String postalCode = principalMemberResidentialInformation.getPostalCode();
                if (postalCode.isEmpty() || postalCode.length() != 4) {
                    // postalCode.setFailureReason("Postal code not correct");
                    setValid(false);
                    throw new Exception("Postal code not correct");
                }
            }

            if (!postalCisPostalCode.isEmpty() || postalCisPostalCodeSearch.equalsIgnoreCase("YES")) {
                String cisPostalCode = principalMemberResidentialInformation.getCisPostalCode();
                if (cisPostalCode.isEmpty() || cisPostalCode.length() != 4) {
                    // reportHandler.setFailureReason("Postal cis code not correct");
                    setValid(false);
                    throw new Exception("Postal cis code not correct");
                }
            }

            if (!postalCisZipCode.isEmpty() || postalCisZipCodeSearch.equalsIgnoreCase("YES")) {
                String cisZipCode = principalMemberResidentialInformation.getCisZipCode();
                if (cisZipCode.isEmpty() || cisZipCode.length() != 4) {
                    // reportHandler.setFailureReason("Postal Cis Zip Code not correct");
                    setValid(false);
                    throw new Exception("Postal Cis Zip Code not correct");
                }
            }
        }

        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

    private boolean validateDateOfBirth(String dateOfBirthData, String companyRegOrIdNumberData) {
        String idYear = companyRegOrIdNumberData.substring(0, 2);
        String idMonth = companyRegOrIdNumberData.substring(2, 4);
        String idDate = companyRegOrIdNumberData.substring(4, 6);

        String dobDate = dateOfBirthData.substring(0, 2);
        String dobMonth = dateOfBirthData.substring(3, 5);
        String dobYear = dateOfBirthData.substring(8, 10);

        if (idYear.equalsIgnoreCase(dobYear) &&
                idMonth.equalsIgnoreCase(dobMonth) &&
                idDate.equalsIgnoreCase(dobDate)) {
            return true;
        }
        return false;
    }

}