package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.claims.AwaitDocDiarizeReviewDeclinedItem;
import co.za.fnb.flow.pages.claims.AwaitDocOrDiarizeItem;
import co.za.fnb.flow.pages.claims.Claims;
import co.za.fnb.flow.pages.covid_screening_details.CovidScreeningDetails;
import co.za.fnb.flow.pages.create_bulk_items.CreateBulkItems;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.setup.TestResultReportFlag;
import cucumber.api.DataTable;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class LossOfIncomeRunner {
    private Logger log  = LogManager.getLogger(LossOfIncomeRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;
    HomePage homePage;
    PolicyDetails policyDetails;
    PrincipalMemberDetails principalMemberDetails;
    Claims claims;
    GenericMaintenance genericMaintenance;
    CreateSearchItemPage createSearchItemPage;
    LegalAdvisorAdminPage legalAdvisorAdminPage;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    WorkItems workItems;
    AuditTrail auditTrail;
    String policyNumberValue;

    public LossOfIncomeRunner() {
    }

    public LossOfIncomeRunner(WebDriver driver) {
        this.driver = driver;
    }

    public LossOfIncomeRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public LossOfIncomeRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        String productName = scenarioOperator.getProductName();
        if (!productName.toUpperCase().contains("LOC")) {
            queryHandler.insertFicaRecord(
                    productName,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber, takeupFirst, takeupCode;

        try {
            if (Arrays.asList(headers).contains("Test Case No")) {
                for (Row row : dataTable.getGherkinRows()) {
                    if (row.getLine() != 1) {
                        dataEntry = row.getCells();
                        run = getCellValue(headers, dataEntry,"Run");
                        if (run.equalsIgnoreCase("YES")) {
                            testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                            scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                            testFunction = getCellValue(headers, dataEntry,"Function");
                            productName = getCellValue(headers, dataEntry,"Product Name");
                            scenarioOperator.setTestCaseNumber(testCaseNumber);
                            scenarioOperator.setScenarioDescription(scenarioDescription);
                            scenarioOperator.setFunction(testFunction);
                            scenarioOperator.setProductName(productName);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                            scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                            // BasePage.setScenarioOperator(scenarioOperator);

                            try {
                                log.info("===========================================================================");
                                log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                                log.info("---------------------------------------------------------------------------");

                                if (testFunction.equalsIgnoreCase("Loss Of Income")) {
                                    lossOfIncome(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                }
                                log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            } catch (Exception e) {
                                log.error(e.getMessage());
                                e.printStackTrace();
                                reportFailure(reportableFields, scenarioOperator, e.getMessage());
                                scenarioOperator.increamentReportRowIndex();
                            } finally {
                                if (driver.getWindowHandles().toArray().length > 1) {
                                    int numberOfWindows = driver.getWindowHandles().toArray().length;
                                    while (numberOfWindows > 1) {
                                        Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows - 1];
                                        WebDriver window = driver.switchTo().window((String) currentWindow);
                                        window.close();
                                        numberOfWindows = driver.getWindowHandles().toArray().length;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: Handle Whole Test Exception
        } finally {
            log.info("Saving Report.");
            scenarioOperator.saveReport();
        }
        log.info("===========================================================================");
    }

    private void lossOfIncome(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Loss Of Income Logic");
        policyNumberValue = "";

        String productName = getCellValue(headers, dataEntry, "Product Name");
        String productPrefix = getCellValue(headers, dataEntry, "Product Prefix");

        String documentReviewed = getCellValue(headers, dataEntry, "Document Reviewed");

        String employerName = getCellValue(headers, dataEntry, "Employer Name");
        String preScreeningCategory = getCellValue(headers, dataEntry, "Pre-Screening Category");
        String preScreeningSubCategory = getCellValue(headers, dataEntry, "Pre-Screening Sub Category");
        String preScreeningComment = getCellValue(headers, dataEntry, "Pre-Screening Comment");

        String targetWorkType = getCellValue(headers, dataEntry, "Target Work Type");
        String targetStatus = getCellValue(headers, dataEntry, "Target Status");
        String targetQueue = getCellValue(headers, dataEntry, "Target Queue");

        String screeningAction = getCellValue(headers, dataEntry, "Screening Action");
        String targetPolicyNumber = getCellValue(headers, dataEntry, "Policy Number");
        String actionCategory = getCellValue(headers, dataEntry, "Action Category");
        String actionSubCategory = getCellValue(headers, dataEntry, "Action Sub Category");
        String changeWorkType = getCellValue(headers, dataEntry, "Change Work Type");
        String changeStatus = getCellValue(headers, dataEntry, "Change Status");
        String changeQueue = getCellValue(headers, dataEntry, "Change Queue");
        String captureIdNumberFromPreScreening = getCellValue(headers, dataEntry, "Capture Id Number From Pre Screening");
        String idNumber = getCellValue(headers, dataEntry, "Id Number");
        String ucnNumber = getCellValue(headers, dataEntry, "UCN Number");

        String duration = getCellValue(headers, dataEntry, "Duration");
        String comments = getCellValue(headers, dataEntry, "Comments");
        String reason = getCellValue(headers, dataEntry, "Reason");
        String communicationMessage = getCellValue(headers, dataEntry, "Communication Message");

        String updatedWorkType = getCellValue(headers, dataEntry, "Updated Work Type");
        String updatedStatus = getCellValue(headers, dataEntry, "Updated Status");
        String updatedQueue = getCellValue(headers, dataEntry, "Updated Queue");

        CovidScreeningDetails covidScreeningDetails = new CovidScreeningDetails(driver, scenarioOperator);
        covidScreeningDetails.maximizePage();
        Thread.sleep(3000);

        // TODO: Add Flag For Create Bulk Item
        homePage = new HomePage(driver, scenarioOperator);
        CreateBulkItems createBulkItems = homePage.selectCreateBulkItems();
        createBulkItems.choose("C:\\Users\\F5255309\\OneDrive - FRG\\Documents\\Adriot Vukosi Mkhari FNB\\Automation\\Loss of Income\\test_upload_001.csv");
        String file = createBulkItems.getFile();
        createBulkItems.upload();
        String summary = createBulkItems.getSummary();
        String fileUploaded = createBulkItems.getFileUploaded();

        String policyNumber = targetPolicyNumber;
        boolean preScreened = false;
        if (targetPolicyNumber.isEmpty()) {
            preScreened = true;
            retrieveNewClaim(covidScreeningDetails);
            Thread.sleep(10000);

            String messageBoxText;
            policyNumber = covidScreeningDetails.getPolicyNumber();

            int preFixLength = productPrefix.length();
            String policyNumberPrefix = policyNumber.substring(0, preFixLength);

            int count = 0;
            while (!policyNumberPrefix.equalsIgnoreCase(productPrefix) && count++ < 50) {
                Thread.sleep(3000);
                retrieveNewClaim(covidScreeningDetails);
                policyNumber = covidScreeningDetails.getPolicyNumber();
                policyNumberPrefix = policyNumber.substring(0, preFixLength);
            }

            policyNumberValue = policyNumber;

            String workItemID = covidScreeningDetails.getWorkItemID();
            String clientName = covidScreeningDetails.getClientName();
            String clientIDNo = covidScreeningDetails.getClientIDNo();

            if (captureIdNumberFromPreScreening.equalsIgnoreCase("Yes")) {
                if (!clientIDNo.isEmpty() && clientIDNo.length() == 13) {
                    idNumber = clientIDNo;
                }
            }

            covidScreeningDetails.clickEditEmployerButton();
            // TODO: Use some kind of explicit wait.
            Thread.sleep(5000);

            covidScreeningDetails.inputEmployerName(employerName);
            Thread.sleep(2000);
            covidScreeningDetails.selectCategory(preScreeningCategory);
            Thread.sleep(5000);
            covidScreeningDetails.selectSubCategory(preScreeningSubCategory);
            Thread.sleep(5000);
            covidScreeningDetails.inputComment(preScreeningComment);
            Thread.sleep(2000);
            covidScreeningDetails.clickPolicySubmitButton();
            Thread.sleep(4000);

            String successfullyCompletedScreening = "Successfully completed screening on claim work item.";
            messageBoxText = covidScreeningDetails.getMessageBoxText();
            if (!messageBoxText.equalsIgnoreCase(successfullyCompletedScreening)) {
                log.error("Invalid Response Message: " + messageBoxText);
                throw new Exception("Invalid Response Message: " + messageBoxText);
            }
            Thread.sleep(3000);
        }

        Thread.sleep(2000);
        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();
        Thread.sleep(3000);

        // scenarioOperator.setPolicyNumber(member.getPolicyNumber());
        // scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumberAndAvoidResponses(policyNumber, false);
            Thread.sleep(10000);
            // TODO: Close -> ERROR! FNB400-PPENQ0001-Policy not found

            WorkItems workItems = new WorkItems(driver, scenarioOperator);
            workItems.getWorkItems(1);
            String[] itemsWorkType = workItems.getWorkType();
            String[] statuses = workItems.getStatus();
            String[] queues = workItems.getQueue();

            String workType;
            String status;
            String queue;
            if (itemsWorkType != null) {
                workType = itemsWorkType[0];
                status = statuses[0];
                queue = queues[0];

                boolean workTypesMatch = workType.equalsIgnoreCase(targetWorkType);
                boolean statusesMatched = status.equalsIgnoreCase(targetStatus);
                boolean queuesMatch = queue.equalsIgnoreCase(targetQueue);

                if (!(workTypesMatch && statusesMatched && queuesMatch)) {
                    StringBuilder stringBuilder = new StringBuilder();
                    stringBuilder.append("Work Items Do Not Match:");
                    stringBuilder.append("\nWork Type [" + workType + "], " + "Expected Work Type: " + targetWorkType);
                    stringBuilder.append("\nStatus [" + status + "], " + "Expected Status: " + targetStatus);
                    stringBuilder.append("\nQueue [" + queue + "], " + "Expected Queue: " + targetQueue);

                    log.error(stringBuilder.toString());
                    throw new Exception(stringBuilder.toString());
                }

                String comment = workItems.getComment(0);
                log.info("First Comment: \n" + comment);

                // TODO: Check Comment Summary
                if (preScreened) {
                    String firstCommentCheck = "Comment:  Status changed from PRESCREENED to " + targetStatus;
                    if (!comment.contains(firstCommentCheck)) {
                        log.error("Comment Summary Does Not Contain: " + firstCommentCheck);
                        throw new Exception("Comment Summary Does Not Contain: " + firstCommentCheck);
                    }
                }

                if (screeningAction.equalsIgnoreCase("Yes")) {
                    CreateOrUpdateWorkItem createOrUpdateWorkItem = new CreateOrUpdateWorkItem(driver, scenarioOperator);
                    if (!actionCategory.isEmpty()) {
                        createSearchItemPage.selectValidationCategory(actionCategory);
                        Thread.sleep(2000);
                    }

                    if (!actionSubCategory.isEmpty()) {
                        createSearchItemPage.selectValidationSubCategory(actionSubCategory);
                        Thread.sleep(2000);
                    }

                    if (!changeWorkType.isEmpty()) {
                        createOrUpdateWorkItem.selectWorkType(changeWorkType);
                        Thread.sleep(2000);
                    }

                    if (!changeQueue.isEmpty()) {
                        createOrUpdateWorkItem.selectQueue(changeQueue);
                        Thread.sleep(2000);
                    }

                    if (!changeStatus.isEmpty()) {
                        createOrUpdateWorkItem.selectStatus(changeStatus);
                        Thread.sleep(2000);
                    }

                    // Click Assign To Me
                    createOrUpdateWorkItem.clickAssignToMe();
                    Thread.sleep(2000);
                    // Id Number
                    createOrUpdateWorkItem.updateIdNumber(idNumber);
                    Thread.sleep(2000);
                    // UCN
                    if (!ucnNumber.isEmpty()) {
                        createOrUpdateWorkItem.updateUcnNumber(ucnNumber);
                        Thread.sleep(2000);
                    }

                    createSearchItemPage.clickUpdate();
                    Thread.sleep(2000);
                    AwaitDocOrDiarizeItem awaitDocOrDiarizeItem = new AwaitDocOrDiarizeItem(driver, scenarioOperator);
                    boolean gotContent = awaitDocOrDiarizeItem.waitForContent();
                    if (gotContent) {
                        awaitDocOrDiarizeItem.selectTime(duration);
                        Thread.sleep(2000);
                        awaitDocOrDiarizeItem.updateComments(comments);
                        Thread.sleep(2000);
                        awaitDocOrDiarizeItem.selectReason(reason);
                        Thread.sleep(2000);
                        awaitDocOrDiarizeItem.clickSubmit();
                        Thread.sleep(1000);
                        // TODO: Validate -> Item updated successfully. The work item has been updated successfully.
                    } else {
                        AwaitDocDiarizeReviewDeclinedItem awaitDocDiarizeReviewDeclinedItem = new AwaitDocDiarizeReviewDeclinedItem(driver, scenarioOperator);
                        boolean gotAwaitContent = awaitDocDiarizeReviewDeclinedItem.waitForContent();
                        if (gotAwaitContent) {
                            awaitDocDiarizeReviewDeclinedItem.clickSubmit();
                            Thread.sleep(1000);
                        }
                    }

                    // TODO: Get Message Box

                    createSearchItemPage.refreshServicePage(1);
                    Thread.sleep(2000);
                    createSearchItemPage.searchPolicyNumberAndAvoidResponses(policyNumber, false);
                    Thread.sleep(10000);
                    // TODO: Close -> ERROR! FNB400-PPENQ0001-Policy not found

                    workItems = new WorkItems(driver, scenarioOperator);
                    workItems.getWorkItems(1);
                    itemsWorkType = workItems.getWorkType();
                    statuses = workItems.getStatus();
                    queues = workItems.getQueue();

                    if (itemsWorkType != null) {
                        workType = itemsWorkType[0];
                        status = statuses[0];
                        queue = queues[0];

                        // TODO: Get Second Targets/Expected Values
                        workTypesMatch = workType.equalsIgnoreCase(updatedWorkType);
                        statusesMatched = status.equalsIgnoreCase(updatedStatus);
                        queuesMatch = queue.equalsIgnoreCase(updatedQueue);

                        if (!(workTypesMatch && statusesMatched && queuesMatch)) {
                            StringBuilder stringBuilder = new StringBuilder();
                            stringBuilder.append("Work Items Do Not Match:");
                            stringBuilder.append("\nWork Type [" + workType + "], " + "Expected Work Type: " + updatedWorkType);
                            stringBuilder.append("\nStatus [" + status + "], " + "Expected Status: " + updatedStatus);
                            stringBuilder.append("\nQueue [" + queue + "], " + "Expected Queue: " + updatedQueue);

                            log.error(stringBuilder.toString());
                            throw new Exception(stringBuilder.toString());
                        }

                        comment = workItems.getComment(0);
                        // TODO: Check Comment Summary
                        log.info("Second Comment: \n" + comment);
                        String secondCommentCheck = "Comment:  Status changed from " + targetStatus + " to " + updatedStatus;
                        if (!comment.contains(secondCommentCheck)) {
                            log.error("Comment Summary Does Not Contain: " + secondCommentCheck);
                            throw new Exception("Comment Summary Does Not Contain: " + secondCommentCheck);
                        }
                    }
                }
            }
            // TODO: Validate If This Is A Suitable Place To Report Success.
            reportSuccess(reportableFields, scenarioOperator);
        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Loss Of Income Logic");
    }

    private void retrieveNewClaim(CovidScreeningDetails covidScreeningDetails) throws Exception {
        covidScreeningDetails.refreshPage();
        covidScreeningDetails.retrieveNewClaim();
        // TODO: Use some kind explicit wait.
        Thread.sleep(10000);
        String noClaimWorkItem = "There are no claim work item available to be screened process";
        String messageBoxText = covidScreeningDetails.getMessageBoxText();
        if (messageBoxText.equalsIgnoreCase(noClaimWorkItem)) {
            log.error(noClaimWorkItem);
            throw new Exception(noClaimWorkItem);
        }
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), policyNumberValue, TestResultReportFlag.DEFAULT);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), policyNumberValue, TestResultReportFlag.DEFAULT);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), policyNumberValue, TestResultReportFlag.DEFAULT);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
