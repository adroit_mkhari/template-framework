package co.za.fnb.flow.handlers.database;

public enum Queries {
    // region Hospital Cash Plan:
    HCP1 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP2 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP3 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP4 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP5 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP6 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP7 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP8 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP9 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP10 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 2000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP11 {
        public String toString() {
            return " Select policyno FROM hostcaplib.hcppolmpf a" +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 2000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP12 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 18 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    "                    ) " +
                    "select * from mytempfile " +
                    "WHERE id  in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP13 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 19 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    "                    ) " +
                    "select * from mytempfile " +
                    "WHERE id  in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 19 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP14 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 631 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    "                    ) " +
                    "select * from mytempfile " +
                    "WHERE id  in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 631 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP15 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 932 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    "                    ) " +
                    "select * from mytempfile " +
                    "WHERE id  in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 932 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HCP16 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 631 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 3 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    "                    ) " +
                    "select * from mytempfile " +
                    "WHERE id  in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 631 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC1 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC2 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC3 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC4 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC5 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC6 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC7 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC8 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC9 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC10 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 2000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC11 {
        public String toString() {
            return"with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 " ;
        }
    },
    HC12 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC13 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC14 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC15 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC16 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1200 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC17 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1400 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC18 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1600 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC19 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 1800 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC20 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where amount = 2000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC21 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID in (18,19,631,932) and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    HC22 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM hostcaplib.hcppolmpf a " +
                    "inner join hostcaplib.HCPROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  hostcaplib.HCPCOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT policy_id FROM hostcaplib.HCPROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    R_HCP1 {
        public String toString() {
            return "Select Policyno FROM hostcaplib.hcppolmpf where status_id = 7 and product_id = 3 and statusdte > current timestamp  -90 days ORDER BY rand () limit 1 ";
        }
    },
    // endregion

    // region Funeral Insurance:
    FI1 {
        public String toString() {
            return "SELECT * FROM FUNINS_P.FIPOLMPF WHERE status_id =6 ORDER BY RAND() limit 1";
        }
    },
    FI2 {
        public String toString() {
            return "SELECT * FROM FUNINS_P.FIPOLMPF WHERE status_id = '6' and Statusdte = DATE(NOW()) ORDER BY RAND() limit 1";
        }
    },
    FI3 {
        public String toString() {
            return "with policylist as (" +
                    "                select distinct a.policyno, count(*) as Count " +
                    "                from fipolmpf a " +
                    "                inner join firolpopf b on a.id = b.policy_id " +
                    "                where b.active = 'Y' " +
                    "                and a.status_id = 6 " +
                    "                group by a.policyno, rand () " +
                    "                having count(*) = 1 " +
                    "                LIMIT 1 " +
                    ") select policyno " +
                    "                from policylist a ";
        }
    },
    FI4 {
        public String toString() {
            return "with policylist as (" +
                    "                select distinct b.policy_id, b.roletyp_id, count(*) as Count " +
                    "                from fipolmpf a " +
                    "                inner join firolpopf b on a.id = b.policy_id " +
                    "                where b.active = 'Y' " +
                    "                group by b.policy_id, b.roletyp_id " +
                    ") , Summary as (select distinct policy_id, count(*) as Count " +
                    "                from policylist " +
                    "                group by policy_id " +
                    "                having count(*) = 6 " +
                    ") select Summary.*, policyno, role.surname, codesdesc(rolpo.roletyp_id) " +
                    "from Summary " +
                    "inner join fipolmpf pol on summary.policy_id = pol.id " +
                    "inner join firolpopf rolpo on pol.id = rolpo.policy_id " +
                    "inner join lsdrolepf role on role.id = rolpo.role_id " +
                    "where pol.status_id = 6 " +
                    "and rolpo.active = 'Y' " +
                    "ORDER by rand() " +
                    "LIMIT 1";
        }
    },
    FI5 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 10000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID = 18 " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI6 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 15000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID = 18 " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI7 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 20000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI8 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 30000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI9 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 40000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI10 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 50000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI11 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 60000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI12 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI13 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 80000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI14 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 90000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI15 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where cover = 100000 " +
                    "and id not in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI16 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 18 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' and c.amount = 60000 " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,634) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI17 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 19 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (19,634) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI18 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 631 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (631,634) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI19 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 932 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (932,634) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    FI20 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.AMOUNT ) as cover FROM funins_p.fipolmpf " +
                    "a inner join funins_p.FIROLPOPF b ON a.ID = b.POLICY_ID INNER JOIN " +
                    "funins_p.FICOVERPF c ON b.POLICY_ID = c.POLICY_ID WHERE " +
                    "b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 2 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    ") " +
                    "select * from mytempfile " +
                    "where id in " +
                    "( " +
                    "SELECT POLICY_ID FROM funins_p.FIROLPOPF b WHERE ROLETYP_ID in (18,19,631,634,932) " +
                    "and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    R_FI1 {
        public String toString() {
            return "Select Policyno from funins_p.fipolmpf where status_id = 7 and statusdte > current timestamp - 30 days order by rand () limit 1";
        }
    },
    R_FI2 {
        public String toString() {
            return "Select Policyno from funins_p.fipolmpf where status_id = 7 and statusdte < current timestamp - 30 days order by rand () limit 1";
        }
    },
    C_FI1 {
        public String toString() {
            return "Select Policyno from funins_p.fipolmpf where status_id = 9 order by rand () limit 1";
        }
    },
    // endregion

    // region LOC:
    LOC1 {
        public String toString() {
            return "with mytempfile as ( " +
                    "select a.idfi1" +
                    ", a.policyno , sum(c.totalamnt) as totalamnt " +
                    "from LEGALPLIB.LCPOLMPF a " +
                    "inner join LEGALPLIB.lcrolpopf b on a.id = b.policy_id " +
                    "inner join LEGALPLIB.lccoverpf c on b.policy_id = c.policy_id " +
                    "where b.roletyp_id = 17 and a.status_id = 6 " +
                    "and b.active = 'Y' and a.product_id = 11 " +
                    "AND c.active = 'Y' " +
                    "group BY a.id, a.policyno ) " +
                    "Select * FROM MYTEMPFILE where totalamnt = 2000000 and id not in " +
                    "(select policy_id from legalplib.lcrolpopf  where roletyp_id = 18 and active = 'Y' )" +
                    " order by rand () limit 1";
        }
    },
    LOC2 {
        public String toString() {
            return "select a.id,a.policyno  " +
                    "from LEGALPLIB.LCPOLMPF a " +
                    "inner join LEGALPLIB.lcrolpopf b on a.id = b.policy_id " +
                    "inner join LEGALPLIB.lccoverpf c on b.policy_id = c.policy_id " +
                    "where a.status_id = 6 and a.product_id = 11  and b.active = 'Y' and b.roletyp_id = 18 and c.active = 'Y' " +
                    "ORDER BY rand () LIMIT 1 " ;
        }
    },
    LOC3 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 6 AND status_id = '6' and inceptdte = DATE(NOW()) order by rand () limit 1";
        }
    },
    LOC4 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 11  AND status_id = '6' order by rand () limit 1 ";
        }
    },
    N_LOC1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (5,6) AND status_id = '1104' order by rand () limit 1";
        }
    },
    P_LOC1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (5,6) AND status_id = '1103' order by rand () limit 1";
        }
    },
    R_LOC1 {
        public String toString() {
            return "SELECT policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 11 AND status_id = '7' order by rand () limit 1";
        }
    },
    R_LOC2 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 11 AND status_id = '7' order by rand () limit 1";
        }
    },
    C_LOC1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 11  AND status_id = '9' order by rand () limit 1";
        }
    },
    // Business
    LOCB1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 7 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB2 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 8 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB3 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 9 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB4 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 10 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB5 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (7,8,9,10) AND status_id = '6' order by rand () limit 1";
        }
    },
    LOCB6 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 12 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB7 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 13 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB8 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 14 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    LOCB9 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id = 15 AND status_id = '6' ORDER BY rand () limit 1";
        }
    },
    N_LOCB1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (7,8,9,10) AND status_id = '1104' ORDER BY rand () limit 1";
        }
    },
    P_LOCB1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (7,8,9,10) AND status_id = '1103' ORDER BY rand () limit 1";
        }
    },
    R_LOCB1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (7,8,9,10) AND status_id = '7' ORDER BY rand () limit 1";
        }
    },
    C_LOCB1 {
        public String toString() {
            return "SELECT Policyno FROM LEGALPLIB.LCPOLMPF WHERE product_id in (7,8,9,10) AND status_id = '9' ORDER BY rand () limit 1";
        }
    },
    // endregion

    // region Accidental Death:
    AD1 {
        public String toString() {
            return "SELECT * FROM PA_PLIB.PAPOLMPF  WHERE status_id =6 and product ID = 1 ORDER BY RAND() limit 1";
        }
    },
    AD2 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 100000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD3 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 150000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD4 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 200000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD5 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 250000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD6 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 300000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD7 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 350000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD8 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 400000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD9 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 500000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD10 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 600000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD11 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 700000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD12 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 800000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD13 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 900000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD14 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1000000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD15 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1100000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD16 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1200000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD17 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1300000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD18 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1400000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD19 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1500000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD20 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 1750000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD21 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 2000000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD22 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 2250000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD23 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 2500000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD24 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 2750000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD25 {
        public String toString() {
            return "with mytempfile as ( " +
                    "SELECT A.id, A.POLICYNO , sum( C.amount ) as amount " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 17 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO " +
                    " ) " +
                    "select * from mytempfile " +
                    "where amount = 3000000 " +
                    "and id NOT in " +
                    "( " +
                    "SELECT policyno FROM PA_PLIB.PAROLPOPF   b WHERE ROLETYP_ID = 18 and ACTIVE = 'Y' " +
                    ") " +
                    "ORDER BY rand () " +
                    "limit 1 ";
        }
    },
    AD26 {
        public String toString() {
            return "SELECT POLICYNO FROM PA_PLIB.PAPOLMPF WHERE PRODUCT_ID = 2 AND  STATUS_ID = 6";
        }
    },
    AD27 {
        public String toString() {
            return "SELECT A.id, A.POLICYNO " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 18 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO ORDER BY rand () limit 1";
        }
    },
    AD28 {
        public String toString() {
            return "SELECT A.id, A.POLICYNO " +
                    "FROM PA_PLIB.PAPOLMPF a " +
                    "inner join PA_PLIB.PAROLPOPF b ON a.ID = b.POLICY_ID " +
                    "INNER JOIN  PA_PLIB.PACOVERPF c ON b.POLICY_ID = c.POLICY_ID " +
                    "WHERE b.ROLETYP_ID = 19 and B.ACTIVE = 'Y' AND a.PRODUCT_ID = 1 AND " +
                    "a.STATUS_ID = 6 AND C.ACTIVE = 'Y' " +
                    "GROUP BY A.id,A.POLICYNO ORDER BY rand () limit 1";
        }
    },
    AD29 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 200000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD30 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 300000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD31 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 400000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD32 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 500000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD33 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 600000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD34 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 700000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD35 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 800000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD36 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 900000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD37 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1000000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD38 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1100000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD39 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1200000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD40 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1300000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1 ";
        }
    },
    AD41 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1400000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD42 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1500000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD43 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1750000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD44 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2000000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD45 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2250000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD46 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2500000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD47 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2750000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD48 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 3000000 \n" +
                    "and spouse_count = 0\n" +
                    "and child_count  != 0\n" +
                    "order by rand () limit 1";
        }
    },
    AD49 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 200000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD50 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 300000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD51 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO,\n" +
                    "sum(PACOVERPF.AMOUNT) as cover_amount,\n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count,\n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF, PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF \n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and  PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\"= PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID= LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") select * from a \n" +
                    "where cover_amount = 200000 \n" +
                    "and spouse_count = 1 \n" +
                    "and child_count  = 2 \n" +
                    "order by rand () limit 1";
        }
    },
    AD52 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO, \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF, PA_PLIB.PAROLPOPF as PAROLPOPF\n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and  PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and  PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ")\n" +
                    "select * from a where cover_amount = 500000 \n" +
                    "and spouse_count = 1 \n" +
                    "order by rand () limit 1";
        }
    },
    AD53 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count , \n" +
                    "sum( case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF\n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and  PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and   PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and  PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ")\n" +
                    "select * from a where cover_amount = 600000\n" +
                    "and spouse_count = 1 \n" +
                    "order by rand () limit 1";
        }
    },
    AD54 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 700000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD55 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 800000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD56 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 900000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD57 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1000000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD58 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1100000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD59 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1200000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD60 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1300000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD61 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1400000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD62 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1500000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD63 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 1750000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD64 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2000000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD65 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2250000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD66 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2500000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD67 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 2750000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },
    AD68 {
        public String toString() {
            return "with a as (\n" +
                    "SELECT PAPOLMPF.POLICYNO , \n" +
                    "sum( PACOVERPF.AMOUNT ) as cover_amount, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 18 then 1 else 0 end) as spouse_count, \n" +
                    "sum(case when PAROLPOPF.ROLETYP_ID = 19 then 1 else 0 end) as child_count \n" +
                    "FROM PA_PLIB.PAPOLMPF as PAPOLMPF , PA_PLIB.PAROLPOPF as PAROLPOPF \n" +
                    "left outer join PA_PLIB.PACOVERPF as PACOVERPF\n" +
                    "on PAROLPOPF.ROLETYP_ID = 17 \n" +
                    "and PAROLPOPF.ROLE_ID = PACOVERPF.ROLE_ID \n" +
                    "and PACOVERPF.ACTIVE = 'Y' , \n" +
                    "LEGALPLIB.LSDROLEPF as LSDROLEPF\n" +
                    "WHERE PAPOLMPF.FREEPAID = 'P' \n" +
                    "and PAPOLMPF.STATUS_ID = 6 \n" +
                    "and PAPOLMPF.\"ID\" = PAROLPOPF.POLICY_ID \n" +
                    "and PAROLPOPF.ROLETYP_ID in (17,18,19) \n" +
                    "and PAROLPOPF.ACTIVE = 'Y' \n" +
                    "and PAROLPOPF.ROLE_ID = LSDROLEPF.\"ID\" \n" +
                    "and LSDROLEPF.SURNAME <> ' ' \n" +
                    "GROUP BY PAPOLMPF.POLICYNO \n" +
                    "ORDER BY PAPOLMPF.POLICYNO desc\n" +
                    ") \n" +
                    "select * from a where cover_amount = 3000000 \n" +
                    "and spouse_count = 1\n" +
                    "order by rand () limit 1";
        }
    },

    // endregion

    // region Pay Protect:
    PP1 {
        public String toString() {
            return "SELECT * FROM PP_PLIB.PPPOLMPF WHERE status_id =6 ORDER BY RAND() limit 1";
        }
    },
    PP2 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA   \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF\n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 24000 AND 35999 ORDER BY Rand () LIMIT 1";
        }
    },
    PP3 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA   \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF\n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 36000 AND 47999 ORDER BY Rand () LIMIT 1";
        }
    },
    PP4 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF \n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 48000 AND 59999 ORDER BY Rand () LIMIT 1\"";
        }
    },
    PP5 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF \n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 60000 AND 119999 ORDER BY Rand () LIMIT 1\" ";
        }
    },
    PP6 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF \n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 120000 AND 179999 ORDER BY Rand () LIMIT 1\" ";
        }
    },
    PP7 {
        public String toString() {
            return "SELECT PPPOLMPF.POLICYNO, LSDROLEPF.IDNUMBER, LSDROLEPF.INCOMEPA \n" +
                    "FROM PP_PLIB/PPPOLMPF, PP_PLIB/PPROLPOPF , LEGALPLIB/LSDROLEPF \n" +
                    "WHERE PPPOLMPF.STATUS_ID = 6 and PPPOLMPF.ID = PPROLPOPF.POLICY_ID \n" +
                    "and PPROLPOPF.ROLETYP_ID =17 and PPROLPOPF.ROLE_ID= LSDROLEPF.ID AND LSDROLEPF.INCOMEPA BETWEEN 180000 AND 999999999 ORDER BY Rand () LIMIT 1\" ";
        }
    },
    C_PP1 {
        public String toString() {
            return "SELECT * FROM PP_PLIB.PPPOLMPF WHERE status_id = 9 ORDER BY RAND() limit 1";
        }
    },
    // endregion

    // region Cover For Life:
    C4L1 {
        public String toString() {
            return "SELECT * FROM cov4lifep.c4lpolmpf WHERE status_id =6 ORDER BY RAND() limit 1";
        }
    },
    C_C4L1 {
        public String toString() {
            return "SELECT * FROM cov4lifep.c4lpolmpf WHERE status_id = 9 ORDER BY RAND() limit 1";
        }
    },
    // endregion

    // endregion
}
