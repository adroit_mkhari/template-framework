package co.za.fnb.flow.pages.generic_maintanence.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class GenericMaintenancePageObjects {

    @FindBy(id = "createSearchItemView:mainTabView:refundView:transactionName")
    private WebElement transactionName;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:transactionName_label")
    private WebElement transactionNameSelect;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:transactionName_items")
    private WebElement transactionNameSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:reason_label")
    private WebElement reasonSelect;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:reason_items")
    private WebElement reasonSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:otherTextArea")
    private WebElement otherTextArea;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:amount")
    private WebElement amount;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:amount_input")
    private WebElement amountInput;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:actionDate_input")
    private WebElement actionDate;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:fromDate_input")
    private WebElement fromDate;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:toDate_input")
    private WebElement toDate;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:memberRadio")
    private WebElement memberRadio;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:newMember")
    private WebElement newMember;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:refundMemberTable:0:tableListMemberSelect_label")
    private WebElement membersSelect;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:refundMemberTable:0:tableListMemberSelect_items")
    private WebElement membersSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:refundInactiveMemberTable:0:tableListMemberSelect_label")
    private WebElement inactiveMembersSelect;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:refundInactiveMemberTable:0:tableListMemberSelect_items")
    private WebElement inactiveMembersSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:refundMemberTable_data")
    private WebElement refundMemberTableData;

    @FindBy(id = "//*[contains(@id,'MemberFromDate_input')]")
    private WebElement memberFromDate;

    @FindBy(id = "//*[contains(@id, 'MemberToDate_input')]")
    private WebElement memberToDate;

    @FindBy(id = "//tbody[@id='createSearchItemView:mainTabView:refundView:refundMemberTable_data']/tr/td[3]")
    private WebElement memberFromDateVis;

    @FindBy(id = "//tbody[@id='createSearchItemView:mainTabView:refundView:refundMemberTable_data']/tr/td[4]")
    private WebElement memberToDateVis;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:customPanel")
    private WebElement customPanel; // Random

    @FindBy(id = "createSearchItemView:mainTabView:refundView:calculate")
    private WebElement calculate;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:payee")
    private WebElement payee;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:accountNumber")
    private WebElement accountNumber;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:branchCode")
    private WebElement branchCode;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:accountType")
    private WebElement accountType;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:accountType_label")
    private WebElement accountTypeSelect;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:accountType_items")
    private WebElement accountTypeSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:refundView:saveRefund")
    private WebElement save;

    public GenericMaintenancePageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getTransactionName() {
        return transactionName;
    }

    public WebElement getTransactionNameSelect() {
        return transactionNameSelect;
    }

    public WebElement getTransactionNameSelectItems() {
        return transactionNameSelectItems;
    }

    public WebElement getReasonSelect() {
        return reasonSelect;
    }

    public WebElement getReasonSelectItems() {
        return reasonSelectItems;
    }

    public WebElement getOtherTextArea() {
        return otherTextArea;
    }

    public WebElement getAmount() {
        return amount;
    }

    public WebElement getAmountInput() {
        return amountInput;
    }

    public WebElement getActionDate() {
        return actionDate;
    }

    public WebElement getFromDate() {
        return fromDate;
    }

    public WebElement getToDate() {
        return toDate;
    }

    public WebElement getMemberRadio() {
        return memberRadio;
    }

    public WebElement getNewMember() {
        return newMember;
    }

    public WebElement getMembersSelect() {
        return membersSelect;
    }

    public WebElement getMembersSelectItems() {
        return membersSelectItems;
    }

    public WebElement getInactiveMembersSelect() {
        return inactiveMembersSelect;
    }

    public WebElement getInactiveMembersSelectItems() {
        return inactiveMembersSelectItems;
    }

    public WebElement getRefundMemberTableData() {
        return refundMemberTableData;
    }

    public WebElement getMemberFromDate() {
        return memberFromDate;
    }

    public WebElement getMemberToDate() {
        return memberToDate;
    }

    public WebElement getMemberFromDateVis() {
        return memberFromDateVis;
    }

    public WebElement getMemberToDateVis() {
        return memberToDateVis;
    }

    public WebElement getCustomPanel() {
        return customPanel;
    }

    public WebElement getCalculate() {
        return calculate;
    }

    public WebElement getPayee() {
        return payee;
    }

    public WebElement getAccountNumber() {
        return accountNumber;
    }

    public WebElement getBranchCode() {
        return branchCode;
    }

    public WebElement getAccountType() {
        return accountType;
    }

    public WebElement getAccountTypeSelect() {
        return accountTypeSelect;
    }

    public WebElement getAccountTypeSelectItems() {
        return accountTypeSelectItems;
    }

    public WebElement getSave() {
        return save;
    }
}
