package co.za.fnb.flow.pages.administration.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AddAttorneyPageObjects extends BasePage {
    // region Attorney Details
    @FindBy(id = "frmAddUser:companyName")
    private WebElement companyName;

    @FindBy(id = "frmAddUser:vatFlag")
    private WebElement vatFlag;

    @FindBy(id = "frmAddUser:vatRegNo")
    private WebElement vatRegNo;

    @FindBy(id = "frmAddUser:email")
    private WebElement email;

    @FindBy(id = "frmAddUser:cellNo")
    private WebElement cellNo;

    @FindBy(id = "frmAddUser:workNo")
    private WebElement workNo;

    @FindBy(id = "frmAddUser:attorneyType")
    private WebElement attorneyType;

    @FindBy(id = "frmAddUser:attorneyType_label")
    private WebElement attorneyTypeLabel;

    @FindBy(id = "frmAddUser:attorneyType_items")
    private WebElement attorneyTypeItems; // Uses @data-label
    // endregion

    // region Attorney Address Details
    @FindBy(id = "frmAddUser:addressType")
    private WebElement addressType;

    @FindBy(id = "frmAddUser:addressType_label")
    private WebElement addressTypeLabel;

    @FindBy(id = "frmAddUser:addressType_items")
    private WebElement addressTypeItems; // Uses @data-label

    @FindBy(id = "frmAddUser:addressLineOne")
    private WebElement addressLineOne;

    @FindBy(id = "frmAddUser:addressLineTwo")
    private WebElement addressLineTwo;

    @FindBy(id = "frmAddUser:surbur")
    private WebElement suburb;

    @FindBy(id = "frmAddUser:country")
    private WebElement country;
    // endregion

    // region Attorney Bank Details
    @FindBy(id = "frmAddUser:bankName")
    private WebElement bankName;

    @FindBy(id = "frmAddUser:bankName_label")
    private WebElement bankNameLabel;

    @FindBy(id = "frmAddUser:bankName_items")
    private WebElement bankNameItems; // Uses @data-label

    @FindBy(id = "frmAddUser:branchCode")
    private WebElement branchCode;

    @FindBy(id = "frmAddUser:accountType")
    private WebElement accountType;

    @FindBy(id = "frmAddUser:accountType_label")
    private WebElement accountTypeLabel;

    @FindBy(id = "frmAddUser:accountType_items")
    private WebElement accountTypeItems; // Uses @data-label

    @FindBy(id = "frmAddUser:accountNumber")
    private WebElement accountNumber;

    @FindBy(id = "frmAddUser:accountholder")
    private WebElement accountHolder;
    // endregion

    //  region Service Area
    @FindBy(id = "frmAddUser:frmAddServiceArea:province")
    private WebElement province;

    @FindBy(id = "frmAddUser:frmAddServiceArea:province_label")
    private WebElement provinceLabel;

    @FindBy(id = "frmAddUser:frmAddServiceArea:province_items")
    private WebElement provinceItems; // Uses @data-label

    @FindBy(id = "frmAddUser:frmAddServiceArea:city")
    private WebElement city;

    @FindBy(xpath = "//*[@id=\"frmAddUser:frmAddServiceArea:city_panel\"]/div[1]/div[1]/div[2]/span")
    private WebElement allCities;

    @FindBy(xpath = "//*[@id=\"frmAddUser:frmAddServiceArea:city_panel\"]/div[2]/ul/li[2]/label")
    private WebElement firstCity;

    @FindBy(id = "frmAddUser:frmAddServiceArea:btnMakePaymentadd")
    private WebElement add;
    // endregion

    // region Buttons
    @FindBy(id = "btnAddUserSubmit")
    private WebElement addUserSubmit;

    @FindBy(id = "btnAddUserCancel")
    private WebElement addUserCancel;
    // endregion

    public AddAttorneyPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getCompanyName() {
        return companyName;
    }

    public WebElement getVatFlag() {
        return vatFlag;
    }

    public WebElement getVatRegNo() {
        return vatRegNo;
    }

    public WebElement getEmail() {
        return email;
    }

    public WebElement getCellNo() {
        return cellNo;
    }

    public WebElement getWorkNo() {
        return workNo;
    }

    public WebElement getAttorneyType() {
        return attorneyType;
    }

    public WebElement getAttorneyTypeLabel() {
        return attorneyTypeLabel;
    }

    public WebElement getAttorneyTypeItems() {
        return attorneyTypeItems;
    }

    public WebElement getAddressType() {
        return addressType;
    }

    public WebElement getAddressTypeLabel() {
        return addressTypeLabel;
    }

    public WebElement getAddressTypeItems() {
        return addressTypeItems;
    }

    public WebElement getAddressLineOne() {
        return addressLineOne;
    }

    public WebElement getAddressLineTwo() {
        return addressLineTwo;
    }

    public WebElement getSuburb() {
        return suburb;
    }

    public WebElement getCountry() {
        return country;
    }

    public WebElement getBankName() {
        return bankName;
    }

    public WebElement getBankNameLabel() {
        return bankNameLabel;
    }

    public WebElement getBankNameItems() {
        return bankNameItems;
    }

    public WebElement getBranchCode() {
        return branchCode;
    }

    public WebElement getAccountType() {
        return accountType;
    }

    public WebElement getAccountTypeLabel() {
        return accountTypeLabel;
    }

    public WebElement getAccountTypeItems() {
        return accountTypeItems;
    }

    public WebElement getAccountNumber() {
        return accountNumber;
    }

    public WebElement getAccountHolder() {
        return accountHolder;
    }

    public WebElement getProvince() {
        return province;
    }

    public WebElement getProvinceLabel() {
        return provinceLabel;
    }

    public WebElement getProvinceItems() {
        return provinceItems;
    }

    public WebElement getCity() {
        return city;
    }

    public WebElement getAllCities() {
        return allCities;
    }

    public WebElement getFirstCity() {
        return firstCity;
    }

    public WebElement getAdd() {
        return add;
    }

    public WebElement getAddUserSubmit() {
        return addUserSubmit;
    }

    public WebElement getAddUserCancel() {
        return addUserCancel;
    }
}
