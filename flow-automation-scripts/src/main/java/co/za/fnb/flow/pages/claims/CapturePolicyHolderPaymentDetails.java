package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.CapturePolicyHolderPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class CapturePolicyHolderPaymentDetails extends BasePage {
    private Logger log  = LogManager.getLogger(CapturePolicyHolderPaymentDetails.class);
    CapturePolicyHolderPaymentPageObjects capturePolicyHolderPaymentPageObjects = new CapturePolicyHolderPaymentPageObjects(driver);

    public CapturePolicyHolderPaymentDetails(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public void clickReceivedDatePolicyHolderInput() throws Exception {
        try {
            click(capturePolicyHolderPaymentPageObjects.getReceivedDatePolicyHolderInput());
        } catch (Exception e) {
            log.error("Error while clicking on Received Date Policy Holder Input");
            throw new Exception("Error while clicking on Received Date Policy Holder Input");
        }
    }

    public void doubleClickReceivedDatePolicyHolderInput() throws Exception {
        try {
            moveToElementAndDoubleClick(capturePolicyHolderPaymentPageObjects.getReceivedDatePolicyHolderInput());
        } catch (Exception e) {
            log.error("Error while double clicking on Received Date Policy Holder Input");
            throw new Exception("Error while double clicking on Received Date Policy Holder Input");
        }
    }

    public void updateReceivedDatePolicyHolderInput(String receivedDate) throws Exception {
        try {
            type(capturePolicyHolderPaymentPageObjects.getReceivedDatePolicyHolderInput(), receivedDate);
        } catch (Exception e) {
            log.error("Error while updating Received Date Policy Holder Input");
            throw new Exception("Error while updating Received Date Policy Holder Input");
        }
    }

    public void clickAolAmounnt() throws Exception {
        try {
            click(capturePolicyHolderPaymentPageObjects.getAolAmountInput());
        } catch (Exception e) {
            log.error("Error while clicking on AOL Amount.");
            throw new Exception("Error while clicking on AOL Amount.");
        }
    }

    public void doubleAolAmounnt() throws Exception {
        try {
            moveToElementAndDoubleClick(capturePolicyHolderPaymentPageObjects.getAolAmountInput());
        } catch (Exception e) {
            log.error("Error while double clicking on AOL Amount.");
            throw new Exception("Error while double clicking on AOL Amount.");
        }
    }

    public void updateAolAmounnt(String aolAmount) throws Exception {
        try {
            doubleAolAmounnt();
            type(capturePolicyHolderPaymentPageObjects.getAolAmountInput(), aolAmount);
        } catch (Exception e) {
            log.error("Error while updating AOL Amount.");
            throw new Exception("Error while updating AOL Amount.");
        }
    }

    public void clickPaymentStageInterimPolicyHolder() throws Exception {
        try {
            moveToElementAndClick(capturePolicyHolderPaymentPageObjects.getPaymentStageInterimPolicyHolder());
        } catch (Exception e) {
            log.error("Error while clicking on Payment Stage Interim Policy Holder.");
            throw new Exception("Error while clicking on Payment Stage Interim Policy Holder.");
        }
    }

    public void clickPaymentStageFinalPolicyHolder() throws Exception {
        try {
            moveToElementAndClick(capturePolicyHolderPaymentPageObjects.getPaymentStageFinalPolicyHolder());
        } catch (Exception e) {
            log.error("Error while clicking on Payment Stage Final Policy Holder.");
            throw new Exception("Error while clicking on Payment Stage Final Policy Holder.");
        }
    }

    public void clickBtnMakePolicyHolderPaymentSubmit() throws Exception {
        try {
            click(capturePolicyHolderPaymentPageObjects.getBtnMakePolicyHolderPaymentSubmit());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Policy Holder Payment Submit.");
            throw new Exception("Error while clicking on Btn Make Policy Holder Payment Submit.");
        }
    }

    public void clickBtnMakePolicyHolderPaymentCancel() throws Exception {
        try {
            click(capturePolicyHolderPaymentPageObjects.getBtnMakePolicyHolderPaymentCancel());
        } catch (Exception e) {
            log.error("Error while clicking on Btn Make Policy Holder Payment Cancel.");
            throw new Exception("Error while clicking on Btn Make Policy Holder Payment Cancel.");
        }
    }

}
