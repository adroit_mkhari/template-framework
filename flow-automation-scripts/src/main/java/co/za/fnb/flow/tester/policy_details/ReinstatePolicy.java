package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

public class ReinstatePolicy {
    private Logger log  = LogManager.getLogger(ReinstatePolicy.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    ReinstatePolicyPopupDialog reinstatePolicyPopupDialog;
    BeneficiaryInformation beneficiaryInformation;
    MemberInformation memberInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public ReinstatePolicy(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        reinstatePolicyPopupDialog = new ReinstatePolicyPopupDialog(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void reinstate(String reinstate, String confirmWithArrears, String reinstatePolicyConfirmation, String acceptTerms, String confirmArrears) throws Exception {
        // region Reinstate Policy Logic:
        log.info("Handling Reinstate Policy logic");
        policyInformation.reinstatePolicy();
        Thread.sleep(5000);
        // TODO: Cater for the create update table frame
        // String reinstate = dataHandler.getCellValue(headers, testData, "Reinstate");
        if (reinstate.equalsIgnoreCase("Yes")) {
            reinstatePolicyPopupDialog.yesCalculateArrears();
            Thread.sleep(1000);
            policyInformation.getResponses(true);
            Thread.sleep(10000);
            if (confirmWithArrears.contains("Yes")) {
                reinstatePolicyPopupDialog.yesContinue();
                Thread.sleep(2000);
                reinstatePolicyPopupDialog.updateDebitOrderDate(member.getDebitOrderDate());
                Thread.sleep(500);
                reinstatePolicyPopupDialog.updateNextDueDate(member.getNextDueDate());
                Thread.sleep(500);
                // TODO: Check if we always want to say ok and not cancel sometimes.
                reinstatePolicyPopupDialog.yesReinstateWithArrears();
                Thread.sleep(2000);
                if (reinstatePolicyConfirmation.equalsIgnoreCase("No")) {
                    reinstatePolicyPopupDialog.noReinstatePolicy();
                } else {
                    reinstatePolicyPopupDialog.yesReinstatePolicy();
                    Thread.sleep(1000);
                    if (acceptTerms.equalsIgnoreCase("No"))
                    {
                        log.info("Terms Not Accepted");
                        reinstatePolicyPopupDialog.deleteCommentsViewButton();
                    } else {
                        reinstatePolicyPopupDialog.deleteCommentsView();
                        reinstatePolicyPopupDialog.deleteCommentsViewButton();
                    }
                }
            } else {
                reinstatePolicyPopupDialog.noHide();
                Thread.sleep(1000);

                if (confirmArrears.equalsIgnoreCase("No")) {
                    reinstatePolicyPopupDialog.noReinstatePolicy();
                    Thread.sleep(1000);
                } else {
                    reinstatePolicyPopupDialog.yesReinstatePolicy();
                    Thread.sleep(1000);
                    reinstatePolicyPopupDialog.deleteCommentsView();
                    Thread.sleep(1000);
                    reinstatePolicyPopupDialog.deleteCommentsViewButton();
                    Thread.sleep(1000);
                }
            }
            policyInformation.getResponses(true);
        } else {
            reinstatePolicyPopupDialog.noCalculateArrears();
            Thread.sleep(1000);
        }
        // endregion
    }

    private Double getPolicyMembersTotalPremium() throws Exception {
        Double totalPremium = 0.00;
        String premium;
        Double premiumValue;
        try {
            int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

            for(int j = 0; j < numberOfExistingMembers ; j++) {
                premium = memberInformation.getPolicyDetailsPremium(j);
                premium = getDoubleString(premium);
                premiumValue = stringToDouble(premium);
                totalPremium += premiumValue;
            }
            return totalPremium;
        } catch (Exception e) {
            log.error("Error while getting policy members total premium.");
            throw new Exception("Error while getting policy members total premium.");
        }
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    public void save() throws Exception {
        try {
            log.info("Saving Cancel Member.");
            String bankName = member.getBankName();
            String debitOrderDate = member.getDebitOrderDate();
            String nextDueDate = member.getNextDueDate();

            // Note: Should our premium status change to have more of these popup values we need to update these.
            String[] popUpWorkType = {member.getPopupWorkTypeZero(), member.getPopupWorkTypeOne()};
            String[] popUpStatus = {member.getPopupStatusZero(), member.getPopupStatusOne()};
            String[] popUpQueue = {member.getPopupQueueZero(), member.getPopupQueueOne()};
            // TODO: Check if we have any of these values for Premium Status ? For now we don't but still to confirm with @Shirley if we may possibly have at some point.
            String commentCategoryValue = "";
            String commentValue = "";

            if (!bankName.isEmpty()) {
                bankInformation.changeBankName(bankName);
            }

            if (!debitOrderDate.isEmpty()) {
                bankInformation.changeDebitOrderDate(debitOrderDate);
            }

            if (!nextDueDate.isEmpty()) {
                bankInformation.changeNextDueDate(nextDueDate);
            }

            if (commentCategoryValue.isEmpty() && commentValue.isEmpty()) {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue);
            } else {
                policyInformation.save(popUpWorkType, popUpStatus, popUpQueue, commentCategoryValue, commentValue);
            }

            testHandle = policyInformation.getTestHandle();
            errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() throws Exception {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}