package co.za.fnb.flow.models;

public class Query {
    private String policyCode;
    private String query;
    private String description;

    public Query(String policyCode, String query, String description) {
        this.policyCode = policyCode;
        this.query = query;
        this.description = description;
    }

    public String getPolicyCode() {
        return policyCode;
    }

    public void setPolicyCode(String policyCode) {
        this.policyCode = policyCode;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}