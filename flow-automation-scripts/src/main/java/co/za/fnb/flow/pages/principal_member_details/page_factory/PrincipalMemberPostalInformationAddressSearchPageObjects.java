package co.za.fnb.flow.pages.principal_member_details.page_factory;

import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPostalInformationAddressSearchPageObjects extends PrincipalMemberPostalInformationPageObjects {

    @FindBy(id = "addressSearchView:location")
    private WebElement location;

    @FindBy(id = "addressSearchView:postalCode")
    private WebElement postalCode;

    @FindBy(id = "addressSearchView:searchAddressButton")
    private WebElement searchAddressButton;

    @FindBy(id = "addressSearchView:clearSearchButton")
    private WebElement clearSearchButton;

    @FindBy(id = "addressSearchView:addressTable:0:addressSearchhash")
    private WebElement addressSearchFirstItem;

    public PrincipalMemberPostalInformationAddressSearchPageObjects(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getLocation() {
        return location;
    }

    public WebElement getPostalCode() {
        return postalCode;
    }

    public WebElement getSearchAddressButton() {
        return searchAddressButton;
    }

    public WebElement getClearSearchButton() {
        return clearSearchButton;
    }

    public WebElement getAddressSearchFirstItem() {
        return addressSearchFirstItem;
    }
}
