package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.CreateOrUpdateWorkItemPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class CreateOrUpdateWorkItem extends CreateSearchItemPage {
    private Logger log  = LogManager.getLogger(CreateOrUpdateWorkItem.class);
    CreateOrUpdateWorkItemPageObjects createOrUpdateWorkItemPageObjects = new CreateOrUpdateWorkItemPageObjects(driver);

    public CreateOrUpdateWorkItem(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public String getReferenceNumber() throws Exception {
        try {
            return getAttribute(createOrUpdateWorkItemPageObjects.getReferenceNumber(), "value");
        } catch (Exception e) {
            log.error("Error while getting reference number.");
            throw new Exception("Error while getting reference number.");
        }
    }

    public void inputReferenceNumber(String referenceNumber) throws Exception {
        try {
            type(createOrUpdateWorkItemPageObjects.getReferenceNumber(), referenceNumber);
        } catch (Exception e) {
            log.error("Error while inputting reference number.");
            throw new Exception("Error while inputting reference number.");
        }
    }

    public void selectWorkType(String workType) throws Exception {
        try {
            selectExactMatchingDataLabel(createOrUpdateWorkItemPageObjects.getSelectWorkTypeSelect(), createOrUpdateWorkItemPageObjects.getSelectWorkTypeSelectItems(), workType);
        } catch (Exception e) {
            log.error("Error while selecting work type.");
            throw new Exception("Error while selecting work type.");
        }
    }

    public void selectStatus(String status) throws Exception {
        try {
            selectExactMatchingDataLabel(createOrUpdateWorkItemPageObjects.getStatusSelect(), createOrUpdateWorkItemPageObjects.getStatusSelectItems(), status);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while selecting status.");
            throw new Exception("Error while selecting status.");
        }
    }

    public String getStatus() throws Exception {
        try {
            String status = getText(createOrUpdateWorkItemPageObjects.getStatusSelect());
            log.info("Status: " + status);
            Thread.sleep(1000);
            return status;
        } catch (Exception e) {
            log.error("Error while getting status.");
            throw new Exception("Error while getting status.");
        }
    }

    public void selectQueue(String queue) throws Exception {
        try {
            selectExactMatchingDataLabel(createOrUpdateWorkItemPageObjects.getDocumentWorkQueueSelect(), createOrUpdateWorkItemPageObjects.getDocumentWorkQueueSelectItems(), queue);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while selecting queue.");
            throw new Exception("Error while selecting queue.");
        }
    }

    public void clickRecordDissatisfaction() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getRecordDissatisfaction());
        } catch (Exception e) {
            log.error("Error while clicking Record Dissatisfaction.");
            throw new Exception("Error while clicking Record Dissatisfaction.");
        }
    }

    public String getVoiceLogRef() throws Exception {
        try {
            return getAttribute(createOrUpdateWorkItemPageObjects.getVoiceLogRef(), "value");
        } catch (Exception e) {
            log.error("Error while getting Voice Log Ref.");
            throw new Exception("Error while getting Voice Log Ref.");
        }
    }

    public void inputVoiceLogRef(String voiceLogRef) throws Exception {
        try {
            type(createOrUpdateWorkItemPageObjects.getVoiceLogRef(), voiceLogRef);
        } catch (Exception e) {
            log.error("Error while inputting Voice Log Ref.");
            throw new Exception("Error while inputting Voice Log Ref.");
        }
    }

    public String getCreatedDate() throws Exception {
        try {
            return getAttribute(createOrUpdateWorkItemPageObjects.getCreatedDate(), "value");
        } catch (Exception e) {
            log.error("Error while getting Created Date.");
            throw new Exception("Error while getting Created Date.");
        }
    }

    public void inputCreatedDate(String createdDate) throws Exception {
        try {
            type(createOrUpdateWorkItemPageObjects.getCreatedDate(), createdDate);
        } catch (Exception e) {
            log.error("Error while inputting Created Date.");
            throw new Exception("Error while inputting Created Date.");
        }
    }

    public String getLockedBy() throws Exception {
        try {
            return getAttribute(createOrUpdateWorkItemPageObjects.getLockedBy(), "value");
        } catch (Exception e) {
            log.error("Error while getting Locked By.");
            throw new Exception("Error while getting Locked By.");
        }
    }

    public void inputLockedBy(String lockedBy) throws Exception {
        try {
            type(createOrUpdateWorkItemPageObjects.getLockedBy(), lockedBy);
        } catch (Exception e) {
            log.error("Error while inputting Locked By.");
            throw new Exception("Error while inputting Locked By.");
        }
    }

    public void clickAssignToMe() throws Exception {
        try {
            WebElement assignToMe = createOrUpdateWorkItemPageObjects.getAssignToMe();
            click(assignToMe);
        } catch (Exception e) {
            log.error("Error while clicking Assign To Me.");
            throw new Exception("Error while clicking Assign To Me.");
        }
    }

    public void updateIdNumber(String idNumber) throws Exception {
        try {
            WebElement idNumberField = createOrUpdateWorkItemPageObjects.getIdNumber();
            clear(idNumberField);
            type(idNumberField, idNumber);
        } catch (Exception e) {
            log.error("Error while Update Id Number.");
            throw new Exception("Error while Update Id Number.");
        }
    }

    public void updateUcnNumber(String ucnNumber) throws Exception {
        try {
            WebElement ucnNumberField = createOrUpdateWorkItemPageObjects.getUcnNumber();
            clear(ucnNumberField);
            type(ucnNumberField, ucnNumber);
        } catch (Exception e) {
            log.error("Error while Update UCN Number.");
            throw new Exception("Error while Update UCN Number.");
        }
    }

    public void clickLoadExtWorkTypes() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getLoadExtWorkTypes());
        } catch (Exception e) {
            log.error("Error while clicking LoadExt Work Types.");
            throw new Exception("Error while clicking LoadExt Work Types.");
        }
    }

    public void selectNumberOfClones(String numberOfClones) throws Exception {
        try {
            selectMatchingDataLabel(createOrUpdateWorkItemPageObjects.getNoOfClonesSelect(), createOrUpdateWorkItemPageObjects.getNoOfClonesSelectItems(), numberOfClones);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while selecting Clone View Number Of clones.");
            throw new Exception("Error while selecting Clone View Number Of clones.");
        }
    }

    public void selectCloneQueue(String cloneQueue) throws Exception {
        try {
            selectMatchingDataLabel(createOrUpdateWorkItemPageObjects.getCloneViewQueueSelect(), createOrUpdateWorkItemPageObjects.getCloneViewQueueItems(), cloneQueue);
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while selecting Clone View Queue.");
            throw new Exception("Error while selecting Clone View Queue.");
        }
    }

    public void clickViewClone() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCloneViewClone());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking Clone View Clone.");
            throw new Exception("Error while clicking Clone View Clone.");
        }
    }

    public void clickViewCancel() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCloneViewCancel());
            Thread.sleep(1000);
        } catch (Exception e) {
            log.error("Error while clicking Clone View Cancel.");
            throw new Exception("Error while clicking Clone View Cancel.");
        }
    }

    public void clickAddComments() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getAddComments());
        } catch (Exception e) {
            log.error("Error while clicking Add Comments.");
            throw new Exception("Error while clicking Add Comments.");
        }
    }

    public void inputComment(String comment) throws Exception {
        try {
            driver.switchTo().frame(0);
            type(createOrUpdateWorkItemPageObjects.getCommentsInputTextArea(), comment);
        } catch (Exception e) {
            log.error("Error while typing Comment.");
            throw new Exception("Error while typing Comment.");
        }
    }

    public void clickAddCommentsAdd() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getAddComment());
        } catch (Exception e) {
            log.error("Error while clicking Add Comment Add.");
            throw new Exception("Error while clicking Add Comment Add.");
        }
    }

    public void clickAddCommentsClear() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getClear());
        } catch (Exception e) {
            log.error("Error while clicking Add Comments Clear.");
            throw new Exception("Error while clicking Add Comments Clear.");
        }
    }

    public void clickAddCommentsCancel() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCancel());
        } catch (Exception e) {
            log.error("Error while clicking Add Comments Cancel.");
            throw new Exception("Error while clicking Add Comments Cancel.");
        }
    }

    public String getConfirmMessage() throws Exception {
        try {
            return getText(createOrUpdateWorkItemPageObjects.getConfirmMessage());
        } catch (Exception e) {
            log.error("Error while getting Confirm Message.");
            throw new Exception("Error while getting Confirm Message.");
        }
    }

    public void clickConfirmYes() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getConfirmYes());
        } catch (Exception e) {
            log.error("Error while clicking Confirm Yes.");
            throw new Exception("Error while clicking Confirm Yes.");
        }
    }

    public void clickConfirmNo() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getConfirmNo());
        } catch (Exception e) {
            log.error("Error while clicking Confirm No.");
            throw new Exception("Error while clicking Confirm No.");
        }
    }

    public String getClaimSequenceNumber() throws Exception {
        try {
            return getAttribute(createOrUpdateWorkItemPageObjects.getPaymentSequenceNumber(), "value");
        } catch (Exception e) {
            log.error("Error while getting claim sequence number.");
            throw new Exception("Error while getting claim sequence number.");
        }
    }

    public void clickCorrespondence() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCorrespondence());
        } catch (Exception e) {
            log.error("Error while clicking Correspondence.");
            throw new Exception("Error while clicking Correspondence.");
        }
    }

    public void clickCorrespondenceEmail() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCorrespondenceEmail());
        } catch (Exception e) {
            log.error("Error while clicking Correspondence Email.");
            throw new Exception("Error while clicking Correspondence Email.");
        }
    }

    public void clickCorrespondenceSms() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCorrespondenceSms());
        } catch (Exception e) {
            log.error("Error while clicking Correspondence Sms.");
            throw new Exception("Error while clicking Correspondence Sms.");
        }
    }

    public void clickOpenSms() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCreateSearchItemOpenSms());
        } catch (Exception e) {
            log.error("Error while clicking Open Sms.");
            throw new Exception("Error while clicking Open Sms.");
        }
    }

    public void clickNewSms() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getCreateSearchItemNewSms());
        } catch (Exception e) {
            log.error("Error while clicking New Sms.");
            throw new Exception("Error while clicking New Sms.");
        }
    }

    public void inputSmsViewToAddress(String toAddress) throws Exception {
        try {
            type(createOrUpdateWorkItemPageObjects.getSmsViewToAddress(), toAddress);
        } catch (Exception e) {
            log.error("Error while inputting Sms View To Address.");
            throw new Exception("Error while inputting Sms View To Address.");
        }
    }

    public void selectSmsViewTemplateSelect(String template) throws Exception {
        try {
            selectMatchingDataLabel(createOrUpdateWorkItemPageObjects.getSmsViewTemplateSelect(), createOrUpdateWorkItemPageObjects.getSmsViewTemplateSelectItems(), template);
        } catch (Exception e) {
            log.error("Error while selecting Template.");
            throw new Exception("Error while selecting Template.");
        }
    }

    public void selectSendSms() throws Exception {
        try {
            click(createOrUpdateWorkItemPageObjects.getSmsViewSend());
        } catch (Exception e) {
            log.error("Error while clicking Sms View Send.");
            throw new Exception("Error while clicking Sms View Send.");
        }
    }

}
