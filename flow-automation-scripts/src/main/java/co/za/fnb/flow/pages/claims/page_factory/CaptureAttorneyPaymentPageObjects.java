package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CaptureAttorneyPaymentPageObjects {

    // region Group Information
    @FindBy(id = "frmAddClaimPayment:companyNamePayment")
    WebElement companyNamePayment; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:bankName")
    WebElement bankName; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:branchCode")
    WebElement branchCode; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:accountType")
    WebElement accountType; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:accountNumber")
    WebElement accountNumber; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:accountHolder")
    WebElement accountHolder; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:attorneyTypee")
    WebElement attorneyTypee; // Attorney Payee, Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:policyNumber")
    WebElement policyNumber; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:claimRefNo")
    WebElement claimRefNo; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:claimStatus")
    WebElement claimStatus; // Has attribute 'value'

    @FindBy(id = "frmAddClaimPayment:remainCaseLimit")
    WebElement remainCaseLimit; // Has attribute 'value'
    // endregion

    // region Invoice Details
    @FindBy(id = "frmAddClaimPayment:invoiceNumber")
    WebElement invoiceNumber;

    @FindBy(id = "frmAddClaimPayment:invoiceDate_input")
    WebElement invoiceDateInput;

    @FindBy(id = "frmAddClaimPayment:dateReceived_input")
    WebElement dateReceivedInput;
    // endregion

    // region Invoice Details
    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceType")
    WebElement serviceType;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceType_label")
    WebElement serviceTypeLabel;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceType_items")
    WebElement serviceTypeItems; // Uses @data-label

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:description")
    WebElement description;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:description_label")
    WebElement descriptionLabel;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:description_items")
    WebElement descriptionItems;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:amount_input")
    WebElement amountInput;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:vatFlag")
    WebElement vatFlag;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:vatFlag_label")
    WebElement vatFlagLabel;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:vatFlag_items")
    WebElement vatFlagItems; // Uses @data-label

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:btnMakePaymentadd")
    WebElement btnMakePaymentAdd;

    @FindBy(id = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable_data")
    WebElement serviceInvoiceInfoTableData;

    String policyDetailsHashLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:policyDetailshash";
    String claimInvoiceServiceTypeLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:claimInvoiceServiceType";
    String claimInvoiceDescLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:claimInvoiceDesc";
    String claimInvoiceAmountLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:claimInvoiceAmount";
    String claimInvoiceVATLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:claimInvoiceVAT";
    String claimInvoiceInfoTableEditLocatorTemplate = "frmAddClaimPayment:frmAddClaimPaymentInvoice:serviceInvoiceInfoTable:0:j_idt284";

    // TODO: Added table elements
    // endregion

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:payment:0")
    WebElement paymentStageInterim;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:payment:1")
    WebElement paymentStageFinal;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:totalAttorneyFee_input")
    WebElement totalAttorneyFeeInput;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:totalDisExclVat_input")
    WebElement totalDisExclVatInput;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:totalDisInclVat_input")
    WebElement totalDisInclVatInput;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:totalExGratia_input")
    WebElement totalExGratiaInput;

    @FindBy(id = "frmAddClaimPayment:frmPaymentStage:totalInvoiceAmount_input")
    WebElement totalInvoiceAmountInput;

    @FindBy(id = "btnMakePaymentSubmit")
    WebElement btnMakePaymentSubmit;

    @FindBy(id = "btnMakePaymentCancel")
    WebElement btnMakePaymentCancel;

    public CaptureAttorneyPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getCompanyNamePayment() {
        return companyNamePayment;
    }

    public WebElement getBankName() {
        return bankName;
    }

    public WebElement getBranchCode() {
        return branchCode;
    }

    public WebElement getAccountType() {
        return accountType;
    }

    public WebElement getAccountNumber() {
        return accountNumber;
    }

    public WebElement getAccountHolder() {
        return accountHolder;
    }

    public WebElement getAttorneyTypee() {
        return attorneyTypee;
    }

    public WebElement getPolicyNumber() {
        return policyNumber;
    }

    public WebElement getClaimRefNo() {
        return claimRefNo;
    }

    public WebElement getClaimStatus() {
        return claimStatus;
    }

    public WebElement getRemainCaseLimit() {
        return remainCaseLimit;
    }

    public WebElement getInvoiceNumber() {
        return invoiceNumber;
    }

    public WebElement getInvoiceDateInput() {
        return invoiceDateInput;
    }

    public WebElement getDateReceivedInput() {
        return dateReceivedInput;
    }

    public WebElement getServiceType() {
        return serviceType;
    }

    public WebElement getServiceTypeLabel() {
        return serviceTypeLabel;
    }

    public WebElement getServiceTypeItems() {
        return serviceTypeItems;
    }

    public WebElement getDescription() {
        return description;
    }

    public WebElement getDescriptionLabel() {
        return descriptionLabel;
    }

    public WebElement getDescriptionItems() {
        return descriptionItems;
    }

    public WebElement getAmountInput() {
        return amountInput;
    }

    public WebElement getVatFlag() {
        return vatFlag;
    }

    public WebElement getVatFlagLabel() {
        return vatFlagLabel;
    }

    public WebElement getVatFlagItems() {
        return vatFlagItems;
    }

    public WebElement getBtnMakePaymentAdd() {
        return btnMakePaymentAdd;
    }

    public WebElement getServiceInvoiceInfoTableData() {
        return serviceInvoiceInfoTableData;
    }

    public String getPolicyDetailsHashLocatorTemplate() {
        return policyDetailsHashLocatorTemplate;
    }

    public String getClaimInvoiceServiceTypeLocatorTemplate() {
        return claimInvoiceServiceTypeLocatorTemplate;
    }

    public String getClaimInvoiceDescLocatorTemplate() {
        return claimInvoiceDescLocatorTemplate;
    }

    public String getClaimInvoiceAmountLocatorTemplate() {
        return claimInvoiceAmountLocatorTemplate;
    }

    public String getClaimInvoiceVATLocatorTemplate() {
        return claimInvoiceVATLocatorTemplate;
    }

    public String getClaimInvoiceInfoTableEditLocatorTemplate() {
        return claimInvoiceInfoTableEditLocatorTemplate;
    }

    public WebElement getPaymentStageInterim() {
        return paymentStageInterim;
    }

    public WebElement getPaymentStageFinal() {
        return paymentStageFinal;
    }

    public WebElement getTotalAttorneyFeeInput() {
        return totalAttorneyFeeInput;
    }

    public WebElement getTotalDisExclVatInput() {
        return totalDisExclVatInput;
    }

    public WebElement getTotalDisInclVatInput() {
        return totalDisInclVatInput;
    }

    public WebElement getTotalExGratiaInput() {
        return totalExGratiaInput;
    }

    public WebElement getTotalInvoiceAmountInput() {
        return totalInvoiceAmountInput;
    }

    public WebElement getBtnMakePaymentSubmit() {
        return btnMakePaymentSubmit;
    }

    public WebElement getBtnMakePaymentCancel() {
        return btnMakePaymentCancel;
    }
}
