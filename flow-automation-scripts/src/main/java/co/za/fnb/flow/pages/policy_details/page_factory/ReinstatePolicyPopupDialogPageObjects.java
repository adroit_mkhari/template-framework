package co.za.fnb.flow.pages.policy_details.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class ReinstatePolicyPopupDialogPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:yes_calculateArrears")
    private WebElement yesCalculateArrears;

    @FindBy(id = "createSearchItemView:mainTabView:yes_continue")
    private WebElement yesContinue;

    @FindBy(id = "createSearchItemView:mainTabView:dialog3_5Form:debitOrderDate")
    private WebElement debitOrderDate;

    @FindBy(id = "createSearchItemView:mainTabView:dialog3_5Form:nextDueDate_input")
    private WebElement nextDueDate;

    @FindBy(id = "createSearchItemView:mainTabView:yes_reinstateWithArrears")
    private WebElement yesReinstateWithArrears;

    @FindBy(id = "createSearchItemView:mainTabView:no_reinstatePolicy")
    private WebElement noReinstatePolicy;

    @FindBy(id = "createSearchItemView:mainTabView:yes_reinstatePolicy")
    private WebElement yesReinstatePolicy;

    @FindBy(xpath = "//div[contains(@id, 'createSearchItemView:mainTabView:deleteCommentsView:j')]")
    private WebElement deleteCommentsView;

    @FindBy(xpath = "//Button[contains(@id, 'createSearchItemView:mainTabView:deleteCommentsView:j')] ")
    private WebElement deleteCommentsViewButton;

    @FindBy(id = "createSearchItemView:mainTabView:no_hide")
    private WebElement noHide;

    @FindBy(id = "createSearchItemView:mainTabView:no_calculateArrears")
    private WebElement noCalculateArrears;

    public ReinstatePolicyPopupDialogPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getYesCalculateArrears() {
        return yesCalculateArrears;
    }

    public WebElement getYesContinue() {
        return yesContinue;
    }

    public WebElement getDebitOrderDate() {
        return debitOrderDate;
    }

    public WebElement getNextDueDate() {
        return nextDueDate;
    }

    public WebElement getYesReinstateWithArrears() {
        return yesReinstateWithArrears;
    }

    public WebElement getNoReinstatePolicy() {
        return noReinstatePolicy;
    }

    public WebElement getYesReinstatePolicy() {
        return yesReinstatePolicy;
    }

    public WebElement getDeleteCommentsView() {
        return deleteCommentsView;
    }

    public WebElement getDeleteCommentsViewButton() {
        return deleteCommentsViewButton;
    }

    public WebElement getNoHide() {
        return noHide;
    }

    public WebElement getNoCalculateArrears() {
        return noCalculateArrears;
    }
}
