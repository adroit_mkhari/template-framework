package co.za.fnb.flow.pages.administration;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.administration.page_factory.LegalAdvisorAdminPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

public class LegalAdvisorAdminPage extends BasePage {
    private Logger log  = LogManager.getLogger(LegalAdvisorAdminPage.class);
    LegalAdvisorAdminPageObjects legalAdvisorAdminPageObjects = new LegalAdvisorAdminPageObjects(driver, scenarioOperator);
    Actions actions = new Actions(driver);

    public LegalAdvisorAdminPage(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public String getAttorneyReference(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithDataRiTd(tableCellValueLocatorTemplate, row, 1);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Attorney Reference.");
            throw new Exception("Error while getting Attorney Reference.");
        }
    }

    public void clickAttorneyReference(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 1);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            click(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Attorney Reference.");
            throw new Exception("Error while getting Attorney Reference.");
        }
    }

    public int getAttorneyListTableSize() throws Exception {
        try {
            return getTableDataRiSize(legalAdvisorAdminPageObjects.getMembersTableData());
        } catch (Exception e) {
            log.error("Error while getting Attorney List Table Size.");
            throw new Exception("Error while getting Attorney List Table Size.");
        }
    }

    public void rightClickAttorneyReference(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 1);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            actions.contextClick(tableCellValue).build().perform();
        } catch (Exception e) {
            log.error("Error while right clicking on Attorney Reference.");
            throw new Exception("Error while right clicking on Attorney Reference.");
        }
    }

    public void clickView() throws Exception {
        try {
            actions.moveToElement(legalAdvisorAdminPageObjects.getMenuItemEditView()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on View.");
            throw new Exception("Error while clicking on View.");
        }
    }

    public void clicksuspendOrUnsuspend() throws Exception {
        try {
            actions.moveToElement(legalAdvisorAdminPageObjects.getMenuItemSuspend()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Suspend/Unsuspend.");
            throw new Exception("Error while clicking on Suspend/Unsuspend.");
        }
    }

    public void clickApproveOrReject() throws Exception {
        try {
            actions.moveToElement(legalAdvisorAdminPageObjects.getMenuItemApprove()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while click Approve/Reject.");
            throw new Exception("Error while click Approve/Reject.");
        }
    }

    public String getCompanyName(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 2);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Company Name.");
            throw new Exception("Error while getting on Company Name.");
        }
    }

    public String getVatRegNumber(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 3);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting VAT Reg. Number.");
            throw new Exception("Error while getting VAT Reg. Number.");
        }
    }

    public String getCellPhone(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 4);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Cell Phone.");
            throw new Exception("Error while getting Cell Phone.");
        }
    }

    public String getEmail(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 5);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Email.");
            throw new Exception("Error while getting Email.");
        }
    }

    public String getWorkNumber(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 6);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Work Number.");
            throw new Exception("Error while getting Work Number.");
        }
    }

    public String getAttorneyStatus(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 7);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Attorney Status.");
            throw new Exception("Error while getting Attorney Status.");
        }
    }

    public String getAttorneyType(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 8);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Attorney Type.");
            throw new Exception("Error while getting Attorney Type.");
        }
    }

    public String getLastUpdateDate(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = legalAdvisorAdminPageObjects.getMembersListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 9);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Last Update Date.");
            throw new Exception("Error while getting Last Update Date.");
        }
    }

    public AddAttorneyPage clickAddUser() throws Exception {
        try {
            actions.moveToElement(legalAdvisorAdminPageObjects.getAddUser()).click().build().perform();
            return new AddAttorneyPage(driver, scenarioOperator);
        } catch (Exception e) {
            log.error("Error while clicking on Add User.");
            throw new Exception("Error while clicking on Add User.");
        }
    }

    public void clickClearFilters() throws Exception {
        try {
            actions.moveToElement(legalAdvisorAdminPageObjects.getClearFilters()).click().build().perform();
        } catch (Exception e) {
            log.error("Error while clicking on Clear Filters.");
            throw new Exception("Error while clicking on Clear Filters.");
        }
    }

}
