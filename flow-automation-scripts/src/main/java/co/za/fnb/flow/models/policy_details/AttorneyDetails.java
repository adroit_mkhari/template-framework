package co.za.fnb.flow.models.policy_details;

public class AttorneyDetails extends PolicyDetailsBase {
    private String companyName;
    private String vatRegistered;
    private String vatRegistrationNumber;
    private String emailAddress;
    private String cellNumber;
    private String workNumber;
    private String attorneyType;
    private String addressType;
    private String addressLineOne;
    private String addressLineTwo;
    private String suburb;
    private String bankName;
    private String branchCode;
    private String accountType;
    private String accountNumber;
    private String accountHolder;
    private String province;
    private String city;
    private String addAttorneySubmit;
    private String addAttorneyYesSubmit;
    private String addAttorneyPopUpWorkType;
    private String addAttorneyPopUpStatus;
    private String addAttorneyPopUpQueue;
    private String addAttorneyWorkItemStatusUpdate;
    private String approve;
    private String yesApprove;
    private String approvePopUpWorkType;
    private String approvePopUpStatus;
    private String approvePopUpQueue;

    public AttorneyDetails(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
    }

    public AttorneyDetails(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String firstPopup, String secondPopup, String debitOrderDate, String nextDueDate, String bankName, String expectResult, String workTypeZero, String statusZero, String queueZero, String popupWorkTypeZero, String popupStatusZero, String popupQueueZero, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String companyName, String vatRegistered, String vatRegistrationNumber, String emailAddress, String cellNumber, String workNumber, String attorneyType, String addressType, String addressLineOne, String addressLineTwo, String suburb, String bankName1, String branchCode, String accountType, String accountNumber, String accountHolder, String province, String city, String addAttorneySubmit, String addAttorneyYesSubmit, String addAttorneyPopUpWorkType, String addAttorneyPopUpStatus, String addAttorneyPopUpQueue, String addAttorneyWorkItemStatusUpdate, String approve, String yesApprove, String approvePopUpWorkType, String approvePopUpStatus, String approvePopUpQueue) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, firstPopup, secondPopup, debitOrderDate, nextDueDate, bankName, expectResult, workTypeZero, statusZero, queueZero, popupWorkTypeZero, popupStatusZero, popupQueueZero, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.companyName = companyName;
        this.vatRegistered = vatRegistered;
        this.vatRegistrationNumber = vatRegistrationNumber;
        this.emailAddress = emailAddress;
        this.cellNumber = cellNumber;
        this.workNumber = workNumber;
        this.attorneyType = attorneyType;
        this.addressType = addressType;
        this.addressLineOne = addressLineOne;
        this.addressLineTwo = addressLineTwo;
        this.suburb = suburb;
        this.bankName = bankName1;
        this.branchCode = branchCode;
        this.accountType = accountType;
        this.accountNumber = accountNumber;
        this.accountHolder = accountHolder;
        this.province = province;
        this.city = city;
        this.addAttorneySubmit = addAttorneySubmit;
        this.addAttorneyYesSubmit = addAttorneyYesSubmit;
        this.addAttorneyPopUpWorkType = addAttorneyPopUpWorkType;
        this.addAttorneyPopUpStatus = addAttorneyPopUpStatus;
        this.addAttorneyPopUpQueue = addAttorneyPopUpQueue;
        this.addAttorneyWorkItemStatusUpdate = addAttorneyWorkItemStatusUpdate;
        this.approve = approve;
        this.yesApprove = yesApprove;
        this.approvePopUpWorkType = approvePopUpWorkType;
        this.approvePopUpStatus = approvePopUpStatus;
        this.approvePopUpQueue = approvePopUpQueue;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getVatRegistered() {
        return vatRegistered;
    }

    public void setVatRegistered(String vatRegistered) {
        this.vatRegistered = vatRegistered;
    }

    public String getVatRegistrationNumber() {
        return vatRegistrationNumber;
    }

    public void setVatRegistrationNumber(String vatRegistrationNumber) {
        this.vatRegistrationNumber = vatRegistrationNumber;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress;
    }

    public String getCellNumber() {
        return cellNumber;
    }

    public void setCellNumber(String cellNumber) {
        this.cellNumber = cellNumber;
    }

    public String getWorkNumber() {
        return workNumber;
    }

    public void setWorkNumber(String workNumber) {
        this.workNumber = workNumber;
    }

    public String getAttorneyType() {
        return attorneyType;
    }

    public void setAttorneyType(String attorneyType) {
        this.attorneyType = attorneyType;
    }

    public String getAddressType() {
        return addressType;
    }

    public void setAddressType(String addressType) {
        this.addressType = addressType;
    }

    public String getAddressLineOne() {
        return addressLineOne;
    }

    public void setAddressLineOne(String addressLineOne) {
        this.addressLineOne = addressLineOne;
    }

    public String getAddressLineTwo() {
        return addressLineTwo;
    }

    public void setAddressLineTwo(String addressLineTwo) {
        this.addressLineTwo = addressLineTwo;
    }

    public String getSuburb() {
        return suburb;
    }

    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }

    @Override
    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    public String getAccountType() {
        return accountType;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountHolder() {
        return accountHolder;
    }

    public void setAccountHolder(String accountHolder) {
        this.accountHolder = accountHolder;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getAddAttorneySubmit() {
        return addAttorneySubmit;
    }

    public void setAddAttorneySubmit(String addAttorneySubmit) {
        this.addAttorneySubmit = addAttorneySubmit;
    }

    public String getAddAttorneyYesSubmit() {
        return addAttorneyYesSubmit;
    }

    public void setAddAttorneyYesSubmit(String addAttorneyYesSubmit) {
        this.addAttorneyYesSubmit = addAttorneyYesSubmit;
    }

    public String getAddAttorneyPopUpWorkType() {
        return addAttorneyPopUpWorkType;
    }

    public void setAddAttorneyPopUpWorkType(String addAttorneyPopUpWorkType) {
        this.addAttorneyPopUpWorkType = addAttorneyPopUpWorkType;
    }

    public String getAddAttorneyPopUpStatus() {
        return addAttorneyPopUpStatus;
    }

    public void setAddAttorneyPopUpStatus(String addAttorneyPopUpStatus) {
        this.addAttorneyPopUpStatus = addAttorneyPopUpStatus;
    }

    public String getAddAttorneyPopUpQueue() {
        return addAttorneyPopUpQueue;
    }

    public void setAddAttorneyPopUpQueue(String addAttorneyPopUpQueue) {
        this.addAttorneyPopUpQueue = addAttorneyPopUpQueue;
    }

    public String getAddAttorneyWorkItemStatusUpdate() {
        return addAttorneyWorkItemStatusUpdate;
    }

    public void setAddAttorneyWorkItemStatusUpdate(String addAttorneyWorkItemStatusUpdate) {
        this.addAttorneyWorkItemStatusUpdate = addAttorneyWorkItemStatusUpdate;
    }

    public String getApprove() {
        return approve;
    }

    public void setApprove(String approve) {
        this.approve = approve;
    }

    public String getYesApprove() {
        return yesApprove;
    }

    public void setYesApprove(String yesApprove) {
        this.yesApprove = yesApprove;
    }

    public String getApprovePopUpWorkType() {
        return approvePopUpWorkType;
    }

    public void setApprovePopUpWorkType(String approvePopUpWorkType) {
        this.approvePopUpWorkType = approvePopUpWorkType;
    }

    public String getApprovePopUpStatus() {
        return approvePopUpStatus;
    }

    public void setApprovePopUpStatus(String approvePopUpStatus) {
        this.approvePopUpStatus = approvePopUpStatus;
    }

    public String getApprovePopUpQueue() {
        return approvePopUpQueue;
    }

    public void setApprovePopUpQueue(String approvePopUpQueue) {
        this.approvePopUpQueue = approvePopUpQueue;
    }

    @Override
    public String toString() {
        return "AttorneyDetails{" +
                "companyName='" + companyName + '\'' +
                ", vatRegistered='" + vatRegistered + '\'' +
                ", vatRegistrationNumber='" + vatRegistrationNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", cellNumber='" + cellNumber + '\'' +
                ", workNumber='" + workNumber + '\'' +
                ", attorneyType='" + attorneyType + '\'' +
                ", addressType='" + addressType + '\'' +
                ", addressLineOne='" + addressLineOne + '\'' +
                ", addressLineTwo='" + addressLineTwo + '\'' +
                ", suburb='" + suburb + '\'' +
                ", bankName='" + bankName + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", accountType='" + accountType + '\'' +
                ", accountNumber='" + accountNumber + '\'' +
                ", accountHolder='" + accountHolder + '\'' +
                ", province='" + province + '\'' +
                ", city='" + city + '\'' +
                ", addAttorneySubmit='" + addAttorneySubmit + '\'' +
                ", addAttorneyYesSubmit='" + addAttorneyYesSubmit + '\'' +
                ", addAttorneyPopUpWorkType='" + addAttorneyPopUpWorkType + '\'' +
                ", addAttorneyPopUpStatus='" + addAttorneyPopUpStatus + '\'' +
                ", addAttorneyPopUpQueue='" + addAttorneyPopUpQueue + '\'' +
                ", addAttorneyWorkItemStatusUpdate='" + addAttorneyWorkItemStatusUpdate + '\'' +
                ", approve='" + approve + '\'' +
                ", yesApprove='" + yesApprove + '\'' +
                ", approvePopUpWorkType='" + approvePopUpWorkType + '\'' +
                ", approvePopUpStatus='" + approvePopUpStatus + '\'' +
                ", approvePopUpQueue='" + approvePopUpQueue + '\'' +
                '}';
    }
}
