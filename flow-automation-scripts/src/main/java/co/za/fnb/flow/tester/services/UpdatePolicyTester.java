package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.ObjectFactory;
import generated.ResponseErrors;
import generated.UpdatePolicyRequestInput;
import generated.UpdateResponseOutput;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class UpdatePolicyTester {
    private Logger log  = LogManager.getLogger(UpdatePolicyTester.class);
    private UpdatePolicyRequestInput updatePolicyRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private UpdateResponseOutput updateResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public UpdatePolicyTester(UpdatePolicyRequestInput updatePolicyRequestInput) {
        this.updatePolicyRequestInput = updatePolicyRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(UpdatePolicyRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(updatePolicyRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(UpdateResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public UpdatePolicyRequestInput getUpdatePolicyRequestInput() {
        return updatePolicyRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public UpdateResponseOutput getUpdateResponseOutput() {
        return updateResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public UpdateResponseOutput sendUpdatePolicyRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvUPDATEPOLICYInput updatePolicyInput = serviceObjectFactory.createGensrvUPDATEPOLICYInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        log.info("pxmlin: " + marshalResult);
        updatePolicyInput.setPXMLIN(pxmlin);
        GensrvUPDATEPOLICYResult gensrvGETPOLICYDETAILResult = gsd00030PRServicesPort.gensrvUpdatepolicy(updatePolicyInput);
        PXMLOUT updatePolicyResponseOutput = gensrvGETPOLICYDETAILResult.getPXMLOUT();
        String pXmlOutString = updatePolicyResponseOutput.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, UpdateResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             updateResponseOutput = (UpdateResponseOutput) unMarshal;
             return updateResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting UpdateResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
