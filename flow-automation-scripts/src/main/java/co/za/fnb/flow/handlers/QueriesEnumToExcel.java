package co.za.fnb.flow.handlers;

import co.za.fnb.flow.handlers.database.Queries;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.ScenarioOperator;

import java.io.IOException;
import java.util.Arrays;

public class QueriesEnumToExcel {
    public QueriesEnumToExcel() {
    }

    public void generate(String customFileName) {
        String[] reportableFields = {"Policy Code", "Query", "Description"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport(customFileName);

        for (Queries query: Queries.values()) {
            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Code"), query.name(), TestResultReportFlag.DEFAULT);
            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Query"), query.toString(), TestResultReportFlag.DEFAULT);
            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Description"), "", TestResultReportFlag.DEFAULT);
            scenarioOperator.increamentReportRowIndex();
        }

        try {
            scenarioOperator.saveReport();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
