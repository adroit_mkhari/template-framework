package co.za.fnb.flow.pages.create_bulk_items.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class CreateBulkItemsPageObjects {

    @FindBy(id = "form:UploadItems")
    private WebElement uploadItems;

    @FindBy(id = "form:j_idt73_input")
    private WebElement choose;

    @FindBy(xpath = "//*[@id=\"form:j_idt73\"]/div[1]/button[1]/span[2]")
    private WebElement upload;

    @FindBy(xpath = "//*[@id=\"form:j_idt73\"]/div[1]/button[2]/span[2]")
    private WebElement cancel;

    @FindBy(xpath = "//*[@id=\"form:j_idt73\"]/div[2]/div[2]/div/div/div[2]")
    private WebElement file;

    @FindBy(xpath = "//*[@id=\"form:messages\"]/div/ul/li/span[1]")
    private WebElement summary;

    @FindBy(xpath = "//*[@id=\"form:messages\"]/div/ul/li/span[2]")
    private WebElement fileUploaded;

    public CreateBulkItemsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getUploadItems() {
        return uploadItems;
    }

    public WebElement getChoose() {
        return choose;
    }

    public WebElement getUpload() {
        return upload;
    }

    public WebElement getCancel() {
        return cancel;
    }

    public WebElement getFile() {
        return file;
    }

    public WebElement getSummary() {
        return summary;
    }

    public WebElement getFileUploaded() {
        return fileUploaded;
    }

}
