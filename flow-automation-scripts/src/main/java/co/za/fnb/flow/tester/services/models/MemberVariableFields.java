package co.za.fnb.flow.tester.services.models;

public class MemberVariableFields {
    // Child Role Id(s) - Action(s)	                                                        Parent/Extended Family Role Id(s) - Action(s)
    // [CH,name,1000,6.00,20130101,genderCode],[CH,name,1000,6.00,19990101,genderCode]	    [PI,name,10000,48.00,19900701,genderCode],[EF,name,10000,63.00,19700401,genderCode]

    private String relationship;
    private String name;
    private String coverAmount;
    private String premium;
    private String birthDate;
    private String genderCode;

    public MemberVariableFields(String relationship, String name, String coverAmount, String premium, String birthDate, String genderCode) {
        this.relationship = relationship;
        this.name = name;
        this.coverAmount = coverAmount;
        this.premium = premium;
        this.birthDate = birthDate;
        this.genderCode = genderCode;
    }

    public String getRelationship() {
        return relationship;
    }

    public String getName() {
        return name;
    }

    public String getCoverAmount() {
        return coverAmount;
    }

    public String getPremium() {
        return premium;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public String getGenderCode() {
        return genderCode;
    }
}
