package co.za.fnb.flow.pages;

import co.za.fnb.flow.pages.root_page_factory.CreateMultipleMintItemTablePageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import java.util.NoSuchElementException;

public class CreateMultipleMintItemTable extends BasePage {
    private Logger log  = LogManager.getLogger(CreateMultipleMintItemTable.class);
    CreateMultipleMintItemTablePageObjects createMultipleMintItemTablePageObjects = new CreateMultipleMintItemTablePageObjects(driver);
    Actions actions = new Actions(driver);

    private String[] workType;
    private String[] status;
    private String[] queue;

    private String popUpWorkType[];
    private String popUpStatus[];
    private String popUpQueue[];

    private String commentCategoryValue;
    private String commentValue;

    private int mintItemCommentCESize;

    public CreateMultipleMintItemTable(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public CreateMultipleMintItemTable(WebDriver driver, ScenarioOperator scenarioOperator, String popUpWorkType[], String popUpStatus[], String popUpQueue[]) {
        super(driver, scenarioOperator);
        this.popUpWorkType = popUpWorkType;
        this.popUpStatus = popUpStatus;
        this.popUpQueue = popUpQueue;
        PageFactory.initElements(driver, this);
    }

    public CreateMultipleMintItemTable(WebDriver driver, ScenarioOperator scenarioOperator, String[] popUpWorkType, String[] popUpStatus, String[] popUpQueue, String commentCategoryValue, String commentValue) {
        super(driver, scenarioOperator);
        this.popUpWorkType = popUpWorkType;
        this.popUpStatus = popUpStatus;
        this.popUpQueue = popUpQueue;
        this.commentCategoryValue = commentCategoryValue;
        this.commentValue = commentValue;
    }

    public String[] getPopUpWorkType() {
        return popUpWorkType;
    }

    public void setPopUpWorkType(String[] popUpWorkType) {
        this.popUpWorkType = popUpWorkType;
    }

    public String[] getPopUpStatus() {
        return popUpStatus;
    }

    public void setPopUpStatus(String[] popUpStatus) {
        this.popUpStatus = popUpStatus;
    }

    public String[] getPopUpQueue() {
        return popUpQueue;
    }

    public void setPopUpQueue(String[] popUpQueue) {
        this.popUpQueue = popUpQueue;
    }

    public String getCommentCategoryValue() {
        return commentCategoryValue;
    }

    public void setCommentCategoryValue(String commentCategoryValue) {
        this.commentCategoryValue = commentCategoryValue;
    }

    public String getCommentValue() {
        return commentValue;
    }

    public void setCommentValue(String commentValue) {
        this.commentValue = commentValue;
    }

    public int getMintItemCommentCESize() {
        return mintItemCommentCESize;
    }

    public void setMintItemCommentCESize(int mintItemCommentCESize) {
        this.mintItemCommentCESize = mintItemCommentCESize;
    }

    public void computeItemCommentCESize() throws Exception {
        log.info("Getting Item CommentCE Size");
        try {
            int frameIndex = 0;
            int mintItemCommentCESize;
            do {
                boolean frameFlag = false;
                while (!frameFlag && frameIndex < 10) {
                    try {
                        driver.switchTo().defaultContent();
                        driver.switchTo().frame(frameIndex++);
                        frameFlag = true;
                    } catch (NoSuchFrameException e) {
                        log.warn("NoSuchFrameException thrown.");
                    }
                }
                mintItemCommentCESize = driver.findElements(createMultipleMintItemTablePageObjects.getItemCommentCESize()).size();
            } while (mintItemCommentCESize == 0 && frameIndex < 10);
            setMintItemCommentCESize(mintItemCommentCESize);
            log.info("mintItemCommentCESize: " + mintItemCommentCESize);
        } catch (Exception e) {
            log.error("Error while Getting Item CommentCE Size.");
            e.printStackTrace();
            throw new Exception("Error while Getting Item CommentCE Size.");
        }
    }

    public void createMintItemForm() throws Exception {
        log.info("Working on Create Mint Item Form");
        computeItemCommentCESize();
        if (mintItemCommentCESize != 0) {
            workType = new String[mintItemCommentCESize];
            status = new String[mintItemCommentCESize];
            queue = new String[mintItemCommentCESize];

            for (int i = 0 ; i < mintItemCommentCESize; i++) {
                int index = i + 1;
                log.debug("trying to get frame items: " + index);
                workType[i] = driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[1]/div[1]/table[1]/tbody[1]/tr["+(index)+"]/td[3]")).getText();
                status[i] = driver.findElement(By.xpath("/html/body/form/div/div[1]/table/tbody/tr[" + index + "]/td[4]/div")).getText();
                queue[i] = driver.findElement(By.xpath("/html/body/form/div/div[1]/table/tbody/tr[" + index + "]/td[5]/div")).getText();
                // workType[i] = driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[1]/div[1]/table[1]/tbody[1]/tr["+(index)+"]/td[3]")).getText();
                // status[i] = driver.findElement(By.xpath("//div[contains (@id, 'createMintItemForm:createMultipleMintItemtable:"+i+":j_idt15')]")).getText();
                // queue[i] = driver.findElement(By.xpath("//div[contains (@id, 'createMintItemForm:createMultipleMintItemtable:"+i+":j_idt19')]")).getText();

                log.info("(Pop Up) Work Type: " + popUpWorkType[i] + " == " + workType[i] + " , Status: " + popUpStatus[i] + " == " + status[i] + " , Queue: " + popUpQueue[i] + " == " + queue[i]);
                if ((!workType[i].equalsIgnoreCase(popUpWorkType[i])) ||  (!status[i].equalsIgnoreCase(popUpStatus[i])) || (!queue[i].equalsIgnoreCase(popUpQueue[i]))){
                    String message = "Work item values do not match: Work type is " + workType[i] + " & Status is: " + status[i] + " & Queue is: " + queue[i];
                    takeScreenShot("");
                    log.error(message);
                    // reportHandler.setFailureReason(message);
                    errorHandle.setError(message);
                    throw new Exception(message);
                }
            }

            actions.moveToElement(createMultipleMintItemTablePageObjects.getCreate()).clickAndHold().build().perform();
            actions.moveToElement(createMultipleMintItemTablePageObjects.getCreate()).doubleClick().build().perform();

            getResponses(true);

            driver.switchTo().defaultContent();
            int numberOfFrames = driver.findElements(By.tagName("iframe")).size();
            if (numberOfFrames == 3) {
                // TODO: Make sure the values for comment categories are set before.
                categoriseComment();
                Thread.sleep(500);
                getResponses(true);
                Thread.sleep(3000);
            }
        } else {
            log.info("Create Item Popup Table not yet displayed.");
            throw new Exception("Create Item Popup Table not yet displayed.");
        }
    }

    public void createMintItemFormSubmit() throws Exception {
        log.info("Working on Create Mint Item Form (Submit)");
        computeItemCommentCESize();
        if (mintItemCommentCESize != 0) {
            workType = new String[mintItemCommentCESize];
            status = new String[mintItemCommentCESize];
            queue = new String[mintItemCommentCESize];

            for (int i = 0 ; i < mintItemCommentCESize; i++) {
                int index = i + 1;
                log.debug("trying to get frame items: " + index);
                workType[i] = driver.findElement(By.xpath("/html[1]/body[1]/form[1]/div[1]/div[1]/table[1]/tbody[1]/tr["+(index)+"]/td[3]")).getText();
                // status[i] = driver.findElement(By.xpath("//div[contains (@id, 'createMintItemForm:createMultipleMintItemtable:"+i+":j_idt15')]")).getText();
                status[i] = driver.findElement(By.xpath("/html/body/form/div/div[1]/table/tbody/tr[" + index + "]/td[4]/div")).getText();
                // queue[i] = driver.findElement(By.xpath("//div[contains (@id, 'createMintItemForm:createMultipleMintItemtable:"+i+":j_idt19')]")).getText();
                queue[i] = driver.findElement(By.xpath("/html/body/form/div/div[1]/table/tbody/tr[" + index + "]/td[5]/div")).getText();

                if ((!workType[i].equalsIgnoreCase(popUpWorkType[i])) ||  (!status[i].equalsIgnoreCase(popUpStatus[i])) || (!queue[i].equalsIgnoreCase(popUpQueue[i]))){
                    String message = "Work item values do not match: Work type is " + workType[i] + " & Status is: " + status[i] + " & Queue is: " + queue[i];
                    takeScreenShot("");
                    log.error(message);
                    // reportHandler.setFailureReason(message);
                    errorHandle.setError(message);
                    throw new Exception(message);
                }
            }

            actions.moveToElement(createMultipleMintItemTablePageObjects.getCreate()).clickAndHold().build().perform();
            actions.moveToElement(createMultipleMintItemTablePageObjects.getCreate()).doubleClick().build().perform();
        } else {
            log.info("Create Item Popup Table not yet displayed.");
            throw new Exception("Create Item Popup Table not yet displayed.");
        }
    }

    private void categoriseComment() throws Exception {
        log.info("Categorizing Comment.");
        driver.switchTo().defaultContent();
        int size = driver.findElements(By.tagName("iframe")).size();
        log.info("Number of frames: " + size);

        try {
            driver.switchTo().frame(size - 1);
            try {
                // TODO: Use select Methods on Base Page.
                // TODO: Check if these values "commentCategoryValue" and "commentValue" are not empty

                if (commentCategoryValue != null) {
                    createMultipleMintItemTablePageObjects.getCommentCategorySelect().click();
                    Thread.sleep(500);
                    createMultipleMintItemTablePageObjects.getCommentCategorySelectItems().findElements(By.xpath("//li[contains(@data-label, '" + commentCategoryValue + "')]")).get(0).click();
                }

                if (commentValue != null) {
                    createMultipleMintItemTablePageObjects.getCommentSelect().click();
                    Thread.sleep(500);
                    createMultipleMintItemTablePageObjects.getCommentSelectItems().findElements(By.xpath("//li[contains(@data-label, '" + commentValue + "')]")).get(0).click();
                }
                createMultipleMintItemTablePageObjects.getSubmitComment().click();
            } catch (NoSuchElementException | InterruptedException e) {
                log.error("Error while Categorising Comment.");
                e.printStackTrace();
                throw new Exception("Error while Categorising Comment.");
            }
        } catch (NoSuchFrameException e) {
            log.error("Error while switching frame on Categorising Comment.");
            e.printStackTrace();
            throw new Exception("Error while switching frame on Categorising Comment.");
        }
    }

}
