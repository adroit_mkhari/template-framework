package co.za.fnb.flow.pages.principal_member_details.information;

import co.za.fnb.flow.pages.principal_member_details.page_factory.PrincipalMemberPostalInformationAddressSearchPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class PrincipalMemberPostalInformationAddressSearch extends PrincipalMemberPostalInformation {
    private Logger log  = LogManager.getLogger(PrincipalMemberPostalInformationAddressSearch.class);
    PrincipalMemberPostalInformationAddressSearchPageObjects principalMemberPostalInformationAddressSearchPageObjects
            = new PrincipalMemberPostalInformationAddressSearchPageObjects(driver, scenarioOperator);

    public PrincipalMemberPostalInformationAddressSearch(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public void clearLocation() throws Exception {
        try {
            clear(principalMemberPostalInformationAddressSearchPageObjects.getLocation());
        } catch (Exception e) {
            log.error("Error while clearing Location.");
            throw new Exception("Error while clearing Location.");
        }
    }

    public void updateLocation(String location) throws Exception {
        try {
            type(principalMemberPostalInformationAddressSearchPageObjects.getLocation(), location);
        } catch (Exception e) {
            log.error("Error while updating Location.");
            throw new Exception("Error while updating Location.");
        }
    }

    public void clearPostalCode() throws Exception {
        try {
            clear(principalMemberPostalInformationAddressSearchPageObjects.getPostalCode());
        } catch (Exception e) {
            log.error("Error while clearing Postal Code.");
            throw new Exception("Error while clearing Postal Code.");
        }
    }

    public void updatePostalCode(String postalCode) throws Exception {
        try {
            type(principalMemberPostalInformationAddressSearchPageObjects.getPostalCode(), postalCode);
        } catch (Exception e) {
            log.error("Error while updating Postal Code.");
            throw new Exception("Error while updating Postal Code.");
        }
    }

    public void clickSearchAddressButton() throws Exception {
        try {
            click(principalMemberPostalInformationAddressSearchPageObjects.getSearchAddressButton());
        } catch (Exception e) {
            log.error("Error while clicking on Search Address Button.");
            throw new Exception("Error while clicking on Search Address Button.");
        }
    }

    public void moveToAndDoubleClickAddressSearchFirstItem() throws Exception {
        try {
            moveToElementAndDoubleClick(principalMemberPostalInformationAddressSearchPageObjects.getAddressSearchFirstItem());
        } catch (Exception e) {
            log.error("Error while clicking on AddressSearchFirstItem.");
            throw new Exception("Error while clicking on AddressSearchFirstItem.");
        }
    }

}
