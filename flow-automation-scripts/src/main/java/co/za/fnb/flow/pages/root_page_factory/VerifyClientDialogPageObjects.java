package co.za.fnb.flow.pages.root_page_factory;

import co.za.fnb.flow.pages.BasePage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static java.util.concurrent.TimeUnit.SECONDS;

public class VerifyClientDialogPageObjects {
    // region FirstPopUp
    @FindBy(xpath = "//*[@id=\"infoVerifiedForm:infoVerifiedRadio\"]/tbody/tr/td[1]/label")
    private WebElement writtenInstruction;

    @FindBy(xpath = "//*[@id=\"infoVerifiedForm:infoVerifiedRadio\"]/tbody/tr/td[2]/label")
    private WebElement inboundOrOutboundCall;

    @FindBy(xpath = "//*[@id=\"infoVerifiedForm:infoVerifiedRadio\"]/tbody/tr/td[3]/label")
    private WebElement callFromABranchOrBanker;

    @FindBy(id = "infoVerifiedForm:verifyBranchCallVerified")
    private WebElement infoVerifiedFormVerifyBranchCallVerified;
    // endregion

    // region Written Instruction
    @FindBy(id = "writtenForm:emailAddressMatch")
    private WebElement emailAddressMatch;

    @FindBy(id = "writtenForm:emailAddressNameMatch")
    private WebElement emailAddressNameMatch;

    @FindBy(id = "writtenForm:clientSignatureVerified")
    private WebElement clientSignatureVerified;

    @FindBy(id = "writtenForm:thirdPartyClientVerified")
    private WebElement thirdPartyClientVerified;

    @FindBy(id = "writtenForm:clientNotVerified")
    private WebElement clientNotVerified;

    @FindBy(id = "writtenForm:verifyWritten")
    private WebElement verifyWritten;
    // endregion

    // region Inbound / Outbound Call
    @FindBy(id = "inoutCallForm:idVerified")
    private WebElement idVerified;

    @FindBy(id = "inoutCallForm:cellNoVerified")
    private WebElement cellNoVerified;

    @FindBy(id = "inoutCallForm:emailAddressVerified")
    private WebElement emailAddressVerified;

    @FindBy(id = "inoutCallForm:residentialAddressVerified")
    private WebElement residentialAddressVerified;

    @FindBy(id = "inoutCallForm:postalCodeVerified")
    private WebElement postalCodeVerified;

    @FindBy(id = "inoutCallForm:dobVerified")
    private WebElement dobVerified;

    @FindBy(id = "inoutCallForm:benNameVerified")
    private WebElement benNameVerified;

    @FindBy(id = "inoutCallForm:verifyInoutCall")
    private WebElement verifyInoutCall;
    // endregion

    // region Call From a Branch / Banker
    @FindBy(id = "branchCallVerifiedForm:branchClientVerified")
    private WebElement branchClientVerified;

    @FindBy(id = "branchCallVerifiedForm:verifyBranchCallVerified")
    private WebElement branchCallVerifyBranchCallVerified;

    @FindBy(id = "branchCallForm:branchConsultantName")
    private WebElement branchConsultantName;

    @FindBy(id = "branchCallForm:branchName")
    private WebElement branchName;

    @FindBy(id = "branchCallForm:branchConsultantFNo")
    private WebElement branchConsultantFNo;

    @FindBy(id = "branchCallForm:branchTelNo")
    private WebElement branchTelNo;

    @FindBy(id = "branchCallForm:verifyBranchCall")
    private WebElement verifyBranchCall;
    // endregion

    public VerifyClientDialogPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getWrittenInstruction() {
        return writtenInstruction;
    }

    public WebElement getInboundOrOutboundCall() {
        return inboundOrOutboundCall;
    }

    public WebElement getCallFromABranchOrBanker() {
        return callFromABranchOrBanker;
    }

    public WebElement getInfoVerifiedFormVerifyBranchCallVerified() {
        return infoVerifiedFormVerifyBranchCallVerified;
    }

    public WebElement getEmailAddressMatch() {
        return emailAddressMatch;
    }

    public WebElement getEmailAddressNameMatch() {
        return emailAddressNameMatch;
    }

    public WebElement getClientSignatureVerified() {
        return clientSignatureVerified;
    }

    public WebElement getThirdPartyClientVerified() {
        return thirdPartyClientVerified;
    }

    public WebElement getClientNotVerified() {
        return clientNotVerified;
    }

    public WebElement getVerifyWritten() {
        return verifyWritten;
    }

    public WebElement getIdVerified() {
        return idVerified;
    }

    public WebElement getCellNoVerified() {
        return cellNoVerified;
    }

    public WebElement getEmailAddressVerified() {
        return emailAddressVerified;
    }

    public WebElement getResidentialAddressVerified() {
        return residentialAddressVerified;
    }

    public WebElement getPostalCodeVerified() {
        return postalCodeVerified;
    }

    public WebElement getDobVerified() {
        return dobVerified;
    }

    public WebElement getBenNameVerified() {
        return benNameVerified;
    }

    public WebElement getVerifyInoutCall() {
        return verifyInoutCall;
    }

    public WebElement getBranchClientVerified() {
        return branchClientVerified;
    }

    public WebElement getBranchCallVerifyBranchCallVerified() {
        return branchCallVerifyBranchCallVerified;
    }

    public WebElement getBranchConsultantName() {
        return branchConsultantName;
    }

    public WebElement getBranchName() {
        return branchName;
    }

    public WebElement getBranchConsultantFNo() {
        return branchConsultantFNo;
    }

    public WebElement getBranchTelNo() {
        return branchTelNo;
    }

    public WebElement getVerifyBranchCall() {
        return verifyBranchCall;
    }
}
