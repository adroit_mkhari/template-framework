package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AwaitDocDiarizeReviewDeclinedItemObjects {

    @FindBy(id = "createSearchItemView:update_dlg")
    WebElement updateDialog;

    @FindBy(id = "awaitDocDiarizeView:inAppMessageValue")
    WebElement inAppMessageValue;

    @FindBy(id = "awaitDocDiarizeView:Submit")
    WebElement submit;

    @FindBy(id = "awaitDocDiarizeView:Cancel")
    WebElement cancel;

    public AwaitDocDiarizeReviewDeclinedItemObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getUpdateDialog() {
        return updateDialog;
    }

    public WebElement getInAppMessageValue() {
        return inAppMessageValue;
    }

    public WebElement getSubmit() {
        return submit;
    }

    public WebElement getCancel() {
        return cancel;
    }
}
