package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.AttorneyClaimsPaymentPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AttorneyClaimsPayment extends BasePage {
    private Logger log  = LogManager.getLogger(AttorneyClaimsPayment.class);
    AttorneyClaimsPaymentPageObjects attorneyClaimsPaymentPageObjects = new AttorneyClaimsPaymentPageObjects(driver);

    public AttorneyClaimsPayment(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public int getAttorneyClaimsPaymentTableList() throws Exception {
        try {
            return getTableDataRiSize(attorneyClaimsPaymentPageObjects.getClaimPaymentListTableData());
        } catch (Exception e) {
            log.error("Error while getting attorney claims payment list table size.");
            throw new Exception("Error while getting attorney claims payment list table size.");
        }
    }

    public void getPolicyNumber(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 2);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting on policy number.");
            throw new Exception("Error while getting on policy number.");
        }
    }

    public String getClaimsNumber(int row) throws Exception {
        try {
            log.info("Getting Claim Number.");
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 3);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting claims number.");
            throw new Exception("Error while getting claims number.");
        }
    }

    public String getPaymentSequence(int row) throws Exception {
        try {
            log.info("Getting Payment Sequence.");
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 4);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Payment Sequence.");
            throw new Exception("Error while getting Payment Sequence.");
        }
    }

    public void getAmount(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 5);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Amount.");
            throw new Exception("Error while getting Amount.");
        }
    }

    public void getRemainingLimit(int row) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 6);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Remaining Limit.");
            throw new Exception("Error while getting Remaining Limit.");
        }
    }

    public String getStatus(int row) throws Exception {
        try {
            log.info("Getting Status.");
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getClaimPaymentListTableDataRowXpathLocator();
            String tableCellValueLocator = getLocatorForTableRowWithTrTd(tableCellValueLocatorTemplate, row, 7);
            WebElement tableCellValue = driver.findElement(By.xpath(tableCellValueLocator));
            return getText(tableCellValue);
        } catch (Exception e) {
            log.error("Error while getting Status.");
            throw new Exception("Error while getting Status.");
        }
    }

    public void clickApproveFail(int rowIndex) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getBtnApproveFailLocatorTemplateId();
            String tableCellValueLocator = getLocatorForIndex(tableCellValueLocatorTemplate, rowIndex);
            WebElement tableCellValue = driver.findElement(By.id(tableCellValueLocator));
            click(tableCellValue);
        } catch (Exception e) {
            log.error("Error while clicking on Approve/Fail");
            throw new Exception("Error while clicking on Approve/Fail");
        }
    }

    public void clickEdit(int rowIndex) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getBtneditClaimPaymentTemplateId();
            String tableCellValueLocator = getLocatorForIndex(tableCellValueLocatorTemplate, rowIndex);
            WebElement tableCellValue = driver.findElement(By.id(tableCellValueLocator));
            click(tableCellValue);
        } catch (Exception e) {
            log.error("Error while clicking on Edit.");
            throw new Exception("Error while clicking on Edit.");
        }
    }

    public void clickView(int rowIndex) throws Exception {
        try {
            String tableCellValueLocatorTemplate = attorneyClaimsPaymentPageObjects.getBtnApproveViewTemplateId();
            String tableCellValueLocator = getLocatorForIndex(tableCellValueLocatorTemplate, rowIndex);
            WebElement tableCellValue = driver.findElement(By.id(tableCellValueLocator));
            click(tableCellValue);
        } catch (Exception e) {
            log.error("Error while clicking on View.");
            throw new Exception("Error while clicking on View.");
        }
    }

}
