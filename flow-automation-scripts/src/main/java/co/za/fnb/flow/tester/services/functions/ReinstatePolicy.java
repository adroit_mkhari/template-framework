package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.ReinstatePolicyTester;
import generated.*;
import generated.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class ReinstatePolicy {
    private Logger log  = LogManager.getLogger(ReinstatePolicy.class);
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private ReinstatePolicyRequestInput reinstatePolicyRequestInput;
    private ReinstatePolicyResponseOutput reinstatePolicyResponseOutput;
    private final String CLEAR = "CLEAR";
    private ScenarioOperator scenarioOperator;
    private String username;

    public ReinstatePolicy(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void sendRequest() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        String policyNumber = scenarioOperator.getPolicyNumber();
        username = scenarioTester.getCellValue("Username");
        String function = scenarioOperator.getFunction();
        String payDay = scenarioTester.getCellValue("Pay Day");
        String debitOrderDate = scenarioTester.getCellValue("Debit Order Date");
        String withArrears = scenarioTester.getCellValue("With Arrears");
        String deleteOverAged = scenarioTester.getCellValue("Delete Over-Aged");
        String employeeName = scenarioTester.getCellValue("Employee Name");
        String employeeNumber = scenarioTester.getCellValue("Employee Number");
        String branchName = scenarioTester.getCellValue("Branch Name");
        String branchNumber = scenarioTester.getCellValue("Branch Number");

        if (reinstatePolicyRequestInput == null) {
            reinstatePolicyRequestInput = new ReinstatePolicyRequestInput();
            reinstatePolicyRequestInput.setUsername(username);
            reinstatePolicyRequestInput.setPolicy(policyNumber);

            if (!debitOrderDate.isEmpty()) {
                if (debitOrderDate.contains("-")) {
                    String[] splitDebitOrderDate = debitOrderDate.split("-");
                    if (splitDebitOrderDate[0].length() == 4) {
                        debitOrderDate = splitDebitOrderDate[0] + splitDebitOrderDate[1] + splitDebitOrderDate[2];
                    } else {
                        debitOrderDate = splitDebitOrderDate[2] + splitDebitOrderDate[1] + splitDebitOrderDate[0];
                    }
                }
            }

            reinstatePolicyRequestInput.setDebitdate(debitOrderDate);

            if (!payDay.isEmpty()) {
                if (payDay.contains(".")) {
                    String[] splitPayDay = payDay.split("\\.");
                    payDay = splitPayDay[0];
                }
            }

            reinstatePolicyRequestInput.setPayday(payDay);
            reinstatePolicyRequestInput.setWitharrears(withArrears);
            reinstatePolicyRequestInput.setDeleteoveraged(deleteOverAged);
            ReinstatePolicyBranchConsultant reinstatePolicyBranchConsultant = new ReinstatePolicyBranchConsultant();
            reinstatePolicyBranchConsultant.setEmployeename(employeeName);
            reinstatePolicyBranchConsultant.setEmployeenumber(employeeNumber);
            reinstatePolicyBranchConsultant.setBranchname(branchName);
            reinstatePolicyBranchConsultant.setBranchnumber(branchNumber);
            reinstatePolicyRequestInput.setBranchconsultant(reinstatePolicyBranchConsultant);
        }

        ReinstatePolicyTester reinstatePolicyTester = new ReinstatePolicyTester(reinstatePolicyRequestInput);

        StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
        try {
            reinstatePolicyResponseOutput = reinstatePolicyTester.sendReinstatePolicyRequest();
        } catch (Exception e) {
            responseErrors = reinstatePolicyTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<generated.Error> errorList = errors.getError();
            for (Error error : errorList) {
                updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(updatePolicyErrorStringBuilder.toString());
        }
    }

    public void performValidations() throws Exception {
        String expectedPolicyStatus = scenarioTester.getCellValue("Expected Policy Status");
        String expectedPolicyStatusDate = scenarioTester.getCellValue("Expected Policy Status Date");
    }

}
