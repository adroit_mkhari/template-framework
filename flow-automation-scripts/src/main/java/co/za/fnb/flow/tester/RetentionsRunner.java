package co.za.fnb.flow.tester;

import co.za.fnb.flow.handlers.database.QueryHandler;
import co.za.fnb.flow.models.custom_exceptions.FicaStatusException;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.models.custom_exceptions.ServicePageException;
import co.za.fnb.flow.pages.*;
import co.za.fnb.flow.pages.administration.LegalAdvisorAdminPage;
import co.za.fnb.flow.pages.audit_trail.AuditTrail;
import co.za.fnb.flow.pages.claims.Claims;
import co.za.fnb.flow.pages.generic_maintanence.GenericMaintenance;
import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import co.za.fnb.flow.pages.principal_member_details.PrincipalMemberDetails;
import co.za.fnb.flow.pages.work_items.WorkItems;
import co.za.fnb.flow.setup.TestResultReportFlag;
import cucumber.api.DataTable;
import gherkin.formatter.model.Row;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.DateUtil;
import org.openqa.selenium.WebDriver;

import java.io.IOException;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public class RetentionsRunner {
    private Logger log  = LogManager.getLogger(RetentionsRunner.class);
    QueryHandler queryHandler = new QueryHandler();
    WebDriver driver;
    DataTable dataTable;
    HomePage homePage;
    PolicyDetails policyDetails;
    PrincipalMemberDetails principalMemberDetails;
    Claims claims;
    GenericMaintenance genericMaintenance;
    CreateSearchItemPage createSearchItemPage;
    LegalAdvisorAdminPage legalAdvisorAdminPage;
    CreateMultipleMintItemTable createMultipleMintItemtable;
    CreateOrUpdateWorkItem createOrUpdateWorkItem;
    WorkItems workItems;
    AuditTrail auditTrail;

    public RetentionsRunner() {
    }

    public RetentionsRunner(WebDriver driver) {
        this.driver = driver;
    }

    public RetentionsRunner(DataTable dataTable) {
        this.dataTable = dataTable;
    }

    public RetentionsRunner(WebDriver driver, DataTable dataTable) {
        this.dataTable = dataTable;
        this.driver = driver;
    }

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public PolicyDetails getPolicyDetails() {
        return policyDetails;
    }

    public void setPolicyDetails(PolicyDetails policyDetails) {
        this.policyDetails = policyDetails;
    }

    public int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    private String getCellValue(Object[] headers, List<String> dataEntry, String field) {
        return getHeaderIndex(headers, field) != -1 ? dataEntry.get(getHeaderIndex(headers, field)) : "";
    }

    private void updateFicaStatus(ScenarioOperator scenarioOperator, String dbPolicyNumber, String risk, String sanction, String edd, String kyc, String ficaStatus, String updatedTe) throws Exception {
        String productName = scenarioOperator.getProductName();
        if (!productName.toUpperCase().contains("LOC")) {
            queryHandler.insertFicaRecord(
                    productName,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            queryHandler.updateFicaStatus(dbPolicyNumber,
                    risk,
                    sanction,
                    edd,
                    kyc,
                    ficaStatus,
                    updatedTe);

            ResultSet ficaUpdatedPolicy = queryHandler.queryForPolicyNumberOnFicaTable(dbPolicyNumber);
            // TODO: Handle errors thrown here.
            if (ficaUpdatedPolicy.next()) {
                String policy = ficaUpdatedPolicy.getString("POLICYNO").trim();
                String status = ficaUpdatedPolicy.getString("STATUS").trim();
                // responseTrace.append("FICA ERROR: " + status + "\n");
                log.debug("POLICYNO: " + policy + " STATUS: " + status);
                if (status.equalsIgnoreCase("FICA ERROR")) {
                    throw new FicaStatusException("FICA ERROR status on policy Number: " + policy);
                }
            } else {
                throw new FicaStatusException("FICA ERROR: SQL Failed to retrieve policy with the policy number: " + scenarioOperator.getPolicyNumber());
            }
        }
    }

    public void run() throws IOException, InterruptedException {
        log.info("Starting ScenarioOperator Tests.");
        String[] reportableFields = {"Test Case Number", "Scenario Description", "Function", "Product Name", "Policy Number", "Result", "Failure Reason"};
        ScenarioOperator scenarioOperator = new ScenarioOperator(reportableFields);
        scenarioOperator.createReport();
        log.info("Reportable Fields: " + Arrays.asList(reportableFields));
        log.info("Report Path: " + scenarioOperator.getReportPath());

        Object[] headers = dataTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        String run, testFunction, scenarioDescription, testCaseNumber, productName, policyNumberQuery, policyCode, policyNumber, takeupFirst, takeupCode;

        try {
            if (Arrays.asList(headers).contains("Test Case No")) {
                for (Row row : dataTable.getGherkinRows()) {
                    if (row.getLine() != 1) {
                        dataEntry = row.getCells();
                        run = getCellValue(headers, dataEntry,"Run");
                        if (run.equalsIgnoreCase("YES")) {
                            testCaseNumber = getCellValue(headers, dataEntry,"Test Case No");
                            scenarioDescription = getCellValue(headers, dataEntry,"Scenario Description");
                            testFunction = getCellValue(headers, dataEntry,"Function");
                            productName = getCellValue(headers, dataEntry,"Product Name");
                            scenarioOperator.setTestCaseNumber(testCaseNumber);
                            scenarioOperator.setScenarioDescription(scenarioDescription);
                            scenarioOperator.setFunction(testFunction);
                            scenarioOperator.setProductName(productName);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Test Case Number"), scenarioOperator.getTestCaseNumber(), TestResultReportFlag.DEFAULT);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Scenario Description"), scenarioOperator.getScenarioDescription(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Function"), scenarioOperator.getFunction(), TestResultReportFlag.WARNING);
                            scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Product Name"), scenarioOperator.getProductName(), TestResultReportFlag.WARNING);

                            scenarioOperator.setScreenShotFileName(scenarioOperator.getFunction() + " " + scenarioOperator.getTestCaseNumber() + " " + scenarioOperator.getScenarioDescription());
                            // BasePage.setScenarioOperator(scenarioOperator);

                            try {
                                policyNumber = getCellValue(headers, dataEntry,"Policy Number");
                                scenarioOperator.setPolicyNumber(policyNumber);

                                log.info("===========================================================================");
                                log.info("Running Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                                log.info("---------------------------------------------------------------------------");

                                if (testFunction.equalsIgnoreCase("Retentions")) {
                                    retentions(reportableFields, scenarioOperator, headers, dataEntry);
                                    scenarioOperator.increamentReportRowIndex();
                                }
                                log.info("End Of Test Case Number: " + testCaseNumber + ", Function: " + testFunction);
                            } catch (Exception e) {
                                log.error(e.getMessage());
                                e.printStackTrace();
                                reportFailure(reportableFields, scenarioOperator, e.getMessage());
                                scenarioOperator.increamentReportRowIndex();
                            } finally {
                                if (driver.getWindowHandles().toArray().length > 1) {
                                    int numberOfWindows = driver.getWindowHandles().toArray().length;
                                    while (numberOfWindows > 1) {
                                        Object currentWindow = driver.getWindowHandles().toArray()[numberOfWindows - 1];
                                        WebDriver window = driver.switchTo().window((String) currentWindow);
                                        window.close();
                                        numberOfWindows = driver.getWindowHandles().toArray().length;
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            // TODO: Handle Whole Test Exception
        } finally {
            log.info("Saving Report.");
            scenarioOperator.saveReport();
        }
        log.info("===========================================================================");
    }

    private void retentions(String[] reportableFields, ScenarioOperator scenarioOperator, Object[] headers, List<String> dataEntry) throws Exception {
        log.info("Running Retentions Logic");

        String retentionMessage = getCellValue(headers, dataEntry, "Retention Message");

        homePage = new HomePage(driver, scenarioOperator);
        createSearchItemPage = homePage.selectPersonalQueueCreateOrSearchItem();

        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Policy Number"), scenarioOperator.getPolicyNumber(), TestResultReportFlag.DEFAULT);

        TestHandle testHandle;
        ErrorHandle errorHandle;

        try {
            createSearchItemPage.searchPolicyNumber(scenarioOperator.getPolicyNumber(), true);

            String retentionsMessage = createSearchItemPage.getRetentionsMessage();

            if (retentionsMessage.equalsIgnoreCase(retentionMessage)) {
                reportSuccess(reportableFields, scenarioOperator);
            } else {
                reportFailure(reportableFields, scenarioOperator, "Error: " + retentionsMessage + "<- is not as expected -> " + retentionMessage);
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            if (e instanceof FicaStatusException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof ServicePageException) {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            } else if (e instanceof NegativeTestException) {
                reportSuccess(reportableFields, scenarioOperator, e.getMessage());
            } else {
                reportFailure(reportableFields, scenarioOperator, e.getMessage());
            }
        }
        log.info("End Of Retentions Logic");
    }

    private String getFlowPolicyNumberFormat(String productName, String policyNumber) throws Exception {
        String actualPolicyNumber;
        log.debug("Formatting Policy Number");
        try {
            log.debug("db Policy Number: " + policyNumber);

            String prefix ="";
            String suffix="";
            String middleValue="";

            if (productName.equalsIgnoreCase("Funeral Insurance")) {
                prefix = policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, 9);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);
                middleValue = "0000";
            } else if (productName.equalsIgnoreCase("Health Cash Plan") || productName.equalsIgnoreCase("Health Cash")) {
                int size = policyNumber.length();
                int diff = 13 - size;

                if (policyNumber.toUpperCase().contains("HCP")) {
                    prefix =  policyNumber.substring(0, 3);
                    suffix = policyNumber.substring(3, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                } else if (policyNumber.toUpperCase().contains("HC")) {
                    prefix =  policyNumber.substring(0, 2);
                    suffix = policyNumber.substring(2, size);
                    log.info("Prefix: " + prefix);
                    log.info("Suffix: " + suffix);
                    for (int i = 0; i < diff ; i++) {
                        middleValue += "0";
                    }
                }
            } else if (productName.contains("LOC")) {
                prefix = "LC";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 13 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.toLowerCase().contains("Personal Accident".toLowerCase())) {
                prefix = "CP";
                suffix = policyNumber;
                int size = policyNumber.length();
                int diff = 11 - size;

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Pay Protect")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            } else if (productName.equalsIgnoreCase("Cover For Life")) {
                int size = policyNumber.length();
                int diff = 13 - size;
                prefix =  policyNumber.substring(0, 2);
                suffix = policyNumber.substring(2, size);
                log.info("Prefix: " + prefix);
                log.info("Suffix: " + suffix);

                for (int i = 0; i < diff; i++){
                    middleValue += "0";
                }
            }

            actualPolicyNumber = prefix + middleValue + suffix;
            log.debug("Policy Number: " + actualPolicyNumber);
            return actualPolicyNumber;
        } catch (Exception e) {
            log.error("Error while getting flow policy number format.");
            throw new Exception("Error while getting flow policy number format.");
        }
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String dateToString(Date date, String dateFormat) {
        return new SimpleDateFormat(dateFormat).format(date);
    }

    private String doubleToDateString(String dateDoubleValue) {
        Double excelDateMilliSeconds = Double.valueOf(dateDoubleValue);
        Date javaDate = DateUtil.getJavaDate(excelDateMilliSeconds);
        return dateToString(javaDate, "dd-MM-YYYY");
    }

    private Date formatDate(String dateValue) throws Exception {
        try {
            if (!dateValue.contains("-")) {
               dateValue = doubleToDateString(dateValue);
            }
            String[] debitOderDateAttributes = dateValue.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateValue);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateValue);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

    public String getDateStringWithFormat(String dateString, String dateFormat) throws Exception {
        try {
            return !dateString.isEmpty() ? dateToString(formatDate(dateString), dateFormat) : "";
        } catch (Exception e) {
            throw new Exception("Error while getting date format from date value: " + dateString);
        }
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
    }

    private void reportSuccess(String[] reportableFields, ScenarioOperator scenarioOperator, String negativeResult) {
        scenarioOperator.setResult("Pass");
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.SUCCESS);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), negativeResult, TestResultReportFlag.WARNING);
    }

    private void reportFailure(String[] reportableFields, ScenarioOperator scenarioOperator, String error) {
        scenarioOperator.setResult("Fail");
        scenarioOperator.setFailureReason(error);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Result"), scenarioOperator.getResult(), TestResultReportFlag.FAIL);
        scenarioOperator.writeToReport(Arrays.asList(reportableFields).indexOf("Failure Reason"), scenarioOperator.getFailureReason(), TestResultReportFlag.WARNING);
    }
}
