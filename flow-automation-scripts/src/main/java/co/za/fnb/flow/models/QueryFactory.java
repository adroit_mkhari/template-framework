package co.za.fnb.flow.models;

import cucumber.api.DataTable;
import gherkin.formatter.model.Row;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

public class QueryFactory {
    static HashMap<String, Query> queries = new HashMap<>();

    private static int getHeaderIndex(Object[] headers, String header) {
        return Arrays.asList(headers).indexOf(header);
    }

    public static void loadQueries(DataTable queriesTable) {
        Object[] headers = queriesTable.getGherkinRows().get(0).getCells().toArray();
        List<String> dataEntry;
        Query query;
        for (
                Row row : queriesTable.getGherkinRows()) {
            if (row.getLine() != 1) {
                dataEntry = row.getCells();
                query = new Query(
                        dataEntry.get(getHeaderIndex(headers,"Policy Code")),
                        dataEntry.get(getHeaderIndex(headers,"Query")),
                        dataEntry.get(getHeaderIndex(headers,"Description")));
                queries.put(query.getPolicyCode(), query);
            }
        }
    }

    public static Query getQuery(String policyCode) {
        return queries.get(policyCode);
    }
}
