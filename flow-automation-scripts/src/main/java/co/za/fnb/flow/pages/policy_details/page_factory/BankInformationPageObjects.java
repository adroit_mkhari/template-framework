package co.za.fnb.flow.pages.policy_details.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;


public class BankInformationPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:bankName_label")
    private  WebElement bankNameSelect;

    @FindBy(id = "createSearchItemView:mainTabView:bankName_items")
    private WebElement bankNameSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:branchName")
    private WebElement branchName;

    @FindBy(id = "createSearchItemView:mainTabView:accountNumber")
    private WebElement accountNumber;

    @FindBy(id = "createSearchItemView:mainTabView:branchCode")
    private WebElement branchCode;

    @FindBy(id = "createSearchItemView:mainTabView:accountType_label")
    private WebElement accountTypeSelect;

    @FindBy(id = "createSearchItemView:mainTabView:accountType_items")
    private WebElement accountTypeSelectItems;

    @FindBy(id = "createSearchItemView:mainTabView:premiumPayerTypeOfId")
    private WebElement premiumPayerTypeOfId;

    @FindBy(id = "createSearchItemView:mainTabView:premiumPayerIdNumber")
    private WebElement premiumPayerIdNumber;

    @FindBy(id = "createSearchItemView:mainTabView:premiumPayerName")
    private WebElement premiumPayerName;

    @FindBy(id = "createSearchItemView:mainTabView:debitOrderDate")
    private WebElement debitOrderDate;

    @FindBy(id = "createSearchItemView:mainTabView:nextDueDate_input")
    private WebElement nextDueDate;

    public BankInformationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getBankNameSelect() {
        return bankNameSelect;
    }

    public WebElement getBankNameSelectItems() {
        return bankNameSelectItems;
    }

    public WebElement getBranchName() {
        return branchName;
    }

    public WebElement getAccountNumber() {
        return accountNumber;
    }

    public WebElement getBranchCode() {
        return branchCode;
    }

    public WebElement getAccountTypeSelect() {
        return accountTypeSelect;
    }

    public WebElement getAccountTypeSelectItems() {
        return accountTypeSelectItems;
    }

    public WebElement getPremiumPayerTypeOfId() {
        return premiumPayerTypeOfId;
    }

    public WebElement getPremiumPayerIdNumber() {
        return premiumPayerIdNumber;
    }

    public WebElement getPremiumPayerName() {
        return premiumPayerName;
    }

    public WebElement getDebitOrderDate() {
        return debitOrderDate;
    }

    public WebElement getNextDueDate() {
        return nextDueDate;
    }
}
