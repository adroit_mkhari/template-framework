package co.za.fnb.flow.models.custom_exceptions;

public class NegativeTestException extends Exception {
    public NegativeTestException(String message) {
        super(message);
    }
}
