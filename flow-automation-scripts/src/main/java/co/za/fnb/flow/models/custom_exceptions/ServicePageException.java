package co.za.fnb.flow.models.custom_exceptions;

public class ServicePageException extends Exception {
    public ServicePageException(String message) {
        super(message);
    }
}
