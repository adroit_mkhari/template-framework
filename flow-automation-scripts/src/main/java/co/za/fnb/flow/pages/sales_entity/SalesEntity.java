package co.za.fnb.flow.pages.sales_entity;

import co.za.fnb.flow.pages.sales_entity.page_factory.SalesEntityPageObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import co.za.fnb.flow.pages.BasePage;

public class SalesEntity extends BasePage {
    SalesEntityPageObjects salesEntityPageObjects = new SalesEntityPageObjects(driver);

    public SalesEntity(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }
}
