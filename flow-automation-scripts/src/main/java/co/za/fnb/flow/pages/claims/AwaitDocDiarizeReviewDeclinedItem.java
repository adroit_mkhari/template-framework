package co.za.fnb.flow.pages.claims;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.pages.claims.page_factory.AwaitDocDiarizeReviewDeclinedItemObjects;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AwaitDocDiarizeReviewDeclinedItem extends BasePage {
    private Logger log  = LogManager.getLogger(AwaitDocDiarizeReviewDeclinedItem.class);
    AwaitDocDiarizeReviewDeclinedItemObjects awaitDocDiarizeReviewDeclinedItemObjects = new AwaitDocDiarizeReviewDeclinedItemObjects(driver);

    public AwaitDocDiarizeReviewDeclinedItem(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
    }

    public boolean waitForContent() throws Exception {
        log.info("Wiating For Content On: awaitdocOrDiarizeItemz");
        try {
            /*try {
                driver.switchTo().frame(0);
            } catch (Exception e) {
                // TODO: Handle Exception
                log.debug("Error switching to frame: " + e.getMessage());
            }
            WebElement updateDialog = awaitDocDiarizeReviewDeclinedItemObjects.getUpdateDialog();*/

            WebElement submit = awaitDocDiarizeReviewDeclinedItemObjects.getSubmit();
            WebDriverWait webDriverWait = new WebDriverWait(driver, 15);
            webDriverWait.until(ExpectedConditions.visibilityOf(submit));
            return true;
        } catch (Exception e) {
            log.error("Content Not Displayed.");
            return false;
        }
    }

    public void clickSubmit() throws Exception {
        try {
            click(awaitDocDiarizeReviewDeclinedItemObjects.getSubmit());
        } catch (Exception e) {
            log.error("Error while clicking submit.");
            throw new Exception("Error while clicking submit.");
        }
    }

}
