package co.za.fnb.flow.pages.financial_administration.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public class FinancialAdministrationPageObjects {

    private String financialAdminDescription = "createSearchItemView:mainTabView:financialAdminTable:0:financialAdminDescription";
    private String financialAdminActionDate = "createSearchItemView:mainTabView:financialAdminTable:0:financialAdminActionDate";
    private String financialAdminAmount = "createSearchItemView:mainTabView:financialAdminTable:0:financialAdminAmount";
    private String financialAdminPolicyBalance = "createSearchItemView:mainTabView:financialAdminTable:0:financialAdminPolicyBalance";
    private String financialAdminMessage = "createSearchItemView:mainTabView:financialAdminTable:0:financialAdminMessage";
    private String transDate = "createSearchItemView:mainTabView:financialAdminTable:0:transDate";
    private String trackingStatus = "createSearchItemView:mainTabView:financialAdminTable:0:trackingStatus";

    public FinancialAdministrationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getFinancialAdminDescription() {
        return financialAdminDescription;
    }

    public String getFinancialAdminActionDate() {
        return financialAdminActionDate;
    }

    public String getFinancialAdminAmount() {
        return financialAdminAmount;
    }

    public String getFinancialAdminPolicyBalance() {
        return financialAdminPolicyBalance;
    }

    public String getFinancialAdminMessage() {
        return financialAdminMessage;
    }

    public String getTransDate() {
        return transDate;
    }

    public String getTrackingStatus() {
        return trackingStatus;
    }
}
