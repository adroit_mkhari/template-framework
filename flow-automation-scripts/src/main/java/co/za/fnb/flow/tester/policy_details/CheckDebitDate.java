package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.DebitDateCheck;
import co.za.fnb.flow.pages.CreateSearchItemPage;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.BankInformation;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import java.text.SimpleDateFormat;
import java.util.Date;

public class CheckDebitDate {
    private Logger log  = LogManager.getLogger(CheckDebitDate.class);
    WebDriver driver;
    DebitDateCheck debitDateCheck;
    BankInformation bankInformation;
    CreateSearchItemPage createSearchItemPage;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();

    public CheckDebitDate(WebDriver driver, DebitDateCheck debitDateCheck, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.debitDateCheck = debitDateCheck;
        bankInformation = new BankInformation(driver, scenarioOperator);
        createSearchItemPage = new CreateSearchItemPage(driver, scenarioOperator);
    }

    public ErrorHandle getErrorHandle() {
        return errorHandle;
    }

    public TestHandle getTestHandle() {
        return testHandle;
    }

    public void setExpectedResults() {
        // bankInformation.setExpectedResults(premiumStatus.getExpectResult());
    }

    public void check() throws Exception {
        log.info("Check Debit Order Date");
        String debitOrderDate = bankInformation.getDebitOrderDate();
        String nextDueDate = bankInformation.getNextDueDate();
        String policyInArrears = createSearchItemPage.getPolicyInArrears();

        Date formattedNextDueDate = formatDate(nextDueDate);
        Date formattedNextDueDateData = formatDate(debitDateCheck.getNextDueDate());

        if (!debitOrderDate.equalsIgnoreCase(debitDateCheck.getDebitOrderDate())) {
            testHandle.setSuccess(false);
            errorHandle.setError("Expected Debit Order Date: " + debitDateCheck.getDebitOrderDate() + " Actual Debit Order Date: " + debitOrderDate);
            throw new Exception("Expected Debit Order Date: " + debitDateCheck.getDebitOrderDate() + " Actual Debit Order Date: " + debitOrderDate);
        }

        if (formattedNextDueDate == null || !formattedNextDueDate.equals(formattedNextDueDateData)) {
            testHandle.setSuccess(false);
            errorHandle.setError("Expected Next Due Date: " + debitDateCheck.getNextDueDate() + " Actual Next Due Date: " + nextDueDate);
            throw new Exception("Expected Next Due Date: " + debitDateCheck.getNextDueDate() + " Actual Next Due Date: " + nextDueDate);
        }

        if (!policyInArrears.equalsIgnoreCase(debitDateCheck.getAreas())) {
            testHandle.setSuccess(false);
            errorHandle.setError("Expected Policy In Areas: " + debitDateCheck.getAreas() + " Actual Policy In Areas: " + policyInArrears);
            throw new Exception("Expected Policy In Areas: " + debitDateCheck.getAreas() + " Actual Policy In Areas: " + policyInArrears);
        }

        testHandle.setSuccess(true);
    }

    private Date formatDate(String dateString) throws Exception {
        try {
            String[] debitOderDateAttributes = dateString.split("-");
            if (debitOderDateAttributes[0].length() == 4) {
                return new SimpleDateFormat("yyyy-MM-dd").parse(dateString);
            } else if (debitOderDateAttributes[2].length() == 4) {
                return new SimpleDateFormat("dd-MM-yyyy").parse(dateString);
            }
        } catch (Exception e) {
            throw new Exception("Error while formatting date.");
        }
        return null;
    }

}