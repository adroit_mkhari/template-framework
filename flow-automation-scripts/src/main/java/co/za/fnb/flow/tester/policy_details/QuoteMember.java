package co.za.fnb.flow.tester.policy_details;

import co.za.fnb.flow.models.policy_details.Member;
import co.za.fnb.flow.pages.CreateMultipleMintItemTable;
import co.za.fnb.flow.pages.ErrorHandle;
import co.za.fnb.flow.pages.TestHandle;
import co.za.fnb.flow.pages.policy_details.information.*;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;

import static co.za.fnb.flow.tester.MemberRelationship.*;

public class QuoteMember {
    private Logger log  = LogManager.getLogger(QuoteMember.class);
    WebDriver driver;
    Member member;
    PolicyInformation policyInformation;
    BeneficiaryInformation beneficiaryInformation;
    MemberInformation memberInformation;
    QuoteInformation quoteInformation;
    BankInformation bankInformation;
    private String comment;
    private boolean valid;
    private ErrorHandle errorHandle = new ErrorHandle();
    private TestHandle testHandle = new TestHandle();
    private final Double MIN_COVER_AMOUNT_FOR_DISCOUNT = 20000.00;

    public QuoteMember(WebDriver driver, Member member, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.member = member;
        policyInformation = new PolicyInformation(driver, scenarioOperator);
        beneficiaryInformation = new BeneficiaryInformation(driver, scenarioOperator);
        memberInformation = new MemberInformation(driver, scenarioOperator);
        quoteInformation = new QuoteInformation(driver, scenarioOperator);
        bankInformation = new BankInformation(driver, scenarioOperator);
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public void setExpectedResults() {
        policyInformation.setExpectedResults(member.getExpectResult());
    }

    public void quoteExistingMember(Double discount, Double expectedPremiumAmount, Double expectedQuotedPremiumAmount) throws Exception {
        String clearValueFlag = "DELETE";
        boolean isLawOnCall = member.getProductName().contains("LOC");
        boolean isPersonalAccident = member.getProductName().equalsIgnoreCase("Personal Accident");
        int numberOfExistingMembers = memberInformation.getNumberOfExistingMembers();

        log.info("Handling Quote Existing Member logic");
        if (numberOfExistingMembers != 0) {
            String memberRelationship,  coverAmount = member.getCoverAmount(), coverAmountFieldText, quotePremium, premium;
            String policyHolderCoverAmount = "", spouseCoverAmount = "", currentMemberCoverAmount, currentMemberRelation;
            Double coverAmountValue = 0.00, coverAmountFieldValue, quotePremiumValue = 0.00, policyHolderCoverAmountValue = 0.00;
            Double spouseCoverAmountValue = 0.00, totalQ = 0.00, currentMemberCoverAmountValue, premiumValue;
            boolean memberQuoted = false;

            for (int i = 0; i < numberOfExistingMembers; i++) {
                memberInformation.selectMember(i);
                memberRelationship = memberInformation.getMemberRelationship(i);
                log.info("Relationship: " + memberRelationship);
                if (memberRelationship.equalsIgnoreCase(member.getRelationship())) {
                    memberQuoted = true;
                    coverAmount = getDoubleString(coverAmount);
                    coverAmountValue = stringToDouble(coverAmount);
                    // Load Cover Amount
                    memberInformation.loadCoverAmount(i);
                    if (!coverAmount.isEmpty()) {
                        if (coverAmount.equalsIgnoreCase(clearValueFlag)) {
                            // TODO: Check If we will ever need to clear cover amount.
                            // Note: Clear does not work.
                            // quoteInformation.doubleClickCoverAmount(i);
                        } else {
                            memberInformation.updateCoverAmount(i, coverAmountValue.toString());
                        }
                        Thread.sleep(1000);
                    }

                    coverAmountFieldText = memberInformation.getMemberCoverAmount(i);
                    coverAmountFieldText = getDoubleString(coverAmountFieldText);
                    coverAmountFieldValue = stringToDouble(coverAmountFieldText);

                    if (!coverAmountFieldValue.equals(coverAmountValue)) {
                        try {
                            memberInformation.selectMemberInfoCoverAmount(coverAmount, i);
                        } catch (Exception e) {
                            log.error("Failed to select the correct cover amount value \"" + coverAmount + "\". Selected \"" + coverAmountFieldText + "\"");
                            throw new Exception("Failed to select the correct cover amount value \"" + coverAmount + "\". Selected \"" + coverAmountFieldText + "\"");
                        }
                    }

                    quotePremium = memberInformation.getPolicyDetailsPremium(i);
                    quotePremium = getDoubleString(quotePremium);
                    quotePremiumValue = stringToDouble(quotePremium);

                    String memberRelation;
                    for (int x = 0; x < numberOfExistingMembers; x++) {
                        memberRelation = memberInformation.getMemberRelationship(x);
                        if (memberRelation.equalsIgnoreCase(String.valueOf(POLICY_HOLDER))) {
                            policyHolderCoverAmount = memberInformation.getMemberCoverAmount(x);
                            policyHolderCoverAmount = getDoubleString(policyHolderCoverAmount);
                            policyHolderCoverAmountValue = stringToDouble(policyHolderCoverAmount);
                        } else if (memberRelation.equalsIgnoreCase(String.valueOf(SPOUSE))) {
                            spouseCoverAmount = memberInformation.getMemberCoverAmount(x);
                            spouseCoverAmount = getDoubleString(spouseCoverAmount);
                            spouseCoverAmountValue = stringToDouble(spouseCoverAmount);
                        }
                    }

                    boolean canApplyDiscount = policyHolderCoverAmountValue.equals(spouseCoverAmountValue) && policyHolderCoverAmountValue >= MIN_COVER_AMOUNT_FOR_DISCOUNT && spouseCoverAmountValue >= MIN_COVER_AMOUNT_FOR_DISCOUNT;

                    for(int j = 0; j < numberOfExistingMembers ; j++) {
                        currentMemberRelation = memberInformation.getMemberRelationship(j);
                        premium = memberInformation.getPolicyDetailsPremium(j);
                        premium = getDoubleString(premium);
                        premiumValue = stringToDouble(premium);

                        if (canApplyDiscount) {
                            if (currentMemberRelation.equalsIgnoreCase(String.valueOf(SPOUSE)) || currentMemberRelation.equalsIgnoreCase(String.valueOf(POLICY_HOLDER))) {
                                premiumValue = premiumValue - discount;
                            } else if (currentMemberRelation.equalsIgnoreCase(String.valueOf(CHILD))) {
                                currentMemberCoverAmount = memberInformation.getMemberCoverAmount(j);
                                currentMemberCoverAmount = getDoubleString(currentMemberCoverAmount);
                                currentMemberCoverAmountValue = stringToDouble(currentMemberCoverAmount);

                                if (currentMemberCoverAmountValue.equals(10000.00)) {
                                    premiumValue = premiumValue - discount;
                                }
                            }
                        }
                        totalQ += premiumValue;
                    }

                    if (!isPersonalAccident) {
                        if (!expectedPremiumAmount.equals(quotePremiumValue))  {
                            log.error("Premium Amounts not correct: Expected Premium Amount: \"" + expectedPremiumAmount + "\" , Retrieved Premium Amount: \"" + quotePremiumValue);
                            throw new Exception("Premium Amounts not correct: Expected Premium Amount: \"" + expectedPremiumAmount + "\" , Retrieved Premium Amount: \"" + quotePremiumValue);
                        }
                    }

                    quoteInformation.getQuote();
                    if (isPersonalAccident) {
                        Thread.sleep(1000);
                        quoteInformation.getResponses(false);
                    }
                    Thread.sleep(1000);
                    String quotedPremiumAmount = quoteInformation.getQuotedPremiumAmount();
                    quotedPremiumAmount = getDoubleString(quotedPremiumAmount);
                    Double quotedPremiumAmountValue = stringToDouble(quotedPremiumAmount);
                    // Double actualQ = quotedPremiumAmountValue;
                    Thread.sleep(1000);

                    if (!isPersonalAccident) {
                        // reportHandler.setCalculatedPremiumOrQuote("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + actualQ);
                        log.debug("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
                        if(!quotedPremiumAmountValue.equals(totalQ) ){
                            log.error("Quoted Premium Amount Value Not Correct");
                            throw new Exception("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
                        }
                    } else {
                        if (!quotedPremiumAmountValue.equals(expectedQuotedPremiumAmount)) {
                            log.error("Expected Quoted Premium Amount Value Not Correct");
                            throw new Exception("Calculated Quote: \"" + expectedQuotedPremiumAmount + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
                        }
                    }
                    Thread.sleep(1000);
                    quoteInformation.getResponses(false);
                    Thread.sleep(1000);
                    break;
                }
            }

            if (!memberQuoted) {
                log.error("No member was quoted. Reason: Relationships");
                throw new Exception("No member was quoted. Reason: Relationships");
            }
        }
        flagTestScenarioAsSuccess();
        log.info("Done Handling Quote Existing Member logic");
    }

    public void quoteNewMember(Double discount, Double expectedPremiumAmount, Double expectedQuotedPremiumAmount) throws Exception {
        String clearValueFlag = "DELETE";
        boolean isLawOnCall = member.getProductName().contains("LOC");
        boolean isPersonalAccident = member.getProductName().equalsIgnoreCase("Personal Accident");
        int numberOfMembersToAdd = getIntegerValue(member.getNumberOfMembers()) != -1 ? getIntegerValue(member.getNumberOfMembers()) : 1;

        log.info("Handling Quote New Member logic");
        int memberInformationTableSize;
        int quoteInformationTableSize;
        String policyHolderCoverAmount;
        Double policyHolderCoverAmountValue = 0.00;
        String spouseCoverAmount = "";
        Double spouseCoverAmountValue = 0.00;
        String memberRelation;
        String quoteMemberRelation;
        Double totalQ;

        for (int i = 0; i < numberOfMembersToAdd; i++) {
            quoteInformation = policyInformation.newMember();
            Thread.sleep(2000);
            quoteInformation.selectMember(i);
            quoteInformation.moveToMemberRelationshipAndDoubleClick(i);
            quoteInformation.selectMemberRelationship(i, member.getRelationship());
            Thread.sleep(2000);

            if (!member.getCompanyOrFullName().isEmpty()) {
                if (member.getCompanyOrFullName().equalsIgnoreCase(clearValueFlag)) {
                    quoteInformation.clearCompanyOrFullName(i);
                } else {
                    quoteInformation.updateCompanyOrFullName(i, member.getCompanyOrFullName());
                }
                Thread.sleep(1000);
            }

            if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                if (!member.getTradingAsOrMiddleName().isEmpty()) {
                    if (member.getTradingAsOrMiddleName().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearMiddleName(i);
                    } else {
                        quoteInformation.updateMiddleName(i, member.getTradingAsOrMiddleName());
                    }
                    Thread.sleep(1000);
                }
            }

            if (!member.getCompanyRegistrationOrIdNumber().isEmpty()) {
                if (member.getCompanyRegistrationOrIdNumber().equalsIgnoreCase(clearValueFlag)) {
                    quoteInformation.clearIdNumber(i);
                } else {
                    quoteInformation.updateIdNumber(i, member.getCompanyRegistrationOrIdNumber());
                }
                Thread.sleep(1000);
            }

            // Re-click at the member to load the Id Number.
            quoteInformation.selectMember(i);
            Thread.sleep(1000);

            if (!member.getProductName().equalsIgnoreCase("LOC-Business")) {
                if (!member.getDateOfBirth().isEmpty()) {
                    if (member.getDateOfBirth().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearDateOfBirth(i);
                    } else {
                        quoteInformation.updateDateOfBirth(i, member.getDateOfBirth());
                    }
                    Thread.sleep(1000);
                }

                // Re-click at the member to reload/refresh.
                quoteInformation.selectMember(i);
                Thread.sleep(1000);

                if (!member.getGender().isEmpty()) {
                    quoteInformation.moveToMemberGenderAndDoubleClick(i);
                    quoteInformation.selectMemberGender(i, member.getGender());
                    Thread.sleep(1000);
                }
            } else {
                // TODO: Check if the email and contact number logic should go here. But at this point it does not make sense for it to be here.
                if (!member.getEmailAddress().isEmpty()) {
                    if (member.getEmailAddress().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearEmailAddress(i);
                    } else {
                        quoteInformation.updateEmailAddress(i, member.getEmailAddress());
                    }
                    Thread.sleep(1000);
                }

                if (!member.getCellPhoneNumber().isEmpty()) {
                    if (member.getCellPhoneNumber().equalsIgnoreCase(clearValueFlag)) {
                        quoteInformation.clearCellPhoneNumber(i);
                    } else {
                        quoteInformation.updateCellPhoneNumber(i, member.getCellPhoneNumber());
                    }
                    Thread.sleep(1000);
                }
            }

            if (!member.getProductName().contains("LOC")) {
                // Load Cover Amount
                quoteInformation.loadCoverAmount(i);

                if (!member.getCoverAmount().isEmpty()) {
                    if (member.getCoverAmount().equalsIgnoreCase(clearValueFlag)) {
                        // TODO: Check If we will ever need to clear cover amount.
                        // Note: Clear does not work.
                        // quoteInformation.doubleClickCoverAmount(i);
                    } else {
                        Double amount = Double.valueOf(member.getCoverAmount());
                        quoteInformation.updateCoverAmount(i, amount.toString());
                    }
                    Thread.sleep(1000);
                }
            }

            // TODO: Find out if reassigning quoteInformation changes anything.
            quoteInformation = policyInformation.addToPolicy();
            Thread.sleep(1000);

            // TODO: Check if we need to get any responses.
            policyInformation.getResponses(false);
        }

        memberInformationTableSize = memberInformation.getMemberInformationTableItemsSize();
        quoteInformationTableSize = quoteInformation.getMemberInformationTableItemsSize();

        for (int j = 0; j < memberInformationTableSize; j++) {
            memberRelation = memberInformation.getMemberRelationship(j);
            if (memberRelation.equalsIgnoreCase(String.valueOf(POLICY_HOLDER))) {
                policyHolderCoverAmount = memberInformation.getMemberCoverAmount(j);
                policyHolderCoverAmount = getDoubleString(policyHolderCoverAmount);
                policyHolderCoverAmountValue = stringToDouble(policyHolderCoverAmount);
            } else if (memberRelation.equalsIgnoreCase(String.valueOf(SPOUSE))) {
                spouseCoverAmount = memberInformation.getMemberCoverAmount(j);
                spouseCoverAmount = getDoubleString(spouseCoverAmount);
                spouseCoverAmountValue = stringToDouble(spouseCoverAmount);
            }
        }

        if (spouseCoverAmount.isEmpty()) {
            if (member.getRelationship().equalsIgnoreCase(String.valueOf(SPOUSE))) {
                for (int z = 0; z < quoteInformationTableSize; z++) {
                    quoteMemberRelation = quoteInformation.getPolicyDetailsRelationship(z);
                    if (quoteMemberRelation.equalsIgnoreCase(String.valueOf(SPOUSE))) {
                        quoteInformation.loadCoverAmount(z);
                        spouseCoverAmount = quoteInformation.getPolicyDetailsCoverAmount(z);
                        spouseCoverAmount = getDoubleString(spouseCoverAmount);
                        spouseCoverAmountValue = stringToDouble(spouseCoverAmount);
                    }
                }
            }
        }
        totalQ = 0.00;
        String currentMemberRelation;
        String currentMemberCoverAmount;
        Double currentMemberCoverAmountValue = 0.00;
        String premium;
        Double premiumValue;
        boolean canApplyDiscount = policyHolderCoverAmountValue.equals(spouseCoverAmountValue) && policyHolderCoverAmountValue >= MIN_COVER_AMOUNT_FOR_DISCOUNT && spouseCoverAmountValue >= MIN_COVER_AMOUNT_FOR_DISCOUNT;

        for(int j = 0; j < memberInformationTableSize ; j++) {
            currentMemberRelation = memberInformation.getMemberRelationship(j);
            premium = memberInformation.getPolicyDetailsPremium(j);
            premium = getDoubleString(premium);
            premiumValue = stringToDouble(premium);

            if (canApplyDiscount) {
                if (currentMemberRelation.equalsIgnoreCase(String.valueOf(SPOUSE)) || currentMemberRelation.equalsIgnoreCase(String.valueOf(POLICY_HOLDER))) {
                    premiumValue = premiumValue - discount;
                } else if (currentMemberRelation.equalsIgnoreCase(String.valueOf(CHILD))) {
                    currentMemberCoverAmount = memberInformation.getMemberCoverAmount(j);
                    currentMemberCoverAmount = getDoubleString(currentMemberCoverAmount);
                    currentMemberCoverAmountValue = stringToDouble(currentMemberCoverAmount);

                    if (currentMemberCoverAmountValue.equals(10000.00)) {
                        premiumValue = premiumValue - discount;
                    }
                }
            }
            totalQ += premiumValue;
        }

        String quotePremium;
        Double quotePremiumValue;
        Double quotePremiumValueBeforeDiscount;
        if (!isPersonalAccident) {
            for (int k = 0; k < quoteInformationTableSize; k++) {
                quotePremium = quoteInformation.getQuotePremium(k);
                quotePremium = getDoubleString(quotePremium);
                quotePremiumValue = stringToDouble(quotePremium);
                quotePremiumValueBeforeDiscount = quotePremiumValue;

                if (canApplyDiscount) {
                    quoteMemberRelation = quoteInformation.getPolicyDetailsRelationship(k);

                    if (quoteMemberRelation.equalsIgnoreCase(String.valueOf(SPOUSE)) || quoteMemberRelation.equalsIgnoreCase(String.valueOf(POLICY_HOLDER))) {
                        quotePremiumValue = quotePremiumValue - discount;
                    } else if (quoteMemberRelation.equalsIgnoreCase(String.valueOf(CHILD))) {
                        quoteInformation.loadCoverAmount(k);
                        currentMemberCoverAmount = quoteInformation.getPolicyDetailsCoverAmount(k);
                        currentMemberCoverAmount = getDoubleString(currentMemberCoverAmount);
                        currentMemberCoverAmountValue = stringToDouble(currentMemberCoverAmount);

                        if (currentMemberCoverAmountValue.equals(10000.00)) {
                            quotePremiumValue = quotePremiumValue - discount;
                        }
                    }
                }

                if (!expectedPremiumAmount.equals(quotePremiumValueBeforeDiscount))  {
                    log.error("Premium Amounts not correct: Expected Premium Amount: \"" + expectedPremiumAmount + "\" , Retrieved Premium Amount: \"" + quotePremiumValue);
                    throw new Exception("Premium Amounts not correct: Expected Premium Amount: \"" + expectedPremiumAmount + "\" , Retrieved Premium Amount: \"" + quotePremiumValue);
                }
                totalQ += quotePremiumValue;
            }
        }

        quoteInformation.getQuote();
        if (isPersonalAccident) {
            Thread.sleep(1000);
            quoteInformation.getResponses(false);
        }
        Thread.sleep(1000);
        String quotedPremiumAmount = quoteInformation.getQuotedPremiumAmount();
        quotedPremiumAmount = getDoubleString(quotedPremiumAmount);
        Double quotedPremiumAmountValue = stringToDouble(quotedPremiumAmount);
        // Double actualQ = quotedPremiumAmountValue;

        if (!isPersonalAccident) {
            // reportHandler.setCalculatedPremiumOrQuote("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + actualQ);
            log.debug("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
            if(!quotedPremiumAmountValue.equals(totalQ) ){
                log.error("Quoted Premium Amount Value Not Correct");
                // reportHandler.setFailureReason("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + actualQ);
                throw new Exception("Calculated Quote: \"" + totalQ + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
            }
        } else {
            if (!quotedPremiumAmountValue.equals(expectedQuotedPremiumAmount)) {
                log.error("Expected Quoted Premium Amount Value Not Correct");
                // reportHandler.setFailureReason("Calculated Quote: \"" + expectedQuotedPremiumAmountValue + "\" , Actual Quote: \"" + actualQ);
                throw new Exception("Calculated Quote: \"" + expectedPremiumAmount + "\" , Actual Quote: \"" + quotedPremiumAmountValue);
            }
        }

        Thread.sleep(1000);
        quoteInformation.getResponses(true);
        Thread.sleep(1000);

        flagTestScenarioAsSuccess();
        log.info("Done Handling Quote New Member logic");
    }

    private void flagTestScenarioAsSuccess() {
        // TODO: Revisit this logic, because it determined whether or not a test has failed.
        testHandle.setSuccess(true);
        policyInformation.setTestHandle(testHandle);
        policyInformation.setErrorHandle(errorHandle);
    }

    private void flagTestScenarioAsFailure(String errorMessage) {
        // TODO: Revisit this logic, because it determined whether or not a test has failed.
        testHandle.setSuccess(false);
        errorHandle.setError(errorMessage);
        policyInformation.setTestHandle(testHandle);
        policyInformation.setErrorHandle(errorHandle);
    }

    private Double stringToDouble(String policyHolderCoverAmount) throws Exception {
        try {
            return Double.valueOf(policyHolderCoverAmount);
        } catch (Exception e) {
            log.error("Error while getting double value: " + e.getMessage());
            throw new Exception("Error while getting double value: " + e.getMessage());
        }
    }

    private String getDoubleString(String numberString) {
        // TODO: Try to get the double value first.
        if (numberString.isEmpty()) {
            numberString = "0.00";
        }

        if (!numberString.contains(".")) {
            return numberString + ".00";
        } else {
            return numberString;
        }
    }

    private String getExpectedDoubleNumberFormat(String premiumAmountDataValue) {
        if (!premiumAmountDataValue.isEmpty()) {
            if (!premiumAmountDataValue.contains(".")) {
                if (premiumAmountDataValue.contains(",")) {
                    return premiumAmountDataValue.replace(",", ".");
                } else {
                    return premiumAmountDataValue + ".00";
                }
            }
        } else {
            log.error("Got an Empty String Value" );
            premiumAmountDataValue = "0.00";
        }
        return premiumAmountDataValue;
    }

    private int getIntegerValue(String value) {
        try {
            return !value.isEmpty() ? Integer.valueOf(value) : 1;
        } catch (Exception e) {
            log.debug(e.getMessage());
            try {
                if (value.contains(".")) {
                    String[] numberProps = value.split("\\.");
                    return Integer.valueOf(numberProps[0]);
                }
            } catch (Exception ex) {
                return 1;
            }
        }
        return -1;
    }

    public void save() throws Exception {
        try {
            log.info("Quote Member Save Logic.");
            // policyInformation.getResults();
            // testHandle = policyInformation.getTestHandle();
            // errorHandle = policyInformation.getErrorHandle();
        } catch (Exception e) {
            testHandle.setSuccess(false);
            errorHandle.setError(e.getMessage());
            throw new Exception("ERROR on Save: " + e.getMessage());
        }
    }

    public CreateMultipleMintItemTable getCreateMultipleMintItemTable() {
        return policyInformation.getCreateMultipleMintItemTable();
    }

    public void validate() {
        // TODO: Implement Validation Logic
        setValid(true);
    }

    public ErrorHandle getErrorHandle() {
        return this.errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        return this.testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

}