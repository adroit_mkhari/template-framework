package co.za.fnb.flow.pages.claims.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class AttorneyClaimsPaymentPageObjects {

    // region Claim Payment Details
    @FindBy(id = "frmAddClaimPaymentRightClick:claimPaymentListTable_data")
    WebElement claimPaymentListTableData;

    String claimPaymentListTableDataRowXpathLocator = "//*[@id=\"frmAddClaimPaymentRightClick:claimPaymentListTable_data\"]/tr[@data-ri=\"0\"]/td[1]";
    String btnApproveFailLocatorTemplateId = "frmAddClaimPaymentRightClick:claimPaymentListTable:0:btnApproveFail";
    String btneditClaimPaymentTemplateId = "frmAddClaimPaymentRightClick:claimPaymentListTable:0:btneditClaimPayment";
    String btnApproveViewTemplateId = "frmAddClaimPaymentRightClick:claimPaymentListTable:0:btnApproveView";
    // endregion

    public AttorneyClaimsPaymentPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getClaimPaymentListTableData() {
        return claimPaymentListTableData;
    }

    public String getClaimPaymentListTableDataRowXpathLocator() {
        return claimPaymentListTableDataRowXpathLocator;
    }

    public String getBtnApproveFailLocatorTemplateId() {
        return btnApproveFailLocatorTemplateId;
    }

    public String getBtneditClaimPaymentTemplateId() {
        return btneditClaimPaymentTemplateId;
    }

    public String getBtnApproveViewTemplateId() {
        return btnApproveViewTemplateId;
    }
}
