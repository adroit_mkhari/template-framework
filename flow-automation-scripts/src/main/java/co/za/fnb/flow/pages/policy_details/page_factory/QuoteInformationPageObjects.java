package co.za.fnb.flow.pages.policy_details.page_factory;

import co.za.fnb.flow.pages.policy_details.PolicyDetails;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class QuoteInformationPageObjects {

    private String policyDetailsHash = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailshash";
    private String policyDetailsRelationshipCE = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsRelationshipCE";
    private String policyDetailsRelationshipSelect = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsRelationship_label";
    private String policyDetailsRelationshipSelectItems = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsRelationship_items";
    private String policyDetailsFullNameCE = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsFullNameCE";
    private String policyDetailsFullName = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsFullName";
    private String policyDetailsMiddleNameCE = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsMiddleNameCE";
    private String policyDetailsMiddleName = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsMiddleName";
    private String policyDetailsIDNumberCE = "createSearchItemView:mainTabView:quoteInfoTable:0:j_idt337";
    private String policyDetailsIDNumber = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsIDNumber";
    private String dateOfBirthCE = "createSearchItemView:mainTabView:quoteInfoTable:0:dateOfBirthCE";
    private String dateOfBirth = "createSearchItemView:mainTabView:quoteInfoTable:0:dateOfBirth_input";
    private String policyDetailsGenderCE = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsGenderCE";
    private String policyDetailsGenderSelect = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsGender_label";
    private String policyDetailsGenderSelectItems = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsGender_items";
    private String emailAddressCE = "createSearchItemView:mainTabView:quoteInfoTable:0:emailAddressCE";
    private String emailAddress = "createSearchItemView:mainTabView:quoteInfoTable:0:emailAddress";
    private String cellPhoneNumberCE = "createSearchItemView:mainTabView:quoteInfoTable:0:cellPhoneNumberCE";
    private String cellPhoneNumber = "createSearchItemView:mainTabView:quoteInfoTable:0:cellPhoneNumber";
    private String policyDetailsCoverAmount = "createSearchItemView:mainTabView:quoteInfoTable:0:policyDetailsCoverAmount";
    private String policyDetailsCoverAmountRefresh = "createSearchItemView:mainTabView:quoteInfoTable:0:cai";
    private String quotePremium = "createSearchItemView:mainTabView:quoteInfoTable:0:quotePremium";
    private String quoteAction = "createSearchItemView:mainTabView:quoteInfoTable:0:quoteaction";
    private By quoteInformationTableItems = By.xpath("//*//tr//td/span[contains (@id,':quotePremium')]");

    public QuoteInformationPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public String getPolicyDetailsHash() {
        return policyDetailsHash;
    }

    public String getPolicyDetailsRelationshipCE() {
        return policyDetailsRelationshipCE;
    }

    public String getPolicyDetailsRelationshipSelect() {
        return policyDetailsRelationshipSelect;
    }

    public String getPolicyDetailsRelationshipSelectItems() {
        return policyDetailsRelationshipSelectItems;
    }

    public String getPolicyDetailsFullNameCE() {
        return policyDetailsFullNameCE;
    }

    public String getPolicyDetailsFullName() {
        return policyDetailsFullName;
    }

    public String getPolicyDetailsMiddleNameCE() {
        return policyDetailsMiddleNameCE;
    }

    public String getPolicyDetailsMiddleName() {
        return policyDetailsMiddleName;
    }

    public String getPolicyDetailsIDNumberCE() {
        return policyDetailsIDNumberCE;
    }

    public String getPolicyDetailsIDNumber() {
        return policyDetailsIDNumber;
    }

    public String getDateOfBirthCE() {
        return dateOfBirthCE;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public String getPolicyDetailsGenderCE() {
        return policyDetailsGenderCE;
    }

    public String getPolicyDetailsGenderSelect() {
        return policyDetailsGenderSelect;
    }

    public String getPolicyDetailsGenderSelectItems() {
        return policyDetailsGenderSelectItems;
    }

    public String getEmailAddressCE() {
        return emailAddressCE;
    }

    public String getEmailAddress() {
        return emailAddress;
    }

    public String getCellPhoneNumberCE() {
        return cellPhoneNumberCE;
    }

    public String getCellPhoneNumber() {
        return cellPhoneNumber;
    }

    public String getPolicyDetailsCoverAmount() {
        return policyDetailsCoverAmount;
    }

    public String getPolicyDetailsCoverAmountRefresh() {
        return policyDetailsCoverAmountRefresh;
    }

    public String getQuotePremium() {
        return quotePremium;
    }

    public String getQuoteAction() {
        return quoteAction;
    }

    public By getQuoteInformationTableItems() {
        return quoteInformationTableItems;
    }
}
