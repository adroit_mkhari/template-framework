package co.za.fnb.flow.tester.letters;

import co.za.fnb.flow.setup.PropertiesSetup;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.sikuli.script.Key;
import org.sikuli.script.Match;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import java.util.Properties;

import static co.za.fnb.flow.pages.Config.mainResourcesFolder;
import static org.sikuli.script.Commands.exists;

public class MintUserInterface {
    private Logger log  = LogManager.getLogger(MintUserInterface.class);
    private Screen screen = new Screen();
    private String sikuliMintImages = mainResourcesFolder + "\\sikuli_images\\mint";
    private Pattern mint_pre = new Pattern(sikuliMintImages + "\\mint-pre.png");
    private Pattern mint_tst = new Pattern(sikuliMintImages + "\\mint-tst.png");
    private Pattern user_id = new Pattern(sikuliMintImages + "\\user-id.png");
    private Pattern password = new Pattern(sikuliMintImages + "\\password.png");
    private Pattern sign_in = new Pattern(sikuliMintImages + "\\sign-in.png");
    private Pattern mint_user = new Pattern(sikuliMintImages + "\\mint-user.png");
    private Pattern mint_password = new Pattern(sikuliMintImages + "\\mint-password.png");
    private Pattern pressEnterToContinue = new Pattern(sikuliMintImages + "\\press-enter-to-continue.png");
    private Pattern selectionOrCommand = new Pattern(sikuliMintImages + "\\selection-or-command.png");
    private Pattern driveUserName = new Pattern(sikuliMintImages + "\\drive-user-name.png");
    private Pattern drivePassword = new Pattern(sikuliMintImages + "\\drive-password.png");
    private Pattern drivesignOnOk = new Pattern(sikuliMintImages + "\\drive-sign-in-ok.png");
    private Pattern mintCloseMenu = new Pattern(sikuliMintImages + "\\mint-close-menu.png");
    private Properties properties;
    private String application;

    public MintUserInterface() {
        log.info("Loading Properties");
        PropertiesSetup propertiesSetup = new PropertiesSetup();
        propertiesSetup.loadProperties();
        properties = propertiesSetup.getProperties();
        application = properties.getProperty("TSM_TARGET_APPLICATION");
        log.info("Done Loading Properties");
    }

    public String getSikuliMintImages() {
        return sikuliMintImages;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public Pattern getMint_pre() {
        return mint_pre;
    }

    public void setMint_pre(Pattern mint_pre) {
        this.mint_pre = mint_pre;
    }

    public Pattern getUser_id() {
        return user_id;
    }

    public void setUser_id(Pattern user_id) {
        this.user_id = user_id;
    }

    public Pattern getPassword() {
        return password;
    }

    public void setPassword(Pattern password) {
        this.password = password;
    }

    public Pattern getSign_in() {
        return sign_in;
    }

    public void setSign_in(Pattern sign_in) {
        this.sign_in = sign_in;
    }

    public Pattern getMint_user() {
        return mint_user;
    }

    public void setMint_user(Pattern mint_user) {
        this.mint_user = mint_user;
    }

    public Pattern getMint_password() {
        return mint_password;
    }

    public void setMint_password(Pattern mint_password) {
        this.mint_password = mint_password;
    }

    public Pattern getPressEnterToContinue() {
        return pressEnterToContinue;
    }

    public void setPressEnterToContinue(Pattern pressEnterToContinue) {
        this.pressEnterToContinue = pressEnterToContinue;
    }

    public Pattern getSelectionOrCommand() {
        return selectionOrCommand;
    }

    public void setSelectionOrCommand(Pattern selectionOrCommand) {
        this.selectionOrCommand = selectionOrCommand;
    }

    public Pattern getDriveUserName() {
        return driveUserName;
    }

    public void setDriveUserName(Pattern driveUserName) {
        this.driveUserName = driveUserName;
    }

    public Pattern getDrivePassword() {
        return drivePassword;
    }

    public void setDrivePassword(Pattern drivePassword) {
        this.drivePassword = drivePassword;
    }

    public Pattern getDrivesignOnOk() {
        return drivesignOnOk;
    }

    public void setDrivesignOnOk(Pattern drivesignOnOk) {
        this.drivesignOnOk = drivesignOnOk;
    }

    public Pattern getMintCloseMenu() {
        return mintCloseMenu;
    }

    public void signIn(String userId, String password) throws Exception {
        try {
            if (application.contains("ecm-pp")) {
                screen.rightClick(mint_pre);
            } else {
                screen.rightClick(mint_tst);
            }
            Thread.sleep(2000);
            screen.keyDown(Key.DOWN);
            Thread.sleep(10);
            screen.keyUp(Key.DOWN);
            Thread.sleep(10);
            screen.keyDown(Key.ENTER);
            Thread.sleep(10);
            screen.keyUp(Key.ENTER);
            Thread.sleep(5000);
            // screen.type(user_id, userId);
            // Thread.sleep(1000);
            // screen.type(this.password, password);
            // Thread.sleep(1000);
            // screen.click(sign_in);
        } catch (Exception e) {
            log.error("Error while signing user in: " + e.getMessage());
            throw new Exception("Error while signing user in: " + e.getMessage());
        }
    }

    public void continueSignIn(String userId, String password) throws Exception {
        try {
            Thread.sleep(5000);
            screen.type(mint_user, userId);
            Thread.sleep(1000);
            screen.type(Key.TAB);
            screen.type(password);
            Thread.sleep(1000);
            screen.type(Key.ENTER);
            Thread.sleep(10000);
        } catch (Exception e) {
            log.error("Error while cont. signing user in: " + e.getMessage());
            throw new Exception("Error while cont. signing user in: " + e.getMessage());
        }
    }

    public void openJobTerminal(String mintJobId) throws Exception {
        try {
            Match pressEnterToContinueExists = exists(pressEnterToContinue);
            if (pressEnterToContinueExists != null) {
                screen.click(pressEnterToContinue);
                screen.type(Key.ENTER);
                Thread.sleep(1000);
                screen.type(Key.ENTER);
                screen.type(Key.F8);
                screen.keyDown(Key.SHIFT);
                Thread.sleep(1000);
                screen.type(Key.ESC);
                Thread.sleep(1000);
                screen.keyUp(Key.SHIFT);
                Thread.sleep(3000);
                screen.type(mintJobId);
                screen.type(Key.ENTER);
            }
            Thread.sleep(3000);
        } catch (Exception e) {
            log.error("Error while opening Job Terminal: " + e.getMessage());
            throw new Exception("Error while opening Job Terminal: " + e.getMessage());
        }
    }

    public void runBatchJob(String job) throws Exception {
        try {
            screen.type(job);
            Thread.sleep(1000);
            screen.type(Key.ENTER);
        } catch (Exception e) {
            log.error("Error while running Job: " + job + " ...-> " + e.getMessage());
            throw new Exception("Error while running Job: " + job + " ...-> " + e.getMessage());
        }
    }

    public void closeMint() throws Exception {
        try {
            screen.click(mintCloseMenu);
            Thread.sleep(1000);
            screen.keyDown(Key.ALT);
            screen.keyDown(Key.F4);
            Thread.sleep(2000);
            screen.keyUp(Key.ALT);
            screen.keyUp(Key.F4);
        } catch (Exception e) {
            log.error("Error while closing mint " + e.getMessage());
            throw new Exception("Error while closing mint " + e.getMessage());
        }
    }

}
