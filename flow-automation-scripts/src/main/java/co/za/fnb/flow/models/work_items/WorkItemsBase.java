package co.za.fnb.flow.models.work_items;

import co.za.fnb.flow.models.ScenarioBase;

public class WorkItemsBase extends ScenarioBase {
    private String expectResult;
    private String auditTrail;
    private String updatedTe;
    private String risk;
    private String sanction;
    private String edd;
    private String kyc;
    private String status;

    public WorkItemsBase(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String expectResult, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run);
        this.expectResult = expectResult;
        this.auditTrail = auditTrail;
        this.updatedTe = updatedTe;
        this.risk = risk;
        this.sanction = sanction;
        this.edd = edd;
        this.kyc = kyc;
        this.status = status;
    }

    public String getExpectResult() {
        return expectResult;
    }

    public void setExpectResult(String expectResult) {
        this.expectResult = expectResult;
    }

    public String getAuditTrail() {
        return auditTrail;
    }

    public void setAuditTrail(String auditTrail) {
        this.auditTrail = auditTrail;
    }

    public String getUpdatedTe() {
        return updatedTe;
    }

    public void setUpdatedTe(String updatedTe) {
        this.updatedTe = updatedTe;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public String getSanction() {
        return sanction;
    }

    public void setSanction(String sanction) {
        this.sanction = sanction;
    }

    public String getEdd() {
        return edd;
    }

    public void setEdd(String edd) {
        this.edd = edd;
    }

    public String getKyc() {
        return kyc;
    }

    public void setKyc(String kyc) {
        this.kyc = kyc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "WorkItemsBase{" +
                "expectResult='" + expectResult + '\'' +
                ", auditTrail='" + auditTrail + '\'' +
                ", updatedTe='" + updatedTe + '\'' +
                ", risk='" + risk + '\'' +
                ", sanction='" + sanction + '\'' +
                ", edd='" + edd + '\'' +
                ", kyc='" + kyc + '\'' +
                ", status='" + status + '\'' +
                '}';
    }

}
