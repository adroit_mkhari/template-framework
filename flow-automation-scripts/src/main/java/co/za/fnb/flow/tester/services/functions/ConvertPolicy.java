package co.za.fnb.flow.tester.services.functions;

import co.za.fnb.flow.models.custom_exceptions.ServiceException;
import co.za.fnb.flow.tester.ScenarioOperator;
import co.za.fnb.flow.tester.ScenarioTester;
import co.za.fnb.flow.tester.services.ConvertPolicyTester;
import co.za.fnb.flow.tester.services.GetPolicyDetailsTester;
import co.za.fnb.flow.tester.services.models.ConvertPolicyMemberSubVariables;
import generated.*;
import generated.Error;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ConvertPolicy {
    private Logger log  = LogManager.getLogger(ConvertPolicy.class);
    private ResponseErrors responseErrors;
    private ScenarioTester scenarioTester;
    private GetPolicyDetailRequestInput getPolicyDetailRequestInput;
    private GetPolicyDetailResponseOutput getPolicyDetailResponseOutputBefore;
    private ConvertPolicyRequestInput convertPolicyRequestInput;
    private ConvertPolicyResponseOutput convertPolicyResponseOutput;
    private final String CLEAR = "CLEAR";
    private ScenarioOperator scenarioOperator;
    private String username;

    public ConvertPolicy(ScenarioTester scenarioTester) {
        this.scenarioTester = scenarioTester;
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public GetPolicyDetailResponseOutput getPolicyDetails(String policyNumber, String username) throws ServiceException {
        if (getPolicyDetailRequestInput == null) {
            getPolicyDetailRequestInput = new GetPolicyDetailRequestInput();
            getPolicyDetailRequestInput.setPolicy(policyNumber);
            getPolicyDetailRequestInput.setUsername(username);
        }
        GetPolicyDetailsTester getPolicyDetailsTester = new GetPolicyDetailsTester(getPolicyDetailRequestInput);

        StringBuilder getPolicyDetailsErrorStringBuilder = new StringBuilder();
        try {
            GetPolicyDetailResponseOutput getPolicyDetailResponseOutput = getPolicyDetailsTester.sendGetPolicyDetailsRequest();
            return getPolicyDetailResponseOutput;
        } catch (Exception e) {
            responseErrors = getPolicyDetailsTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<Error> errorList = errors.getError();
            for (Error error : errorList) {
                getPolicyDetailsErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(getPolicyDetailsErrorStringBuilder.toString());
        }
    }

    public void sendRequest() throws Exception {
        scenarioOperator = scenarioTester.getScenarioOperator();
        String policyNumber = scenarioOperator.getPolicyNumber();
        username = scenarioTester.getCellValue("Username");
        String function = scenarioOperator.getFunction();
        String transactionName = scenarioTester.getCellValue("Transaction Name");
        String newPremium = scenarioTester.getCellValue("New Premium");
        String childRoles = scenarioTester.getCellValue("Child Roles");
        String parentRoles = scenarioTester.getCellValue("Parent Roles");
        String extendedFamilyRoles = scenarioTester.getCellValue("Extended Family Roles");

        String policyHolderAction = scenarioTester.getCellValue("Policy Holder Action");
        String policyHolderFullName = scenarioTester.getCellValue("Policy Holder Full Name");
        String policyHolderMiddleName = scenarioTester.getCellValue("Policy Holder Middle Name");
        String policyHolderBirthDate = scenarioTester.getCellValue("Policy Holder Birth Date");
        String policyHolderNewCoverAmount = scenarioTester.getCellValue("Policy Holder New Cover Amount");
        String policyHolderNewPremium = scenarioTester.getCellValue("Policy Holder New Premium");
        String policyHolderComment = scenarioTester.getCellValue("Policy Holder Comment");

        String spouseAction = scenarioTester.getCellValue("Spouse Action");
        String spouseFullName = scenarioTester.getCellValue("Spouse Full Name");
        String spouseMiddleName = scenarioTester.getCellValue("Spouse Middle Name");
        String spouseBirthDate = scenarioTester.getCellValue("Spouse Birth Date");
        String spouseNewCoverAmount = scenarioTester.getCellValue("Spouse New Cover Amount");
        String spouseNewPremium = scenarioTester.getCellValue("Spouse New Premium");
        String spouseComment = scenarioTester.getCellValue("Spouse Comment");

        getPolicyDetailResponseOutputBefore = getPolicyDetails(policyNumber, username);

        if (convertPolicyRequestInput == null) {
            // TODO: Compute Convert Policy Input Request
            convertPolicyRequestInput = new ConvertPolicyRequestInput();
            convertPolicyRequestInput.setUsername(username);
            convertPolicyRequestInput.setPolicy(policyNumber);
            convertPolicyRequestInput.setTransactionname(transactionName);
            convertPolicyRequestInput.setNewpremium(newPremium);

            List<ConvertPolicyRole> newRoles = convertPolicyRequestInput.getRoles();

            if (getPolicyDetailResponseOutputBefore != null) {
                ResponsePolicy policy = getPolicyDetailResponseOutputBefore.getPolicy();

                if (policy != null) {
                    String premium = policy.getPremium();
                    convertPolicyRequestInput.setOldpremium(premium);

                    ResponseRoles policyRoles = policy.getRoles();
                    List<ResponsePolicyRoleType> roles = policyRoles.getRole();

                    computePolicyHolderRole(policyHolderAction, policyHolderFullName, policyHolderMiddleName, policyHolderBirthDate, policyHolderNewCoverAmount, policyHolderNewPremium, policyHolderComment, newRoles, roles);

                    computeSpouseRole(spouseNewCoverAmount, spouseNewPremium, spouseComment, spouseAction, spouseFullName, spouseMiddleName, spouseBirthDate, newRoles, roles);

                    List<ConvertPolicyMemberSubVariables> childRoleList = new ArrayList<>();
                    getConvertPolicyMemberSubVariablesList(childRoles, childRoleList);
                    computeConvertPolicyRole(newRoles, roles, childRoleList);

                    List<ConvertPolicyMemberSubVariables> parentRoleList = new ArrayList<>();
                    getConvertPolicyMemberSubVariablesList(parentRoles, parentRoleList);
                    computeConvertPolicyRole(newRoles, roles, parentRoleList);

                    List<ConvertPolicyMemberSubVariables> extendedFamilyMemberRoleList = new ArrayList<>();
                    getConvertPolicyMemberSubVariablesList(extendedFamilyRoles, extendedFamilyMemberRoleList);
                    computeConvertPolicyRole(newRoles, roles, extendedFamilyMemberRoleList);
                }
            }
        }

        ConvertPolicyTester convertPolicyTester = new ConvertPolicyTester(convertPolicyRequestInput);

        StringBuilder updatePolicyErrorStringBuilder = new StringBuilder();
        try {
            convertPolicyResponseOutput = convertPolicyTester.sendConvertPolicyRequest();
        } catch (Exception e) {
            responseErrors = convertPolicyTester.getResponseErrors();
            Errors errors = responseErrors.getErrors();
            List<generated.Error> errorList = errors.getError();
            for (Error error : errorList) {
                updatePolicyErrorStringBuilder.append("Code: ").append(error.getCode())
                        .append(", Subject: ").append(error.getSubject())
                        .append(", Description: ").append(error.getDescription())
                        .append(", Adjunct: ").append(error.getAdjunct())
                        .append("\n");
            }
            throw new ServiceException(updatePolicyErrorStringBuilder.toString());
        }
    }

    private void computeConvertPolicyRole(List<ConvertPolicyRole> newRoles, List<ResponsePolicyRoleType> roles, List<ConvertPolicyMemberSubVariables> childRoleList) {
        for (ConvertPolicyMemberSubVariables roleVariables: childRoleList) {
            String roleVariablesRoleId = roleVariables.getRoleId();
            for (ResponsePolicyRoleType role : roles) {
                String roleId = role.getRoleid();
                String status = role.getStatus();
                if (roleId.equalsIgnoreCase(roleVariablesRoleId)
                    && status.equalsIgnoreCase("ACTIVE")) {
                    String roleType = roleVariables.getRoleType();
                    String fullName = roleVariables.getFullName();
                    String middleName = roleVariables.getMiddleName();
                    String birthDate = roleVariables.getBirthDate();
                    String newCoverAmount = roleVariables.getNewCoverAmount();
                    String newPremiumAmount = roleVariables.getNewPremium();
                    String comment = roleVariables.getComment();

                    ConvertPolicyRole convertPolicyRole = new ConvertPolicyRole();
                    ConvertPolicyRoleFields convertPolicyRoleFields = new ConvertPolicyRoleFields();
                    convertPolicyRoleFields.setRoleid(role.getRoleid());
                    convertPolicyRoleFields.setRoletype(roleType);
                    convertPolicyRoleFields.setAction(roleVariables.getAction());

                    if (fullName.isEmpty()) {
                        convertPolicyRoleFields.setFullname(role.getFullname());
                    } else {
                        convertPolicyRoleFields.setFullname(fullName);
                    }

                    if (middleName.isEmpty()) {
                        convertPolicyRoleFields.setMiddlename(role.getMiddlename());
                    } else {
                        convertPolicyRoleFields.setMiddlename(middleName);
                    }

                    if (birthDate.isEmpty()) {
                        convertPolicyRoleFields.setBirthdate(role.getBirthdate());
                    } else {
                        convertPolicyRoleFields.setBirthdate(birthDate);
                    }

                    convertPolicyRoleFields.setNewcoveramount(newCoverAmount);
                    convertPolicyRoleFields.setOldcoveramount(role.getCoveramnt());
                    convertPolicyRoleFields.setNewpremium(newPremiumAmount);
                    convertPolicyRoleFields.setOldpremium(role.getPremium());
                    convertPolicyRoleFields.setComment(comment);
                    convertPolicyRole.setRole(convertPolicyRoleFields);
                    newRoles.add(convertPolicyRole);
                    break;
                }
            }
        }
    }

    private void getConvertPolicyMemberSubVariablesList(String roleValues, List<ConvertPolicyMemberSubVariables> roleList) {
        if (!roleValues.isEmpty()) {
            String[] splitChildRoles = roleValues.split("],");

            for (String splitChildRole: splitChildRoles) {
                if (!splitChildRole.isEmpty()) {
                    splitChildRole = splitChildRole.replaceAll("]", "").replaceAll("\\[", "");
                    String[] childRoleValues = splitChildRole.split(",");

                    ConvertPolicyMemberSubVariables convertPolicyMemberSubVariables = new ConvertPolicyMemberSubVariables();
                    convertPolicyMemberSubVariables.setRoleId(childRoleValues[0].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setRoleType(childRoleValues[1].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setAction(childRoleValues[2].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setFullName(childRoleValues[3].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setMiddleName(childRoleValues[4].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setBirthDate(childRoleValues[5].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setNewCoverAmount(childRoleValues[6].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setNewPremium(childRoleValues[7].replaceAll("\"", ""));
                    convertPolicyMemberSubVariables.setComment(childRoleValues[8].replaceAll("\"", ""));
                    roleList.add(convertPolicyMemberSubVariables);
                }
            }
        }
    }

    private void computeSpouseRole(String spouseNewCoverAmount, String spouseNewPremium, String spouseComment, String spouseAction, String spouseFullName, String spouseMiddleName, String spouseBirthDate, List<ConvertPolicyRole> newRoles, List<ResponsePolicyRoleType> roles) {
        for (ResponsePolicyRoleType role : roles) {
            String roleType = role.getRoletype();
            String status = role.getStatus();
            if (roleType.equalsIgnoreCase("SPOUSE")
                && status.equalsIgnoreCase("ACTIVE")) {
                ConvertPolicyRole convertPolicyRole = new ConvertPolicyRole();
                ConvertPolicyRoleFields convertPolicyRoleFields = new ConvertPolicyRoleFields();
                convertPolicyRoleFields.setRoleid(role.getRoleid());
                convertPolicyRoleFields.setRoletype("SPOUSE");
                convertPolicyRoleFields.setAction(spouseAction);

                if (spouseFullName.isEmpty()) {
                    convertPolicyRoleFields.setFullname(role.getFullname());
                } else {
                    convertPolicyRoleFields.setFullname(spouseFullName);
                }

                if (spouseMiddleName.isEmpty()) {
                    convertPolicyRoleFields.setMiddlename(role.getMiddlename());
                } else {
                    convertPolicyRoleFields.setMiddlename(spouseMiddleName);
                }

                if (spouseBirthDate.isEmpty()) {
                    convertPolicyRoleFields.setBirthdate(role.getBirthdate());
                } else {
                    convertPolicyRoleFields.setBirthdate(spouseBirthDate);
                }

                convertPolicyRoleFields.setNewcoveramount(spouseNewCoverAmount);
                convertPolicyRoleFields.setOldcoveramount(role.getCoveramnt());
                convertPolicyRoleFields.setNewpremium(spouseNewPremium);
                convertPolicyRoleFields.setOldpremium(role.getPremium());
                convertPolicyRoleFields.setComment(spouseComment);
                convertPolicyRole.setRole(convertPolicyRoleFields);
                newRoles.add(convertPolicyRole);
                break;
            }
        }
    }

    private void computePolicyHolderRole(String policyHolderAction, String policyHolderFullName, String policyHolderMiddleName, String policyHolderBirthDate, String policyHolderNewCoverAmount, String policyHolderNewPremium, String policyHolderComment, List<ConvertPolicyRole> newRoles, List<ResponsePolicyRoleType> roles) {
        for (ResponsePolicyRoleType role : roles) {
            String roleType = role.getRoletype();
            String status = role.getStatus();
            if (roleType.equalsIgnoreCase("POLICY HOLDER")
                && status.equalsIgnoreCase("ACTIVE")) {
                ConvertPolicyRole convertPolicyRole = new ConvertPolicyRole();
                ConvertPolicyRoleFields convertPolicyRoleFields = new ConvertPolicyRoleFields();
                convertPolicyRoleFields.setRoleid(role.getRoleid());
                convertPolicyRoleFields.setRoletype("POLICY HOLDER");
                convertPolicyRoleFields.setAction(policyHolderAction);

                if (policyHolderFullName.isEmpty()) {
                    convertPolicyRoleFields.setFullname(role.getFullname());
                } else {
                    convertPolicyRoleFields.setFullname(policyHolderFullName);
                }

                if (policyHolderMiddleName.isEmpty()) {
                    convertPolicyRoleFields.setMiddlename(role.getMiddlename());
                } else {
                    convertPolicyRoleFields.setMiddlename(policyHolderMiddleName);
                }

                if (policyHolderBirthDate.isEmpty()) {
                    convertPolicyRoleFields.setBirthdate(role.getBirthdate());
                } else {
                    convertPolicyRoleFields.setBirthdate(policyHolderBirthDate);
                }

                convertPolicyRoleFields.setNewcoveramount(policyHolderNewCoverAmount);
                convertPolicyRoleFields.setOldcoveramount(role.getCoveramnt());
                convertPolicyRoleFields.setNewpremium(policyHolderNewPremium);
                convertPolicyRoleFields.setOldpremium(role.getPremium());
                convertPolicyRoleFields.setComment(policyHolderComment);
                convertPolicyRole.setRole(convertPolicyRoleFields);
                newRoles.add(convertPolicyRole);
                break;
            }
        }
    }

    public void performValidations() throws Exception {

    }

}
