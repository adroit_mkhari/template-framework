package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class TmsHomePageFactory extends BasePage {
    // QA Distribution
    // @xpath = //*[@id="ajaxDiv"]/div[3]/div/div/p[4]/a[2]
    @FindBy(xpath = "//*[@id=\"ajaxDiv\"]/div[3]/div/div/p[4]/a[2]")
    private WebElement qADistribution;

    // Help, Work Queue and  Logout
    // @xapth = //*[@id="drpMenu"]/span
    @FindBy(xpath = "//*[@id=\"drpMenu\"]/span")
    private WebElement userMenu;

    // Help
    // @xapth = //*[@id="helpLink"]
    @FindBy(xpath = "//*[@id=\"helpLink\"]")
    private WebElement help;

    // Work Queue
    // @xapth = //*[@id="messages"]/li[2]
    @FindBy(xpath = "//*[@id=\"messages\"]/li[2]")
    private WebElement workQueue;

    // Logout
    // @xapth = //*[@id="messages"]/li[3]
    @FindBy(xpath = "//*[@id=\"messages\"]/li[3]")
    private WebElement logout;

    public TmsHomePageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getqADistribution() {
        return qADistribution;
    }

    public WebElement getUserMenu() {
        return userMenu;
    }

    public WebElement getHelp() {
        return help;
    }

    public WebElement getWorkQueue() {
        return workQueue;
    }

    public WebElement getLogout() {
        return logout;
    }
}
