package co.za.fnb.flow.pages.root_page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePageObjects {
    // region Personal Queue
    @FindBy(linkText = "Personal Queue")
    private WebElement personalQueue;

    @FindBy(linkText = "Get My Work")
    private WebElement getMyWork;

    @FindBy(linkText = "Create/Search Item")
    private WebElement createOrSearchItem;

    @FindBy(linkText = "Work Lookup")
    private WebElement workLookup;

    @FindBy(linkText = "Main Pool Lookup")
    private WebElement MainPoolLookup;

    @FindBy(linkText = "Search Customer")
    private WebElement searchCustomer;

    @FindBy(linkText = "Create Bulk Items")
    private WebElement CreateBulkItems;
    // endregion

    // region Index Work
    @FindBy(linkText = "Index Work")
    private WebElement indexWork;

    @FindBy(linkText = "Index Items")
    private WebElement indexItems;

    @FindBy(linkText = "Archived Emails")
    private WebElement archivedEmails;
    // endregion

    // region Group Funeral
    @FindBy(linkText = "Quick Quote")
    private WebElement quickQuote;

    @FindBy(linkText = "Create Group Scheme")
    private WebElement createGroupScheme;
    // endregion

    // region Administration
    @FindBy(linkText = "Administration")
    private WebElement administration;

    @FindBy(linkText = "User Admin")
    private WebElement userAdmin;

    @FindBy(linkText = "Workflow Admin")
    private WebElement workflowAdmin;

    @FindBy(linkText = "Application Admin")
    private WebElement applicationAdmin;

    @FindBy(linkText = "Legal Advisor Admin")
    private WebElement legalAdvisorAdmin;
    // endregion

    // region Dashboard
    @FindBy(linkText = "Dashboard")
    private WebElement dashboard;

    @FindBy(linkText = "Allocate Work Items")
    private WebElement allocateWorkItems;
    // endregion

    public HomePageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getPersonalQueue() {
        return personalQueue;
    }

    public WebElement getGetMyWork() {
        return getMyWork;
    }

    public WebElement getCreateOrSearchItem() {
        return createOrSearchItem;
    }

    public WebElement getWorkLookup() {
        return workLookup;
    }

    public WebElement getMainPoolLookup() {
        return MainPoolLookup;
    }

    public WebElement getSearchCustomer() {
        return searchCustomer;
    }

    public WebElement getCreateBulkItems() {
        return CreateBulkItems;
    }

    public WebElement getIndexWork() {
        return indexWork;
    }

    public WebElement getIndexItems() {
        return indexItems;
    }

    public WebElement getArchivedEmails() {
        return archivedEmails;
    }

    public WebElement getQuickQuote() {
        return quickQuote;
    }

    public WebElement getCreateGroupScheme() {
        return createGroupScheme;
    }

    public WebElement getAdministration() {
        return administration;
    }

    public WebElement getUserAdmin() {
        return userAdmin;
    }

    public WebElement getWorkflowAdmin() {
        return workflowAdmin;
    }

    public WebElement getApplicationAdmin() {
        return applicationAdmin;
    }

    public WebElement getDashboard() {
        return dashboard;
    }

    public WebElement getAllocateWorkItems() {
        return allocateWorkItems;
    }

    public WebElement getLegalAdvisorAdmin() {
        return legalAdvisorAdmin;
    }

}
