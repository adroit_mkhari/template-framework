package co.za.fnb.flow.models.work_items;

import co.za.fnb.flow.models.policy_details.PolicyDetailsBase;

public class FlowWorkItem extends WorkItemsBase {
    private String referenceNumber;
    private String workType;
    private String workItemstatus;
    private String queue;
    private String searchStatus;
    private String recordDissatisfaction;
    private String voiceLogRef;
    private String createdDate;
    private String lockedBy;
    private String assignToMe;
    private String loadExternalWorkTypes;
    private String callFromTransaction;
    private String numberOfClones;
    private String workItemQueue;
    private String comment;
    private String awaitDocDurationPerDay;
    private String awaitDocComments;
    private String awaitDocReason;
    private String awaitDocSubmit;
    private String smsViewToAddress;
    private String smsViewTemplate;

    public FlowWorkItem(String testCaseNumber, String productName, String scenarioDescription, String policyNumberCode, String policyNumber, String function, String run, String expectResult, String auditTrail, String updatedTe, String risk, String sanction, String edd, String kyc, String status, String referenceNumber, String workType, String workItemstatus, String queue, String searchStatus, String recordDissatisfaction, String voiceLogRef, String createdDate, String lockedBy, String assignToMe, String loadExternalWorkTypes, String callFromTransaction, String numberOfClones, String workItemQueue, String comment, String awaitDocDurationPerDay, String awaitDocComments, String awaitDocReason, String awaitDocSubmit, String smsViewToAddress, String smsViewTemplate) {
        super(testCaseNumber, productName, scenarioDescription, policyNumberCode, policyNumber, function, run, expectResult, auditTrail, updatedTe, risk, sanction, edd, kyc, status);
        this.referenceNumber = referenceNumber;
        this.workType = workType;
        this.workItemstatus = workItemstatus;
        this.queue = queue;
        this.searchStatus = searchStatus;
        this.recordDissatisfaction = recordDissatisfaction;
        this.voiceLogRef = voiceLogRef;
        this.createdDate = createdDate;
        this.lockedBy = lockedBy;
        this.assignToMe = assignToMe;
        this.loadExternalWorkTypes = loadExternalWorkTypes;
        this.callFromTransaction = callFromTransaction;
        this.numberOfClones = numberOfClones;
        this.workItemQueue = workItemQueue;
        this.comment = comment;
        this.awaitDocDurationPerDay = awaitDocDurationPerDay;
        this.awaitDocComments = awaitDocComments;
        this.awaitDocReason = awaitDocReason;
        this.awaitDocSubmit = awaitDocSubmit;
        this.smsViewToAddress = smsViewToAddress;
        this.smsViewTemplate = smsViewTemplate;
    }

    public String getReferenceNumber() {
        return referenceNumber;
    }

    public void setReferenceNumber(String referenceNumber) {
        this.referenceNumber = referenceNumber;
    }

    public String getWorkType() {
        return workType;
    }

    public void setWorkType(String workType) {
        this.workType = workType;
    }

    public String getWorkItemstatus() {
        return workItemstatus;
    }

    public void setWorkItemstatus(String workItemstatus) {
        this.workItemstatus = workItemstatus;
    }

    public String getQueue() {
        return queue;
    }

    public void setQueue(String queue) {
        this.queue = queue;
    }

    public String getSearchStatus() {
        return searchStatus;
    }

    public void setSearchStatus(String searchStatus) {
        this.searchStatus = searchStatus;
    }

    public String getRecordDissatisfaction() {
        return recordDissatisfaction;
    }

    public void setRecordDissatisfaction(String recordDissatisfaction) {
        this.recordDissatisfaction = recordDissatisfaction;
    }

    public String getVoiceLogRef() {
        return voiceLogRef;
    }

    public void setVoiceLogRef(String voiceLogRef) {
        this.voiceLogRef = voiceLogRef;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getLockedBy() {
        return lockedBy;
    }

    public void setLockedBy(String lockedBy) {
        this.lockedBy = lockedBy;
    }

    public String getAssignToMe() {
        return assignToMe;
    }

    public void setAssignToMe(String assignToMe) {
        this.assignToMe = assignToMe;
    }

    public String getLoadExternalWorkTypes() {
        return loadExternalWorkTypes;
    }

    public void setLoadExternalWorkTypes(String loadExternalWorkTypes) {
        this.loadExternalWorkTypes = loadExternalWorkTypes;
    }

    public String getCallFromTransaction() {
        return callFromTransaction;
    }

    public void setCallFromTransaction(String callFromTransaction) {
        this.callFromTransaction = callFromTransaction;
    }

    public String getNumberOfClones() {
        return numberOfClones;
    }

    public void setNumberOfClones(String numberOfClones) {
        this.numberOfClones = numberOfClones;
    }

    public String getWorkItemQueue() {
        return workItemQueue;
    }

    public void setWorkItemQueue(String workItemQueue) {
        this.workItemQueue = workItemQueue;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAwaitDocDurationPerDay() {
        return awaitDocDurationPerDay;
    }

    public void setAwaitDocDurationPerDay(String awaitDocDurationPerDay) {
        this.awaitDocDurationPerDay = awaitDocDurationPerDay;
    }

    public String getAwaitDocComments() {
        return awaitDocComments;
    }

    public void setAwaitDocComments(String awaitDocComments) {
        this.awaitDocComments = awaitDocComments;
    }

    public String getAwaitDocReason() {
        return awaitDocReason;
    }

    public void setAwaitDocReason(String awaitDocReason) {
        this.awaitDocReason = awaitDocReason;
    }

    public String getAwaitDocSubmit() {
        return awaitDocSubmit;
    }

    public void setAwaitDocSubmit(String awaitDocSubmit) {
        this.awaitDocSubmit = awaitDocSubmit;
    }

    public String getSmsViewToAddress() {
        return smsViewToAddress;
    }

    public void setSmsViewToAddress(String smsViewToAddress) {
        this.smsViewToAddress = smsViewToAddress;
    }

    public String getSmsViewTemplate() {
        return smsViewTemplate;
    }

    public void setSmsViewTemplate(String smsViewTemplate) {
        this.smsViewTemplate = smsViewTemplate;
    }

    @Override
    public String toString() {
        return "FlowWorkItem{" +
                "referenceNumber='" + referenceNumber + '\'' +
                ", workType='" + workType + '\'' +
                ", workItemstatus='" + workItemstatus + '\'' +
                ", queue='" + queue + '\'' +
                ", searchStatus='" + searchStatus + '\'' +
                ", recordDissatisfaction='" + recordDissatisfaction + '\'' +
                ", voiceLogRef='" + voiceLogRef + '\'' +
                ", createdDate='" + createdDate + '\'' +
                ", lockedBy='" + lockedBy + '\'' +
                ", assignToMe='" + assignToMe + '\'' +
                ", loadExternalWorkTypes='" + loadExternalWorkTypes + '\'' +
                ", callFromTransaction='" + callFromTransaction + '\'' +
                ", numberOfClones='" + numberOfClones + '\'' +
                ", workItemQueue='" + workItemQueue + '\'' +
                ", comment='" + comment + '\'' +
                ", awaitDocDurationPerDay='" + awaitDocDurationPerDay + '\'' +
                ", awaitDocComments='" + awaitDocComments + '\'' +
                ", awaitDocReason='" + awaitDocReason + '\'' +
                ", awaitDocSubmit='" + awaitDocSubmit + '\'' +
                ", smsViewToAddress='" + smsViewToAddress + '\'' +
                ", smsViewTemplate='" + smsViewTemplate + '\'' +
                '}';
    }

}
