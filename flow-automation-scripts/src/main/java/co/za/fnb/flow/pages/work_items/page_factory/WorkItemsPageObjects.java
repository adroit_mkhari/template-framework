package co.za.fnb.flow.pages.work_items.page_factory;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class WorkItemsPageObjects {
    @FindBy(id = "createSearchItemView:mainTabView:workItemtable_data")
    private WebElement workItemTable;

    private String workItemTableWorkTypeLocator = "createSearchItemView:mainTabView:workItemtable:0:workItemTableWorkType";
    private WebElement workItemTableWorkType;

    private String workItemTableStatusLocator = "createSearchItemView:mainTabView:workItemtable:0:workItemTableStatus";
    private WebElement workItemTableStatus;

    private String workItemTableQueueLocator = "createSearchItemView:mainTabView:workItemtable:0:workItemTableQueue";
    private WebElement workItemTableQueue;

    private String workItemTableCreatedDateLocator = "createSearchItemView:mainTabView:workItemtable:0:workItemTableCreatedDate";
    private WebElement workItemTableCreatedDate;

    @FindBy(id = "createSearchItemView:mainTabView:commentstable:CommentsSummary_dlg")
    private WebElement commentsSummaryDialog;

    @FindBy(id = "createSearchItemView:mainTabView:commentstable:CommentsSummary")
    private WebElement commentsSummary;

    @FindBy(id = "commentsSummaryView:commentsTextArea")
    private WebElement commentsTextArea;

    @FindBy(xpath = "//*[contains(@id, 'createSearchItemView:mainTabView:commentstable:CommentsSummary_dlg')]//div//a//span[contains(@class, 'ui-icon ui-icon-closethick')]")
    private WebElement commentsSummaryDialogClose;

    // @id = createSearchItemView:mainTabView:itemTabView:itemDocumentsTable_data
    @FindBy(id = "createSearchItemView:mainTabView:itemTabView:itemDocumentsTable_data")
    private WebElement itemDocumentsTableData;

    // TODO: Find Construct The Xpaths Using These As Templates
    // region Item Documents Table Data (First Record)
    // @xpath = //*[@id="createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsUploadedDate"]
    @FindBy(xpath = "//*[@id=\"createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsUploadedDate\"]")
    private WebElement firstItemDocumentsTableDataWorkItemDocumentsUploadedDate;

    // @xpath = //*[@id="createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsDocumentName"]
    @FindBy(xpath = "//*[@id=\"createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsDocumentName\"]")
    private WebElement firstItemDocumentsTableDataWorkItemDocumentName;

    // @xpath = //*[@id="createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsDocumentStatus"]
    @FindBy(xpath = "//*[@id=\"createSearchItemView:mainTabView:itemTabView:itemDocumentsTable:0:owrkItemDocumentsDocumentStatus\"]")
    private WebElement firstItemDocumentsTableDataWorkItemDocumentStatus;
    // endregion

    @FindBy(id = "createSearchItemView:referenceNumber")
    private WebElement referenceNumber;

    public WorkItemsPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getWorkItemTable() {
        return workItemTable;
    }

    public String getWorkItemTableWorkTypeLocator() {
        return workItemTableWorkTypeLocator;
    }

    public WebElement getWorkItemTableWorkType() {
        return workItemTableWorkType;
    }

    public String getWorkItemTableStatusLocator() {
        return workItemTableStatusLocator;
    }

    public WebElement getWorkItemTableStatus() {
        return workItemTableStatus;
    }

    public String getWorkItemTableQueueLocator() {
        return workItemTableQueueLocator;
    }

    public WebElement getWorkItemTableQueue() {
        return workItemTableQueue;
    }

    public String getWorkItemTableCreatedDateLocator() {
        return workItemTableCreatedDateLocator;
    }

    public WebElement getWorkItemTableCreatedDate() {
        return workItemTableCreatedDate;
    }

    public WebElement getCommentsSummaryDialog() {
        return commentsSummaryDialog;
    }

    public WebElement getCommentsSummary() {
        return commentsSummary;
    }

    public WebElement getCommentsTextArea() {
        return commentsTextArea;
    }

    public WebElement getCommentsSummaryDialogClose() {
        return commentsSummaryDialogClose;
    }

    public WebElement getItemDocumentsTableData() {
        return itemDocumentsTableData;
    }

    public WebElement getFirstItemDocumentsTableDataWorkItemDocumentsUploadedDate() {
        return firstItemDocumentsTableDataWorkItemDocumentsUploadedDate;
    }

    public WebElement getFirstItemDocumentsTableDataWorkItemDocumentName() {
        return firstItemDocumentsTableDataWorkItemDocumentName;
    }

    public WebElement getFirstItemDocumentsTableDataWorkItemDocumentStatus() {
        return firstItemDocumentsTableDataWorkItemDocumentStatus;
    }

    public WebElement getReferenceNumber() {
        return referenceNumber;
    }
}
