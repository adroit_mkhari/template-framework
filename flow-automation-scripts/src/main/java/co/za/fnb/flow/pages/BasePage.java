package co.za.fnb.flow.pages;

import co.za.fnb.flow.handlers.Scenario;
import co.za.fnb.flow.models.custom_exceptions.NegativeTestException;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.openqa.selenium.support.ui.ExpectedConditions.*;
import static co.za.fnb.flow.pages.MessagesContainer.responseTrace;

/**
 * This is the entry page that contains most of the common functionality that some of the other co.za.fnb.flow.pages may perform.
 */
public class BasePage implements Config {
    private Logger log  = LogManager.getLogger(BasePage.class);
    protected WebDriver driver;
    MessagesContainer messagesContainer;

    public ErrorHandle errorHandle;
    {
        new ErrorHandle();
    }

    public TestHandle testHandle = new TestHandle();
    public ScenarioOperator scenarioOperator;
    Actions actions;

    public BasePage(WebDriver driver, ScenarioOperator scenarioOperator) {
        this.driver = driver;
        this.scenarioOperator = scenarioOperator;
        actions = new Actions(driver);
        errorHandle = new ErrorHandle();
        messagesContainer = new MessagesContainer(driver, errorHandle);
    }

    public String getLocatorForTableRowWithTrTd(String templateLocator, int row, int column) throws Exception {
        try {
            log.info("Template Locator String: " + templateLocator);
            String locatorString = templateLocator.replaceAll("tr\\[1\\]", "tr[\"" + row + "\"]").replaceAll("td\\[1\\]", "td[" + column + "]");
            log.info("Locator String: " + locatorString);
            return locatorString;
        } catch (Exception e) {
            log.error("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
            throw new Exception("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
        }
    }

    public String getLocatorForTableRowWithTrTdNoCommas(String templateLocator, int row, int column) throws Exception {
        try {
            log.info("Template Locator String: " + templateLocator);
            String locatorString = templateLocator.replaceAll("tr\\[1\\]", "tr[" + row + "]").replaceAll("td\\[1\\]", "td[" + column + "]");
            log.info("Locator String: " + locatorString);
            return locatorString;
        } catch (Exception e) {
            log.error("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
            throw new Exception("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
        }
    }

    public String getLocatorForTableRowWithDataRiTd(String templateLocator, int row, int column) throws Exception {
        try {
            log.info("Template Locator String: " + templateLocator);
            String locatorString = templateLocator.replaceAll("tr\\[@data\\-ri=\"0\"\\]", "tr[@data-ri=\"" + row + "\"]").replaceAll("td\\[1\\]", "td[" + column + "]");
            log.info("Locator String: " + locatorString);
            return locatorString;
        } catch (Exception e) {
            log.error("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
            throw new Exception("Error while computing locator for row " + row + " column " + column + " on " + "\"" + templateLocator + "\"");
        }
    }

    public String getLocatorForIndex(String templateLocator, int index) throws Exception {
        try {
            log.info("Template Locator String: " + templateLocator);
            String locatorString = templateLocator.replaceAll(":0:", ":" + index + ":");
            log.info("Locator String: " + locatorString);
            return locatorString;
        } catch (Exception e) {
            log.error("Error while computing locator for index " + index + " on " + "\"" + templateLocator + "\"");
            throw new Exception("Error while computing locator for index " + index + " on " + "\"" + templateLocator + "\"");
        }
    }

    public String getLocatorForIndex(String templateLocator, String targetTextRegex, String matchPrefix, String matchPostfix, int index) throws Exception {
        try {
            log.info("Template Locator String: " + templateLocator);
            String locatorString = templateLocator.replaceAll(targetTextRegex, matchPrefix + index + matchPostfix);
            log.info("Locator String: " + locatorString);
            return locatorString;
        } catch (Exception e) {
            log.error("Error while computing locator for index " + index + " on " + "\"" + templateLocator + "\"");
            throw new Exception("Error while computing locator for index " + index + " on " + "\"" + templateLocator + "\"");
        }
    }

    public void setExpectedResults(String expectedResults) {
        log.info("Setting expected results.");
        messagesContainer.setExpectedResult(expectedResults);
    }

    public void getResponses(boolean checkExpectedResponse) throws Exception {
        try {
            log.info("Getting Responses. Expected Response: " + checkExpectedResponse);
            messagesContainer.getResponseMessages(checkExpectedResponse);
        } catch (Exception e) {
            // TODO: Work on screenshots.
            takeScreenShot(reportSourcePath + "\\screenshots\\" + scenarioOperator.getScreenShotFileName() + " " + getDateTimeStamp());
            log.error(e.getMessage());
            errorHandle = messagesContainer.getErrorHandle();
            getTestResults();
            if (!testHandle.isSuccess()) {
                throw new Exception(errorHandle.getError());
            } else {
                throw new NegativeTestException(errorHandle.getError());
            }
        }
    }

    public void getTestResults() throws Exception {
        // TODO: Revisit This Logic
        log.info("Getting Test Results");
        try {
            String responses = responseTrace.toString();
            String screenShotLocation = ""; // utility.takeScreenshot(commonSeleniumTester.getDriver(), testCaseNo + scenarioDescription);

            // TODO: Find a way to set the expected results value on the message container.
            String expectedResult = messagesContainer.getExpectedResult();
            String actualFailureResult = errorHandle.getError();
            boolean isExpectedFailure = false;

            ArrayList<String> resultOfExpected = new ArrayList<String>();
            if (actualFailureResult == null) {
                testHandle.setSuccess(true);
            }

            if (!responses.isEmpty()) {
                if (!expectedResult.isEmpty()) {
                    String[] expectedMessageBoxList;
                    expectedMessageBoxList = expectedResult.split(",");
                    if (expectedMessageBoxList.length == 0) {
                        expectedMessageBoxList = new String[1];
                        expectedMessageBoxList[0] = expectedResult;
                    }

                    for (String expectedMessageBox: expectedMessageBoxList) {
                        if (!expectedMessageBox.isEmpty()) {
                            String[] expectedMessages;
                            expectedMessages = expectedMessageBox.split("\n");

                            if (expectedMessages.length == 0) {
                                expectedMessages = new String[1];
                                expectedMessages[0] = expectedMessageBox;
                            }
                            for (String message : expectedMessages) {
                                if (!responses.toLowerCase().contains(message.toLowerCase())) {
                                    log.info("Expected Result not found: " + message + "\n");
                                    resultOfExpected.add(message);
                                }
                                // Check if one of the expected result messages is the failure reason:
                                if (actualFailureResult != null && actualFailureResult.toLowerCase().contains(message.toLowerCase())) {
                                    isExpectedFailure = true;
                                }
                            }
                        }
                    }
                }
            }

            if (!resultOfExpected.isEmpty()) {
                log.info("Some expected results were not found.");
                StringBuilder listNotFound = new StringBuilder();
                for (String notFound : resultOfExpected) {
                    listNotFound.append(notFound).append("\n");
                }
                testHandle.setSuccess(false);
                if (actualFailureResult == null) {
                    errorHandle.setError("The Following Expected Results were not found: \n\n" + listNotFound);
                    log.debug(errorHandle.getError());
                } else {
                    errorHandle.setError(actualFailureResult + "\n\nThe Following Expected Results were not found: \n\n" + listNotFound);
                    log.debug(errorHandle.getError());
                }
            }

            // If the failure was expected we want to pass the anyway:
            if (isExpectedFailure) {
                testHandle.setSuccess(true);
            }

            log.info("Test Result Passed ? : " + testHandle.isSuccess());
            // reportHandler.setGeneralComment(responses);
        } catch (Exception exception) {
            log.error(exception.getMessage());
            throw new Exception("Error while Processing results");
        }
    }

    public ErrorHandle getErrorHandle() {
        log.info("Getting Error Handle.");
        return errorHandle;
    }

    public void setErrorHandle(ErrorHandle errorHandle) {
        this.errorHandle = errorHandle;
    }

    public TestHandle getTestHandle() {
        log.info("Getting Test Handle.");
        return testHandle;
    }

    public void setTestHandle(TestHandle testHandle) {
        this.testHandle = testHandle;
    }

    public void visit(String url) throws Exception {
        log.info("Opening Target Application on: " + url);
        try {
            driver.get(url);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getPageTitle() throws Exception {
        try {
            return driver.getTitle();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getUrl() throws Exception {
        try {
            return driver.getCurrentUrl();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public ScenarioOperator getScenarioOperator() {
        return scenarioOperator;
    }

    public void setScenarioOperator(ScenarioOperator scenarioOperator) {
        this.scenarioOperator = scenarioOperator;
    }

    public WebElement find(By locator) throws Exception {
        try {
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(visibilityOfElementLocated(locator));
            return driver.findElement(locator);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public WebElement findXPath(String locatorXpath) throws Exception {
        try {
            By locator = By.xpath(locatorXpath);
            WebDriverWait wait = new WebDriverWait(driver, 15);
            wait.until(visibilityOfElementLocated(locator));
            WebElement element = driver.findElement(locator);
            return element;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void switchToFrame(int index) throws Exception {
        try {
            log.info("Switching to frame with index: " + index);
            driver.switchTo().frame(index);
        } catch (Exception e) {
            log.error("Error while switching to frame index: " + index);
            throw new Exception("Error while switching to frame index: " + index);
        }
    }

    public void switchToDefaultContent() throws Exception {
        try {
            log.info("Switching to default content.");
            driver.switchTo().defaultContent();
        } catch (Exception e) {
            log.error("Error while switching to default content.");
            throw new Exception("Error while switching to default content.");
        }
    }

    public void switchToActiveElement() throws Exception {
        try {
            log.info("Switching to active element.");
            driver.switchTo().activeElement();
        } catch (Exception e) {
            log.error("Error while switching to active element.");
            throw new Exception("Error while switching to active element.");
        }
    }

    public void click(By locator) throws Exception {
        try {
            log.info("Find and Clinking element.");
            find(locator).click();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void scrollToElement(WebElement webElement) throws Exception {
        try {
            log.info("Scrolling to element.");
            ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", webElement);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void clickElementJavaScriptExecutor(WebElement webElement) throws Exception {
        try {
            log.info("Clicking element.");
            ((JavascriptExecutor) driver).executeScript("arguments[0].click;", webElement);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void click(WebElement webElement) throws Exception {
        try {
            log.info("Clicking element.");
            webElement.click();
        } catch (Exception e) {
            takeScreenShot("");
            log.error("Error On Clicking element: " + e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public Actions moveToElement(WebElement webElement) throws Exception {
        try {
            log.info("Moving to element.");
            return actions.moveToElement(webElement);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void moveToElementAndClick(WebElement webElement) throws Exception {
        try {
            log.info("Moving to element and click.");
            actions.moveToElement(webElement).click().build().perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void moveToElementAndClickJsExec(WebElement webElement) throws Exception {
        try {
            log.info("Click using JS Executor.");
            JavascriptExecutor executor = (JavascriptExecutor)driver;
            executor.executeScript("arguments[0].click();", webElement);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void moveToElementAndDoubleClick(WebElement webElement) throws Exception {
        try {
            log.info("Moving to element and double click.");
            actions.moveToElement(webElement).doubleClick().build().perform();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void clear(By locator) throws Exception {
        try {
            log.info("Finding and clearing element.");
            find(locator).clear();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void clear(WebElement webElement) throws Exception {
        // TODO: Figure out what kind of validation we can do.
        try {
            log.info("Clearing web element.");
            webElement.clear();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void type(By locator, String inputText) throws Exception {
        try {
            log.info("Sending Keys (typing/updating).");
            find(locator).sendKeys(inputText);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void type(WebElement webElement, String inputText) throws Exception {
        // TODO: Figure out what kind of validation we can do.
        try {
            log.info("Sending Keys (typing/updating).");
            webElement.sendKeys(inputText);
        } catch (Exception e) {
            log.error("Error while sending keys: " + e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void submit(By locator) throws Exception {
        try {
            log.info("Finding element and Submitting.");
            find(locator).submit();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void submit(WebElement webElement) throws Exception {
        // TODO: Figure out what kind of validation we can do.
        try {
            log.info("Submitting web element.");
            webElement.submit();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getText(By locator) throws Exception {
        try {
            log.info("Finding element and getting text.");
            return find(locator).getText();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getText(WebElement webElement) throws Exception {
        // TODO: Figure out what kind of validation we can do.
        try {
            log.info("Getting element text.");
            return webElement.getText();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getAttribute(By locator, String attribute) throws Exception {
        try {
            log.info("Finding element and getting attribute.");
            return find(locator).getAttribute(attribute);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public String getAttribute(WebElement webElement, String attribute) throws Exception {
        // TODO: Figure out what kind of validation we can do.
        try {
            log.info("Getting element attribute(" + attribute + ").");
            return webElement.getAttribute(attribute);
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    private int getRandomNumber(int max, int min) {
        int range = max - min + 1;

        return (int)(Math.random() * range) + min;
    }

    public void takeScreenShot(String fileName) throws Exception {
        log.info("Taking Screen Shot.");
        byte[] screenshotBytes = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
        try {
            try {
                Scenario scenario = scenarioOperator.getScenario();
                scenario.addScreenshot(screenshotBytes);
            } catch (Exception e) {
                e.printStackTrace();
                log.error("Error While Attaching Screenshot To Excel File: " + e.getMessage());
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectUsingTableRowData(WebElement tableDataItems, int row) throws Exception {
        try {
            log.info("select Using Table Row Data.");
            tableDataItems.findElement(By.xpath("//tr[contains(@data-ri, '" + row + "')]//td")).click();
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public int getTableDataRiSize(WebElement tableDataItems) throws Exception {
        try {
            log.info("Getting Data Table Size.");
            Thread.sleep(2000);
            int size = tableDataItems.findElements(By.xpath("tr[contains(@data-ri, '')]")).size();
            log.info("Table Size: " + size);
            return size;
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public int getElementsList(String locator) throws Exception {
        try {
            log.info("Getting Elements List.");
            Thread.sleep(2000);
            int size = driver.findElements(By.xpath(locator)).size();
            log.info("Number Of Elements: " + size);
            return size;
        } catch (Exception e) {
            log.error("Error while getting List Of Elements: " + e.getMessage());
            throw new Exception("Error while getting List Of Elements: " + e.getMessage());
        }
    }

    public void selectUsingDataLabel(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Data Label.");
                click(dropdown);
                dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(0).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectUsingDataLabel(WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Data Label.");
                dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(0).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectMatchingDataLabel(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Matching Data Label.");
                click(dropdown);
                Thread.sleep(5000);
                dropdownItems.findElement(By.xpath("//li[contains(@data-label, '" + selection + "')]")).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectMatchingDataLabel(WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Matching Data Label.");
                dropdownItems.findElement(By.xpath("//li[contains(@data-label, '" + selection + "')]")).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectExactMatchingDataLabel(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Exact Matching Data Label.");
                click(dropdown);
                Thread.sleep(5000);
                dropdownItems.findElement(By.xpath("//li[@data-label= '" + selection + "']")).click();
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectExactMatchingDataLabelWithFilter(WebElement dropdown, WebElement dropdownItems, String selection, String filterId) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Exact Matching Data Label.");
                click(dropdown);
                Thread.sleep(3000);
                By selectionElementBy = By.xpath("//li[@data-label= '" + selection + "']");
                WebElement selectionElement = dropdownItems.findElement(selectionElementBy);

                try {
                    selectionElement.click();
                } catch (Exception clickException) {
                    if (clickException instanceof ElementNotVisibleException) {
                        log.error("Error while clicking list element: ElementNotVisibleException");
                        try {
                            WebDriverWait wait = new WebDriverWait(driver, 15);
                            wait.until(visibilityOfElementLocated(selectionElementBy));
                            selectionElement.click();
                        } catch (Exception e) {
                            log.debug("Error while waiting for element.");
                            try {
                                log.info("Looking for list filter element.");
                                WebElement element = driver.findElement(By.id(filterId));
                                element.sendKeys(selection);
                                element.sendKeys(Keys.ENTER);
                            } catch (Exception exc) {
                                log.error("Error while filtering elements: " + exc.getMessage());
                                throw new Exception("Error while filtering elements: " + exc.getMessage());
                            }
                        }
                    }
                }
                Thread.sleep(2000);
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectExactMatchingDataLabel(WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Exact Matching Data Label.");
                dropdownItems.findElement(By.xpath("//li[@data-label= '" + selection + "']")).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectUsingValue(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Using Value.");
                dropdown.click();
                dropdownItems.findElements(By.xpath("//option[contains(@value, '" + selection + "')]")).get(0).click();
            }
        } catch (Exception e) {
            log.error(e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectOnDashboard(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting On Dashboard.");
                click(dropdown);
                int numberOfItems = dropdownItems.findElements(By.xpath("//option[contains(@value, '" + selection + "')]")).size();

                for (int i = 0; i < numberOfItems; i++) {
                    if (dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(i).getText().equals(selection)){
                        dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]")).get(i).click();
                    }
                }
            }
        } catch (Exception e) {
            log.error("Error On Selecting On Dashboard: " + e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    public void selectRelationshipOnFlow(WebElement dropdown, WebElement dropdownItems, String selection) throws Exception {
        try {
            if (!selection.isEmpty()) {
                log.info("Selecting Relationship On Dashboard: " + selection);
                click(dropdown);
                Thread.sleep(1000);
                List<WebElement> elements = dropdownItems.findElements(By.xpath("//li[contains(@data-label, '" + selection + "')]"));
                WebElement relationshipOption = null;
                for (WebElement element: elements) {
                    String relationship = element.getText();
                    log.info("Relationship Element Text: " + relationship);
                    if (relationship.equals(selection)) {
                        relationshipOption = element;
                        break;
                    }
                }

                if (relationshipOption != null) {
                    relationshipOption.click();
                } else {
                    throw new Exception("No Matching Relationship.");
                }
            }
        } catch (Exception e) {
            log.error("Error On Selecting On Dashboard: " + e.getMessage());
            e.printStackTrace();
            throw new Exception(e.getMessage());
        }
    }

    private String getDateTimeStamp() {
        log.info("Getting Date Time Stamp.");
        DateTimeFormatter format = DateTimeFormatter.ofPattern("dd_MM_yyyy_HH_mm_ss");
        LocalDateTime now = LocalDateTime.now();
        return format.format(now);
    }

    public void switchToAnfMaximizeServiceWindow(int windowIndex) {
        log.info("Switch To And Maximize Service Window.");
        actions = new Actions(driver);
        Object currentWindow = driver.getWindowHandles().toArray()[windowIndex];
        driver.switchTo().window((String) currentWindow);
        driver.manage().window().maximize();
    }

    public void switchToAndCloseServiceWindow(int windowIndex) {
        log.info("Switch To And Close Service Window.");
        actions = new Actions(driver);
        Object currentWindow = driver.getWindowHandles().toArray()[windowIndex];
        WebDriver window = driver.switchTo().window((String) currentWindow);
        window.close();
    }

    public void refreshPage(int windowIndex) throws InterruptedException {
        log.info("Refreshing Page.");
        Object currentWindow = driver.getWindowHandles().toArray()[windowIndex];
        driver.switchTo().window((String) currentWindow);
        driver.navigate().refresh();
        Thread.sleep(2000);
        try {
            log.debug("Trying To Accept Alert.");
            driver.switchTo().alert().accept();
        } catch (Exception e) {
            // UnhandledAlertException unhandledAlertException
            log.debug("Error While Trying To Accept Alert: \n" + e.getMessage());
        }
        Thread.sleep(1000);
    }
}
