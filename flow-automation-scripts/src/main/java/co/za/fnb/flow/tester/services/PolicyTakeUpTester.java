package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.ObjectFactory;
import generated.PolicyTakeUpRequestInput;
import generated.PolicyTakeUpResponse;
import generated.ResponseErrors;
import iseries.wsbeans.gsddsync00.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class PolicyTakeUpTester {
    private Logger log  = LogManager.getLogger(GetPolicyDetailsTester.class);
    private PolicyTakeUpRequestInput policyTakeUpRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private PolicyTakeUpResponse policyTakeUpResponse;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public PolicyTakeUpTester(PolicyTakeUpRequestInput policyTakeUpRequestInput) {
        this.policyTakeUpRequestInput = policyTakeUpRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(PolicyTakeUpRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(policyTakeUpRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(PolicyTakeUpResponse.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public PolicyTakeUpRequestInput getPolicyTakeUpRequestInput() {
        return policyTakeUpRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public PolicyTakeUpResponse getPolicyTakeUpResponse() {
        return policyTakeUpResponse;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public PolicyTakeUpResponse sendPolicyTakeUpRequest(String requestType) throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSDDSYNC00PRE gsddsync00PRE = new GSDDSYNC00PRE();
        GSDDSYNC00TST gsddsync00TST = new GSDDSYNC00TST();
        GSDDSYNC00Services gsddsync00ServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsddsync00ServicesPort = gsddsync00PRE.getGSDDSYNC00ServicesPort();
            } else {
                gsddsync00ServicesPort = gsddsync00TST.getGSDDSYNC00ServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsddsync00.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsddsync00.ObjectFactory();
        Gsddsync00Input gsddsync00Input = serviceObjectFactory.createGsddsync00Input();

        String takeUpRequestInput = marshalResult;
        String takeUpCustomer = takeUpRequestInput.replaceAll("<\\?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"\\?>", "")
                .replaceAll("<input xsi:type=\"PolicyTakeUpRequestInput\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">", "")
                .replaceAll("</input>", "")
                .trim();
        log.info("REQUEST: \n" + takeUpCustomer);
        gsddsync00Input.setPREQUEST(takeUpCustomer);
        gsddsync00Input.setPTYPE(requestType);

        Gsddsync00Result gsddsync00Result = gsddsync00ServicesPort.gsddsync00(gsddsync00Input);
        String presponse = gsddsync00Result.getPRESPONSE();
        presponse = presponse.replaceAll("RESPONSE", "response");

        if (presponse.contains("<response>")) {

            String formattedStringOutput = presponse.replaceAll("<\\?xml version=\"1.0\"\\?>", "")
                                                    .replaceAll("<\\?XML VERSION=\"1.0\"\\?>", "")
                                                    .replace("<output>", "")
                                                    .replace("</output>", "")
                                                    .replace("<OUTPUT>", "")
                                                    .replace("</OUTPUT>", "")
                                                    .replaceAll("COMMONDATA", "commonData")
                                                    .replaceAll("POLICYDATA", "policyData")
                                                    .replaceAll("RESULTMSG", "resultmsg")
                                                    .replaceAll("ERRORCODE", "errorcode")
                                                    .replaceAll("POLICYNO", "policyNo")
                                                    .replace("<ERRORS>", "<errors>")
                                                    .replace("</ERRORS>", "</errors>")
                                                    .replace("<ERROR>", "<error>")
                                                    .replace("</ERROR>", "</error>")
                                                    .replace("<CODE>", "<code>")
                                                    .replace("</CODE>", "</code>")
                                                    .replace("<SUBJECT>", "<subject>")
                                                    .replace("</SUBJECT>", "</subject>")
                                                    .replace("<ADJUNCT>", "<adjunct>")
                                                    .replace("</ADJUNCT>", "</adjunct>")
                                                    .replace("<DESCRIPTION>", "<description>")
                                                    .replace("</DESCRIPTION>", "</description>")
                                                    .trim();

            unMarshallerJaxbHelper.formatOutputXml(formattedStringOutput, PolicyTakeUpResponse.class);
            Object unMarshal = unMarshallerJaxbHelper.unMarshal();
            if (!(unMarshal instanceof ResponseErrors)) {
                policyTakeUpResponse = (PolicyTakeUpResponse) unMarshal;
                return policyTakeUpResponse;
            } else {
                responseErrors = (ResponseErrors) unMarshal;
                throw new Exception("Error while getting PolicyTakeUpResponse: " + ((ResponseErrors) unMarshal).getErrors());
            }
        } else {
            throw new Exception("No Response Payload: " + presponse);
        }
    }
}
