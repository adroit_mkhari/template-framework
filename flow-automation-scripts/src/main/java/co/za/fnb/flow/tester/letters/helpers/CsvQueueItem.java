package co.za.fnb.flow.tester.letters.helpers;

public class CsvQueueItem {
    private String id;
    private String fileName;
    private String blobfld;
    private String type;
    private String procFlag;
    private String procDate;
    private String userCapt;
    private String timeCapt;
    private String chgStamp;

    public CsvQueueItem(String id, String fileName, String blobfld, String type, String procFlag, String procDate, String userCapt, String timeCapt, String chgStamp) {
        this.id = id;
        this.fileName = fileName;
        this.blobfld = blobfld;
        this.type = type;
        this.procFlag = procFlag;
        this.procDate = procDate;
        this.userCapt = userCapt;
        this.timeCapt = timeCapt;
        this.chgStamp = chgStamp;
    }

    public String getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public String getBlobfld() {
        return blobfld;
    }

    public String getType() {
        return type;
    }

    public String getProcFlag() {
        return procFlag;
    }

    public String getProcDate() {
        return procDate;
    }

    public String getUserCapt() {
        return userCapt;
    }

    public String getTimeCapt() {
        return timeCapt;
    }

    public String getChgStamp() {
        return chgStamp;
    }

}
