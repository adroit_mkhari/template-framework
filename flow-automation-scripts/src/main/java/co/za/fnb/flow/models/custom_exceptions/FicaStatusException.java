package co.za.fnb.flow.models.custom_exceptions;

public class FicaStatusException extends Exception {
    public FicaStatusException(String message) {
        super(message);
    }
}
