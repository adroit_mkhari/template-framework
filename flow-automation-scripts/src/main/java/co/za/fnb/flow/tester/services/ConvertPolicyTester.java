package co.za.fnb.flow.tester.services;

import co.za.fnb.flow.handlers.JAXBHelper;
import co.za.fnb.flow.setup.PropertiesSetup;
import generated.*;
import generated.ObjectFactory;
import iseries.wsbeans.gsd00030pr.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import java.util.Properties;

public class ConvertPolicyTester {
    private Logger log  = LogManager.getLogger(ConvertPolicyTester.class);
    private ConvertPolicyRequestInput convertPolicyRequestInput;
    private JAXBHelper marshallerJaxbHelper = new JAXBHelper();
    private JAXBHelper unMarshallerJaxbHelper = new JAXBHelper();
    private JAXBElement<Object> input;
    private ConvertPolicyResponseOutput convertPolicyResponseOutput;
    private ResponseErrors responseErrors;
    private Properties properties = null;
    private String environment = "";

    public ConvertPolicyTester(ConvertPolicyRequestInput convertPolicyRequestInput) {
        this.convertPolicyRequestInput = convertPolicyRequestInput;
        try {
            // Setup The Marshalling Context
            marshallerJaxbHelper.setCustomJaxbContext(ConvertPolicyRequestInput.class);
            ObjectFactory objectFactory = new ObjectFactory();
            input = objectFactory.createInput(convertPolicyRequestInput);
            marshallerJaxbHelper.setDataObject(input);

            // Setup The Expected Output UnMarshalling Context
            unMarshallerJaxbHelper.setCustomJaxbContext(ConvertPolicyResponseOutput.class);

            log.info("Loading Properties");
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            environment = properties.getProperty("ENVIROMENT");
            log.info("Done Loading Properties");
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public ConvertPolicyRequestInput getConvertPolicyRequestInput() {
        return convertPolicyRequestInput;
    }

    public JAXBHelper getMarshallerJaxbHelper() {
        return marshallerJaxbHelper;
    }

    public JAXBHelper getUnMarshallerJaxbHelper() {
        return unMarshallerJaxbHelper;
    }

    public JAXBElement<Object> getInput() {
        return input;
    }

    public ConvertPolicyResponseOutput getConvertPolicyResponseOutput() {
        return convertPolicyResponseOutput;
    }

    public ResponseErrors getResponseErrors() {
        return responseErrors;
    }

    public ConvertPolicyResponseOutput sendConvertPolicyRequest() throws Exception {
        String marshalResult = marshallerJaxbHelper.marshal(false);

        GSD00030PRPre gsd00030PRPre = new GSD00030PRPre();
        GSD00030PRTst gsd00030PRTst = new GSD00030PRTst();
        GSD00030PRServices gsd00030PRServicesPort = null;

        if (!environment.isEmpty()) {
            if (environment.equalsIgnoreCase("pre")) {
                gsd00030PRServicesPort = gsd00030PRPre.getGSD00030PRServicesPort();
            } else {
                gsd00030PRServicesPort = gsd00030PRTst.getGSD00030PRServicesPort();
            }
        } else {
            log.error("Error: No Target Environment Specified.");
            throw new Exception("Error: No Target Environment Specified.");
        }

        iseries.wsbeans.gsd00030pr.ObjectFactory serviceObjectFactory = new iseries.wsbeans.gsd00030pr.ObjectFactory();
        GensrvCONVERTPOLICYInput convertPolicyInput = serviceObjectFactory.createGensrvCONVERTPOLICYInput();
        PXMLIN pxmlin = serviceObjectFactory.createPXMLIN();
        pxmlin.setLength(10000);
        pxmlin.setString(marshalResult);
        convertPolicyInput.setPXMLIN(pxmlin);
        GensrvCONVERTPOLICYResult gensrvCONVERTPOLICYResult = gsd00030PRServicesPort.gensrvConvertpolicy(convertPolicyInput);
        PXMLOUT convertPolicyResponseOutput = gensrvCONVERTPOLICYResult.getPXMLOUT();
        String pXmlOutString = convertPolicyResponseOutput.getString();

        unMarshallerJaxbHelper.formatOutputXml(pXmlOutString, ConvertPolicyResponseOutput.class);
        Object unMarshal = unMarshallerJaxbHelper.unMarshal();
         if (!(unMarshal instanceof ResponseErrors)) {
             this.convertPolicyResponseOutput = (ConvertPolicyResponseOutput) unMarshal;
             return this.convertPolicyResponseOutput;
         } else {
             responseErrors = (ResponseErrors) unMarshal;
             throw new Exception("Error while getting ConvertPolicyResponseOutput: " + ((ResponseErrors) unMarshal).getErrors());
         }
    }
}
