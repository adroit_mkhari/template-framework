package co.za.fnb.flow.pages.sales_entity.page_factory;

import co.za.fnb.flow.pages.sales_entity.SalesEntity;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class VoiceRecordingRetrievalPopupPageObjects {

    @FindBy(id = "auditTrailDetailView:reasonTextArea")
    private WebElement reasonTextArea;

    @FindBy(id = "auditTrailDetailView:Enter")
    private WebElement enter;

    @FindBy(id = "auditTrailDetailView:Cancel")
    private WebElement cancel;

    @FindBy(id = "auditTrailDetailView:Clear")
    private WebElement clear;

    public VoiceRecordingRetrievalPopupPageObjects(WebDriver driver) {
        PageFactory.initElements(driver, this);
    }

    public WebElement getReasonTextArea() {
        return reasonTextArea;
    }

    public WebElement getEnter() {
        return enter;
    }

    public WebElement getCancel() {
        return cancel;
    }

    public WebElement getClear() {
        return clear;
    }
}
