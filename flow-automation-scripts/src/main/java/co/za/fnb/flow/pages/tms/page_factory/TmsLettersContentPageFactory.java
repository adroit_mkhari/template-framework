package co.za.fnb.flow.pages.tms.page_factory;

import co.za.fnb.flow.pages.BasePage;
import co.za.fnb.flow.tester.ScenarioOperator;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class TmsLettersContentPageFactory extends BasePage {
    // Content 1
    // @id = btnContent
    // @xpath = //*[@id="btnContent"]
    @FindBy(xpath = "//*[@id=\"btnContent\"]")
    private WebElement contentOne;

    // TODO: Get Content 2 Locator
    // Content 2
    // @id = btnContent
    // @xpath = //*[@id="btnContent"]
    @FindBy(xpath = "//*[@id=\"btnContent\"]")
    private List<WebElement> contentTwo;

    // Download
    // @id = download
    // @xpath = //*[@id="download"]
    @FindBy(id = "download")
    private WebElement download;

    @FindBy(xpath = "//*[@id=\"toolbarContainer\"]")
    private WebElement toolbarContainer;

    @FindBy(xpath = "//*[@id=\"pageNumberLabel\"]")
    private WebElement pageNumberLabel;

    @FindBy(xpath = "//*[@id=\"dsm_pdf_js_viewer\"]/iframe[contains (@ng-src, \"pdfjs\")]")
    private WebElement pdfJs;

    public TmsLettersContentPageFactory(WebDriver driver, ScenarioOperator scenarioOperator) {
        super(driver, scenarioOperator);
        PageFactory.initElements(driver, this);
    }

    public WebElement getContentOne() {
        return contentOne;
    }

    public List<WebElement> getContentTwo() {
        return contentTwo;
    }

    public WebElement getDownload() {
        return download;
    }

    public WebElement getToolbarContainer() {
        return toolbarContainer;
    }

    public WebElement getPageNumberLabel() {
        return pageNumberLabel;
    }

    public WebElement getPdfJs() {
        return pdfJs;
    }
}
