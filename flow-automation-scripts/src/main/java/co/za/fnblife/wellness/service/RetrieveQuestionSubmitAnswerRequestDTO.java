
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionSubmitAnswerRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionSubmitAnswerRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionAnswerRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionAnswerDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionSubmitAnswerRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class RetrieveQuestionSubmitAnswerRequestDTO {

    protected RetrieveQuestionAnswerRequestHeader requestHeader;
    protected RetrieveQuestionAnswerDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionAnswerRequestHeader }
     *     
     */
    public RetrieveQuestionAnswerRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionAnswerRequestHeader }
     *     
     */
    public void setRequestHeader(RetrieveQuestionAnswerRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveQuestionAnswerDataAreaRq }
     *     
     */
    public RetrieveQuestionAnswerDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveQuestionAnswerDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrieveQuestionAnswerDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
