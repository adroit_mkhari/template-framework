
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboardResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboardResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboardResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class RetrievePreloadDashboardResponseDTO {

    protected RetrievePreloadDashboardResponseHeader responseHeader;
    protected RetrievePreloadDashboardDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardResponseHeader }
     *     
     */
    public RetrievePreloadDashboardResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardResponseHeader }
     *     
     */
    public void setResponseHeader(RetrievePreloadDashboardResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardDataAreaRs }
     *     
     */
    public RetrievePreloadDashboardDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardDataAreaRs }
     *     
     */
    public void setDataAreaRs(RetrievePreloadDashboardDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
