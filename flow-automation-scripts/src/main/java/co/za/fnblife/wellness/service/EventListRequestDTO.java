
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventListRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventListRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}eventListRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}eventListDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventListRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class EventListRequestDTO {

    protected EventListRequestHeader requestHeader;
    protected EventListDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link EventListRequestHeader }
     *     
     */
    public EventListRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventListRequestHeader }
     *     
     */
    public void setRequestHeader(EventListRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link EventListDataAreaRq }
     *     
     */
    public EventListDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventListDataAreaRq }
     *     
     */
    public void setDataAreaRq(EventListDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
