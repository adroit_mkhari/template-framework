
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionAnswerSubCategory complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionAnswerSubCategory"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="answerOption" type="{http://service.wellness.fnblife.za.co/}answerOption" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="answerType" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="question" type="{http://service.wellness.fnblife.za.co/}question" minOccurs="0"/&gt;
 *         &lt;element name="quickTip" type="{http://service.wellness.fnblife.za.co/}quickTip" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="status" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subCategoryID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="subCategoryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subCategoryScore" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionAnswerSubCategory", propOrder = {
    "answerOption",
    "answerType",
    "question",
    "quickTip",
    "status",
    "subCategoryID",
    "subCategoryName",
    "subCategoryScore"
})
public class RetrieveQuestionAnswerSubCategory {

    @XmlElement(nillable = true)
    protected List<AnswerOption> answerOption;
    protected int answerType;
    protected Question question;
    @XmlElement(nillable = true)
    protected List<QuickTip> quickTip;
    protected String status;
    protected int subCategoryID;
    protected String subCategoryName;
    protected int subCategoryScore;

    /**
     * Gets the value of the answerOption property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the answerOption property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getAnswerOption().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AnswerOption }
     * 
     * 
     */
    public List<AnswerOption> getAnswerOption() {
        if (answerOption == null) {
            answerOption = new ArrayList<AnswerOption>();
        }
        return this.answerOption;
    }

    /**
     * Gets the value of the answerType property.
     * 
     */
    public int getAnswerType() {
        return answerType;
    }

    /**
     * Sets the value of the answerType property.
     * 
     */
    public void setAnswerType(int value) {
        this.answerType = value;
    }

    /**
     * Gets the value of the question property.
     * 
     * @return
     *     possible object is
     *     {@link Question }
     *     
     */
    public Question getQuestion() {
        return question;
    }

    /**
     * Sets the value of the question property.
     * 
     * @param value
     *     allowed object is
     *     {@link Question }
     *     
     */
    public void setQuestion(Question value) {
        this.question = value;
    }

    /**
     * Gets the value of the quickTip property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the quickTip property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getQuickTip().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link QuickTip }
     * 
     * 
     */
    public List<QuickTip> getQuickTip() {
        if (quickTip == null) {
            quickTip = new ArrayList<QuickTip>();
        }
        return this.quickTip;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatus(String value) {
        this.status = value;
    }

    /**
     * Gets the value of the subCategoryID property.
     * 
     */
    public int getSubCategoryID() {
        return subCategoryID;
    }

    /**
     * Sets the value of the subCategoryID property.
     * 
     */
    public void setSubCategoryID(int value) {
        this.subCategoryID = value;
    }

    /**
     * Gets the value of the subCategoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubCategoryName() {
        return subCategoryName;
    }

    /**
     * Sets the value of the subCategoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubCategoryName(String value) {
        this.subCategoryName = value;
    }

    /**
     * Gets the value of the subCategoryScore property.
     * 
     */
    public int getSubCategoryScore() {
        return subCategoryScore;
    }

    /**
     * Sets the value of the subCategoryScore property.
     * 
     */
    public void setSubCategoryScore(int value) {
        this.subCategoryScore = value;
    }

}
