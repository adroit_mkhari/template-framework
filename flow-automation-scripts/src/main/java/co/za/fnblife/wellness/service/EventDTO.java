
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="contactDetails" type="{http://service.wellness.fnblife.za.co/}contactDetailsDTO" minOccurs="0"/&gt;
 *         &lt;element name="eventDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventSport" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="fees" type="{http://service.wellness.fnblife.za.co/}feesDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="location" type="{http://service.wellness.fnblife.za.co/}locationDTO" minOccurs="0"/&gt;
 *         &lt;element name="eventItems" type="{http://service.wellness.fnblife.za.co/}eventItemsDTO" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventDTO", propOrder = {
    "contactDetails",
    "eventDate",
    "eventDescription",
    "eventID",
    "eventImage",
    "eventName",
    "eventSport",
    "eventType",
    "fees",
    "location",
    "eventItems"
})
public class EventDTO {

    protected ContactDetailsDTO contactDetails;
    protected String eventDate;
    protected String eventDescription;
    protected String eventID;
    protected String eventImage;
    protected String eventName;
    protected String eventSport;
    protected String eventType;
    @XmlElement(nillable = true)
    protected List<FeesDTO> fees;
    protected LocationDTO location;
    @XmlElement(nillable = true)
    protected List<EventItemsDTO> eventItems;

    /**
     * Gets the value of the contactDetails property.
     * 
     * @return
     *     possible object is
     *     {@link ContactDetailsDTO }
     *     
     */
    public ContactDetailsDTO getContactDetails() {
        return contactDetails;
    }

    /**
     * Sets the value of the contactDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link ContactDetailsDTO }
     *     
     */
    public void setContactDetails(ContactDetailsDTO value) {
        this.contactDetails = value;
    }

    /**
     * Gets the value of the eventDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventDate() {
        return eventDate;
    }

    /**
     * Sets the value of the eventDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventDate(String value) {
        this.eventDate = value;
    }

    /**
     * Gets the value of the eventDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventDescription() {
        return eventDescription;
    }

    /**
     * Sets the value of the eventDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventDescription(String value) {
        this.eventDescription = value;
    }

    /**
     * Gets the value of the eventID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventID() {
        return eventID;
    }

    /**
     * Sets the value of the eventID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventID(String value) {
        this.eventID = value;
    }

    /**
     * Gets the value of the eventImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventImage() {
        return eventImage;
    }

    /**
     * Sets the value of the eventImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventImage(String value) {
        this.eventImage = value;
    }

    /**
     * Gets the value of the eventName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventName() {
        return eventName;
    }

    /**
     * Sets the value of the eventName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventName(String value) {
        this.eventName = value;
    }

    /**
     * Gets the value of the eventSport property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventSport() {
        return eventSport;
    }

    /**
     * Sets the value of the eventSport property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventSport(String value) {
        this.eventSport = value;
    }

    /**
     * Gets the value of the eventType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * Sets the value of the eventType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventType(String value) {
        this.eventType = value;
    }

    /**
     * Gets the value of the fees property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the fees property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFees().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FeesDTO }
     * 
     * 
     */
    public List<FeesDTO> getFees() {
        if (fees == null) {
            fees = new ArrayList<FeesDTO>();
        }
        return this.fees;
    }

    /**
     * Gets the value of the location property.
     * 
     * @return
     *     possible object is
     *     {@link LocationDTO }
     *     
     */
    public LocationDTO getLocation() {
        return location;
    }

    /**
     * Sets the value of the location property.
     * 
     * @param value
     *     allowed object is
     *     {@link LocationDTO }
     *     
     */
    public void setLocation(LocationDTO value) {
        this.location = value;
    }

    /**
     * Gets the value of the eventItems property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the eventItems property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEventItems().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link EventItemsDTO }
     * 
     * 
     */
    public List<EventItemsDTO> getEventItems() {
        if (eventItems == null) {
            eventItems = new ArrayList<EventItemsDTO>();
        }
        return this.eventItems;
    }

}
