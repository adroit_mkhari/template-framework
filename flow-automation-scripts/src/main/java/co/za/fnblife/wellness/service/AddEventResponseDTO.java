
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEventResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEventResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}addEventResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}addEventResponseType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEventResponseDTO", propOrder = {
    "header",
    "dataArea"
})
public class AddEventResponseDTO {

    protected AddEventResponseHeader header;
    protected AddEventResponseType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link AddEventResponseHeader }
     *     
     */
    public AddEventResponseHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddEventResponseHeader }
     *     
     */
    public void setHeader(AddEventResponseHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link AddEventResponseType }
     *     
     */
    public AddEventResponseType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddEventResponseType }
     *     
     */
    public void setDataArea(AddEventResponseType value) {
        this.dataArea = value;
    }

}
