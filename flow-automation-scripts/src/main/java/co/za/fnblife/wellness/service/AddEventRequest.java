
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEventRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEventRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}addEventRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}addEventRequestType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEventRequest", propOrder = {
    "header",
    "dataArea"
})
public class AddEventRequest {

    protected AddEventRequestHeader header;
    protected AddEventRequestType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link AddEventRequestHeader }
     *     
     */
    public AddEventRequestHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddEventRequestHeader }
     *     
     */
    public void setHeader(AddEventRequestHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link AddEventRequestType }
     *     
     */
    public AddEventRequestType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddEventRequestType }
     *     
     */
    public void setDataArea(AddEventRequestType value) {
        this.dataArea = value;
    }

}
