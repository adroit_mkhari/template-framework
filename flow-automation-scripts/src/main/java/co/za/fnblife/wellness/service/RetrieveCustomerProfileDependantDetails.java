
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveCustomerProfileDependantDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveCustomerProfileDependantDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dependantCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantDOB" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantInitials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dependantTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="noOfDependants" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveCustomerProfileDependantDetails", propOrder = {
    "dependantCode",
    "dependantDOB",
    "dependantID",
    "dependantInitials",
    "dependantName",
    "dependantSurname",
    "dependantTitle",
    "noOfDependants"
})
public class RetrieveCustomerProfileDependantDetails {

    protected String dependantCode;
    protected String dependantDOB;
    protected String dependantID;
    protected String dependantInitials;
    protected String dependantName;
    protected String dependantSurname;
    protected String dependantTitle;
    protected int noOfDependants;

    /**
     * Gets the value of the dependantCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantCode() {
        return dependantCode;
    }

    /**
     * Sets the value of the dependantCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantCode(String value) {
        this.dependantCode = value;
    }

    /**
     * Gets the value of the dependantDOB property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantDOB() {
        return dependantDOB;
    }

    /**
     * Sets the value of the dependantDOB property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantDOB(String value) {
        this.dependantDOB = value;
    }

    /**
     * Gets the value of the dependantID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantID() {
        return dependantID;
    }

    /**
     * Sets the value of the dependantID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantID(String value) {
        this.dependantID = value;
    }

    /**
     * Gets the value of the dependantInitials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantInitials() {
        return dependantInitials;
    }

    /**
     * Sets the value of the dependantInitials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantInitials(String value) {
        this.dependantInitials = value;
    }

    /**
     * Gets the value of the dependantName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantName() {
        return dependantName;
    }

    /**
     * Sets the value of the dependantName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantName(String value) {
        this.dependantName = value;
    }

    /**
     * Gets the value of the dependantSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantSurname() {
        return dependantSurname;
    }

    /**
     * Sets the value of the dependantSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantSurname(String value) {
        this.dependantSurname = value;
    }

    /**
     * Gets the value of the dependantTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDependantTitle() {
        return dependantTitle;
    }

    /**
     * Sets the value of the dependantTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDependantTitle(String value) {
        this.dependantTitle = value;
    }

    /**
     * Gets the value of the noOfDependants property.
     * 
     */
    public int getNoOfDependants() {
        return noOfDependants;
    }

    /**
     * Sets the value of the noOfDependants property.
     * 
     */
    public void setNoOfDependants(int value) {
        this.noOfDependants = value;
    }

}
