
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RetrieveEventRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RetrieveEventRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveEventRequestHeaderDTO" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}retrieveEventRequestTypeDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RetrieveEventRequestDTO", propOrder = {
    "responseHeader",
    "dataArea"
})
public class RetrieveEventRequestDTO {

    protected RetrieveEventRequestHeaderDTO responseHeader;
    protected RetrieveEventRequestTypeDTO dataArea;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveEventRequestHeaderDTO }
     *     
     */
    public RetrieveEventRequestHeaderDTO getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveEventRequestHeaderDTO }
     *     
     */
    public void setResponseHeader(RetrieveEventRequestHeaderDTO value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveEventRequestTypeDTO }
     *     
     */
    public RetrieveEventRequestTypeDTO getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveEventRequestTypeDTO }
     *     
     */
    public void setDataArea(RetrieveEventRequestTypeDTO value) {
        this.dataArea = value;
    }

}
