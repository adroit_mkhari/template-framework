
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for MaintainCustomerProfileDataAreaRq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="MaintainCustomerProfileDataAreaRq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personalDetails" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfilePersonalDetails" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidDetails" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileMedicalAidDetails" minOccurs="0"/&gt;
 *         &lt;element name="dependantDetails" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileDependantDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="events" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileEvents" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MaintainCustomerProfileDataAreaRq", propOrder = {
    "personalDetails",
    "medicalAidDetails",
    "dependantDetails",
    "events"
})
public class MaintainCustomerProfileDataAreaRq {

    protected MaintainCustomerProfilePersonalDetails personalDetails;
    protected MaintainCustomerProfileMedicalAidDetails medicalAidDetails;
    @XmlElement(nillable = true)
    protected List<MaintainCustomerProfileDependantDetails> dependantDetails;
    @XmlElement(nillable = true)
    protected List<MaintainCustomerProfileEvents> events;

    /**
     * Gets the value of the personalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfilePersonalDetails }
     *     
     */
    public MaintainCustomerProfilePersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    /**
     * Sets the value of the personalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfilePersonalDetails }
     *     
     */
    public void setPersonalDetails(MaintainCustomerProfilePersonalDetails value) {
        this.personalDetails = value;
    }

    /**
     * Gets the value of the medicalAidDetails property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfileMedicalAidDetails }
     *     
     */
    public MaintainCustomerProfileMedicalAidDetails getMedicalAidDetails() {
        return medicalAidDetails;
    }

    /**
     * Sets the value of the medicalAidDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfileMedicalAidDetails }
     *     
     */
    public void setMedicalAidDetails(MaintainCustomerProfileMedicalAidDetails value) {
        this.medicalAidDetails = value;
    }

    /**
     * Gets the value of the dependantDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependantDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependantDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MaintainCustomerProfileDependantDetails }
     * 
     * 
     */
    public List<MaintainCustomerProfileDependantDetails> getDependantDetails() {
        if (dependantDetails == null) {
            dependantDetails = new ArrayList<MaintainCustomerProfileDependantDetails>();
        }
        return this.dependantDetails;
    }

    /**
     * Gets the value of the events property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the events property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link MaintainCustomerProfileEvents }
     * 
     * 
     */
    public List<MaintainCustomerProfileEvents> getEvents() {
        if (events == null) {
            events = new ArrayList<MaintainCustomerProfileEvents>();
        }
        return this.events;
    }

}
