
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for saveGoalsResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="saveGoalsResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}goalsResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}goalsDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "saveGoalsResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class SaveGoalsResponseDTO {

    protected GoalsResponseHeader responseHeader;
    protected GoalsDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link GoalsResponseHeader }
     *     
     */
    public GoalsResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoalsResponseHeader }
     *     
     */
    public void setResponseHeader(GoalsResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link GoalsDataAreaRs }
     *     
     */
    public GoalsDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link GoalsDataAreaRs }
     *     
     */
    public void setDataAreaRs(GoalsDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
