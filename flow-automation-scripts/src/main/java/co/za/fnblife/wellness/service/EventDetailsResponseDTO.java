
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventDetailsResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventDetailsResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}eventDetailsResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}eventDetailsDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventDetailsResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class EventDetailsResponseDTO {

    protected EventDetailsResponseHeader responseHeader;
    protected EventDetailsDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link EventDetailsResponseHeader }
     *     
     */
    public EventDetailsResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventDetailsResponseHeader }
     *     
     */
    public void setResponseHeader(EventDetailsResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link EventDetailsDataAreaRs }
     *     
     */
    public EventDetailsDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link EventDetailsDataAreaRs }
     *     
     */
    public void setDataAreaRs(EventDetailsDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
