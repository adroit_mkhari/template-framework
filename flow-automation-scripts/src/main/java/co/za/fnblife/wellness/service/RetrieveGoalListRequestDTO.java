
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveGoalListRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveGoalListRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}retrieveGoalMessageHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}retrieveGoalDataAreaRqDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveGoalListRequestDTO", propOrder = {
    "header",
    "dataArea"
})
public class RetrieveGoalListRequestDTO {

    protected RetrieveGoalMessageHeader header;
    protected RetrieveGoalDataAreaRqDTO dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalMessageHeader }
     *     
     */
    public RetrieveGoalMessageHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalMessageHeader }
     *     
     */
    public void setHeader(RetrieveGoalMessageHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveGoalDataAreaRqDTO }
     *     
     */
    public RetrieveGoalDataAreaRqDTO getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveGoalDataAreaRqDTO }
     *     
     */
    public void setDataArea(RetrieveGoalDataAreaRqDTO value) {
        this.dataArea = value;
    }

}
