
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionnaireDataAreaRs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionnaireDataAreaRs"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="categoryID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="categoryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="categoryScore" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="categoryText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subCategory" type="{http://service.wellness.fnblife.za.co/}retrieveQuestionnaireSubCategory" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionnaireDataAreaRs", propOrder = {
    "categoryID",
    "categoryName",
    "categoryScore",
    "categoryText",
    "sessionKey",
    "subCategory"
})
public class RetrieveQuestionnaireDataAreaRs {

    protected int categoryID;
    protected String categoryName;
    protected int categoryScore;
    protected String categoryText;
    protected String sessionKey;
    @XmlElement(nillable = true)
    protected List<RetrieveQuestionnaireSubCategory> subCategory;

    /**
     * Gets the value of the categoryID property.
     * 
     */
    public int getCategoryID() {
        return categoryID;
    }

    /**
     * Sets the value of the categoryID property.
     * 
     */
    public void setCategoryID(int value) {
        this.categoryID = value;
    }

    /**
     * Gets the value of the categoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets the value of the categoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryName(String value) {
        this.categoryName = value;
    }

    /**
     * Gets the value of the categoryScore property.
     * 
     */
    public int getCategoryScore() {
        return categoryScore;
    }

    /**
     * Sets the value of the categoryScore property.
     * 
     */
    public void setCategoryScore(int value) {
        this.categoryScore = value;
    }

    /**
     * Gets the value of the categoryText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryText() {
        return categoryText;
    }

    /**
     * Sets the value of the categoryText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryText(String value) {
        this.categoryText = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the subCategory property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the subCategory property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSubCategory().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetrieveQuestionnaireSubCategory }
     * 
     * 
     */
    public List<RetrieveQuestionnaireSubCategory> getSubCategory() {
        if (subCategory == null) {
            subCategory = new ArrayList<RetrieveQuestionnaireSubCategory>();
        }
        return this.subCategory;
    }

}
