
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveCustomerProfileDataAreaRs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveCustomerProfileDataAreaRs"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="personalDetails" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfilePersonalDetails" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidDetails" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileMedicalAidDetails" minOccurs="0"/&gt;
 *         &lt;element name="dependantDetails" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileDependantDetails" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="events" type="{http://service.wellness.fnblife.za.co/}retrieveCustomerProfileEvents" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveCustomerProfileDataAreaRs", propOrder = {
    "personalDetails",
    "medicalAidDetails",
    "dependantDetails",
    "events"
})
public class RetrieveCustomerProfileDataAreaRs {

    protected RetrieveCustomerProfilePersonalDetails personalDetails;
    protected RetrieveCustomerProfileMedicalAidDetails medicalAidDetails;
    @XmlElement(nillable = true)
    protected List<RetrieveCustomerProfileDependantDetails> dependantDetails;
    @XmlElement(nillable = true)
    protected List<RetrieveCustomerProfileEvents> events;

    /**
     * Gets the value of the personalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfilePersonalDetails }
     *     
     */
    public RetrieveCustomerProfilePersonalDetails getPersonalDetails() {
        return personalDetails;
    }

    /**
     * Sets the value of the personalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfilePersonalDetails }
     *     
     */
    public void setPersonalDetails(RetrieveCustomerProfilePersonalDetails value) {
        this.personalDetails = value;
    }

    /**
     * Gets the value of the medicalAidDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveCustomerProfileMedicalAidDetails }
     *     
     */
    public RetrieveCustomerProfileMedicalAidDetails getMedicalAidDetails() {
        return medicalAidDetails;
    }

    /**
     * Sets the value of the medicalAidDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveCustomerProfileMedicalAidDetails }
     *     
     */
    public void setMedicalAidDetails(RetrieveCustomerProfileMedicalAidDetails value) {
        this.medicalAidDetails = value;
    }

    /**
     * Gets the value of the dependantDetails property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the dependantDetails property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getDependantDetails().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetrieveCustomerProfileDependantDetails }
     * 
     * 
     */
    public List<RetrieveCustomerProfileDependantDetails> getDependantDetails() {
        if (dependantDetails == null) {
            dependantDetails = new ArrayList<RetrieveCustomerProfileDependantDetails>();
        }
        return this.dependantDetails;
    }

    /**
     * Gets the value of the events property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the events property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEvents().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetrieveCustomerProfileEvents }
     * 
     * 
     */
    public List<RetrieveCustomerProfileEvents> getEvents() {
        if (events == null) {
            events = new ArrayList<RetrieveCustomerProfileEvents>();
        }
        return this.events;
    }

}
