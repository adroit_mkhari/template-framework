
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loadCustomerProfileResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loadCustomerProfileResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}loadCustomerProfileResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}loadCustomerProfileDataAreaRs" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loadCustomerProfileResponseDTO", propOrder = {
    "responseHeader",
    "dataAreaRs"
})
public class LoadCustomerProfileResponseDTO {

    protected LoadCustomerProfileResponseHeader responseHeader;
    protected LoadCustomerProfileDataAreaRs dataAreaRs;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link LoadCustomerProfileResponseHeader }
     *     
     */
    public LoadCustomerProfileResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadCustomerProfileResponseHeader }
     *     
     */
    public void setResponseHeader(LoadCustomerProfileResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link LoadCustomerProfileDataAreaRs }
     *     
     */
    public LoadCustomerProfileDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadCustomerProfileDataAreaRs }
     *     
     */
    public void setDataAreaRs(LoadCustomerProfileDataAreaRs value) {
        this.dataAreaRs = value;
    }

}
