
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboardRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboardRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboardRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class RetrievePreloadDashboardRequestDTO {

    protected RetrievePreloadDashboardRequestHeader requestHeader;
    protected RetrievePreloadDashboardDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardRequestHeader }
     *     
     */
    public RetrievePreloadDashboardRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardRequestHeader }
     *     
     */
    public void setRequestHeader(RetrievePreloadDashboardRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardDataAreaRq }
     *     
     */
    public RetrievePreloadDashboardDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardDataAreaRq }
     *     
     */
    public void setDataAreaRq(RetrievePreloadDashboardDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
