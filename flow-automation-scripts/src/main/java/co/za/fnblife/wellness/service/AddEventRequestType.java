
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for addEventRequestType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="addEventRequestType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addEventDetails" type="{http://service.wellness.fnblife.za.co/}addEventDetails" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "addEventRequestType", propOrder = {
    "addEventDetails",
    "sessionKey"
})
public class AddEventRequestType {

    protected AddEventDetails addEventDetails;
    protected String sessionKey;

    /**
     * Gets the value of the addEventDetails property.
     * 
     * @return
     *     possible object is
     *     {@link AddEventDetails }
     *     
     */
    public AddEventDetails getAddEventDetails() {
        return addEventDetails;
    }

    /**
     * Sets the value of the addEventDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link AddEventDetails }
     *     
     */
    public void setAddEventDetails(AddEventDetails value) {
        this.addEventDetails = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

}
