
package co.za.fnblife.wellness.service;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for Goal complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Goal"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="subCategoryID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="quickTipID" type="{http://www.w3.org/2001/XMLSchema}integer"/&gt;
 *         &lt;element name="quickTipMakeGoal" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="goalDeadline" type="{http://www.w3.org/2001/XMLSchema}date" minOccurs="0"/&gt;
 *         &lt;element name="goalStatus" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Goal", propOrder = {
    "subCategoryID",
    "quickTipID",
    "quickTipMakeGoal",
    "goalDeadline",
    "goalStatus"
})
public class Goal {

    @XmlElement(required = true)
    protected BigInteger subCategoryID;
    @XmlElement(required = true)
    protected BigInteger quickTipID;
    protected BigInteger quickTipMakeGoal;
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar goalDeadline;
    protected BigInteger goalStatus;

    /**
     * Gets the value of the subCategoryID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubCategoryID() {
        return subCategoryID;
    }

    /**
     * Sets the value of the subCategoryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubCategoryID(BigInteger value) {
        this.subCategoryID = value;
    }

    /**
     * Gets the value of the quickTipID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuickTipID() {
        return quickTipID;
    }

    /**
     * Sets the value of the quickTipID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuickTipID(BigInteger value) {
        this.quickTipID = value;
    }

    /**
     * Gets the value of the quickTipMakeGoal property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuickTipMakeGoal() {
        return quickTipMakeGoal;
    }

    /**
     * Sets the value of the quickTipMakeGoal property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuickTipMakeGoal(BigInteger value) {
        this.quickTipMakeGoal = value;
    }

    /**
     * Gets the value of the goalDeadline property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getGoalDeadline() {
        return goalDeadline;
    }

    /**
     * Sets the value of the goalDeadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setGoalDeadline(XMLGregorianCalendar value) {
        this.goalDeadline = value;
    }

    /**
     * Gets the value of the goalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getGoalStatus() {
        return goalStatus;
    }

    /**
     * Sets the value of the goalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setGoalStatus(BigInteger value) {
        this.goalStatus = value;
    }

}
