
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pushEventImageRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pushEventImageRequest"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}pushEventImageRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataArea" type="{http://service.wellness.fnblife.za.co/}pushEventImageType" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pushEventImageRequest", propOrder = {
    "header",
    "dataArea"
})
public class PushEventImageRequest {

    protected PushEventImageRequestHeader header;
    protected PushEventImageType dataArea;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link PushEventImageRequestHeader }
     *     
     */
    public PushEventImageRequestHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link PushEventImageRequestHeader }
     *     
     */
    public void setHeader(PushEventImageRequestHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataArea property.
     * 
     * @return
     *     possible object is
     *     {@link PushEventImageType }
     *     
     */
    public PushEventImageType getDataArea() {
        return dataArea;
    }

    /**
     * Sets the value of the dataArea property.
     * 
     * @param value
     *     allowed object is
     *     {@link PushEventImageType }
     *     
     */
    public void setDataArea(PushEventImageType value) {
        this.dataArea = value;
    }

}
