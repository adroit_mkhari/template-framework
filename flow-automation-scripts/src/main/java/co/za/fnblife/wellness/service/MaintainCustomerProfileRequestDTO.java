
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintainCustomerProfileRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintainCustomerProfileRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}MaintainCustomerProfileDataAreaRq" minOccurs="0"/&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileRequestHeader" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintainCustomerProfileRequestDTO", propOrder = {
    "dataAreaRq",
    "requestHeader"
})
public class MaintainCustomerProfileRequestDTO {

    protected MaintainCustomerProfileDataAreaRq dataAreaRq;
    protected MaintainCustomerProfileRequestHeader requestHeader;

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfileDataAreaRq }
     *     
     */
    public MaintainCustomerProfileDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfileDataAreaRq }
     *     
     */
    public void setDataAreaRq(MaintainCustomerProfileDataAreaRq value) {
        this.dataAreaRq = value;
    }

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfileRequestHeader }
     *     
     */
    public MaintainCustomerProfileRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfileRequestHeader }
     *     
     */
    public void setRequestHeader(MaintainCustomerProfileRequestHeader value) {
        this.requestHeader = value;
    }

}
