
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboardDataAreaRs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboardDataAreaRs"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="goTo" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="notifications" type="{http://service.wellness.fnblife.za.co/}notifications" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboardDataAreaRs", propOrder = {
    "goTo",
    "notifications",
    "sessionKey"
})
public class RetrievePreloadDashboardDataAreaRs {

    protected int goTo;
    @XmlElement(nillable = true)
    protected List<Notifications> notifications;
    protected String sessionKey;

    /**
     * Gets the value of the goTo property.
     * 
     */
    public int getGoTo() {
        return goTo;
    }

    /**
     * Sets the value of the goTo property.
     * 
     */
    public void setGoTo(int value) {
        this.goTo = value;
    }

    /**
     * Gets the value of the notifications property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the notifications property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getNotifications().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link Notifications }
     * 
     * 
     */
    public List<Notifications> getNotifications() {
        if (notifications == null) {
            notifications = new ArrayList<Notifications>();
        }
        return this.notifications;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

}
