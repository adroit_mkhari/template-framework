
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pushEvent complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pushEvent"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="eventID" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="eventImageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pushEvent", propOrder = {
    "eventID",
    "eventImage",
    "eventImageName"
})
public class PushEvent {

    protected String eventID;
    protected byte[] eventImage;
    protected String eventImageName;

    /**
     * Gets the value of the eventID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventID() {
        return eventID;
    }

    /**
     * Sets the value of the eventID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventID(String value) {
        this.eventID = value;
    }

    /**
     * Gets the value of the eventImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getEventImage() {
        return eventImage;
    }

    /**
     * Sets the value of the eventImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setEventImage(byte[] value) {
        this.eventImage = value;
    }

    /**
     * Gets the value of the eventImageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventImageName() {
        return eventImageName;
    }

    /**
     * Sets the value of the eventImageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventImageName(String value) {
        this.eventImageName = value;
    }

}
