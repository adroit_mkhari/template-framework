
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for loadCustomerProfileRequestDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="loadCustomerProfileRequestDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="requestHeader" type="{http://service.wellness.fnblife.za.co/}loadCustomerProfileRequestHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRq" type="{http://service.wellness.fnblife.za.co/}loadCustomerProfileDataAreaRq" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "loadCustomerProfileRequestDTO", propOrder = {
    "requestHeader",
    "dataAreaRq"
})
public class LoadCustomerProfileRequestDTO {

    protected LoadCustomerProfileRequestHeader requestHeader;
    protected LoadCustomerProfileDataAreaRq dataAreaRq;

    /**
     * Gets the value of the requestHeader property.
     * 
     * @return
     *     possible object is
     *     {@link LoadCustomerProfileRequestHeader }
     *     
     */
    public LoadCustomerProfileRequestHeader getRequestHeader() {
        return requestHeader;
    }

    /**
     * Sets the value of the requestHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadCustomerProfileRequestHeader }
     *     
     */
    public void setRequestHeader(LoadCustomerProfileRequestHeader value) {
        this.requestHeader = value;
    }

    /**
     * Gets the value of the dataAreaRq property.
     * 
     * @return
     *     possible object is
     *     {@link LoadCustomerProfileDataAreaRq }
     *     
     */
    public LoadCustomerProfileDataAreaRq getDataAreaRq() {
        return dataAreaRq;
    }

    /**
     * Sets the value of the dataAreaRq property.
     * 
     * @param value
     *     allowed object is
     *     {@link LoadCustomerProfileDataAreaRq }
     *     
     */
    public void setDataAreaRq(LoadCustomerProfileDataAreaRq value) {
        this.dataAreaRq = value;
    }

}
