
package co.za.fnblife.wellness.service;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintainCustomerProfileMedicalAidDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintainCustomerProfileMedicalAidDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="medicalCardImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidPlan" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberInitials" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberGender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberLanguage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberHomeTel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberCellNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="mainMemberEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintainCustomerProfileMedicalAidDetails", propOrder = {
    "medicalCardImage",
    "medicalAidName",
    "medicalAidPlan",
    "medicalAidNo",
    "mainMemberCode",
    "mainMemberName",
    "mainMemberSurname",
    "mainMemberInitials",
    "mainMemberTitle",
    "mainMemberID",
    "mainMemberGender",
    "mainMemberLanguage",
    "mainMemberHomeTel",
    "mainMemberCellNo",
    "mainMemberEmail"
})
public class MaintainCustomerProfileMedicalAidDetails {

    protected String medicalCardImage;
    protected String medicalAidName;
    protected String medicalAidPlan;
    protected String medicalAidNo;
    protected String mainMemberCode;
    protected String mainMemberName;
    protected String mainMemberSurname;
    protected String mainMemberInitials;
    protected String mainMemberTitle;
    protected BigInteger mainMemberID;
    protected String mainMemberGender;
    protected String mainMemberLanguage;
    protected String mainMemberHomeTel;
    protected String mainMemberCellNo;
    protected String mainMemberEmail;

    /**
     * Gets the value of the medicalCardImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalCardImage() {
        return medicalCardImage;
    }

    /**
     * Sets the value of the medicalCardImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalCardImage(String value) {
        this.medicalCardImage = value;
    }

    /**
     * Gets the value of the medicalAidName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalAidName() {
        return medicalAidName;
    }

    /**
     * Sets the value of the medicalAidName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalAidName(String value) {
        this.medicalAidName = value;
    }

    /**
     * Gets the value of the medicalAidPlan property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalAidPlan() {
        return medicalAidPlan;
    }

    /**
     * Sets the value of the medicalAidPlan property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalAidPlan(String value) {
        this.medicalAidPlan = value;
    }

    /**
     * Gets the value of the medicalAidNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalAidNo() {
        return medicalAidNo;
    }

    /**
     * Sets the value of the medicalAidNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalAidNo(String value) {
        this.medicalAidNo = value;
    }

    /**
     * Gets the value of the mainMemberCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberCode() {
        return mainMemberCode;
    }

    /**
     * Sets the value of the mainMemberCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberCode(String value) {
        this.mainMemberCode = value;
    }

    /**
     * Gets the value of the mainMemberName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberName() {
        return mainMemberName;
    }

    /**
     * Sets the value of the mainMemberName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberName(String value) {
        this.mainMemberName = value;
    }

    /**
     * Gets the value of the mainMemberSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberSurname() {
        return mainMemberSurname;
    }

    /**
     * Sets the value of the mainMemberSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberSurname(String value) {
        this.mainMemberSurname = value;
    }

    /**
     * Gets the value of the mainMemberInitials property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberInitials() {
        return mainMemberInitials;
    }

    /**
     * Sets the value of the mainMemberInitials property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberInitials(String value) {
        this.mainMemberInitials = value;
    }

    /**
     * Gets the value of the mainMemberTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberTitle() {
        return mainMemberTitle;
    }

    /**
     * Sets the value of the mainMemberTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberTitle(String value) {
        this.mainMemberTitle = value;
    }

    /**
     * Gets the value of the mainMemberID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getMainMemberID() {
        return mainMemberID;
    }

    /**
     * Sets the value of the mainMemberID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setMainMemberID(BigInteger value) {
        this.mainMemberID = value;
    }

    /**
     * Gets the value of the mainMemberGender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberGender() {
        return mainMemberGender;
    }

    /**
     * Sets the value of the mainMemberGender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberGender(String value) {
        this.mainMemberGender = value;
    }

    /**
     * Gets the value of the mainMemberLanguage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberLanguage() {
        return mainMemberLanguage;
    }

    /**
     * Sets the value of the mainMemberLanguage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberLanguage(String value) {
        this.mainMemberLanguage = value;
    }

    /**
     * Gets the value of the mainMemberHomeTel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberHomeTel() {
        return mainMemberHomeTel;
    }

    /**
     * Sets the value of the mainMemberHomeTel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberHomeTel(String value) {
        this.mainMemberHomeTel = value;
    }

    /**
     * Gets the value of the mainMemberCellNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberCellNo() {
        return mainMemberCellNo;
    }

    /**
     * Sets the value of the mainMemberCellNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberCellNo(String value) {
        this.mainMemberCellNo = value;
    }

    /**
     * Gets the value of the mainMemberEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMainMemberEmail() {
        return mainMemberEmail;
    }

    /**
     * Sets the value of the mainMemberEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMainMemberEmail(String value) {
        this.mainMemberEmail = value;
    }

}
