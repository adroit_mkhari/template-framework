
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for myEvents complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="myEvents"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="savedEvent" type="{http://service.wellness.fnblife.za.co/}savedEvent" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "myEvents", propOrder = {
    "savedEvent"
})
public class MyEvents {

    protected SavedEvent savedEvent;

    /**
     * Gets the value of the savedEvent property.
     * 
     * @return
     *     possible object is
     *     {@link SavedEvent }
     *     
     */
    public SavedEvent getSavedEvent() {
        return savedEvent;
    }

    /**
     * Sets the value of the savedEvent property.
     * 
     * @param value
     *     allowed object is
     *     {@link SavedEvent }
     *     
     */
    public void setSavedEvent(SavedEvent value) {
        this.savedEvent = value;
    }

}
