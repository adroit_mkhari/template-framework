
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for goal1 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="goal1"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="goalActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalDeadline" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/&gt;
 *         &lt;element name="goalDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalImage" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalImageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalStatus" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="quickTipID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="subCategoryID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "goal1", propOrder = {
    "goalActivity",
    "goalDeadline",
    "goalDetails",
    "goalImage",
    "goalImageName",
    "goalName",
    "goalStatus",
    "quickTipID",
    "subCategoryID"
})
public class Goal1 {

    protected String goalActivity;
    protected Object goalDeadline;
    protected String goalDetails;
    protected String goalImage;
    protected String goalImageName;
    protected String goalName;
    protected int goalStatus;
    protected int quickTipID;
    protected int subCategoryID;

    /**
     * Gets the value of the goalActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalActivity() {
        return goalActivity;
    }

    /**
     * Sets the value of the goalActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalActivity(String value) {
        this.goalActivity = value;
    }

    /**
     * Gets the value of the goalDeadline property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getGoalDeadline() {
        return goalDeadline;
    }

    /**
     * Sets the value of the goalDeadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setGoalDeadline(Object value) {
        this.goalDeadline = value;
    }

    /**
     * Gets the value of the goalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalDetails() {
        return goalDetails;
    }

    /**
     * Sets the value of the goalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalDetails(String value) {
        this.goalDetails = value;
    }

    /**
     * Gets the value of the goalImage property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalImage() {
        return goalImage;
    }

    /**
     * Sets the value of the goalImage property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalImage(String value) {
        this.goalImage = value;
    }

    /**
     * Gets the value of the goalImageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalImageName() {
        return goalImageName;
    }

    /**
     * Sets the value of the goalImageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalImageName(String value) {
        this.goalImageName = value;
    }

    /**
     * Gets the value of the goalName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalName() {
        return goalName;
    }

    /**
     * Sets the value of the goalName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalName(String value) {
        this.goalName = value;
    }

    /**
     * Gets the value of the goalStatus property.
     * 
     */
    public int getGoalStatus() {
        return goalStatus;
    }

    /**
     * Sets the value of the goalStatus property.
     * 
     */
    public void setGoalStatus(int value) {
        this.goalStatus = value;
    }

    /**
     * Gets the value of the quickTipID property.
     * 
     */
    public int getQuickTipID() {
        return quickTipID;
    }

    /**
     * Sets the value of the quickTipID property.
     * 
     */
    public void setQuickTipID(int value) {
        this.quickTipID = value;
    }

    /**
     * Gets the value of the subCategoryID property.
     * 
     */
    public int getSubCategoryID() {
        return subCategoryID;
    }

    /**
     * Sets the value of the subCategoryID property.
     * 
     */
    public void setSubCategoryID(int value) {
        this.subCategoryID = value;
    }

}
