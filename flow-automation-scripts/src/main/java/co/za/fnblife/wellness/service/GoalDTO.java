
package co.za.fnblife.wellness.service;

import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for goalDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="goalDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="categoryName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalActivity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalDeadline" type="{http://www.w3.org/2001/XMLSchema}anySimpleType" minOccurs="0"/&gt;
 *         &lt;element name="goalDetails" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="goalImageName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quickTipID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="subCategoryID" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "goalDTO", propOrder = {
    "categoryName",
    "goalActivity",
    "goalDeadline",
    "goalDetails",
    "goalImageName",
    "quickTipID",
    "subCategoryID"
})
public class GoalDTO {

    protected String categoryName;
    protected String goalActivity;
    protected Object goalDeadline;
    protected String goalDetails;
    protected String goalImageName;
    protected BigInteger quickTipID;
    protected BigInteger subCategoryID;

    /**
     * Gets the value of the categoryName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCategoryName() {
        return categoryName;
    }

    /**
     * Sets the value of the categoryName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCategoryName(String value) {
        this.categoryName = value;
    }

    /**
     * Gets the value of the goalActivity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalActivity() {
        return goalActivity;
    }

    /**
     * Sets the value of the goalActivity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalActivity(String value) {
        this.goalActivity = value;
    }

    /**
     * Gets the value of the goalDeadline property.
     * 
     * @return
     *     possible object is
     *     {@link Object }
     *     
     */
    public Object getGoalDeadline() {
        return goalDeadline;
    }

    /**
     * Sets the value of the goalDeadline property.
     * 
     * @param value
     *     allowed object is
     *     {@link Object }
     *     
     */
    public void setGoalDeadline(Object value) {
        this.goalDeadline = value;
    }

    /**
     * Gets the value of the goalDetails property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalDetails() {
        return goalDetails;
    }

    /**
     * Sets the value of the goalDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalDetails(String value) {
        this.goalDetails = value;
    }

    /**
     * Gets the value of the goalImageName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGoalImageName() {
        return goalImageName;
    }

    /**
     * Sets the value of the goalImageName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGoalImageName(String value) {
        this.goalImageName = value;
    }

    /**
     * Gets the value of the quickTipID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getQuickTipID() {
        return quickTipID;
    }

    /**
     * Sets the value of the quickTipID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setQuickTipID(BigInteger value) {
        this.quickTipID = value;
    }

    /**
     * Gets the value of the subCategoryID property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getSubCategoryID() {
        return subCategoryID;
    }

    /**
     * Sets the value of the subCategoryID property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setSubCategoryID(BigInteger value) {
        this.subCategoryID = value;
    }

}
