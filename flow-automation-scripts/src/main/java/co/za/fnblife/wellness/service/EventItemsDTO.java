
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for eventItemsDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="eventItemsDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="entryFee" type="{http://www.w3.org/2001/XMLSchema}float"/&gt;
 *         &lt;element name="eventItemDescription" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="eventItemName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "eventItemsDTO", propOrder = {
    "entryFee",
    "eventItemDescription",
    "eventItemName"
})
public class EventItemsDTO {

    protected float entryFee;
    protected String eventItemDescription;
    protected String eventItemName;

    /**
     * Gets the value of the entryFee property.
     * 
     */
    public float getEntryFee() {
        return entryFee;
    }

    /**
     * Sets the value of the entryFee property.
     * 
     */
    public void setEntryFee(float value) {
        this.entryFee = value;
    }

    /**
     * Gets the value of the eventItemDescription property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventItemDescription() {
        return eventItemDescription;
    }

    /**
     * Sets the value of the eventItemDescription property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventItemDescription(String value) {
        this.eventItemDescription = value;
    }

    /**
     * Gets the value of the eventItemName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEventItemName() {
        return eventItemName;
    }

    /**
     * Sets the value of the eventItemName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEventItemName(String value) {
        this.eventItemName = value;
    }

}
