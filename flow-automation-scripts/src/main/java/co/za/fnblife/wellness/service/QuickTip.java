
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for quickTip complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="quickTip"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="makeGoal" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="quickTipID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="quickTipText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="quickTipTitle" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "quickTip", propOrder = {
    "makeGoal",
    "quickTipID",
    "quickTipText",
    "quickTipTitle"
})
public class QuickTip {

    protected int makeGoal;
    protected int quickTipID;
    protected String quickTipText;
    protected String quickTipTitle;

    /**
     * Gets the value of the makeGoal property.
     * 
     */
    public int getMakeGoal() {
        return makeGoal;
    }

    /**
     * Sets the value of the makeGoal property.
     * 
     */
    public void setMakeGoal(int value) {
        this.makeGoal = value;
    }

    /**
     * Gets the value of the quickTipID property.
     * 
     */
    public int getQuickTipID() {
        return quickTipID;
    }

    /**
     * Sets the value of the quickTipID property.
     * 
     */
    public void setQuickTipID(int value) {
        this.quickTipID = value;
    }

    /**
     * Gets the value of the quickTipText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickTipText() {
        return quickTipText;
    }

    /**
     * Sets the value of the quickTipText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickTipText(String value) {
        this.quickTipText = value;
    }

    /**
     * Gets the value of the quickTipTitle property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQuickTipTitle() {
        return quickTipTitle;
    }

    /**
     * Sets the value of the quickTipTitle property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQuickTipTitle(String value) {
        this.quickTipTitle = value;
    }

}
