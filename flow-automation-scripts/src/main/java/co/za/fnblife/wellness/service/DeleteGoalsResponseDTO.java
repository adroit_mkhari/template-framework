
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for deleteGoalsResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="deleteGoalsResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="header" type="{http://service.wellness.fnblife.za.co/}deleteGoalMessageHeader" minOccurs="0"/&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}deleteGoalDataAreaRsDTO" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "deleteGoalsResponseDTO", propOrder = {
    "header",
    "dataAreaRs"
})
public class DeleteGoalsResponseDTO {

    protected DeleteGoalMessageHeader header;
    protected DeleteGoalDataAreaRsDTO dataAreaRs;

    /**
     * Gets the value of the header property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteGoalMessageHeader }
     *     
     */
    public DeleteGoalMessageHeader getHeader() {
        return header;
    }

    /**
     * Sets the value of the header property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteGoalMessageHeader }
     *     
     */
    public void setHeader(DeleteGoalMessageHeader value) {
        this.header = value;
    }

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link DeleteGoalDataAreaRsDTO }
     *     
     */
    public DeleteGoalDataAreaRsDTO getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeleteGoalDataAreaRsDTO }
     *     
     */
    public void setDataAreaRs(DeleteGoalDataAreaRsDTO value) {
        this.dataAreaRs = value;
    }

}
