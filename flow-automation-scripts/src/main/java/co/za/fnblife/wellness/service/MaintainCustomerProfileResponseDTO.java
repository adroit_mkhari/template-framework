
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for maintainCustomerProfileResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="maintainCustomerProfileResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dataAreaRs" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileDataAreaRs" minOccurs="0"/&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}maintainCustomerProfileResponseHeader" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "maintainCustomerProfileResponseDTO", propOrder = {
    "dataAreaRs",
    "responseHeader"
})
public class MaintainCustomerProfileResponseDTO {

    protected MaintainCustomerProfileDataAreaRs dataAreaRs;
    protected MaintainCustomerProfileResponseHeader responseHeader;

    /**
     * Gets the value of the dataAreaRs property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfileDataAreaRs }
     *     
     */
    public MaintainCustomerProfileDataAreaRs getDataAreaRs() {
        return dataAreaRs;
    }

    /**
     * Sets the value of the dataAreaRs property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfileDataAreaRs }
     *     
     */
    public void setDataAreaRs(MaintainCustomerProfileDataAreaRs value) {
        this.dataAreaRs = value;
    }

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link MaintainCustomerProfileResponseHeader }
     *     
     */
    public MaintainCustomerProfileResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link MaintainCustomerProfileResponseHeader }
     *     
     */
    public void setResponseHeader(MaintainCustomerProfileResponseHeader value) {
        this.responseHeader = value;
    }

}
