
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveQuestionAnswerDataAreaRq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveQuestionAnswerDataAreaRq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="answerSet" type="{http://service.wellness.fnblife.za.co/}answerSet" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="subCategoryID" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveQuestionAnswerDataAreaRq", propOrder = {
    "answerSet",
    "sessionKey",
    "subCategoryID",
    "ucn"
})
public class RetrieveQuestionAnswerDataAreaRq {

    protected AnswerSet answerSet;
    protected String sessionKey;
    protected int subCategoryID;
    protected int ucn;

    /**
     * Gets the value of the answerSet property.
     * 
     * @return
     *     possible object is
     *     {@link AnswerSet }
     *     
     */
    public AnswerSet getAnswerSet() {
        return answerSet;
    }

    /**
     * Sets the value of the answerSet property.
     * 
     * @param value
     *     allowed object is
     *     {@link AnswerSet }
     *     
     */
    public void setAnswerSet(AnswerSet value) {
        this.answerSet = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the subCategoryID property.
     * 
     */
    public int getSubCategoryID() {
        return subCategoryID;
    }

    /**
     * Sets the value of the subCategoryID property.
     * 
     */
    public void setSubCategoryID(int value) {
        this.subCategoryID = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     */
    public int getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     */
    public void setUcn(int value) {
        this.ucn = value;
    }

}
