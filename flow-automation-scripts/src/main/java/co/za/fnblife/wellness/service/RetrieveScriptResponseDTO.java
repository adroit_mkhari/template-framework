
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveScriptResponseDTO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveScriptResponseDTO"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="responseHeader" type="{http://service.wellness.fnblife.za.co/}retrieveScriptResponseHeader" minOccurs="0"/&gt;
 *         &lt;element name="futureCollections" type="{http://service.wellness.fnblife.za.co/}retrieveFutureCollections" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="historyCollections" type="{http://service.wellness.fnblife.za.co/}historyCollections" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveScriptResponseDTO", propOrder = {
    "responseHeader",
    "futureCollections",
    "historyCollections"
})
public class RetrieveScriptResponseDTO {

    protected RetrieveScriptResponseHeader responseHeader;
    @XmlElement(nillable = true)
    protected List<RetrieveFutureCollections> futureCollections;
    @XmlElement(nillable = true)
    protected List<HistoryCollections> historyCollections;

    /**
     * Gets the value of the responseHeader property.
     * 
     * @return
     *     possible object is
     *     {@link RetrieveScriptResponseHeader }
     *     
     */
    public RetrieveScriptResponseHeader getResponseHeader() {
        return responseHeader;
    }

    /**
     * Sets the value of the responseHeader property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrieveScriptResponseHeader }
     *     
     */
    public void setResponseHeader(RetrieveScriptResponseHeader value) {
        this.responseHeader = value;
    }

    /**
     * Gets the value of the futureCollections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the futureCollections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFutureCollections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RetrieveFutureCollections }
     * 
     * 
     */
    public List<RetrieveFutureCollections> getFutureCollections() {
        if (futureCollections == null) {
            futureCollections = new ArrayList<RetrieveFutureCollections>();
        }
        return this.futureCollections;
    }

    /**
     * Gets the value of the historyCollections property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the historyCollections property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getHistoryCollections().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link HistoryCollections }
     * 
     * 
     */
    public List<HistoryCollections> getHistoryCollections() {
        if (historyCollections == null) {
            historyCollections = new ArrayList<HistoryCollections>();
        }
        return this.historyCollections;
    }

}
