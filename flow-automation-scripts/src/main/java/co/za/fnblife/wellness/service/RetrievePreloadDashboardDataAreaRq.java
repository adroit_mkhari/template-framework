
package co.za.fnblife.wellness.service;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrievePreloadDashboardDataAreaRq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrievePreloadDashboardDataAreaRq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerResponse" type="{http://service.wellness.fnblife.za.co/}retrievePreloadDashboardCustomerResponse" minOccurs="0"/&gt;
 *         &lt;element name="idNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrievePreloadDashboardDataAreaRq", propOrder = {
    "customerResponse",
    "idNumber",
    "sessionKey",
    "ucn"
})
public class RetrievePreloadDashboardDataAreaRq {

    protected RetrievePreloadDashboardCustomerResponse customerResponse;
    protected String idNumber;
    protected String sessionKey;
    protected int ucn;

    /**
     * Gets the value of the customerResponse property.
     * 
     * @return
     *     possible object is
     *     {@link RetrievePreloadDashboardCustomerResponse }
     *     
     */
    public RetrievePreloadDashboardCustomerResponse getCustomerResponse() {
        return customerResponse;
    }

    /**
     * Sets the value of the customerResponse property.
     * 
     * @param value
     *     allowed object is
     *     {@link RetrievePreloadDashboardCustomerResponse }
     *     
     */
    public void setCustomerResponse(RetrievePreloadDashboardCustomerResponse value) {
        this.customerResponse = value;
    }

    /**
     * Gets the value of the idNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIdNumber() {
        return idNumber;
    }

    /**
     * Sets the value of the idNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIdNumber(String value) {
        this.idNumber = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     */
    public int getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     */
    public void setUcn(int value) {
        this.ucn = value;
    }

}
