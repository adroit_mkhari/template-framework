
package co.za.fnblife.wellness.service;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for retrieveCustomerProfilePersonalDetails complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="retrieveCustomerProfilePersonalDetails"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="customerAge" type="{http://www.w3.org/2001/XMLSchema}int"/&gt;
 *         &lt;element name="customerHeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="customerName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customerNickname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customerSurname" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="customerWeight" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/&gt;
 *         &lt;element name="dob" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="idNo" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *         &lt;element name="profilePicture" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="smoker" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}integer" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "retrieveCustomerProfilePersonalDetails", propOrder = {
    "customerAge",
    "customerHeight",
    "customerName",
    "customerNickname",
    "customerSurname",
    "customerWeight",
    "dob",
    "idNo",
    "profilePicture",
    "smoker",
    "ucn"
})
public class RetrieveCustomerProfilePersonalDetails {

    protected int customerAge;
    protected BigDecimal customerHeight;
    protected String customerName;
    protected String customerNickname;
    protected String customerSurname;
    protected BigDecimal customerWeight;
    protected String dob;
    protected BigInteger idNo;
    protected String profilePicture;
    protected Boolean smoker;
    protected BigInteger ucn;

    /**
     * Gets the value of the customerAge property.
     * 
     */
    public int getCustomerAge() {
        return customerAge;
    }

    /**
     * Sets the value of the customerAge property.
     * 
     */
    public void setCustomerAge(int value) {
        this.customerAge = value;
    }

    /**
     * Gets the value of the customerHeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCustomerHeight() {
        return customerHeight;
    }

    /**
     * Sets the value of the customerHeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCustomerHeight(BigDecimal value) {
        this.customerHeight = value;
    }

    /**
     * Gets the value of the customerName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the value of the customerName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerName(String value) {
        this.customerName = value;
    }

    /**
     * Gets the value of the customerNickname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNickname() {
        return customerNickname;
    }

    /**
     * Sets the value of the customerNickname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNickname(String value) {
        this.customerNickname = value;
    }

    /**
     * Gets the value of the customerSurname property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerSurname() {
        return customerSurname;
    }

    /**
     * Sets the value of the customerSurname property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerSurname(String value) {
        this.customerSurname = value;
    }

    /**
     * Gets the value of the customerWeight property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getCustomerWeight() {
        return customerWeight;
    }

    /**
     * Sets the value of the customerWeight property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setCustomerWeight(BigDecimal value) {
        this.customerWeight = value;
    }

    /**
     * Gets the value of the dob property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDob() {
        return dob;
    }

    /**
     * Sets the value of the dob property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDob(String value) {
        this.dob = value;
    }

    /**
     * Gets the value of the idNo property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getIdNo() {
        return idNo;
    }

    /**
     * Sets the value of the idNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setIdNo(BigInteger value) {
        this.idNo = value;
    }

    /**
     * Gets the value of the profilePicture property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProfilePicture() {
        return profilePicture;
    }

    /**
     * Sets the value of the profilePicture property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProfilePicture(String value) {
        this.profilePicture = value;
    }

    /**
     * Gets the value of the smoker property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSmoker() {
        return smoker;
    }

    /**
     * Sets the value of the smoker property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSmoker(Boolean value) {
        this.smoker = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link BigInteger }
     *     
     */
    public BigInteger getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigInteger }
     *     
     */
    public void setUcn(BigInteger value) {
        this.ucn = value;
    }

}
