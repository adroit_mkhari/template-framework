
package co.za.fnblife.wellness.service;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for submitScriptDataAreaRq complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="submitScriptDataAreaRq"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="addInfo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="allergies" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="cellphoneNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="collectFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="description" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="dispensedPatient" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="generics" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medicalAid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="medicalAidNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="onceOffDependant" type="{http://service.wellness.fnblife.za.co/}onceOffDependant" maxOccurs="unbounded" minOccurs="0"/&gt;
 *         &lt;element name="paymentType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="repeatService" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="scriptImage" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/&gt;
 *         &lt;element name="sessionKey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="ucn" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "submitScriptDataAreaRq", propOrder = {
    "addInfo",
    "allergies",
    "cellphoneNo",
    "collectFrom",
    "description",
    "dispensedPatient",
    "generics",
    "medicalAid",
    "medicalAidNo",
    "onceOffDependant",
    "paymentType",
    "repeatService",
    "scriptImage",
    "sessionKey",
    "ucn"
})
public class SubmitScriptDataAreaRq {

    protected String addInfo;
    protected String allergies;
    protected String cellphoneNo;
    protected String collectFrom;
    protected String description;
    protected String dispensedPatient;
    protected String generics;
    protected String medicalAid;
    protected String medicalAidNo;
    @XmlElement(nillable = true)
    protected List<OnceOffDependant> onceOffDependant;
    protected String paymentType;
    protected String repeatService;
    protected byte[] scriptImage;
    protected String sessionKey;
    protected String ucn;

    /**
     * Gets the value of the addInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAddInfo() {
        return addInfo;
    }

    /**
     * Sets the value of the addInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAddInfo(String value) {
        this.addInfo = value;
    }

    /**
     * Gets the value of the allergies property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAllergies() {
        return allergies;
    }

    /**
     * Sets the value of the allergies property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAllergies(String value) {
        this.allergies = value;
    }

    /**
     * Gets the value of the cellphoneNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCellphoneNo() {
        return cellphoneNo;
    }

    /**
     * Sets the value of the cellphoneNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCellphoneNo(String value) {
        this.cellphoneNo = value;
    }

    /**
     * Gets the value of the collectFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCollectFrom() {
        return collectFrom;
    }

    /**
     * Sets the value of the collectFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCollectFrom(String value) {
        this.collectFrom = value;
    }

    /**
     * Gets the value of the description property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescription() {
        return description;
    }

    /**
     * Sets the value of the description property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescription(String value) {
        this.description = value;
    }

    /**
     * Gets the value of the dispensedPatient property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDispensedPatient() {
        return dispensedPatient;
    }

    /**
     * Sets the value of the dispensedPatient property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDispensedPatient(String value) {
        this.dispensedPatient = value;
    }

    /**
     * Gets the value of the generics property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGenerics() {
        return generics;
    }

    /**
     * Sets the value of the generics property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGenerics(String value) {
        this.generics = value;
    }

    /**
     * Gets the value of the medicalAid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalAid() {
        return medicalAid;
    }

    /**
     * Sets the value of the medicalAid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalAid(String value) {
        this.medicalAid = value;
    }

    /**
     * Gets the value of the medicalAidNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMedicalAidNo() {
        return medicalAidNo;
    }

    /**
     * Sets the value of the medicalAidNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMedicalAidNo(String value) {
        this.medicalAidNo = value;
    }

    /**
     * Gets the value of the onceOffDependant property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the onceOffDependant property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOnceOffDependant().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link OnceOffDependant }
     * 
     * 
     */
    public List<OnceOffDependant> getOnceOffDependant() {
        if (onceOffDependant == null) {
            onceOffDependant = new ArrayList<OnceOffDependant>();
        }
        return this.onceOffDependant;
    }

    /**
     * Gets the value of the paymentType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentType() {
        return paymentType;
    }

    /**
     * Sets the value of the paymentType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentType(String value) {
        this.paymentType = value;
    }

    /**
     * Gets the value of the repeatService property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRepeatService() {
        return repeatService;
    }

    /**
     * Sets the value of the repeatService property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRepeatService(String value) {
        this.repeatService = value;
    }

    /**
     * Gets the value of the scriptImage property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getScriptImage() {
        return scriptImage;
    }

    /**
     * Sets the value of the scriptImage property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setScriptImage(byte[] value) {
        this.scriptImage = value;
    }

    /**
     * Gets the value of the sessionKey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSessionKey() {
        return sessionKey;
    }

    /**
     * Sets the value of the sessionKey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSessionKey(String value) {
        this.sessionKey = value;
    }

    /**
     * Gets the value of the ucn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUcn() {
        return ucn;
    }

    /**
     * Sets the value of the ucn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUcn(String value) {
        this.ucn = value;
    }

}
