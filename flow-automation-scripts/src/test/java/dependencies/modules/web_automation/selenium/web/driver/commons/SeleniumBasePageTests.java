package dependencies.modules.web_automation.selenium.web.driver.commons;

import dependencies.modules.web_automation.selenium.web.driver.commons.page.factory.GoogleHomePage;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import selenium.web.driver.DriverManagerFactory;
import selenium.web.driver.DriverType;
import selenium.web.driver.managers.DriverManager;

public class SeleniumBasePageTests {
    @Test
    public void chromeDriverManagerTest() {
        try {
            DriverManager driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
            WebDriver webDriver = driverManager.getWebDriver(true);
            webDriver.get("https://www.google.com");
            String title = webDriver.getTitle();
            System.out.println("Title: " + title);
            Assert.assertEquals("Google", title);

            GoogleHomePage googleHomePage = new GoogleHomePage(webDriver);
            googleHomePage.inputSearchText("Selenium Automation");
            Thread.sleep(5000);
            googleHomePage.pressEscapeOnSearch();
            Thread.sleep(5000);
            // googleHomePage.clickSearch();
            // Thread.sleep(5000);
            googleHomePage.pressEnterOnSearch();
            Thread.sleep(5000);
            webDriver.quit();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
