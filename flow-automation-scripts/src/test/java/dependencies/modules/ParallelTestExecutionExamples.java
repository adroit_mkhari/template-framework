package dependencies.modules;

import co.za.fnb.automation.common.testing.Scenario;
import co.za.fnb.automation.common.testing.TestCase;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import selenium.web.driver.DriverManagerFactory;
import selenium.web.driver.DriverType;
import selenium.web.driver.managers.DriverManager;

import java.util.HashSet;
import java.util.List;
import java.util.concurrent.*;

public class ParallelTestExecutionExamples {
    private static TestCase testCase;
    private ExecutorService executorService = Executors.newFixedThreadPool(1);
    private HashSet<Callable<String>> callables = new HashSet<Callable<String>>();

    @BeforeClass
    public static void setup() {
        testCase = new TestCase("Automation Test Case",
                "src/test/resources/data_config.properties",
                "TEST_DATA_WORKBOOK", "TEST_DATA_SHEET", "Test Case No",
                "Field 1,Field 2,Field 3,Field 4,Field 5");

        try {
            testCase.loadDataExcel();
            testCase.loadTestCaseScenarios();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @DataProvider(name = "Excel")
    public Object[][] getTestCaseScenarios() {
        try {
            if (testCase != null) {
                Object[][] scenarioDataProvider = testCase.getScenarioDataProvider();
                return scenarioDataProvider;
            } else {
                throw new Exception("No Test Cases Loaded. ie., Test Case Is Null.");
            }
        } catch (Exception e) {
            // TODO: Handle Exception
            e.printStackTrace();
        }
        return null;
    }

    @Test(dataProvider = "Excel")
    public void scenarioTests(final Scenario scenario) {
        System.out.println("Name: " + scenario.getCellValue("Name") + ", Surname: " + scenario.getCellValue("Surname"));

        callables.add(new Callable<String>() {
            @Override
            public String call() throws Exception {
                DriverManager driverManager = DriverManagerFactory.getDriverManager(DriverType.CHROME);
                WebDriver webDriver = driverManager.getWebDriver(true);
                webDriver.get("https://www.google.com");
                Thread.sleep(10000);
                String title = webDriver.getTitle();
                System.out.println("Title: " + title);
                WebElement input = webDriver.findElement(By.id("input"));
                input.sendKeys("Adroit");
                Thread.sleep(3000);
                WebElement element = webDriver.findElement(By.xpath("//*[@id=\"description\"]/span"));
                String text = element.getText();
                System.out.println("Search Text: " + text);
                webDriver.quit();
                return "Name: " + scenario.getCellValue("Name") + ", Surname: " + scenario.getCellValue("Surname");
            }
        });
    }

    @AfterClass
    public void executeTests() throws InterruptedException, ExecutionException {
        List<Future<String>> futures = executorService.invokeAll(callables);

        for (Future<String> future : futures) {
            String futureValue = future.get();
            System.out.println("Future Value: " + futureValue);
        }

        executorService.shutdown();
    }
}
