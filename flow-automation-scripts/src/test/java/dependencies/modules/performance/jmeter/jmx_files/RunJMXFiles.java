package dependencies.modules.performance.jmeter.jmx_files;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        tags = {"@JMXFiles"},
        features = {"src/test/resources/feature_files/jmx_files"},
        glue = {"dependencies/modules/performance/jmeter/jmx_files/steps"}
)

public class RunJMXFiles {
}
