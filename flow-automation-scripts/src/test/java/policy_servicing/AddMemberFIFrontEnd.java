package policy_servicing;

import co.za.fnb.flow.handlers.Scenario;
import co.za.fnb.flow.handlers.TestCase;
import co.za.fnb.flow.pages.LoginPage;
import co.za.fnb.flow.setup.DriverSetup;
import co.za.fnb.flow.setup.PropertiesSetup;
import co.za.fnb.flow.setup.TestResultReportFlag;
import co.za.fnb.flow.tester.*;
import co.za.fnb.flow.tester.services.functions.Takeup;
import co.za.fnb.flow.tester.services.models.TakeupScenario;
import co.za.fnb.flow.tester.services.models.TakeupScenarioFactory;
import cucumber.api.DataTable;
import generated.PolicyTakeUpResponse;
import generated.PolicyTakeUpResponsePayload;
import generated.PolicyTakeUpResponsePolicyData;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.*;

public class AddMemberFIFrontEnd {
    private Logger log  = LogManager.getLogger(AddMemberFIFrontEnd.class);
    private Properties properties;
    private String application;
    private String username;
    private String password;
    private static TestCase testCase;
    private static TestCase takeupTestCase;
    private ExecutorService executorService;
    private HashSet<Callable<Scenario>> callables = new HashSet<Callable<Scenario>>();

    private WebDriver getDriver() {
        DriverSetup driverSetup = new DriverSetup();
        driverSetup.setProperties(properties);
        driverSetup.setBrowserCapabilities();
        return driverSetup.getDriver();
    }

    private LoginPage openApplication(WebDriver driver, ScenarioOperator scenarioOperator) throws Exception {
        LoginPage loginPage = new LoginPage(driver, scenarioOperator);
        loginPage.open(application);
        return new LoginPage(driver, scenarioOperator);
    }

    private void login(LoginPage loginPage) throws Exception {
        username = properties.getProperty("USERNAME");
        password = properties.getProperty("PASSWORD");
        loginPage.login(username, password);
    }

    private String takeUpPolicy(Scenario scenario) throws Exception {
        String takeupFirst = scenario.getCellValue("Takeup");
        String takeupCode = scenario.getCellValue("Takeup Code");

        if (takeupFirst.equalsIgnoreCase("YES") && !takeupCode.isEmpty()) {
            TakeupScenario takeupScenario = TakeupScenarioFactory.getTakeupScenario(takeupCode);
            ScenarioTester scenarioTester = new ScenarioTester(TestComponent.LETTERS,
                    takeupTestCase.getDataTable(), null);
            Takeup takeup = new Takeup(scenarioTester, takeupScenario);

            takeup.setupPolicyTakeUpRequestInputFromTakeupScenario();
            PolicyTakeUpResponse policyTakeUpResponse = takeup.sendRequest();
            PolicyTakeUpResponsePayload response = policyTakeUpResponse.getResponse();
            PolicyTakeUpResponsePolicyData policyData = response.getPolicyData();
            String policyNumber = policyData.getPolicyNo();
            log.info("Successfully Created Policy");
            log.info("------------------------------");
            log.info("Policy Number: " + policyNumber);
            log.info("------------------------------");
            return policyNumber;
        }
        return null;
    }

    private void killDriver(WebDriver driver) {
        if (driver != null) {
            driver.quit();
        }
    }

    private void attachScreenshots(Scenario scenario, Integer scenarioReportRowIndex) {
        List<byte[]> screenshots = scenario.getScreenshots();
        int size = screenshots.size();
        log.info("Number Of Screenshots On Scenario Number: " + scenarioReportRowIndex + " Is " + size);
        for (int i = 0; i < size; i++) {
            int pictureRow = 0;
            if (i != 0) {
                pictureRow = i * 35;
                pictureRow = pictureRow + 2;
            }
            testCase.writePictureToReport(String.valueOf(scenarioReportRowIndex),
                    pictureRow, 0, TestResultReportFlag.PICTURE, screenshots.get(0));
        }
    }

    @BeforeClass
    public void setup() {
        try {
            PropertiesSetup propertiesSetup = new PropertiesSetup();
            propertiesSetup.loadProperties();
            properties = propertiesSetup.getProperties();
            String numberOfThreads = properties.getProperty("NUMBER_OF_THREADS");
            application = properties.getProperty("TARGET_APPLICATION");
            executorService = Executors.newFixedThreadPool(Integer.parseInt(numberOfThreads));

            testCase = new TestCase("Add Member Funeral Insurance",
                    "src/main/resources/config.properties",
                    "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_FI",
                    "POLICY_DETAILS_ADD_MEMBER_SHEET_NAME", "Test Case No",
                    "Test Case Number,Scenario Description,Function,Product Name,Policy Number,Result,Failure Reason");

            testCase.loadDataExcel();
            testCase.loadTestCaseScenarios();
            testCase.createReport();

            takeupTestCase = new TestCase("Add Member Funeral Insurance",
                    "src/main/resources/config.properties",
                    "POLICY_TAKEUP_DATA_SHEET",
                    "POLICY_TAKEUP_SHEET_NAME", "Test Case No",
                    "Test Case Number,Scenario Description,Function,Product Name,Policy Number,Result,Failure Reason");

            takeupTestCase.loadDataExcel();
            DataTable takeupTestCaseDataTable = takeupTestCase.getDataTable();
            TakeupScenarioFactory.loadTakeupScenarios(takeupTestCaseDataTable);
        } catch (Exception e) {
           e.printStackTrace();
           System.exit(-1);
        }
    }

    @DataProvider(name = "Excel")
    public Object[][] getTestCaseScenarios() {
        try {
            if (testCase != null) {
                Object[][] scenarioDataProvider = testCase.getScenarioDataProvider();
                return scenarioDataProvider;
            } else {
                throw new Exception("No Test Cases Loaded. ie., Test Case Is Null.");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    @Test(dataProvider = "Excel")
    public void scenarioTests(final Scenario scenario) {
        final String run = scenario.getCellValue("Run");
        if (run.equalsIgnoreCase("Yes")) {
            callables.add(new Callable<Scenario>() {
                @Override
                public Scenario call() {
                    try {
                        String policyNumber = takeUpPolicy(scenario);
                        policyNumber = policyNumber != null ? policyNumber : scenario.getCellValue("Policy Number");
                        scenario.setPolicyNumber(policyNumber);

                        ScenarioOperator scenarioOperator = new ScenarioOperator(testCase.getReportableFieldList());

                        WebDriver driver = getDriver();
                        LoginPage loginPage = openApplication(driver, scenarioOperator);
                        login(loginPage);

                        scenarioOperator.setScreenShotFileName(scenario.getCellValue("Function") + " " +
                                                              scenario.getCellValue("Test Case No") + " " +
                                                              scenario.getCellValue("Scenario Description"));
                        scenarioOperator.setReport(testCase.getReport());
                        Integer scenarioReportRowIndex = Integer.valueOf(scenario.getCellValue("Test Case No").split("\\.")[0]);
                        scenarioOperator.setTestCaseNumber(String.valueOf(scenarioReportRowIndex));

                        scenarioOperator.setScenario(scenario);
                        AddMemberRunner addMemberRunner = new AddMemberRunner(driver, policyNumber, scenario, scenarioOperator);
                        addMemberRunner.run();

                        killDriver(driver);
                        return scenario;
                    } catch (Exception e) {
                        scenario.setResult(String.valueOf(TestResult.FAIL));
                        scenario.setFailureReason(e.getMessage());
                        return scenario;
                    }
                }
            });
        }
    }

    @AfterClass
    public void executeTests() throws InterruptedException, ExecutionException {
        List<Future<Scenario>> futures = executorService.invokeAll(callables);

        for (Future<Scenario> future : futures) {
            Scenario scenario = future.get();

            log.info("Test Case No: " + scenario.getCellValue("Test Case No") + ", " +
                        "Scenario Description: " + scenario.getCellValue("Scenario Description") + ", " +
                        "Result: " + scenario.getResult() + ", " +
                        "Failure Reason: " + scenario.getFailureReason());

            Integer scenarioReportRowIndex = Integer.valueOf(scenario.getCellValue("Test Case No").split("\\.")[0]);
            testCase.setReportRowIndex(scenarioReportRowIndex);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Test Case Number"),
                    String.valueOf(scenarioReportRowIndex), TestResultReportFlag.DEFAULT);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Scenario Description"),
                    scenario.getCellValue("Scenario Description"), TestResultReportFlag.WARNING);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Function"),
                    scenario.getCellValue("Function"), TestResultReportFlag.WARNING);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Product Name"),
                    scenario.getCellValue("Product Name"), TestResultReportFlag.WARNING);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Policy Number"),
                    scenario.getPolicyNumber(), TestResultReportFlag.DEFAULT);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Result"),
                    scenario.getResult(), scenario.getResult().equalsIgnoreCase(String.valueOf(TestResult.PASS))
                                          ? TestResultReportFlag.SUCCESS
                                          : TestResultReportFlag.FAIL);

            testCase.writeToReport(Arrays.asList(testCase.getReportableFieldList()).indexOf("Failure Reason"),
                    scenario.getFailureReason(), scenario.getResult().equalsIgnoreCase(String.valueOf(TestResult.PASS))
                                                 ? TestResultReportFlag.SUCCESS
                                                 : TestResultReportFlag.FAIL);

            attachScreenshots(scenario, scenarioReportRowIndex);
        }

        try {
            testCase.saveReport();
        } catch (IOException e) {
            e.printStackTrace();
        }

        executorService.shutdown();
    }

}
