@UpdatePolicyEditBeneficiaryServiceLevel
Feature: Flow Service Level Tests: Update Policy

Scenario: Update Policy Edit Beneficiary
    Given Update Policy Edit Beneficiary Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Update Policy Edit Beneficiary Test Data at "UPDATE_POLICY_EDIT_BENEFICIARY_DATA_SHEET,UPDATE_POLICY_EDIT_BENEFICIARY_SHEET_NAME"
        Then Test Update Policy Edit Beneficiary Service
