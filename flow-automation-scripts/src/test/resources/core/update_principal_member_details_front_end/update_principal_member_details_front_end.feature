@UpdatePrincipalMemberDetailsFrontEnd
Feature: Update Principal Member Details

  Scenario: Update Principal Member Details
    Given Flow Access For Update Principal Member Details
    Then Authenticate The User For Update Principal Member Details
    Then Get Queries Sheet For Update Principal Member Details "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Update Principal Member Details Data Sheet "POLICY_DETAILS_UPDATE_PRINCIPAL_MEMBER_DATA_SHEET,POLICY_DETAILS_UPDATE_PRINCIPAL_MEMBER_SHEET_NAME"
    Then Update Principal Member Details Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Update Principal Member Details