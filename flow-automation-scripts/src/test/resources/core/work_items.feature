@WorkItems
Feature: Google Search Engine

  Scenario: Search
    Given We Can Access FLOW Open The Login Page For Work Items
    Then Authenticate The User For Work Items
    Then Get Queries Sheet to use For Work Items at "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Data Sheet to use For Work Items at "POLICY_DETAILS_WORK_ITEMS_DATA_SHEET,POLICY_DETAILS_WORK_ITEMS_SHEET_NAME"
    Then Test Work Items Functionality