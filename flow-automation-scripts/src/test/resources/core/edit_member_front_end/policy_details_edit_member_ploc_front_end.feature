@PolicyDetailsEditMemberPLOCFrontEnd
Feature: Policy Servicing

  Scenario: Edit Member
    Given Flow Access For Policy Details Edit Member PLOC
    Then Authenticate The User For Policy Details Edit Member PLOC
    Then Get Queries Sheet For Policy Details Edit Member PLOC "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Edit Member PLOC Data Sheet "POLICY_DETAILS_EDIT_MEMBER_DATA_SHEET_LOC_P,POLICY_DETAILS_EDIT_MEMBER_SHEET_NAME"
    Then Policy Details Edit Member PLOC Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Edit Member PLOC