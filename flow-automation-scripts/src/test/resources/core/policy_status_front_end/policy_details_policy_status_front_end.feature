@PolicyDetailsPolicyStatusFrontEnd
Feature: Policy Servicing

  Scenario: Policy Status
    Given Flow Access For Policy Details Policy Status
    Then Authenticate The User For Policy Details Policy Status
    Then Get Queries Sheet For Policy Details Policy Status "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Policy Status Data Sheet "POLICY_DETAILS_UPDATE_POLICY_STATUS_DATA_SHEET,POLICY_DETAILS_UPDATE_POLICY_STATUS_SHEET_NAME"
    Then Policy Details Policy Status Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Policy Status