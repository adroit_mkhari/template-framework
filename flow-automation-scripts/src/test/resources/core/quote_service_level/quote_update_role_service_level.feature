@QuoteUpdateRoleServiceLevel
Feature: Flow Service Level Tests: Quote

Scenario: Quote Update Role
    Given Quote Update Role Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Quote Update Role Test Data at "QUOTE_UPDATE_ROLE_DATA_SHEET,QUOTE_UPDATE_ROLE_SHEET_NAME"
        Then Test Quote Update Role Service
