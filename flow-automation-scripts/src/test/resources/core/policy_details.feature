@PolicyDetails
Feature: Google Search Engine

  Scenario: Search
    Given We can access FLOW open the login page
    Then Authenticate the user
    Then Get Queries Sheet to use at "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Get Data Sheet to use at "DATA_SHEET,SHEET_NAME"
    # Then Get Data to use From Text File "DATA_TEXT_FILE"
    Then Test Policy Details Functionality
    # Then Change Policy Status
