@PolicyDetailsReinstatePlanFrontEnd
Feature: Policy Servicing

  Scenario: Reinstate Plan
    Given Flow Access For Policy Details Reinstate Plan
    Then Authenticate The User For Policy Details Reinstate Plan
    Then Get Queries Sheet For Policy Details Reinstate Plan "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Reinstate Plan Data Sheet "POLICY_DETAILS_REINSTATE_PLAN_DATA_SHEET,POLICY_DETAILS_REINSTATE_PLAN_SHEET_NAME"
    Then Policy Details Reinstate Plan Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Reinstate Plan