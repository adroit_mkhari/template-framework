@PolicyDetailsUpdatePremiumStatusFIFrontEnd
Feature: Policy Servicing

  Scenario: Update Premium Status
    Given Flow Access For Policy Details Update Premium Status FI
    Then Authenticate The User For Policy Details Update Premium Status FI
    Then Get Queries Sheet For Policy Details Update Premium Status FI "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Update Premium Status FI Data Sheet "POLICY_DETAILS_PREMIUM_STATUS_DATA_SHEET_FI,POLICY_DETAILS_PREMIUM_STATUS_SHEET_NAME"
    Then Policy Details Update Premium Status FI Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Update Premium Status FI