@PolicyDetailsAddMemberBLOCFrontEnd
Feature: Policy Servicing

  Scenario: Add Member
    Given Flow Access For Policy Details Add Member BLOC
    Then Authenticate The User For Policy Details Add Member BLOC
    Then Get Queries Sheet For Policy Details Add Member BLOC "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Add Member BLOC Data Sheet "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_LOC_B,POLICY_DETAILS_ADD_MEMBER_SHEET_NAME"
    Then Policy Details Add Member BLOC Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET_BLOC,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Add Member BLOC