@PolicyDetailsAddMemberFIFrontEnd
Feature: Policy Servicing

  Scenario: Add Member
    Given Flow Access For Policy Details Add Member FI
    Then Authenticate The User For Policy Details Add Member FI
    Then Get Queries Sheet For Policy Details Add Member FI "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Add Member FI Data Sheet "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_FI,POLICY_DETAILS_ADD_MEMBER_SHEET_NAME"
    Then Policy Details Add Member FI Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Add Member FI