@PolicyDetailsAddMemberHCPFrontEnd
Feature: Policy Servicing

  Scenario: Add Member
    Given Flow Access For Policy Details Add Member HCP
    Then Authenticate The User For Policy Details Add Member HCP
    Then Get Queries Sheet For Policy Details Add Member HCP "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Add Member HCP Data Sheet "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_HCP,POLICY_DETAILS_ADD_MEMBER_SHEET_NAME"
    Then Policy Details Add Member HCP Takeup Scenarios Data "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_HCP,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Add Member HCP

