@ConvertPolicyServiceLevel
Feature: Flow Service Level Tests: Convert Policy

Scenario: Convert Policy
    Given Convert Policy Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Convert Policy Test Data at "CONVERT_POLICY_DATA_SHEET,CONVERT_POLICY_SHEET_NAME"
        Then Test Convert Policy Service
