@CancelPolicyServiceLevel
Feature: Flow Service Level Tests: Cancel Policy

Scenario: Cancel Policy
    Given Cancel Policy Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Cancel Policy Test Data at "CANCEL_POLICY_DATA_SHEET,CANCEL_POLICY_SHEET_NAME"
        Then Test Cancel Policy Service
