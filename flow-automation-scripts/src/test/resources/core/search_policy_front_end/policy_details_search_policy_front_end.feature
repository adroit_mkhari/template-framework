@PolicyDetailsSearchPolicyFrontEnd
Feature: Google Search Engine

  Scenario: Search
    Given Flow Access For Policy Details Search Policy
    Then Authenticate The User For Policy Details Search Policy
    Then Get Queries Sheet For Policy Details Search Policy "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Search Policy Data Sheet "POLICY_DETAILS_SEARCH_POLICY_DATA_SHEET,POLICY_DETAILS_SEARCH_POLICY_SHEET_NAME"
    Then Policy Details Search Policy Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Search Policy