@PolicyDetailsQuoteFrontEnd
Feature: Policy Servicing

  Scenario: Quote
    Given Flow Access For Policy Details Quote
    Then Authenticate The User For Policy Details Quote
    Then Get Queries Sheet For Policy Details Quote "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Quote Data Sheet "POLICY_DETAILS_QUOTE_DATA_SHEET,POLICY_DETAILS_QUOTE_SHEET_NAME"
    Then Policy Details Quote Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Quote