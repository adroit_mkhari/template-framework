@ReinstatePolicyServiceLevel
Feature: Flow Service Level Tests: Reinstate Policy

Scenario: Reinstate Policy
    Given Reinstate Policy Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Reinstate Policy Test Data at "REINSTATE_POLICY_DATA_SHEET,REINSTATE_POLICY_SHEET_NAME"
        Then Test Reinstate Policy Service
