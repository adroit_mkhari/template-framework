@LossOfIncomeFrontEnd
Feature: Loss Of Income

  Scenario: Loss Of Income
    Given Flow Access For Loss Of Income
    Then Authenticate The User Loss Of Income
    Then Get Loss Of Income Data Sheet "LOSS_OF_INCOME_DATA_SHEET,LOSS_OF_INCOME_SHEET_NAME"
    Then Test Loss Of Income