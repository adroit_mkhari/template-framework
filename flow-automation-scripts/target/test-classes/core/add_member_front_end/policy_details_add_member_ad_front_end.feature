@PolicyDetailsAddMemberADFrontEnd
Feature: Policy Servicing

  Scenario: Add Member
    Given Flow Access For Policy Details Add Member AD
    Then Authenticate The User For Policy Details Add Member AD
    Then Get Queries Sheet For Policy Details Add Member AD "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Add Member AD Data Sheet "POLICY_DETAILS_ADD_MEMBER_DATA_SHEET_AD,POLICY_DETAILS_ADD_MEMBER_SHEET_NAME"
    Then Policy Details Add Member AD Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET_AD,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Add Member AD