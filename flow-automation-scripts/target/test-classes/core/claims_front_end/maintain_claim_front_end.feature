@MaintainClaimFrontEnd
Feature: Maintain Claim

  Scenario: Maintain Claim
    Given Flow Access For Maintain Claim
    Then Authenticate The User For Maintain Claim
    Then Get Queries Sheet For Maintain Claim "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Maintain Claim Data Sheet "MAINTAIN_CLAIM_DATA_SHEET,MAINTAIN_CLAIM_SHEET_NAME"
    Then Maintain Claim Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Maintain Claim