@RetentionsFIFrontEnd
Feature: Retentions

  Scenario: Retentions
    Given Flow Access For Retentions FI
    Then Authenticate The User For Retentions FI
    Then Get Retentions FI Data Sheet "RETENTIONS_FI,RETENTIONS_FI_SHEET_NAME"
    Then Test Retentions FI