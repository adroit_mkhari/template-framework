@UpdatePolicyEditBankingServiceLevel
Feature: Flow Service Level Tests: Update Policy

Scenario: Update Policy Edit Banking
    Given Update Policy Edit Banking Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Update Policy Edit Banking Test Data at "UPDATE_POLICY_EDIT_BANKING_DATA_SHEET,UPDATE_POLICY_EDIT_BANKING_SHEET_NAME"
        Then Test Update Policy Edit Banking Service
