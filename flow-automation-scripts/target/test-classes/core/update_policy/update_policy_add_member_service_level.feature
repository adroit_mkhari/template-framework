@UpdatePolicyAddMemberServiceLevel
Feature: Flow Service Level Tests: Update Policy

Scenario: Update Policy Add Member
    Given Update Policy Add Member Policy Takeup Scenarios Data at "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
        Then Get Update Policy Add Member Test Data at "UPDATE_POLICY_ADD_MEMBER_DATA_SHEET,UPDATE_POLICY_ADD_MEMBER_SHEET_NAME"
        Then Test Update Policy Add Member Service
