@PolicyDetailsCancelPolicyHCPFrontEnd
Feature: Policy Servicing

  Scenario: Cancel Policy
    Given Flow Access For Policy Details Cancel Policy HCP
    Then Authenticate The User For Policy Details Cancel Policy HCP
    Then Get Queries Sheet For Policy Details Cancel Policy HCP "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Cancel Policy HCP Data Sheet "POLICY_DETAILS_CANCEL_POLICY_DATA_SHEET_HCP,POLICY_DETAILS_CANCEL_POLICY_SHEET_NAME"
    Then Policy Details Cancel Policy HCP Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Cancel Policy HCP