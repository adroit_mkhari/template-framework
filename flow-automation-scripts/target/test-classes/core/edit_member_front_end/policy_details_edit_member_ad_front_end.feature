@PolicyDetailsEditMemberADFrontEnd
Feature: Policy Servicing

  Scenario: Edit Member
    Given Flow Access For Policy Details Edit Member AD
    Then Authenticate The User For Policy Details Edit Member AD
    Then Get Queries Sheet For Policy Details Edit Member AD "QUERIES_SHEET,QUERIES_SHEET_NAME"
    Then Get Policy Details Edit Member AD Data Sheet "POLICY_DETAILS_EDIT_MEMBER_DATA_SHEET_AD,POLICY_DETAILS_EDIT_MEMBER_SHEET_NAME"
    Then Policy Details Edit Member AD Takeup Scenarios Data "POLICY_TAKEUP_DATA_SHEET,POLICY_TAKEUP_SHEET_NAME"
    Then Test Policy Details Edit Member AD