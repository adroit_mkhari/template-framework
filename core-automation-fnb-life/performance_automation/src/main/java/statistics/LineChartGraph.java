package statistics;

import com.sun.javafx.application.PlatformImpl;
import com.sun.javafx.jmx.MXExtension;
import com.sun.javafx.runtime.SystemProperties;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.Scene;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.image.WritableImage;
import javafx.stage.Stage;
import jmeter.ApplicationPropertiesConfig;

import javax.imageio.ImageIO;
import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.CountDownLatch;

public class LineChartGraph extends Application {
    private ApplicationPropertiesConfig applicationPropertiesConfig = new ApplicationPropertiesConfig();
    private Properties properties;
    private String reportingPath;
    private Scene scene;
    private boolean generatedHistory = false;
    private static boolean launched = false;
    private boolean startedToolkit = false;
    HistoryTrend historyTrend = new HistoryTrend();
    private static volatile RuntimeException launchException = null;
    private Map<String, Double> benchmarkMap = new HashMap<>();
    String jsonResultsNewFileName = applicationPropertiesConfig.getLineGraphDataPath() + "/statistics.json";
    String responseTimeFile = applicationPropertiesConfig.getLineGraphDataPath() + "/ResponseTime.csv";
    String benchmark = applicationPropertiesConfig.getLineGraphDataPath() + "/Benchmark.csv";
    String history = applicationPropertiesConfig.getLineGraphDataPath() + "/history.csv";
    private static Thread javaFXApplicationThread;
    private int imageCount = 0;

    public LineChartGraph() {
        this.properties = applicationPropertiesConfig.getProperties();
        reportingPath = applicationPropertiesConfig.getReportingPath();
    }

    public boolean isLaunched() {
        return launched;
    }

    public void joinJavaFXApplicationThread() {
        try {
            if (javaFXApplicationThread != null) {
                System.out.println("Java FX Application Thread Alive: " + javaFXApplicationThread.isAlive());
                System.out.println("Joining Java FX Application Thread: " + javaFXApplicationThread.getName());
                javaFXApplicationThread.join(10000);
            } else {
                System.out.println("Java FX Application Thread Is Null");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error While Joining Java FX Application Thread.");
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        javaFXApplicationThread = Thread.currentThread();
        String name = javaFXApplicationThread.getName();
        System.out.println("Current Thread Name: " + name);
        try {
            System.out.println("Starting Java FX Application");
            createStage(jsonResultsNewFileName, 1, "" +
                    "Average response time for the previous results and current results");
            saveAsPng(scene, getReportPath() + "/average.png");
            createStage(jsonResultsNewFileName, 3, "" +
                    "95 percentile response time for the previous results and current results");
            saveAsPng(scene, getReportPath() + "/95_percentile.png");
            createStage(jsonResultsNewFileName, 6, "Average Trend");
            saveAsPng(scene, getReportPath() + "/AverageTrend.png");
            createStage(jsonResultsNewFileName, 7, "95 percentile Trend");
            saveAsPng(scene, getReportPath() + "/95PercentileTrend.png");
            System.out.println("Finished Starting Java FX Application Logic.");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error While Starting Java FX Application Logic.");
        }
    }

    public void launch() throws Exception {
        System.out.println("Manually Launching Java FX Application");
        if (!launched) {
            Platform.setImplicitExit(false);
            Thread mainLaunchThread = new Thread(() -> {
                try {
                    Application.launch(this.getClass());
                } finally {
                    System.out.println("Done Launching Java FX Application");
                }
            });

            mainLaunchThread.setName("JavaFX-Launcher");
            System.out.println("Starting Launch Thread: " + mainLaunchThread.getName());
            mainLaunchThread.start();
            System.out.println("Started Launch Thread: " + mainLaunchThread.getName());

            if (launchException != null) {
                throw launchException;
            }

            launched = true;
            startToolkit();
        }
    }

    private void startToolkit() throws InterruptedException {
        if (startedToolkit) {
            return;
        }

        if (SystemProperties.isDebug()) {
            MXExtension.initializeIfAvailable();
        }

        final CountDownLatch startupLatch = new CountDownLatch(1);
        countDown(startupLatch);
    }

    private void countDown(CountDownLatch startupLatch) throws InterruptedException {
        // Note, this method is called on the FX Application Thread
        PlatformImpl.startup(() -> startupLatch.countDown());

        // Wait for FX platform to start
        startupLatch.await();
    }

    public void runStart() {
        Platform.runLater(() -> {
            try {
                Stage stage = new Stage();
                start(stage);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
    }

    public void saveAsPng(Scene scene, String path) {
        try {
            System.out.println("(" + ++imageCount + ") Saving Results As A PNG Image: " + path);
            WritableImage image = scene.snapshot(null);
            File file = new File(path);
            ImageIO.write(SwingFXUtils.fromFXImage(image, null), "png", file);
            System.out.println("Done Saving Results As A PNG Image.");
        } catch (Exception e) {
            System.out.println("Error While Saving Results As A PNG Image.");
            e.printStackTrace();
        }
    }

    public Map<String, String> getServiceName(String pathToCSVFile) throws IOException {
        Map<String, String> map = new HashMap<>();
        String thisLine;
        int count = 0;
        FileInputStream fis = new FileInputStream(pathToCSVFile);
        DataInputStream myInput = new DataInputStream(fis);
        int iteration = 0;
        while ((thisLine = myInput.readLine()) != null) {
            if (iteration == 0) {
                iteration++;
                continue;
            }
            String[] response = thisLine.split(",");
            map.put(response[2], response[2]);

        }
        return map;
    }

    public Stage createStage(String pathToJson, int column, String headingName) throws IOException {
        System.out.println("Creating Stage: " + headingName);
        reportingPath = applicationPropertiesConfig.getReportingPath();
        Stage stage = new Stage();
        stage.setTitle("Line chart");
        final CategoryAxis categoryAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        categoryAxis.setLabel("service names");
        final LineChart<String, Number> lineChart =
                new LineChart<String, Number>(categoryAxis, yAxis);
        yAxis.setLabel("Response time(ms)");
        lineChart.setTitle(headingName);
        lineChart.setId("chart-series-line");
        XYChart.Series series = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        series.setName("Current results");
        series2.setName("Benchmark");
        JSONFileRead jsonFileRead = new JSONFileRead();
        String jsonFilePath = pathToJson;
        jsonFileRead.readFile(jsonFilePath);
        if ((!generatedHistory && column == 6) || (column == 7)) {
            if (column == 6) {
                column = 1;
            } else if (column == 7) {
                column = 3;
            }

            historyTrend.generateHistoryData(jsonFileRead.historyDataHashMap);
            generatedHistory = true;
            String thisLine2;
            FileInputStream fis = new FileInputStream(history);
            DataInputStream myInput2 = new DataInputStream(fis);
            XYChart.Series series4 = new XYChart.Series();
            XYChart.Series series5 = new XYChart.Series();
            XYChart.Series series6 = new XYChart.Series();
            XYChart.Series series7 = new XYChart.Series();
            XYChart.Series series8 = new XYChart.Series();
            series4.setName("Current results");
            series5.setName("Previous results 1");
            series6.setName("Previous results 2");
            series7.setName("Previous results 3");
            series8.setName("Previous results 4");
            while (((thisLine2 = myInput2.readLine()) != null)) {
                System.out.println(thisLine2);
                String[] response = thisLine2.split(",");
                if (HistoryTrend.lastMap.containsKey(response[0]) && !(response[0].equalsIgnoreCase("Total"))) {
                    int arraySize = response.length;
                    if (arraySize > column && !response[column].isEmpty()) {
                        series4.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column])));
                    }
                    if (arraySize > column + 3 && !response[column + 3].isEmpty()) {
                        series5.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column + 3])));
                    }
                    if (arraySize > column + 6 && !response[column + 6].isEmpty()) {
                        series6.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column + 6])));
                    }
                    if (arraySize > column + 9 && !response[column + 9].isEmpty()) {
                        series7.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column + 9])));
                    }
                    if (arraySize > column + 12 && !response[column + 12].isEmpty()) {
                        series8.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column + 12])));
                    }

                }
            }
            scene = new Scene(lineChart, 800, 600);
            lineChart.setAnimated(false);
            lineChart.getData().addAll(series4, series5, series6, series7, series8);
            scene.getStylesheets().add(getClass().getClassLoader().getResource(
                    "css/style.css").toExternalForm());
            stage.setScene(scene);
            System.out.println("Done Creating Stage");
        } else {
            String thisLine;
            String thisLine1;
            FileInputStream fis = new FileInputStream(responseTimeFile);
            DataInputStream myInput = new DataInputStream(fis);
            FileInputStream fis1 = new FileInputStream(benchmark);
            DataInputStream myInput1 = new DataInputStream(fis1);

            while ((thisLine1 = myInput1.readLine()) != null) {
                String[] benchmarkData = thisLine1.split(",");
                benchmarkMap.put(benchmarkData[0], Double.parseDouble(benchmarkData[column]));
            }

            while (((thisLine = myInput.readLine()) != null)) {
                System.out.println(thisLine);
                String[] response = thisLine.split(",");
                if (benchmarkMap.containsKey(response[0])) {
                    series.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column])));
                    series2.getData().add(new XYChart.Data(response[0], benchmarkMap.get(response[0])));
                }
            }

            scene = new Scene(lineChart, 800, 600);
            lineChart.setAnimated(false);
            lineChart.getData().addAll(series, series2);
            scene.getStylesheets().add(getClass().getClassLoader().getResource("css/style.css").toExternalForm());
            stage.setScene(scene);
            System.out.println("Done Creating Stage");
        }
        return stage;

    }

    public Stage createStage(int column, String headingName) throws IOException {
        reportingPath = applicationPropertiesConfig.getReportingPath();
        Stage stage = new Stage();
        stage.setTitle("Line chart");
        final CategoryAxis categoryAxis = new CategoryAxis();
        final NumberAxis yAxis = new NumberAxis();
        categoryAxis.setLabel("service names");
        final LineChart<String, Number> lineChart =
                new LineChart<String, Number>(categoryAxis, yAxis);
        yAxis.setLabel("Response time(ms)");
        lineChart.setTitle(headingName);
        lineChart.setId("chart-series-line");
        XYChart.Series series = new XYChart.Series();
        XYChart.Series series2 = new XYChart.Series();
        series.setName("Current results");
        series2.setName("Previous results");
        JSONFileRead jsonFileRead = new JSONFileRead();
        String jsonFilePath = jsonResultsNewFileName;
        jsonFileRead.readFile(jsonFilePath);
        String thisLine;
        String thisLine1;
        FileInputStream fis = new FileInputStream(responseTimeFile);
        DataInputStream myInput = new DataInputStream(fis);
        FileInputStream fis1 = new FileInputStream(benchmark);
        DataInputStream myInput1 = new DataInputStream(fis1);
        while ((thisLine1 = myInput1.readLine()) != null) {
            String[] benchmarkData = thisLine1.split(",");
            benchmarkMap.put(benchmarkData[0], Double.parseDouble(benchmarkData[column]));
        }
        while (((thisLine = myInput.readLine()) != null)) {
            System.out.println(thisLine);
            String[] response = thisLine.split(",");
            if (benchmarkMap.containsKey(response[0])) {
                series.getData().add(new XYChart.Data(response[0], Double.parseDouble(response[column])));
                series2.getData().add(new XYChart.Data(response[0], benchmarkMap.get(response[0])));
            }

        }
        scene = new Scene(lineChart, 800, 600);
        lineChart.setAnimated(false);
        lineChart.getData().addAll(series, series2);
        scene.getStylesheets().add(getClass().getClassLoader().getResource("css/style.css").toExternalForm());
        stage.setScene(scene);

        return stage;
    }

    public String getReportPath() throws IOException {
        BufferedReader reportingPath = new BufferedReader(new FileReader(
                applicationPropertiesConfig.getLineGraphDataPath() + "/path.txt"));
        String path = reportingPath.readLine();
        System.out.println("Report path is : " + path);
        return path;
    }
}