package statistics;

public final class  CostantValues {
    private final String AVERAGE ="meanResTime";
    private final String FIRST_PERCENTILE ="minResTime";
    private final String SECOND_PERCENTILE = "pct2ResTime";
}
