package statistics;

import jmeter.ApplicationPropertiesConfig;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static statistics.ConstantValues.*;

public class JSONFileRead {
    private ApplicationPropertiesConfig applicationPropertiesConfig = new ApplicationPropertiesConfig();
    public HashMap<String, HistoryData> historyDataHashMap = new HashMap<>();
    public HashMap<String, List<Double>> dataHashMap = new HashMap<>();
    public static String testName = "";

    @SuppressWarnings("unchecked")
    public void readFile(String pathToJSonFile) {
        JSONParser jsonParser = new JSONParser();

        try (FileReader reader = new FileReader(pathToJSonFile)) {
            Object obj = jsonParser.parse(reader);
            JSONObject statisticList = (JSONObject) obj;
            System.out.println(obj);
            parseStatisticObject(statisticList);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    private void parseStatisticObject(JSONObject statistic) throws IOException {
        FileWriter fileWriter = new FileWriter(applicationPropertiesConfig.getLineGraphDataPath() + "/ResponseTime.csv");
        String filepath = applicationPropertiesConfig.getLineGraphDataPath() + "/AllResponseTime.csv";
        File file = new File(filepath);
        if (!file.exists()) {
            file.createNewFile();
        }
        FileInputStream fis = new FileInputStream(filepath);
        FileWriter fileWriter1 = new FileWriter(file,true);
        BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
        BufferedWriter bufferedWriter1 = new BufferedWriter(fileWriter1);
        List<JSONObject> keys = getJSONOObjects(statistic);
        DataInputStream myInput = new DataInputStream(fis);
        String thisLine;
        while ((thisLine = myInput.readLine()) != null) {
            String[] data = thisLine.split(",");
            List<Double> list = new ArrayList<>();
            for (int index = 1; index < data.length; index++) {
                list.add(Double.parseDouble(data[index]));
            }
            dataHashMap.put(data[0], list);
        }

        myInput.close();
        fis.close();
        for (JSONObject object : keys) {
            Object keyName = (object.get(TRANSACTION_NAME));
            if (keyName.toString().equalsIgnoreCase("Total")) {
                continue;
            }
            JSONObject jsonObject = (JSONObject) statistic.get(keyName);
            Double averageResponseTime = (Double) jsonObject.get(AVERAGE);
            Double firstPercentile = (Double) jsonObject.get(FIRST_PERCENTILE);
            Double secondPercentile = (Double) jsonObject.get(SECOND_PERCENTILE);
            if(!(dataHashMap.containsKey(testName+"_"+keyName.toString()))){
                bufferedWriter1.write(testName+"_"+keyName.toString());
                bufferedWriter1.append(",");
                bufferedWriter1.append(averageResponseTime.toString());
                bufferedWriter1.append(",");
                bufferedWriter1.append(firstPercentile.toString());
                bufferedWriter1.append(",");
                bufferedWriter1.append(secondPercentile.toString());
                bufferedWriter1.append("\n");
            }
            HistoryData historyData = new HistoryData();
            System.out.println(object.get(TRANSACTION_NAME));
            bufferedWriter.write(keyName.toString());
            bufferedWriter.append(",");
            historyData.setAverage(averageResponseTime);
            bufferedWriter.append(averageResponseTime.toString());
            bufferedWriter.append(",");
            System.out.println(averageResponseTime);
            bufferedWriter.append(firstPercentile.toString());
            historyData.setFirstPercentile(firstPercentile);
            bufferedWriter.append(",");
            System.out.println(firstPercentile);
            bufferedWriter.append(secondPercentile.toString());
            historyData.setSecondPercentile(secondPercentile);
            System.out.println(secondPercentile);
            bufferedWriter.newLine();
            historyDataHashMap.put(keyName.toString(), historyData);
        }
        bufferedWriter.flush();
        bufferedWriter1.flush();
        bufferedWriter.close();
        bufferedWriter1.close();
    }

    public List<JSONObject> getJSONOObjects(JSONObject jsonObject) {
        List<JSONObject> objects = new ArrayList<>();
        jsonObject.keySet().forEach(keyString ->
        {
            JSONObject keyValue = (JSONObject) jsonObject.get(keyString);
            objects.add(keyValue);

        });
        return objects;
    }

    public void generateHistory(HashMap hashMap) throws IOException {
        File inputFile = new File(applicationPropertiesConfig.getLineGraphDataPath() + "/history.csv");
        {

        }
    }
}
