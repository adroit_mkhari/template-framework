package statistics;

public interface ConstantValues {
    String LINE_GRAPH_DATA="line_graph_data";
    String TRANSACTION_NAME = "transaction";
    String AVERAGE ="meanResTime";
    String FIRST_PERCENTILE ="pct1ResTime";
    String SECOND_PERCENTILE = "pct2ResTime";
}
