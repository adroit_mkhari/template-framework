package statistics;


public class HistoryData {
    private double average;
    private double firstPercentile;
    private double secondPercentile;

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public double getFirstPercentile() {
        return firstPercentile;
    }

    public void setFirstPercentile(double firstPercentile) {
        this.firstPercentile = firstPercentile;
    }

    public double getSecondPercentile() {
        return secondPercentile;
    }

    public void setSecondPercentile(double secondPercentile) {
        this.secondPercentile = secondPercentile;
    }


}
