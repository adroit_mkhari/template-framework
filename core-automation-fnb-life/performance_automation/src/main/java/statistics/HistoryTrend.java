package statistics;

import jmeter.ApplicationPropertiesConfig;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HistoryTrend {
    public HashMap<String, List<Double>> dataHashMap = new HashMap<>();
    private ApplicationPropertiesConfig applicationPropertiesConfig = new ApplicationPropertiesConfig();
    static boolean flag;
    public static HashMap<String, HistoryData> lastMap;

    public synchronized void generateHistoryData(HashMap<String, HistoryData> map) throws IOException {
        if (lastMap != null) {
            if (!(map.keySet().equals(lastMap.keySet()))) {
                flag = false;
            }
        }
        if (!flag) {
            lastMap = map;
            flag = true;
            FileInputStream fis = null;
            try {
                File file = new File(applicationPropertiesConfig.getLineGraphDataPath() + "/history.csv");
                if (file.exists()) {
                    fis = new FileInputStream(applicationPropertiesConfig.getLineGraphDataPath() + "/history.csv");
                } else {
                    file.createNewFile();
                    fis = new FileInputStream(applicationPropertiesConfig.getLineGraphDataPath() + "/history.csv");
                }

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            DataInputStream myInput = new DataInputStream(fis);
            String thisLine;
            while ((thisLine = myInput.readLine()) != null) {
                String[] data = thisLine.split(",");
                List<Double> list = new ArrayList<>();
                for (int index = 1; index < data.length; index++) {
                    list.add(Double.parseDouble(data[index]));
                }
                dataHashMap.put(data[0], list);
            }

            myInput.close();
            fis.close();

            FileWriter fileWriter = new FileWriter(applicationPropertiesConfig.getLineGraphDataPath()
                    + "/history.csv");
            BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            for (Map.Entry<String, HistoryData> entry : map.entrySet()) {
                List<Double> listOfPreviousData = dataHashMap.get(entry.getKey());
                if (listOfPreviousData != null) {
                    List<Double> list = new ArrayList<>();
                    list.add(entry.getValue().getAverage());
                    list.add(entry.getValue().getFirstPercentile());
                    list.add(entry.getValue().getSecondPercentile());
                    for (int index = 0; index < listOfPreviousData.size(); index++) {
                        list.add(listOfPreviousData.get(index));
                    }
                    dataHashMap.put(entry.getKey(), list);
                } else {
                    List<Double> list = new ArrayList<>();
                    list.add(entry.getValue().getAverage());
                    list.add(entry.getValue().getFirstPercentile());
                    list.add(entry.getValue().getSecondPercentile());
                    dataHashMap.put(entry.getKey(), list);
                }
            }
            for (Map.Entry<String, List<Double>> entry : dataHashMap.entrySet()) {
                if (!entry.getKey().isEmpty()) {
                    bufferedWriter.write(entry.getKey());
                    List<Double> list = entry.getValue();
                    int limit;
                    if (list.size() > 14) {
                        limit = 15;
                    } else {
                        limit = list.size();
                    }
                    for (int index = 0; index < limit; index++) {
                        bufferedWriter.write(',');
                        bufferedWriter.write(String.valueOf(list.get(index)));
                    }
                    bufferedWriter.newLine();
                }

            }
            bufferedWriter.close();
            fileWriter.close();
            dataHashMap.clear();
        }
    }
}