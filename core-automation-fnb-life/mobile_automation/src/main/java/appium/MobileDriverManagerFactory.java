package appium;

import appium.managers.AppiumDriverManager;
import appium.managers.MobileDriverManager;

public class MobileDriverManagerFactory {
    public static MobileDriverManager getDriverManager(MobileDriverType driverType) {
        MobileDriverManager driverManager;
        switch (driverType) {
            case APPIUM:
                driverManager = new AppiumDriverManager();
                break;
            case ANDROID:
                driverManager = new AppiumDriverManager();
                break;
            default:
                driverManager = new AppiumDriverManager();
        }
        return driverManager;
    }
}
