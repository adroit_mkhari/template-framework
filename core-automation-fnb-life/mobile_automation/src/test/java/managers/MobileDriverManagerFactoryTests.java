package managers;

import appium.MobileDriverManagerFactory;
import appium.MobileDriverType;
import appium.managers.MobileDriverManager;
import io.appium.java_client.AppiumDriver;
import org.junit.Assert;
import org.testng.annotations.Test;

public class MobileDriverManagerFactoryTests {
    @Test
    public void mobileDriverManagerFactoryAppiumTest() {
        MobileDriverManager driverManager = MobileDriverManagerFactory.getDriverManager(MobileDriverType.APPIUM);
        AppiumDriver appiumDriver = driverManager.getAppiumDriver();
        Assert.assertNotNull(appiumDriver);
    }
}
