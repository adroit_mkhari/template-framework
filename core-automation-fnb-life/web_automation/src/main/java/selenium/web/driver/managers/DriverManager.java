package selenium.web.driver.managers;

import files.FilePropertiesConfig;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.util.Properties;

public abstract class DriverManager {
    private Logger log  = LogManager.getLogger(DriverManager.class);
    protected WebDriver driver;
    protected String version;
    protected DesiredCapabilities desiredCapabilities;

    public DriverManager() {
        try {
            FilePropertiesConfig filePropertiesConfig = new FilePropertiesConfig();
            filePropertiesConfig.loadProperties();
            Properties properties = filePropertiesConfig.getProperties();
            version = properties.getProperty("CHROME_DRIVER_VERSION");
        } catch (Exception e) {
            log.error("Error while loading CHROME_DRIVER_VERSION");
        }
    }

    protected abstract void createDriver();
    protected abstract void createDriver(boolean headless);

    public WebDriver getWebDriver(boolean headless) {
        if (driver == null) {
            createDriver(headless);
        }
        return driver;
    }

    public void quitWebDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}
