<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>core-automation-fnb-life</groupId>
    <artifactId>core-automation-fnb-life</artifactId>
    <packaging>pom</packaging>
    <version>1.0-SNAPSHOT</version>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.8.1</version>
                <configuration>
                    <source>8</source>
                    <target>8</target>
                </configuration>
            </plugin>

            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <version>2.6</version>
                <configuration>
                    <archive>
                        <manifest>
                            <addClasspath>true</addClasspath>
                        </manifest>
                    </archive>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>

            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>2.22.2</version>
                <configuration>
                    <skipTests>true</skipTests>
                    <testFailureIgnore>false</testFailureIgnore>
                    <!--<argLine>
                        -javaagent:"${settings.localRepository}/org/aspectj/aspectjweaver/${aspectj.version}/aspectjweaver-${aspectj.version}.jar"
                        -Dcucumber.options="&#45;&#45;plugin io.qameta.allure.cucumber4jvm.AllureCucumber4Jvm"
                        -Dcucumber.options="&#45;&#45;plugin ru.yandex.qatools.allure.cucumberjvm.AllureReporter"
                    </argLine>-->
                    <systemProperties>
                        <property>
                            <name>junit.jupiter.extensions.autodetection.enabled</name>
                            <value>true</value>
                        </property>
                        <property>
                            <name>maven.junit.sysproperties</name>
                            <value>log4j.configuration</value>
                        </property>
                        <property>
                            <name>listener</name>
                            <value>io.qameta.allure.junit4.AllureJunit4</value>
                        </property>
                        <property>
                            <name>allure.results.directory</name>
                            <value>${project.build.directory}/allure-results</value>
                        </property>
                    </systemProperties>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>org.junit.platform</groupId>
                        <artifactId>junit-platform-surefire-provider</artifactId>
                        <version>1.3.2</version>
                    </dependency>

                    <dependency>
                        <groupId>org.aspectj</groupId>
                        <artifactId>aspectjweaver</artifactId>
                        <version>${aspectj.version}</version>
                    </dependency>

                    <dependency>
                        <groupId>org.apache.maven.surefire</groupId>
                        <artifactId>surefire-junit47</artifactId>
                        <version>2.14.1</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <groupId>io.qameta.allure</groupId>
                <artifactId>allure-maven</artifactId>
                <version>2.10.0</version>
                <configuration>
                    <outputDirectory>${project.build.directory}</outputDirectory>
                    <resultsDirectory>${basedir}/allure-results</resultsDirectory>
                    <reportDirectory>${project.build.directory}/allure-report</reportDirectory>
                    <propertiesFilePath>${basedir}/src/test/resources/allure.properties</propertiesFilePath>
                </configuration>
            </plugin>
        </plugins>

        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.springframework.boot</groupId>
                    <artifactId>spring-boot-maven-plugin</artifactId>
                </plugin>
            </plugins>
        </pluginManagement>
    </build>

    <modules>
        <module>web_automation</module>
        <module>mobile_automation</module>
        <module>performance_automation</module>
        <module>communication</module>
        <module>windows_automation</module>
        <module>web_service_automation</module>
        <module>rsa_id_number_generator</module>
    </modules>

    <properties>
        <camel.version>2.14.4</camel.version>
        <commons.dbcp.version>1.4</commons.dbcp.version>
        <commons.io.version>2.4</commons.io.version>
        <commons.lang.version>2.6</commons.lang.version>
        <commons.logging.version>1.1.2</commons.logging.version>
        <gts.testauto-ui-automation>1.0.0-SNAPSHOT</gts.testauto-ui-automation>
        <hamcrest.version>1.3</hamcrest.version>
        <ims.connect.version>1.0.6</ims.connect.version>
        <jackson-databind.version>2.2.0</jackson-databind.version>
        <jagacy-version>5.0.1</jagacy-version>
        <javamail.version>1.4.3</javamail.version>
        <javax.persistence.version>1.0.2</javax.persistence.version>
        <javax.resource.connector>1.5</javax.resource.connector>
        <jmock.version>2.6.0</jmock.version>
        <jms.version>1.4.0</jms.version>
        <jrebel.maven.version>1.1.1</jrebel.maven.version>
        <jsch.version>0.1.54</jsch.version>
        <json-api.version>20140107</json-api.version>
        <json-path.version>0.8.1</json-path.version>
        <jta.version>1.1</jta.version>
        <junit.version>4.12</junit.version>
        <log4j.version>1.2.17</log4j.version>
        <org.codehaus.mojo.version>1.0-alpha-2</org.codehaus.mojo.version>
        <pdfbox.version>2.0.9</pdfbox.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <slf4j.version>1.7.5</slf4j.version>
        <sonar.skippedModules>mft-api</sonar.skippedModules>
        <sonar.exclusions>src/main/generated/*</sonar.exclusions>
        <sonar.jacoco.excludes>src/main/generated/*</sonar.jacoco.excludes>
        <spring.version>3.2.5.RELEASE</spring.version>
        <spring-ws-version>2.1.2.RELEASE</spring-ws-version>
        <testauto-framework-version>1.2.30-SNAPSHOT</testauto-framework-version>
        <tibco.jms.version>4.4.3</tibco.jms.version>
        <xmlunit.version>1.3</xmlunit.version>
        <aspectj.version>1.9.6</aspectj.version>
    </properties>

    <dependencies>
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <version>5.8.0-M1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.8.0-M1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-maven</artifactId>
            <version>2.10.0</version>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-testng</artifactId>
            <version>2.13.6</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit4</artifactId>
            <version>2.13.6</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-junit5</artifactId>
            <version>2.13.6</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-cucumber4-jvm</artifactId>
            <version>2.13.6</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>io.qameta.allure</groupId>
            <artifactId>allure-cucumber-jvm</artifactId>
            <version>2.13.6</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>ru.yandex.qatools.allure</groupId>
            <artifactId>allure-cucumber-jvm-adaptor</artifactId>
            <version>1.6.4</version>
        </dependency>

        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-surefire-provider</artifactId>
            <version>1.3.2</version>
        </dependency>

        <dependency>
            <groupId>org.testng</groupId>
            <artifactId>testng</artifactId>
            <version>7.0.0</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-core</artifactId>
            <version>2.11.2</version>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-java</artifactId>
            <version>1.2.5</version>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-jvm</artifactId>
            <version>1.2.5</version>
            <type>pom</type>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-jvm-deps</artifactId>
            <version>1.0.5</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-junit</artifactId>
            <version>1.2.5</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-testng</artifactId>
            <version>1.2.5</version>
        </dependency>

        <dependency>
            <groupId>info.cukes</groupId>
            <artifactId>cucumber-picocontainer</artifactId>
            <version>1.2.5</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>com.beust</groupId>
            <artifactId>jcommander</artifactId>
            <version>1.72</version>
        </dependency>

        <dependency>
            <groupId>net.masterthought</groupId>
            <artifactId>cucumber-reporting</artifactId>
            <version>4.7.0</version>
        </dependency>

        <dependency>
            <groupId>com.fasterxml.jackson.core</groupId>
            <artifactId>jackson-databind</artifactId>
        </dependency>

        <dependency>
            <groupId>com.relevantcodes</groupId>
            <artifactId>extentreports</artifactId>
        </dependency>

         <dependency>
            <groupId>commons-dbcp</groupId>
            <artifactId>commons-dbcp</artifactId>
        </dependency>

        <dependency>
            <groupId>com.jcraft</groupId>
            <artifactId>jsch</artifactId>
        </dependency>

        <dependency>
            <groupId>log4j</groupId>
            <artifactId>log4j</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.pdfbox</groupId>
            <artifactId>pdfbox</artifactId>
            <version>${pdfbox.version}</version>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-core</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-jetty</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-jms</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-netty</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-spring</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-test</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-test-spring</artifactId>
        </dependency>

        <dependency>
            <groupId>org.apache.camel</groupId>
            <artifactId>camel-velocity</artifactId>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-log4j12</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-aop</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-beans</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context-support</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jdbc</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-jms</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-oxm</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-test</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-tx</artifactId>
        </dependency>

        <dependency>
            <groupId>org.springframework.ws</groupId>
            <artifactId>spring-xml</artifactId>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>xmlunit</groupId>
            <artifactId>xmlunit</artifactId>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
        </dependency>

        <dependency>
            <groupId>org.mockito</groupId>
            <artifactId>mockito-all</artifactId>
        </dependency>

        <dependency>
            <groupId>de.redsix</groupId>
            <artifactId>pdfcompare</artifactId>
            <version>1.1.58</version>
        </dependency>

        <dependency>
            <groupId>com.testautomationguru.pdfutil</groupId>
            <artifactId>pdf-util</artifactId>
            <version>0.0.3</version>
        </dependency>

        <dependency>
            <groupId>com.sikulix</groupId>
            <artifactId>sikulixlibswin</artifactId>
            <version>1.1.1</version>
        </dependency>

        <dependency>
            <groupId>com.sikulix</groupId>
            <artifactId>sikulixlibslux</artifactId>
            <version>1.1.1</version>
        </dependency>

        <dependency>
            <groupId>com.sikulix</groupId>
            <artifactId>sikulixlibsmac</artifactId>
            <version>1.1.1</version>
        </dependency>

        <dependency>
            <groupId>com.sikulix</groupId>
            <artifactId>sikulixapi</artifactId>
            <version>1.1.2</version>
            <exclusions>
                <exclusion>
                    <groupId>com.github.vidstige</groupId>
                    <artifactId>jadb</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>com.sikulix</groupId>
                    <artifactId>sikulix2tigervnc</artifactId>
                </exclusion>
            </exclusions>
        </dependency>
    </dependencies>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.fasterxml.jackson.core</groupId>
                <artifactId>jackson-databind</artifactId>
                <version>${jackson-databind.version}</version>
            </dependency>

            <dependency>
                <groupId>com.relevantcodes</groupId>
                <artifactId>extentreports</artifactId>
                <version>2.41.2</version>
            </dependency>

            <dependency>
                <groupId>org.mockito</groupId>
                <artifactId>mockito-all</artifactId>
                <version>1.9.5</version>
                <scope>test</scope>
            </dependency>

            <dependency>
                <groupId>com.jayway.jsonpath</groupId>
                <artifactId>json-path-assert</artifactId>
                <version>${json-path.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>com.jcraft</groupId>
                <artifactId>jsch</artifactId>
                <version>${jsch.version}</version>
            </dependency>

            <dependency>
                <groupId>com.sun.xml.messaging.saaj</groupId>
                <artifactId>saaj-impl</artifactId>
                <version>1.3.2</version>
                <exclusions>
                    <exclusion>
                        <artifactId>activation</artifactId>
                        <groupId>javax.activation</groupId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>com.tibco.adapter.adswift</groupId>
                <artifactId>root</artifactId>
                <version>5.12</version>
                <type>pom</type>
            </dependency>

            <dependency>
                <groupId>com.tibco.tra</groupId>
                <artifactId>TIBCOjms</artifactId>
                <version>${tibco.jms.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-dbcp</groupId>
                <artifactId>commons-dbcp</artifactId>
                <version>${commons.dbcp.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>commons-io</groupId>
                <artifactId>commons-io</artifactId>
                <version>${commons.io.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-lang</groupId>
                <artifactId>commons-lang</artifactId>
                <version>${commons.lang.version}</version>
            </dependency>

            <dependency>
                <groupId>commons-logging</groupId>
                <artifactId>commons-logging</artifactId>
                <version>${commons.logging.version}</version>
            </dependency>

            <dependency>
                <groupId>javax.mail</groupId>
                <artifactId>mail</artifactId>
                <version>${javamail.version}</version>
            </dependency>

            <dependency>
                <groupId>javax.persistence</groupId>
                <artifactId>persistence-api</artifactId>
                <version>${javax.persistence.version}</version>
            </dependency>

            <dependency>
                <groupId>javax.resource</groupId>
                <artifactId>connector-api</artifactId>
                <version>${javax.resource.connector}</version>
            </dependency>

            <dependency>
                <groupId>javax.xml.soap</groupId>
                <artifactId>saaj-api</artifactId>
                <version>1.3.2</version>
                <exclusions>
                    <exclusion>
                        <artifactId>activation</artifactId>
                        <groupId>javax.activation</groupId>
                    </exclusion>
                </exclusions>
            </dependency>

            <dependency>
                <groupId>junit</groupId>
                <artifactId>junit</artifactId>
                <version>${junit.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>log4j</groupId>
                <artifactId>log4j</artifactId>
                <version>${log4j.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.pdfbox</groupId>
                <artifactId>pdfbox</artifactId>
                <version>${pdfbox.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-core</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-jetty</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-jms</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-netty</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-spring</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-test</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-test-spring</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.apache.camel</groupId>
                <artifactId>camel-velocity</artifactId>
                <scope>compile</scope>
                <version>${camel.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-all</artifactId>
                <version>${hamcrest.version}</version>
            </dependency>

            <dependency>
                <groupId>org.hamcrest</groupId>
                <artifactId>hamcrest-core</artifactId>
                <version>${hamcrest.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.jmock</groupId>
                <artifactId>jmock</artifactId>
                <version>${jmock.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.jmock</groupId>
                <artifactId>jmock-junit4</artifactId>
                <version>${jmock.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.jmock</groupId>
                <artifactId>jmock-legacy</artifactId>
                <version>${jmock.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.json</groupId>
                <artifactId>json</artifactId>
                <version>${json-api.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>jetty</artifactId>
                <version>${jetty.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>jetty-management</artifactId>
                <version>${jetty.version}</version>
            </dependency>

            <dependency>
                <groupId>org.mortbay.jetty</groupId>
                <artifactId>jetty-util</artifactId>
                <version>${jetty.version}</version>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-api</artifactId>
                <version>${slf4j.version}</version>
            </dependency>

            <dependency>
                <groupId>org.slf4j</groupId>
                <artifactId>slf4j-log4j12</artifactId>
                <version>${slf4j.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-aop</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-beans</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-context-support</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jdbc</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-jms</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-oxm</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-test</artifactId>
                <version>${spring.version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>org.springframework</groupId>
                <artifactId>spring-tx</artifactId>
                <version>${spring.version}</version>
            </dependency>

            <dependency>
                <groupId>org.springframework.ws</groupId>
                <artifactId>spring-xml</artifactId>
                <version>${spring-ws-version}</version>
                <scope>compile</scope>
            </dependency>

            <dependency>
                <groupId>xmlunit</groupId>
                <artifactId>xmlunit</artifactId>
                <version>${xmlunit.version}</version>
            </dependency>
        </dependencies>
    </dependencyManagement>

</project>