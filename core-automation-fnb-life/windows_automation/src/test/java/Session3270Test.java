import com.jagacy.Field;
import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import gts.automation.terminal._3270.Hogan3270Exception;
import gts.testauto.base.*;
import gts.testauto.flow.Steps;
import gts.testauto.interaction.base.Output;
import gts.testauto.text.Resource;
import org.apache.log4j.Logger;
import org.junit.Test;

import java.io.IOException;

import static gts.testauto.flow.Steps._set;
import static org.junit.Assert.assertEquals;

public class Session3270Test extends LifeBase3270FunctionalTest {
    static final Logger LOG = Logger.getLogger(Session3270Test.class);

    private static int counter = 0;
    @Test
    public void verifyWarehousedTest() throws Exception {
        Life3270Session hogan3270Session = getHogan3270Session();
        Session3270 session3270 = hogan3270Session.getSession3270();

        // hogan3270Session.inputValue(18, 022, "FNBTTEST5");
        // hogan3270Session.inputValue(20, 022, "FNBTTEST5");
        // hogan3270Session.sendKey(Key.ENTER);

        hogan3270Session.inputValueAtFirstCursor("FNBTTEST5");
        hogan3270Session.sendKey(Key.TAB);
        session3270.writeString("FNBTTEST5");

        hogan3270Session.logScreen("After Screen");
        hogan3270Session.sendKey(Key.ENTER);
        closeSession();
    }

    private Resource<Output> loginInput() {
        return () -> {
            Output output = new Output();
            output.add("Operator Identification", get("3270.user"));
            output.add("Password", get("3270.password"));
            return output;
        };
    }

    private Resource<Output> inQ1Input() {
        Resource<Output> outputResource = new Resource<Output>() {
            @Override
            public Output getContent() {
                Output output = new Output();
                output.add("ACCOUNT NUMBER :", getProperty("AccountNumber"));
                output.add("CHANNEL ID :", getProperty("ChannelID"));
                output.add("ENTER BATCH ID :", getProperty("BatchID"));
                output.add("PRODUCT    :", getProperty("Product"));
                return output;
            }
        };
        return outputResource;
    }

    public TestStep verifyResult() {

        return Steps._do("Verify Results", () -> {
            try {
                String result = getSession().getText(22, 21, 22, 80);
                assertEquals("99 BATCH COMPLETE", result);
                set("propertyForReport", result);
            } catch (Hogan3270Exception e) {
                LOG.error(e.getMessage(), e);
                fail("Problem occurred while reading from the screen", e);
            }
        });
    }

    @Override
    public String getReportFolder() {
        return "3270Sessions";
    }

    @Override
    public String getReportName() {
        return get("report_name");
    }

    @Override
    public String provideOrderOfPropertiesForReport() {
        return "Test-ID," +
                "Test Result," +
                "propertyForReport," +
                "key2";
    }
}
