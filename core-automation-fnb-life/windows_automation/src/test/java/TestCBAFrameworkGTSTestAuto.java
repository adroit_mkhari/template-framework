import gts.testauto.base.BaseFunctionalTest;
import gts.testauto.base.RichAssertionError;
import gts.testauto.base.TestStep;
import gts.testauto.flow.Steps;
import gts.testauto.flow.TestExecution;
import gts.testauto.flow.TestExecutionTestInterceptor;
import gts.testauto.text.ScenarioFileResource;
import gts.testauto.text.TextSupport;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import static gts.testauto.flow.Steps.*;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

public class TestCBAFrameworkGTSTestAuto extends BaseFunctionalTest {
    private ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();

    @Before
    public void setMeUp() {
        byteArrayOutputStream = new ByteArrayOutputStream();
        setCompletedFlows("");
        setInitiatedFlows("");
    }

    @Test
    public void testNoFlowName() throws Exception {



        try {
            setFlow("", getTextSupport());
            Assert.fail("Should have gotten an exception");
        } catch (IllegalArgumentException e) {
        } catch (Throwable t) {
            Assert.fail("Should have gotten an IllegalArgumentException!");
        }

    }

    @Test
    public void testFlowTest() throws Exception {

        boolean[] failOnCompletedFlows = new boolean[] { false/*, true*/};
        boolean[] causeDeliberateFailures = new boolean[] { false /*, true */};
        boolean[] forceAltFlowExecutions = new boolean[] { /*false, */true };
        boolean[] allowAltFlowExecutions = new boolean[] { false/*, true */};

        for (boolean failOnCompletedFlow : failOnCompletedFlows) {
            for (boolean causeDeliberateFailure : causeDeliberateFailures) {
                for (boolean forceAltFlowExecution : forceAltFlowExecutions) {
                    for (boolean allowAltFlowExecution : allowAltFlowExecutions) {

                        genericFlowTest(failOnCompletedFlow, causeDeliberateFailure, forceAltFlowExecution,
                                allowAltFlowExecution);

                        byteArrayOutputStream.reset();
                        super.baseFunctionTestTearDown();
                        super.baseFunctionTestSetUp();
                    }
                }
            }
        }

    }

    private void genericFlowTest(boolean failOnCompletedFlow,
                                 boolean causeDeliberateFailure,
                                 boolean forceAltFlowExecution,
                                 boolean allowAltFlowExecution) throws Exception {

        String combinationString = String.format("Test combination : failOnCompletedFlow = %s"
                        + ",causeDeliberateFailure = %s,"
                        + "forceAltFlowExecution = %s,"
                        + "allowAltFlowExecution = %s",
                failOnCompletedFlow,
                causeDeliberateFailure,
                forceAltFlowExecution,
                allowAltFlowExecution);

        setTextSupport(new TextSupport());
        setScenario("TestFlow");


        List<String> failCheckList = new ArrayList<String>();

        runTestFlow(failOnCompletedFlow, causeDeliberateFailure, forceAltFlowExecution, allowAltFlowExecution, failCheckList);

        checkIterateOnFailureActionsExecuted(failOnCompletedFlow, causeDeliberateFailure, forceAltFlowExecution,
                allowAltFlowExecution, failCheckList, combinationString);

        checkReportContents(combinationString);

        checkLastFailure(failOnCompletedFlow, causeDeliberateFailure, forceAltFlowExecution, allowAltFlowExecution,
                combinationString);

        if (!causeDeliberateFailure && !failOnCompletedFlow && forceAltFlowExecution && allowAltFlowExecution) {
            //check alt flow executed
            assertThat(combinationString + " - Alternative flow was not executed!",
                    get("alt-flow-executed"),
                    equalTo("true"));
        }

    }

    private void runTestFlow(boolean failOnCompletedFlow, boolean causeDeliberateFailure, boolean forceAltFlowExecution, boolean allowAltFlowExecution, List<String> failCheckList) {

        try {
            test("use-case-normal-flow").is(

                    iterate(

                            _do("force flow conditions",
                                    () ->
                                            forceFlowConditions(failOnCompletedFlow, causeDeliberateFailure, forceAltFlowExecution, allowAltFlowExecution)),

                            normalFlow(causeDeliberateFailure),
                            Steps._do("", () -> _set("key", (String) null))

                    ).onFailure(_set("TEST_STATUS", "FAILED"),
                            _do("Add to list",
                                    () ->
                                            failCheckList.add("fail")))
            ).run();

            if ("FAILED".equals(get("expectedStatus"))) {
                Assert.fail("This run should have failed!");
            }
        } catch (AssertionError e) {
            if (!"FAILED".equals(get("expectedStatus"))) {
                Assert.fail("This run should not have failed!");
            }
        }
    }


    private void checkIterateOnFailureActionsExecuted(boolean failOnCompletedFlow,
                                                      boolean causeDeliberateFailure,
                                                      boolean forceAltFlowExecution,
                                                      boolean allowAltFlowExecution,
                                                      List<String> failCheckList,
                                                      String combinationString) {

        if (failOnCompletedFlow || causeDeliberateFailure || forceAltFlowExecution && !allowAltFlowExecution) {
            assertThat(combinationString + " - Fail check list does not indicate the right number of onFailure executions!",
                    failCheckList.size(),
                    equalTo(2));
        }
    }


    private void checkReportContents(String combinationString) {
        // assert report contents
        String content = fileResource("expectedreport.csv").getContent().replace("\r\n", System.lineSeparator())
                .replace("\r", System.lineSeparator())
                .replace("\n", System.lineSeparator()).trim();

        assertThat(String.format("Combination: %s - \r\nGenerated report not equal to expected!",
                combinationString),
                byteArrayOutputStream.toString().replace("\r\n", System.lineSeparator())
                        .replace("\r", System.lineSeparator())
                        .replace("\n", System.lineSeparator()).trim(),
                equalTo(content));
    }


    private void checkLastFailure(boolean failOnCompletedFlow, boolean causeDeliberateFailure,
                                  boolean forceAltFlowExecution, boolean allowAltFlowExecution, String combinationString) {
        //Assert last failure recorded
        if (failOnCompletedFlow || forceAltFlowExecution && !allowAltFlowExecution || causeDeliberateFailure) {
            AssertionError lastFailure = TestExecution.getInstance().getLastFailure();

            assertThat(String.format("Combination: %s - \r\nLast failure must be set on test execution!",
                    combinationString),
                    lastFailure != null,
                    equalTo(true));
        }
        if (failOnCompletedFlow && !forceAltFlowExecution && !causeDeliberateFailure) {
            RichAssertionError lastFailure = getRichAssertionError(combinationString);

            assertThat(String.format("Combination: %s - \r\nLast failure code not correct!",
                    combinationString),
                    lastFailure.getCode(),
                    equalTo(TestExecutionTestInterceptor.CODE_UNEXPECTED_FLOW_COMPLETED));
        }
        if (forceAltFlowExecution && !allowAltFlowExecution && !causeDeliberateFailure) {
            RichAssertionError lastFailure = getRichAssertionError(combinationString);

            assertThat(String.format("Combination: %s - \r\nLast failure code not correct!",
                    combinationString),
                    lastFailure.getCode(),
                    equalTo(TestExecutionTestInterceptor.CODE_UNEXPECTED_FLOW_INITIATED));

        }
    }


    private RichAssertionError getRichAssertionError(String combinationString) {
        AssertionError _lastFailure = TestExecution.getInstance().getLastFailure();
        RichAssertionError lastFailure = null;

        if (_lastFailure instanceof RichAssertionError) {
            lastFailure = (RichAssertionError) _lastFailure;

        } else {
            if (_lastFailure.getCause() instanceof RichAssertionError) {
                lastFailure = (RichAssertionError) _lastFailure.getCause();
            }
        }
        if( lastFailure == null) {
            Assert.fail(combinationString + " - Expected an instance of RichAssertionError for last failure, did not get one!");
        }

        return lastFailure;
    }

    private void forceFlowConditions(boolean failOnCompletedFlow, boolean causeDeliberateFailure,
                                     boolean forceAltFlowExecution,
                                     boolean allowAltFlowExecution) {
        set("expectedStatus",
                (failOnCompletedFlow || causeDeliberateFailure || (forceAltFlowExecution && !allowAltFlowExecution))
                        ? "FAILED" : "PASSED");

        if ((!failOnCompletedFlow && !forceAltFlowExecution)) {
            //mustn't fail on normal flow completion and alternate flow not acceptable
            setCompletedFlows("normal");
        } else
        if (!failOnCompletedFlow && forceAltFlowExecution && allowAltFlowExecution) {
            //normal flow completing is acceptable and alternate
            setCompletedFlows("normal, alt-flow-receive-mismatch");

        } else {
            //set invalid completed flow so 'normal' will be considered an invalid flow
            setCompletedFlows("bogus1");
        }

        if (forceAltFlowExecution) {
            //corrupt received result so alternative flow will be forced
            set("actualReceiveResult", "blahhrb");
        }

        if (allowAltFlowExecution) {
            setInitiatedFlows("normal, alt-flow-receive-mismatch");
        } else {
            setInitiatedFlows("normal");
        }

    }

    private ScenarioFileResource fileResource(String reportName) {
        ScenarioFileResource scenarioFileResource = scenarioFileResource(reportName);
        scenarioFileResource.setTextProvider(getTextSupport());

        return scenarioFileResource;
    }

    private TestStep normalFlow(boolean causeFailure) {

        return setFlow("normal").is(

                sendStep(),

                Steps._do("Cause a failure!",
                        () -> {
                            if (causeFailure) Assert.fail("Causing deliberate failure!");
                        }),

                checkSendStepSetValueCorrectly(),

                receiveStep()
                        .onFailure(responseAlternateFlows()),

                _set("TEST_STATUS", "PASSED")
        );
    }

    private TestStep checkSendStepSetValueCorrectly() {
        return Steps._do("check valueSelected",
                () -> assertThat("value selected not equal to expected!", get("requestSent"), equalTo("request1")));
    }

    private TestStep responseAlternateFlows() {
        return setFlow("alt-flow-receive-mismatch").is(

                echo("Response alternate flow section executed!"),
                _set("alt-flow-executed", "true")
        );
    }

    private TestStep receiveStep() {
        return Steps._do("Receive checking result!",
                () -> assertThat(get("actualReceiveResult"), equalTo(get("expectedReceiveResult"))));
    }

    private TestStep sendStep() {
        return new TestStep("send to queue").is(

                ifSwitchHasValue("${requestType}", "type1")
                        .then(
                                _echo("Value 1"),
                                _set("requestSent", "request1")),

                ifSwitchHasValue("${requestType}", "type2")
                        .then(
                                _echo("Value 2"),
                                _set("requestSent", "request2"))
        );
    }

    @Override
    public String getReportFolder() {
        return "/tmp";
    }

    @Override
    public String getReportName() {
        return "test.csv";
    }

    @Override
    public Properties provideReportData() {

        return multipleReportRows(getTextSupport().stack(), getTextSupport().stack());
    }

    @Override
    public String provideOrderOfPropertiesForReport() {
        return "TEST_STATUS";
    }

    @Override
    public OutputStream provideAlternateReportStream(String absoluteReportCreation) {

        return byteArrayOutputStream;
    }

}
