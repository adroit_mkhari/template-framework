import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import gts.automation.terminal._3270.Hogan3270Exception;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.Properties;

public class JagacyTest {
    Session3270 session3270;
    @Test
    public void testJagacySession() {
        try {
            Properties properties = new Properties();

            InputStream inputStream = new FileInputStream(new File("C:\\adroit\\projects\\fnb_life\\core-automation-fnb-life\\windows_automation\\src\\main\\resources\\jagacy.properties"));
            properties.load(inputStream);
            inputStream.close();

            System.setProperty("jagacy.host", (String) properties.get("3270.host"));
            System.setProperty("jagacy.port", (String) properties.get("3270.port"));
            this.session3270 = new Session3270("C:\\adroit\\projects\\fnb_life\\core-automation-fnb-life\\windows_automation\\src\\main\\resources\\jagacy.properties");

            try {
                this.session3270.open();
                // TODO: Write some code
                String[] screen = this.session3270.readScreen();
                this.session3270.close();
            } catch (JagacyException var4) {
                this.session3270.close();
                this.session3270.open();
            }

        } catch (Exception var5) {
            var5.printStackTrace();
        }
    }
}
