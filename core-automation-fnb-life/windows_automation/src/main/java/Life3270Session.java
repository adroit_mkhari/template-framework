//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import com.jagacy.Field;
import com.jagacy.Key;
import com.jagacy.Session3270;
import com.jagacy.util.JagacyException;
import com.jagacy.util.JagacyProperties;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import gts.automation.terminal._3270.Hogan3270Exception;
import gts.automation.terminal._3270.ScreenNotFoundException;
import gts.automation.terminal._3270.ScreenTraceLog;
import org.apache.log4j.Logger;

public class Life3270Session {
    private static final Logger LOG = Logger.getLogger(Life3270Session.class);
    private Field[] fields;
    private boolean logScreenTrace;
    private boolean logInputTrace;
    private boolean logSendKeyTrace;
    private int waitForChangeTimeout;
    private int waitForUnlockTimeout;
    private int waitForIntermediateTimeout;
    private String folderName = "";
    private File logFile;
    private Session3270 session3270;

    private Life3270Session(String configFile) throws Hogan3270Exception, IOException {
        this.setJagacyCommonProperties(configFile);
        this.verifyScreen((String)null);
    }

    public static Life3270Session newSession(String configFile) throws Hogan3270Exception, IOException {
        return new Life3270Session(configFile);
    }

    private void setJagacyCommonProperties(String configFile) throws Hogan3270Exception {
        try {
            JagacyProperties jagacyProperties = new JagacyProperties(configFile);
            System.setProperty("jagacy.host", jagacyProperties.get("3270.host"));
            System.setProperty("jagacy.port", jagacyProperties.get("3270.port"));
            System.setProperty("jagacy.ssl", jagacyProperties.get("3270.ssl"));
            this.setWaitForChangeTimeout(jagacyProperties.getTimeout("waitForChangeTimeout"));
            this.setWaitForUnlockTimeout(jagacyProperties.getTimeout("waitForUnlockTimeout"));
            this.setWaitForIntermediateTimeout(jagacyProperties.getTimeout("waitForIntermediateTimeout"));
            this.setLogScreenTrace(jagacyProperties.getBoolean("logScreenTrace"));
            this.setLogInputTrace(jagacyProperties.getBoolean("logInputTrace"));
            this.setLogSendKeyTrace(jagacyProperties.getBoolean("logSendKeyTrace"));
            this.session3270 = new Session3270(configFile);

            try {
                this.session3270.open();
            } catch (JagacyException var4) {
                this.session3270.close();
                this.session3270.open();
            }

        } catch (JagacyException var5) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var5);
        }
    }

    public Session3270 getSession3270() {
        return session3270;
    }

    public boolean verifyScreen(String screenName) throws Hogan3270Exception, IOException {
        return this.verifyScreen(screenName, true);
    }

    public boolean verifyScreen(String screenName, boolean required) throws Hogan3270Exception, IOException {
        try {
            return this.screenHndl(screenName, required);
        } catch (Hogan3270Exception var4) {
            // TODO
            // throw Hogan3270Exception.createHogan3270Exception(this, screenName, var4);
        }
        return false;
    }

    private boolean screenHndl(String screenName, boolean requiredScreen) throws Hogan3270Exception, IOException {
        try {
            label52:
            while(true) {
                if (!this.session3270.waitForChange(this.waitForIntermediateTimeout)) {
                    if (this.session3270.waitForUnlock(this.waitForUnlockTimeout)) {
                        if (screenName != null) {
                            String[] var3 = this.session3270.readScreen();
                            int var4 = var3.length;

                            for(int var5 = 0; var5 < var4; ++var5) {
                                String row = var3[var5];
                                if (row.contains(screenName)) {
                                    this.logScreen(screenName);
                                    return true;
                                }
                            }

                            if (requiredScreen) {
                                if (this.session3270.waitForChange(this.waitForChangeTimeout)) {
                                    continue;
                                }

                                this.logScreen((String)null);
                                throw new ScreenNotFoundException(screenName);
                            }

                            if (this.session3270.waitForChange(this.waitForIntermediateTimeout)) {
                                continue;
                            }

                            LOG.info(screenName + "SCREEN NOT FOUND, BUT IS NOT REQUIRED");
                            return false;
                        }

                        do {
                            if (this.session3270.waitForChange(this.waitForIntermediateTimeout)) {
                                continue label52;
                            }
                        } while(this.session3270.waitForChange(this.waitForIntermediateTimeout));

                        return true;
                    }

                    this.logScreen((String)null);
                    throw new Hogan3270Exception("CONNECTION TIMED OUT DUE TO LOCKED STATUS");
                }
            }
        } catch (JagacyException var7) {
            this.logScreen((String)null);
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var7);
        }
    }

    public int findFieldNumber(String fieldValue) throws Hogan3270Exception {
        return this.findFieldNumber(fieldValue, 0);
    }

    public int findFieldNumber(String fieldValue, int sequenceNumber) throws Hogan3270Exception {
        List<Integer> fieldPostion = new ArrayList();
        int fieldNum = -1;

        try {
            this.fields = this.getFields();
            Field[] var5 = this.fields;
            int var6 = var5.length;

            for(int var7 = 0; var7 < var6; ++var7) {
                Field field = var5[var7];
                if (field.getValue().trim().contains(fieldValue)) {
                    fieldPostion.add(field.getFieldNumber());
                }
            }

            if (fieldPostion.size() > 0) {
                fieldNum = (Integer)fieldPostion.get(sequenceNumber);
            }

            return fieldNum;
        } catch (JagacyException var9) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var9);
        }
    }

    public String findFieldValue(int fieldNumber) throws Hogan3270Exception {
        try {
            return this.getFields()[fieldNumber - 1].getValue().trim();
        } catch (JagacyException var3) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var3);
        }
    }

    public boolean inputValue(String fieldValue, String inputValue) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, 0, true, (Key)null);
    }

    public boolean inputValue(String fieldValue, String inputValue, boolean position) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, 0, position, (Key)null);
    }

    public boolean inputValue(String fieldValue, String inputValue, Key key) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, 0, true, key);
    }

    public boolean inputValue(String fieldValue, String inputValue, boolean position, Key key) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, 0, position, key);
    }

    public boolean inputValue(String fieldValue, String inputValue, int sequenceNumber) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, sequenceNumber, true, (Key)null);
    }

    public boolean inputValue(String fieldValue, String inputValue, int sequenceNumber, boolean position) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, sequenceNumber, position, (Key)null);
    }

    public boolean inputValue(String fieldValue, String inputValue, int sequenceNumber, Key key) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldValue, inputValue, sequenceNumber, true, key);
    }

    public boolean inputValue(String fieldValue, String inputValue, int sequenceNumber, boolean position, Key key) throws Hogan3270Exception, IOException {
        ArrayList fieldPostion = new ArrayList();

        try {
            this.fields = this.getFields();
            Field[] var8 = this.fields;
            int var9 = var8.length;

            for(int var10 = 0; var10 < var9; ++var10) {
                Field field = var8[var10];
                if (field.getValue().contains(fieldValue)) {
                    fieldPostion.add(field.getFieldNumber());
                }
            }

            if (fieldPostion.size() > 0) {
                int fieldPos = (Integer)fieldPostion.get(sequenceNumber);
                int i = fieldPos;

                while(i < this.fields.length) {
                    if (!this.fields[i].isProtected()) {
                        this.inputValue(this.fields[i].getFieldNumber(), inputValue, key);
                        return true;
                    }

                    if (position) {
                        ++i;
                    } else {
                        --i;
                    }
                }

                return false;
            } else {
                return false;
            }
        } catch (JagacyException var12) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var12);
        }
    }

    public boolean inputValue(int fieldNumber, String inputValue) throws Hogan3270Exception, IOException {
        return this.inputValue(fieldNumber, inputValue, (Key)null);
    }

    public boolean inputValue(int fieldNumber, String inputValue, Key key) throws Hogan3270Exception, IOException {
        String emptyFieldValue = "";

        try {
            this.fields = this.getFields();
            int fieldValueLength = this.fields[fieldNumber - 1].getValue().length();

            for(int k = 0; k < fieldValueLength; ++k) {
                emptyFieldValue = emptyFieldValue + " ";
            }

            this.session3270.writeField(fieldNumber, 1, emptyFieldValue);
            this.session3270.writeField(fieldNumber, 1, inputValue);
            String inputDetails = "Value: '" + inputValue + "' | Number: " + fieldNumber + " | Row: " + this.fields[fieldNumber].getRow() + " | Col: " + this.fields[fieldNumber].getColumn();
            this.logInput(inputDetails);
            if (key != null) {
                this.sendKey(key);
            }

            return true;
        } catch (JagacyException var8) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var8);
        }
    }

    public boolean inputValue(int x_Coordinates, int y_Coordinates, String inputValue) throws Hogan3270Exception, IOException {
        try {
            this.session3270.writePosition(x_Coordinates - 1, y_Coordinates - 1, inputValue);
            String inputDetails = "Value: '" + inputValue + " | Row: " + x_Coordinates + " | Col: " + y_Coordinates;
            this.logInput(inputDetails);
            return true;
        } catch (JagacyException var6) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var6);
        }
    }

    public boolean inputValueAtFirstCursor(String inputValue) throws Hogan3270Exception, IOException {
        try {
            this.session3270.writeString(inputValue);
            String inputDetails = "Value: '" + inputValue + "' | Number: <Unknown> | Row: <Unknown> | Col: <Unknown>";
            this.logInput(inputDetails);
            return true;
        } catch (JagacyException var4) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var4);
        }
    }

    public void sendKey(Key key) throws Hogan3270Exception, IOException {
        try {
            if (this.session3270.waitForUnlock(this.waitForUnlockTimeout)) {
                this.logScreen((String)null);
                this.session3270.writeKey(key);
                this.logSendKeys(key);
                this.session3270.waitForUnlock(this.waitForUnlockTimeout);
            } else {
                throw new Hogan3270Exception("CONNECTION TIMED OUT DUE TO LOCKED STATUS");
            }
        } catch (JagacyException var3) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var3);
        }
    }

    public void logScreen(String screenName) throws Hogan3270Exception, IOException {
        if (this.isLogScreenTrace()) {
            FileWriter writer = null;
            if (this.logFile != null) {
                writer = new FileWriter(this.logFile, true);
            }

            if (screenName != null) {
                ScreenTraceLog.logScreenRetrieved(screenName, writer);
            }

            try {
                ScreenTraceLog.logScreenContent(this.session3270.readScreen(), writer);
            } catch (JagacyException var4) {
                throw Hogan3270Exception.convertJagacyExceptionToHoganException(var4);
            }
        }

    }

    private void logSendKeys(Key key) throws Hogan3270Exception, IOException {
        if (this.isLogSendKeyTrace()) {
            FileWriter writer = null;
            if (this.logFile != null) {
                writer = new FileWriter(this.logFile, true);
            }

            ScreenTraceLog.logSendKey(key.toString(), writer);
        }

    }

    private void logInput(String inputDetails) throws Hogan3270Exception, IOException {
        if (this.isLogInputTrace()) {
            FileWriter writer = null;
            if (this.logFile != null) {
                writer = new FileWriter(this.logFile, true);
            }

            ScreenTraceLog.logInputValue(inputDetails, writer);
        }

    }

    public void logFields() throws Hogan3270Exception, IOException {
        FileWriter writer = null;
        if (this.logFile != null) {
            writer = new FileWriter(this.logFile, true);
        }

        try {
            this.fields = this.getFields();
            String[] fieldDetails = new String[this.fields.length];

            for(int i = 0; i < this.fields.length; ++i) {
                String fieldVal = this.fields[i].getValue();
                fieldDetails[i] = "Field number: " + this.fields[i].getFieldNumber() + "\nField Location: " + this.fields[i].getLocation() + "\nIs Protected: " + this.fields[i].isProtected() + "\nField Value: " + fieldVal + "\n ----------------------------------";
            }

            ScreenTraceLog.logFields(fieldDetails, writer);
        } catch (JagacyException var5) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var5);
        }
    }

    public String getText(int fromRow, int fromCol, int toRow, int toCol) throws Hogan3270Exception {
        StringBuilder text = new StringBuilder();
        --fromCol;
        --fromRow;
        --toCol;
        --toRow;

        try {
            char[][] lineGrid = new char[24][80];
            String[] screenContent = this.session3270.readScreen();

            int i;
            for(i = 0; i < screenContent.length; ++i) {
                lineGrid[i] = screenContent[i].toCharArray();
            }

            for(i = fromRow; i <= toRow; ++i) {
                if (i >= fromRow && i <= toRow) {
                    for(int j = fromCol; j <= toCol; ++j) {
                        if (j >= fromCol && j <= toCol) {
                            text.append(lineGrid[i][j]);
                        }
                    }

                    if (i + 1 <= toRow) {
                        text.append("\n");
                    }
                }
            }
        } catch (JagacyException var10) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var10);
        }

        return text.toString().trim();
    }

    public String readRow(int rowNumber) throws Hogan3270Exception {
        try {
            return this.session3270.readRow(rowNumber);
        } catch (JagacyException var3) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var3);
        }
    }

    public int readFieldsAmount() throws Hogan3270Exception {
        try {
            return this.session3270.readFields().length;
        } catch (JagacyException var2) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var2);
        }
    }

    public void close() throws Hogan3270Exception {
        try {
            this.session3270.close();
        } catch (JagacyException var2) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var2);
        }
    }

    public void open() throws Hogan3270Exception {
        try {
            this.session3270.open();
        } catch (JagacyException var2) {
            throw Hogan3270Exception.convertJagacyExceptionToHoganException(var2);
        }
    }

    Field[] getFields() throws JagacyException {
        this.fields = this.session3270.readFields();
        return this.fields;
    }

    public void setFields(Field[] fields) {
        this.fields = fields;
    }

    private boolean isLogScreenTrace() {
        return this.logScreenTrace;
    }

    private void setLogScreenTrace(boolean logScreenTrace) {
        this.logScreenTrace = logScreenTrace;
    }

    private boolean isLogInputTrace() {
        return this.logInputTrace;
    }

    private void setLogInputTrace(boolean logInputTrace) {
        this.logInputTrace = logInputTrace;
    }

    private boolean isLogSendKeyTrace() {
        return this.logSendKeyTrace;
    }

    private void setLogSendKeyTrace(boolean logSendKeyTrace) {
        this.logSendKeyTrace = logSendKeyTrace;
    }

    public int getWaitForChangeTimeout() {
        return this.waitForChangeTimeout;
    }

    private void setWaitForChangeTimeout(int waitForChangeTimeout) {
        this.waitForChangeTimeout = waitForChangeTimeout;
    }

    public int getWaitForUnlockTimeout() {
        return this.waitForUnlockTimeout;
    }

    private void setWaitForUnlockTimeout(int waitForUnlockTimeout) {
        this.waitForUnlockTimeout = waitForUnlockTimeout;
    }

    public int getWaitForIntermediateTimeout() {
        return this.waitForIntermediateTimeout;
    }

    public void setWaitForIntermediateTimeout(int waitForIntermediateTimeout) {
        this.waitForIntermediateTimeout = waitForIntermediateTimeout;
    }

    public File getLogFile() {
        return this.logFile;
    }

    public void setLogFile(int iteration) throws IOException {
        boolean directoryCreated = true;
        this.logFile = new File(this.folderName);
        if (!this.logFile.isDirectory()) {
            directoryCreated = this.logFile.mkdirs();
        }

        if (directoryCreated) {
            this.logFile = new File(this.folderName + "\\3270-screenTrace-iteration-" + iteration + ".txt");
            if (!this.logFile.exists()) {
                this.logFile.createNewFile();
            }

        } else {
            throw new IOException("The directory for the report folder was not created successfully");
        }
    }

    public void setFolderName(String folderName) {
        this.folderName = folderName;
    }

    public void setLogFileNull() {
        this.logFile = null;
    }
}
