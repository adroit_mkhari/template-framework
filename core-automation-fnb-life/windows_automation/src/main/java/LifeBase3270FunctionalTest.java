//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//


import com.jagacy.Key;
import gts.automation.terminal._3270.Hogan3270Exception;
import gts.automation.terminal._3270.Hogan3270Key;
import gts.automation.terminal._3270.Hogan3270Session;
import gts.automation.terminal._3270.athorization.AuthorizeLogin;
import gts.testauto.base.BaseFunctionalTest;
import gts.testauto.base.TestRuntimeException;
import gts.testauto.base.TestStep;
import gts.testauto.equivalency.OutputEquivalency;
import gts.testauto.flow.StackIteratingStep;
import gts.testauto.flow.Steps;
import gts.testauto.interaction.base.Output;
import gts.testauto.text.Resource;
import gts.testauto.text.regex.StringMatchPredicate;
import gts.testauto.ui.flow.BaseAssertion;
import java.io.IOException;
import java.util.Map;
import java.util.Properties;
import java.util.function.BiConsumer;
import java.util.function.Predicate;
import org.apache.log4j.Logger;
import org.hamcrest.BaseMatcher;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.junit.Before;

public abstract class LifeBase3270FunctionalTest extends BaseFunctionalTest {
    private static final Logger LOG = Logger.getLogger(LifeBase3270FunctionalTest.class);
    private Life3270Session hogan3270Session;
    private int screenLogCounter = 0;
    private final String POSITION = "position:";
    private String configFile;

    public LifeBase3270FunctionalTest() {
    }

    @Before
    public void mainSetup() throws Exception {
        this.configFile = System.getProperty("user.dir") + System.getProperty("file.separator") + this.text("src/test/resources/test-qa", new Output[0]);
        this._initialize(this.configFile);
    }

    public TestStep initialize(String configFile) {
        return Steps._do("initializing session", () -> {
            this._initialize(configFile);
        });
    }

    private void _initialize(String configFile) {
        try {
            this.hogan3270Session = Life3270Session.newSession(configFile);
            this.hogan3270Session.setFolderName(this.getExactReportFolderLocation());
        } catch (Hogan3270Exception var3) {
            var3.printStackTrace();
            throw new TestRuntimeException("could not initialize session.", var3);
        } catch (IOException var4) {
            var4.printStackTrace();
            throw new TestRuntimeException(var4.getMessage(), var4);
        }
    }

    public TestStep resetSession() {
        return Steps._do("resetting session", () -> {
            this._resetSession();
        });
    }

    private void _resetSession() {
        try {
            this.hogan3270Session.logScreen((String)null);
            this.hogan3270Session.close();
            this._initialize(this.configFile);
        } catch (Hogan3270Exception var2) {
            LOG.error("Error when trying to reset session.", var2);
            throw new TestRuntimeException("Error when trying to reset session.", var2);
        } catch (IOException var3) {
            throw new TestRuntimeException("Issue logging screen before session reset");
        }
    }

    public TestStep closeSession() {
        return Steps._do("closing session", () -> {
            this._closeSession();
        });
    }

    private void _closeSession() {
        try {
            this.hogan3270Session.logScreen((String)null);
            this.hogan3270Session.close();
        } catch (Hogan3270Exception var2) {
            LOG.error(var2.getMessage(), var2);
        } catch (IOException var3) {
            throw new TestRuntimeException("Issue logging screen before session close");
        }

    }

    public TestStep inputDataOnScreen(String screenName, Resource<Output> input) {
        return Steps._do("Input Data On Screen", () -> {
            this._inputDataOnScreen(screenName, (Output)input.getContent(), "ENTER");
        });
    }

    public TestStep inputDataOnScreen(String screenName, Resource<Output> input, String hogan3270Key) {
        return Steps._do("Input Data On Screen", () -> {
            this._inputDataOnScreen(screenName, (Output)input.getContent(), hogan3270Key);
        });
    }

    public TestStep inputDataOnScreenBeforeLabel(String screenName, Resource<Output> input) {
        return Steps._do("Input Data On Screen", () -> {
            this._inputDataOnScreenBeforeLabel(screenName, (Output)input.getContent(), "ENTER");
        });
    }

    public TestStep inputDataOnScreenBeforeLabel(String screenName, Resource<Output> input, String hogan3270Key) {
        return Steps._do("Input Data On Screen", () -> {
            this._inputDataOnScreenBeforeLabel(screenName, (Output)input.getContent(), hogan3270Key);
        });
    }

    public TestStep login3270() {
        return this.login3270((String)null, (String)null);
    }

    public TestStep login3270(String username, String password) {
        if (username == null || password == null) {
            AuthorizeLogin.authorize(username, password);
            if (AuthorizeLogin.getUsername() == null || AuthorizeLogin.getPassword() == null) {
                throw new TestRuntimeException("Credentials to be used is not set");
            }

            username = AuthorizeLogin.getUsername();
            password = AuthorizeLogin.getPassword();
        }

        return this.step("Login", "Login to 3270 using a username and password", new OutputEquivalency[0]).is(new TestStep[]{this.inputDataOnScreen("Terminal", this.keyValueInput("position:1", this.get("3270.network"))), this.pressKeyStep("ENTER"), this.inputDataOnScreen(this.get("3270.network").toUpperCase() + " - ", this.loginInput(username, password)), this.inputDataOnScreen("SERVICE SELECTION MENU", this.keyValueInput("==>", "lf " + this.get("3270.service"))), this.inputDataOnScreen("SERVICE SELECTION MENU", this.keyValueInput("==>", this.get("3270.service")))});
    }

    private Resource<Output> loginInput(String username, String password) {
        Resource<Output> outputResource = () -> {
            Output output = new Output();
            output.add("Operator Identification", username);
            output.add("Password", password);
            return output;
        };
        return outputResource;
    }

    private void _inputDataOnScreen(String screenName, Output input, String hogan3270Key) {
        try {
            if (this.hogan3270Session.verifyScreen(screenName)) {
                Map map = input.getAttributes();
                map.forEach((k, v) -> {
                    this.checkForInteger(k.toString(), v.toString(), true);
                });
            }

            // this.hogan3270Session.sendKey(Hogan3270Key.getKey(hogan3270Key));
        } catch (Hogan3270Exception var5) {
            LOG.error(var5.getMessage(), var5);
            this.fail("Error verifying screen", var5);
        } catch (IOException var6) {
            LOG.error(var6.getMessage(), var6);
            this.fail("Error with logging the screen", var6);
        }

    }

    private void _inputDataOnScreenBeforeLabel(String screenName, Output input, String hogan3270Key) {
        try {
            if (this.hogan3270Session.verifyScreen(screenName)) {
                Map map = input.getAttributes();
                map.forEach((k, v) -> {
                    this.checkForInteger(k.toString(), v.toString(), false);
                });
            }

            // this.hogan3270Session.sendKey(Hogan3270Key.getKey(hogan3270Key));
        } catch (Hogan3270Exception var5) {
            LOG.error(var5.getMessage(), var5);
            this.fail("Error verifying screen", var5);
        } catch (IOException var6) {
            LOG.error(var6.getMessage(), var6);
            this.fail("Error with logging the screen", var6);
        }

    }

    public TestStep setLogFile() {
        return Steps._do("set log file", () -> {
            ++this.screenLogCounter;

            try {
                this.hogan3270Session.setLogFile(this.screenLogCounter);
            } catch (IOException var2) {
                LOG.error("Error while creating the log file.", var2);
                throw new TestRuntimeException("Error while creating the log file.", var2);
            }
        });
    }

    public TestStep logOffSession() {
        return Steps._do("Return to beginning", () -> {
            try {
                this.hogan3270Session.sendKey(Key.CLEAR);
                this.hogan3270Session.inputValueAtFirstCursor("/rcl");
                this.hogan3270Session.sendKey(Key.ENTER);
                if (this.hogan3270Session.verifyScreen("service")) {
                    this.hogan3270Session.inputValue("==>", "logoff");
                    this.hogan3270Session.sendKey(Key.ENTER);
                }
            } catch (Hogan3270Exception var2) {
                LOG.error(var2.getMessage(), var2);
                this.fail("Problem occurred while clearing the screen", var2);
            } catch (IOException var3) {
                LOG.error(var3.getMessage(), var3);
                this.fail("Error with logging the screen or key presses", var3);
            }

        });
    }

    public TestStep returnToMainMenu() {
        return Steps._do("Return to beginning", () -> {
            try {
                this.hogan3270Session.sendKey(Key.CLEAR);
                this.hogan3270Session.inputValueAtFirstCursor("rmenu");
                this.hogan3270Session.sendKey(Key.ENTER);
            } catch (Hogan3270Exception var2) {
                LOG.error(var2.getMessage(), var2);
                this.fail("Problem occurred while clearing the screen", var2);
            } catch (IOException var3) {
                LOG.error(var3.getMessage(), var3);
                this.fail("Error with logging the screen or key presses", var3);
            }

        });
    }

    public TestStep pressKeyStep(String hogan3270Key) {
        return Steps._do("Press Received Key", () -> {
            //this._pressKey(Hogan3270Key.getKey(hogan3270Key));
        });
    }

    public void pressKey(String hogan3270Key) {
        //this._pressKey(Hogan3270Key.getKey(hogan3270Key));
    }

    private void _pressKey(Key key) {
        if (key == null) {
            String msg = "Invalid key recieved to press";
            this.fail(msg, new Hogan3270Exception(msg));
        } else {
            try {
                this.hogan3270Session.sendKey(key);
            } catch (Hogan3270Exception var3) {
                LOG.error(var3.getMessage(), var3);
                this.fail("Error pressing key : " + key.toString(), var3);
            } catch (IOException var4) {
                LOG.error(var4.getMessage(), var4);
                this.fail("Error with logging the key pressed", var4);
            }
        }

    }

    private void checkForInteger(String atField, String valueToEnter, boolean afterLable) {
        if (atField.startsWith("position:")) {
            atField = atField.replace("position:", "");

            try {
                int field = Integer.parseInt(atField);
                this.inputValue(field, valueToEnter, afterLable);
            } catch (NumberFormatException var5) {
                throw new TestRuntimeException("Invalid number entered for position [" + atField + "].", var5);
            }
        } else {
            this.inputValue(atField, valueToEnter, afterLable);
        }

    }

    private void inputValue(Object atField, String valueToEnter, boolean afterLable) {
        try {
            if (atField instanceof Integer) {
                this.hogan3270Session.inputValue((Integer)atField, valueToEnter);
            } else {
                this.hogan3270Session.inputValue((String)atField, valueToEnter, afterLable);
            }
        } catch (Hogan3270Exception var5) {
            LOG.error(var5.getMessage(), var5);
            this.fail("Error entering value at label [" + atField + "] value [" + valueToEnter + "]", var5);
        } catch (IOException var6) {
            LOG.error(var6.getMessage(), var6);
            this.fail("Error with logging the input values", var6);
        }

    }

    public Resource<Output> keyValueInput(final String key, final String value) {
        Resource<Output> output = new Resource<Output>() {
            public Output getContent() {
                Output output = new Output();
                output.add(key, value);
                return output;
            }
        };
        return output;
    }

    public void fail(String message, Throwable cause) {
        throw new AssertionError(message, cause);
    }

    public Life3270Session getSession() {
        return this.hogan3270Session;
    }

    public StackIteratingStep forEveryDataSheetRow(TestStep... steps) {
        TestStep[] mySteps = new TestStep[steps.length + 1];
        mySteps[0] = this.setLogFile();
        int pos = 1;
        TestStep[] var4 = steps;
        int var5 = steps.length;

        for(int var6 = 0; var6 < var5; ++var6) {
            TestStep step = var4[var6];
            mySteps[pos] = step;
            ++pos;
        }

        return this.iterate(mySteps);
    }

    public String getObjectValue(String propertyName) throws Hogan3270Exception {
        String propertyValue = this.getProperty(propertyName);
        String returnValue = null;
        if (propertyValue != null) {
            String[] propertyObject = propertyValue.toString().split(",");
            if (propertyObject.length > 1) {
                try {
                    returnValue = this.hogan3270Session.getText(Integer.parseInt(propertyObject[0]), Integer.parseInt(propertyObject[1]), Integer.parseInt(propertyObject[2]), Integer.parseInt(propertyObject[3]));
                } catch (NumberFormatException var7) {
                    this.fail("The given property value is incorrect.", new Hogan3270Exception("The given property value is incorrect."));
                }
            } else if (propertyObject.length == 1) {
                try {
                    returnValue = this.hogan3270Session.findFieldValue(Integer.parseInt(propertyObject[0]));
                } catch (NumberFormatException var6) {
                    this.fail("The given property value is incorrect. Value received from property " + propertyName + " is " + propertyObject[0], new Hogan3270Exception("The given property value is incorrect. Value received from property " + propertyName + " is " + propertyObject[0]));
                }
            } else {
                this.fail("The given property value is incorrect. Indicate value type with either 'region:' or 'field:'.", new Hogan3270Exception("The given property value is incorrect.  Indicate value type with either 'region:' or 'field:'."));
            }
        } else {
            this.fail("The given property value is incorrect. Indicate value type with either 'region:' or 'field:'.", new Hogan3270Exception("The given property value is incorrect.  Indicate value type with either 'region:' or 'field:'."));
        }

        return returnValue;
    }

    public TestStep navigateToSystem(String systemName) {
        return Steps._do("Navigate through the listed systems", () -> {
            try {
                String pageNumberField;
                if (this.hogan3270Session.verifyScreen("MENU 0 ZME")) {
                    do {
                        pageNumberField = this.hogan3270Session.findFieldValue(25);
                        int fieldPosition = 32;

                        for(int i = 0; i < 30; ++i) {
                            String fieldValue = this.hogan3270Session.findFieldValue(fieldPosition);
                            if (fieldValue.equals(systemName.toUpperCase())) {
                                String command = this.hogan3270Session.findFieldValue(fieldPosition - 1);
                                this.hogan3270Session.inputValue("Command ==>", command.trim());
                                this.hogan3270Session.sendKey(Key.ENTER);
                                return;
                            }

                            fieldPosition += 3;
                        }

                        this.hogan3270Session.sendKey(Key.PF8);
                    } while(!this.hogan3270Session.findFieldValue(25).equalsIgnoreCase(pageNumberField));
                }
            } catch (Hogan3270Exception var7) {
                LOG.error("Something went wrong in the navigating to " + systemName, var7);
                this.fail("Something went wrong in the navigating to " + systemName, var7);
            } catch (IOException var8) {
                LOG.error("Something went wrong in the logging of the navigating to " + systemName, var8);
                throw new TestRuntimeException("Something went wrong in the logging of the navigating to " + systemName, var8);
            }

        });
    }

    public Predicate<Boolean> possibleScreen(String screenName) {
        return (b) -> {
            try {
                return this.hogan3270Session.verifyScreen(screenName, false);
            } catch (Hogan3270Exception var4) {
                LOG.error("Something went wrong in the verification of the screen " + screenName, var4);
                this.fail("Something went wrong in the verification of the screen " + screenName, var4);
                return false;
            } catch (IOException var5) {
                LOG.error("Something went wrong in the logging of the verification of the screen " + screenName, var5);
                throw new TestRuntimeException("Something went wrong in the verification of the screen " + screenName, var5);
            }
        };
    }

    public TestStep pauseBreakScreen(String command) {
        return Steps._do("Clear the screen and navigate to specified command screen", () -> {
            int timeOutCounter = 0;
            byte timeOutCounterLimit = 10;

            try {
                this.hogan3270Session.sendKey(Key.CLEAR);

                while(!this.hogan3270Session.getText(1, 1, 24, 80).equalsIgnoreCase("")) {
                    if (timeOutCounter > timeOutCounterLimit) {
                        throw new Hogan3270Exception("The screen did not clear within 10 attempts to clear the screen.");
                    }

                    this.hogan3270Session.sendKey(Key.CLEAR);
                    ++timeOutCounter;
                }

                this.hogan3270Session.inputValueAtFirstCursor(command);
                this.hogan3270Session.sendKey(Key.ENTER);
            } catch (Hogan3270Exception var5) {
                LOG.error("Something went wrong in the navigation using the command " + command, var5);
                this.fail("Something went wrong in the navigation using the command " + command, var5);
            } catch (IOException var6) {
                LOG.error("Something went wrong in the logging of the navigation using the command " + command, var6);
                throw new TestRuntimeException("Something went wrong in the navigation using the command " + command, var6);
            }

        });
    }

    public Properties provideReportData() {
        return this.getTextSupport().stack();
    }

    public <T> BaseAssertion thisValue(Resource<T> value) {
        return new BaseAssertion(value, this.getTextSupport());
    }

    public static Matcher<String> matchOneOf(final String firstValue, final String secondValue, final String... moreValues) {
        return new BaseMatcher<String>() {
            public void describeTo(Description description) {
                description.appendText("");
            }

            public boolean matches(Object o) {
                StringMatchPredicate stringMatchPredicate = new StringMatchPredicate(firstValue);
                if (stringMatchPredicate.test(o.toString())) {
                    return true;
                } else {
                    stringMatchPredicate = new StringMatchPredicate(secondValue);
                    if (stringMatchPredicate.test(o.toString())) {
                        return true;
                    } else {
                        String[] var3 = moreValues;
                        int var4 = var3.length;

                        for(int var5 = 0; var5 < var4; ++var5) {
                            String value = var3[var5];
                            stringMatchPredicate = new StringMatchPredicate(value);
                            if (stringMatchPredicate.test(o.toString())) {
                                return true;
                            }
                        }

                        return false;
                    }
                }
            }
        };
    }

    public Life3270Session getHogan3270Session() {
        return this.hogan3270Session;
    }

    public void setHogan3270Session(Life3270Session hogan3270Session) {
        this.hogan3270Session = hogan3270Session;
    }
}
