package files;

import org.testng.annotations.Test;

import java.io.FileWriter;
import java.sql.ResultSet;

public class DatabaseTransactorTests {
    @Test
    public void databaseTransactorDefaultConfigFile() {
        try {
            DatabaseTransactor databaseTransactor = new DatabaseTransactor();
            databaseTransactor.connectToDatabase();
            ResultSet resultSet = databaseTransactor.executeQuery("SELECT * FROM FUNINS_P.FIPOLMPF");
            while (resultSet.next()) {
                String someValue = resultSet.getString("POLICYNO");
                System.out.println("POLICYNO: " + someValue);
            }
            databaseTransactor.closeDatabaseConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void databaseTransactorSpecificConfigFile() {
        try {
            DatabaseTransactor databaseTransactor = new DatabaseTransactor("src\\test\\resources\\data_config.properties");
            databaseTransactor.connectToDatabase();
            ResultSet resultSet = databaseTransactor.executeQuery("SELECT * FROM SOME_TABLE LIMIT 1");
            while (resultSet.next()) {
                String someValue = resultSet.getString("SOME_VALUE");
                System.out.println("SOME_VALUE: " + someValue);
            }
            databaseTransactor.closeDatabaseConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void databaseTransactorDefaultConfigFileAlis() {
        try {

            FileWriter writer = new FileWriter("test.csv");
            DatabaseTransactor databaseTransactor = new DatabaseTransactor();
            ResultSet resultSet = databaseTransactor.executeQuery("select top 1000 pcr.id, mc.External_id,COUNT" +
                    "(distinct policy_no) from p_client_role pcr\n" +
                    "inner join z_m_client_expiry_status zmces on pcr.id = zmces.client_id\n" +
                    "inner join m_clients mc on zmces.client_id = mc.id and mc.closing_status = 0 and" +
                    " mc.external_id != '' and mc.external_id != '9999999999999' and mc.external_id != '99999999'\n" +
                    "where pcr.opening_status between 10 and 19 and pcr.closing_status = 0 and role = 10\n" +
                    "and pcr.id not in (1466759\n" +
                    ",1541764\n" +
                    ",797323\n" +
                    ",1184401\n" +
                    ",1191640\n" +
                    ",1575724\n" +
                    ",1500719\n" +
                    ",1414230\n" +
                    ",1578718\n" +
                    ",1622419\n" +
                    ",437404\n" +
                    ",1306370\n" +
                    ",1211560\n" +
                    ",1228540\n" +
                    ",784688\n" +
                    ",898236\n" +
                    ",1289390\n" +
                    ",1557393\n" +
                    ",1392905\n" +
                    ",1332055\n" +
                    ",1636674\n" +
                    ",739367\n" +
                    ",1349035\n" +
                    ",1409885\n" +
                    ",1096561\n" +
                    ",1619694\n" +
                    ",1262500\n" +
                    ",498254\n" +
                    ",1323350\n" +
                    ",1602714\n" +
                    ",1551774\n" +
                    ",1585734\n" +
                    ",1460825\n" +
                    ",1443845\n" +
                    ",1130521\n" +
                    ",288445\n" +
                    ",1326290\n" +
                    ",1261195\n" +
                    ",1629704\n" +
                    ",173715\n" +
                    ",1309310\n" +
                    ",1608479\n" +
                    ",964851\n" +
                    ",1329115\n" +
                    ",1042681\n" +
                    ",1312135\n" +
                    ",1322045\n" +
                    ",1356005\n" +
                    ",241635\n" +
                    ",1025701\n" +
                    ",1372985\n" +
                    ",1282420\n" +
                    ",1295155\n" +
                    ",1389965\n" +
                    ",1059661\n" +
                    ",1595744\n" +
                    ",1003056\n" +
                    ",1642439\n" +
                    ",1591499\n" +
                    ",1477974\n" +
                    ",1056936\n" +
                    ",983182\n" +
                    ",1581489\n" +
                    ",89951\n" +
                    ",1073916\n" +
                    ",1319105\n" +
                    ",1211391\n" +
                    ",1494954\n" +
                    ",1564509\n" +
                    ",1353280\n" +
                    ",1177431\n" +
                    ",567763\n" +
                    ",1444014\n" +
                    ",1285145\n" +
                    ",1268165\n" +
                    ",322574\n" +
                    ",1548834\n" +
                    ",1527609\n" +
                    ",1584383\n" +
                    ",1157511\n" +
                    ",1476669\n" +
                    ",1093836\n" +
                    ",1106571\n" +
                    ",1497894\n" +
                    ",1622588\n" +
                    ",1514874\n" +
                    ",484168\n" +
                    ",1470835\n" +
                    ",1531854\n" +
                    ",1510629\n" +
                    ",1635323\n" +
                    ",1571648\n" +
                    ",1110816\n" +
                    ",797492\n" +
                    ",1248245\n" +
                    ",1588628\n" +
                    ",1567403\n" +
                    ",1475080\n" +
                    ",861167\n" +
                    ",1252490\n" +
                    ",728313\n" +
                    ",602176\n" +
                    ",254892\n" +
                    ",322812\n" +
                    ",1410292\n" +
                    ",1043111\n" +
                    ",1516509\n" +
                    ",1448589\n" +
                    ",691413\n" +
                    ",657453\n" +
                    ",1482549\n" +
                    ",1113948\n" +
                    ",397986\n" +
                    ",1359352\n" +
                    ",1046028\n" +
                    ",1339455\n" +
                    ",1499529\n" +
                    ",745270\n" +
                    ",568216\n" +
                    ",1169225\n" +
                    ",1004737\n" +
                    ",1356435\n" +
                    ",47785\n" +
                    ",585196\n" +
                    ",1288515\n" +
                    ",1465569\n" +
                    ",288852\n" +
                    ",1502446\n" +
                    ",1519426\n" +
                    ",1481152\n" +
                    ",933877\n" +
                    ",1420179\n" +
                    ",899917\n" +
                    ",1159338\n" +
                    ",376692\n" +
                    ",578103\n" +
                    ",1472662\n" +
                    ",629043\n" +
                    ",359712\n" +
                    ",803564\n" +
                    ",1447192\n" +
                    ",646023\n" +
                    ",654513\n" +
                    ",80348\n" +
                    ",812054\n" +
                    ",1394709\n" +
                    ",341189\n" +
                    ",324209\n" +
                    ",88838\n" +
                    ",385182\n" +
                    ",1377729\n" +
                    ",586593\n" +
                    ",1455682\n" +
                    ",317262\n" +
                    ",281759\n" +
                    ",1369239\n" +
                    ",1116888\n" +
                    ",1184808\n" +
                    ",334242\n" +
                    ",393672\n" +
                    ",1445649\n" +
                    ",595083\n" +
                    ",1413232\n" +
                    ",942367\n" +
                    ",690016\n" +
                    ",882937\n" +
                    ",871484\n" +
                    ",332699\n" +
                    ",837524\n" +
                    ",1489642\n" +
                    ",1167828\n" +
                    ",515733\n" +
                    ",114331\n" +
                    ",532713\n" +
                    ",413592\n" +
                    ",396612\n" +
                    ",134228\n" +
                    ",1230198\n" +
                    ",1221708\n" +
                    ",899894\n" +
                    ",1620193\n" +
                    ",142718\n" +
                    ",1167851\n" +
                    ",63391\n" +
                    ",388122\n" +
                    ",549693\n" +
                    ",105841\n" +
                    ",1611703\n" +
                    ",1184831\n" +
                    ",405102\n" +
                    ",88861\n" +
                    ",1637173\n" +
                    ",575163\n" +
                    ",1560502\n" +
                    ",871507\n" +
                    ",91778\n" +
                    ",846037\n" +
                    ",100268\n" +
                    ",1298379\n" +
                    ",1628683\n" +
                    ",403275\n" +
                    ",437235\n" +
                    ",1509278\n" +
                    ",1516225\n" +
                    ",716599\n" +
                    ",995963\n" +
                    ",1038413\n" +
                    ",1246894\n" +
                    ",472738\n" +
                    ",1526258\n" +
                    ",1465285\n" +
                    ",882630\n" +
                    ",159414\n" +
                    ",1238404\n" +
                    ",1195954\n" +
                    ",1534748\n" +
                    ",394785\n" +
                    ",1541695\n" +
                    ",445725\n" +
                    ",1212934\n" +
                    ",455758\n" +
                    ",386295\n" +
                    ",1258324\n" +
                    ",984533\n" +
                    ",671209\n" +
                    ",204804\n" +
                    ",1021410\n" +
                    ",1453855\n" +
                    ",176417\n" +
                    ",1289367\n" +
                    ",654229\n" +
                    ",1309264\n" +
                    ",1343224\n" +
                    ",1317754\n" +
                    ",628759\n" +
                    ",1428385\n" +
                    ",1283794\n" +
                    ",933593\n" +
                    ",961980\n" +
                    ",648656\n" +
                    ",247254\n" +
                    ",1266814\n" +
                    ",953490\n" +
                    ",238764\n" +
                    ",1419895\n" +
                    ",1445365\n" +
                    ",674126\n" +
                    ",552088\n" +
                    ",1462345\n" +
                    ",1436875\n" +
                    ",1482695\n" +
                    ",395215\n" +
                    ",1561945\n" +
                    ",1342226\n" +
                    ",1262976\n" +
                    ",1288446\n" +
                    ",992102\n" +
                    ",1013373\n" +
                    ",1143901\n" +
                    ",725519\n" +
                    ",1508165\n" +
                    ",1550830\n" +
                    ",1274091\n" +
                    ",585050\n" +
                    ",1079842\n" +
                    ",1593280\n" +
                    ",1177861\n" +
                    ",1454285\n" +
                    ",978062\n" +
                    ",1472516\n" +
                    ",1404596\n" +
                    ",1428815\n" +
                    ",1638670\n" +
                    ",637679\n" +
                    ",1550515\n" +
                    ",1542025\n" +
                    ",1508065\n" +
                    ",1316856\n" +
                    ",1386365\n" +
                    ",1268426\n" +
                    ",1108352\n" +
                    ",1641395\n" +
                    ",1289697\n" +
                    ",1234466\n" +
                    ",1251446\n" +
                    ",1564770\n" +
                    ",1294111\n" +
                    ",1454185\n" +
                    ",1471165\n" +
                    ",1323657\n" +
                    ",1624415\n" +
                    ",739674\n" +
                    ",1496850\n" +
                    ",1306677\n" +
                    ",1530810\n" +
                    ",942413\n" +
                    ",519955\n" +
                    ",980787\n" +
                    ",1437205\n" +
                    ",1640044\n" +
                    ",1206271\n" +
                    ",1618650\n" +
                    ",1218837\n" +
                    ",1635630\n" +
                    ",1322306\n" +
                    ",1601670\n" +
                    ",975022\n" +
                    ",1235817\n" +
                    ",1587515\n" +
                    ",1519595\n" +
                    ",1184877\n" +
                    ",1252797\n" +
                    ",1305326\n" +
                    ",1584690\n" +
                    ",762419\n" +
                    ",1257211\n" +
                    ",1339286\n" +
                    ",114377\n" +
                    ",800693\n" +
                    ",1363620\n" +
                    ",657384\n" +
                    ",1418851\n" +
                    ",1478281\n" +
                    ",1156467\n" +
                    ",834653\n" +
                    ",1591829\n" +
                    ",1241582\n" +
                    ",1469791\n" +
                    ",1486771\n" +
                    ",1427341\n" +
                    ",1233092\n" +
                    ",1380600\n" +
                    ",1258562\n" +
                    ",1372110\n" +
                    ",911278\n" +
                    ",614934\n" +
                    ",1451460\n" +
                    ",1476930\n" +
                    ",1510890\n" +
                    ",1493910\n" +
                    ",1485420\n" +
                    ",1373461\n" +
                    ",1356481\n" +
                    ",1407421\n" +
                    ",1339501\n" +
                    ",1060137\n" +
                    ",1085607\n" +
                    ",1442970\n" +
                    ",1415911\n" +
                    ",1111077)\n" +
                    "group by pcr.id, mc.external_id\n" +
                    "having COUNT(Distinct Policy_No) < 2\n" +
                    "order by 3 desc");
            while (resultSet.next()) {
                String someValue = resultSet.getString("External_id");
                System.out.println("External_id: " + someValue);

                writer.append(someValue);
                writer.append('\n');
            }
            writer.flush();
            writer.close();
            databaseTransactor.closeDatabaseConnection();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
