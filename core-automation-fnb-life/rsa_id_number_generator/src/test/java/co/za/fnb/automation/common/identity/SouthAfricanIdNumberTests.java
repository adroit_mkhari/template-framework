package co.za.fnb.automation.common.identity;

import org.junit.Assert;
import org.junit.Test;

public class SouthAfricanIdNumberTests {
    @Test
    public void southAfricanIdNumberGeneratorTest() {
        try {
            SouthAfricanIdNumber southAfricanIdNumber = new SouthAfricanIdNumber("93", "01", "11", Gender.MALE, true);
            southAfricanIdNumber.generateIdNumber();
            IdNumberValidatorUtility.IDNumberDetails idNumberDetails = southAfricanIdNumber.getIdNumberDetails();
            Assert.assertNotNull(idNumberDetails);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
