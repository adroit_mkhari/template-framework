package co.za.fnb.automation.common.identity;

public enum Gender {
    MALE,
    FEMALE
}
